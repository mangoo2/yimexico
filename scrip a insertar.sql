ALTER TABLE `grado_riesgo_actividad` ADD `clasificacion` TINYINT(2) NULL DEFAULT NULL AFTER `detalle_tipo_cli`;

ALTER TABLE `grado_riesgo_actividad`  
ADD `preg1` TEXT NULL DEFAULT NULL COMMENT '20. ¿El servicio lo está liquidando directamente el PEP?'  AFTER `grado`,  
ADD `preg2` TEXT NULL DEFAULT NULL COMMENT '21. Si la respuesta 20 es negativa, ¿quién está liquidando el servicio?'  AFTER `preg1`,  
ADD `preg3` TEXT NULL DEFAULT NULL COMMENT '22. ¿El servicio se liquidará total o parcialmente en efectivo?'  AFTER `preg2`,  
ADD `preg4` TEXT NULL DEFAULT NULL COMMENT '23. ¿El servicio se liquidará con transferencia bancaria internacional?'  AFTER `preg3`,  
ADD `preg5` TEXT NULL DEFAULT NULL COMMENT '24. ¿El servicio se liquidará con transferencia bancaria nacional?'  AFTER `preg4`,  
ADD `preg6` TEXT NULL DEFAULT NULL COMMENT '25. ¿La transferencia bancaria nacional o internacional proviene de la cuenta del PEP?'  AFTER `preg5`,  
ADD `preg7` TEXT NULL DEFAULT NULL COMMENT '26. Si la respuesta 23 es afirmativa, ¿de qué país provienen los fondos? (tener al lado el catálogo de países de alto riesgo para consulta)'  AFTER `preg6`,  
ADD `preg8` TEXT NULL DEFAULT NULL COMMENT '27. Si la respuesta 23 es afirmativa ¿Por qué los fondos provienen de ese país? '  AFTER `preg7`,  
ADD `preg9` TEXT NULL DEFAULT NULL COMMENT '28. ¿El servicio se liquidará con transferencia bancaria proveniente de alguna otra cuenta que no sea del PEP? '  AFTER `preg8`,  
ADD `preg10` TEXT NULL DEFAULT NULL COMMENT '29. Si la respuesta 28 es afirmativa, indicar quién es el titular de la cuenta de donde proviene el pago?'  AFTER `preg9`,  
ADD `preg11` TEXT NULL DEFAULT NULL COMMENT '30. Si la respuesta 28 es afirmativa, indicar ¿qué relación tiene el titular de la cuenta con el PEP?'  AFTER `preg10`,  
ADD `preg12` TEXT NULL DEFAULT NULL COMMENT '31 Si la respuesta 28 es afirmativa, indicar ¿El servicio lo liquida alguna empresa en donde el PEP o algún familiar sea accionista?'  AFTER `preg11`,  
ADD `preg13` TEXT NULL DEFAULT NULL COMMENT '32. ¿Cómo se liquidará el servicio?'  AFTER `preg12`,  
ADD `preg14` TEXT NULL DEFAULT NULL COMMENT '33 ¿El servicio lo liquidan dos o más personas (físicas o morales)?'  AFTER `preg13`,  
ADD `preg15` TEXT NULL DEFAULT NULL COMMENT '34. ¿El servicio se liquidará en uno o varios pagos?'  AFTER `preg14`;


ALTER TABLE `grado_riesgo_actividad` 
ADD `c_d_notaria` TINYINT(1) NULL DEFAULT NULL AFTER `preg15`, 
ADD `c_d_cliente_usuario` TINYINT(1) NULL DEFAULT NULL AFTER `c_d_notaria`;

---------------------------------------------------------------------------------------------
ALTER TABLE `grado_riesgo_actividad` ADD `estado` INT NULL DEFAULT NULL AFTER `nacionalidad`;

ALTER TABLE `grado_riesgo_actividad` ADD `preg16` TEXT NULL DEFAULT NULL AFTER `preg15`, ADD `preg17` TEXT NULL DEFAULT NULL AFTER `preg16`;

/* ***************************************** */
/*Yisus*/
/*add table uniones
add table uniones_clientes
add table beneficiario_union_cliente
*/
ALTER TABLE `beneficiario_fideicomiso` ADD `id_union` BIGINT(1) NOT NULL DEFAULT '0' AFTER `id_bene_moral`;
ALTER TABLE `beneficiario_fisica` ADD `id_union` BIGINT NOT NULL DEFAULT '0' AFTER `idtipo_cliente`;
ALTER TABLE `beneficiario_moral_moral` ADD `id_union` BIGINT NOT NULL DEFAULT '0' AFTER `id_bene_moral`;
ALTER TABLE `operaciones` ADD `id_union` INT NOT NULL DEFAULT '0' AFTER `id_cliente_transac`;

CREATE VIEW  lista_cli_union AS
	SELECT `uc`.*, `uc`.`id` as `iduc`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, concat(pfe.nombre, ' ', `pfe`.`apellido_paterno`, ' ', pfe.apellido_materno) as cliente, IFNULL(pfe.idtipo_cliente_p_f_e, 0) as idcc3, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_f_e` as `id_cliente_tipo`
FROM `uniones` `u`
JOIN `uniones_clientes` `uc` ON `uc`.`id_union`=`u`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`uc`.`id_perfilamiento`
JOIN `tipo_cliente_p_f_e` `pfe` ON `pfe`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `uc`.`activo` = 1
AND `u`.`activo` = 1

UNION 
SELECT `uc`.*, `uc`.`id` as `iduc`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `ecoi`.`denominacion` as `cliente`, IFNULL(ecoi.idtipo_cliente_e_c_o_i, 0) as idcc1, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_e_c_o_i` as `id_cliente_tipo`
FROM `uniones` `u`
JOIN `uniones_clientes` `uc` ON `uc`.`id_union`=`u`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`uc`.`id_perfilamiento`
JOIN `tipo_cliente_e_c_o_i` `ecoi` ON `ecoi`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE  `uc`.`activo` = 1
AND `u`.`activo` = 1

UNION 
SELECT `uc`.*, `uc`.`id` as `iduc`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `cf`.`denominacion` as `cliente`, IFNULL(cf.idtipo_cliente_f, 0) as idcc2, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_f` as `id_cliente_tipo`
FROM `uniones` `u`
JOIN `uniones_clientes` `uc` ON `uc`.`id_union`=`u`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`uc`.`id_perfilamiento`
JOIN `tipo_cliente_f` `cf` ON `cf`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `uc`.`activo` = 1
AND `u`.`activo` = 1
UNION 
SELECT `uc`.*, `uc`.`id` as `iduc`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, concat(pfm.nombre, ' ', `pfm`.`apellido_paterno`, ' ', pfm.apellido_materno) as cliente, IFNULL(pfm.idtipo_cliente_p_f_m, 0) as idcc4, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_f_m` as `id_cliente_tipo`
FROM `uniones` `u`
JOIN `uniones_clientes` `uc` ON `uc`.`id_union`=`u`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`uc`.`id_perfilamiento`
JOIN `tipo_cliente_p_f_m` `pfm` ON `pfm`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `uc`.`activo` = 1
AND `u`.`activo` = 1
UNION 
SELECT `uc`.*, `uc`.`id` as `iduc`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `pmmd`.`nombre_persona` as `cliente`, IFNULL(pmmd.idtipo_cliente_p_m_m_d, 0) as idcc5, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_m_m_d` as `id_cliente_tipo`
FROM `uniones` `u`
JOIN `uniones_clientes` `uc` ON `uc`.`id_union`=`u`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`uc`.`id_perfilamiento`
JOIN `tipo_cliente_p_m_m_d` `pmmd` ON `pmmd`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `uc`.`activo` = 1
AND `u`.`activo` = 1
UNION 
SELECT `uc`.*, `uc`.`id` as `iduc`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `pmme`.`razon_social` as `cliente`, IFNULL(pmme.idtipo_cliente_p_m_m_e, 0) as idcc6, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_m_m_e` as `id_cliente_tipo`
FROM `uniones` `u`
JOIN `uniones_clientes` `uc` ON `uc`.`id_union`=`u`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`uc`.`id_perfilamiento`
JOIN `tipo_cliente_p_m_m_e` `pmme` ON `pmme`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `uc`.`activo` = 1
AND `u`.`activo` = 1

CREATE VIEW  lista_operaciones AS 
SELECT `oc`.*, `o`.`id` as `idopera`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, concat(pfe.nombre, ' ', `pfe`.`apellido_paterno`, ' ', pfe.apellido_materno) as cliente, IFNULL(pfe.idtipo_cliente_p_f_e, 0) as idcc3, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_f_e` as `id_cliente_tipo`
FROM `operaciones` `o`
JOIN `operacion_cliente` `oc` ON `oc`.`id_operacion`=`o`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`oc`.`id_perfilamiento`
JOIN `tipo_cliente_p_f_e` `pfe` ON `pfe`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `o`.`activo` = 1
AND `oc`.`activo` = 1 UNION SELECT `oc`.*, `o`.`id` as `idopera`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `ecoi`.`denominacion` as `cliente`, IFNULL(ecoi.idtipo_cliente_e_c_o_i, 0) as idcc1, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_e_c_o_i` as `id_cliente_tipo`
FROM `operaciones` `o`
JOIN `operacion_cliente` `oc` ON `oc`.`id_operacion`=`o`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`oc`.`id_perfilamiento`
JOIN `tipo_cliente_e_c_o_i` `ecoi` ON `ecoi`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `o`.`activo` = 1
AND `oc`.`activo` = 1 UNION SELECT `oc`.*, `o`.`id` as `idopera`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `cf`.`denominacion` as `cliente`, IFNULL(cf.idtipo_cliente_f, 0) as idcc2, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_f` as `id_cliente_tipo`
FROM `operaciones` `o`
JOIN `operacion_cliente` `oc` ON `oc`.`id_operacion`=`o`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`oc`.`id_perfilamiento`
JOIN `tipo_cliente_f` `cf` ON `cf`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `o`.`activo` = 1
AND `oc`.`activo` = 1 UNION SELECT `oc`.*, `o`.`id` as `idopera`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, concat(pfm.nombre, ' ', `pfm`.`apellido_paterno`, ' ', pfm.apellido_materno) as cliente, IFNULL(pfm.idtipo_cliente_p_f_m, 0) as idcc4, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_f_m` as `id_cliente_tipo`
FROM `operaciones` `o`
JOIN `operacion_cliente` `oc` ON `oc`.`id_operacion`=`o`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`oc`.`id_perfilamiento`
JOIN `tipo_cliente_p_f_m` `pfm` ON `pfm`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `o`.`activo` = 1
AND `oc`.`activo` = 1 UNION SELECT `oc`.*, `o`.`id` as `idopera`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `pmmd`.`nombre_persona` as `cliente`, IFNULL(pmmd.idtipo_cliente_p_m_m_d, 0) as idcc5, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_m_m_d` as `id_cliente_tipo`
FROM `operaciones` `o`
JOIN `operacion_cliente` `oc` ON `oc`.`id_operacion`=`o`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`oc`.`id_perfilamiento`
JOIN `tipo_cliente_p_m_m_d` `pmmd` ON `pmmd`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `o`.`activo` = 1
AND `oc`.`activo` = 1 UNION SELECT `oc`.*, `o`.`id` as `idopera`, `p`.`idperfilamiento` as `idperfilamientop`, `p`.`idcliente`, `p`.`idtipo_cliente`, `pmme`.`razon_social` as `cliente`, IFNULL(pmme.idtipo_cliente_p_m_m_e, 0) as idcc6, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, `idtipo_cliente_p_m_m_e` as `id_cliente_tipo`
FROM `operaciones` `o`
JOIN `operacion_cliente` `oc` ON `oc`.`id_operacion`=`o`.`id`
JOIN `perfilamiento` `p` ON `p`.`idperfilamiento`=`oc`.`id_perfilamiento`
JOIN `tipo_cliente_p_m_m_e` `pmme` ON `pmme`.`idperfilamiento`=`p`.`idperfilamiento`
WHERE `o`.`activo` = 1
AND `oc`.`activo` = 1 

/*add table grado_riesgo_actividad
add table cuestionario_ampliado
*/
ALTER TABLE `pagos_transac_anexo_8` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo_8_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `fecha_reg`;
ALTER TABLE  anexo_8_aviso DROP FOREIGN KEY perfila_fk_anexo8_aviso;

ALTER TABLE `pagos_transac_anexo_8` ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo1` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `otro`;
ALTER TABLE `pagos_transac_anexo1_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `otro`;
ALTER TABLE `pagos_transac_anexo1` ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;

ALTER TABLE `pagos_transac_anexo2a` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo2a_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo2a` ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo2b` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo2b_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo2c` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo2c_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo3` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo3_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo4` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo4_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
/***** hasta cá ya en sicoi ****** */

ALTER TABLE `pagos_transac_anexo5a` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo5a_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `anexo5b` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `monto_recibido`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `anexo5b_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_creacion`;
ALTER TABLE `pagos_transac_anexo6_pago` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo6_pago_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo_7` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo7_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
/* hasta acá en sicoi */
ALTER TABLE `pagos_transac_anexo9` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo9_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo10` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `EDPOA`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo10_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `EDPOA`;
ALTER TABLE `anexo12a` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `activo`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `anexo12a_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_creacion`;
ALTER TABLE `pagos_transac_anexo13_num` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo13_num_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`;
ALTER TABLE `pagos_transac_anexo_15` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `status`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `pagos_transac_anexo_15_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `status`;
/* hasta aca sicoi  */
ALTER TABLE anexo_15 DROP FOREIGN KEY perfila_fk_anexo15;
ALTER TABLE `completar_anexo16` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `Activo`, ADD `id_pago_ayuda` INT NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`;
ALTER TABLE `completar_anexo16_aviso` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `Activo`;
/* hasta aca sicoi  */
ALTER TABLE `pagos_transac_anexo1` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `activo`;
ALTER TABLE `pagos_transac_anexo_8` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `status`;
ALTER TABLE `pagos_transac_anexo2a` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo2b` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo2c` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo3` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`
ALTER TABLE `pagos_transac_anexo4` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo5a` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `activo`;
/* hasta aca sicoi  */
ALTER TABLE `anexo5b` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `anexo12a` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo6_pago` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `activo`;
ALTER TABLE `pagos_transac_anexo_7` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `activo`;
ALTER TABLE `pagos_transac_anexo9` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo10` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `anexo12a` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo13_num` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo_15` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `completar_anexo16` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
/* hasta aca sicoi  */
ALTER TABLE `pagos_transac_anexo_8_aviso` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `status`;
ALTER TABLE `pagos_transac_anexo1_aviso` CHANGE `monto_operacion` `monto_operacion` DECIMAL(17,2) NOT NULL;

ALTER TABLE `tipo_cliente_p_f_m` ADD `otra_ocupa` VARCHAR(100) NOT NULL AFTER `activida_o_giro`;
ALTER TABLE `tipo_cliente_p_f_e` ADD `otra_ocupa` VARCHAR(100) NOT NULL AFTER `actividad_o_giro`;

ALTER TABLE `cliente_fisica` ADD `actividad_o_giro` VARCHAR(15) NOT NULL AFTER `rfc`, ADD `otra_ocupa` VARCHAR(100) NOT NULL AFTER `actividad_o_giro`;
ALTER TABLE `cliente_moral` ADD `actividad_o_giro` VARCHAR(15) NOT NULL AFTER `r_c_f`, ADD `otra_ocupa` VARCHAR(100) NOT NULL AFTER `actividad_o_giro`;
ALTER TABLE `cliente_fideicomiso` ADD `actividad_o_giro` VARCHAR(15) NOT NULL AFTER `rfc_fideicomiso`, ADD `otra_ocupa` VARCHAR(100) NOT NULL AFTER `actividad_o_giro`;
ALTER TABLE `tipo_cliente_p_f_m` ADD `fecha_nac_apodera` DATE NOT NULL AFTER `numero_identificacion_p`, ADD `rfc_apodera` VARCHAR(55) NOT NULL AFTER `fecha_nac_apodera`, ADD `curp_apodera` VARCHAR(55) NOT NULL AFTER `rfc_apodera`;
ALTER TABLE `anexo11_actividad1` ADD `valor_referencia1_1` DATE NOT NULL AFTER `fecha_contrato1`;
ALTER TABLE `anexo11_actividad1_aviso` ADD `valor_referencia1_1` DATE NOT NULL AFTER `fecha_contrato1`;
ALTER TABLE `anexo11_actividad3` ADD `clave_presta` VARCHAR(15) NOT NULL AFTER `descripcion3`, ADD `descrip_otra_presta` VARCHAR(100) NOT NULL AFTER `clave_presta`, ADD `clave_activo` VARCHAR(15) NOT NULL AFTER `descrip_otra_presta`;
ALTER TABLE `anexo11_actividad3` ADD `descrip_otro_activo` VARCHAR(100) NOT NULL AFTER `clave_activo`;
ALTER TABLE `anexo11_actividad3_aviso` ADD `clave_presta` VARCHAR(15) NOT NULL AFTER `descripcion3`, ADD `descrip_otra_presta` VARCHAR(100) NOT NULL AFTER `clave_presta`, ADD `clave_activo` VARCHAR(15) NOT NULL AFTER `descrip_otra_presta`;
ALTER TABLE `anexo11_actividad3_aviso` ADD `descrip_otro_activo` VARCHAR(100) NOT NULL AFTER `clave_activo`;
ALTER TABLE `anexo11_actividad3` ADD `num_emplea` VARCHAR(4) NOT NULL AFTER `descrip_otro_activo`, ADD `num_opera` VARCHAR(4) NOT NULL AFTER `num_emplea`;
ALTER TABLE `anexo11_actividad3_aviso` ADD `num_emplea` VARCHAR(4) NOT NULL AFTER `descrip_otro_activo`, ADD `num_opera` VARCHAR(4) NOT NULL AFTER `num_emplea`;
ALTER TABLE `anexo11_actividad1` CHANGE `valor_referencia1_1` `valor_referencia1_1` DECIMAL(16,2) NOT NULL;
ALTER TABLE `anexo11_actividad1_aviso` CHANGE `valor_referencia1_1` `valor_referencia1_1` DECIMAL(16,2) NOT NULL;
ALTER TABLE `anexo11_actividad10` ADD `activo_virtual` VARCHAR(15) NOT NULL AFTER `monto_operacion10`, ADD `nombre_otro_activo` VARCHAR(100) NOT NULL AFTER `activo_virtual`, ADD `cant_activo` DECIMAL(14,2) NOT NULL AFTER `nombre_otro_activo`;
ALTER TABLE `anexo11_actividad10_aviso` ADD `activo_virtual` VARCHAR(15) NOT NULL AFTER `monto_operacion10`, ADD `nombre_otro_activo` VARCHAR(100) NOT NULL AFTER `activo_virtual`, ADD `cant_activo` DECIMAL(14,2) NOT NULL AFTER `nombre_otro_activo`;

=====================================================================================================
ALTER TABLE `paises_lugares_frontera` ADD `id_estado` INT(2) NOT NULL DEFAULT '0' AFTER `ciudad`;
ALTER TABLE `paises_indice_paz` ADD `id_estado` INT NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `paises_incidencia_delictiva` ADD `id_estado` INT NOT NULL DEFAULT '0' AFTER `ciudad`;
ALTER TABLE `paises_percepcion_corrupcion_estatal_zonas_urbanas` ADD `id_estado` INT NOT NULL DEFAULT '0' AFTER `ciudad`;


ALTER TABLE `pais` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);

ALTER TABLE `paises_guia_terrorismo` ADD `id_pais` INT NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `paises_indice_corrupcion_global` ADD `id_pais` INT NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `paises_indice_corrupcion` ADD `id_pais` INT NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `paises_indice_gafi` ADD `id_pais` INT NOT NULL DEFAULT '0' AFTER `id`;

/*add tabla areas_servicio
add tabla activo_administrado
*/

ALTER TABLE `cuestionario_ampliado` ADD `id_perfilamiento` BIGINT NOT NULL AFTER `id_operacion`;
ALTER TABLE `cuestionario_ampliado_pep` ADD `id_perfilamiento` BIGINT NOT NULL AFTER `id_actividad`;
ALTER TABLE diagnostico_alertasADD `id_perfilamiento` BIGINT NOT NULL AFTER `id_operacion`;

-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-06-2021 a las 21:48:03
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `yimexico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo1`
--

DROP TABLE IF EXISTS `alertas_anexo1`;
CREATE TABLE IF NOT EXISTS `alertas_anexo1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo1`
--

INSERT INTO `alertas_anexo1` (`id`, `clave`, `indicador`) VALUES
(1, '2101', 'El cliente o usuario introduce fondos en las máquinas de juego y de inmediato pide la devolución de los mismos en forma de créditos.'),
(2, '2102', 'El cliente o usuario solicita el pago del crédito de una máquina de juego sin haber ganado un premio.'),
(3, '2103', 'El cliente o usuario solicita los pagos del crédito de diversas máquinas por un monto elevado.'),
(4, '2104', 'Se observa un cambio drástico en el patrón de gasto o apuesta del cliente o usuario.'),
(5, '2105', 'El cliente o usuario realiza apuestas en juegos en los que cubre ambos lados de la apuesta de manera recurrente.'),
(6, '2106', 'El cliente o usuario compra fichas o crédito para jugar y solicita el reintegro después de haber tenido poca actividad de juego.'),
(7, '2107', 'El cliente o usuario realiza múltiples retiros de crédito o solicitudes de reintegro de fichas en el mismo día.'),
(8, '2108', 'El cliente o usuario realiza la compra de crédito o fichas en efectivo y solicita el reintegro por medio de un cheque o transferencia.'),
(9, '2109', 'Varios clientes o usuarios solicitan el pago del crédito obtenido por medio de una transferencia o cheque a nombre de un mismo sujeto.'),
(10, '2110', 'El cliente o usuario solicita el pago del crédito o premio por medio de varios cheques al portador.\r\n'),
(11, '2111', 'El cliente o usuario realiza operaciones frecuentes por debajo del umbral de identificación o aviso.'),
(12, '2112', 'El cliente o usuario solicita que el pago de un premio o fichas sea realizado por medio de una combinación de efectivo, cheques o transferencias.'),
(13, '2113', 'El cliente o usuario frecuentemente realiza reclamaciones de premios de alto valor ganados por sorteos.'),
(14, '2114', 'El cliente o usuario solicita el pago de premios o fichas sin que se tenga registro de que dicho cliente o usuario haya comprado billetes de lotería, crédito o fichas para jugar.'),
(15, '2115', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(16, '2116', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(17, '2117', 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente o usuario.'),
(18, '2118', 'Hay indicios, o certeza, que el cliente o usuario no está actuando en nombre propio y está tratando de ocultar la identidad del cliente o usuario real.'),
(19, '2119', 'Uso de divisas en efectivo sin justificación alguna.'),
(20, '2120', 'El cliente o usuario liquida la operación por medio de una transferencia proveniente de un país extranjero.'),
(21, '2121', 'El cliente o usuario insiste en liquidar o pagar la operación en efectivo rebasando el umbral permitido en la Ley.'),
(22, '2122', 'El cliente o usuario intenta sobornar, extorsionar o amenaza con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(23, '2123', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(24, '2124', 'El cliente o usuario no quiere ser relacionado con la operación realizada.'),
(25, '2125', 'El cliente o usuario o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente.'),
(26, '2126', 'El cliente o usuario solicita que el pago de los premios sea realizado a la cuenta bancaria de una tercera persona.'),
(27, '2127', 'La operación la paga un tercero sin relación aparente con el cliente o usuario.\r\n'),
(28, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo2a`
--

DROP TABLE IF EXISTS `alertas_anexo2a`;
CREATE TABLE IF NOT EXISTS `alertas_anexo2a` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo2a`
--

INSERT INTO `alertas_anexo2a` (`id`, `clave`, `indicador`) VALUES
(1, '2201', 'El cliente realiza operaciones que salen de su perfil transaccional.'),
(2, '2202', 'El cliente recurrentemente realiza el pago del saldo de la tarjeta en efectivo por cantidades elevadas por medio de canales no bancarios.'),
(3, '2203', 'La tarjeta registra la mayoría de sus operaciones en lugares distantes de donde se tiene domiciliada sin razón aparente.'),
(4, '2204', 'El cliente mantiene de manera regular saldo a favor en su cuenta sin justificación aparente.'),
(5, '2205', 'El cliente realiza una compra por un valor significativamente elevado, pagando en la fecha de corte dicha operación en efectivo.'),
(6, '2206', 'El cliente se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(7, '2207', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona r'),
(8, '2208', 'El monto de las operaciones realizadas con la tarjeta no es acorde con la actividad económica o giro mercantil declarado por el cliente.'),
(9, '2209', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente real.'),
(10, '2210', 'El cliente intenta sobornar, extorsionar o amenaza al promotor con el fin de que se otorgue la tarjeta fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(11, '2211', 'La información y documentación presentada por el cliente es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(12, '2212', 'La  tarjeta es pagada por un tercero sin relación aparente con el cliente.'),
(13, '2213', 'La tarjeta se liquida por medio de una transferencia proveniente de un país extranjero.'),
(14, '2214', 'El cliente insiste liquidar el saldo de la tarjeta en efectivo en divisas diferentes a la moneda nacional, a pesar de indicarle que esto no está permitido.'),
(15, '2215', 'El cliente solicita varias tarjetas con características similares sin que exista justificación para ello.'),
(16, '2216', 'Otorgamiento de múltiples tarjetas adicionales o suplementarias sin causa justificada.'),
(17, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo2b`
--

DROP TABLE IF EXISTS `alertas_anexo2b`;
CREATE TABLE IF NOT EXISTS `alertas_anexo2b` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo2b`
--

INSERT INTO `alertas_anexo2b` (`id`, `clave`, `indicador`) VALUES
(1, '2301', 'El cliente o usuario realiza operaciones de carga en diferentes tarjetas de forma periódica rebasando el umbral de aviso.'),
(2, '2302', 'El cliente o usuario realiza la operación de carga o recarga por montos elevados liquidando en efectivo'),
(3, '2303', 'Se observa que el cliente o usuario realiza operaciones de carga o recarga por montos por arriba del umbral de identificación utilizando diversas tarjetas'),
(4, '2304', 'El cliente o usuario realiza diversas operaciones de carga o recarga por montos elevados en un periodo corto de tiempo.'),
(5, '2305', 'Se observa que diferentes clientes o usuarios realizan operaciones de carga o recarga por arriba del umbral de identificación en la misma tarjeta.'),
(6, '2306', 'Se observa que se realizan operaciones de carga o recarga en diferentes localidades, estados o jurisdicciones por arriba del umbral de identificación en la misma tarjeta'),
(7, '2307', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(8, '2308', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(9, '2309', 'El monto de carga o recarga no es acorde con la actividad económica o giro mercantil declarado por el cliente o usuario.'),
(10, '2310', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real'),
(11, '2311', 'Uso de divisas en efectivo sin justificación alguna.'),
(12, '2312', 'El cliente o usuario intenta sobornar, extorsionar o amenaza con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(13, '2313', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(14, '2314', 'El cliente o usuario no quiere ser relacionado con la operación realizada.'),
(15, '2315', 'La operación la paga un tercero sin relación aparente con el cliente o usuario.'),
(16, '2316', 'El cliente o usuario liquida la operación por medio de una transferencia proveniente de un país extranjero.\r\n'),
(17, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo2c`
--

DROP TABLE IF EXISTS `alertas_anexo2c`;
CREATE TABLE IF NOT EXISTS `alertas_anexo2c` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo2c`
--

INSERT INTO `alertas_anexo2c` (`id`, `clave`, `indicador`) VALUES
(1, '3701', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '3702', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(3, '3703', 'El monto disponible en la tarjeta no es acorde con la actividad económica o giro mercantil declarado por el cliente o usuario.'),
(4, '3704', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real.'),
(5, '3705', 'El cliente o usuario intenta sobornar, extorsionar o amenaza al vendedor con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(6, '3706', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(7, '3707', 'El cliente o usuario realiza una o más compras por un valor significativamente elevado sin lógica aparente, con el objeto de retirar efectivo con las recompensas ganadas.'),
(8, '3708', 'El cliente o usuario realiza una o más compras por un valor significativamente elevado, con el objeto de obtener la devolución del monto pagado.'),
(9, '3709', 'Uso de divisas en efectivo para la adquisición de un bien con el único propósito de obtener la devolución en moneda nacional en un período corto de tiempo.'),
(10, '3710', 'El cliente o usuario no quiere ser relacionado con la operación realizada.'),
(11, '3711', 'El cliente o usuario o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente.'),
(12, '3712', 'La operación la realiza un tercero sin relación aparente con el cliente o usuario.'),
(13, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo3`
--

DROP TABLE IF EXISTS `alertas_anexo3`;
CREATE TABLE IF NOT EXISTS `alertas_anexo3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo3`
--

INSERT INTO `alertas_anexo3` (`id`, `clave`, `indicador`) VALUES
(1, '301', 'El cliente o usuario realiza operaciones de compra o venta de cheques de viajero de forma periódica sin razón aparente'),
(2, '302', 'El cliente o usuario realiza la operación de compra o venta de cheques de viajero por monto elevados llevando a cabo su liquidando en efectivo'),
(3, '303', 'El cliente o usuario realiza diversas operación de compra o venta de cheques de viajero por montos elevados en un periodo corto de tiempo sin justificación aparente'),
(4, '304', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen'),
(5, '305', 'Se conoce un historial criminal del cliente o usuario, de algún familiar directo o persona relacionada con él'),
(6, '306', 'De acuerdo con la ocupación del cliente o usuario, el monto de compra o venta de cheques de viajero parece estar fuera de su alcance'),
(7, '307', 'De acuerdo con los ingresos declarados por el cliente o usuario,  el monto de compra o venta de cheques de viajero parece estar fuera de su alcance'),
(8, '308', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real'),
(9, '309', 'Uso de divisas en efectivo en montos elevados o de poco uso sin que la ocupación del cliente o usuario lo justifique'),
(10, '310', 'El cliente o usuario intenta sobornar o extorsionar al vendedor con  el fin de realizar la operación de forma irregular'),
(11, '311', 'El cliente o usuario solicita condiciones especiales poco usuales en la realización de la operación'),
(12, '312', 'El cliente o usuario proporcionó datos falsos  o documentos apócrifos al realizar la operación'),
(13, '313', 'El cliente o usuario no quiere ser relacionado con la operación realizada'),
(14, '314', 'El cliente o usuario muestra fuerte interés en la realización de la transacción con rapidez, sin que exista causa justificada'),
(15, '315', 'La operación se liquida por medio de una transferencia internacional sin que la ocupación, perfil o nacionalidad del cliente o usuario lo justifique'),
(16, '316', 'La operación se liquida por medio de una transferencia internacional proveniente de un país considerado como paraíso fiscal o de alto riesgo'),
(17, '317', 'Operaciones con organizaciones sin fines de lucro, cuando las características de la transacción no coinciden con los objetivos de la entidad'),
(18, '9999', 'Otra alerta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo4`
--

DROP TABLE IF EXISTS `alertas_anexo4`;
CREATE TABLE IF NOT EXISTS `alertas_anexo4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo4`
--

INSERT INTO `alertas_anexo4` (`id`, `clave`, `indicador`) VALUES
(1, '2801', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '2802', 'El cliente o usuario realiza varias operaciones en un periodo corto de tiempo en las que se desconoce el origen de los objetos empeñados.'),
(3, '2803', 'La operación de mutuo o crédito se lleva a cabo por medio de una garantía poco usual o que no corresponde con la actividad o ingresos del cliente o usuario.'),
(4, '2804', 'Hay indicios, o certeza, de que los bienes empeñados son robados o provienen de una actividad ilícita.'),
(5, '2805', 'El cliente o usuario o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente.'),
(6, '2806', 'El cliente o usuario realiza operaciones de manera periódica en las que se liquida el total del monto del préstamo otorgado en efectivo al poco tiempo de haberlo adquirido.'),
(7, '2807', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(8, '2808', 'El cliente o usuario no quiere ser relacionado con la operación realizada.'),
(9, '2809', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real.'),
(10, '2810', 'El cliente o usuario intenta sobornar, extorsionar o amenaza al vendedor con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(11, '2811', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(12, '2812', 'Hay indicios o certeza de que los recursos solicitados se están utilizando para fines distintos a los declarados por el cliente o usuario.'),
(13, '2813', 'El cliente o usuario registra domicilios distintos cada vez que solicita un préstamo o crédito.'),
(14, '2814', 'El cliente pretende liquidar la operación con monedas virtuales.'),
(15, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo5a`
--

DROP TABLE IF EXISTS `alertas_anexo5a`;
CREATE TABLE IF NOT EXISTS `alertas_anexo5a` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(265) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo5a`
--

INSERT INTO `alertas_anexo5a` (`id`, `clave`, `indicador`) VALUES
(1, '3101', 'El cliente se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '3102', 'El pago por el inmueble es realizado por un tercero sin relación aparente con el cliente.'),
(3, '3103', 'El cliente o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente.'),
(4, '3104', 'Al cliente parece no importarle pagar precios superiores a los del mercado con la finalidad de que la operación se realice fuera de los parámetros establecidos.'),
(5, '3105', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(6, '3106', 'El cliente no quiere ser relacionado con la compra del inmueble.'),
(7, '3107', 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente.'),
(8, '3108', 'El cliente solicita que se realice la operación por medio de un contrato privado, donde no hay intención de registrarlo ante notario, o cuando esta intención se expresa, no se lleva a cabo finalmente.'),
(9, '3109', 'Transacciones sucesivas de compra y venta de la misma propiedad en un periodo corto de tiempo, con cambios injustificados del valor de la misma'),
(10, '3110', 'La operación se lleva acabo a un valor de venta o compra significativamente diferente (mucho mayor o mucho menor) a partir del valor real de la propiedad o a los valores de mercado.'),
(11, '3111', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente real.'),
(12, '3112', 'Uso de divisas en efectivo sin justificación alguna.'),
(13, '3113', 'El cliente liquida la operación por medio de una transferencia proveniente de un país extranjero.'),
(14, '3114', 'El cliente insiste en liquidar o pagar la operación en efectivo rebasando el umbral permitido en la Ley.'),
(15, '3115', 'El cliente intenta sobornar, extorsionar o amenaza al vendedor con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(16, '3116', 'La información y documentación presentada por el cliente es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(17, '3117', 'Se tiene sospecha de que algunos intermediarios dentro del sistema financiero están coludidos con el cliente.'),
(18, '3118', 'El cliente es un Fideicomiso y se niega a proporcionar información sobre el beneficiario controlador.'),
(19, '3119', 'El cliente opera en grupos sin que exista algún parentesco o relación entre ellos.'),
(20, '3120', 'El cliente pretende pagar o liquidar la operación con monedas virtuales.'),
(21, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo5b`
--

DROP TABLE IF EXISTS `alertas_anexo5b`;
CREATE TABLE IF NOT EXISTS `alertas_anexo5b` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo5b`
--

INSERT INTO `alertas_anexo5b` (`id`, `clave`, `indicador`) VALUES
(1, '3901', 'La(s) persona(s) que participa(n) en la operación se rehúsa(n) a proporcionar documentos personales que lo(s) identifiquen.'),
(2, '3902', 'El pago en preventa por el inmueble es realizado por un tercero sin relación aparente con el cliente.'),
(3, '3903', 'La(s) persona(s) que participa(n) en la operación realiza(n) múltiples aportaciones en un periodo muy corto de tiempo sin razón aparente.'),
(4, '3904', 'El cliente de la preventa no muestra tener interés en las características de la propiedad objeto de la operación, o en el precio y condiciones de la transacción.'),
(5, '3905', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(6, '3906', 'La(s) persona(s) que participa(n) en la operación no quiere(n) ser relacionada(s) con el desarrollo del inmueble.'),
(7, '3907', 'La operación no es acorde con la actividad económica o giro mercantil declarado por la(s) persona(s) que participa(n) en la operación.'),
(8, '3908', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad de quien aporta los recursos.'),
(9, '3909', 'Uso de divisas en efectivo sin justificación alguna.'),
(10, '3910', 'Se conoce que las aportaciones se realizan por medio de transferencia(s) proveniente(s) de un país extranjero.'),
(11, '3911', 'La(s) persona(s) que participa(n) en la operación insiste(n) en realizar sus aportaciones en efectivo por montos elevados.'),
(12, '3912', 'La información y documentación presentada por la(s) persona(s) que participa(n) en la operación es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(13, '3913', 'La(s) persona(s) que participa(n) en la operación intenta(n) sobornar, extorsionar o amenaza(n) con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(14, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo6`
--

DROP TABLE IF EXISTS `alertas_anexo6`;
CREATE TABLE IF NOT EXISTS `alertas_anexo6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo6`
--

INSERT INTO `alertas_anexo6` (`id`, `clave`, `indicador`) VALUES
(1, '2901', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '2902', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(3, '2903', 'El cliente o usuario compra o vende grandes cantidades de metales preciosos, piedras preciosas, joyas y/o relojes sin justificar su procedencia.'),
(4, '2904', 'El cliente o usuario o personas relacionadas con él realizan diversas operaciones de compra o venta de grandes cantidades de metales preciosas, piedras preciosas, joyas y/o relojes en un periodo muy corto de tiempo sin justificación aparente.'),
(5, '2905', 'El cliente o usuario realiza compras indiscriminadas de mercancía (sin importar tamaño, color, precio) de metales preciosos, piedras preciosas, joyas y/o relojes, sin que estén asociadas a su actividad.'),
(6, '2906', 'El cliente o usuario no quiere ser relacionado con la operación realizada.'),
(7, '2907', 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente o usuario.'),
(8, '2908', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real.'),
(9, '2909', 'Uso de divisas en efectivo sin justificación alguna.'),
(10, '2910', 'El cliente o usuario liquida la operación por medio de una transferencia proveniente de un país extranjero.'),
(11, '2911', 'El cliente o usuario insiste en liquidar o pagar la operación en efectivo rebasando el umbral permitido en la Ley.'),
(12, '2912', 'El cliente o usuario intenta sobornar, extorsionar o amenaza al vendedor con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(13, '2913', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(14, '2914', 'Se conoce que el cliente o usuario recolecta metales preciosos (ej. oro) y después lo funde con la intención de venderlo con una calidad diferente a la declarada.'),
(15, '2915', 'Exporta e importa joyería, relojes, metales y piedras preciosas de países riesgosos sin que haya justificación aparente.'),
(16, '2916', 'El cliente o usuario vende pedacería de metales preciosos que podría provenir de actividades ilícitas como el robo.'),
(17, '2917', 'El cliente o usuario compra pedacería de metales preciosos por cantidades bajas a precios que exceden los de mercado o viceversa.'),
(18, '2918', 'El cliente o usuario liquida la mayoría de sus operaciones con otras divisas, sin que su actividad lo justifique.'),
(19, '2919', 'La operación se liquida a través de cheques de caja con la intención de ocultar el origen de los recursos.'),
(20, '2920', 'Para liquidar sus operaciones el cliente o usuario utiliza recursos de diferentes cuentas, instituciones financieras, denominaciones o con diversos instrumentos monetarios, con la intención de dificultar el rastreo de los recursos.'),
(21, '2921', 'El cliente o usuario opera en grupos, sin que exista algún parentesco entre ellos.'),
(22, '2922', 'El cliente o usuario solicita que el pago le sea depositado en diferentes cuentas sin razón aparente.'),
(23, '2923', 'El cliente o usuario hace uso de servicios de paquetería para entregar la mercancía vendida.'),
(24, '2924', 'Al cliente o usuario parece no importarle pagar precios superiores a los del mercado con la finalidad de que la operación se realice fuera de los parámetros establecidos.'),
(25, '2925', 'La operación la paga un tercero sin relación aparente con el cliente o usuario.'),
(26, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo7`
--

DROP TABLE IF EXISTS `alertas_anexo7`;
CREATE TABLE IF NOT EXISTS `alertas_anexo7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo7`
--

INSERT INTO `alertas_anexo7` (`id`, `clave`, `indicador`) VALUES
(1, '3301', 'El cliente se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '3302', 'El cliente no muestra tener interés en las características de la obra, el autor de la misma o en el precio y condiciones de la transacción.'),
(3, '3303', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(4, '3304', 'El cliente no quiere ser relacionado con la compra de la obra.'),
(5, '3305', 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente.'),
(6, '3306', 'Transacciones sucesivas de compra y venta de la misma obra en un periodo corto de tiempo, con cambios injustificados del valor de la misma.'),
(7, '3307', 'La operación se lleva acabo a un valor de venta o compra significativamente diferente (mucho mayor o mucho menor) a partir del precio estimado de la obra o a los valores de mercado.'),
(8, '3308', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente real.'),
(9, '3309', 'El cliente realiza múltiples compras liquidándolas en efectivo por montos elevados sin justificación aparente.'),
(10, '3310', 'Uso de divisas en efectivo sin justificación alguna.'),
(11, '3311', 'El cliente  liquida la operación por medio de una transferencia proveniente de un país extranjero.'),
(12, '3312', 'La información y documentación presentada por el cliente es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(13, '3313', 'El cliente insiste en liquidar o pagar la operación en efectivo rebasando el umbral permitido en la Ley.'),
(14, '3314', 'El cliente intenta sobornar, extorsionar o amenaza con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(15, '3315', 'El cliente o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente.'),
(16, '3316', 'La operación la paga un tercero sin relación aparente con el cliente.'),
(17, '3317', 'El agente que vende la obra a la casa de subastas se niega a proporcionar el nombre del propietario real al que representa.'),
(18, '3318', 'El cliente solicita que la factura refleje un precio mucho menor del pactado, y el resto liquidarlo en efectivo.'),
(19, '3319', '3319	El cliente pretende liquidar la operación con monedas virtuales.'),
(20, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo8`
--

DROP TABLE IF EXISTS `alertas_anexo8`;
CREATE TABLE IF NOT EXISTS `alertas_anexo8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo8`
--

INSERT INTO `alertas_anexo8` (`id`, `clave`, `indicador`) VALUES
(1, '2501', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '2502', 'La operación es pagada, en parte o en su totalidad, por uno o más terceros sin relación aparente con el cliente o usuario.'),
(3, '2503', 'El cliente o usuario solicita el reembolso del pago del vehículo poco tiempo después de ser adquirido.'),
(4, '2504', 'El cliente o usuario compra múltiples vehículos en un periodo muy corto de tiempo, sin tener la preocupación sobre el costo, condiciones o tipo de veh'),
(5, '2505', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona r'),
(6, '2506', 'El cliente o usuario no quiere ser relacionado con la compra del vehículo.'),
(7, '2507', 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente o usuario.'),
(8, '2508', 'El cliente o usuario vende su vehículo a precios muy por debajo del precio de mercado (aplica para Sujetos Obligados que se dedican a la compra de aut'),
(9, '2509', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real.'),
(10, '2510', 'Uso de divisas en efectivo sin justificación alguna.'),
(11, '2511', 'El cliente o usuario liquida la operación  por medio de una transferencia proveniente de un país extranjero.'),
(12, '2512', 'El cliente o usuario insiste en liquidar/pagar la operación en efectivo rebasando el umbral permitido en la Ley.'),
(13, '2513', 'El cliente o usuario intenta sobornar, extorsionar o amenaza con el fin de realizar la operación fuera de los parámetros establecidos, o con la finali'),
(14, '2514', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(15, '2515', 'Al cliente o usuario parece no importarle pagar precios superiores a los del mercado con la finalidad de que la operación se realice fuera de los pará'),
(16, '2516', 'El cliente o usuario o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto de tiempo sin razón aparente.'),
(17, '2517', 'El cliente o usuario registra el mismo domicilio que otros clientes sin que exista  relación aparente entre ellos.'),
(18, '2518', 'El cliente o usuario es menor de edad y no cuenta con la capacidad de decisión ni la documentación necesaria para realizar la operación.'),
(19, '2519', 'Hay indicios o certeza  de que los vehículos adquiridos son para exportación.'),
(20, '2520', 'El cliente o usuario solicita que la operación se realice en un lugar distinto al establecimiento sin que exista causa justificada.'),
(21, '2521', 'El cliente o usuario pretende liquidar la operación con monedas virtuales.'),
(22, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo9`
--

DROP TABLE IF EXISTS `alertas_anexo9`;
CREATE TABLE IF NOT EXISTS `alertas_anexo9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo9`
--

INSERT INTO `alertas_anexo9` (`id`, `clave`, `indicador`) VALUES
(1, '2601', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '2602', 'El servicio otorgado es pagado por un tercero sin relación aparente con el cliente o usuario.'),
(3, '2603', 'El cliente o usuario solicita múltiples servicios en diversos vehículos y/o inmuebles en un periodo muy corto de tiempo sin que su actividad lo justifique.'),
(4, '2604', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(5, '2605', 'Al cliente o usuario parece no importarle pagar precios superiores a los del mercado con la finalidad de que la operación se realice fuera de los parámetros establecidos.'),
(6, '2606', '2606	El cliente o usuario no quiere ser relacionado con la contratación del servicio.'),
(7, '2607', 'El servicio de blindaje no es acorde con la actividad económica o giro mercantil declarado por el cliente o usuario.'),
(8, '2608', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real.'),
(9, '2609', 'Uso de divisas en efectivo sin justificación alguna.'),
(10, '2610', '2610	El cliente o usuario liquida la operación por medio de una transferencia proveniente de un país extranjero.'),
(11, '2611', 'El cliente o usuario insiste en liquidar o pagar la operación en efectivo rebasando el umbral permitido en la Ley.'),
(12, '2612', 'El cliente o usuario intenta sobornar, extorsionar o amenaza al vendedor  con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(13, '2613', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(14, '2614', 'El cliente o usuario contrata el servicio vía remota (ej. teléfono/internet) y da justificaciones para evitar ir a la agencia.'),
(15, '2615', 'El cliente o usuario solicita el blindaje de bienes muebles o partes de inmuebles poco comunes o de poco valor.'),
(16, '2616', 'El nombre del solicitante del servicio es diferente al nombre del dueño real del bien a blindarse.'),
(17, '2617', 'El cliente o usuario pretende liquidar o pagar la operación con monedas virtuales.'),
(18, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo10`
--

DROP TABLE IF EXISTS `alertas_anexo10`;
CREATE TABLE IF NOT EXISTS `alertas_anexo10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo10`
--

INSERT INTO `alertas_anexo10` (`id`, `clave`, `indicador`) VALUES
(1, '3201', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, '3202', 'El servicio otorgado es pagado por un tercero sin relación aparente con el cliente o usuario.'),
(3, '3203', 'Custodia o traslado de bienes poco comunes o que no coinciden con la actividad económica o giro mercantil del cliente o usuario.'),
(4, '3204', 'El cliente o usuario se rehúsa a revelar cuáles son los bienes transportados o custodiados sin razón aparente'),
(5, '3205', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(6, '3206', 'El cliente o usuario parece no importarle pagar precios superiores a los del mercado con el fin de que el servicio se realice fuera de los parámetros establecidos.'),
(7, '3207', 'El cliente o usuario no quiere ser relacionado con la contratación del servicio.'),
(8, '3208', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real.'),
(9, '3209', 'El cliente o usuario liquida la operación por medio de una transferencia proveniente de un país extranjero, cuando no existe justificación para ello.'),
(10, '3210', 'El cliente o usuario intenta sobornar, extorsionar o amenaza con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(11, '3211', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(12, '3212', '3212	El cliente o usuario solicita, en su mayoría y de forma continua, traslado de metales preciosos sin que su actividad lo justifique'),
(13, '3213', '3213	El cliente o usuario o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente.'),
(14, '3214', 'Traslado habitual de valores a nombre de diferentes personas en el mismo domicilio de entrega, sin causa que lo justifique'),
(15, '3215', 'Clientes o usuarios personas morales que solicitan traslado de efectivo que sean recogidos o entregados en casas habitación u oficinas virtuales.'),
(16, '3216', 'El monto trasladado excede significativamente el promedio de los servicios solicitados por el cliente con anterioridad.'),
(17, '3217', 'El cliente evita que el domicilio exacto de recepción o entrega de los valores quede registrado, proporcionando la información por otros medios (teléfono, correo electrónico, etc.).'),
(18, '3218', 'El monto del numerario o valores trasladados no corresponde a la magnitud del negocio o la actividad declarada del cliente.'),
(19, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo11`
--

DROP TABLE IF EXISTS `alertas_anexo11`;
CREATE TABLE IF NOT EXISTS `alertas_anexo11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` int(5) NOT NULL,
  `indicador` varchar(155) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo11`
--

INSERT INTO `alertas_anexo11` (`id`, `clave`, `indicador`) VALUES
(1, 3401, 'El cliente se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(2, 3402, 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente.'),
(3, 3403, 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente real.'),
(4, 3404, 'La información y documentación presentada por el cliente es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(5, 3405, 'El cliente intenta sobornar, extorsionar o amenaza con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar e'),
(6, 3406, 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relaci'),
(7, 3407, 'El cliente hace uso de un intermediario para realizar operaciones no acordes con la actividad económica o giro mercantil del cliente.'),
(8, 3408, 'El cliente no quiere ser relacionado con la operación realizada.'),
(9, 3409, 'El cliente o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente.'),
(10, 3410, 'El cliente solicita que los honorarios del servicio sean pagados por un tercero que no participó en la operación.'),
(11, 3411, 'El cliente se rehúsa  a proporcionar información sobre la identidad del beneficiario final.'),
(12, 3412, 'El cliente insiste en contratar los servicios de un intermediario en todas sus transacciones, evitando el trato personal, sin causa que lo justifique.'),
(13, 3413, 'Cliente extranjero aparentemente no cuenta con transacciones/negocios significativos en el país y solicita el apoyo de servicios profesionales.'),
(14, 3414, 'El cliente es el firmante de las cuentas de la persona moral sin que exista causa que justifique el servicio profesional.'),
(15, 3415, 'Hay indicios de que posterior a la constitución de la empresa, hubo un periodo largo de inactividad seguido de un aumento repentino e inexplicable de las a'),
(16, 3416, 'El cliente es una persona moral que se describe como un negocio comercial, sin embargo no es posible corroborar esta información en fuentes abiertas.'),
(17, 3417, 'El cliente (persona moral) está registrado con un nombre que indica actividades o servicios distintos a los que realiza.'),
(18, 3418, 'La dirección proporcionada por el cliente es la misma que han indicado otros clientes (personas físicas y morales), sin que formen parte de un mismo grupo '),
(19, 3419, 'Se tiene una cantidad inusualmente grande de beneficiarios y participantes mayoritarios.'),
(20, 3420, 'Se sospecha que la persona moral que ha sido constituida/incorporada plantea un alto riesgo de lavado de dinero o de financiamiento del terrorismo.'),
(21, 3421, 'La administración de recursos del cliente permite identificar que usa múltiples cuentas bancarias sin justificación aparente.'),
(22, 3422, 'El cliente no muestra interés en la estructura de la(s) empresa(s) que se está(n) constituyendo.'),
(23, 3423, 'Se tienen indicios de que el cliente involucra a múltiples profesionales para facilitar (o relacionar) aspectos de una transacción sin una razón que lo jus'),
(24, 3424, 'La operación involucra a dos personas morales con directores, accionistas o beneficiarios finales similares o idénticos.'),
(25, 3425, 'El cliente (personas físicas y  morales), declara una ocupación o actividad distinta a la que aparece en su Cédula de Identificación Fiscal.'),
(26, 9999, 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo12a`
--

DROP TABLE IF EXISTS `alertas_anexo12a`;
CREATE TABLE IF NOT EXISTS `alertas_anexo12a` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo12a`
--

INSERT INTO `alertas_anexo12a` (`id`, `clave`, `indicador`) VALUES
(1, '3501', 'Los sujetos o partes involucradas se rehúsan a proporcionar documentos personales que los identifiquen.'),
(2, '3502', 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente.'),
(3, '3503', 'Hay indicios, o certeza, que los sujetos o partes involucradas no están actuando en nombre propio y están tratando de ocultar la identidad del beneficiario o controlador real.'),
(4, '3504', '3504	La información y documentación presentada por las partes involucradas es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(5, '3505', 'Los sujetos o partes involucradas intentan sobornar, extorsionar o amenazan con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(6, '3506', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(7, '3507', 'El cliente o partes involucradas no quiere(n) ser relacionado(s) con la operación realizada.'),
(8, '3508', 'Los sujetos o partes involucradas realizan múltiples operaciones en un periodo muy corto de tiempo sin justificación aparente.'),
(9, '3509', 'Los sujetos o partes involucradas hacen uso de un intermediario (ejemplo: apoderado o representante legal) para realizar la operación o acto sin que exista una causa que lo justifique.'),
(10, '3510', 'El cliente desea liquidar la operación con monedas virtuales.'),
(11, '3511', 'Se constituye una Sociedad por Acciones Simplificadas (SAS), en la que no hay forma de corroborar la identidad de los accionistas.'),
(12, '3512', 'Hay indicios, o certeza de que una de las partes involucradas está siendo presionada para que realice la operación.'),
(13, '3513', 'Hay indicios de que existe suplantación de identidad de alguna de las partes involucradas en la operación.'),
(14, '3514', 'El cliente realiza múltiples operaciones de constitución de personas morales en un periodo corto de tiempo.'),
(15, '3515', 'Se tiene conocimiento de que otro notario se negó a realizar la operación.'),
(16, '3516', 'El pago de la operación es realizado por un tercero sin relación aparente con el cliente o alguna de las partes involucradas.'),
(17, '3517', 'El cliente (personas físicas y  morales), declara una ocupación o actividad distinta a la que aparece en su Cédula de Identificación Fiscal.'),
(18, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo12b`
--

DROP TABLE IF EXISTS `alertas_anexo12b`;
CREATE TABLE IF NOT EXISTS `alertas_anexo12b` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo12b`
--

INSERT INTO `alertas_anexo12b` (`id`, `clave`, `indicador`) VALUES
(1, '1250', 'Uno o más de los sujetos o partes involucradas se rehúsan a proporcionar documentos personales que los identifiquen'),
(2, '1251', 'De acuerdo con la ocupación de uno o más de los sujetos o partes involucradas, la operación o acto parece estar fuera de su alcance'),
(3, '1252', 'De acuerdo con los ingresos declarados por uno o más de los sujetos o partes involucradas, la operación o acto parece estar fuera de su alcance'),
(4, '1253', 'Hay indicios, o certeza, que uno o más de los sujetos o partes involucradas no están actuando en nombre propio y están tratando de ocultar la identidad del beneficiario o controlador real'),
(5, '1254', 'Uno o más de los sujetos o partes involucradas muestran fuerte interés en la realización de la operación o acto con rapidez, sin que exista causa justificada'),
(6, '1255', 'Uno o más de los sujetos o partes involucradas proporcionaron datos falsos  o documentos apócrifos al realizar la operación o acto'),
(7, '1256', 'Uno o más de los sujetos o partes involucradas intentan sobornar o extorsionar a alguna autoridad con  el fin de realizar algún acto de forma irregular.'),
(8, '1257', 'Uno o más de los sujetos o partes involucradas solicitan condiciones especiales poco usuales en la realización de la operación o acto'),
(9, '1258', 'Operaciones con organizaciones sin fines de lucro, cuando las características de la transacción no coinciden con los objetivos de la entidad'),
(10, '1259', 'Se conoce un historial criminal de uno o más de los sujetos o partes involucradas, de algún familiar directo o persona relacionada con ellos'),
(11, '1260', 'Uno o más de los sujetos o partes involucradas hace uso de un intermediario para realizar la operación o acto sin una razón lógica, sin que exista causa justificada'),
(12, '1261', 'Uno o más de los sujetos o partes involucradas realizan múltiples operaciones en un periodo corto de tiempo'),
(13, '1262', 'Uno o más de los sujetos o partes involucradas no muestra tener interés en las características de la propiedad objeto de la operación, en el valor de mercado del inmueble y/o condiciones de la transacción (sólo aplica para Constitución o Transmisión de derechos reales sobre inmuebles)'),
(14, '1263', 'La operación se lleva acabo a un valor significativamente diferente (mucho mayor o mucho menor) a partir del valor real de la propiedad o a los valores de mercado (sólo aplica para Constitución o Transmisión de derechos reales sobre inmuebles)'),
(15, '1264', 'Uno o más de los sujetos o partes involucradas realizan varias operaciones en un periodo corto de tiempo en las que se desconoce el origen de los objetos empeñados (sólo aplica para Otorgamiento de contratos de mutuo o crédito con o sin garantía)'),
(16, '1265', 'La operación de mutuo o crédito se lleva a cabo por medio de una garantía poco usual o que no corresponde con la actividad o ingresos de uno o más de los sujetos o partes involucradas (sólo aplica para Otorgamiento de contratos de mutuo o crédito con o sin garantía)'),
(17, '1266', 'Hay indicios, o certeza, de que las garantías o los bienes empeñados son robados o provienen de una actividad ilícita (sólo aplica para Otorgamiento de contratos de mutuo o crédito con o sin garantía)'),
(18, '1267', 'Uno o más de los sujetos o partes involucradas o personas relacionadas con él realizan múltiples operaciones en un periodo muy corto sin razón aparente (sólo aplica para Otorgamiento de contratos de mutuo o crédito con o sin garantía)'),
(19, '1268', '1268	Uno o más de los sujetos o partes involucradas no muestran tener interés en las características y condiciones del crédito otorgado (sólo aplica para Otorgamiento de contratos de mutuo o crédito con o sin garantía)'),
(20, '1269', 'Uno o más de los sujetos o partes involucradas realizan operaciones de manera periódica en las que se liquida el total del monto del préstamo otorgado en efectivo al poco tiempo de haberlo adquirido (sólo aplica para Otorgamiento de contratos de mutuo o crédito con o sin garantía)'),
(21, '9999', 'Otra alerta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo13`
--

DROP TABLE IF EXISTS `alertas_anexo13`;
CREATE TABLE IF NOT EXISTS `alertas_anexo13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo13`
--

INSERT INTO `alertas_anexo13` (`id`, `clave`, `indicador`) VALUES
(1, '2701', 'El donante condiciona o pretende condicionar el otorgamiento del donativo a la imposición de ciertas condiciones poco usuales para el uso del mismo.'),
(2, '2702', 'El donante otorga uno o varios donativos por un monto elevado sin mostrar interés en el objeto de la organización.'),
(3, '2703', 'El donante intenta o realiza un donativo en especie de bienes poco usuales o sin relación con el objeto de la organización.'),
(4, '2704', 'Hay indicios, o certeza, de que los bienes donados provienen de actividades ilícitas.'),
(5, '2705', 'El donante se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(6, '2706', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.'),
(7, '2707', 'El donante no quiere ser relacionado con el donativo.'),
(8, '2708', 'El monto donado no es acorde con la actividad económica o giro mercantil del donante.'),
(9, '2709', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del donante real.'),
(10, '2710', 'El donativo se realiza en efectivo por un monto elevado.'),
(11, '2711', 'El donativo se realiza usando divisas en efectivo en montos elevados'),
(12, '2712', 'El donativo se realiza por medio de una transferencia proveniente de un país extranjero.'),
(13, '2713', 'La información y documentación presentada por el donante es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(14, '2714', 'El donante realiza un elevado número de donaciones en efectivo y en cantidades elevadas mensualmente, o en periodos identificablemente cortos.'),
(15, '2715', 'El donante intenta sobornar, extorsionar o amenaza a la institución con el fin de realizar la donación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(16, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo15`
--

DROP TABLE IF EXISTS `alertas_anexo15`;
CREATE TABLE IF NOT EXISTS `alertas_anexo15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo15`
--

INSERT INTO `alertas_anexo15` (`id`, `clave`, `indicador`) VALUES
(1, '3001', 'El cliente o usuario paga u ofrece pagar en efectivo, y por adelantado, las rentas correspondientes a un periodo largo de tiempo sin justificación lógica para ello.'),
(2, '3002', 'El cliente o usuario paga por adelantado las rentas de un periodo largo de tiempo en efectivo y posteriormente pide la cancelación del contrato y solicita el reembolso del monto pagado por medio de otro instrumento monetario diferente al efectivo.'),
(3, '3003', 'El cliente o usuario se niega a dar información sobre el uso que se dará al inmueble por arrendar.'),
(4, '3004', 'Hay indicios, o certeza, de que el inmueble arrendado no está siendo utilizado para el propósito expresado por el cliente o usuario, sino para posibles actividades ilícitas.'),
(5, '3005', 'El cliente o usuario se rehúsa a proporcionar documentos personales que lo identifiquen.'),
(6, '3006', 'El pago de las rentas del inmueble es realizado por un tercero sin relación aparente con el cliente o usuario.'),
(7, '3007', 'El cliente o usuario o personas relacionadas con él, realizan múltiples operaciones de arrendamiento con el mismo Sujeto Obligado, en un periodo muy corto de tiempo y sin razón aparente.'),
(8, '3008', 'El cliente o usuario no muestra tener interés en las características del inmueble objeto de arrendamiento, o en el monto de la renta y condiciones del contrato.'),
(9, '3009', 'De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación.\r\n'),
(10, '3010', 'El cliente o usuario no quiere ser relacionado con la operación de arrendamiento.'),
(11, '3011', 'La operación no es acorde con la actividad económica o giro mercantil declarado por el cliente o usuario.'),
(12, '3012', 'El cliente o usuario ofrece pagar un monto de arrendamiento superior al solicitado con la finalidad de que la operación se realice fuera de los parámetros establecidos.'),
(13, '3013', 'Hay indicios, o certeza, que las partes no están actuando en nombre propio y están tratando de ocultar la identidad del cliente o usuario real.'),
(14, '3014', 'El cliente o usuario realiza pagos mediante el uso de divisas en efectivo sin justificación alguna.'),
(15, '3015', 'El cliente o usuario realiza pagos  por medio de transferencia (s) proveniente(s) de un país extranjero.'),
(16, '3016', 'El cliente o usuario insiste en liquidar o pagar la operación en efectivo rebasando el umbral permitido en la Ley.'),
(17, '3017', 'El cliente o usuario intenta sobornar, extorsionar o amenaza al intermediario o dueño del inmueble con el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del Aviso.'),
(18, '3018', 'La información y documentación presentada por el cliente o usuario es inconsistente o de difícil verificación por parte del Sujeto Obligado.'),
(19, '3019', 'El cliente o usuario realiza los pagos con cheques de caja, sin causa que lo justifique.'),
(20, '3020', 'Se tiene sospecha de que el inmueble es utilizado como centro que brinda ayuda a la comunidad o grupos vulnerables que pudieran estar vinculados con actividades u organizaciones delictivas.'),
(21, '3021', 'El cliente o usuario es una persona moral que cambia constantemente de razón social o de representante legal.'),
(22, '3022', 'Un mismo representante legal solicita arrendamiento a nombre de distintas personas (físicas o morales) que no pertenecen al mismo grupo.'),
(23, '3023', 'El cliente o usuario pretende liquidar o pagar la operación con monedas virtuales.'),
(24, '3024', 'El cliente o usuario, su representante o personas ajenas a estos, intenta(n) sobornar o amenazar al Sujeto Obligado o al intermediario para que se agregue una cláusula de subarrendamiento al contrato, sin justificación lógica.'),
(25, '9999', 'Otra alerta.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alertas_anexo16`
--

DROP TABLE IF EXISTS `alertas_anexo16`;
CREATE TABLE IF NOT EXISTS `alertas_anexo16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alertas_anexo16`
--

INSERT INTO `alertas_anexo16` (`id`, `clave`, `indicador`) VALUES
(1, '4101', 'Rápido movimiento de recursos sin justificación aparente.'),
(2, '4102', 'Poca permanencia de los recursos en la plataforma sin justificación aparente.'),
(3, '4103', 'Compra y venta de activos virtuales sin importar pérdidas o ganancias.'),
(4, '4104', 'Cuentas inactivas por largo periodo de tiempo que empiezan a operar altos volúmenes de forma repentina sin justificación aparente.'),
(5, '4105', 'Los datos proporcionados por el cliente no coinciden con los datos de los documentos de identificación.'),
(6, '4106', 'Cambios repentinos en el perfil transaccional del cliente sin aparente justificación.'),
(7, '4107', 'El nombre del cliente se encuentra en listas negras emitidas por organismos nacionales o internacionales  (ONU, OFAC).'),
(8, '4108', 'El cliente se niega a entregar documentación que lo identifique.'),
(9, '4109', 'El cliente actúa a nombre de un tercero sin haberlo declarado previamente.'),
(10, '4110', 'Ingresos no acorde al perfil transaccional o a la actividad económica del cliente.'),
(11, '4111', 'Inactividad en la cuenta tras grandes volúmenes de operación sin justificación aparente.'),
(12, '4112', 'El cliente envía o recibe recursos de una cuenta en algún país de alto riesgo.'),
(13, '4113', 'El cliente no puede justificar el origen de los recursos o es diferente al declarado al inicio de la relación comercial.'),
(14, '4114', 'Hay indicios de que los recursos pudieran provenir de una actividad Ilícita (Ej. noticias, relación con persona sentenciada con actividades ilícitas,etc.).'),
(15, '4115', 'El cliente es identificado con múltiples cuentas con perfiles de operación similares sin aparente justificación.'),
(16, '4116', 'Realización de operaciones desde una dirección IP o un dispositivo distinto a la actividad normal, sin justificación aparente.'),
(17, '4117', 'Recepción de recursos de la cuenta de un tercero sin justificación aparente.'),
(18, '4118', 'Envío de recursos a una cuenta a nombre de un tercero sin justificación aparente.'),
(19, '4119', 'El cliente es una Persona Políticamente Expuesta identificado como de alto riesgo.'),
(20, '4120', 'Hay indicios o certeza de que el cliente no está actuando en nombre propio y está tratando de ocultar la identidad del propietario real.'),
(21, '4121', 'El cliente realiza operaciones fraccionadas por debajo del umbral para evitar que se presente el Aviso correspondiente.'),
(22, '4122', 'El identificador de la transacción coincide con alguna lista nacional o internacional de cadenas identificadas con relación a actividades ilícitas (OFAC).'),
(23, '9999', 'Otra alerta.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `bitacora_docs_cliente` ADD `id_cliente` INT NOT NULL AFTER `fecha`;
ALTER TABLE `grado_riesgo_actividad` ADD `preg16` VARCHAR(1) NULL AFTER `preg15`, ADD `preg17` VARCHAR(1) NULL AFTER `preg16`;

/* ****************************************************************** */
ALTER TABLE `formulario_pep` ADD `si_esposa` VARCHAR(1) NOT NULL COMMENT '1=si, 0=no' AFTER `puesto_cargo_ocupados_ultimo_anio`;

ALTER TABLE `grado_riesgo_actividad` ADD `transaccionalidad_forma` VARCHAR(1) NOT NULL AFTER `transaccionalidad`;

INSERT INTO `menu_sub_usuario_cliente` (`id`, `idmenu`, `nombre`) VALUES (NULL, '2', 'Conf. Diagnostico de Alerta'), (NULL, '2', 'Catálogo de Inmuebles');
UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Expedientes' WHERE `menu_sub_usuario_cliente`.`id` = 7;
INSERT INTO `menu_sub_usuario_cliente` (`id`, `idmenu`, `nombre`) VALUES (NULL, '3', 'Divisas'), (NULL, '3', 'Avisos');

/* ****************************** */
INSERT INTO `menu_usuario_cliente` (`idmenu`, `nombre`) VALUES (NULL, 'Configuración');
INSERT INTO `menu_sub_usuario_cliente` (`id`, `idmenu`, `nombre`) VALUES (NULL, '6', 'Autorizar continuación de Proceso'), (NULL, '6', 'Validar Documentos');
/* ********************************* */
ALTER TABLE `doc_sin_bene` ADD `validado` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `fecha_reg`;

/* ********************************* */
ALTER TABLE `pagos_transac_anexo12a` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `fecha_reg`, ADD `id_pago_ayuda` INT(11) NOT NULL COMMENT 'id del pago que ayudó a la sumatoria' AFTER `acusado`, ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml' AFTER `id_pago_ayuda`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `datos_tipo_patrimoniofi` VARCHAR(5) NOT NULL AFTER `identificador_fideicomisof`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `monedafi` FLOAT(16,2) NOT NULL AFTER `datos_tipo_patrimoniofi`;
ALTER TABLE `anexo12a_datos_fideicomitente` CHANGE `monedafi` `monedafi` VARCHAR(4) NOT NULL;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `monto_operacionfi` DECIMAL(16,2) NOT NULL AFTER `monedafi`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `tipo_inmueblefi` VARCHAR(5) NOT NULL AFTER `monto_operacionfi`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `codigo_postalfi` VARCHAR(5) NOT NULL AFTER `tipo_inmueblefi`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `folio_realfi` VARCHAR(50) NOT NULL AFTER `codigo_postalfi`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `importe_garantiafi` DECIMAL(16,2) NOT NULL AFTER `monto_operacionfi`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `descripcionfi` TEXT NOT NULL AFTER `folio_realfi`;
ALTER TABLE `anexo12a_datos_fideicomitente` ADD `valor_bienfi` DECIMAL(16,2) NOT NULL AFTER `descripcionfi`;

ALTER TABLE `cliente` ADD `cambio_pass` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `status`;
ALTER TABLE `anexo12a` ADD `id_pago_ayude` INT NOT NULL COMMENT 'id del registro al que auyuda a sumatoria' AFTER `id_pago_ayuda`;

ALTER TABLE `limite_anexo_config` CHANGE `permite_avance` `permite_avance` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '0=no,1=si';
ALTER TABLE `diagnostico_alertas` ADD `pregunta_pep_20` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `pregunta_pep_19`, ADD `pregunta_pep_21` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `pregunta_pep_20`, ADD `pregunta_pep_22` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `pregunta_pep_21`, ADD `pregunta_pep_23` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `pregunta_pep_22`;
ALTER TABLE `diagnostico_alertas` ADD `pregunta_25` VARCHAR(1) NOT NULL AFTER `pregunta_24`, ADD `pregunta_26` VARCHAR(1) NOT NULL AFTER `pregunta_25`, ADD `pregunta_27` VARCHAR(1) NOT NULL AFTER `pregunta_26`, ADD `pregunta_28` VARCHAR(1) NOT NULL AFTER `pregunta_27`, ADD `pregunta_29` VARCHAR(1) NOT NULL AFTER `pregunta_28`, ADD `pregunta_30` VARCHAR(1) NOT NULL AFTER `pregunta_29`, ADD `pregunta_31` VARCHAR(1) NOT NULL AFTER `pregunta_30`, ADD `pregunta_32` VARCHAR(1) NOT NULL AFTER `pregunta_31`, ADD `pregunta_33` VARCHAR(1) NOT NULL AFTER `pregunta_32`, ADD `pregunta_34` VARCHAR(1) NOT NULL AFTER `pregunta_33`;

UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Conf. Diagnostico de Alerta (botón)' WHERE `menu_sub_usuario_cliente`.`id` = 11;
UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Config. limite efectivo de anexo (Botón)' WHERE `menu_sub_usuario_cliente`.`id` = 4;
UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Autorizar continuación de Proceso (Por Contraseña)' WHERE `menu_sub_usuario_cliente`.`id` = 15;
UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Validar Documentos (Expedientes)' WHERE `menu_sub_usuario_cliente`.`id` = 16;
DELETE FROM `menu_sub_usuario_cliente` WHERE `menu_sub_usuario_cliente`.`id` = 3

/******************* */
ALTER TABLE `anexo12b` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `activo`, ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no xml, 1 = si xml ' AFTER `acusado`;

ALTER TABLE `diagnostico_alertas` ADD `pregunta_36` VARCHAR(1) NULL AFTER `pregunta_34`, ADD `pregunta_37` VARCHAR(1) NULL AFTER `pregunta_36`, ADD `pregunta_38` VARCHAR(1) NULL AFTER `pregunta_37`, ADD `pregunta_39` VARCHAR(1) NULL AFTER `pregunta_38`, ADD `pregunta_40` VARCHAR(1) NULL AFTER `pregunta_39`, ADD `pregunta_41` VARCHAR(1) NULL AFTER `pregunta_40`, ADD `pregunta_42` VARCHAR(1) NULL AFTER `pregunta_41`, ADD `pregunta_43` VARCHAR(1) NULL AFTER `pregunta_42`, ADD `pregunta_44` VARCHAR(1) NULL AFTER `pregunta_43`, ADD `pregunta_45` VARCHAR(1) NULL AFTER `pregunta_44`;
ALTER TABLE `diagnostico_alertas` ADD `pregunta_35` VARCHAR(1) NULL AFTER `pregunta_34`;
ALTER TABLE `anexo12b` ADD `id_pago_ayuda` INT NOT NULL AFTER `xml`, ADD `id_pago_ayude` INT NOT NULL AFTER `id_pago_ayuda`;

ALTER TABLE `diagnostico_alertas` ADD `conclusiones` TEXT NOT NULL AFTER `pregunta_45`;
ALTER TABLE `diagnostico_alertas` ADD `conclusiones_dir` TEXT NOT NULL AFTER `conclusiones`;
ALTER TABLE `docs_cliente` ADD `cuest_pep` TEXT NULL AFTER `poder_not2`, ADD `cuest_amp` TEXT NULL AFTER `cuest_pep`;

ALTER TABLE `docs_cliente` ADD `fecha_valida` DATETIME NOT NULL AFTER `fecha`;

UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Configuración de Límite de Efectivo' WHERE `menu_sub_usuario_cliente`.`id` = 4;
UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Configuración de Diagnóstico de Alertas' WHERE `menu_sub_usuario_cliente`.`id` = 11;
UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Autorización para clientes de alto riesgo' WHERE `menu_sub_usuario_cliente`.`id` = 15;
UPDATE `menu_sub_usuario_cliente` SET `nombre` = 'Validación de expedientes' WHERE `menu_sub_usuario_cliente`.`id` = 16;

ALTER TABLE `diagnostico_alertas` CHANGE `pregunta_pep_21` `pregunta_pep_21` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0';
ALTER TABLE `diagnostico_alertas` ADD `pregunta_pep_24` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `pregunta_pep_23`, ADD `pregunta_pep_25` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `pregunta_pep_24`;
ALTER TABLE `diagnostico_alertas` ADD `pregunta_25_nva` VARCHAR(1) NOT NULL AFTER `pregunta_24`, ADD `pregunta_26_nva` VARCHAR(1) NOT NULL AFTER `pregunta_25_nva`, ADD `pregunta_27_nva` VARCHAR(1) NOT NULL AFTER `pregunta_26_nva`, ADD `pregunta_28_nva` VARCHAR(1) NOT NULL AFTER `pregunta_27_nva`, ADD `pregunta_29_nva` VARCHAR(1) NOT NULL AFTER `pregunta_28_nva`, ADD `pregunta_30_nva` VARCHAR(1) NOT NULL AFTER `pregunta_29_nva`, ADD `pregunta_31_nva` VARCHAR(1) NOT NULL AFTER `pregunta_30_nva`, ADD `pregunta_32_nva` VARCHAR(1) NOT NULL AFTER `pregunta_31_nva`, ADD `pregunta_33_nva` VARCHAR(1) NOT NULL AFTER `pregunta_32_nva`;

/* ***** ADD table cuestionarios ***** */
/*cuestionario_ampliado_moral
	cuestionario_ampliado_fide
	cuestionario_ampliado_embajada
	cuest_amp_otros_dependiente
	hijos_formulario_pep
	hijos_formulario_pepmoral
*/

ALTER TABLE `operaciones` ADD `folio_cliente` INT NOT NULL AFTER `id`;

INSERT INTO `estados_cp` (`id`, `id_estado`, `codigo`, `colonia`, `mnpio`, `ciudad`) VALUES (NULL, '26', '83268', 'Agua Prieta', 'Hermosillo', 'Hermosillo');
INSERT INTO `estados_cp` (`id`, `id_estado`, `codigo`, `colonia`, `mnpio`, `ciudad`) VALUES (NULL, '26', '83268', 'Centenario', 'Hermosillo', 'Hermosillo');
INSERT INTO `estados_cp` (`id`, `id_estado`, `codigo`, `colonia`, `mnpio`, `ciudad`) VALUES (NULL, '26', '83268', 'Isssteson Centenario', 'Hermosillo', 'Hermosillo');
INSERT INTO `estados_cp` (`id`, `id_estado`, `codigo`, `colonia`, `mnpio`, `ciudad`) VALUES (NULL, '26', '83268', 'Prados del Centenario', 'Hermosillo', 'Hermosillo');
INSERT INTO `estados_cp` (`id`, `id_estado`, `codigo`, `colonia`, `mnpio`, `ciudad`) VALUES (NULL, '26', '83268', 'San Antonio', 'Hermosillo', 'Hermosillo');

ALTER TABLE `alertas_anexo9` CHANGE `indicador` `indicador` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
/* HASTA ACÁ EN PRODUCTIVO Y EL PRUEBAS */

/** *******************UPDATES PARA NVA VERSION 20-09-22********************************/
UPDATE `menu` SET `orden` = '3' WHERE `menu`.`MenuId` = 2;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `activar`, `orden`) VALUES (NULL, '2', 'Bitácora Búsquedas ', 'Bitacora', 'fa fa-line-chart ', '', '1');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '19');


DROP TABLE IF EXISTS `acuses_generados`;
CREATE TABLE IF NOT EXISTS `acuses_generados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anexo` int(11) NOT NULL COMMENT 'id del anexo (actividad)',
  `id_anexo_tabla` int(11) NOT NULL COMMENT 'id del anexo en tabla ',
  `acuse` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `acuses_avisos`;
CREATE TABLE IF NOT EXISTS `acuses_avisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anexo` int(11) NOT NULL COMMENT 'id del anexo (actividad)',
  `id_anexo_tabla` int(11) NOT NULL COMMENT 'id del anexo en tabla ',
  `acuse` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `menu_sub_usuario_cliente` (`id`, `idmenu`, `nombre`) VALUES (NULL, '2', 'Catálogo de desarrollo inmobiliario');

DROP TABLE IF EXISTS `inmuebles5b_cliente`;
CREATE TABLE IF NOT EXISTS `inmuebles5b_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `codigo_postal` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `colonia` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `calle` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_desarrollo` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_desarrollo` text COLLATE utf8_spanish_ci NOT NULL,
  `monto_desarrollo` decimal(14,2) NOT NULL,
  `unidades_comercializadas` tinyint(4) NOT NULL,
  `costo_unidad` decimal(10,2) NOT NULL,
  `otras_empresas` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

ALTER TABLE `anexo5b` ADD `id_inmueble_cli` INT NOT NULL AFTER `registro_licencia`;
ALTER TABLE `anexo5b_aviso` ADD `id_inmueble_cli` INT NOT NULL AFTER `registro_licencia`;

DROP TABLE IF EXISTS `terceros_5b`;
CREATE TABLE IF NOT EXISTS `terceros_5b` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_anexo` int(11) NOT NULL,
  `tipo_persona` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `app` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `apm` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `pais_nacionalidad` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `curp` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `actividad_ocupa` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `razon_social` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_consti` date NOT NULL,
  `pais_nacion_moral` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_moral` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `app_moral` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `apm_moral` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nac_moral` date NOT NULL,
  `rfc_moral` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `curp_moral` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `razon_social_fide` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `rfc_fide` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `curp_fide` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `aportacion` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `instrum_notario` int(4) NOT NULL,
    `tipo_moneda` int(2) NOT NULL,
  `descrip_bien` text COLLATE utf8_spanish_ci NOT NULL,
   `monto_aporta` decimal(16,2) NOT NULL,
  `num_aporta_tercero` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_tercero` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descrip_tercero` text COLLATE utf8_spanish_ci NOT NULL,
  `aport_fide` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `valor_pactado` decimal(16,2) NOT NULL,
   nombre_insti varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


ALTER TABLE `terceros_5b` ADD `estatus` VARCHAR(1) NOT NULL DEFAULT '1' AFTER `nombre_insti`;
ALTER TABLE `terceros_5b` ADD `aviso` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0= no, 1=aviso' AFTER `nombre_insti`;
ALTER TABLE `terceros_5b` ADD `comprobante` TEXT NOT NULL AFTER `valor_pactado`;

ALTER TABLE clientesc_transacciones DROP FOREIGN KEY bene_transac_fk_cctransac;
ALTER TABLE `clientesc_transacciones` CHANGE `idtransbene` `idtransbene` BIGINT(20) NOT NULL COMMENT 'id de tabla = beneficiario_transaccion, se quita fk porque hay clientes que no se le solicita beneficiario';
ALTER TABLE bitacora_xml DROP FOREIGN KEY bene_fk_xml;
ALTER TABLE `bitacora_xml` CHANGE `id_bene_transac` `id_bene_transac` BIGINT(20) NOT NULL COMMENT 'id de tabla = beneficiario_transaccion, se quita fk porque hay clientes que no se le solicita beneficiario';
ALTER TABLE `pagos_transac_anexo5a` ADD `id_pago_ayude` INT NOT NULL AFTER `id_pago_ayuda`;
ALTER TABLE `pagos_transac_anexo5a` ADD `comprobante` TEXT NOT NULL AFTER `monto`;
ALTER TABLE `pagos_transac_anexo5a_aviso` ADD `comprobante` TEXT NOT NULL AFTER `monto`;
ALTER TABLE `terceros_5b` ADD `acusado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=acusado' AFTER `aviso`, ADD `id_pago_ayuda` INT NOT NULL AFTER `acusado`, ADD `id_pago_ayude` INT NOT NULL AFTER `id_pago_ayuda`;
ALTER TABLE `terceros_5b` ADD `xml` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `id_pago_ayude`;
ALTER TABLE `terceros_5b` ADD `comprobante2` TEXT NOT NULL AFTER `comprobante`, ADD `comprobante3` TEXT NOT NULL AFTER `comprobante2`;
ALTER TABLE `pagos_transac_anexo5a` ADD `comprobante2` TEXT NOT NULL AFTER `comprobante`, ADD `comprobante3` TEXT NOT NULL AFTER `comprobante2`;
/* **** CARGADO EN PRUEBAS Y PRODUCTIVO SICOI **** */

ALTER TABLE `pagos_transac_anexo5a_aviso` ADD `comprobante2` TEXT NOT NULL AFTER `comprobante`, ADD `comprobante3` TEXT NOT NULL AFTER `comprobante2`;
ALTER TABLE `aviso_cero` ADD `acuse_sat` TEXT NOT NULL AFTER `id_perfilamiento`;

DROP TABLE IF EXISTS `acuses_24hrs`;
CREATE TABLE IF NOT EXISTS `acuses_24hrs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anexo` int(11) NOT NULL COMMENT 'id del anexo (actividad)	',
  `id_anexo_tabla` int(11) NOT NULL COMMENT 'id del anexo en tabla',
  `acuse` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/* ****************** 20-01-23 ************* */
ALTER TABLE `acuses_24hrs` ADD `acuse_pdf` TEXT NOT NULL AFTER `acuse`;
ALTER TABLE `aviso_cero` ADD `acuse_sat_pdf` TEXT NOT NULL AFTER `acuse_sat`;
ALTER TABLE `acuses_avisos` ADD `acuse_pdf` TEXT NOT NULL AFTER `acuse`;
ALTER TABLE `acuses_generados` ADD `acuse_pdf` TEXT NOT NULL AFTER `acuse`;

/* **** CARGADO EN PRUEBAS Y PRODUCTIVO .APP **** */



/* ********************10-08-2023****************************/
INSERT INTO `estados_cp` (`id`, `id_estado`, `codigo`, `colonia`, `mnpio`, `ciudad`) VALUES (NULL, '15', '45115', 'Colinas Virreyes', 'Zapopan', 'Jalisco'), (NULL, '15', '45115', 'Entrelomas Residencial', 'Zapopan', 'Jalisco');
INSERT INTO `estados_cp` (`id`, `id_estado`, `codigo`, `colonia`, `mnpio`, `ciudad`) VALUES (NULL, '15', '45115', 'Los Castaños', 'Zapopan', 'Jalisco'), (NULL, '15', '45115', 'Puerta las Lomas', 'Zapopan', 'Jalisco'),
(NULL, '15', '45115', 'Residencial Parque Virreyes', 'Zapopan', 'Jalisco'), (NULL, '15', '45115', 'Villa Verona', 'Zapopan', 'Jalisco'),
(NULL, '15', '45115', 'Vistas del Tule I', 'Zapopan', 'Jalisco'), (NULL, '15', '45115', 'Vistas del Tule II', 'Zapopan', 'Jalisco');


/* **********************01-09-23*********************************** */
ALTER TABLE `anexo5a` ADD `liquidada` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `fecha_contrato`;
ALTER TABLE `anexo5a_aviso` ADD `liquidada` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `fecha_creacion`;
ALTER TABLE `anexo11_actividad3` ADD `tipo_area_servicio` INT NOT NULL AFTER `num_opera`, ADD `descripcion4` INT NOT NULL AFTER `tipo_area_servicio`, ADD `desc_otro_activo_admin` TEXT NOT NULL AFTER `descripcion4`, ADD `num_empledos` TINYINT NOT NULL AFTER `desc_otro_activo_admin`;
ALTER TABLE `anexo11_actividad3` ADD `tipo_activo_admin` INT NOT NULL AFTER `descripcion4`;

ALTER TABLE `anexo11_actividad3_aviso` ADD `tipo_area_servicio` INT NOT NULL AFTER `num_opera`, ADD `tipo_activo_admin` INT NOT NULL AFTER `tipo_area_servicio`, ADD `desc_otro_activo_admin` TEXT NOT NULL AFTER `tipo_activo_admin`, ADD `num_empledos` TINYINT NOT NULL AFTER `desc_otro_activo_admin`;
ALTER TABLE `anexo11_actividad3_aviso` ADD `descripcion4` VARCHAR(100) NOT NULL AFTER `tipo_area_servicio`;
ALTER TABLE `anexo11_actividad3` CHANGE `descripcion4` `descripcion4` TEXT NOT NULL;

ALTER TABLE `anexo11_actividad3` CHANGE `num_empledos` `num_empledos` INT(5) NOT NULL;

ALTER TABLE `anexo11` ADD `ocupacion` VARCHAR(2) NOT NULL AFTER `num_factura`;
ALTER TABLE `anexo11` ADD `desc_otra_ocupa` VARCHAR(100) NOT NULL AFTER `ocupacion`;
ALTER TABLE `anexo11_aviso` ADD `ocupacion` VARCHAR(2) NOT NULL AFTER `num_factura`;
ALTER TABLE `anexo11_aviso` ADD `desc_otra_ocupa` VARCHAR(100) NOT NULL AFTER `ocupacion`;

/* *********************************************/
ALTER TABLE `anexo11_actividad3` ADD `num_opera_out` INT NOT NULL AFTER `num_empledos`;
ALTER TABLE `anexo11_actividad3_aviso` ADD `num_opera_out` INT NOT NULL AFTER `num_empledos`;

CREATE TABLE IF NOT EXISTS `anexo11_actividad3_pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anexo11` int(11) NOT NULL,
  `instrumento_monetario3_4` int(11) NOT NULL,
  `moneda3_4` int(11) NOT NULL,
  `monto_operacion3_4` decimal(10,2) NOT NULL,
  `fecha` date NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE IF NOT EXISTS `anexo11_aviso_actividad3_pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idanexo11_aviso` int(11) NOT NULL,
  `instrumento_monetario3_4` int(11) NOT NULL,
  `moneda3_4` int(11) NOT NULL,
  `monto_operacion3_4` decimal(10,2) NOT NULL,
  `fecha` date NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

/* *************************14-01-2025**********************************/
ALTER TABLE `bitacora_docs_cliente` ADD `id_operacion` INT NOT NULL AFTER `id_cliente`;
ALTER TABLE `docs_cliente` ADD `id_operacion` INT NOT NULL AFTER `fecha_valida`;
ALTER TABLE `docs_extra_clientesc` ADD `id_operacion` INT NOT NULL AFTER `fecha`;

ALTER TABLE `bitacora_docs_bene` ADD `id_operacion` INT NOT NULL AFTER `id_cliente`;
ALTER TABLE `docs_beneficiario` ADD `id_operacion` INT NOT NULL AFTER `carta_podert`;
ALTER TABLE `docs_extra_bene` ADD `id_operacion` INT NOT NULL AFTER `fecha_reg`;