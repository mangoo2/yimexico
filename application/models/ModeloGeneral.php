<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGeneral extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
//==============Personal==========
    function getpersonal($params,$perfil,$sucursal){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.id',
            2=>'p.nombre',
            3=>'p.telefono',
            4=>'s.sucursal',

        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('sucursal s', 'p.idsucursal=s.idsucursal');
            if($perfil==1){
                $where = array('p.activo'=>1);
            }else{
                $where = array('p.activo'=>1,'p.idsucursal'=>$sucursal);
            }

            $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_personas($params,$perfil,$sucursal){
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('sucursal s', 'p.idsucursal=s.idsucursal');
            if($perfil==1){
                $where = array('p.activo'=>1);
            }else{
                $where = array('p.activo'=>1,'p.idsucursal'=>$sucursal);
            }
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row()->total;
    }
//==============Perfiles====================
    function getperfiles($params){
        $columns = array( 
            0=>'perfilId',
            1=>'perfil',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('perfiles');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_perfiles($params){
        $this->db->select('COUNT(1) as total');
        $this->db->from('perfiles');
        $query=$this->db->get();
        return $query->row()->total;
    }
    // Actasadministrativas
    public function getpersona($id)
    {
        $strq = "SELECT p.personalId,p.id,p.nombre,pu.puesto,pe.perfil FROM personal AS p
                INNER JOIN usuarios AS u ON u.personalId = p.personalId
                INNER JOIN perfiles AS pe ON pe.perfilId = u.perfilId
                INNER JOIN puesto AS pu ON pu.idpuesto = p.idpuesto
                WHERE p.personalId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    // Formato PDf
    public function getpersonaformatoacta($id)
    {
        $strq = "SELECT p.personalId,p.nombre,s.ciudad,e.nom_ent,pu.puesto,a.fecha,a.hora,a.incidencia,a.comentarios FROM actas_administrativas AS a
                INNER JOIN personal AS p ON p.personalId = a.personalId
                INNER JOIN sucursal AS s ON s.idsucursal = p.idsucursal
                INNER JOIN estados AS e ON e.cve_ent = s.estado
                INNER JOIN puesto AS pu ON pu.idpuesto = p.idpuesto
                WHERE a.idacta=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getpersonaformatocarta($id)
    {
        $strq = "SELECT p.personalId,p.nombre,s.ciudad,e.nom_ent,pu.puesto,c.fecha,c.hora,c.evento,c.comentario FROM carta_compromiso AS c
                INNER JOIN personal AS p ON p.personalId = c.personalId
                INNER JOIN sucursal AS s ON s.idsucursal = p.idsucursal
                INNER JOIN estados AS e ON e.cve_ent = s.estado
                INNER JOIN puesto AS pu ON pu.idpuesto = p.idpuesto
                WHERE c.idcarta=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getpersonaformatoconstancia($id)
    {
        $strq = "SELECT p.id,p.personalId,p.nombre,s.ciudad,e.nom_ent,pu.puesto,c.fecha,c.hora,c.comentario,s.direccion,pe.nombre AS encargado,pue.puesto AS puesto_encargado,per.nombre AS delegado,pues.puesto AS puesto_delegado,pers.nombre AS coordinador,puest.puesto AS puesto_coordinador FROM constaciahechos AS c
                INNER JOIN personal AS p ON p.personalId = c.personalId
                INNER JOIN personal AS pe ON pe.personalId = c.personalId_encargado
                INNER JOIN personal AS per ON per.personalId = c.personalId_delegado
                INNER JOIN personal AS pers ON pers.personalId = c.personalId_coordinador
                INNER JOIN sucursal AS s ON s.idsucursal = p.idsucursal
                INNER JOIN estados AS e ON e.cve_ent = s.estado
                INNER JOIN puesto AS pu ON pu.idpuesto = p.idpuesto
                INNER JOIN puesto AS pue ON pue.idpuesto = pe.idpuesto
                INNER JOIN puesto AS pues ON pues.idpuesto = per.idpuesto
                INNER JOIN puesto AS puest ON puest.idpuesto = pers.idpuesto
                WHERE c.idconstancia=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    //==============SASSO==========
    function getsasso($params,$perfil,$sucursal){
        $columns = array( 
            0=>'s.idsasso',
            2=>'p.personalId',
            3=>'p.nombre',
            4=>'s.fecha',
            5=>'s.hora',
            6=>'s.evento',
            7=>'s.observaciones',
            8=>'s.foto',
            9=>'s.tipo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('sasso s');
        $this->db->join('personal p', 'p.personalId=s.personalId');
            if($perfil==1){
                $where = array('s.activo'=>1);
            }else{
                $where = array('s.activo'=>1,'p.idsucursal'=>$sucursal);
            }

            $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_sasso($params,$perfil,$sucursal){
        $this->db->select('COUNT(1) as total');
        $this->db->from('sasso s');
        $this->db->join('personal p', 'p.personalId=s.personalId');
            if($perfil==1){
                $where = array('s.activo'=>1);
            }else{
                $where = array('s.activo'=>1,'p.idsucursal'=>$sucursal);
            }
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row()->total;
    }
//==============Clientes====================   
    function get_cliente($params){
        $columns = array( 
            0=>'c.idcliente',
            1=>'pf.nombre',
            2=>'pf.apellido_paterno',
            4=>'pf.apellido_materno',
            5=>'pm.razon_social',
            6=>'pfi.denominacion_razon_social',
            7=>'c.fecha_alta_sistema',
            8=>'c.tipo_persona',
            9=>'a.actividad',
            10=>'ag.nombre as nombreAnexo'
        );
        $columnsSearch = array( 
            0=>'c.idcliente',
            1=>'pf.nombre',
            2=>'pf.apellido_paterno',
            4=>'pf.apellido_materno',
            5=>'pm.razon_social',
            6=>'pfi.denominacion_razon_social',
            7=>'c.fecha_alta_sistema',
            8=>'c.tipo_persona',
            9=>'a.actividad',
            10=>'ag.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cliente c');
        $this->db->join('cliente_actividad ca', 'ca.idcliente = c.idcliente','left');
        $this->db->join('cliente_fisica pf', 'pf.idcliente=c.idcliente','left');
        $this->db->join('cliente_moral pm', 'pm.idcliente=c.idcliente','left');
        $this->db->join('cliente_fideicomiso pfi', 'pfi.idcliente=c.idcliente','left');

        $this->db->join('actividad a', 'ON a.id = ca.idactividad','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad = ca.idactividad','left');
        $this->db->join('anexos_grales ag', 'ag.id = aa.id_anexo','left');

        $this->db->where('c.activo=1');
        $this->db->where('ca.activo=1');
        $this->db->group_by('c.idcliente');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsSearch as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsSearch[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_cliente($params){
        $columns = array( 
            0=>'c.idcliente',
            1=>'pf.nombre',
            2=>'pf.apellido_paterno',
            4=>'pf.apellido_materno',
            5=>'pm.razon_social',
            6=>'pfi.denominacion_razon_social',
            7=>'c.fecha_alta_sistema',
            8=>'c.tipo_persona',
            9=>'a.actividad',
            10=>'ag.nombre'
        );
        $columnsSearch = array( 
            0=>'c.idcliente',
            1=>'pf.nombre',
            2=>'pf.apellido_paterno',
            4=>'pf.apellido_materno',
            5=>'pm.razon_social',
            6=>'pfi.denominacion_razon_social',
            7=>'c.fecha_alta_sistema',
            8=>'c.tipo_persona',
            9=>'a.actividad',
            10=>'ag.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('cliente c');
        $this->db->join('cliente_actividad ca', 'ca.idcliente = c.idcliente','left');
        $this->db->join('cliente_fisica pf', 'pf.idcliente=c.idcliente','left');
        $this->db->join('cliente_moral pm', 'pm.idcliente=c.idcliente','left');
        $this->db->join('cliente_fideicomiso pfi', 'pfi.idcliente=c.idcliente','left');
        $this->db->join('actividad a', 'ON a.id = ca.idactividad','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad = ca.idactividad','left');
        $this->db->join('anexos_grales ag', 'ag.id = aa.id_anexo','left');

        $this->db->where('c.activo=1');
        $this->db->where('ca.activo=1');
        $this->db->group_by('c.idcliente');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsSearch as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        $query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    //<!--- Usuarios clientes -->
    function get_usuario_clientes($params,$idcliente){
        $columns = array( 
            0=>'u.idcliente_usuario',
            1=>'u.nombre',
            2=>'u.apellido_paterno',
            3=>'u.apellido_materno',
            4=>'p.perfil',
            5=>"funcion_puesto"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('usuarios_clientes u');
        $this->db->JOIN('perfiles p','p.perfilId=u.funcion_puesto','left');
        $arraywhere = array('u.activo' => 1,'u.idcliente'=>$idcliente);
        $this->db->where($arraywhere);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_usuarios_clientes($params,$idcliente){
        $columns = array( 
            0=>'u.idcliente_usuario',
            1=>'u.nombre',
            2=>'u.apellido_paterno',
            3=>'u.apellido_materno',
            4=>'p.perfil',
            5=>"funcion_puesto"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('usuarios_clientes u');
        $this->db->JOIN('perfiles p','p.perfilId=u.funcion_puesto','left');
        $arraywhere = array('u.activo' => 1,'u.idcliente'=>$idcliente);
        $this->db->where($arraywhere);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        $query=$this->db->get();
        //return $query->row()->total;
        return $query->row()->total;
    }
    //<!--- listado de estados -->
    function get_datos_estados($params){
        $columns = array( 
            0=>'clave',
            1=>'estado',
            2=>'calificacion',
            3=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('estado');
        if( !empty($params['search']['value']) ) {
            if($params['search']['value']=='baja' || $params['search']['value']=='Baja'){
                $arraywhere = array('riesgo'=>1);
                $this->db->where($arraywhere);
            }else if($params['search']['value']=='alta' || $params['search']['value']=='Alta'){
                $arraywhere = array('riesgo'=>2);
                $this->db->where($arraywhere); 
            }else{              
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end(); 
            } 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_get_datos_estados($params){
        $columns = array( 
            0=>'clave',
            1=>'estado',
            2=>'calificacion',
            3=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('estado');
        if( !empty($params['search']['value']) ) {
            if($params['search']['value']=='baja' || $params['search']['value']=='Baja'){
                $arraywhere = array('riesgo'=>1);
                $this->db->where($arraywhere);
            }else if($params['search']['value']=='alta' || $params['search']['value']=='Alta'){
                $arraywhere = array('riesgo'=>2);
                $this->db->where($arraywhere); 
            }else{              
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end(); 
            } 
        }  
        $query=$this->db->get();
        return $query->row()->total;
        return $query->row()->total;
    }
    //<!--- listado de actividad fisica -->
    function get_activida_fisica($params){
        $columns = array( 
            0=>'clave',
            1=>'acitividad',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('actividad_econominica_fisica');
        if( !empty($params['search']['value']) ) {
            if($params['search']['value']=='baja' || $params['search']['value']=='Baja'){
                $arraywhere = array('riesgo'=>1);
                $this->db->where($arraywhere);
            }else if($params['search']['value']=='alta' || $params['search']['value']=='Alta'){
                $arraywhere = array('riesgo'=>2);
                $this->db->where($arraywhere); 
            }else{              
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end(); 
            } 
        }         
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_activida_fisica($params){
        $columns = array( 
            0=>'clave',
            1=>'acitividad',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('actividad_econominica_fisica');
        if( !empty($params['search']['value']) ) {
            if($params['search']['value']=='baja' || $params['search']['value']=='Baja'){
                $arraywhere = array('riesgo'=>1);
                $this->db->where($arraywhere);
            }else if($params['search']['value']=='alta' || $params['search']['value']=='Alta'){
                $arraywhere = array('riesgo'=>2);
                $this->db->where($arraywhere); 
            }else{              
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end(); 
            } 
        }     
        $query=$this->db->get();
        return $query->row()->total;
        return $query->row()->total;
    }
    //<!--- listado de actividad fisica -->
    function get_activida_moral($params){
        $columns = array( 
            0=>'clave',
            1=>'giro_mercantil',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('actividad_econominica_morales');
        if( !empty($params['search']['value']) ) {
            if($params['search']['value']=='baja' || $params['search']['value']=='Baja'){
                $arraywhere = array('riesgo'=>1);
                $this->db->where($arraywhere);
            }else if($params['search']['value']=='alta' || $params['search']['value']=='Alta'){
                $arraywhere = array('riesgo'=>2);
                $this->db->where($arraywhere); 
            }else{              
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end(); 
            } 
        }              
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_activida_moral($params){
        $columns = array( 
            0=>'clave',
            1=>'giro_mercantil',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('actividad_econominica_morales');
        if( !empty($params['search']['value']) ) {
            if($params['search']['value']=='baja' || $params['search']['value']=='Baja'){
                $arraywhere = array('riesgo'=>1);
                $this->db->where($arraywhere);
            }else if($params['search']['value']=='alta' || $params['search']['value']=='Alta'){
                $arraywhere = array('riesgo'=>2);
                $this->db->where($arraywhere); 
            }else{              
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end(); 
            } 
        }    
        $query=$this->db->get();
        //return $query->row()->total;
        return $query->row()->total;
    }

    public function getDatosCPEstado($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        /*if($col!="0"){
            $this->db->like("colonia",$col);
        }*/
        $query=$this->db->get();
        return $query->result();
    }

    public function data_cp_DF(){
        $this->db->select("*");
        $this->db->from("estados_cp ep");
        $this->db->where("CHARACTER_LENGTH(codigo)",4);

        $query=$this->db->get();
        return $query->result();
    }
    ////////////////// AVISO CEROS /////////////////////// 
    function get_aviso_cero($params){
        $columns = array( 
            0=>'id',
            1=>'mes_acuse',
            2=>'informe',
            3=>'acuse_sat',
            4=>'actividad'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('aviso_cero');
        $where = array('activo'=>1,'anio_acuse'=>$params['anio'],'actividad'=>$params['actividad'],'idcliente'=>$params['idcliente'],'informe'=>$params['tipo']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_aviso_cero($params){
        $columns = array( 
            0=>'id',
            1=>'mes_acuse',
            2=>'informe'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('aviso_cero');
        $where = array('activo'=>1,'anio_acuse'=>$params['anio'],'actividad'=>$params['actividad'],'idcliente'=>$params['idcliente'],'informe'=>$params['tipo']);
        $this->db->where($where);
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        } 
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_opera_benes($id,$idcc,$id_p){
        $this->db->select("ob.*");
        $this->db->from("operacion_beneficiario ob");
        $this->db->join("operaciones o","o.id=ob.id_operacion");
        $this->db->where("ob.id_perfilamiento",$id_p);
        //$this->db->where("ob.id_clientec",$idcc);
        $this->db->where("o.id",$id);
        $this->db->where("ob.activo",1);
        $query=$this->db->get();
        return $query->result();
    }
    ////////////////////////////////////////////// Paises
    //<!--- listado de estados -->
    function get_lugares_frontera($params){
        $columns = array( 
            0=>'id',
            1=>'ciudad',
            2=>'estado',
            3=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_lugares_frontera');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }      
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_get_lugares_frontera($params){
        $columns = array( 
            0=>'id',
            1=>'ciudad',
            2=>'estado',
            3=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_lugares_frontera');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_paises_puertos_entrada_salida_acuerdo_sistema_portuario_nacional($params){
        $columns = array( 
            0=>'id',
            1=>'puerto',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_puertos_entrada_salida_acuerdo_sistema_portuario_nacional');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }      
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_get_paises_puertos_entrada_salida_acuerdo_sistema_portuario_nacional($params){
        $columns = array( 
            0=>'id',
            1=>'puerto',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_puertos_entrada_salida_acuerdo_sistema_portuario_nacional');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_indice_basilea($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'nivel_riesgo',
            3=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_indice_basilea');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }       
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_indice_basilea($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'nivel_riesgo',
            3=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_indice_basilea');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_indice_secrecia_bancaria($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_indice_secrecia_bancaria');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_indice_secrecia_bancaria($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_indice_secrecia_bancaria');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_indice_corrupcion($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_indice_corrupcion');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_indice_corrupcion($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_indice_corrupcion');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_indice_gafi($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_indice_gafi');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_indice_gafi($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_indice_gafi');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_incidencia_delictiva($params){
        $columns = array( 
            0=>'id',
            1=>'ciudad',
            2=>'estado',
            3=>'indice_delictivo',
            4=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_incidencia_delictiva');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_incidencia_delictiva($params){
        $columns = array( 
            0=>'id',
            1=>'ciudad',
            2=>'estado',
            3=>'indice_delictivo',
            4=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_incidencia_delictiva');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_percepcion_corrupcion_estatal_zonas_urbanas($params){
        $columns = array( 
            0=>'id',
            1=>'ciudad',
            2=>'estado',
            3=>'percepcion_corrupcion',
            4=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_percepcion_corrupcion_estatal_zonas_urbanas');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_percepcion_corrupcion_estatal_zonas_urbanas($params){
        $columns = array( 
            0=>'id',
            1=>'ciudad',
            2=>'estado',
            3=>'percepcion_corrupcion',
            4=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_percepcion_corrupcion_estatal_zonas_urbanas');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_indice_paz($params){
        $columns = array( 
            0=>'id',
            1=>'estado',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_indice_paz');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_indice_paz($params){
        $columns = array( 
            0=>'id',
            1=>'estado',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_indice_paz');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_paises_guia_terrorismo($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_guia_terrorismo');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_paises_guia_terrorismo($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_guia_terrorismo');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_indice_global_terrorismo($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_indice_global_terrorismo');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_indice_global_terrorismo($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_indice_global_terrorismo');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    
    function get_paises_indice_corrupcion_global($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_indice_corrupcion_global');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_paises_indice_corrupcion_global($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_indice_corrupcion_global');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_paises_regimenes_fiscales_preferentes($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('paises_regimenes_fiscales_preferentes');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_paises_regimenes_fiscales_preferentes($params){
        $columns = array( 
            0=>'id',
            1=>'pais',
            2=>'riesgo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('paises_regimenes_fiscales_preferentes');
        $this->db->where('activo=1');
        if( !empty($params['search']['value']) ) {            
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}