<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModeloConfiguracion extends CI_Model {

	public function __construct(){
		parent::__construct();
		
	}

    public function getDatosConfig($idcli){
        $this->db->select('a.id as id_act, ca.*, a.actividad, ,ag.nombre, IFNULL(la.permite_avance,"n") as permite_avance, IFNULL(la.id,"0") as idconfig');
        $this->db->from("cliente_actividad ca");
        $this->db->join("actividad a","a.id=ca.idactividad");
        $this->db->join("actividad_anexo aa","aa.id_actividad=ca.idactividad");
        $this->db->join("anexos_grales ag","ag.id=aa.id_anexo");
        $this->db->join("limite_anexo_config la","la.id_cliente=ca.idcliente","left");
        $this->db->where("ca.idcliente",$idcli);
        $this->db->where("ca.activo","1");
        $this->db->group_by("idactividad");
        $this->db->order_by("a.id","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getConfigAnexo($idcli,$ida){
        $this->db->select('la.*');
        $this->db->from("limite_anexo_config la");
        $this->db->where("id_cliente",$idcli);
        $this->db->where("id_anexo",$ida);
        $query=$this->db->get();
        return $query->result();
    }

}
