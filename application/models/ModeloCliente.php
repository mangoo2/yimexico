<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModeloCliente extends CI_Model {

  public function __construct(){
    parent::__construct();
    
  }
    public function get_select_like_pais($buscar) {
        $strq = "SELECT * FROM pais where pais like '%$buscar%' or clave like '%$buscar%'";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_actividad($id) {
        $strq = "SELECT 
                    c.tipo_persona,c.telefono,c.movil,c.correo, IFNULL(pf.actividad_o_giro,'') as actividad_o_giropf, IFNULL(pf.otra_ocupa,'') as otra_ocupapf,
                    IFNULL(pm.actividad_o_giro,'') as actividad_o_giropm, IFNULL(pm.otra_ocupa,'') as otra_ocupapm,
                    IFNULL(pfi.actividad_o_giro,'') as actividad_o_giropfi, IFNULL(pfi.otra_ocupa,'') as otra_ocupapfi,
                    pf.idfisica,pf.nombre,pf.apellido_paterno,pf.apellido_materno,IFNULL(pf.rfc,'') as rfc,pf.fecha_constitucion AS fecha_constitucionf,pf.pais_nacimiento,pf.pais_nacionalidad AS pais_nacionalidad_f,pf.curp,
                    pm.idmoral,pm.razon_social,IFNULL(pm.r_c_f,'') as r_c_f,pm.fecha_constitucion,pm.pais_nacionalidad,pm.nombre_l,pm.apellido_paterno_l,pm.apellido_materno_l,pm.fecha_nacimiento_l,pm.rfc_l,pm.curp_l,
                    pfi.idfideicomiso,pfi.denominacion_razon_social,IFNULL(pfi.rfc_fideicomiso,'') as rfc_fideicomiso,pfi.identificador_fideicomiso,pfi.nombre_l AS nombre_lf,pfi.apellido_paterno_l AS apellido_paterno_lf,pfi.apellido_materno_l AS apellido_materno_lf,pfi.fecha_nacimiento_l AS fecha_nacimiento_lf,pfi.rfc_l AS rfc_lf,pfi.curp_l AS curp_lf,
                    u.UsuarioID,u.Usuario FROM cliente AS c
                LEFT JOIN cliente_fisica AS pf ON pf.idcliente=c.idcliente
                LEFT JOIN cliente_moral AS pm ON pm.idcliente=c.idcliente
                LEFT JOIN cliente_fideicomiso AS pfi ON pfi.idcliente=c.idcliente
                LEFT JOIN usuarios AS u ON u.idcliente=c.idcliente
                where c.idcliente=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_clintes_all($id) {
        $strq = "SELECT 
                    c.tipo_persona,c.telefono,c.movil,c.correo,
                    pf.idfisica,pf.nombre,pf.apellido_paterno,pf.apellido_materno,pf.rfc,pf.fecha_constitucion AS fecha_constitucionf,pf.pais_nacimiento,pf.pais_nacionalidad AS pais_nacionalidad_f,pf.curp,
                    pm.idmoral,pm.razon_social,pm.r_c_f,pm.fecha_constitucion,pm.pais_nacionalidad,pm.nombre_l,pm.apellido_paterno_l,pm.apellido_materno_l,pm.fecha_nacimiento_l,pm.rfc_l,pm.curp_l,
                    pfi.idfideicomiso,pfi.denominacion_razon_social,pfi.rfc_fideicomiso,pfi.identificador_fideicomiso,pfi.nombre_l AS nombre_lf,pfi.apellido_paterno_l AS apellido_paterno_lf,pfi.apellido_materno_l AS apellido_materno_lf,pfi.fecha_nacimiento_l AS fecha_nacimiento_lf,pfi.rfc_l AS rfc_lf,pfi.curp_l AS curp_lf,
                    u.UsuarioID,u.Usuario FROM cliente AS c
                LEFT JOIN cliente_fisica AS pf ON pf.idcliente=c.idcliente
                LEFT JOIN cliente_moral AS pm ON pm.idcliente=c.idcliente
                LEFT JOIN cliente_fideicomiso AS pfi ON pfi.idcliente=c.idcliente
                LEFT JOIN usuarios AS u ON u.idcliente=c.idcliente
                where c.idcliente=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getselecsucursal($id) {
        $strq = "SELECT * FROM cliente_direccion AS c
                 LEFT JOIN pais AS p ON p.clave = c.idpais
                 WHERE c.activo=1 AND idcliente=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getselectactividades($id) {
        $strq = "SELECT a.*,c.id_cliente_actividad,c.idactividad, a.actividad,ag.nombre FROM cliente_actividad AS c
                 LEFT JOIN actividad AS a ON a.id = c.idactividad
                 LEFT JOIN actividad_anexo AS aa ON aa.id_actividad = c.idactividad
                 LEFT JOIN anexos_grales AS ag ON ag.id = aa.id_anexo
                 WHERE c.activo=1 AND idcliente=$id
                 group by a.id
                 order by a.id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function getActividadesCliente($id) {
        $strq = "SELECT c.id_cliente_actividad,c.idactividad, a.actividad,ag.nombre, aa.id_anexo FROM cliente_actividad AS c
                 LEFT JOIN actividad AS a ON a.id = c.idactividad
                 LEFT JOIN actividad_anexo AS aa ON aa.id_actividad = c.idactividad
                 LEFT JOIN anexos_grales AS ag ON ag.id = aa.id_anexo
                 WHERE c.activo=1 AND idcliente=$id
                 group by c.idactividad
                 order by a.id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getActividadesCliente_all() {
        $strq = "SELECT a.id, a.actividad,ag.nombre, aa.id_anexo FROM actividad AS a
                 LEFT JOIN actividad_anexo AS aa ON aa.id_actividad = a.id
                 LEFT JOIN anexos_grales AS ag ON ag.id = aa.id_anexo
                 GROUP BY a.id
                 order by a.id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getselectanexoa($id) {
        $strq = "SELECT ad.*,p.pais,a.actividad FROM anexoa AS ax
                LEFT JOIN anexoa_direccion AS ad ON ad.idanexoa = ax.idanexoa
                LEFT JOIN pais AS p ON p.clave = ad.idpais
                LEFT JOIN actividad AS a ON a.id = ad.idactividad
                WHERE ad.activo=1 AND ax.idcliente=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_direccion_cliente($id){
        $strq = "SELECT c.*, p.pais FROM cliente_direccion AS c 
                    LEFT JOIN pais AS p ON p.clave = c.idpais
                    WHERE idcliente = $id
                    GROUP BY idcliente";
        $query = $this->db->query($strq);
        return $query->result();
    }
    
    public function get_tableActiv($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('activo',1);
        $query = $this->db->get();
        if($query->num_rows()>0)
            return $query->result();
        else
           return 0; 
        //return $query->result();
    }

    /*public function get_tipoclientes_all($idc){
      $strq = "SELECT * FROM perfilamiento as p where p.activo=1 and p.idcliente=$idc";
      $query = $this->db->query($strq);
      return $query->result(); 
    }*/

    public function get_clientes_all($idc){ //del listado de clientes
        $this->db->select('dc.*,p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          (select hpb.resultado from historico_consulta_pb hpb where hpb.id_perfilamiento=p.idperfilamiento order by hpb.id DESC limit 1) as resultado,
          IFNULL((select max(id_actividad) as ultimo_activ from clientesc_transacciones where id_perfilamiento=p.idperfilamiento),0) as ultimo_activ, 
          IFNULL((select max(id_transaccion) as ultimo_id from clientesc_transacciones where id_perfilamiento=p.idperfilamiento),0) as ultimo_id,
          (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, 
          "0" as tipo_cli,
          IFNULL(cpf.actualmente_usted_precandidato, "0") as act_precand, 
          IFNULL(cpf.familiar_primer_segundo_tercer_grado_socio_algun_precandidato, "0") as familiar_1_2_3_grado, 
          IFNULL(cpf.usted_socio_algun_precandidato, "0") as socio_candidato, 
          IFNULL(cpf.usted_asesor_agente_representante_algun_precandidato, "0") as asesor_candidato, 
          IFNULL(cpf.usted_responsable_campanna_politica, "0") as responsable_campania, 
          IFNULL(cpm.empresa_persona_moral_hay_algun_apoderado_legal, "0") as empresa_apoderado, 
          IFNULL(cpm.empresa_persona_moral_hay_algun_apoderado_legal_accionista, "0") as empresa_apoderado_accionista, 
          IFNULL(cpm.empresa_persona_moral_existe_algun_precandidato, "0") as empresa_precandidato, 
          IFNULL(cpm.empresa_persona_moral_existe_algun_responsable_campana_politica, "0") as empresa_resp_campania ');
        $this->db->from('perfilamiento p');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        //$this->db->join('clientesc_transacciones ct','ct.id_perfilamiento=p.idperfilamiento','left');
        $this->db->join('docs_cliente dc', 'dc.id_perfilamiento=p.idperfilamiento','left');
        $this->db->join('historico_consulta_pb hpb', 'hpb.id_perfilamiento=p.idperfilamiento','left');

        $this->db->join('complemento_persona_fisica cpf', 'cpf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('complemento_persona_moral cpm', 'cpm.idperfilamiento=p.idperfilamiento','left');

        $this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$idc);
        $this->db->group_by('hpb.id_perfilamiento');
        $this->db->order_by('pfm.nombre',"asc");
        $this->db->order_by('pfe.nombre',"asc");
        $this->db->order_by('pmme.razon_social',"asc");
        $this->db->order_by('pmmd.nombre_persona',"asc");
        $this->db->order_by('ecoi.denominacion',"asc");
        $this->db->order_by('cf.denominacion',"asc");

        $query = $this->db->get();
        return $query->result();
    }

    /*public function get_clientes_union_all($idc){ //del listado de clientes unidos
        $this->db->select('uc.*,uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, "1" as tipo_cli ');
        $this->db->from('uniones u');
        $this->db->join('uniones_clientes uc','uc.id_union=u.id');
        $this->db->join('perfilamiento p',"p.idperfilamiento=uc.id_perfilamiento");
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');

        $this->db->where('p.activo',1);
        $this->db->where('uc.id_cliente',$idc);
        $this->db->where('uc.activo',1);
        $this->db->where('u.activo',1);
        $this->db->group_by('uc.id_union');
        $query = $this->db->get();
        return $query->result();
    }*/

    public function get_clientes_union_all($idc){
      $this->db->select("uc.*, uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
          concat(pfe.nombre,' ',pfe.apellido_paterno,' ',pfe.apellido_materno) as cliente, 
          IFNULL(pfe.idtipo_cliente_p_f_e, 0) as idcc3, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_f_e as id_cliente_tipo");
        $this->db->from('uniones u');
        $this->db->join('uniones_clientes uc','uc.id_union=u.id');
        $this->db->join('perfilamiento p',"p.idperfilamiento=uc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('uc.id_cliente',$idc);
        $this->db->where('uc.activo',1);
        $this->db->where('u.activo',1);
        //$this->db->group_by('uc.id_union');

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("uc.*, uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
        ecoi.denominacion as cliente, IFNULL(ecoi.idtipo_cliente_e_c_o_i, 0) as idcc1, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_e_c_o_i as id_cliente_tipo");
        $this->db->from('uniones u');
        $this->db->join('uniones_clientes uc','uc.id_union=u.id');
        $this->db->join('perfilamiento p',"p.idperfilamiento=uc.id_perfilamiento");
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('uc.id_cliente',$idc);
        $this->db->where('uc.activo',1);
        $this->db->where('u.activo',1);
        //$this->db->group_by('uc.id_union');

        $query2 = $this->db->get_compiled_select(); 

        $this->db->select("uc.*, uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
        cf.denominacion as cliente, IFNULL(cf.idtipo_cliente_f, 0) as idcc2, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli,idtipo_cliente_f as id_cliente_tipo");
        $this->db->from('uniones u');
        $this->db->join('uniones_clientes uc','uc.id_union=u.id');
        $this->db->join('perfilamiento p',"p.idperfilamiento=uc.id_perfilamiento");
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('uc.id_cliente',$idc);
        $this->db->where('uc.activo',1);
        $this->db->where('u.activo',1);
        //$this->db->group_by('uc.id_union');

        $query3 = $this->db->get_compiled_select(); 

        $this->db->select("uc.*, uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
        concat(pfm.nombre,' ',pfm.apellido_paterno,' ',pfm.apellido_materno) as cliente,
        IFNULL(pfm.idtipo_cliente_p_f_m, 0) as idcc4, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_f_m as id_cliente_tipo");
        $this->db->from('uniones u');
        $this->db->join('uniones_clientes uc','uc.id_union=u.id');
        $this->db->join('perfilamiento p',"p.idperfilamiento=uc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('uc.id_cliente',$idc);
        $this->db->where('uc.activo',1);
        $this->db->where('u.activo',1);
        //$this->db->group_by('uc.id_union');
        
        $query4 = $this->db->get_compiled_select(); 

        $this->db->select("uc.*, uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente, 
        pmmd.nombre_persona as cliente, IFNULL(pmmd.idtipo_cliente_p_m_m_d, 0) as idcc5, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_m_m_d as id_cliente_tipo");
        $this->db->from('uniones u');
        $this->db->join('uniones_clientes uc','uc.id_union=u.id');
        $this->db->join('perfilamiento p',"p.idperfilamiento=uc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('uc.id_cliente',$idc);
        $this->db->where('uc.activo',1);
        $this->db->where('u.activo',1);
        //$this->db->group_by('uc.id_union');

        $query5 = $this->db->get_compiled_select(); 

        $this->db->select("uc.*, uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente, 
          pmme.razon_social as cliente, IFNULL(pmme.idtipo_cliente_p_m_m_e, 0) as idcc6, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_m_m_e as id_cliente_tipo");
        $this->db->from('uniones u');
        $this->db->join('uniones_clientes uc','uc.id_union=u.id');
        $this->db->join('perfilamiento p',"p.idperfilamiento=uc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('uc.id_cliente',$idc);
        $this->db->where('uc.activo',1);
        $this->db->where('u.activo',1);
        //$this->db->group_by('uc.id_union');

        $query6 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2." UNION ".$query3." UNION ".$query4." UNION ".$query5." UNION ".$query6;
        return $this->db->query($queryunions)->result();
    }

    public function lista_union_cli_vista($idc){
      $this->db->select("lcu.*, group_concat(cliente, '/') as cliente");
      $this->db->from('lista_cli_union lcu');
      $this->db->group_by('id_union');
      $this->db->where('id_cliente',$idc);
      $this->db->where('activo',1);
      $query = $this->db->get();
      return $query->result(); 
    }

    /*public function get_clientes_all($idc){
        $this->db->select('p.idcliente,p.idtipo_cliente');
        $this->db->from('perfilamiento p');
        $this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$idc);

        $query = $this->db->get();
        return $query->result();
    }*/

    public function get_fecha_trans($idc){
        $this->db->select('max(fecha_reg) as ultimo_reg');
        $this->db->from('perfilamiento p');
        $this->db->join('clientesc_transacciones ct','ct.id_perfilamiento=p.idperfilamiento');
        $this->db->where('id_clientec',$idc);

        $query = $this->db->get();
        return $query->result();
    }
    
    ////////////////////////////////
    public function get_beneficiario_cliente($id,$tipoc){//idperfilamiento
        $strq = "";
        if($tipoc==1){
            $strq = "SELECT tc.* FROM tipo_cliente_p_f_m  AS t
                     INNER JOIN tipo_cliente_p_f_m_beneficiario AS tc ON tc.idtipo_cliente_p_f_m = t.idtipo_cliente_p_f_m
                     WHERE tc.activo=1 AND t.idperfilamiento=$id";
        }else if($tipoc==2){
            $strq = "SELECT tc.* FROM tipo_cliente_p_f_e  AS t
                     INNER JOIN tipo_cliente_p_f_e_beneficiario AS tc ON tc.idtipo_cliente_p_f_m = t.idtipo_cliente_p_f_m
                     WHERE tc.activo=1 AND t.idperfilamiento=$id";
        }
        $query = $this->db->query($strq);
        return $query->result();
    }
  
  /*public function get_beneficiario_tansferenciaF($idc,$idp){ 
        $strq = "SELECT IFNULL(bf.id,'0') as idbf, IFNULL(concat(bf.nombre,' ',bf.apellido_paterno,' ',bf.apellido_materno),'0') as beneficiario
        FROM beneficiario_transaccion bt
        left join beneficiario_fisica bf on bf.id_beneficiario_fisica=bt.id
        WHERE bt.id_perfilamiento=$idp";
        $query = $this->db->query($strq);
        return $query->result();
  }*/

  public function get_beneficiario_f($id){ 
        $strq = "SELECT IFNULL(bf.id,'0') as idbf, IFNULL(concat(bf.nombre,' ',bf.apellido_paterno,' ',bf.apellido_materno),'0') as beneficiario, '1' as tipo_bene
        FROM beneficiario_fisica bf
        WHERE bf.id=$id and bf.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
  }

    public function get_beneficiario_tansferenciaF($idc,$idp){ 
        $strq = "SELECT IFNULL(bf.id,'0') as idbf, IFNULL(concat(bf.nombre,' ',bf.apellido_paterno,' ',bf.apellido_materno),'0') as beneficiario, '1' as tipo_bene
        FROM beneficiario_fisica bf
        WHERE bf.idtipo_cliente=$idp and activo=1 and bf.id_union=0";
        $query = $this->db->query($strq);
        return $query->result();
    }

  /*public function get_beneficiario_tansferenciaM($idc,$idp){ 
        $strq = "SELECT IFNULL(bmm.id,'0') as idbmm, IFNULL(bmm.razon_social,'0') as beneficiario,
        IFNULL(bmf.id,'0') as idbmf, IFNULL(concat(bmf.nombre,' ',bmf.apellido_paterno,' ',bmf.apellido_materno),'0') as beneficiario
        FROM beneficiario_transaccion bt
        left join beneficiario_moral bm on bm.id_bene_trans=bt.id
        left join beneficiario_moral_moral bmm on bmm.id_bene_moral=bm.id
        left join beneficiario_moral_fisica bmf on bmf.id_bene_moral=bm.id
        WHERE bt.id_perfilamiento=$idp";
        $query = $this->db->query($strq);
        return $query->result();
  }*/

  public function get_beneficiario_tansferenciaM($idc,$idp){ 
        $strq = "SELECT IFNULL(bmm.id,'0') as idbmm, IFNULL(bmm.id_bene_moral,'0') as idbmm_bm, IFNULL(bmm.razon_social,'0') as beneficiariom,
          IFNULL(bmf.id,'0') as idbmf, IFNULL(bmf.id_bene_moral,'0') as idbmf_bm, IFNULL(concat(bmf.nombre,' ',bmf.apellido_paterno,' ',bmf.apellido_materno),'0') as beneficiariof, '2' as tipo_bene
          FROM beneficiario_moral bm
          left join beneficiario_moral_moral bmm on bmm.id_bene_moral=bm.id
          left join beneficiario_moral_fisica bmf on bmf.id_bene_moral=bm.id
          WHERE bm.id_perfilamiento=$idp and bm.status=1 and bmm.id_union=0";
        $query = $this->db->query($strq);
        return $query->result();
  }

    public function get_beneficiario_m($id){ 
        $strq = "SELECT IFNULL(bmm.id,'0') as idbmm, IFNULL(bmm.id_bene_moral,'0') as idbmm_bm, IFNULL(bmm.razon_social,'0') as beneficiariom,
          IFNULL(bmm.id,'0') as idbmf, IFNULL(bmm.id_bene_moral,'0') as idbmf_bm, '2' as tipo_bene
          FROM beneficiario_moral bm
          left join beneficiario_moral_moral bmm on bmm.id_bene_moral=bm.id
          WHERE bmm.id=$id and bm.status=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

  public function get_beneficiario_tansferenciaFide($idc,$idp){ 
     $strq = "SELECT IFNULL(bf.id,'0') as idbf, IFNULL(bf.id_bene_moral,'0') as idbf_bm, IFNULL(bf.razon_social,'0') as beneficiario, '3' as tipo_bene
          FROM beneficiario_moral bm
          left join beneficiario_fideicomiso bf on bf.id_bene_moral=bm.id
          WHERE bm.id_perfilamiento=$idp and bm.status=1 and bf.id_union=0";
        $query = $this->db->query($strq);
        return $query->result();
  }

    public function get_beneficiario_fide($id){ 
     $strq = "SELECT IFNULL(bf.id,'0') as idbf, IFNULL(bf.id_bene_moral,'0') as idbf_bm, IFNULL(bf.razon_social,'0') as beneficiario, '3' as tipo_bene
          FROM beneficiario_moral bm
          left join beneficiario_fideicomiso bf on bf.id_bene_moral=bm.id
          WHERE bf.id=$id and bf.status=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_detalle_transac($idp,$idc,$tipo,$id_union){
        $this->db->select('ct.*, nombre as transaccion,bt.tipo_persona,bt.dueno_beneficiario, IFNULL(oc.id,"0") as idopera, oc.id_operacion,o.id_union, o.id as idopera_opera, o.fecha_reg as fecha_reg_opera');
        $this->db->from('operaciones o');
        $this->db->join("clientesc_transacciones ct", "ct.id=o.id_cliente_transac","left");
        //$this->db->join('clientes_operaciones co', 'co.id_cliente_transac=ct.id','left');
        $this->db->join('operacion_cliente oc', 'oc.id_perfilamiento=ct.id_perfilamiento');
        //$this->db->join('operaciones o', 'o.id=oc.id_operacion');
        $this->db->join('actividad_anexo aa','aa.id_actividad=ct.id_actividad',"left");
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo',"left");
        $this->db->join('beneficiario_transaccion bt','bt.id=ct.idtransbene',"left");
    
        if($tipo==1){
            $this->db->where('ct.id_clientec',$idc);
            $this->db->where('ct.id_perfilamiento',$idp);
            $this->db->where('o.id_union',0);
        }else{
            $this->db->where('o.id_union',$id_union);
        }
        if($tipo==1){
            $this->db->group_by('ct.id');
        }else{
            $this->db->group_by('o.id');
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    public function get_detalle_transac2($idp,$idc,$tipo,$id_union){
        $this->db->select('ct.*, nombre as transaccion,bt.tipo_persona,bt.dueno_beneficiario, IFNULL(oc.id,"0") as idopera, oc.id_operacion,o.id_union, o.id as idopera_opera, o.fecha_reg as fecha_reg_opera');
        $this->db->from('operaciones o');
        $this->db->join("clientesc_transacciones ct", "ct.id=o.id_cliente_transac","left");
        $this->db->join('operacion_cliente oc', 'oc.id_perfilamiento=ct.id_perfilamiento');
        //$this->db->join("clientesc_transacciones ct", "ct.id=o.id_cliente_transac","left");    
        $this->db->join('actividad_anexo aa','aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('beneficiario_transaccion bt','bt.id=ct.idtransbene');
        $this->db->where('o.id_union',$id_union);
        $this->db->group_by('o.id');

        $query = $this->db->get();
        return $query->result();
    }

    public function getBeneMoral($id){
      $this->db->select('bmm.razon_social as bene_moral');
      $this->db->from('beneficiario_moral_moral bmm');
      $this->db->join('beneficiario_moral bm','bm.id=bmm.id_bene_moral');
      $this->db->join('beneficiario_transaccion bt','bt.id=bm.id_bene_trans');
      $this->db->where('bt.id',$id);
      $query = $this->db->get();
      return $query->result();
    }

    public function getBeneMoral2($id){
      $this->db->select('bmm.razon_social as bene_moral');
      $this->db->from('beneficiario_transaccion bt');
      $this->db->join('beneficiario_moral_moral bmm','bmm.id=bt.id_beneficiario');
      $this->db->where('bt.id',$id);
      $query = $this->db->get();
      return $query->result();
    }

    public function getBeneFisica($id){
      $this->db->select("concat(nombre,' ',apellido_paterno,' ',apellido_materno) as bene_moral");
      $this->db->from('beneficiario_transaccion bt');
      $this->db->join('beneficiario_moral_fisica bmf','bmf.id=bt.id_beneficiario');
      $this->db->where('bt.id',$id);
      $query = $this->db->get();
      return $query->result();
    }

    public function getBitacoraXML($id){
      $this->db->select('btx.*,a.actividad,bt.id_actividad,ct.id_transaccion');
      $this->db->from('bitacora_xml btx');
      $this->db->join('clientesc_transacciones ct','ct.id=btx.id_clientec_transac');
      $this->db->join('beneficiario_transaccion bt','bt.id=btx.id_bene_transac');
      $this->db->join('actividad a','a.id=bt.id_actividad');
      $this->db->where('btx.id_perfilamiento',$id);
      $query = $this->db->get();
      return $query->result();
    }
    /// tabla de los beneficiario del cliente cliente
    /*
    public function get_c_c_bfi($params,$id){ 
        $strq = "SELECT IFNULL(bf.id,'0') as idbf, IFNULL(concat(bf.nombre,' ',bf.apellido_paterno,' ',bf.apellido_materno),'0') as beneficiario
          FROM beneficiario_transaccion bt
          left join beneficiario_fisica bf on bf.id_beneficiario_fisica=bt.id
          WHERE bt.id_perfilamiento=$idp";
          $query = $this->db->query($strq);
        return $query->result();
    }

    public function total_c_c_bfi($params,$id){ 
        $strq = "SELECT IFNULL(bmm.id,'0') as idbmm, IFNULL(bmm.razon_social,'0') as beneficiario,
          IFNULL(bmf.id,'0') as idbmf, IFNULL(concat(bmf.nombre,' ',bmf.apellido_paterno,' ',bmf.apellido_materno),'0') as beneficiario
          FROM beneficiario_transaccion bt
          left join beneficiario_moral bm on bm.id_bene_trans=bt.id
          left join beneficiario_moral_moral bmm on bmm.id_bene_moral=bm.id
          left join beneficiario_moral_fisica bmf on bmf.id_bene_moral=bm.id
          WHERE bt.id_perfilamiento=$idp";
          $query = $this->db->query($strq);
        return $query->result();
    }
    */

    /*public function get_benef_control($id){
      $this->db->select('bf.*,bmcd.*,bmf.*,bmm.*');
      $this->db->from('beneficiario_fisica bf');
      $this->db->join('beneficiario_moral bm', 'bm.id_perfilamiento=bf.idtipo_cliente','left');
      $this->db->join('beneficiario_moral_c_d bmcd', 'bmcd.id_bene_moral=bm.id','left');
      $this->db->join('beneficiario_moral_fisica bmf', 'bmf.id_bene_moral=bm.id','left');
      $this->db->join('beneficiario_moral_moral bmm', 'bmm.id_bene_moral=bm.id','left');
      $this->db->where('bf.idtipo_cliente',$id);
      $this->db->where('bf.activo',1);
      $this->db->where('bm.status',1);
      $query=$this->db->get();
      return $query;
    }*/

    public function get_benef_control_f($tabla,$where){
      $this->db->select('*, "f" as tabla');
      $this->db->from($tabla);
      $this->db->where($where);
      $query=$this->db->get();
      return $query->result();
    }

    public function get_benef_control_f2($tabla,$where){
      $this->db->select('*, "f" as tabla ');//.$value
      $this->db->from($tabla);
      $this->db->where($where);
      $this->db->where('id_union!=',0);
      $query=$this->db->get();
      return $query->result();
    }

    public function get_benef_control_mf($id,$join){
      $this->db->select("*, bm.id as idbm, 'm' as tabla");
      $this->db->from('beneficiario_moral bm');
      $this->db->join($join, $join.'.id_bene_moral=bm.id');
      //$this->db->join('beneficiario_moral_c_d bmcd', 'bmcd.id_bene_moral=bm.id');
      //$this->db->join('beneficiario_moral_moral bmm', 'bmm.id_bene_moral=bm.id');
      $this->db->where('bm.id_perfilamiento',$id);
      $this->db->where('bm.status',1);
      $this->db->where('id_union',0);
      $query=$this->db->get();
      return $query->result();
    }
    /*public function get_benef_control_mf($id,$join){
      $this->db->select("*, bf.id as idbm, 'm' as tabla");
      $this->db->from('beneficiario_fideicomiso bf');
      $this->db->where('bf.id_perfilamiento',$id);
      $this->db->where('bf.status',1);
      $this->db->where('id_union',0);
      $query=$this->db->get();
      return $query->result();
    }*/

    public function get_benef_control_mfUnion($id,$join,$value){
      $this->db->select("*, bm.id as idbm, 'm' as tabla, $value");
      $this->db->from('beneficiario_moral bm');
      $this->db->join($join, $join.'.id_bene_moral=bm.id');
      /*$this->db->join('beneficiario_moral_c_d bmcd', 'bmcd.id_bene_moral=bm.id');
      $this->db->join('beneficiario_moral_moral bmm', 'bmm.id_bene_moral=bm.id');*/
      $this->db->where('id_union',$id);
      $this->db->where('bm.status',1);
      $this->db->where('id_union!=',0);
      $query=$this->db->get();
      return $query->result();
    }

    function get_c_c_bfi($params){
        $columns = array( 
            0=>'bt.id',
            1=>'bf.nombre',
            2=>'bf.apellido_paterno',
            3=>'bf.apellido_materno',
            4=>'bt.tipo_persona',
            5=>'bm.tipo_persona as tpm',
            6=>'bmf.nombre as nom',
            7=>'bmf.apellido_paterno as nomp',
            8=>'bmf.apellido_materno as nomm',
            9=>'bmm.razon_social',
            10=>'bf.id as idf',
            11=>'bt.id_actividad',
            12=>'bt.id_perfilamiento',
            13=>'bt.id_cliente',
            14=>'bm.id as idm',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('beneficiario_transaccion bt');
        $this->db->join('beneficiario_fisica bf', 'bf.id_beneficiario_fisica=bt.id','left');
        $this->db->join('beneficiario_moral bm', 'bm.id_bene_trans=bt.id','left');
        $this->db->join('beneficiario_moral_moral bmm', 'bmm.id_bene_moral=bm.id','left');
        $this->db->join('beneficiario_moral_fisica bmf', 'bmf.id_bene_moral=bm.id','left');
        $this->db->where('bt.id_perfilamiento='.$idp);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_bfi($params,$idp){
        $columns = array( 
            0=>'bt.id',
            1=>'bf.nombre',
            2=>'bf.apellido_paterno',
            3=>'bf.apellido_materno',
            4=>'bt.tipo_persona',
            5=>'bm.tipo_persona as m',
            6=>'bmf.nombre as nom',
            7=>'bmf.apellido_paterno as nomp',
            8=>'bmf.apellido_materno as nomm',
            9=>'bmm.razon_social',
            10=>'bf.id as idf',
            11=>'bt.id_actividad',
            12=>'bt.id_perfilamiento',
            13=>'bt.id_cliente',
            14=>'bm.id as idm',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('beneficiario_transaccion bt');
        $this->db->join('beneficiario_fisica bf', 'bf.id_beneficiario_fisica=bt.id','left');
        $this->db->join('beneficiario_moral bm', 'bm.id_bene_trans=bt.id','left');
        $this->db->join('beneficiario_moral_moral bmm', 'bmm.id_bene_moral=bm.id','left');
        $this->db->join('beneficiario_moral_fisica bmf', 'bmf.id_bene_moral=bm.id','left');
        $this->db->where('bt.id_perfilamiento='.$idp);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /// Clientes clientes transaccion
    /*
        $this->db->select('ct.*, nombre as transaccion,bt.tipo_persona,bt.dueno_beneficiario');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('actividad_anexo aa','aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('beneficiario_transaccion bt','bt.id=ct.idtransbene');

        $this->db->where('ct.id_perfilamiento',$idp);
        $this->db->where('id_clientec',$idc);

        $this->db->select('dc.*,p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg');
        $this->db->from('perfilamiento p');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        //$this->db->join('clientesc_transacciones ct','ct.id_perfilamiento=p.idperfilamiento','left');
        $this->db->join('docs_cliente dc', 'dc.id_perfilamiento=p.idperfilamiento','left');
        $this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$idc);
    */
    /*function get_c_c_transaccion($params,$cliente){
        $columns = array( 
            0=>'a8.fecha_opera',
            1=>'bt.dueno_beneficiario',
            2=>'bt.tipo_persona',
            3=>'a.actividad'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          a8.fecha_opera, bt.dueno_beneficiario, bt.tipo_persona, a.actividad as transaccion, a8.id as ida8,
          SUM(pta8.monto) as total');
        $this->db->from('perfilamiento p');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('beneficiario_transaccion bt','bt.id_perfilamiento=p.idperfilamiento');
        $this->db->join('actividad a','a.id=bt.id_actividad');
        $this->db->join('actividad_anexo aa','aa.id_actividad=a.id');
        $this->db->join('anexo_8 a8','a8.id_cliente=bt.id_cliente');
        $this->db->join('pagos_transac_anexo_8 pta8','pta8.id_anexo_8=a8.id');
        $this->db->where('p.activo',1);
        $this->db->where('bt.activo',1);
        $this->db->where('p.idcliente='.$cliente);

        $month = $params['mes'];//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        $ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month, 1, $year));

        $this->db->where('a8.fecha_opera BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 00:00:00"');
        if($params['cliente']!=0){
         $this->db->where('p.idperfilamiento='.$params['cliente']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }*/

    /* CONSULTAS PARA BITACORAS POR TIPO DE ANEXO */
    // ANEXO 8 
    function get_c_c_transaccion($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_operacion',
            9=>'(SELECT totalpagos from (select a82.id,sum(pt8.monto)
            from anexo_8 a82
            join pagos_transac_anexo_8 pt8 on pt8.id_anexo_8=a82.id
            join clientesc_transacciones c_t on c_t.id_transaccion=a82.id
           where pt8.status=1 GROUP by a82.id) as lispagos WHERE lispagos.id=`a8`.`id`)',
            9=>'a8.monto_opera',
            10=>"a8.fecha_opera",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        /*(SELECT tot_pagoa from (select a8avi2.id,sum(pt8a.monto) as tot_pagoa
            from anexo_8_aviso a8avi2
            join pagos_transac_anexo_8_aviso pt8a on pt8a.id_anexo_8_aviso=a8avi2.id
            join clientesc_transacciones c_t_ on c_t_.id_transaccion=a8avi2.id
           where pt8a.status=1 GROUP by pt8a.id) as lispagos WHERE lispagos.id=`a8avi`.`id`) as tot_pagoa*/

        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a8avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a8.id as idA8, IFNULL(o.id,"0") as idopera,
          (SELECT totalpagos from (select a82.id,sum(pt8.monto) as totalPagos
            from anexo_8 a82
            join pagos_transac_anexo_8 pt8 on pt8.id_anexo_8=a82.id
            join clientesc_transacciones c_t on c_t.id_transaccion=a82.id
           where pt8.status=1 GROUP by a82.id) as lispagos WHERE lispagos.id=`a8`.`id`) as totalpagos,
           a8.monto_opera as totalPP,
           group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pta8.xml, pta8.id_pago_ayuda, pta8.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union,
                a8.fecha_opera as fecha_operacion, pta8.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf,"") as acuse_avi_pdf, aa.id_actividad, a8.id as id_anexo_tabla, a8avi.id as id_anexo_tabla_aviso, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf,"") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id','left');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        //$this->db->join('beneficiario_transaccion bt','bt.id=ct.idtransbene');
        //$this->db->join('bitacora_xml btx','btx.id_bene_transac=bt.id','left');
        $this->db->join('anexo_8 a8','a8.id=ct.id_transaccion');
        //$this->db->join('pagos_transac_anexo_8 pta8','pta8.id_anexo_8=a8.id');
        $this->db->join('anexo_8_aviso a8avi','a8avi.id_anexo8=a8.id','left');
        $this->db->join('acuses_generados acu',"acu.id_anexo_tabla=a8.id and acu.estatus=1",'left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav',"acuav.id_anexo_tabla=a8avi.id and acuav.estatus=1",'left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a8.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        $this->db->join('pagos_transac_anexo_8 pta8','pta8.id_anexo_8=a8.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente='.$cliente);
        $this->db->where('ct.id_actividad','11'); //ESPECIFICAMENTE PARA ANEXO 8 - ACT. VEHICULOS

        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by('a8.id');
        //$this->db->group_by('ct.idtransbene');
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a8.fecha_opera BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"'); //se comenta, bere pide por mes consultado vs mes acusado de transaccion

        $this->db->where('a8.anio_acuse',$params["anio"]);
        if($params["mes"]!="00"){
            $this->db->where('a8.mes_acuse',$params["mes"]);
        }
        //if($params['cliente']!=0)
        //var_dump($tipocliente[0]);die;
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_trasnsaccion($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_operacion',
            9=>'(SELECT totalpagos from (select a82.id,sum(pt8.monto) as totalPagos
            from anexo_8 a82
            join pagos_transac_anexo_8 pt8 on pt8.id_anexo_8=a82.id
            join clientesc_transacciones c_t on c_t.id_transaccion=a82.id
           where pt8.status=1 GROUP by a82.id) as lispagos WHERE lispagos.id=`a8`.`id`)',
           10=>"a8.fecha_opera",
           11=>"o.id",
            12=>'o.folio_cliente'
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id','left');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        //$this->db->join('beneficiario_transaccion bt','bt.id=ct.idtransbene');
        //$this->db->join('bitacora_xml btx','btx.id_bene_transac=bt.id','left');
        $this->db->join('anexo_8 a8','a8.id=ct.id_transaccion');
        //$this->db->join('pagos_transac_anexo_8 pta8','pta8.id_anexo_8=a8.id');
        $this->db->join('pagos_transac_anexo_8 pta8','pta8.id_anexo_8=a8.id','left');
        $this->db->join('anexo_8_aviso a8avi','a8avi.id_anexo8=a8.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente='.$cliente);
        $this->db->where('ct.id_actividad','11'); //ESPECIFICAMENTE PARA ANEXO 8 - ACT. VEHICULOS
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by('a8.id');
        //$this->db->group_by('ct.idtransbene');

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        //$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        //$primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        //$this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');

        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a8.fecha_opera BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        $this->db->where('a8.anio_acuse',$params["anio"]);
        if($params["mes"]!="00"){
            $this->db->where('a8.mes_acuse',$params["mes"]);
        }
        
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 2a Anexo 2A - Emisión o comercialización de tarjetas prepagadas o el abono a instrumentos de almacenamiento de valor monetario.
    function get_c_c_anexo2a($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a2.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a2avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a2.id as idA2a, IFNULL(o.id,"0") as idopera,
          a2.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pta2a.xml, pta2a.id_pago_ayuda, pta2a.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a2.fecha_operacion, pta2a.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a2.id as id_anexo_tabla, a2avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id','left');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo2a a2','a2.id=ct.id_transaccion');
        $this->db->join('anexo2a_aviso a2avi','a2avi.idanexo2a=a2.id','left');
        $this->db->join('pagos_transac_anexo2a pta2a','pta2a.idanexo2a=a2.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a2.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a2avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a2.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','2'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a2.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a2.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        $this->db->where('a2.anio_acuse',$params["anio"]);
        if($params["mes"]!="00"){
            $this->db->where('a2.mes_acuse',$params["mes"]);
        }
        
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo2a($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a2.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id','left');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo2a a2','a2.id=ct.id_transaccion');
        $this->db->join('anexo2a_aviso a2avi','a2avi.idanexo2a=a2.id','left');
        $this->db->join('pagos_transac_anexo2a pta2a','pta2a.idanexo2a=a2.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','2'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE

        if($where!=""){
           $this->db->where($where); 
        }

        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a2.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a2.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a2.mes_acuse',$params["mes"]);
        }
        $this->db->where('a2.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
            $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
            $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // Anexo 2B - Emisión o comercialización de tarjetas prepagadas.
    function get_c_c_anexo2b($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a2avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a2.id as idA2b, IFNULL(o.id,"0") as idopera,
          a2.monto_opera as totalpagos, 
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pta2b.xml, pta2b.id_pago_ayuda, pta2b.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a2.fecha_operacion, pta2b.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a2.id as id_anexo_tabla, a2avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi24, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo2b a2','a2.id=ct.id_transaccion');
        $this->db->join('anexo2b_aviso a2avi','a2avi.idanexo2b=a2.id','left');
        $this->db->join('pagos_transac_anexo2b pta2b','pta2b.idanexo2b=a2.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a2.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a2avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a2.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','3'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a2.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a2.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a2.mes_acuse',$params["mes"]);
        }
        $this->db->where('a2.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
            $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
            $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_anexo2b($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a2.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo2b a2','a2.id=ct.id_transaccion');
        $this->db->join('anexo2b_aviso a2avi','a2avi.idanexo2b=a2.id','left');
        $this->db->join('pagos_transac_anexo2b pta2b','pta2b.idanexo2b=a2.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','3'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a2.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a2.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a2.mes_acuse',$params["mes"]);
        }
        $this->db->where('a2.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
            $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
            $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    //Anexo 1
    function get_c_c_anexo1($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array(
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a1.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(an1avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, an1.id as idA1, IFNULL(o.id,"0") as idopera,
          an1.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente,
        pta1.xml, pta1.id_pago_ayuda, pta1.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, an1.fecha_operacion, pta1.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, an1.id as id_anexo_tabla, an1avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo1 an1','an1.id=ct.id_transaccion');
        $this->db->join('anexo1_aviso an1avi','an1avi.idanexo1=an1.id','left');
        $this->db->join('pagos_transac_anexo1 pta1','pta1.idanexo1=an1.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=an1.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=an1avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=an1.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','1'); //esta linea cambia dependiendo de la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an1.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an1.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an1.mes_acuse',$params["mes"]);
        }
        $this->db->where('an1.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
            $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
            $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_anexo1($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"an1.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo1 an1','an1.id=ct.id_transaccion');
        $this->db->join('anexo1_aviso an1avi','an1avi.idanexo1=an1.id','left');
        $this->db->join('pagos_transac_anexo1 pta1','pta1.idanexo1=an1.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','1'); //Esta linea cambia dependiendo la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an1.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an1.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an1.mes_acuse',$params["mes"]);
        }
        $this->db->where('an1.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
            $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
            $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // Anexo 11 - Servicios Profesionales.
    function get_c_c_anexo11($params,$cliente,$where){ //FALTA ACUSE Y XML
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a11.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a11avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a11.id as idA11, IFNULL(o.id,"0") as idopera,
          a11.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, a11.acusado, a11.xml, a11.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a11.fecha_operacion, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, IFNULL(acuav.id,0) as id_acuse_avi, IFNULL(acuav.acuse,"") as acuse_aviso, IFNULL(acuav.acuse_pdf,"") as acuse_avi_pdf, aa.id_actividad, a11.id as id_anexo_tabla, a11avi.id as id_anexo_tabla_aviso, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo11 a11','a11.id=ct.id_transaccion');
        $this->db->join('anexo11_aviso a11avi','a11avi.idanexo11=a11.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a11.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a11avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a11.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','14'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a11.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a11.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a11.mes_acuse',$params["mes"]);
        }
        $this->db->where('a11.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo11($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a11.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo11 a11','a11.id=ct.id_transaccion');
        $this->db->join('anexo11_aviso a11avi','a11avi.idanexo11=a11.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','14'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a11.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a11.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a11.mes_acuse',$params["mes"]);
        }
        $this->db->where('a11.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    //=================
    // Anexo 12 A - Fedatarios públicos
    function get_c_c_anexo12a($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a12.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a12avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a12.id as idA12a, IFNULL(o.id,"0") as idopera,
          a12.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, a12.xml, a12.id_pago_ayuda, a12.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a12.fecha_operacion, a12.id_pago_ayude, a12.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a12.id as id_anexo_tabla, a12avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo12a a12','a12.id=ct.id_transaccion');
        $this->db->join('anexo12a_aviso a12avi','a12avi.idanexo12a=a12.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a12.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a12avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a12.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','15'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        //$this->db->where('a12.id_pago_ayude','0');
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a12.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a12.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a12.mes_acuse',$params["mes"]);
        }
        $this->db->where('a12.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo12a($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a12.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo12a a12','a12.id=ct.id_transaccion');
        $this->db->join('anexo12a_aviso a12avi','a12avi.idanexo12a=a12.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        ////$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','15'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a12.id");
        
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a12.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a12.mes_acuse',$params["mes"]);
        }
        $this->db->where('a12.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // Anexo 12 B - Servidores públicos
    function get_c_c_anexo12b($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a12.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a12avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a12.id as idA12b, IFNULL(o.id,"0") as idopera,
          a12.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, a12.xml, a12.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a12.fecha_operacion, a12.id_pago_ayuda, a12.id_pago_ayude, a12.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a12.id as id_anexo_tabla, a12avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo12b a12','a12.id=ct.id_transaccion');
        $this->db->join('anexo12b_aviso a12avi','a12avi.idanexo12b=a12.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a12.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a12avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a12.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','16'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a12.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');        
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a12.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a12.mes_acuse',$params["mes"]);
        }
        $this->db->where('a12.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo12b($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a12.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo12b a12','a12.id=ct.id_transaccion');
        $this->db->join('anexo12b_aviso a12avi','a12avi.idanexo12b=a12.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','16'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a12.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a12.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a12.mes_acuse',$params["mes"]);
        }
        $this->db->where('a12.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    //=================
    function get_c_c_anexo16($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array(
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"an16.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(an16avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, an16.id as idA16, IFNULL(o.id,"0") as idopera,
          an16.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pt16.xml, pt16.id_pago_ayuda, pt16.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, an16.fecha_operacion, pt16.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, an16.id as id_anexo_tabla, an16avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi24, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo16 an16','an16.id=ct.id_transaccion');
        $this->db->join('anexo16_aviso an16avi','an16avi.idanexo16 = an16.id','left');
        $this->db->join('completar_anexo16 pt16','pt16.idanexo16 = an16.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=an16.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=an16avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=an16.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','20'); //esta linea cambia dependiendo de la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an16.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an16.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an16.mes_acuse',$params["mes"]);
        }
        $this->db->where('an16.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_anexo16($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"an16.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo16 an16','an16.id=ct.id_transaccion');
        $this->db->join('anexo16_aviso an16avi','an16avi.idanexo16=an16.id','left');
        $this->db->join('completar_anexo16 pt16','pt16.idanexo16 = an16.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','20'); //Esta linea cambia dependiendo la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an16.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an16.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an16.mes_acuse',$params["mes"]);
        }
        $this->db->where('an16.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    //Anexo 7
    function get_c_c_anexo7($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array(
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"an7.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(an7avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, an7.id as idA7, IFNULL(o.id,"0") as idopera,
          an7.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pt7.xml, pt7.id_pago_ayuda, pt7.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, an7.fecha_operacion, pt7.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, an7.id as id_anexo_tabla, an7avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi24, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo7 an7','an7.id=ct.id_transaccion');
        $this->db->join('anexo7_aviso an7avi','an7avi.idanexo7=an7.id','left');
        $this->db->join('pagos_transac_anexo_7 pt7','pt7.idanexo7=an7.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=an7.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=an7avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=an7.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','10'); //esta linea cambia dependiendo de la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an7.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an7.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an7.mes_acuse',$params["mes"]);
        }
        $this->db->where('an7.anio_acuse',$params["anio"]);
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_anexo7($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"an7.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo7 an7','an7.id=ct.id_transaccion');
        $this->db->join('anexo7_aviso an7avi','an7avi.idanexo7=an7.id','left');
        $this->db->join('pagos_transac_anexo_7 pt7','pt7.idanexo7=an7.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','10'); //Esta linea cambia dependiendo la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an7.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        /*$day = date("d", mktime(0,0,0, $month+1, 0, $year));
        $ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/

        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an7.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an7.mes_acuse',$params["mes"]);
        }
        $this->db->where('an7.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // Anexo 2C - Modederos y tarjetas de devoluciones y recompensas.
    function get_c_c_anexo2c($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a2.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a2avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a2.id as idA2c, IFNULL(o.id,"0") as idopera,
          a2.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pta2c.xml, pta2c.id_pago_ayuda, pta2c.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a2.fecha_operacion, pta2c.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a2.id as id_anexo_tabla, a2avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi24, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo2c a2','a2.id=ct.id_transaccion');
        $this->db->join('anexo2c_aviso a2avi','a2avi.idanexo2c=a2.id','left');
        $this->db->join('pagos_transac_anexo2c pta2c','pta2c.idanexo2c=a2.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a2.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a2avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a2.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','4'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by("o.id");
        //$this->db->group_by("ct.id_transaccion");
        $this->db->group_by("a2.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/

        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a2.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a2.mes_acuse',$params["mes"]);
        }
        $this->db->where('a2.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_anexo2c($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a2.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo2c a2','a2.id=ct.id_transaccion');
        $this->db->join('anexo2c_aviso a2avi','a2avi.idanexo2c=a2.id','left');
        $this->db->join('pagos_transac_anexo2c pta2c','pta2c.idanexo2c=a2.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','4'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a2.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/

        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a2.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a2.mes_acuse',$params["mes"]);
        }
        $this->db->where('a2.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 3 blindage
    function get_c_c_anexo3($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a3.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a3avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a3.id as idA3, IFNULL(o.id,"0") as idopera,
          a3.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pta3.xml, pta3.id_pago_ayuda, pta3.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a3.fecha_operacion, pta3.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a3.id as id_anexo_tabla, a3avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo3 a3','a3.id=ct.id_transaccion');
        $this->db->join('anexo3_aviso a3avi','a3avi.idanexo3=a3.id','left');
        $this->db->join('pagos_transac_anexo3 pta3','pta3.idanexo3=a3.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a3.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a3avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a3.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','5'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a3.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a3.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a3.mes_acuse',$params["mes"]);
        }
        $this->db->where('a3.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo3($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a3.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo3 a3','a3.id=ct.id_transaccion');
        $this->db->join('anexo3_aviso a3avi','a3avi.idanexo3=a3.id','left');
        $this->db->join('pagos_transac_anexo3 pta3','pta3.idanexo3=a3.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','5'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a3.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a3.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a3.mes_acuse',$params["mes"]);
        }
        $this->db->where('a3.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    //////////////////////////////////////////////////////////
    // ANEXO 4
    function get_c_c_anexo4($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a4.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a4avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a4.id as idA4, IFNULL(o.id,"0") as idopera,
          a4.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pta4.xml, pta4.id_pago_ayuda, pta4.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a4.fecha_operacion, pta4.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a4.id as id_anexo_tabla, a4avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24,  IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo4 a4','a4.id=ct.id_transaccion');
        $this->db->join('anexo4_aviso a4avi','a4avi.idanexo4=a4.id','left');
        $this->db->join('pagos_transac_anexo4 pta4','pta4.idanexo4=a4.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a4.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a4avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a4.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','6'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a4.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a4.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a4.mes_acuse',$params["mes"]);
        }
        $this->db->where('a4.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_anexo4($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a4.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo4 a4','a4.id=ct.id_transaccion');
        $this->db->join('anexo4_aviso a4avi','a4avi.idanexo4=a4.id','left');
        $this->db->join('pagos_transac_anexo4 pta4','pta4.idanexo4=a4.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','6'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }

        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a4.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a4.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a4.mes_acuse',$params["mes"]);
        }
        $this->db->where('a4.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 5A
    // ANEXO 4
    function get_c_c_anexo5a($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a5.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a5avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a5.id as idA5a, IFNULL(o.id,"0") as idopera,
          a5.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pta5a.xml, pta5a.id_pago_ayuda, pta5a.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a5.fecha_operacion, pta5a.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a5.id as id_anexo_tabla, a5avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo5a a5','a5.id=ct.id_transaccion');
        $this->db->join('anexo5a_aviso a5avi','a5avi.idanexo5a=a5.id','left');
        $this->db->join('pagos_transac_anexo5a pta5a','pta5a.idanexo5a=a5.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a5.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a5avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a5.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','7'); //ESPECIFICAMENTE PARA ANEXO 
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a5.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a5.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a5.mes_acuse',$params["mes"]);
        }
        $this->db->where('a5.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo5a($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a5.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo5a a5','a5.id=ct.id_transaccion');
        $this->db->join('anexo5a_aviso a5avi','a5avi.idanexo5a=a5.id','left');
        $this->db->join('pagos_transac_anexo5a pta5a','pta5a.idanexo5a=a5.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','7'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a5.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a5.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a5.mes_acuse',$params["mes"]);
        }
        $this->db->where('a5.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if(!empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 6
    function get_c_c_anexo6($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a6.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a6avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a6.id as idA6, IFNULL(o.id,"0") as idopera,
          a6.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pt6.xml, pt6.id_pago_ayuda, pt6.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a6.fecha_operacion, pt6.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a6.id as id_anexo_tabla, a6avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo6 a6','a6.id=ct.id_transaccion');
        $this->db->join('anexo6_aviso a6avi','a6avi.idanexo6=a6.id','left');
        $this->db->join('pagos_transac_anexo6_pago pt6','pt6.idanexo6=a6.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a6.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a6avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a6.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','9'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a6.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a6.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a6.mes_acuse',$params["mes"]);
        }
        $this->db->where('a6.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo6($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a6.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo6 a6','a6.id=ct.id_transaccion');
        $this->db->join('anexo6_aviso a6avi','a6avi.idanexo6=a6.id','left');
        $this->db->join('pagos_transac_anexo6_pago pt6','pt6.idanexo6=a6.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','9'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a6.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a6.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a6.mes_acuse',$params["mes"]);
        }
        $this->db->where('a6.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 9 blindage
    function get_c_c_anexo9($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a9.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a9avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a9.id as idA9, IFNULL(o.id,"0") as idopera,
          a9.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pt9.xml, pt9.id_pago_ayuda, pt9.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a9.fecha_operacion, pt9.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a9.id as id_anexo_tabla, a9avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo9 a9','a9.id=ct.id_transaccion');
        $this->db->join('anexo9_aviso a9avi','a9avi.idanexo9=a9.id','left');
        $this->db->join('pagos_transac_anexo9 pt9','pt9.idanexo9=a9.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a9.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a9avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a9.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','12'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a9.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a9.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a9.mes_acuse',$params["mes"]);
        }
        $this->db->where('a9.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo9($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a9.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo9 a9','a9.id=ct.id_transaccion');
        $this->db->join('anexo9_aviso a9avi','a9avi.idanexo9=a9.id','left');
        $this->db->join('pagos_transac_anexo9 pt9','pt9.idanexo9=a9.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','12'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a9.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a9.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a9.mes_acuse',$params["mes"]);
        }
        $this->db->where('a9.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 13
    function get_c_c_donativos13($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a13.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a13avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a13.id as idA13, IFNULL(o.id,"0") as idopera,
          a13.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pt13.xml, pt13.id_pago_ayuda, pt13.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a13.fecha_operacion, pt13.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a13.id as id_anexo_tabla, a13avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo13 a13','a13.id=ct.id_transaccion');
        $this->db->join('anexo13_aviso a13avi','a13avi.idanexo13=a13.id','left');
        $this->db->join('pagos_transac_anexo13_num pt13','pt13.idanexo13=a13.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a13.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a13avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a13.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','17'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a13.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a13.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a13.mes_acuse',$params["mes"]);
        }
        $this->db->where('a13.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_donativos13($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a13.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo13 a13','a13.id=ct.id_transaccion');
        $this->db->join('anexo13_aviso a13avi','a13avi.idanexo13=a13.id','left');
        $this->db->join('pagos_transac_anexo13_num pt13','pt13.idanexo13=a13.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','17'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a13.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a13.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a13.mes_acuse',$params["mes"]);
        }
        $this->db->where('a13.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 15
    function get_c_c_transaccionA15($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a15.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a15avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a15.id as idA15, IFNULL(o.id,"0") as idopera,
          a15.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pt15.xml, pt15.id_pago_ayuda, pt15.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, a15.fecha_operacion, pt15.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a15.id as id_anexo_tabla, a15avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acuav.acuse_pdf, "") as acuse_aviso, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo_15 a15','a15.id=ct.id_transaccion');
        $this->db->join('anexo_15_aviso a15avi','a15avi.id_anexo15=a15.id','left');
        $this->db->join('pagos_transac_anexo_15 pt15','pt15.id_anexo_15=a15.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a15.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a15avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a15.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','19'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a15.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a15.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a15.mes_acuse',$params["mes"]);
        }
        $this->db->where('a15.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_trasnsaccionA15($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a15.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo_15 a15','a15.id=ct.id_transaccion');
        $this->db->join('anexo_15_aviso a15avi','a15avi.id_anexo15=a15.id','left');
        $this->db->join('pagos_transac_anexo_15 pt15','pt15.id_anexo_15=a15.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','19'); //ESPECIFICAMENTE PARA ANEXO 15 - ACT. INMUEBLE
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a15.id");
        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a15.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a15.mes_acuse',$params["mes"]);
        }
        $this->db->where('a15.anio_acuse',$params["anio"]);
        
        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    // ANEXO 15 TERMINA

    /* ************************************************* */
    /*function get_operaciones($params){
        $columns = array( 
            0=>'co.id',
            1=>'co.fecha_reg',
            2=>'ecoi.denominacion',
            3=>'cf.denominacion',
            4=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            5=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            6=>'pmmd.nombre_persona',
            7=>'pmme.razon_social',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('co.*,co.id as idopera,p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,');
        $this->db->from('clientes_operaciones co');
        $this->db->join('perfilamiento p','p.idperfilamiento=co.id_perfilamiento');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$this->session->userdata('idcliente'));
        $this->db->where('co.activo',1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function get_total_operaciones($params){
        $columns = array( 
            0=>'co.id',
            1=>'co.fecha_reg',
            2=>'ecoi.denominacion',
            3=>'cf.denominacion',
            4=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            5=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            6=>'pmmd.nombre_persona',
            7=>'pmme.razon_social',
        );

        $this->db->select('COUNT(1)');
        $this->db->from('clientes_operaciones co');
        $this->db->join('perfilamiento p','p.idperfilamiento=co.id_perfilamiento');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$this->session->userdata('idcliente'));
        $this->db->where('co.activo',1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }*/
    //SE MODIFICAN LAS FUNCIONES PARA LA NUEVA ESTRUCTURA DE MULTIPLES CLIENTES

    public function get_operaciones($idc){
      $this->db->select("oc.*, o.id as idopera, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
          concat(pfe.nombre,' ',pfe.apellido_paterno,' ',pfe.apellido_materno) as cliente, 
          IFNULL(pfe.idtipo_cliente_p_f_e, 0) as idcc3, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_f_e as id_cliente_tipo");
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        $this->db->join('perfilamiento p',"p.idperfilamiento=oc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento');

        //$this->db->where('oc.id_cliente',$idc);
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("oc.*, o.id as idopera, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
        ecoi.denominacion as cliente, IFNULL(ecoi.idtipo_cliente_e_c_o_i, 0) as idcc1, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_e_c_o_i as id_cliente_tipo");
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        $this->db->join('perfilamiento p',"p.idperfilamiento=oc.id_perfilamiento");
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento');

        //$this->db->where('oc.id_cliente',$idc);
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);

        $query2 = $this->db->get_compiled_select(); 

        $this->db->select("oc.*, o.id as idopera, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
        cf.denominacion as cliente, IFNULL(cf.idtipo_cliente_f, 0) as idcc2, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli,idtipo_cliente_f as id_cliente_tipo");
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        $this->db->join('perfilamiento p',"p.idperfilamiento=oc.id_perfilamiento");
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento');

        //$this->db->where('oc.id_cliente',$idc);
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);

        $query3 = $this->db->get_compiled_select(); 

        $this->db->select("oc.*, o.id as idopera, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente,
        concat(pfm.nombre,' ',pfm.apellido_paterno,' ',pfm.apellido_materno) as cliente,
        IFNULL(pfm.idtipo_cliente_p_f_m, 0) as idcc4, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_f_m as id_cliente_tipo");
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        $this->db->join('perfilamiento p',"p.idperfilamiento=oc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento');

        //$this->db->where('oc.id_cliente',$idc);
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);
        
        $query4 = $this->db->get_compiled_select(); 

        $this->db->select("oc.*, o.id as idopera, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente, 
        pmmd.nombre_persona as cliente, IFNULL(pmmd.idtipo_cliente_p_m_m_d, 0) as idcc5, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_m_m_d as id_cliente_tipo");
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        $this->db->join('perfilamiento p',"p.idperfilamiento=oc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento');

        //$this->db->where('oc.id_cliente',$idc);
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);

        $query5 = $this->db->get_compiled_select(); 

        $this->db->select("oc.*, o.id as idopera, p.idperfilamiento as idperfilamientop, p.idcliente, p.idtipo_cliente, 
          pmme.razon_social as cliente, IFNULL(pmme.idtipo_cliente_p_m_m_e, 0) as idcc6, (select max(fecha_reg) as ultimo_reg from clientesc_transacciones where id_perfilamiento=p.idperfilamiento) as ultimo_reg, '1' as tipo_cli, idtipo_cliente_p_m_m_e as id_cliente_tipo");
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        $this->db->join('perfilamiento p',"p.idperfilamiento=oc.id_perfilamiento");
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento');

        //$this->db->where('oc.id_cliente',$idc);
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);

        $query6 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2." UNION ".$query3." UNION ".$query4." UNION ".$query5." UNION ".$query6;
        return $this->db->query($queryunions)->result();
    }

    public function operaciones_vista($idc){
      $this->db->select("lo.*,DATE_FORMAT(lo.fecha_reg,  '%d / %m / %Y %r') AS fecha_reg , group_concat(DISTINCT cliente, ' /') as cliente, ag.nombre as anexo, IFNULL(id_cliente_transac,'0') as id_cliente_transac, IFNULL(bt.id_clientec_transac,'0') as id_ct_bt, IFNULL(ob.id,'N') as idob, IFNULL(gr.grado,'0') as grado, IFNULL(aop.id,'0') as id_autorizado, IFNULL(ct.id_actividad,'0') as id_actividad, IFNULL(gr.transaccionalidad_forma,'0') as transaccionalidad_forma, IFNULL(ct.id_transaccion,'0') as id_transaccion, o.folio_cliente, o.id_union");
      $this->db->from('lista_operaciones lo');
      $this->db->join("operaciones o", "o.id=lo.id_operacion");
      //$this->db->join("clientesc_transacciones ct", "ct.id_perfilamiento=lo.id_perfilamiento","left");
      $this->db->join("clientesc_transacciones ct", "ct.id=o.id_cliente_transac","left");
      $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad',"left");
      $this->db->join('anexos_grales ag','ag.id=aa.id_anexo',"left");
      $this->db->join('bitacora_xml bt','bt.id_clientec_transac=o.id_cliente_transac',"left");
      $this->db->join('operacion_beneficiario ob','ob.id_operacion=o.id',"left");
      $this->db->join("grado_riesgo_actividad gr", "gr.id_operacion=o.id","left");
      $this->db->join("autoriza_operacion aop", "aop.id_operacion=o.id","left");
      $this->db->group_by('lo.id_operacion');
      $this->db->where('lo.id_cliente',$idc);
      $this->db->where('o.activo',1);

      $query = $this->db->get();
      return $query->result(); 
    }

    public function get_folio_cli($idc){
      $this->db->select("o.folio_cliente");
      $this->db->from('operaciones o');
      $this->db->where('activo',1);
      $this->db->where('id_cliente',$idc);
      //$this->db->where('folio_cliente!=',0);
      $this->db->order_by('id',"desc");
      $this->db->limit('1');

      $query = $this->db->get();
      return $query->row(); 
    }

    /*function get_operaciones($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.fecha_reg',
            2=>'ecoi.denominacion',
            3=>'cf.denominacion',
            4=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            5=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            6=>'pmmd.nombre_persona',
            7=>'pmme.razon_social'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('o.*,o.id as idopera,p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, ecoi.denominacion as cliente1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, cf.denominacion as cliente1,
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno) as cliente1,
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, concat(pfm.nombre," ", pfm.apellido_paterno," ",pfm.apellido_materno) as cliente1,
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5, pmmd.nombre_persona as cliente1,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6, pmme.razon_social as cliente1,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2');
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        //$this->db->join("operacion_beneficiario ob", "ob.id_operacion=o.id and ob.id_perfilamiento=oc.id_perfilamiento","left");
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$this->session->userdata('idcliente'));
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);
        //$this->db->group_by('o.id');
        //$this->db->where('ob.activo',1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }*/

    /*public function get_total_operaciones($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.fecha_reg',
            2=>'ecoi.denominacion',
            3=>'cf.denominacion',
            4=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            5=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            6=>'pmmd.nombre_persona',
            7=>'pmme.razon_social',
        );

        $this->db->select('COUNT(1)');
        $this->db->from('operaciones o');
        $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
        //$this->db->join("operacion_beneficiario ob", "ob.id_operacion=o.id and ob.id_perfilamiento=oc.id_perfilamiento","left");
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$this->session->userdata('idcliente'));
        $this->db->where('o.activo',1);
        $this->db->where('oc.activo',1);
        //$this->db->where('ob.activo',1);
        $this->db->group_by('o.id');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }*/
    /* ******************************************** */
     // ******** ANEXO 5B -- ACTIVIDAD 8
    function get_c_c_anexo5b($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a5.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(a5bvi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, a5.id as idA5b, IFNULL(o.id,"0") as idopera,
          a5.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, a5.xml, a5.id_pago_ayuda, a5.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse,o.id_union, a5.fecha_operacion, a5.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, a5.id as id_anexo_tabla, a5bvi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi24, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo5b a5','a5.id=ct.id_transaccion');
        $this->db->join('anexo5b_aviso a5bvi','a5bvi.id_anexo5b=a5.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=a5.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=a5bvi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a5.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','8'); //ESPECIFICAMENTE PARA ANEXO 5B
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a5.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a5.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a5.mes_acuse',$params["mes"]);
        }
        $this->db->where('a5.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_c_c_anexo5b($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"a5.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo5b a5','a5.id=ct.id_transaccion');
        $this->db->join('anexo5b_aviso a5bvi','a5bvi.id_anexo5b=a5.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');

        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','8'); //ESPECIFICAMENTE PARA ANEXO 5b
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("a5.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('a5.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('a5.mes_acuse',$params["mes"]);
        }
        $this->db->where('a5.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }

    function get_c_c_anexo10($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array(
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"an10.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        
        $this->db->select('p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
          IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
          IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
          IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
          IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
          IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
          IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
          IFNULL(an10avi.id,0) as idaviso,
          pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
          ecoi.denominacion as denominacion2,
          ct.*, ag.nombre as transaccion, ct.id as idctrans, ct.fecha_reg, an10.id as idA10, IFNULL(o.id,"0") as idopera,
          an10.monto_opera as totalpagos,
          group_concat(DISTINCT IFNULL(pfm.nombre,"")," ",IFNULL(pfm.apellido_paterno,"")," ",IFNULL(pfm.apellido_materno,""), 
            IFNULL(pfe.nombre,"")," ",IFNULL(pfe.apellido_paterno,"")," ",IFNULL(pfe.apellido_materno,""),
            IFNULL(pmme.razon_social,""),
            IFNULL(pmmd.nombre_persona,""),
            IFNULL(ecoi.denominacion,""),
            IFNULL(cf.denominacion,""),
        "/") as cliente, pt10.xml, pt10.id_pago_ayuda, pt10.id as id_pago_normal, hpb.resultado, hpb.id_perfilamiento as idpacuse, o.id_union, an10.fecha_operacion, pt10.acusado, o.folio_cliente, IFNULL(acu.id,0) as id_acuse, IFNULL(acu.acuse,"") as acuse, IFNULL(acu.acuse_pdf,"") as acuse_pdf, aa.id_actividad, an10.id as id_anexo_tabla, an10avi.id as id_anexo_tabla_aviso, IFNULL(acuav.id, 0) as id_acuse_avi, IFNULL(acuav.acuse, "") as acuse_aviso, IFNULL(acuav.acuse_pdf, "") as acuse_avi_pdf, IFNULL(acu24.id, 0) as id_acuse_24, IFNULL(acu24.acuse, "") as acuse_24, IFNULL(acu24.acuse_pdf, "") as acuse24_pdf');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');
        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo10 an10','an10.id=ct.id_transaccion');
        $this->db->join('anexo10_aviso an10avi','an10avi.idanexo10=an10.id','left');
        $this->db->join('pagos_transac_anexo10 pt10','pt10.idanexo10=an10.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        $this->db->join('acuses_generados acu','acu.id_anexo_tabla=an10.id and acu.estatus=1','left'); //REPOSITORIO ACUSES SAT
        $this->db->join('acuses_avisos acuav','acuav.id_anexo_tabla=an10avi.id and acuav.estatus=1','left'); //REPOSITORIO ACUSES MODIFICATORIOS SAT
        $this->db->join('acuses_24hrs acu24',"acu24.id_anexo_tabla=a10.id and acu24.estatus=1",'left'); //REPOSITORIO ACUSES 24HRS SAT
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','13'); //esta linea cambia dependiendo de la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an10.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an10.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an10.mes_acuse',$params["mes"]);
        }
        $this->db->where('an10.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_c_c_anexo10($params,$cliente,$where){
        $tipocliente=explode("-",$params['cliente']);
        $columns = array( 
            0=>'ct.id',
            1=>'ecoi.denominacion',
            2=>'cf.denominacion',
            3=>'concat(pfe.nombre," ",pfe.apellido_paterno," ",pfe.apellido_materno)',
            4=>'concat(pfm.nombre," ",pfm.apellido_paterno," ",pfm.apellido_materno)',
            5=>'pmmd.nombre_persona',
            6=>'pmme.razon_social',
            7=>'ag.nombre',
            //8=>'fecha_reg',
            //9=>'monto_opera',
            10=>"an10.fecha_operacion",
            11=>"o.id",
            12=>"o.folio_cliente"
        );

        $this->db->select('count(1)');
        $this->db->from('clientesc_transacciones ct');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id','left');
        $this->db->join('perfilamiento p','p.idperfilamiento=oc.id_perfilamiento','left');
        $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
        $this->db->join('actividad_anexo aa', 'aa.id_actividad=ct.id_actividad');

        $this->db->join('anexos_grales ag','ag.id=aa.id_anexo');
        $this->db->join('anexo10 an10','an10.id=ct.id_transaccion');
        $this->db->join('anexo10_aviso an10avi','an10avi.idanexo10=an10.id','left');
        $this->db->join('pagos_transac_anexo10 pt10','pt10.idanexo10=an10.id','left');
        $this->db->join('historico_consulta_pb hpb','hpb.id_operacion=o.id and hpb.id_perfilamiento=oc.id_perfilamiento');
        
        //$this->db->where('p.activo',1);
        $this->db->where('p.idcliente',$cliente);
        $this->db->where('ct.id_actividad','13'); //Esta linea cambia dependiendo la actividad
        if($where!=""){
           $this->db->where($where); 
        }
        //$this->db->group_by('o.id');
        //$this->db->group_by('ct.id_transaccion');
        $this->db->group_by("an10.id");

        $month1=01;
        $month2=01;
        $month=$params['mes'];
        if ($params['mes'] == "00"){
          $month1="01";
          $month2="12";
        }else{
          $month1=$params['mes'];
          $month2=$params['mes'];
        }//date('m');
        $year = $params['anio'];//date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        /*$ultimo_dia_fecha=date('Y-m-d', mktime(0,0,0, $month2, $day, $year));
        $primer_dia_fecha= date('Y-m-d', mktime(0,0,0, $month1, 1, $year));
        $this->db->where('ct.fecha_reg BETWEEN '.'"'.$primer_dia_fecha.' 00:00:00"'.' AND '.'"'.$ultimo_dia_fecha.' 23:59:59"');*/
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month1, $year);
        $ultimo_dia_fecha=date('Y-m-d', strtotime($year."-".$month2."-".$num_days));
        $primer_dia_fecha= date('Y-m-d', strtotime($year."-".$month1."-"."01"));
        //$this->db->where('an10.fecha_operacion BETWEEN '.'"'.$primer_dia_fecha.'"'.' AND '.'"'.$ultimo_dia_fecha.'"');
        if($params["mes"]!="00"){
            $this->db->where('an10.mes_acuse',$params["mes"]);
        }
        $this->db->where('an10.anio_acuse',$params["anio"]);

        if($tipocliente[1]!=0 && $tipocliente[3]==0){ //este se queda
         $this->db->where('p.idperfilamiento='.$tipocliente[1]);
        }else if($tipocliente[1]!=0 && $tipocliente[3]>0){
         $this->db->where('o.id_union',$tipocliente[3]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        //$query=$this->db->get();
        //return $query->row()->total;
        return $this->db->count_all_results();
    }
    ////////* ******************************************** ////
    
    /*public function verificaOperacion($id){
      $this->db->select("co.id, IFNULL(co.id_perfilamiento,'') as id_perfilamiento, IFNULL(co.id_clientec,'') as id_clientec, IFNULL(id_duenio_bene,'') as id_duenio_bene, IFNULL(id_docs_cliente,'') as id_docs_cliente, IFNULL(id_docs_bene,'') as id_docs_bene, IFNULL(id_cliente_transac,'') as id_cliente_transac, co.tipo_bene, co.fecha_reg, IFNULL(dc.validado,'') as validadoC, IFNULL(db.validado,'') as validadoB, IFNULL(dc.id,'') as idDC, IFNULL(db.id,'') as idDB, IFNULL(gr.grado,'0') as grado");
      $this->db->from("clientes_operaciones co");
      $this->db->join("grado_riesgo gr", "gr.id_operacion=co.id","left");
      $this->db->join("docs_cliente dc", "dc.id_perfilamiento=co.id_perfilamiento","left");
      $this->db->join("docs_beneficiario db", "db.id_beneficiario=co.id_duenio_bene","left");
      $this->db->where('co.id',$id);
      $query=$this->db->get();
      return $query->result();
    }*/
 
    /* ********** SE MODIFICA LA FUNCION PARA TRAER LOS DATOS DE LAS OPERACIONES CON MULTIPLES CLIENTES */ 
    public function verificaOperacion($id){
      $this->db->select("o.id, IFNULL(oc.id_perfilamiento,'') as id_perfilamiento, IFNULL(oc.id_clientec,'') as id_clientec, 
        IFNULL(oc.id_cliente,'') as id_cliente, IFNULL(p.idtipo_cliente,'') as tipo_cliente, IFNULL(id_duenio_bene,'') as id_duenio_bene, IFNULL(id_docs_cliente,'') as id_docs_cliente, IFNULL(id_docs_bene,'') as id_docs_bene, IFNULL(id_cliente_transac,'') as id_cliente_transac, ob.tipo_bene, o.fecha_reg, IFNULL(dc.validado,'') as validadoC, IFNULL(db.validado,'') as validadoB, IFNULL(dc.id,'') as idDC, IFNULL(db.id,'') as idDB, IFNULL(gr.grado,'0') as grado, oc.id as id_oc");
      $this->db->from("operaciones o");
      $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
      //$this->db->join("operacion_beneficiario ob", "ob.id_operacion=o.id and ob.id_perfilamiento=oc.id_perfilamiento","left");
      $this->db->join("operacion_beneficiario ob", "ob.id_operacion=o.id","left");
      $this->db->join("perfilamiento p", "p.idperfilamiento=oc.id_perfilamiento");
      $this->db->join("grado_riesgo_actividad gr", "gr.id_operacion=o.id","left");
      $this->db->join("docs_cliente dc", "dc.id_perfilamiento=oc.id_perfilamiento","left");
      $this->db->join("docs_beneficiario db", "db.id_beneficiario=ob.id_duenio_bene","left");
      $this->db->where('o.id',$id);
      $this->db->where('oc.activo',1);
      $this->db->group_by('id_clientec');
      $query=$this->db->get();
      return $query->result();
    }

    public function verificaOperacionBenes($id,$idp){
      $this->db->select("IFNULL(ob.id,'0') as id_ob, IFNULL(ob.id_duenio_bene,'') as id_duenio_bene_nvo, IFNULL(ob.tipo_bene,'') as tipo_bene_opera, IFNULL(id_docs_bene,'') as id_docs_bene,  IFNULL(db.validado,'') as validadoB, 'soy de tipo benes' as benes_tipo");
      $this->db->from("operacion_beneficiario ob");
      $this->db->join("operaciones o", "o.id=ob.id_operacion and ob.id_perfilamiento=o.id_perfilamiento","left");
      $this->db->join("docs_beneficiario db", "db.id_beneficiario=ob.id_duenio_bene","left");
      $this->db->where('ob.id_operacion',$id);
      $this->db->where('ob.id_perfilamiento',$idp);
      $this->db->where('ob.activo',1);
      //$this->db->group_by('id_clientec');
      $query=$this->db->get();
      return $query->result();
    }
    /* ************************************* */

    /*public function verificaOperacionMoral($id){
      $this->db->select("co.id, IFNULL(co.id_perfilamiento,'') as id_perfilamiento, IFNULL(co.id_clientec,'') as id_clientec, IFNULL(id_duenio_bene,'') as id_duenio_bene, IFNULL(id_docs_cliente,'') as id_docs_cliente, IFNULL(id_docs_bene,'') as id_docs_bene, IFNULL(id_cliente_transac,'') as id_cliente_transac, co.tipo_bene, co.fecha_reg, IFNULL(dc.validado,'') as validadoC, IFNULL(db.validado,'') as validadoB, IFNULL(dc.id,'') as idDC, IFNULL(db.id,'') as idDB, IFNULL(gr.grado,'0') as grado");
      $this->db->from("clientes_operaciones co");
      $this->db->join("grado_riesgo gr", "gr.id_operacion=co.id","left");
      $this->db->join("docs_cliente dc", "dc.id_perfilamiento=co.id_perfilamiento","left");
      $this->db->join("beneficiario_moral_moral bmm", "bmm.id=co.id_duenio_bene","left");
      $this->db->join("docs_beneficiario db", "db.id_beneficiario=bmm.id_bene_moral","left");
      $this->db->where('co.id',$id);
      $query=$this->db->get();
      return $query->result();
    }*/

    /* ********** SE MODIFICA LA FUNCION PARA TRAER LOS DATOS DE LAS OPERACIONES CON MULTIPLES CLIENTES */
    public function verificaOperacionMoral($id){
      $this->db->select("o.id, IFNULL(oc.id_perfilamiento,'') as id_perfilamiento, IFNULL(oc.id_clientec,'') as id_clientec, 
        IFNULL(oc.id_cliente,'') as id_cliente, IFNULL(p.idtipo_cliente,'') as tipo_cliente,
        IFNULL(id_duenio_bene,'') as id_duenio_bene, IFNULL(id_docs_cliente,'') as id_docs_cliente, IFNULL(id_docs_bene,'') as id_docs_bene, IFNULL(id_cliente_transac,'') as id_cliente_transac, ob.tipo_bene, o.fecha_reg, IFNULL(dc.validado,'') as validadoC, IFNULL(db.validado,'') as validadoB, IFNULL(dc.id,'') as idDC, IFNULL(db.id,'') as idDB, IFNULL(gr.grado,'0') as grado, oc.id as id_oc");
      $this->db->from("operaciones o");
      $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
      $this->db->join("operacion_beneficiario ob", "ob.id_operacion=o.id and ob.id_perfilamiento=oc.id_perfilamiento","left");
      $this->db->join("perfilamiento p", "p.idperfilamiento=oc.id_perfilamiento");
      $this->db->join("grado_riesgo_actividad gr", "gr.id_operacion=o.id","left");
      $this->db->join("docs_cliente dc", "dc.id_perfilamiento=oc.id_perfilamiento","left");
      $this->db->join("beneficiario_moral_moral bmm", "bmm.id=ob.id_duenio_bene","left");
      //$this->db->join("docs_beneficiario db", "db.id_beneficiario=bmm.id_bene_moral","left");
      $this->db->join("docs_beneficiario db", "db.id_beneficiario=ob.id_duenio_bene","left");
      $this->db->where('o.id',$id);
      $this->db->group_by('id_clientec');
      $query=$this->db->get();
      return $query->result();
    }
    /* ************************************************ */

    /*public function verificaOperacionFide($id){
      $this->db->select("co.id, IFNULL(co.id_perfilamiento,'') as id_perfilamiento, IFNULL(co.id_clientec,'') as id_clientec, IFNULL(id_duenio_bene,'') as id_duenio_bene, IFNULL(id_docs_cliente,'') as id_docs_cliente, IFNULL(id_docs_bene,'') as id_docs_bene, IFNULL(id_cliente_transac,'') as id_cliente_transac, co.tipo_bene, co.fecha_reg, IFNULL(dc.validado,'') as validadoC, IFNULL(db.validado,'') as validadoB, IFNULL(dc.id,'') as idDC, IFNULL(db.id,'') as idDB, IFNULL(gr.grado,'0') as grado");
      $this->db->from("clientes_operaciones co");
      $this->db->join("grado_riesgo gr", "gr.id_operacion=co.id","left");
      $this->db->join("docs_cliente dc", "dc.id_perfilamiento=co.id_perfilamiento","left");
      $this->db->join("beneficiario_fideicomiso bmf", "bmf.id=co.id_duenio_bene","left");
      $this->db->join("docs_beneficiario db", "db.id_beneficiario=bmf.id_bene_moral","left");
      $this->db->where('co.id',$id);
      $query=$this->db->get();
      return $query->result();
    }*/

    /* ********** SE MODIFICA LA FUNCION PARA TRAER LOS DATOS DE LAS OPERACIONES CON MULTIPLES CLIENTES */
    public function verificaOperacionFide($id){
      $this->db->select("o.id, IFNULL(oc.id_perfilamiento,'') as id_perfilamiento, IFNULL(oc.id_clientec,'') as id_clientec, 
        IFNULL(oc.id_cliente,'') as id_cliente, IFNULL(p.idtipo_cliente,'') as tipo_cliente,
        IFNULL(id_duenio_bene,'') as id_duenio_bene, IFNULL(id_docs_cliente,'') as id_docs_cliente, IFNULL(id_docs_bene,'') as id_docs_bene, IFNULL(id_cliente_transac,'') as id_cliente_transac, ob.tipo_bene, o.fecha_reg, IFNULL(dc.validado,'') as validadoC, IFNULL(db.validado,'') as validadoB, IFNULL(dc.id,'') as idDC, IFNULL(db.id,'') as idDB, IFNULL(gr.grado,'0') as grado, oc.id as id_oc");
      $this->db->from("operaciones o");
      $this->db->join("operacion_cliente oc", "oc.id_operacion=o.id");
      $this->db->join("operacion_beneficiario ob", "ob.id_operacion=o.id and ob.id_perfilamiento=oc.id_perfilamiento","left");
      $this->db->join("perfilamiento p", "p.idperfilamiento=oc.id_perfilamiento");
      $this->db->join("grado_riesgo_actividad gr", "gr.id_operacion=o.id","left");
      $this->db->join("docs_cliente dc", "dc.id_perfilamiento=oc.id_perfilamiento","left");
      $this->db->join("beneficiario_fideicomiso bmf", "bmf.id=ob.id_duenio_bene","left");
      //$this->db->join("docs_beneficiario db", "db.id_beneficiario=bmf.id_bene_moral","left");
      $this->db->join("docs_beneficiario db", "db.id_beneficiario=ob.id_duenio_bene","left");
      $this->db->where('o.id',$id);
      $this->db->group_by('id_clientec');
      $query=$this->db->get();
      return $query->result();
    }
    /** ************************************************* */
    public function get_proceso_detalle($iddb){
      $this->db->select("*");
      $this->db->from("docs_beneficiario db");
      $this->db->where('id_beneficiario',$iddb);
      $query=$this->db->get();
      return $query->result();
    }

    public function detalles_busquedaPB($id,$idp,$idc){
      $this->db->select("*, MAX(id)");
      $this->db->from("historico_consulta_pb");
      $this->db->where('id_operacion',$id);
      $this->db->where('id_perfilamiento',$idp);
      $this->db->where('id_clientec',$idc);
      $query=$this->db->get();
      return $query->result();
    }

    public function detalles_busquedaPBDB($id,$idb,$tipo){
      $this->db->select("*, MAX(id)");
      $this->db->from("historico_consulta_pb");
      $this->db->where('id_operacion',$id);
      $this->db->where('id_bene',$idb);
      $this->db->where('tipo_bene',$tipo);
      $query=$this->db->get();
      return $query->result();
    }

    public function getInmuebles($idc){
      $this->db->select("*");
      $this->db->from("inmuebles_cliente ic");
      $this->db->where('id_cliente',$idc);
      $this->db->where('ic.status',1);
      $query=$this->db->get();
      return $query->result();
    }

    public function getInmuebles5b($idc){
      $this->db->select("*");
      $this->db->from("inmuebles5b_cliente ic");
      $this->db->where('id_cliente',$idc);
      $this->db->where('ic.estatus',1);
      $query=$this->db->get();
      return $query->result();
    }

    public function getInmuebleId($id){
      $this->db->select("*");
      $this->db->from("inmuebles_cliente ic");
      $this->db->where('id',$id);
      $query=$this->db->get();
      return $query->result();
    }

    public function getUnionCliente($id){
      $this->db->select('uc.*, uc.id as iduc, p.idperfilamiento as idperfilamientop, p.idcliente,p.idtipo_cliente,ecoi.*,cf.*,pfe.*,pfm.*,pmmd.*,pmme.*, 
        IFNULL(ecoi.idtipo_cliente_e_c_o_i,0) as idcc1, 
        IFNULL(cf.idtipo_cliente_f,0) as idcc2, 
        IFNULL(pfe.idtipo_cliente_p_f_e,0) as idcc3, 
        IFNULL(pfm.idtipo_cliente_p_f_m,0) as idcc4, 
        IFNULL(pmmd.idtipo_cliente_p_m_m_d,0) as idcc5,
        IFNULL(pmme.idtipo_cliente_p_m_m_e,0) as idcc6,
        pfe.nombre as nombre2, pfe.apellido_paterno as apellido_paterno2, pfe.apellido_materno as apellido_materno2,
        ecoi.denominacion as denominacion2');
      $this->db->from('uniones_clientes uc');
      $this->db->join('perfilamiento p','p.idperfilamiento=uc.id_perfilamiento');
      $this->db->join('tipo_cliente_e_c_o_i ecoi','ecoi.idperfilamiento=p.idperfilamiento','left');
      $this->db->join('tipo_cliente_f cf','cf.idperfilamiento=p.idperfilamiento','left');
      $this->db->join('tipo_cliente_p_f_e pfe','pfe.idperfilamiento=p.idperfilamiento','left');
      $this->db->join('tipo_cliente_p_f_m pfm','pfm.idperfilamiento=p.idperfilamiento','left');
      $this->db->join('tipo_cliente_p_m_m_d pmmd','pmmd.idperfilamiento=p.idperfilamiento','left');
      $this->db->join('tipo_cliente_p_m_m_e pmme','pmme.idperfilamiento=p.idperfilamiento','left');
      $this->db->where('uc.activo',1);
      $this->db->where('uc.id_union',$id);
      $query=$this->db->get();
      return $query->result();
    }
    
    /*
    public function getCuestionarGrado($idg){
      $this->db->select('ca.*');
      $this->db->from('grado_riesgo_actividad gra');
      $this->db->join('cuestionario_ampliado ca','ca.id_grado_riesgo=gra.id');
      $this->db->where('gra.id',$idg);
      $query=$this->db->get();
      return $query->row();
    }
    */

}