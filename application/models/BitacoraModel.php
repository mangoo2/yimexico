<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class BitacoraModel extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function getSearchCli($search){
        $strq = "SELECT * from clientes WHERE status=1 and (empresa like '%".$search."%' or alias like '%".$search."%' )";
        $query = $this->db->query($strq);
        return $query->result();
    }

//==============Clientes====================   
    function get_cliente(){
        $columns = array( 
            0=>'c.idcliente',
            1=>'pf.nombre',
            2=>'pf.apellido_paterno',
            4=>'pf.apellido_materno',
            5=>'pm.razon_social',
            6=>'pfi.denominacion_razon_social',
            7=>'c.fecha_alta_sistema',
            8=>'c.tipo_persona',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cliente c');
        $this->db->join('cliente_fisica pf', 'pf.idcliente=c.idcliente','left');
        $this->db->join('cliente_moral pm', 'pm.idcliente=c.idcliente','left');
        $this->db->join('cliente_fideicomiso pfi', 'pfi.idcliente=c.idcliente','left');

        $this->db->where('c.activo=1');
        $this->db->group_by('c.idcliente');

        $query=$this->db->get();
        return $query->result();
    }


    function get_clienteBusqueda($params){
        $sel1="";
        $sel2="";
        $sel3="";
        if($params["id_tipo"]==1){
            $sel1="pf.nombre"; $sel2="pf.apellido_paterno"; $sel3="pf.apellido_materno";
        }
        else if($params["id_tipo"]==2){ 
            $sel1="pm.razon_social";
        }
        else if($params["id_tipo"]==3) {
            $sel1="pfi.denominacion_razon_social";
        }
        $columns = array( 
            0=>'hpb.id',
            1=> $sel1,
            2=> $sel2,
            3=> $sel3,
            4=>"DATE_FORMAT(hpb.fecha_consulta, '%d/%m/%Y %r') as fecha_consulta",
            5=>'hpb.resultado',
            6=>'hpb.id_bene',
            7=>'hpb.resultado_bene',
            8=>'p.idtipo_cliente',
            9=>'o.folio_cliente',
            10=>'concat(hpb.nombre," ",hpb.apellidos) as cliente',
            11=>'concat(hpb.nombre_db," ",hpb.apellidos_db) as beneficiario'
        );

        $columns2 = array( 
            0=>'hpb.id',
            1=> $sel1,
            2=> $sel2,
            3=> $sel3,
            4=>"hpb.fecha_consulta",
            5=>'hpb.resultado',
            6=>'hpb.id_bene',
            7=>'hpb.resultado_bene',
            8=>'p.idtipo_cliente',
            9=>'o.folio_cliente',
            10=>'concat(hpb.nombre," ",hpb.apellidos)',
            11=>'concat(hpb.nombre_db," ",hpb.apellidos_db)'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('historico_consulta_pb hpb');
        $this->db->join('operaciones o', 'o.id = hpb.id_operacion');
        $this->db->join('perfilamiento p', 'p.idperfilamiento = hpb.id_perfilamiento');

        if($params["id_tipo"]==1)
            $this->db->join('cliente_fisica pf', 'pf.idcliente=p.idcliente');
        else if($params["id_tipo"]==2) 
            $this->db->join('cliente_moral pm', 'pm.idcliente=p.idcliente');
        else if($params["id_tipo"]==3) 
            $this->db->join('cliente_fideicomiso pfi', 'pfi.idcliente=p.idcliente');

        $this->db->where('p.idcliente',$params["id_cliente"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_clienteBusqueda($params){
        $sel1="";
        $sel2="";
        $sel3="";
        if($params["id_tipo"]==1){
            $sel1="pf.nombre"; $sel2="pf.apellido_paterno"; $sel3="pf.apellido_materno";
        }
        else if($params["id_tipo"]==2){ 
            $sel1="pm.razon_social";
        }
        else if($params["id_tipo"]==3) {
            $sel1="pfi.denominacion_razon_social";
        }

        $columns = array( 
            0=>'hpb.id',
            1=> $sel1,
            2=> $sel2,
            3=> $sel3,
            4=>'hpb.fecha_consulta',
            5=>'hpb.resultado',
            6=>'hpb.id_bene',
            7=>'hpb.resultado_bene',
            8=>'p.idtipo_cliente',
            9=>'o.folio_cliente',
            10=>'concat(hpb.nombre," ",hpb.apellidos)',
            11=>'concat(hpb.nombre_db," ",hpb.apellidos_db)'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('historico_consulta_pb hpb');
        $this->db->join('operaciones o', 'o.id = hpb.id_operacion');
        $this->db->join('perfilamiento p', 'p.idperfilamiento = hpb.id_perfilamiento');

        if($params["id_tipo"]==1)
            $this->db->join('cliente_fisica pf', 'pf.idcliente=p.idcliente');
        else if($params["id_tipo"]==2) 
            $this->db->join('cliente_moral pm', 'pm.idcliente=p.idcliente');
        else if($params["id_tipo"]==3) 
            $this->db->join('cliente_fideicomiso pfi', 'pfi.idcliente=p.idcliente');

        $this->db->where('p.idcliente',$params["id_cliente"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        $query=$this->db->get();
        return $query->row()->total;
    }


    function exportarLista($id_cli,$tipo){
        $sel1="";
        $sel2="";
        $sel3="";
        if($tipo==1){
            $sel1="pf.nombre"; $sel2="pf.apellido_paterno"; $sel3="pf.apellido_materno";
        }
        else if($tipo==2){ 
            $sel1="pm.razon_social";
        }
        else if($tipo==3) {
            $sel1="pfi.denominacion_razon_social";
        }
        $columns = array( 
            0=>'hpb.id',
            1=> $sel1,
            2=> $sel2,
            3=> $sel3,
            4=>"DATE_FORMAT(hpb.fecha_consulta, '%d/%m/%Y %r') as fecha_consulta",
            5=>'hpb.resultado',
            6=>'hpb.id_bene',
            7=>'hpb.resultado_bene',
            8=>'p.idtipo_cliente',
            9=>'o.folio_cliente',
            10=>'concat(hpb.nombre," ",hpb.apellidos) as cliente',
            11=>'concat(hpb.nombre_db," ",hpb.apellidos_db) as beneficiario'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('historico_consulta_pb hpb');
        $this->db->join('operaciones o', 'o.id = hpb.id_operacion');
        $this->db->join('perfilamiento p', 'p.idperfilamiento = hpb.id_perfilamiento');

        if($tipo==1)
            $this->db->join('cliente_fisica pf', 'pf.idcliente=p.idcliente');
        else if($tipo==2) 
            $this->db->join('cliente_moral pm', 'pm.idcliente=p.idcliente');
        else if($tipo==3) 
            $this->db->join('cliente_fideicomiso pfi', 'pfi.idcliente=p.idcliente');

        $this->db->where('p.idcliente',$id_cli);
        $query=$this->db->get();
        return $query->result();
    }
}