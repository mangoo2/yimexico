<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function tabla_inserta($tabla,$data){
        $this->db->insert($tabla,$data);   
        return $this->db->insert_id();
    }
    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwhereOR($tables,$where,$or){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->or_where($or);
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwhererow($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->row();
    }
    public function getselectwherestatus($select,$tables,$where){
        $this->db->select($select);
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getBitocoraDocs($where){
        $this->db->select("bd.*, p.idtipo_cliente, DATE_FORMAT(bd.fecha, '%Y') AS anio_opera, IFNULL(o.folio_cliente,0) as folio_cliente");
        $this->db->from("bitacora_docs_cliente bd");
        $this->db->join("perfilamiento p","p.idperfilamiento=bd.id_perfilamiento");
        $this->db->join("operaciones o","o.id=bd.id_operacion","left");
        $this->db->where($where);
        $this->db->order_by("id_perfilamiento","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getBitacoraDocsDB($where){
        $this->db->select("bd.*, DATE_FORMAT(bd.fecha, '%Y') AS anio_opera, o.folio_cliente");
        $this->db->from("bitacora_docs_bene bd");
        $this->db->join("operaciones o","o.id=bd.id_operacion","left");
        $this->db->join("operacion_cliente oc","oc.id_operacion=bd.id_operacion","left");
        $this->db->where($where);
        $this->db->order_by("id_operacion","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getBitacoraDocsSinDB($id,$where){
        $this->db->select("dsb.*, DATE_FORMAT(dsb.fecha_reg, '%Y') AS anio_opera, o.folio_cliente");
        $this->db->from("operaciones o");
        $this->db->join("doc_sin_bene dsb","dsb.id_operacion=o.id");
        $this->db->join("operacion_cliente oc","oc.id_operacion=o.id","left");
        $this->db->where("o.id_cliente",$id);
        $this->db->where($where);
        $this->db->order_by("dsb.id_operacion","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwherestatusAll($tables,$where){
        $this->db->select('*');
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }


    public function getselectwherestatus2($select,$tables,$where,$col){
        $this->db->select($select);
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $this->db->group_by($col);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);/// Se ocupa para n select
        $this->db->update($catalogo);
        return $id;
    }
    public function updateCatalogoW($data,$where,$catalogo,$id){
        $this->db->set($data);
        $this->db->where($where);/// Se ocupa para n select
        $this->db->update($catalogo);
        return $id;
    }
    public function updateCatalogoCT($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);/// Se ocupa para n select
        //$this->db->update($catalogo);
        return $this->db->update($catalogo);
        
    }

    public function updateCatalogo2($data,$where,$catalogo){
        $this->db->set($data);
        $this->db->where($where);/// Se ocupa para n select
        $this->db->update($catalogo);
        //return $id;
    } 

    public function updateCatalogo2OR($data,$where,$or,$catalogo){
        $this->db->set($data);
        $this->db->where($where);/// Se ocupa para n select
        $this->db->or_where($or);
        $this->db->update($catalogo);
        //return $id;
    } 

    function getData($tabla){
        $this->db->select("*");
        $this->db->from($tabla);
        $query=$this->db->get();
        return $query->result();
    }
    public function update_foto($data,$id,$tabla) {
    $this->db->set($data);
    $this->db->where('id', $id);
    return $this->db->update($tabla); 
    }
    function getdeletewheren($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }
    function verificar_pass($pass) {
        $strq = "SELECT * FROM usuarios WHERE perfilId=1";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $verificar = password_verify($pass,$passwo);
            if ($verificar){
                $count=1;
            }
        } 
        return $count;
    }
    //==========Menu
    public function getDatamenu($id)
    {
        $sql = 'SELECT m.Nombre FROM menu_sub AS ms
                INNER JOIN menu AS m ON m.MenuId = ms.MenuId
                WHERE m.MenuId ='.$id.' GROUP BY m.MenuId';
        $query = $this->db->query($sql);
        return $query->result();        
    }
    //==========Sub Menu
    public function getDatasudmenu($id)
    {
        $sql = 'SELECT ms.* FROM menu_sub AS ms
                INNER JOIN menu AS m ON m.MenuId = ms.MenuId
                WHERE m.MenuId ='.$id;
        $query = $this->db->query($sql);
        return $query->result();        
    }
    /// Permisos
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }

    function SimpleGetRow($where,$tabla){
        //$this->db->select();
        $this->db->where($where);
        return $this->db->get($tabla)->row();
        //return $this->db->last_query();
    }

    function SimpleGetWhere($where,$tabla,$cot){
        $this->db->select($cot);
        $this->db->where($where);
        return $this->db->get($tabla)->row()->$cot;
        //return $this->db->last_query();
    }
    ///===== perfiles
    public function get_perfiles(){
        $strq = "SELECT * FROM perfiles WHERE perfilId >=6";
        $query = $this->db->query($strq);
        return $query->result();
    }
    ///===== Puesto
    public function get_puesto(){
        $strq = "SELECT * FROM perfiles WHERE perfilId >=6";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function update_docs_extrac($id,$descrip){
        $strq = "UPDATE docs_extra_clientesc set descrip='$descrip' WHERE id =$id";
        $query = $this->db->query($strq);
        return $id;
    }

    public function getUmbrales(){
        $this->db->select("ac.*, a.nombre as anexo");
        $this->db->from("anexo_config ac");
        $this->db->join("anexos_grales a","a.id=ac.id_anexo_gral");
        $this->db->where("a.status",1);
        $this->db->order_by("ac.id_anexo_gral","asc");
        $this->db->group_by("ac.id_anexo_gral");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDivisas($params){
        $this->db->select("d.*, DATE_FORMAT(d.fecha,'%d-%m-%Y') as fecha_format");
        $this->db->from("divisas d");
        $this->db->where("status",1);
        if($params["moneda"]!=0)
            $this->db->where("moneda",$params["moneda"]);
        $query=$this->db->get();
        return $query->result();
    }

    public function getUmasAnio($anio){
        $this->db->select("*");
        $this->db->from("umas");
        $this->db->where("status",1);
        $this->db->where("anio",$anio);
        $query=$this->db->get();
        return $query->result();
    }

    public function getUmbral($id){
        $this->db->select("*");
        $this->db->from("anexo_config");
        $this->db->where("id_anexo_gral",$id);
        $query=$this->db->get();
        return $query->result();
    }

    public function getUmbralActividad($id,$act){
        $this->db->select("*");
        $this->db->from("anexo_config");
        $this->db->where("id_anexo_gral",$id);
        $this->db->where("sub_anexo",$act);
        $query=$this->db->get();
        return $query->result();
    }

    public function getDivisa($id,$fecha){
        $this->db->select("*");
        $this->db->from("divisas");
        $this->db->where("moneda",$id);
        $this->db->where("fecha",$fecha);
        $query=$this->db->get();
        return $query->result();
    }

    public function getDivisaUltimo($id){ //se cambia el de arriba, ahora se ocupará el ultimo registro disponible
        $this->db->select("*");
        $this->db->from("divisas");
        $this->db->where("moneda",$id);
        $this->db->order_by("fecha","DESC");
        $this->db->limit("1");
        $query=$this->db->get();
        return $query->row();
    }
    public function getdivisascambio(){
        $sql = "SELECT divi.moneda,max(divi.fecha) as fecha,tic.clave,CURDATE() as fecha_actual 
                FROM divisas as divi 
                INNER JOIN ws_tipocambio as tic on tic.relacion=divi.moneda 
                WHERE tic.activo=1 and divi.status=1 GROUP BY moneda";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    
    public function getselectwhere_orden_asc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query->result();
    }
    public function getselectwhere_orden_desc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'DESC');
        $query=$this->db->get();
        return $query->result();
    } 

    public function GetLista($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'ligaindentificacion',
            3=>'listaecuentra',
            4=>'razonlista',
            5=>'rfc',
            6=>'direccion',
            7=>'apellido_p',
            8=>'apellido_m',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('nombreln');
        $where = array('status'=>1);
        if (isset($params['fil'])){
            $this->db->where("razonlista=".$params['fil']);
        }
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function get_result_bo($ido){
        $this->db->select("ob.*, hcc.resultado_bene");
        $this->db->from("operacion_beneficiario ob");
        $this->db->join("historico_consulta_pb hcc","hcc.id_operacion=ob.id_operacion and ob.id_duenio_bene=hcc.id_bene and ob.tipo_bene=hcc.tipo_bene","left");
        $this->db->where("ob.id_operacion",$ido);
        $this->db->where("ob.activo",1);
        $this->db->group_by("ob.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function get_result_bo2($ido){
        $this->db->select("hcc.resultado_bene");
        $this->db->from("operacion_beneficiario ob");
        $this->db->join("historico_consulta_pb hcc","hcc.id_operacion=ob.id_operacion and ob.id_duenio_bene=hcc.id_bene and ob.tipo_bene=hcc.tipo_bene","left");
        $this->db->where("ob.id_operacion",$ido);
        $this->db->where("ob.activo",1);
        $this->db->group_by("ob.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_result_co($ido){
        $this->db->select("oc.*, hcc.resultado");
        $this->db->from("operacion_cliente oc");
        $this->db->join("historico_consulta_pb hcc","hcc.id_operacion=oc.id_operacion and oc.id_perfilamiento=hcc.id_perfilamiento","left");
        $this->db->where("oc.id_operacion",$ido);
        $this->db->where("oc.activo",1);
        $this->db->group_by("oc.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function get_result_co2($ido){
        $this->db->select("hcc.resultado, LENGTH(resultado) - LENGTH(REPLACE(resultado,',','')) as total_comas, oc.id_perfilamiento");
        $this->db->from("operacion_cliente oc");
        $this->db->join("historico_consulta_pb hcc","hcc.id_operacion=oc.id_operacion and oc.id_perfilamiento=hcc.id_perfilamiento","left");
        $this->db->where("oc.id_operacion",$ido);
        $this->db->where("oc.activo",1);
        $this->db->group_by("oc.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_benes_opera_detalles($ido){
        $this->db->select("ob.*, p.idtipo_cliente, oc.id_clientec");
        $this->db->from("operacion_beneficiario ob");
        $this->db->join("perfilamiento p","p.idperfilamiento=ob.id_perfilamiento");
        $this->db->join("operacion_cliente oc","oc.id_operacion=ob.id_operacion");
        $this->db->where("ob.id_operacion",$ido);
        $this->db->where("ob.activo",1);
        $query=$this->db->get();
        return $query->result();
    } 


    public function get_usuario_permiso_menu($idper){
        $strq = "SELECT u.*,m.nombre
                FROM usuarios_clientes_menu as u
                INNER JOIN menu_usuario_cliente AS m ON m.idmenu=u.id_menu
                WHERE u.idcliente_usuario=$idper";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function getPagosOperaAct1($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa1.*, oc.id_perfilamiento, a1.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo1 a1', 'a1.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo1 pa1', 'pa1.idanexo1=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            $this->db->where("o.id_perfilamiento",$idper);
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa1.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa1.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa1.idanexo1!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a1.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa1.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function getPagosOperaAct2a($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa2.*, oc.id_perfilamiento, a2a.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo2a a2a', 'a2a.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo2a pa2', 'pa2.idanexo2a=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa2.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa2.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa2.idanexo2a!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a2a.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa2.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function getPagosOperaAct2b($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa2.*, oc.id_perfilamiento, a2b.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo2b a2b', 'a2b.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo2b pa2', 'pa2.idanexo2b=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa2.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa2.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa2.idanexo2b!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a2b.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa2.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct2c($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa2.*, oc.id_perfilamiento, a2c.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo2c a2c', 'a2c.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo2c pa2', 'pa2.idanexo2c=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa2.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa2.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa2.idanexo2c!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a2c.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa2.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct3($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa3.*, oc.id_perfilamiento, a3.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo3 a3', 'a3.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo3 pa3', 'pa3.idanexo3=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa3.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa3.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa3.idanexo3!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a3.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa3.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct4($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa4.*, oc.id_perfilamiento, a4.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo4 a4', 'a4.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo4 pa4', 'pa4.idanexo4=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa4.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa4.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa4.idanexo4!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a4.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa4.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct5a($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa5.*, oc.id_perfilamiento, a5.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo5a a5', 'a5.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo5a pa5', 'pa5.idanexo5a=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo5a_aviso pa5', 'pa5.idanexo5a_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa5.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa5.acusado",0);
        $this->db->where("a5.liquidada",1); //se agrega condicion, solo operas liquidadas apoyan en sumatoria
        if($idane>0 && $aviso==0){
            $this->db->where("pa5.idanexo5a!=",$idane);
        }
        //$this->db->where("pa5.idanexo5a!=156");  //se quita ese id de operacion ya que es de pruebas, 28 del cliente ACV, usuario ACV5A

        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a5.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa5.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct5b($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("oc.id_perfilamiento, a5.*, IFNULL(t5.id,0) as id_t, t5.aportacion as aportacion_t, t5.tipo_moneda as tipo_moneda_t, t5.monto_aporta as monto_aporta_t"); //llamar los de terceros_5b con un alias mas 
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo5b a5', 'a5.id=ct.id_transaccion');
        $this->db->join('terceros_5b t5', 't5.id_anexo=a5.id and aviso=0','left');
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("a5.acusado",0);
        //$this->db->where("xml",0);
        if($idane>0 && $aviso==0){
            $this->db->where("a5.id!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a5.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("a5.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct6($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa6.*, oc.id_perfilamiento, a6.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo6 a6', 'a6.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo6_pago pa6', 'pa6.idanexo6=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa6.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa6.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa6.idanexo6!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a6.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa6.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct7($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa7.*, oc.id_perfilamiento, a7.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo7 a7', 'a7.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo_7 pa7', 'pa7.idanexo7=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa7.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa7.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa7.idanexo7!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a7.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa7.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa8.*, oc.id_perfilamiento, a8.fecha_opera");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo_8 a8', 'a8.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo_8 pa8', 'pa8.id_anexo_8=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            $this->db->where("o.id_perfilamiento",$idper);
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento"); 
        }else{
            /*$this->db->join('uniones_clientes uc', 'uc.id_union='.$idu.'');
            $this->db->join('operacion_cliente oc', 'oc.id_perfilamiento=uc.id_perfilamiento');
            $this->db->where("ct.id_perfilamiento","uc.id_perfilamiento");*/
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }

        /*SELECT `pa8`.*, `oc`.`id_perfilamiento`, a8.fecha_opera
        FROM `operaciones` `o`
        JOIN `clientesc_transacciones` `ct` ON `ct`.`id`=`o`.`id_cliente_transac`
        JOIN `anexo_8` `a8` ON `a8`.`id`=`ct`.`id_transaccion`
        JOIN `pagos_transac_anexo_8` `pa8` ON `pa8`.`id_anexo_8`=`ct`.`id_transaccion`

        left JOIN `uniones_clientes` `uc` ON `uc`.`id_union`=o.id_union
        left JOIN `operacion_cliente` `oc` ON `oc`.`id_operacion`=`o`.`id`
        WHERE `ct`.`id_actividad` = '11'
        //AND `ct`.`id_perfilamiento` = oc.id_perfilamiento
        AND `o`.`id_union` = '7'
        AND `pa8`.`status` = 1
        AND `oc`.`activo` = 1
        AND `pa8`.`acusado` = 0
        AND TIMESTAMPDIFF(MONTH, a8.fecha_opera, CURDATE()) <= 6
        GROUP BY `pa8`.`id` */
        
        //$this->db->where("o.id",$ido);
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa8.status",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa8.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa8.id_anexo_8!=",$idane);
        }
        if($idane>0 && $aviso>0){
            $this->db->where("pa8.id_anexo_8_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a8.fecha_opera, CURDATE()) <= 6");
        $this->db->group_by("pa8.id");
        $query=$this->db->get();
        return $query->result();
    } 
    public function getPagosOperaAct9($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa9.*, oc.id_perfilamiento, a9.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo9 a9', 'a9.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo9 pa9', 'pa9.idanexo9=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_9_aviso pa9', 'pa9.id_anexo_9_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
            
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa9.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa9.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa9.idanexo9!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a9.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa9.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct10($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa10.*, oc.id_perfilamiento, a10.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo10 a10', 'a10.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo10 pa10', 'pa10.idanexo10=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa10.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa10.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa10.idanexo10!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a10.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa10.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct12a($ido,$ida,$idu,$idane,$aviso,$tipo,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("a12a.*, oc.id_perfilamiento, a12a.fecha_operacion, count(a12a.id) as total_regs");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo12a a12a', 'a12a.id=ct.id_transaccion');
        //if($aviso==0){
        if($tipo=="6"){
            //$this->db->join('pagos_transac_anexo12a pa12a', 'pa12a.idanexo12a=ct.id_transaccion');
        }
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("a12a.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("a12a.acusado",0);
        $this->db->where("a12a.tipo_actividad",$tipo); //agregado para traer solo las mismas operaciones del mismo tipo de act. vulnerable del 12a
        if($tipo=="6"){
            //$this->db->where("pa12a.activo",1);
        }
        if($idane>0 && $aviso==0){
            $this->db->where("a12a.id!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a12a.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("a12a.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct12aAct6($id){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("*");
        $this->db->from("pagos_transac_anexo12a pa12a");
        $this->db->where("pa12a.idanexo12a",$id);
        $this->db->where("pa12a.activo",1);
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct12b($ido,$ida,$idu,$idane,$aviso,$tipo,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $sel="";
        if($tipo==1){
            $sel=", acta12b.valor_catastralact1";
        }
        $this->db->select("a12b.*, oc.id_perfilamiento, a12b.fecha_operacion, count(a12b.id) as total_regs ".$sel."");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo12b a12b', 'a12b.id=ct.id_transaccion');
        if($tipo==1){
            $this->db->join('anexo12b_actividad1 acta12b', 'acta12b.anexo12b=a12b.id');
        }
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("a12b.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("a12b.acusado",0);
        $this->db->where("a12b.tipo_actividad",$tipo); //agregado para traer solo las mismas operaciones del mismo tipo de act. vulnerable del 12a
        
        if($idane>0 && $aviso==0){
            $this->db->where("a12b.id!=",$idane);
        }

        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a12b.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("a12b.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function getPagosOperaAct13($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa13.*, oc.id_perfilamiento, a13.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo13 a13', 'a13.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo13_num pa13', 'pa13.idanexo13=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa13.activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa13.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa13.idanexo13!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a13.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa13.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct15($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa15.*, oc.id_perfilamiento, a15.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo_15 a15', 'a15.id=ct.id_transaccion');
        //if($aviso==0){
            $this->db->join('pagos_transac_anexo_15 pa15', 'pa15.id_anexo_15=ct.id_transaccion');
        /*}else{
            $this->db->join('pagos_transac_anexo_8_aviso pa8', 'pa8.id_anexo_8_aviso=ct.id_transaccion');
        }*/
        $this->db->where("ct.id_actividad",$ida);

        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_perfilamiento",$idper);
            
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa15.status",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa15.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("pa15.id_anexo_15!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a15.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa15.id");
        $query=$this->db->get();
        return $query->result();
    }
    public function getPagosOperaAct16($ido,$ida,$idu,$idane,$aviso,$idper){ //donde compruebas si el mismo(s) han realziado pagos de la misma activ.
        $this->db->select("pa16.*, oc.id_perfilamiento, a16.fecha_operacion");
        $this->db->from("operaciones o");
        $this->db->join("clientesc_transacciones ct","ct.id=o.id_cliente_transac");
        $this->db->join('anexo16 a16', 'a16.id=ct.id_transaccion');
        $this->db->where("ct.id_actividad",$ida);
        $this->db->join('completar_anexo16 pa16', 'pa16.idanexo16=ct.id_transaccion');
        if($idu==0){
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento"); 
            $this->db->where("o.id_perfilamiento",$idper);
        }else{
            $this->db->join('uniones_clientes uc', 'uc.id_union=o.id_union');
            $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
            //$this->db->where("ct.id_perfilamiento","oc.id_perfilamiento");
            $this->db->where("o.id_union",$idu);
        }
        $this->db->where("o.activo",1);
        $this->db->where("oc.activo",1);
        $this->db->where("pa16.Activo",1);
        //$this->db->where("oc.activo",1);
        $this->db->where("pa16.acusado",0);
        if($idane>0 && $aviso==0){
            $this->db->where("a16.id!=",$idane);
        }
        if($idane>0 && $aviso>0){
            //$this->db->where("pa1.idanexo1_aviso!=",$idane);
        }
        //$this->db->where("TIMESTAMPDIFF(MONTH, fecha_pago, CURDATE()) >= 1");
        $this->db->where("TIMESTAMPDIFF(MONTH, a16.fecha_operacion, CURDATE()) <= 6");
        $this->db->group_by("pa16.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function getPagosOperaActAviso1($idane,$bit){
        $this->db->select("paa1.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo1_aviso paa1");
        $this->db->join("anexo1_aviso aa1","aa1.id=paa1.idanexo1_aviso");
            
        $this->db->where("paa1.activo",1);
        $this->db->where("paa1.acusado",0);
        $this->db->where("paa1.idanexo1_aviso",$idane);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa1.*, (select id from pagos_transac_anexo1 where id_pago_ayuda = pa1.id) as id_pago_ayuda");
        $this->db->from("anexo1_aviso aa1");
        $this->db->join("pagos_transac_anexo1 pa1","pa1.idanexo1=aa1.idanexo1","left");

        $this->db->where("pa1.activo",1);
        $this->db->where("pa1.acusado",1);
        if($bit==0){
            $this->db->where("aa1.id",$idane);
        }
        else{
            $this->db->where("aa1.idanexo1",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    } 

    public function getPagosOperaActAviso2a($idane,$bit){
        $this->db->select("paa2.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo2a_aviso paa2");
        $this->db->join("anexo2a_aviso aa2","aa2.id=paa2.idanexo2a_aviso");
            
        $this->db->where("paa2.activo",1);
        $this->db->where("paa2.acusado",0);
        $this->db->where("paa2.idanexo2a_aviso",$idane);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa2.*, (select id from pagos_transac_anexo2a where id_pago_ayuda = pa2.id) as id_pago_ayuda");
        $this->db->from("anexo2a_aviso aa2");
        $this->db->join("pagos_transac_anexo2a pa2","pa2.idanexo2a=aa2.idanexo2a","left");

        $this->db->where("pa2.activo",1);
        $this->db->where("pa2.acusado",1);
        if($bit==0){
            $this->db->where("aa2.id",$idane);
        }
        else{
            $this->db->where("aa2.idanexo2a",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }

    public function getPagosOperaActAviso2b($idane,$bit){
        $this->db->select("paa2.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo2b_aviso paa2");
        $this->db->join("anexo2b_aviso aa2","aa2.id=paa2.idanexo2b_aviso");
            
        $this->db->where("paa2.activo",1);
        $this->db->where("paa2.acusado",0);
        $this->db->where("paa2.idanexo2b_aviso",$idane);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa2.*, (select id from pagos_transac_anexo2b where id_pago_ayuda = pa2.id) as id_pago_ayuda");
        $this->db->from("anexo2b_aviso aa2");
        $this->db->join("pagos_transac_anexo2b pa2","pa2.idanexo2B=aa2.idanexo2b","left");

        $this->db->where("pa2.activo",1);
        $this->db->where("pa2.acusado",1);
        if($bit==0){
            $this->db->where("aa2.id",$idane);
        }
        else{
            $this->db->where("aa2.idanexo2b",$idane);
        }
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso2c($idane,$bit){
        $this->db->select("paa2.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo2c_aviso paa2");
        $this->db->join("anexo2c_aviso aa2","aa2.id=paa2.idanexo2c_aviso");
            
        $this->db->where("paa2.activo",1);
        $this->db->where("paa2.acusado",0);
        $this->db->where("paa2.idanexo2c_aviso",$idane);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa2.*, (select id from pagos_transac_anexo2c where id_pago_ayuda = pa2.id) as id_pago_ayuda");
        $this->db->from("anexo2c_aviso aa2");
        $this->db->join("pagos_transac_anexo2c pa2","pa2.idanexo2a=aa2.idanexo2c","left");
        $this->db->where("pa2.activo",1);
        $this->db->where("pa2.acusado",1);
        if($bit==0){
            $this->db->where("aa2.id",$idane);
        }
        else{
            $this->db->where("aa2.idanexo2c",$idane);
        }
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso3($idane,$bit){
        $this->db->select("paa3.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo3_aviso paa3");
        $this->db->join("anexo3_aviso aa3","aa3.id=paa3.idanexo3_aviso");
            
        $this->db->where("paa3.activo",1);
        $this->db->where("paa3.acusado",0);
        $this->db->where("paa3.idanexo3_aviso",$idane);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa3.*, (select id from pagos_transac_anexo3 where id_pago_ayuda = pa3.id) as id_pago_ayuda");
        $this->db->from("anexo3_aviso aa3");
        $this->db->join("pagos_transac_anexo3 pa3","pa3.idanexo3=aa3.idanexo3","left");

        $this->db->where("pa3.activo",1);
        $this->db->where("pa3.acusado",1);
        if($bit==0){
            $this->db->where("aa3.id",$idane);
        }
        else{
            $this->db->where("aa3.idanexo3",$idane);
        }
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso4($idane,$bit){
        $this->db->select("paa4.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo4_aviso paa4");
        $this->db->join("anexo4_aviso aa4","aa4.id=paa4.idanexo4_aviso");
            
        $this->db->where("paa4.activo",1);
        $this->db->where("paa4.acusado",0);
        $this->db->where("paa4.idanexo4_aviso",$idane);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa4.*, (select id from pagos_transac_anexo4 where id_pago_ayuda = pa4.id) as id_pago_ayuda");
        $this->db->from("anexo4_aviso aa4");
        $this->db->join("pagos_transac_anexo4 pa4","pa4.idanexo4=aa4.idanexo4","left");

        $this->db->where("pa4.activo",1);
        $this->db->where("pa4.acusado",1);
        if($bit==0){
            $this->db->where("aa4.id",$idane);
        }
        else{
            $this->db->where("aa4.idanexo4",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso5a($idane,$bit){
        $this->db->select("paa5.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo5a_aviso paa5");
        $this->db->join("anexo5a_aviso aa5","aa5.id=paa5.idanexo5a_aviso");
        $this->db->where("paa5.activo",1);
        $this->db->where("paa5.acusado",0);
        $this->db->where("aa5.liquidada",1); //se agrega condicion, solo operas liquidadas apoyan en sumatoria
        $this->db->where("paa5.idanexo5a_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa5.*, (select id from pagos_transac_anexo5a where id_pago_ayuda = pa5.id) as id_pago_ayuda");
        $this->db->from("anexo5a_aviso aa5");
        $this->db->join("pagos_transac_anexo5a pa5","pa5.idanexo5a=aa5.idanexo5a","left");
        $this->db->where("pa5.activo",1);
        $this->db->where("pa5.acusado",1);
        if($bit==0){
            $this->db->where("aa5.id",$idane);
        }
        else{
            $this->db->where("aa5.idanexo5a",$idane);
        }
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso5b($idane){
        $this->db->select("a5.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso"); //llamar los de terceros_5b con un alias mas 
        $this->db->join("anexo5b a5");
        $this->db->join('terceros_5b t5', 't5.id_anexo=a5.id and aviso=1','left');
        //$this->db->where("aa5.activo",1);
        $this->db->where("a5.acusado",0);
        $this->db->where("a5.id",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("aa5.*, aa5.id as id_pago_ayuda, '0' as pagos_aviso");
        $this->db->from("anexo5b_aviso aa5");
        //$this->db->join("pagos_transac_anexo5a pa5","pa5.idanexo5a=aa5.idanexo5a","left");
        //$this->db->where("pa5.activo",1);
        $this->db->where("aa5.acusado",1);
        $this->db->where("aa5.idanexo5b",$idane);
        $query2 = $this->db->get_compiled_select(); 
        
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso6($idane,$bit){
        $this->db->select("paa6.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo6_pago_aviso paa6");
        $this->db->join("anexo6_aviso aa6","aa6.id=paa6.idanexo6_aviso");
        $this->db->where("paa6.activo",1);
        $this->db->where("paa6.acusado",0);
        $this->db->where("paa6.idanexo6_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa6.*, (select id from pagos_transac_anexo6_pago where id_pago_ayuda = pa6.id) as id_pago_ayuda");
        $this->db->from("anexo6_aviso aa6");
        $this->db->join("pagos_transac_anexo6_pago pa6","pa6.idanexo6=aa6.idanexo6","left");
        $this->db->where("pa6.activo",1);
        $this->db->where("pa6.acusado",1);
        if($bit==0){
            $this->db->where("aa6.id",$idane);
        }
        else{
            $this->db->where("aa6.idanexo6",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso7($idane,$bit){
        $this->db->select("paa7.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo7_aviso paa7");
        $this->db->join("anexo7_aviso aa7","aa7.id=paa7.idanexo7_aviso");
        $this->db->where("paa7.activo",1);
        $this->db->where("paa7.acusado",0);
        $this->db->where("paa7.idanexo7_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa7.*, (select id from pagos_transac_anexo_7 where id_pago_ayuda = pa7.id) as id_pago_ayuda");
        $this->db->from("anexo7_aviso aa7");
        $this->db->join("pagos_transac_anexo_7 pa7","pa7.idanexo7=aa7.idanexo7","left");
        $this->db->where("pa7.activo",1);
        $this->db->where("pa7.acusado",1);
        if($bit==0){
            $this->db->where("aa7.id",$idane);
        }
        else{
            $this->db->where("aa7.idanexo7",$idane);
        }
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso($idane,$bit){
        $this->db->select("paa8.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo_8_aviso paa8");
        $this->db->join("anexo_8_aviso aa8","aa8.id=paa8.id_anexo_8_aviso");
            
        $this->db->where("paa8.status",1);
        $this->db->where("paa8.acusado",0);
        $this->db->where("paa8.id_anexo_8_aviso",$idane);

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa8.*, (select id from pagos_transac_anexo_8 where id_pago_ayuda = pa8.id) as id_pago_ayuda, '0' as pagos_aviso");
        $this->db->from("anexo_8_aviso aa8");
        $this->db->join("pagos_transac_anexo_8 pa8","pa8.id_anexo_8=aa8.id_anexo8","left");

        $this->db->where("pa8.status",1);
        $this->db->where("pa8.acusado",1);
        if($bit==0){
            $this->db->where("aa8.id",$idane);
        }
        else{
            $this->db->where("aa8.id_anexo8",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();

        /*SELECT `paa8`.*, '0' as id_pago_ayuda, '00' as id_ayuda
        FROM `pagos_transac_anexo_8_aviso` `paa8`
        join anexo_8_aviso aa8 on aa8.id=paa8.id_anexo_8_aviso

        where `paa8`.`status` = 1
        AND `paa8`.`acusado` = 0
        AND `paa8`.`id_anexo_8_aviso` = '5'

        union 

        SELECT  pa8.*, (select id from pagos_transac_anexo_8 where id_pago_ayuda = pa8.id) as id_pago_ayuda
        FROM `anexo_8_aviso` `aa8`
        left join pagos_transac_anexo_8 pa8 on pa8.id_anexo_8=aa8.id_anexo8
        where `pa8`.`status` = 1
        AND `pa8`.`acusado` = 1
        AND `aa8`.`id` = '5'*/
    } 
    public function getPagosOperaActAviso9($idane,$bit){
        $this->db->select("paa9.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo9_aviso paa9");
        $this->db->join("anexo9_aviso aa9","aa9.id=paa9.idanexo9_aviso");
        $this->db->where("paa9.activo",1);
        $this->db->where("paa9.acusado",0);
        $this->db->where("paa9.idanexo9_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa9.*, (select id from pagos_transac_anexo9 where id_pago_ayuda = pa9.id) as id_pago_ayuda");
        $this->db->from("anexo9_aviso aa9");
        $this->db->join("pagos_transac_anexo9 pa9","pa9.idanexo9=aa9.idanexo9","left");
        $this->db->where("pa9.activo",1);
        $this->db->where("pa9.acusado",1);
        if($bit==0){
            $this->db->where("aa9.id",$idane);
        }
        else{
            $this->db->where("aa9.idanexo9",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso10($idane,$bit){
        $this->db->select("paa10.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo10_aviso paa10");
        $this->db->join("anexo10_aviso aa10","aa10.id=paa10.idanexo10_aviso");
        $this->db->where("paa10.activo",1);
        $this->db->where("paa10.acusado",0);
        $this->db->where("paa10.idanexo10_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa10.*, (select id from pagos_transac_anexo10 where id_pago_ayuda = pa10.id) as id_pago_ayuda");
        $this->db->from("anexo10_aviso aa10");
        $this->db->join("pagos_transac_anexo10 pa10","pa10.idanexo10=aa10.idanexo10","left");
        $this->db->where("pa10.activo",1);
        $this->db->where("pa10.acusado",1);
        if($bit==0){
            $this->db->where("aa10.id",$idane);
        }
        else{
            $this->db->where("aa10.idanexo10",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso12a($idane,$tipo){
        $this->db->select("aa12a.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("anexo12a_aviso aa12a");
        //$this->db->join("anexo12a_aviso aa12a","aa12a.id=paa12a.idanexo10_aviso");
        if($tipo=="6"){
            //$this->db->join("pagos_transac_anexo12a_aviso paa12a","paa12a.idanexo12a_aviso=aa12a.idanexo12a","left");
        }
        $this->db->where("aa12a.activo",1);
        $this->db->where("aa12a.acusado",0);
        $this->db->where("aa12a.idanexo12a",$idane);

        $this->db->where("aa12a.tipo_actividad",$tipo); //agregado para traer solo las mismas operaciones del mismo tipo de act. vulnerable del 12a
        if($tipo=="6"){
            //$this->db->where("paa12a.activo",1);
            //$this->db->where("paa12a.acusado",0);
        }

        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("a12a.*, a12a.id as id_pago_ayuda, '0' as pagos_aviso");
        $this->db->from("anexo12a a12a");
        if($tipo=="6"){
            //$this->db->join("pagos_transac_anexo12a pa12a","pa12a.idanexo12a=a12a.id","left");
        }
        $this->db->where("a12a.activo",1);
        $this->db->where("a12a.acusado",1);
        $this->db->where("a12a.id",$idane);

        $this->db->where("a12a.tipo_actividad",$tipo); //agregado para traer solo las mismas operaciones del mismo tipo de act. vulnerable del 12a
        if($tipo=="6"){
            //$this->db->where("pa12a.activo",1);
            //$this->db->where("pa12a.acusado",1);
        }

        $query2 = $this->db->get_compiled_select(); 
        
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso12b($idane,$tipo){
        $this->db->select("aa12b.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("anexo12b_aviso aa12b");

        $this->db->where("aa12b.activo",1);
        $this->db->where("aa12b.acusado",0);
        $this->db->where("aa12b.idanexo12a",$idane);

        $this->db->where("aa12b.tipo_actividad",$tipo); //agregado para traer solo las mismas operaciones del mismo tipo de act. vulnerable del 12b
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("a12b.*, a12b.id as id_pago_ayuda, '0' as pagos_aviso");
        $this->db->from("anexo12b a12b");

        $this->db->where("a12b.activo",1);
        $this->db->where("a12b.acusado",1);
        $this->db->where("a12b.id",$idane);

        $this->db->where("a12b.tipo_actividad",$tipo); //agregado para traer solo las mismas operaciones del mismo tipo de act. vulnerable del 12b
        $query2 = $this->db->get_compiled_select(); 
        
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso13($idane,$bit){
        $this->db->select("paa13.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo13_num_aviso paa13");
        $this->db->join("anexo13_aviso aa13","aa13.id=paa13.idanexo13_aviso");
        $this->db->where("paa13.activo",1);
        $this->db->where("paa13.acusado",0);
        $this->db->where("paa13.idanexo13_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa13.*, (select id from pagos_transac_anexo13 where id_pago_ayuda = pa13.id) as id_pago_ayuda");
        $this->db->from("anexo13_aviso aa13");
        $this->db->join("pagos_transac_anexo13_num pa13","pa13.idanexo13=aa13.idanexo13","left");
        $this->db->where("pa13.activo",1);
        $this->db->where("pa13.acusado",1);
        if($bit==0){
            $this->db->where("aa13.id",$idane);
        }
        else{
            $this->db->where("aa13.idanexo13",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso15($idane,$bit){
        $this->db->select("paa15.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("pagos_transac_anexo_15_aviso paa15");
        $this->db->join("anexo_15_aviso aa15","aa15.id=paa15.id_anexo_15_aviso");
        $this->db->where("paa15.status",1);
        $this->db->where("paa15.acusado",0);
        $this->db->where("paa15.id_anexo_15_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa15.*, (select id from pagos_transac_anexo_15 where id_pago_ayuda = pa15.id) as id_pago_ayuda");
        $this->db->from("anexo_15_aviso aa15");
        $this->db->join("pagos_transac_anexo_15 pa15","pa15.id_anexo_15=aa15.id_anexo15","left");
        $this->db->where("pa15.status",1);
        $this->db->where("pa15.acusado",1);
        if($bit==0){
            $this->db->where("aa15.id",$idane);
        }
        else{
            $this->db->where("aa15.id_anexo15",$idane);
        }
        
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosOperaActAviso16($idane){
        $this->db->select("paa16.*, '0' as id_pago_ayuda, '00' as id_ayuda, '1' as pagos_aviso");
        $this->db->from("completar_anexo16_aviso paa16");
        $this->db->join("anexo16_aviso aa16","aa16.id=paa16.idanexo16_aviso");
        $this->db->where("paa16.Activo",1);
        $this->db->where("paa16.acusado",0);
        $this->db->where("paa16.idanexo16_aviso",$idane);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pa16.*, (select id from pagos_transac_anexo_15 where id_pago_ayuda = pa15.id) as id_pago_ayuda");
        $this->db->from("anexo16_aviso aa16");
        $this->db->join("completar_anexo16 pa16","pa16.idanexo16=aa16.idanexo16","left");
        $this->db->where("pa16.Activo",1);
        $this->db->where("pa16.acusado",1);
        if($bit==0){
            $this->db->where("aa16.id",$idane);
        }
        else{
            $this->db->where("aa16.idanexo16",$idane);
        }
        $query2 = $this->db->get_compiled_select(); 
        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
    public function getPagosAyuda($id){
        $this->db->select("pa8.*");
        $this->db->from("pagos_transac_anexo_8 pa8");
        $this->db->where("pa8.id",$id);
        
        $query=$this->db->get();
        return $query->result();

    } 
}    