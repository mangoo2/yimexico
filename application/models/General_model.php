<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		
	}

	public function GetAll($tabla){
		$query=$this->db->get($tabla);
		return $query->result();
	}

	public function GetAllWhere($tabla,$where){
		$this->db->where($where);
		$query=$this->db->get($tabla);
		return $query->result();
	}

    public function GetAllPagos($tabla,$where){
        $this->db->select("*");
        $this->db->from($tabla);
        $this->db->where($where);
        $this->db->order_by("fecha_pago");
        $query=$this->db->get();
        return $query->result();
    }

	public function get_record($id,$table){
		$this->db->select("*");
        $this->db->from($table);
        $this->db->where('id_cliente',$id);
        $query=$this->db->get();
        return $query->row();
    }

    public function get_tableRow($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_tablell($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }

    public function get_tableRowC($col,$table,$where){
        $this->db->select($col);
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_tableQuery($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }

    public function get_tableQueryRow($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_table($table){
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query;
    }

    public function get_tableECOI($id){
        $this->db->select('*');
        $this->db->from('tipo_cliente_e_c_o_i');
        $this->db->where('idperfilamiento',$id);
        $query = $this->db->get();
        return $query;
    }
    public function get_tableF($id){
        $this->db->select('*');
        $this->db->from('tipo_cliente_f');
        $this->db->where('idperfilamiento',$id);
        $query = $this->db->get();
        return $query;
    }
    public function get_tablePFE($id){
        $this->db->select('*');
        $this->db->from('tipo_cliente_p_f_e');
        $this->db->where('idperfilamiento',$id);
        $query = $this->db->get();
        return $query;
    }
    public function get_tablePFM($id){
        $this->db->select('*');
        $this->db->from('tipo_cliente_p_f_m');
        $this->db->where('idperfilamiento',$id);
        $query = $this->db->get();
        return $query;
    }
    public function get_tablePMMD($id){
        $this->db->select('*');
        $this->db->from('tipo_cliente_p_m_m_d');
        $this->db->where('idperfilamiento',$id);
        $query = $this->db->get();
        return $query;
    }
    public function get_tablePMME($id){
        $this->db->select('*');
        $this->db->from('tipo_cliente_p_m_m_e');
        $this->db->where('idperfilamiento',$id);
        $query = $this->db->get();
        return $query;
    }

    public function get_recordFoto($idp,$idcc,$table,$col,$id_opera){
        $this->db->select('IFNULL('.$col.', "0") as '.$col.'');
        $this->db->from($table);
        $this->db->where('id_perfilamiento',$idp);
        $this->db->where('id_clientec',$idcc);
        $cant_reg=2996; //productivo
        //$cant_reg=854; //calidad
        if($id_opera>$cant_reg){ //cant de registros en productivo
            $this->db->where('id_operacion',$id_opera);
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function get_recordFotoExtra($idp,$idcc,$table,$col,$col2,$id_opera){
        $this->db->select('IFNULL('.$col.', "0") as '.$col.', '.$col2.' ');
        $this->db->from($table);
        $this->db->where('id_perfilamiento',$idp);
        $this->db->where('id_clientec',$idcc);
        $cant_reg=2996;
        //$cant_reg=854; //calidad
        if($id_opera>$cant_reg){ //cant de registros en productivo
            $this->db->where('id_operacion',$id_opera);
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function get_docsBenes($table,$where,$col,$id_opera){
        $this->db->select('IFNULL('.$col.', "0") as '.$col.'');
        $this->db->from($table);
        $this->db->where($where);
        $cant_reg=2996; //productivo
        //$cant_reg=854; //calidad
        if($id_opera>$cant_reg){ //cant de registros en productivo
            $this->db->where('id_operacion',$id_opera);
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function get_docsExtraBenes($table,$where,$col,$col2,$id_opera){
        $this->db->select('IFNULL('.$col.', "0") as '.$col.', '.$col2.' ');
        $this->db->from($table);
        $this->db->where($where);
        $cant_reg=2996; //productivo
        //$cant_reg=854; //calidad
        if($id_opera>$cant_reg){ //cant de registros en productivo
            $this->db->where('id_operacion',$id_opera);
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function get_tipoCliente($table,$where){
        if($table=="tipo_cliente_p_f_m" || $table=="tipo_cliente_p_f_e")
            $sel="TIMESTAMPDIFF(MONTH, fecha_nacimiento, CURDATE()) AS edad";
        if($table=="tipo_cliente_p_m_m_e" || $table=="tipo_cliente_p_m_m_d")
            $sel="TIMESTAMPDIFF(MONTH, fecha_constitucion_d, CURDATE()) AS edad";
        if($table=="tipo_cliente_e_c_o_i")
            $sel="TIMESTAMPDIFF(MONTH, fecha_establecimiento, CURDATE()) AS edad";
        if($table=="tipo_cliente_f")
            $sel="";
        $this->db->select('*, '.$sel.'');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function GetAllWhere_group_by($tabla,$where,$id){
        $this->db->where($where);
        $this->db->group_by($id);
        $query=$this->db->get($tabla);

        return $query->result();
    }

    public function add_record($table,$data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($cos,$id,$data,$table){
        $this->db->set($data);
        $this->db->where($cos, $id);
        return $this->db->update($table);
    }
    
    public function delete_record($tabla,$where){
        $this->db->delete($tabla,$where);
    }

    

}

/* End of file General_model.php */
/* Location: ./application/models/General_model.php */