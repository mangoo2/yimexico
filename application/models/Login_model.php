<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Login_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function login($usuario){
    	$strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena, usu.idcliente, usu.tipo, cli.tipo_persona,usu.idcliente_usuario,cli.status, (SELECT usm.check_menu from usuarios_clientes_menu_sub as usm where usm.idcliente_usuario=usu.idcliente_usuario and id_menusub=15 group by idcliente_usuario) as permiso_avanza, cli.cambio_pass
                FROM usuarios as usu 
                INNER JOIN personal as per on per.personalId=usu.personalId
                LEFT JOIN cliente as cli on cli.idcliente=usu.idcliente
                LEFT JOIN usuarios_clientes as usc on usc.idcliente_usuario=usu.idcliente_usuario
                /*LEFT JOIN usuarios_clientes_menu_sub as usm on usm.idcliente_usuario=usu.idcliente_usuario*/
                where usu.Usuario ='".$usuario."'";
                //log_message('error', 'sql: '.$strq);
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getmenus_permiso($usuario){
        $strq ="SELECT * from usuarios_clientes_menu where idcliente_usuario=$usuario";
        $query = $this->db->query($strq);
        return $query->result();
    } 
    function getmenus_permiso_sub($usuario,$menu){
        $strq ="SELECT * from usuarios_clientes_menu_sub where idcliente_usuario=$usuario AND id_menu=$menu";
        $query = $this->db->query($strq);
        return $query->result();
    } 

    function getMenus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon,men.orden from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil'
                ORDER BY men.orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    } 

    function submenus($perfil,$menu){
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon,menus.activar,menus.orden from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function permisoadmin() {
        $strq = "SELECT * FROM usuarios WHERE perfilId=1";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function getpersona($personalId,$idcliente,$idcliente_usuario,$tipo){
        $strq ="";
        if($tipo==1){
            $strq = "SELECT p.nombre,pe.perfil, uc.funcion_puesto, u.idcliente_usuario,
                IFNULL(concat(pf.nombre,' ',pf.apellido_paterno,' ',pf.apellido_materno),'') as name_emp, IFNULL(pm.razon_social,'') as razon_social, IFNULL(pfi.denominacion_razon_social,'') as denominacion_razon_social
                FROM personal AS p
                INNER JOIN usuarios AS u ON u.personalId = p.personalId
                left JOIN cliente AS c ON c.idcliente = u.idcliente
                INNER JOIN perfiles AS pe ON pe.perfilId = u.perfilId
                left JOIN usuarios_clientes AS uc ON uc.idcliente_usuario = u.idcliente_usuario
                LEFT JOIN cliente_fisica AS pf ON pf.idcliente = c.idcliente
                LEFT JOIN cliente_moral AS pm ON pm.idcliente = c.idcliente
                LEFT JOIN cliente_fideicomiso AS pfi ON pfi.idcliente = c.idcliente
                WHERE p.personalId=$personalId AND u.tipo=1";    
        }else if($tipo==3){
            $strq = "SELECT pf.nombre,pf.apellido_paterno,pf.apellido_materno,pm.razon_social,pfi.denominacion_razon_social,pe.perfil, uc.funcion_puesto, u.idcliente_usuario
                FROM cliente AS c
                INNER JOIN usuarios AS u ON u.idcliente = c.idcliente
                INNER JOIN perfiles AS pe ON pe.perfilId = u.perfilId
                left JOIN usuarios_clientes AS uc ON uc.idcliente_usuario = u.idcliente_usuario
                LEFT JOIN cliente_fisica AS pf ON pf.idcliente = c.idcliente
                LEFT JOIN cliente_moral AS pm ON pm.idcliente = c.idcliente
                LEFT JOIN cliente_fideicomiso AS pfi ON pfi.idcliente = c.idcliente
                WHERE c.idcliente=$idcliente AND u.tipo=3";
        }else if($tipo==4){
            $strq = "SELECT uc.nombre,uc.apellido_paterno,uc.apellido_materno,pe.perfil, uc.funcion_puesto, uc.idcliente_usuario,
                IFNULL(concat(pf.nombre,' ',pf.apellido_paterno,' ',pf.apellido_materno),'') as name_emp, IFNULL(pm.razon_social,'') as razon_social, IFNULL(pfi.denominacion_razon_social,'') as denominacion_razon_social
                FROM usuarios_clientes AS uc
                INNER JOIN usuarios AS u ON u.idcliente_usuario = uc.idcliente_usuario
                left JOIN cliente AS c ON c.idcliente = u.idcliente
                INNER JOIN perfiles AS pe ON pe.perfilId = u.perfilId
                LEFT JOIN cliente_fisica AS pf ON pf.idcliente = c.idcliente
                LEFT JOIN cliente_moral AS pm ON pm.idcliente = c.idcliente
                LEFT JOIN cliente_fideicomiso AS pfi ON pfi.idcliente = c.idcliente
                WHERE uc.idcliente_usuario=$idcliente_usuario AND u.tipo=4";
        }
        $query = $this->db->query($strq);
        return $query->result();
    }
    ///////////////////////////////////////////////////////////////////////
    public function verificar_trasacciones($idcliente,$fechainicio,$fechafin){
        //var_dump($fecha.' 00:00:00');
        $strq = "SELECT * FROM perfilamiento AS p
                 LEFT JOIN docs_cliente AS d ON d.id_perfilamiento = p.idperfilamiento 
                 WHERE p.idcliente=$idcliente AND d.fecha BETWEEN '$fechainicio' AND '$fechafin' or p.idcliente = $idcliente";
                $query = $this->db->query($strq);
        return $query->result();
    }
}
//2020-03-18 00:00:00