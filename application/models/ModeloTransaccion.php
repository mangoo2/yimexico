<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloTransaccion extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
    //========= Anexo11 ============
    public function anexo11_constitucion_personas_morales_socios_t($tabla,$col,$id){
        $strq = "SELECT a.*,pa.pais AS pais,pa2.pais AS paism FROM $tabla AS a
                LEFT JOIN pais AS pa ON pa.clave = a.pais_nacionalidad
                LEFT JOIN pais AS pa2 ON pa2.clave = a.pais_nacionalidadm
                WHERE $col=$id AND a.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
    }
    //========= Anexo12a ============
    // tipo actividad 3
    public function anexo12a_constitucion_personas_morales_socios_t($tabla,$col,$id){
        $strq = "SELECT a.*,pa.pais AS pais_acc,pa2.pais AS paism_acc FROM $tabla AS a
                LEFT JOIN pais AS pa ON pa.clave = a.pais_nacionalidadco
                LEFT JOIN pais AS pa2 ON pa2.clave = a.pais_nacionalidadmco
                WHERE $col=$id AND a.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
    }
    // tipo actividad 3
    public function anexo12a_modificacion_patrimonial_socios_t($tabla,$col,$id){
        $strq = "SELECT a.*,pa.pais AS pais_acc,pa2.pais AS paism_acc FROM $tabla AS a
                LEFT JOIN pais AS pa ON pa.clave = a.pais_nacionalidad_acc
                LEFT JOIN pais AS pa2 ON pa2.clave = a.pais_nacionalidadm_acc
                WHERE $col=$id AND a.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
    }
    // tipo actividad 3
    public function anexo12a_accionistas_socios_t($tabla,$col,$id){
        $strq = "SELECT a.*,pa.pais AS pais,pa2.pais AS paism FROM $tabla AS a
                LEFT JOIN pais AS pa ON pa.clave = a.pais_nacionalidad
                LEFT JOIN pais AS pa2 ON pa2.clave = a.pais_nacionalidadm
                WHERE $col=$id AND a.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
    }
    // Anexo12b 
    public function anexo12b_constitucion_personas_morales_socios_t($tabla,$col,$id){
        $strq = "SELECT a.*,pa.pais AS pais,pa2.pais AS paism FROM $tabla AS a
                LEFT JOIN pais AS pa ON pa.clave = a.pais_nacionalidad
                LEFT JOIN pais AS pa2 ON pa2.clave = a.pais_nacionalidadm
                WHERE $col=$id AND a.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operaciones_transac1($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo1 pg1');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo1 pg12','pg12.id=pg1.id_pago_ayuda');
            $this->db->join('anexo1 a1','a1.id=pg12.idanexo1');
        }else{    
            $this->db->join('anexo1 a1','a1.id=pg1.idanexo1');
        }*/
         $this->db->join('anexo1 a1','a1.id=pg1.idanexo1');
        /*}else{
            $this->db->from('pagos_transac_anexo1_aviso pg1');
            $this->db->join('anexo1_aviso a1','a1.id=pg1.idanexo1_aviso'); 
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a1.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg1.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac2a($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo2a pg2a');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo2a pg22','pg22.id=pg2a.id_pago_ayuda');
            $this->db->join('anexo2a a2a','a2a.id=pg22.idanexo2a');
        }else{    
            $this->db->join('anexo2a a2a','a2a.id=pg2a.idanexo2a');
        }*/
        $this->db->join('anexo2a a2a','a2a.id=pg2a.idanexo2a'); 
        /*}else{
            $this->db->from('pagos_transac_anexo2a_aviso pg2a');
            $this->db->join('anexo2a_aviso a2a','a2a.id=pg2a.idanexo2a_aviso');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a2a.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg2a.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac2b($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo2b pg2b');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo2b pg22','pg22.id=pg2b.id_pago_ayuda');
            $this->db->join('anexo2b a2b','a2b.id=pg22.idanexo2b');
        }else{    
            $this->db->join('anexo2b a2b','a2b.id=pg2b.idanexo2b');
        }*/
        $this->db->join('anexo2b a2b','a2b.id=pg2b.idanexo2b');   
        /*}else{
            $this->db->from('pagos_transac_anexo2b_aviso pg2b');
            $this->db->join('anexo2b_aviso a2b','a2b.id=pg2b.idanexo2b_aviso');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a2b.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg2b.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac2c($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo2c pg2c');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo2c pg22','pg22.id=pg2c.id_pago_ayuda');
            $this->db->join('anexo2c a2c','a2c.id=pg22.idanexo2c');
        }else{    
            $this->db->join('anexo2c a2c','a2c.id=pg2c.idanexo2c');
        }*/
        $this->db->join('anexo2c a2c','a2c.id=pg2c.idanexo2c');   
        /*}else{
            $this->db->from('pagos_transac_anexo2c_aviso pg2c');
            $this->db->join('anexo2c_aviso a2c','a2c.id=pg2c.idanexo2c_Aviso');  
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a2c.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg2c.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac3($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo3 pg3');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo3 pg32','pg32.id=pg3.id_pago_ayuda');
            $this->db->join('anexo3 a3','a3.id=pg32.idanexo3');
        }else{    
            $this->db->join('anexo3 a3','a3.id=pg3.idanexo3');
        }*/
        $this->db->join('anexo3 a3','a3.id=pg3.idanexo3'); 
        /*}else{
            $this->db->from('pagos_transac_anexo3_aviso pg3');
            $this->db->join('anexo3_aviso a3','a3.id=pg3.idanexo3_aviso');   
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a3.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg3.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac4($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo4 pg4');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo4 pg42','pg42.id=pg4.id_pago_ayuda');
            $this->db->join('anexo4 a4','a4.id=pg42.idanexo4');
        }else{    
            $this->db->join('anexo4 a4','a4.id=pg4.idanexo4');
        }*/
        $this->db->join('anexo4 a4','a4.id=pg4.idanexo4');  
        /*}else{
            $this->db->from('pagos_transac_anexo4_aviso pg4');
            $this->db->join('anexo4_aviso a4','a4.id=pg4.idanexo4_aviso'); 
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a4.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg4.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac5a($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo5a pg5');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo5a pg52','pg52.id=pg5.id_pago_ayuda');
            $this->db->join('anexo5a a5','a5.id=pg52.idanexo5a');
        }else{    
            $this->db->join('anexo5a a5','a5.id=pg5.idanexo5a');
        }*/    
        $this->db->join('anexo5a a5','a5.id=pg5.idanexo5a');
        /*}else{
            $this->db->from('pagos_transac_anexo5a_aviso pg5');
            $this->db->join('anexo5a_aviso a5','a5.id=pg5.idanexo5a_aviso');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a5.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg5.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac5b($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('anexo5b a5');
        /*if($bit==0){
            $this->db->join('anexo5b a52','a52.id=a5.id_pago_ayuda');
        }*/
        /*else{    
            $this->db->from('anexo5b a5');
        } */
        /*}else{
            $this->db->from('anexo5b_aviso a5');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a5.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('a5.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac6($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo6_pago pg6');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo6_pago pg62','pg62.id=pg6.id_pago_ayuda');
            $this->db->join('anexo6 a6','a6.id=pg62.idanexo6');
        }else{    
            $this->db->join('anexo6 a6','a6.id=pg6.idanexo6');
        } */   
        $this->db->join('anexo6 a6','a6.id=pg6.idanexo6');
        /*}else{
            $this->db->from('pagos_transac_anexo6_pago_aviso pg6');
            $this->db->join('anexo6_aviso a6','a6.id=pg6.idanexo6_aviso'); 
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a6.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg6.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac7($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo_7 pg7');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo_7 pg72','pg72.id=pg7.id_pago_ayuda');
            $this->db->join('anexo7 a7','a7.id=pg72.idanexo7');
        }else{    
           $this->db->join('anexo7 a7','a7.id=pg7.idanexo7');
        } */    
        $this->db->join('anexo7 a7','a7.id=pg7.idanexo7');       
        /*}else{
            $this->db->from('pagos_transac_anexo7_aviso pg7');
            $this->db->join('anexo7_aviso a7','a7.id=pg7.idanexo7_aviso');  
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a7.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg7.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo_8 pg8');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo_8 pg82','pg82.id=pg8.id_pago_ayuda');
            $this->db->join('anexo_8 a8','a8.id=pg82.id_anexo_8');
        }else{    
            $this->db->join('anexo_8 a8','a8.id=pg8.id_anexo_8');
        }*/
        $this->db->join('anexo_8 a8','a8.id=pg8.id_anexo_8');
        /*}else{
           $this->db->from('pagos_transac_anexo_8_aviso pg8');
            $this->db->join('anexo_8_aviso a8','a8.id=pg8.id_anexo_8_aviso'); 
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a8.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        //$this->db->join('anexo_8_aviso a8avi','a8avi.id_anexo8=a8.id','left');
        $this->db->where('pg8.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac9($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo9 pg9');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo9 pg92','pg92.id=pg9.id_pago_ayuda');
            $this->db->join('anexo9 a9','a9.id=pg92.idanexo9');
        }else{
            $this->db->join('anexo9 a9','a9.id=pg9.idanexo9');
        }*/
        $this->db->join('anexo9 a9','a9.id=pg9.idanexo9');
        /*}else{
            $this->db->from('pagos_transac_anexo9_aviso pg9');
            $this->db->join('anexo9_aviso a9','a9.id=pg9.idanexo9_aviso');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a9.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg9.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac10($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo10 pg10');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo10 pg102','pg102.id=pg10.id_pago_ayuda');
            $this->db->join('anexo10 a10','a10.id=pg102.idanexo10');
        }else{
            $this->db->join('anexo10 a10','a10.id=pg10.idanexo10');
        }  */   
        $this->db->join('anexo10 a10','a10.id=pg10.idanexo10');  
        /*}else{
            $this->db->from('pagos_transac_anexo10_aviso pg10');
            $this->db->join('anexo10_aviso a10','a10.id=pg10.idanexo10_aviso'); 
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a10.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg10.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac12a($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        $this->db->from('anexo12a a12a');
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a12a.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('a12a.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }

    function get_operaciones_transac12aPagos($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo12a pg12a');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo12a pg122','pg122.id=pg12a.id_pago_ayuda');
            $this->db->join('anexo12a a12a','a12a.id=pg122.idanexo12a');
        }else{
            $this->db->join('anexo12a a12a','a12a.id=pg12a.idanexo12a');
        }*/
        $this->db->join('anexo12a a12a','a12a.id=pg12a.idanexo12a'); 
        /*}else{
            $this->db->from('pagos_transac_anexo12a_aviso pg12a');
            $this->db->join('anexo12a_aviso a12a','a12a.id=pg12a.idanexo12a_aviso');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a12a.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg12a.idanexo12a',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac12b($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        $this->db->from('anexo12b a12b');
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a12b.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('a12b.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac13($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo13_num pg13');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo13_num pg132','pg132.id=pg13.id_pago_ayuda');
            $this->db->join('anexo13 a13','a13.id=pg132.idanexo13');
        }else{
            $this->db->join('anexo13 a13','a13.id=pg13.idanexo13');
        }*/
        $this->db->join('anexo13 a13','a13.id=pg13.idanexo13');
        /*}else{
            $this->db->from('pagos_transac_anexo13_num_aviso pg13');
            $this->db->join('anexo13_aviso a13','a13.id=pg13.idanexo13_aviso');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a13.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg13.id',$id_anexo_act);
        //$this->db->or_where('pg13.id_pago_ayuda',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac15($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('pagos_transac_anexo_15 pg15');
        /*if($bit==0){
            $this->db->join('pagos_transac_anexo_15 pg152','pg152.id=pg15.id_pago_ayuda');
            $this->db->join('anexo_15 a15','a15.id=pg152.id_anexo_15');
        }else{
            $this->db->join('anexo_15 a15','a15.id=pg15.id_anexo_15');
        }*/
         $this->db->join('anexo_15 a15','a15.id=pg15.id_anexo_15');  
        /*}
        else{
            $this->db->from('pagos_transac_anexo_15_aviso pg15');
            $this->db->join('anexo_15_aviso a15','a15.id=pg15.id_anexo_15_aviso');   
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a15.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg15.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }
    function get_operaciones_transac16($id_anexo_act,$id_act,$aviso,$bit){
        $this->db->select('o.id as id_operacion');
        //if($aviso==0){
            $this->db->from('completar_anexo16 pg16');
        if($bit==0){
            $this->db->join('completar_anexo16 pg162','pg162.id=pg16.id_pago_ayuda');
            $this->db->join('anexo16 a16','a16.id=pg162.idanexo16');
        }else{
            $this->db->join('anexo16 a16','a16.id=pg16.idanexo16');
        }
        /*}else{
            $this->db->from('completar_anexo16_aviso pg16');
            $this->db->join('anexo16_aviso a16','a16.id=pg16.idanexo16_aviso');
        }*/
        $this->db->join('clientesc_transacciones ct','ct.id_transaccion=a16.id');
        $this->db->join('operaciones o', 'o.id_cliente_transac=ct.id');
        $this->db->join('operacion_cliente oc', 'oc.id_operacion=o.id');
        $this->db->where('pg16.id',$id_anexo_act);
        $this->db->group_by('o.id');
        $this->db->order_by('id_operacion DESC');
        $this->db->limit('1');
        $query=$this->db->get();
        return $query->row();
    }

    function GetPagosAvisoAyuda1($id_anexo){
        $this->db->select('paa1.*, pa1.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo1_aviso paa1');
        $this->db->join('pagos_transac_anexo1 pa1','pa1.id=paa1.idanexo1_aviso',"left");
        $this->db->join('anexo1_aviso aa1','aa1.idanexo1=pa1.idanexo1');
        $this->db->where('paa1.idanexo1_aviso',$id_anexo);
        $this->db->where('paa1.activo',1);
        $this->db->where('pa1.activo',1);
        $this->db->where('pa1.id_pago_ayuda',$id_anexo);

        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda2a($id_anexo){
        $this->db->select('paa2.*, pa2.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo2a_aviso paa2');
        $this->db->join('pagos_transac_anexo2a pa2','pa2.id=paa2.idanexo2a_aviso',"left");
        $this->db->join('anexo2a_aviso aa2','aa2.idanexo2a=pa2.idanexo2a');
        $this->db->where('paa2.idanexo2a_aviso',$id_anexo);
        $this->db->where('paa2.activo',1);
        $this->db->where('pa2.activo',1);
        $this->db->where('pa2.id_pago_ayuda',$id_anexo);

        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda2b($id_anexo){
        $this->db->select('paa2.*, pa2.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo2b_aviso paa2');
        $this->db->join('pagos_transac_anexo2b pa2','pa2.id=paa2.idanexo2b_aviso',"left");
        $this->db->join('anexo2b_aviso aa2','aa2.idanexo2b=pa2.idanexo2b');
        $this->db->where('paa2.idanexo2b_aviso',$id_anexo);
        $this->db->where('paa2.activo',1);
        $this->db->where('pa2.activo',1);
        $this->db->where('pa2.id_pago_ayuda',$id_anexo);

        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda2c($id_anexo){
        $this->db->select('paa2.*, pa2.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo2c_aviso paa2');
        $this->db->join('pagos_transac_anexo2c pa2','pa2.id=paa2.idanexo2c_Aviso',"left");
        $this->db->join('anexo2c_aviso aa2','aa2.idanexo2c=pa2.idanexo2c');
        $this->db->where('paa2.idanexo2c_aviso',$id_anexo);
        $this->db->where('paa2.activo',1);
        $this->db->where('pa2.activo',1);
        $this->db->where('pa2.id_pago_ayuda',$id_anexo);

        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda3($id_anexo){
        $this->db->select('paa3.*, pa3.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo3_aviso paa3');
        $this->db->join('pagos_transac_anexo3 pa3','pa3.id=paa3.idanexo3_aviso',"left");
        $this->db->join('anexo3_aviso aa3','aa3.idanexo3=pa3.idanexo3');
        $this->db->where('paa3.idanexo3_aviso',$id_anexo);
        $this->db->where('paa3.activo',1);
        $this->db->where('pa3.activo',1);
        $this->db->where('pa3.id_pago_ayuda',$id_anexo);

        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda4($id_anexo){
        $this->db->select('paa4.*, pa4.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo4_aviso paa4');
        $this->db->join('pagos_transac_anexo4 pa4','pa4.id=paa4.idanexo4_aviso',"left");
        $this->db->join('anexo4_aviso aa4','aa4.idanexo4=pa4.idanexo4');
        $this->db->where('paa4.idanexo4_aviso',$id_anexo);
        $this->db->where('paa4.activo',1);
        $this->db->where('pa4.activo',1);
        $this->db->where('pa4.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda5a($id_anexo){
        $this->db->select('paa5.*, pa5.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo5a_aviso paa5');
        $this->db->join('pagos_transac_anexo5a pa5','pa5.id=paa5.idanexo5a_aviso',"left");
        $this->db->join('anexo5a_aviso aa5','aa5.idanexo5a=pa5.idanexo5a');
        $this->db->where('paa5.idanexo5a_aviso',$id_anexo);
        $this->db->where('paa5.activo',1);
        $this->db->where('pa5.activo',1);
        $this->db->where('pa5.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda5b($id_anexo){
        $this->db->select('a5.*, a5.id_pago_ayuda');
        $this->db->from('anexo5b a5');
        $this->db->join('anexo5b_aviso aa5','aa5.id_anexo5b=a5.id');
        $this->db->where('aa5.id_anexo5b',$id_anexo);
        /*$this->db->where('paa5.activo',1);
        $this->db->where('pa5.activo',1);*/
        $this->db->where('a5.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda6($id_anexo){
        $this->db->select('paa6.*, pa6.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo6_pago_aviso paa6');
        $this->db->join('pagos_transac_anexo6_pago pa6','pa6.id=paa6.idanexo6_aviso',"left");
        $this->db->join('anexo6_aviso aa6','aa6.idanexo6=pa6.idanexo6');
        $this->db->where('paa6.idanexo6_aviso',$id_anexo);
        $this->db->where('paa6.activo',1);
        $this->db->where('pa6.activo',1);
        $this->db->where('pa6.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda7($id_anexo){
        $this->db->select('paa7.*, pa7.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo7_aviso paa7');
        $this->db->join('pagos_transac_anexo_7 pa7','pa7.id=paa7.idanexo7_aviso',"left");
        $this->db->join('anexo7_aviso aa7','aa7.idanexo7=pa7.idanexo7');
        $this->db->where('paa7.idanexo7_aviso',$id_anexo);
        $this->db->where('paa7.activo',1);
        $this->db->where('pa7.activo',1);
        $this->db->where('pa7.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    
    function GetPagosAvisoAyuda($id_anexo){
        $this->db->select('paa8.*, pa8.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo_8_aviso paa8');
        $this->db->join('pagos_transac_anexo_8 pa8','pa8.id=paa8.id_anexo_8_aviso',"left");
        $this->db->join('anexo_8_aviso aa8','aa8.id_anexo8=pa8.id_anexo_8');
        $this->db->where('paa8.id_anexo_8_aviso',$id_anexo);
        $this->db->where('paa8.status',1);
        $this->db->where('pa8.status',1);
        $this->db->where('pa8.id_pago_ayuda',$id_anexo);

        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda9($id_anexo){
        $this->db->select('paa9.*, pa9.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo9_aviso paa9');
        $this->db->join('pagos_transac_anexo9 pa9','pa9.id=paa9.idanexo9_aviso',"left");
        $this->db->join('anexo9_aviso aa9','aa9.idanexo9=pa9.idanexo9');
        $this->db->where('paa9.idanexo9_aviso',$id_anexo);
        $this->db->where('paa9.activo',1);
        $this->db->where('pa9.activo',1);
        $this->db->where('pa9.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda10($id_anexo){
        $this->db->select('paa10.*, pa10.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo10_aviso paa10');
        $this->db->join('pagos_transac_anexo10 pa10','pa10.id=paa10.idanexo10_aviso',"left");
        $this->db->join('anexo10_aviso aa10','aa10.idanexo10=pa10.idanexo10');
        $this->db->where('paa10.idanexo10_aviso',$id_anexo);
        $this->db->where('paa10.activo',1);
        $this->db->where('pa10.activo',1);
        $this->db->where('pa10.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda12a($id_anexo){
        $this->db->select('a12a.*, a12a.id_pago_ayuda');
        $this->db->from('anexo12a a12a');
        $this->db->join('anexo12a_aviso aa12a','aa12a.idanexo12a=a12a.id',"left");
        /*$this->db->from('pagos_transac_anexo12a_aviso paa12a');
        $this->db->join('pagos_transac_anexo12a pa12a','pa12a.id=paa12a.idanexo12a_aviso');
        $this->db->join('anexo12a_aviso aa12a','aa12a.idanexo12a=pa12a.idanexo12a');*/
        $this->db->where('aa12a.idanexo12a',$id_anexo);
        $this->db->where('aa12a.activo',1);
        $this->db->where('a12a.activo',1);
        $this->db->where('a12a.id_pago_ayuda',$id_anexo);
        $this->db->or_where('a12a.id_pago_ayude',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda12b($id_anexo){
        $this->db->select('a12b.*, a12b.id_pago_ayuda');
        $this->db->from('anexo12b a12b');
        $this->db->join('anexo12b_aviso aa12b','aa12b.idanexo12b=a12b.id',"left");
        /*$this->db->from('pagos_transac_anexo12a_aviso paa12a');
        $this->db->join('pagos_transac_anexo12a pa12a','pa12a.id=paa12a.idanexo12a_aviso');
        $this->db->join('anexo12a_aviso aa12a','aa12a.idanexo12a=pa12a.idanexo12a');*/
        $this->db->where('aa12b.idanexo12b',$id_anexo);
        $this->db->where('aa12b.activo',1);
        $this->db->where('a12b.activo',1);
        $this->db->where('a12b.id_pago_ayuda',$id_anexo);
        $this->db->or_where('a12b.id_pago_ayude',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda13($id_anexo){
        $this->db->select('paa13.*, pa13.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo13_num_aviso paa13');
        $this->db->join('pagos_transac_anexo13_num pa13','pa13.id=paa13.idanexo13_aviso',"left");
        $this->db->join('anexo13_aviso aa13','aa13.idanexo13=pa13.idanexo13');
        $this->db->where('paa13.idanexo13_aviso',$id_anexo);
        $this->db->where('paa13.activo',1);
        $this->db->where('pa13.activo',1);
        $this->db->where('pa13.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda15($id_anexo){
        $this->db->select('paa15.*, pa15.id_pago_ayuda');
        $this->db->from('pagos_transac_anexo_15_aviso paa15');
        $this->db->join('pagos_transac_anexo_15 pa15','pa15.id=paa15.id_anexo_15_aviso',"left");
        $this->db->join('anexo_15_aviso aa15','aa15.id_anexo15=pa15.id_anexo_15');
        $this->db->where('paa15.id_anexo_15_aviso',$id_anexo);
        $this->db->where('paa15.status',1);
        $this->db->where('pa15.status',1);
        $this->db->where('pa15.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }
    function GetPagosAvisoAyuda16($id_anexo){
        $this->db->select('paa16.*, pa16.id_pago_ayuda');
        $this->db->from('completar_anexo16_aviso paa16');
        $this->db->join('completar_anexo16 pa16','pa16.id=paa16.idanexo16_aviso',"left");
        $this->db->join('anexo16_aviso aa16','aa16.idanexo16=pa16.idanexo16');
        $this->db->where('paa16.idanexo16_aviso',$id_anexo);
        $this->db->where('paa16.Activo',1);
        $this->db->where('pa16.Activo',1);
        $this->db->where('pa16.id_pago_ayuda',$id_anexo);
        $query=$this->db->get();
        return $query->row();
    }

    function getGradoCli($id,$ido){
        $this->db->select('IFNULL(grado,"0") as grado, id_actividad');
        $this->db->from('grado_riesgo_actividad gra');
        $this->db->where('gra.id_perfilamiento',$id);
        $this->db->where('gra.id_operacion',$ido);
        $query=$this->db->get();
        return $query->result();
    }


    function getPagosAnexo($id_anexo){ //convertila en 2 cols, union
        /*$this->db->select('DISTINCT(pa5.idanexo5a), pa52.idanexo5a as idanexo5a_2');
        $this->db->from('pagos_transac_anexo5a pa5');
        $this->db->join('pagos_transac_anexo5a pa52','pa52.idanexo5a=pa5.id_pago_ayuda and pa52.activo=1','left');
        $this->db->join('pagos_transac_anexo5a pa53','pa53.idanexo5a=pa5.id_pago_ayude and pa53.activo=1','left');
        $this->db->where('pa5.idanexo5a',$id_anexo);
        $this->db->where('pa5.activo',1);
        $query=$this->db->get();
        return $query->result();*/

        $this->db->select('DISTINCT(IFNULL(pa5.idanexo5a,0)) as idanexo5a');
        $this->db->from('pagos_transac_anexo5a pa5');
        $this->db->where('pa5.id_pago_ayude',$id_anexo);
        $this->db->where('pa5.activo',1);
        $query = $this->db->get_compiled_select(); 

        $this->db->select('IFNULL(pa52.idanexo5a,0) as idanexo5a');
        $this->db->from('pagos_transac_anexo5a pa5');
        $this->db->join('pagos_transac_anexo5a pa52','pa52.idanexo5a=pa5.id_pago_ayuda and pa52.activo=1 and pa5.idanexo5a !='.$id_anexo.'','left');
        $this->db->where('pa5.idanexo5a',$id_anexo);
        $this->db->where('pa5.activo',1);
        $query2 = $this->db->get_compiled_select();

        $queryunions=$query." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }

    function getPagosAnexo5b($id_anexo){
        $this->db->select('DISTINCT(IFNULL(pa5.id_anexo,0)) as id_anexo');
        $this->db->from('terceros_5b pa5');
        $this->db->where('pa5.id_pago_ayude',$id_anexo);
        $this->db->where('pa5.estatus',1);
        $query = $this->db->get_compiled_select(); 

        $this->db->select('IFNULL(pa52.id_anexo,0) as id_anexo');
        $this->db->from('terceros_5b pa5');
        $this->db->join('terceros_5b pa52','pa52.id_anexo=pa5.id_pago_ayuda and pa52.estatus=1 and pa5.id_anexo !='.$id_anexo.'','left');
        $this->db->where('pa5.id_anexo',$id_anexo);
        $this->db->where('pa5.estatus',1);
        $query2 = $this->db->get_compiled_select();

        /*$this->db->select('pa53.id_anexo');
        $this->db->from('terceros_5b pa5');
        $this->db->join('terceros_5b pa53','pa53.id_pago_ayuda=pa5.id_pago_ayuda and pa53.estatus=1','left');
        $this->db->where('pa5.id_anexo',$id_anexo);
        $this->db->where('pa5.estatus',1);
        $query3 = $this->db->get_compiled_select();*/

        $queryunions=$query." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }
}