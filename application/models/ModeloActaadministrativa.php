<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloActaadministrativa extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
    ///=======Acta administrativa
    function getactaadministrativa($params,$perfil,$sucursal){
        $columns = array( 
            0=>'a.idacta',
            2=>'p.personalId',
            3=>'p.nombre',
            4=>'a.fecha',
            5=>'a.incidencia',
            6=>'a.comentarios',
            7=>'a.foto',
            8=>'a.tipo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('actas_administrativas a');
        $this->db->join('personal p', 'p.personalId=a.personalId');
            if($perfil==1){
                $where = array('a.activo'=>1);
            }else{
                $where = array('a.activo'=>1,'p.idsucursal'=>$sucursal);
            }

            $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_actaadministrativa($params,$perfil,$sucursal){
        $this->db->select('COUNT(1) as total');
        $this->db->from('actas_administrativas a');
        $this->db->join('personal p', 'p.personalId=a.personalId');
            if($perfil==1){
                $where = array('a.activo'=>1);
            }else{
                $where = array('a.activo'=>1,'p.idsucursal'=>$sucursal);
            }
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///=======Carta permisos
    function getcartapermisos($params,$perfil,$sucursal){
        $columns = array( 
            0=>'c.idcarta',
            2=>'p.personalId',
            3=>'p.nombre',
            4=>'c.fecha',
            5=>'c.comentario',
            6=>'pe.nombre AS capturista',
            7=>'c.foto',
            8=>'c.tipo',
        );
        $columns2 = array( 
            0=>'c.idcarta',
            2=>'p.personalId',
            3=>'p.nombre',
            4=>'c.fecha',
            5=>'c.comentario',
            6=>'pe.nombre',
            7=>'c.foto',
            8=>'c.tipo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('carta_compromiso c');
        $this->db->join('personal p', 'p.personalId=c.personalId');
        $this->db->join('personal pe', 'pe.personalId=c.personalId_usuario');
            if($perfil==1){
                $where = array('c.activo'=>1);
            }else{
                $where = array('c.activo'=>1,'p.idsucursal'=>$sucursal);
            }

            $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_cartapermiso($params,$perfil,$sucursal){
        $this->db->select('COUNT(1) as total');
        $this->db->from('carta_compromiso c');
        $this->db->join('personal p', 'p.personalId=c.personalId');
        $this->db->join('personal pe', 'pe.personalId=c.personalId_usuario');
            if($perfil==1){
                $where = array('c.activo'=>1);
            }else{
                $where = array('c.activo'=>1,'p.idsucursal'=>$sucursal);
            }
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///======= Constancia hechos
    function getconstanciahechos($params,$perfil,$sucursal){
        $columns = array( 
            0=>'c.idconstancia',
            2=>'p.personalId',
            3=>'p.nombre',
            4=>'c.fecha',
            5=>'c.comentario',
            6=>'pe.nombre AS jefe',
            7=>'c.foto',
            8=>'c.tipo',
        );
        $columns2 = array( 
            0=>'c.idconstancia',
            2=>'p.personalId',
            3=>'p.nombre',
            4=>'c.fecha',
            5=>'c.comentario',
            6=>'pe.nombre',
            7=>'c.foto',
            8=>'c.tipo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('constaciahechos c');
        $this->db->join('personal p', 'p.personalId=c.personalId');
        $this->db->join('personal pe', 'pe.personalId=c.personalId_encargado');
            if($perfil==1){
                $where = array('c.activo'=>1);
            }else{
                $where = array('c.activo'=>1,'p.idsucursal'=>$sucursal);
            }

            $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_constanciahechos($params,$perfil,$sucursal){
        $this->db->select('COUNT(1) as total');
        $this->db->from('constaciahechos c');
        $this->db->join('personal p', 'p.personalId=c.personalId');
        $this->db->join('personal pe', 'pe.personalId=c.personalId_encargado');
            if($perfil==1){
                $where = array('c.activo'=>1);
            }else{
                $where = array('c.activo'=>1,'p.idsucursal'=>$sucursal);
            }
            $this->db->where($where);
        $query=$this->db->get();
        return $query->row()->total;
    }
   

}
