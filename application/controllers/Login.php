<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
    function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        
        //$this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        //$this->fechahoy = date('Y-m-d');
        $this->horahoy = date('g:i a');
        //$this->fecha = date('d-m-Y');
        $this->fecha = date('Y');
    }
	public function index()	{
            $data['fecha']=$this->fecha; 
            $this->load->view('login2/header');
            $this->load->view('login2/index',$data);
            $this->load->view('login2/footer');
            $this->load->view('login/login_js');  
	}   
    public function session(){
        // Obtenemos el usuario y la contraseña ingresados en el formulario
        $username = $this->input->post('usu');
        $password = $this->input->post('passw');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario
        //log_message('error', 'usuario: '.$username);
        $respuesta = $this->Login_model->login($username);
        $contrasena='';
        foreach ($respuesta as $item) {
            $contrasena =$item->contrasena;
        }
        // Verificamos si las contraseñas son iguales
        //log_message('error', 'contra1: '.$password);
        //log_message('error', 'contra2: '.$contrasena);
        $verificar = password_verify($password,$contrasena);

        // En caso afirmativo, inicializamos datos de sesión
        if ($verificar) 
        {
            $data = array(
                'logeado' => true,
                'usuarioid' => $respuesta[0]->UsuarioID,
                'usuario' => $respuesta[0]->nombre,
                'perfilid'=>$respuesta[0]->perfilId,
                'idpersonal'=>$respuesta[0]->personalId,
                'idcliente'=>$respuesta[0]->idcliente,
                'tipo'=>$respuesta[0]->tipo,
                'tipo_persona'=>$respuesta[0]->tipo_persona,
                'idcliente_usuario'=>$respuesta[0]->idcliente_usuario,
                'status'=>$respuesta[0]->status,
                'permiso_avanza' => $respuesta[0]->permiso_avanza,
                'cambio_pass' => $respuesta[0]->cambio_pass,
                'token_ws' => "0"
            );
            $this->session->set_userdata($data);
            $count=1;
        }
        // Devolvemos la respuesta
        echo $count;
        
    }    
    public function cerrar_sesion() 
    {
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
}
