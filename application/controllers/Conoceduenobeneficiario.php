<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conoceduenobeneficiario extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('General_model');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
    if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        /*
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        $this->idcliente_usuario=$this->session->userdata('idcliente_usuario');
        */
    }
  }
  public function index($id){      
      
  }
  function doc($id){
    $idtipo_cliente=0;
    $idtipo_cliente=0;
    $data['tipocliente']='';
    $data['nombrebeneficiario']='';
    $data['nombrebeneficiario']='';
    $data['nombre_identificacion']='';
    $data['numero_identificacion']='';
    $data['fecha_nacimiento']='';
    $data['pais_nacimiento']='';
    $data['pais_nacionalidad']='';
    $data['domicilio']='';
    $data['correo_d']='';
    $data['r_f_c_d']='';
    $data['curp_d']='';
    $id_cliente=0;
    $resultd=$this->ModeloCatalogos->getselectwhere('clientes_operaciones','id',$id);
    foreach ($resultd as $item) {
      $id_duenio_bene=$item->id_duenio_bene;
      $id_cliente=$item->id_cliente;
    }
    $resultdb=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_duenio_bene);
    foreach ($resultdb as $item) {
      $idtipo_cliente=$item->idtipo_cliente;
      $data['nombrebeneficiario']=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
      $data['nombre_identificacion']=$item->nombre_identificacion;
      $data['numero_identificacion']=$item->numero_identificacion;
      $data['fecha_nacimiento']=$item->fecha_nacimiento;
      $data['pais_nacimiento']=$item->pais_nacimiento;
      $data['pais_nacionalidad']=$item->pais_nacionalidad;
      $data['domicilio']=$item->calle.' No Ext. '.$item->no_ext.' No Int. '.$item->no_int.' Colonia. '.$item->colonia.' Municipio '.$item->monicipio.' Localidad '.$item->localidad.' Estado '.$item->estado.' C. P. '.$item->cp;
      $data['correo_d']=$item->correo_d;
      $data['r_f_c_d']=$item->r_f_c_d;
      $data['curp_d']=$item->curp_d;
    }
    $resultpp=$this->ModeloCatalogos->getselectwhere('perfilamiento','idperfilamiento',$idtipo_cliente);
    foreach ($resultpp as $item) {
      $idtipo_cliente=$item->idtipo_cliente;
    }
    $resultpps=$this->ModeloCatalogos->getselectwhere('tipo_cliente','idtipo_cliente',$idtipo_cliente);
    foreach ($resultpps as $item) {
      $data['tipocliente']=$item->tipo;
    }
    $this->load->view('Reportes/conoceduenobeneficiario',$data);
  }

}