<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
	
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        //ira el permiso del modulo
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,3);// 2 es el id del submenu
        if ($permiso==0) {
          //redirect('/Sistema'); //comentado para pruebas 
        }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
  
	public function index(){
		$data['act']=$this->General_model->GetAll('actividad');
		$data['sact']=$this->General_model->GetAll('sub_actividad');
		$this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/listado',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jslistado');
	}

  /*public function datatable_records(){
    $get_actividad=$this->ModeloCliente->get_clientes_all($this->idcliente);
    //$clientes = $this->ModeloCliente->get_tableClientes($this->idcliente);
    $json_data = array("data" => $clientesc);
    echo json_encode($json_data);
  }*/

	public function add($id=0){
		//$data['act']=$this->General_model->GetAll('actividad');
		$data['sact']=$this->General_model->GetAll('sub_actividad');
    $data['act']=$this->ModeloCliente->getActividadesCliente_all();
    ////Tabla usuarios////
    $data['UsuarioID']=0;
    $data['Usuario']='';
    $data['contrasena']='';
    if($id==0){
    ////////////=====////////////
      $data['titulo']='Nuevo';  
      $data['boton']='Guardar';
      ////Tabla clientes////
      $data['idcliente']=0;
      $data['tipo_persona']='';
      //$data['actividad_vulnerable']=0;
      ////Tabla contactos////
      $data['idcontacto']=0;
      $data['nombre']='';
      $data['apellido_paterno']='';
      $data['apellido_materno']='';
      $data['fecha_inicio']='';
      $data['telefono']='';
      $data['celular']='';
      $data['correo']='';
     
    //////////=====//////////
    }else{
    //////////=====/////////
      $data['titulo']='Edición de';
      $data['boton']='Editar';
      $result_cliente = $this->ModeloCatalogos->getselectwhere('cliente','idcliente',$id);
      foreach ($result_cliente as $item) {
        $data['idcliente']=$item->idcliente;
        $data['tipo_persona']=$item->tipo_persona;
      //  $data['actividad_vulnerable']=$item->actividad_vulnerable;
      }
      $result_contacto = $this->ModeloCatalogos->getselectwhere('cliente_contacto','idcliente',$id);
      foreach ($result_contacto as $item) {
        $data['idcontacto']=$item->idcontacto;
        $data['nombre']=$item->nombre;
        $data['apellido_paterno']=$item->apellido_paterno;
        $data['apellido_materno']=$item->apellido_materno;
        $data['fecha_inicio']=$item->fecha_inicio;
        $data['telefono']=$item->telefono;
        $data['celular']=$item->celular;
        $data['correo']=$item->correo;
      }
      $array_usu = array('idcliente' =>$id,'tipo'=>3);
      $result_user=$this->ModeloCatalogos->getselectwherestatus('*','usuarios',$array_usu);
      foreach ($result_user as $item){
          $data['UsuarioID']=$item->UsuarioID;
          $data['Usuario']=$item->Usuario;
          $data['contrasena']='xxxxxx_xxxx';
      }
    /////////=====////////   
    }
    //log_message('error', ' UsuarioID : '.$data['UsuarioID']);
    //log_message('error', ' Usuario : '.$data['Usuario']);
   	$this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/addcliente',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jsCliente');
	}
  public function registro_cliente(){   
      $data = $this->input->post();

      $id = $data['idcliente'];
      unset($data['idcliente']);
      $aux=0;
      if ($id>0) {
          $this->ModeloCatalogos->updateCatalogo($data,'idcliente',$id,'cliente');
          $result=2;
          $aux=$id;
      }else{
          $data['fecha_alta_sistema']=$this->fechaactual;
          $aux=$this->ModeloCatalogos->tabla_inserta('cliente',$data);
          $result=1;
      }   
      $arraydata = array('id'=>$aux,'status'=>$result);
      echo json_encode($arraydata);         
  }
  public function registro_fisica(){   
    $data = $this->input->post();
    $id = $data['idfisica'];
    unset($data['idfisica']);
    $aux=0;
    if ($id>0) {
      $this->ModeloCatalogos->updateCatalogo($data,'idfisica',$id,'cliente_fisica');
      $result=2;
      $aux=$id;
    }else{
      $aux=$this->ModeloCatalogos->tabla_inserta('cliente_fisica',$data);
      $result=1;
    } 
    echo $aux;      
  }
  public function registro_moral(){   
    $data = $this->input->post();
    $id = $data['idmoral'];
    unset($data['idmoral']);
    $aux=0;
    if ($id>0) {
      $this->ModeloCatalogos->updateCatalogo($data,'idmoral',$id,'cliente_moral');
      $result=2;
      $aux=$id;
    }else{
      $aux=$this->ModeloCatalogos->tabla_inserta('cliente_moral',$data);
      $result=1;
    }
    echo $aux;      
  }
  public function registro_fideicomiso(){   
    $data = $this->input->post();
    $id = $data['idfideicomiso'];
    unset($data['idfideicomiso']);
    $aux=0;
    if ($id>0) {
      $this->ModeloCatalogos->updateCatalogo($data,'idfideicomiso',$id,'cliente_fideicomiso');
      $result=2;
      $aux=$id;
    }else{
      $aux=$this->ModeloCatalogos->tabla_inserta('cliente_fideicomiso',$data);
      $result=1;
    }
    echo $aux;      
  }
  public function registro_contacto(){   
      $data = $this->input->post();
      $id = $data['idcontacto'];
      unset($data['idcontacto']);
      $aux=0;
      if ($id>0) {
          $this->ModeloCatalogos->updateCatalogo($data,'idcontacto',$id,'cliente_contacto');
          $result=2;
          $aux=$id;
      }else{
          $aux=$this->ModeloCatalogos->tabla_inserta('cliente_contacto',$data);
          $result=1;
      }  
      echo $aux;     
  }
  public function registro_user(){ 
    $data=$this->input->post();
    $id=$data['UsuarioID'];
    unset($data['UsuarioID']);
    $pass = $data['contrasena'];
    if ($pass=='xxxxxx_xxxx') {
            unset($data['contrasena']);
    }else{
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena']=$pass;
    } 
    $aux=0;
    if ($id>0) {
        $this->ModeloCatalogos->updateCatalogo($data,'UsuarioID',$id,'usuarios');
    }else{
        $data['perfilId']=3;//Clientes
        $data['tipo']=3;//clientes
        $data['personalId']=$this->idpersonal;
        $this->ModeloCatalogos->tabla_inserta('usuarios',$data);
    }
  }
	public function GetSubAct(){
		$id=$this->input->post('id');
		$data=$this->General_model->GetAllWhere('sub_actividad','idActividad = '.$id);
		echo json_encode($data);
	}
	public function searchpais(){
        $search = $this->input->get('search');
        $results=$this->ModeloCliente->get_select_like_pais($search);
        echo json_encode($results);    
  }
  //// Funcion para saber el tipo de persona que esta seleccionando
  public function tipo_persona(){  
      $tipo = $this->input->post('id');
      $idcli = $this->input->post('idcli');
      ////Tabla persona_fisica////
      $idfisica=0;
      $nombre='';
      $apellido_paterno='';
      $apellido_materno='';
      $rfc='';
      $actividad_o_giro='';
      $otra_ocupa='';
      ////Tabla persona_moral////
      $idmoral=0;
      $razon_social='';
      $r_c_f='';
      ////Tabla persona_fideicomiso////
      $idfideicomiso=0;
      $denominacion_razon_social='';
      $rfc_fideicomiso='';
      ////////////=====////////////
      $result_fisica = $this->ModeloCatalogos->getselectwhere('cliente_fisica','idcliente',$idcli);
      foreach ($result_fisica as $item) {
        $idfisica=$item->idfisica;
        $nombre=$item->nombre;
        $apellido_paterno=$item->apellido_paterno;
        $apellido_materno=$item->apellido_materno;
        $rfc=$item->rfc;
        $actividad_o_giro=$item->actividad_o_giro;
        $otra_ocupa=$item->otra_ocupa;
      }
      $result_moral = $this->ModeloCatalogos->getselectwhere('cliente_moral','idcliente',$idcli);
      foreach ($result_moral as $item) {
        $idmoral=$item->idmoral;
        $razon_social=$item->razon_social;
        $r_c_f=$item->r_c_f;
        $actividad_o_giro=$item->actividad_o_giro;
        $otra_ocupa=$item->otra_ocupa;
      }
      $result_fideicomiso = $this->ModeloCatalogos->getselectwhere('cliente_fideicomiso','idcliente',$idcli);
      foreach ($result_fideicomiso as $item) {
        $idfideicomiso=$item->idfideicomiso;
        $denominacion_razon_social=$item->denominacion_razon_social;
        $rfc_fideicomiso=$item->rfc_fideicomiso;
        $actividad_o_giro=$item->actividad_o_giro;
        $otra_ocupa=$item->otra_ocupa;
      }
  
      $html = '';
      if($tipo==1){/// Persona fisica
        $get_actividad_fisica=$this->ModeloCatalogos->getData('actividad_econominica_fisica');
        $html.='<hr class="subtitle">
              <form class="form" method="post" role="form" id="form_fisica">      
                  <input type="hidden" name="idfisica" value="'.$idfisica.'">
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Nombre(s):</label>
                    <input onchange="preGuardar()" class="form-control" type="text" name="nombre" value="'.$nombre.'">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido paterno:</label>
                    <input onchange="preGuardar()" class="form-control" type="text" name="apellido_paterno" value="'.$apellido_paterno.'">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido materno:</label>
                    <input onchange="preGuardar()" class="form-control" type="text" name="apellido_materno" value="'.$apellido_materno.'">
                  </div>
                </div>
                <div class="row">
  	              <div class="col-md-6 form-group">
  	                <label>R.F.C:</label>
  	                <input onchange="preGuardar()" class="form-control" type="text" name="rfc" id="rfc_fisica" value="'.$rfc.'">
  	              </div>
       
                  <div class="col-md-6 form-group">
                    <label>Actividad, ocupación, profesión o giro al que se le dedique el cliente:</label>
                    <select onchange="preGuardar()" class="form-control actividad_fisica_1" name="actividad_o_giro">
                      <option disabled selected>Selecciona una opción</option>';
                      foreach ($get_actividad_fisica as $item) {
                        $selected="";
                        if($actividad_o_giro==$item->clave){
                          $selected="selected";
                        }
                        $html.='<option value="'.$item->clave.'" '.$selected.'>'.$item->acitividad.'</option>';  
                      }
                  $html.='</select>
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Otra ocupación:</label>
                    <input onchange="preGuardar()" class="form-control" type="text" name="otra_ocupa" value="'.$otra_ocupa.'">
                  </div>
  	            </div>
              </form>  
              '; 
      }else if($tipo==2){/// Perosna moral
        $get_actividad_moral=$this->ModeloCatalogos->getData('actividad_econominica_morales');
        $html.='<hr class="subtitle">
              <form class="form" method="post" role="form" id="form_moral">
                  <input type="hidden" name="idmoral" value="'.$idmoral.'">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Razón social:</label>
                      <input onchange="preGuardar()" class="form-control" type="text" name="razon_social" value="'.$razon_social.'">
                    </div>
                    <div class="col-md-4 form-group">
    	                <label>R.F.C:</label>
    	                <input onchange="preGuardar()" class="form-control" type="text" name="r_c_f" value="'.$r_c_f.'">
    	              </div>
                    <div class="col-md-6 form-group">
                      <label>Actividad, ocupación, profesión o giro al que se le dedique el cliente:</label>
                      <select onchange="preGuardar()" class="form-control actividad_fisica_1" name="actividad_o_giro">
                        <option disabled selected>Selecciona una opción</option>';
                        foreach ($get_actividad_moral as $item) {
                          $selected="";
                          if($actividad_o_giro==$item->clave){
                            $selected="selected";
                          }
                          $html.='<option value="'.$item->clave.'" '.$selected.'>'.$item->giro_mercantil.'</option>';  
                        }
                    $html.='</select>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Otra ocupación:</label>
                      <input onchange="preGuardar()" class="form-control" type="text" name="otra_ocupa" value="'.$otra_ocupa.'">
                    </div>
                  </div>
              </form>    
              '; 
      }else if($tipo==3){/// Persona fideicomiso
        $get_actividad_moral=$this->ModeloCatalogos->getData('actividad_econominica_morales');
        $html.='<hr class="subtitle">
              <form class="form" method="post" role="form" id="form_fideicomiso">
                  <input type="hidden" name="idfideicomiso" value="'.$idfideicomiso.'">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Denominación o razón social del fiduciario:</label>
                      <input onchange="preGuardar()" class="form-control" type="text" name="denominacion_razon_social" value="'.$denominacion_razon_social.'">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>R.F.C del fideicomiso:</label>
                      <input onchange="preGuardar()" class="form-control" type="text" name="rfc_fideicomiso" value="'.$rfc_fideicomiso.'">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Actividad, ocupación, profesión o giro al que se le dedique el cliente:</label>
                      <select onchange="preGuardar()" class="form-control actividad_fisica_1" name="actividad_o_giro">
                        <option disabled selected>Selecciona una opción</option>';
                        foreach ($get_actividad_moral as $item) {
                          $selected="";
                          if($actividad_o_giro==$item->clave){
                            $selected="selected";
                          }
                          $html.='<option value="'.$item->clave.'" '.$selected.'>'.$item->giro_mercantil.'</option>';  
                        }
                    $html.='</select>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Otra ocupación:</label>
                      <input onchange="preGuardar()" class="form-control" type="text" name="otra_ocupa" value="'.$otra_ocupa.'">
                    </div>
                  </div>
              </form>    
              '; 
      }
    echo $html;   
  }
  // Funcion para verificar que no exista  el usuario
  public function verificauser(){  
    $user = $this->input->post('user');
    $aux=0;
    $result_user=$this->ModeloCatalogos->getselectwhere('usuarios','Usuario',$user);
    foreach ($result_user as $item) {
        $aux=1;
    }
    echo $aux;
  }
  /// Listado de la tabla
  public function getlistado_cliente(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_cliente($params);
        $totaldata= $this->ModeloGeneral->total_cliente($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
  }
  public function updateregistro(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'idcliente',$id,'cliente');
  }
  /// Guaradar actividad
  public function registro_actividad(){   
      $data = $this->input->post();
      $aux=0;
      $aux=$this->ModeloCatalogos->tabla_inserta('actividad',$data);
      echo $aux;         
  }
  /// Vista Cuando inicia por primera ves el cliente para que pueda restablecer su contrasea
  /*
  public function actualizar_cliente(){
    $data['idcliente']=$this->idcliente;
    $data['get_actividad_cli']=$this->ModeloCliente->getselectactividades($this->idcliente);
    $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
    $data['nombre_actividad']='';
    foreach ($get_actividad as $item) {
      $data['tipo_persona']=$item->tipo_persona;
      $data['nombre']=$item->nombre;
      $data['apellido_paterno']=$item->apellido_paterno;
      $data['apellido_materno']=$item->apellido_materno;
      $data['razon_social']=$item->razon_social;
      $data['denominacion_razon_social']=$item->denominacion_razon_social;
      $data['UsuarioID']=$item->UsuarioID;
      $data['Usuario']=$item->Usuario;
      $data['contrasena']='xxxxxx_xxxx';
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/inicio_cliente',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jsinicio_cliente');
  }
  
  public function registro_clientes_usuarios_yi(){ 
    $data=$this->input->post();
    $id=$data['UsuarioID'];
    $idcli=$data['idcliente'];
    unset($data['UsuarioID']);
    unset($data['idcliente']);
    $pass = $data['contrasena'];
    if ($pass=='xxxxxx_xxxx') {
            unset($data['contrasena']);
    }else{
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena']=$pass;
    } 
    $this->ModeloCatalogos->updateCatalogo($data,'UsuarioID',$id,'usuarios');
    $where_cliente = array('status'=>1);
    $this->ModeloCatalogos->updateCatalogo($where_cliente,'idcliente',$idcli,'cliente');
  }
  
  public function cliente(){ 
    $data['idcliente']=$this->idcliente;
    $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
    $data['nombre_actividad']='';
    foreach ($get_actividad as $item) {
      //<!-- Cliente
      $data['nombre_actividad']=$item->actividad;
      $data['tipo_persona']=$item->tipo_persona;
      $data['telefono']=$item->telefono;
      $data['movil']=$item->movil;
      $data['correo']=$item->correo;
      //<!-- Cliente fisica -->
      $data['idfisica']=$item->idfisica;
      $data['nombre']=$item->nombre;
      $data['apellido_paterno']=$item->apellido_paterno;
      $data['apellido_materno']=$item->apellido_materno;
      $data['rfc']=$item->rfc;
      $data['fecha_nacimiento']=$item->fecha_nacimiento;
      $pais_nacimiento=$item->pais_nacimiento;
      $pais_nacionalidad_f=$item->pais_nacionalidad_f;
      $data['curp']=$item->curp;
      //<!-- Cliente moral -->
      $data['idmoral']=$item->idmoral;
      $data['razon_social']=$item->razon_social;
      $data['r_c_f']=$item->r_c_f;
      $data['fecha_constitucion']=$item->fecha_constitucion;
      $pais_nacionalidad_m=$item->pais_nacionalidad;
      $data['nombre_l']=$item->nombre_l;
      $data['apellido_paterno_l']=$item->apellido_paterno_l;
      $data['apellido_materno_l']=$item->apellido_materno_l;
      $data['fecha_nacimiento_l']=$item->fecha_nacimiento_l;
      $data['rfc_l']=$item->rfc_l;
      $data['curp_l']=$item->curp_l;
      //<!-- Cliente fideicomiso -->
      $data['idfideicomiso']=$item->idfideicomiso;
      $data['denominacion_razon_social']=$item->denominacion_razon_social;
      $data['rfc_fideicomiso']=$item->rfc_fideicomiso;
      $data['identificador_fideicomiso']=$item->identificador_fideicomiso;
      $data['nombre_lf']=$item->nombre_lf;
      $data['apellido_paterno_lf']=$item->apellido_paterno_lf;
      $data['apellido_materno_lf']=$item->apellido_materno_lf;
      $data['fecha_nacimiento_lf']=$item->fecha_nacimiento_lf;
      $data['rfc_lf']=$item->rfc_lf;
      $data['curp_lf']=$item->curp_lf;

    }
    //<!-- Cliente fisica -->
    $result_nacimiento=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacimiento);
    foreach ($result_nacimiento as $item){
        $data['pais_nacimiento']=$item->clave;
        $data['pais_nacimiento_nombre']=$item->pais;
    } 
    $result_nacionalidad=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacionalidad_f);
    foreach ($result_nacionalidad as $item){
        $data['pais_nacionalidad_f']=$item->clave;
        $data['pais_nacionalidad_f_nombre']=$item->pais;
    }
    //<!-- Cliente moral -->
    $result_nacionalidad_m=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacionalidad_m);
    foreach ($result_nacionalidad_m as $item){
        $data['pais_nacionalidad_m']=$item->clave;
        $data['pais_nacimiento_nombre_m']=$item->pais;
    } 
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/cliente',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jsclienteyi');
  }
  */
  
  function get_sucursal(){
    $id = $this->input->post('idcliente');
    $results=$this->ModeloCliente->getselecsucursal($id);
    echo json_encode($results);
  }
  function updateregistro_sucursal(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $this->ModeloCatalogos->updateCatalogo($data,'iddireccion',$id,'cliente_direccion');
  }
  public function registro_actividades(){
    $datos = $this->input->post('data');
    var_dump($datos);
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
      $id_cliente_actividad=$DATA[$i]->id_cliente_actividad;
      $data['idcliente']=$DATA[$i]->idcliente;
      $data['idactividad']=$DATA[$i]->idactividad;
      $idactividad=$DATA[$i]->idactividad;
      if($idactividad>0){
        if($id_cliente_actividad==0){
          $this->ModeloCatalogos->tabla_inserta('cliente_actividad',$data);
        }else{
          $this->ModeloCatalogos->updateCatalogo($data,'idactividad',$idactividad,'cliente_actividad');
        }
      }
    }
  }
  public function get_actividades_cliente(){
    $id = $this->input->post('id');
    $html='';
    $html.='<div class="table-responsive">
              <table class="table" id="table_cliente">
                <thead>
                  <tr>
                    <th>Actividades</th>  
                  </tr>
                </thead>
                <tbody>';
            $result = $this->ModeloCliente->getselectactividades($id);      
           foreach ($result as $item) {
            $html.='<tr>
                      <td>'.$item->actividad.'</td> 
                    </tr>';
            } 
        $html.='</tbody>
              </table>
            </div>';  
    echo $html;
  }

  public function get_actividades_clienteUnica(){
    $id = $this->input->post('id');
    $result = $this->ModeloCliente->getselectactividades($id);      
    $act="";
    foreach ($result as $item) {
      $act=$item->actividad;
    } 
    echo $act;
  }
  function get_actividades_cli(){
    $id = $this->input->post('idcliente');
    $results=$this->ModeloCliente->getselectactividades($id);
    echo json_encode($results);
  }
  function updateregistro_actividad(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $this->ModeloCatalogos->updateCatalogo($data,'id_cliente_actividad',$id,'cliente_actividad');
  }

  public function catalogoInmuebles(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/listado_inmuebles');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jslistado_inmuebles');
  }

  public function addInmueble($id=0){
    if($id>0){
      $data["a"]=$this->General_model->get_tableRow("inmuebles_cliente",array("id"=>$id));
    }else{
      $data="";
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/add_inmuebles',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jsadd_inmuebles');
  }

  public function data_records_inmuebles(){
    $results=$this->ModeloCliente->getInmuebles($this->idcliente);
    $json_data = array("data" => $results);
    echo json_encode($json_data);
  }

  public function data_record_inmuebleId(){
    $id=$this->input->post("id");
    $results=$this->ModeloCliente->getInmuebleId($id);
    echo json_encode($results);
  }

  public function submitInmueble(){
    $data=$this->input->post();
    $valor = str_replace(",", "", $data["valor"]);
    $data["valor"] = str_replace("$", "",$valor);
    if(!isset($data['id']) || $data['id']==0){ //insert
      $data["id_cliente"]=$this->idcliente;
      $id=$this->ModeloCatalogos->tabla_inserta("inmuebles_cliente",$data);
    }
    else{ //update
      $id=$data["id"]; unset($data["id"]);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'inmuebles_cliente');
    }
    echo $id;
  }

  function deleteInmueble(){
    $id = $this->input->post('id');
    $data = array('status'=>0);
    $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'inmuebles_cliente');
  }

  /* ******************CATALOGOS DE INMUEBLES DEL ANEXO 5B************************ */
  public function catalogoInmuebles5b(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/listado_inmuebles5b');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jslistado_inmuebles5b');
  }

  public function addInmueble5b($id=0){
    if($id>0){
      $data["b"]=$this->General_model->get_tableRow("inmuebles5b_cliente",array("id"=>$id));
    }else{
      $data="";
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/add_inmuebles5b',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jsadd_inmuebles5b');
  }

  public function data_records_inmuebles5b(){
    $results=$this->ModeloCliente->getInmuebles5b($this->idcliente);
    $json_data = array("data" => $results);
    echo json_encode($json_data);
  }

  public function data_record_inmuebleId5b(){
    $id=$this->input->post("id");
    $results=$this->General_model->get_tablell("inmuebles5b_cliente",array("id"=>$id,"estatus"=>1));
    echo json_encode($results->result());
  }

  public function submitInmueble5b(){
    $data=$this->input->post();
    $data["monto_desarrollo"]=str_replace("$","",$data["monto_desarrollo"]);
    $data["monto_desarrollo"]=str_replace(",","",$data["monto_desarrollo"]);
    $data["costo_unidad"]=str_replace("$","",$data["costo_unidad"]);
    $data["costo_unidad"]=str_replace(",","",$data["costo_unidad"]);
    if(!isset($data['id']) || $data['id']==0){ //insert
      $data["id_cliente"]=$this->idcliente;
      $id=$this->ModeloCatalogos->tabla_inserta("inmuebles5b_cliente",$data);
    }
    else{ //update
      $id=$data["id"]; unset($data["id"]);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'inmuebles5b_cliente');
    }
    echo $id;
  }

  function deleteInmueble5b(){
    $id = $this->input->post('id');
    $data = array('estatus'=>0);
    $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'inmuebles5b_cliente');
  }


   /* *******************************************/


}

/* End of file Clientes.php */
/* Location: ./application/controllers/Clientes.php */

