<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class ConfigAlerta extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('General_model');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->usuarioid=$this->session->userdata('usuarioid');
            $this->idcliente=$this->session->userdata('idcliente');
            log_message('error', 'idcliente: '.$this->idcliente);
            //ira el permiso del modulo
            $this->load->model('Login_model');
            $permiso=1;
            //$permiso=$this->Login_model->getviewpermiso($this->usuarioid,8);// 8 es el id del submenu
            if ($permiso==0) {
                //redirect('/Sistema');
            }
        }
    }

    public function index(){
        $data["config"]=$this->General_model->get_tableRow("config_alerta",array("id_cliente"=>$this->idcliente));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/alertas/config',$data);
        $this->load->view('templates/footer');
        $this->load->view('configuracion/alertas/configjs');
    }

    public function submitConfig(){
        $data=$this->input->post();
        if($data['id']==0){ //insert
          $data["id_cliente"]=$this->idcliente;
          $id=$this->ModeloCatalogos->tabla_inserta("config_alerta",$data);
        }
        else{ //update
          $id=$data["id"]; unset($data["id"]);
          $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'config_alerta');
        }
    }

}