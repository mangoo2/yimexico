<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaNegra extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('ModeloCatalogos');
		$this->load->model('General_model');
	}

	public function index(){
		$this->load->view('templates/header');
    	$this->load->view('templates/navbar');
    	$this->load->view('catalogos/ListaN/Listado');
		$this->load->view('templates/footer');
    	$this->load->view('catalogos/ListaN/Listadojs');
	}

	public function GetListado(){
		$params=$this->input->post();
		$getdata=$this->ModeloCatalogos->GetLista($params);
		$totaldata= count($getdata->result());
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
	}

	public function Alta($id=0){
		$data['id']=0;
		$data['nombre']="";
		$data['ligaindentificacion']="";
		$data['listaecuentra']="";
		$data['razonlista']="";
		$data['entidad']="";
		$data['apellido_p']="";
		$data['apellido_m']="";
		$data['rfc']="";
		$data['direccion']="";
		$data['puesto']="";
		$data['dependencia']="";
		$data['anioser']="";
		$data['cp']="";
		$data['calle']="";
		$data['next']="";
		$data['nint']="";
		$data['colonia']="";
		$data['municipio']="";
		$data['localidad']="";
		$data['estado']="";
		$data['estados']=$this->General_model->GetAll('estado');
		if ($id!=0){
			$info=$this->General_model->get_tableRow("nombreln","id=".$id);
			$data['id']=$id;
			$data['nombre']=$info->nombre;
			$data['ligaindentificacion']=$info->ligaindentificacion;
			$data['listaecuentra']=$info->listaecuentra;
			$data['razonlista']=$info->razonlista;
			$data['entidad']=$info->entidad;
			$data['apellido_p']=$info->apellido_p;
			$data['apellido_m']=$info->apellido_m;
			$data['rfc']=$info->rfc;
			$data['direccion']=$info->direccion;
			$data['puesto']=$info->puesto;
			$data['dependencia']=$info->dependencia;
			$data['anioser']=$info->anioser;
			$data['cp']=$info->cp;
			$data['calle']=$info->calle;
			$data['next']=$info->next;
			$data['nint']=$info->nint;
			$data['colonia']=$info->colonia;
			$data['municipio']=$info->municipio;
			$data['localidad']=$info->localidad;
			$data['estado']=$info->estado;
		}
		$this->load->view('templates/header');
    	$this->load->view('templates/navbar');
    	$this->load->view('catalogos/ListaN/ListaNegra',$data);
		$this->load->view('templates/footer');
    	$this->load->view('catalogos/ListaN/ListaNegrajs');
	}

	function saveln(){
		$data=$this->input->post();
		$id=$data['id_ln'];
		unset($data['id_ln']);
		if ($id!=0){
			$this->ModeloCatalogos->updateCatalogo($data,"id",$id,"nombreln");
			echo $id;
		}else{
			$id=$this->ModeloCatalogos->tabla_inserta("nombreln",$data);
			echo $id;
		}
	}

	function saveOther(){
		$idnl=$this->input->post('id_ln');
		$data=$this->input->post('data');
		$tabla=$this->input->post('tabla');
		$num=count($data);
		$table="";
		switch ($tabla){
			case 1:
				$table="aliasln";
				break;
			case 2:
				$table="direccionln";
				break;
			case 3:
				$table="nacimientoln";
				break;
			default:
				echo "Algo se rompio";
				die();
				break;
		}
		$arrd=$this->GetArrayInsert($data);
		//var_dump($arrd);
		$num=count($arrd);
		for ($i=0; $i < $num; $i++){
			var_dump($arrd[$i]);
			$info=$arrd[$i];
			$id=$info['id'];
			unset($info['id']);
			$info['id_ln']=$idnl;
			if ($id==0){
				$this->ModeloCatalogos->tabla_inserta($table,$info);
			}else{
				$this->ModeloCatalogos->updateCatalogo($info,'id',$id,$table);
			}
		}
	}

	public function GetInfoOther(){
		$idnl=$this->input->post('id_ln');
		$tabla=$this->input->post('tabla');
		switch ($tabla){
			case 1:
				$table="aliasln";
				break;
			case 2:
				$table="direccionln";
				break;
			case 3:
				$table="nacimientoln";
				break;
			default:
				echo "Algo se rompio";
				die();
				break;
		}
		$where="status=1 AND id_ln=".$idnl;
		$data=$this->General_model->GetAllWhere($table,$where);
		echo json_encode($data);
	}

	public function DeleteElement(){
		$id=$this->input->post('id');
		$tabla=$this->input->post('tabla');
		switch ($tabla){
			case 1:
				$table="aliasln";
				break;
			case 2:
				$table="direccionln";
				break;
			case 3:
				$table="nacimientoln";
				break;
			default:
				echo "Algo se rompio";
				die();
				break;
		}
		$info=array('status' => 0);
		$this->ModeloCatalogos->updateCatalogo($info,'id',$id,$table);
		echo $this->db->last_query();
	}

	public function GetDataDir(){
		$cp=$this->input->post('cp');
		$data=$this->General_model->GetAllWhere("estados_cp","codigo=".$cp);
		$aux=array('estado' => $data[0]->id_estado, "muni"=>$data[0]->mnpio, "ciu"=>$data[0]->ciudad);
		$arrm=array('estate'=>$aux,'arrdata'=>$data);
		echo json_encode($arrm);
	}

	public function GetArrayInsert($arr1){
		$dtacom=array();
		foreach ($arr1 as $key){	
			$arr=$key;
			$con=0;
			$temp = array();
			foreach ($arr as $ele) {
				$name=$ele['name'];
				$val=$ele['value'];
				$tem= array($name => $val);
				//array_push($temp,$tem);
				$temp= array_merge($temp,$tem);
			}
			array_push($dtacom,$temp);
		}
		return $dtacom;
	}



}

/* End of file ListaNegra.php */
/* Location: ./application/controllers/ListaNegra.php */