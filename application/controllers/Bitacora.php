<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bitacora extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('BitacoraModel');
	
  	if (!$this->session->userdata('logeado')){
      redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        //ira el permiso del modulo
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,19);// 19 es el id del submenu
        if ($permiso==0) {
            redirect('/Sistema');
        }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}

  public function index(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('bitacoras/busquedas');
    $this->load->view('templates/footer');
    $this->load->view('bitacoras/jsbusquedas');
  }

  public function getClientes(){
    $cli=$this->BitacoraModel->get_cliente();
    $html='<select class="form-control" id="id_cliente"><option selected disabled value="0">Elige un cliente:</option>';
    $nombre="";
    foreach ($cli as $c) {
      if($c->tipo_persona==1){
        $nombre=$c->nombre.' '.$c->apellido_paterno.' '.$c->apellido_materno;
      }else if($c->tipo_persona==2){
        $nombre=$c->razon_social;
      }else if($c->tipo_persona==3){
        $nombre=$c->denominacion_razon_social;
      }   
      $html.='<option data-tipo="'.$c->tipo_persona.'" value="'.$c->idcliente.'">'.$nombre.'</option>';
    } 
    $html.='</select>';
    echo $html;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->BitacoraModel->get_clienteBusqueda($params);
    $totaldata= $this->BitacoraModel->total_clienteBusqueda($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function exportar($id_cliente,$tipo){
    $data["b"] = $this->BitacoraModel->exportarLista($id_cliente,$tipo);
    $this->load->view('bitacoras/exportar',$data);
  }

}
