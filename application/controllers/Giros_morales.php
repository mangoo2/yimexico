<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Giros_morales extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        $this->idcliente_usuario=$this->session->userdata('idcliente_usuario');
    }
	}
	public function index(){      
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('configuracion/giro_moral/moral');
      $this->load->view('templates/footer');
      $this->load->view('configuracion/giro_moral/moraljs');
	}
  /// Listado de la tabla
  public function getlistado(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_activida_moral($params);
        $totaldata= $this->ModeloGeneral->total_activida_moral($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
  }
  public function registro(){
    $data = $this->input->post();
    $id = $data['clave'];
    unset($data['clave']); 
    $this->ModeloCatalogos->updateCatalogo($data,'clave',$id,'actividad_econominica_morales');
  }
}