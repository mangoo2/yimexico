<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct(){
        parent::__construct();
        //$this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,2);//1-> id del perfil y  2-> id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){   
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('catalogos/listUsuarios');
        $this->load->view('templates/footer');
        $this->load->view('catalogos/jssucursales');
	}

    function add($id=0){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('catalogos/addUsuarios',$data);
        $this->load->view('templates/footer');
        $this->load->view('catalogos/jsUsuarios');
    }

    public function registro(){
        $data = $this->input->post();
        $id = $data['idsucursal'];
        unset($data['idsucursal']); 
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo($data,'idsucursal',$id,'sucursal');
            $result=2;
        }else{
            $this->ModeloCatalogos->tabla_inserta('sucursal',$data);
            $result=1;
        }   
        echo $result;
    }

    public function getListado(){
        $data = $this->ModeloCatalogos->getselectwhere('sucursal','activo',1);
        $json_data = array("data" => $data);
        echo json_encode($json_data);
    }

    public function verificapass(){
        $pass = $this->input->post('pass'); 
        $verifica = $this->ModeloCatalogos->verificar_pass($pass);
        $aux = 0;
        if($verifica == 1){
            $aux=1;
        }
        if($aux==1){
            
        }
    echo $aux;
    }

    public function updateregistro(){
        $id = $this->input->post('id');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo($data,'idsucursal',$id,'sucursal');
    }
}