<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Umbrales extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('General_model');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->usuarioid=$this->session->userdata('usuarioid');
            //ira el permiso del modulo
            $this->load->model('Login_model');
            $permiso=1;
            //$permiso=$this->Login_model->getviewpermiso($this->usuarioid,8);// 8 es el id del submenu
            if ($permiso==0) {
                //redirect('/Sistema');
            }
        }
    }

    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/umbrales/lista');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/umbrales/listajs');
    }

    public function alta($id=0){
        if($id>0){
            $data['ua'] = $this->General_model->get_tableRow("anexo_config",array("id"=>$id));
            $data["info"] = $this->ModeloCatalogos->getselectwherestatus('*',"anexos_grales",array("id"=>$data["ua"]->id_anexo_gral));
            $data['u'] = $this->General_model->get_tableRow("anexo_config",array("id_anexo_gral"=>$data["ua"]->id_anexo_gral));
        }
        else{
            $data["u"]="";
            $data["info"] = $this->ModeloCatalogos->getselectwherestatus('*',"anexos_grales",array("status"=>1));
        }
        $data["idanexo"]=$id;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/umbrales/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('configuracion/umbrales/addjs');
    }

    public function datatable_records(){
        $datas = $this->ModeloCatalogos->getUmbrales();
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    public function submit(){
        $data=$this->input->post();
        if($data['id']==0){ //insert
            $id=$this->ModeloCatalogos->tabla_inserta("anexo_config",$data);
        }
        else{ //update
            $id=$data["id"]; unset($data["id"]);
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'anexo_config');
        }
        echo $id;
    }

    public function submit12a(){
        $data = $this->input->post('data');
        $DATA = json_decode($data);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->id;
            $datas['id'] = $DATA[$i]->id;
            $datas['id_anexo_gral']= $DATA[$i]->id_anexo_gral;
            $datas['sub_anexo']= $DATA[$i]->sub_anexo;
            $datas['identificacion'] = $DATA[$i]->identificacion;
            $datas['aviso'] = $DATA[$i]->aviso;
            $datas['limite_efectivo'] = $DATA[$i]->limite_efectivo;
            $datas['siempre_iden'] = $DATA[$i]->siempre_iden;
            $datas['siempre_avi'] = $DATA[$i]->siempre_avi;
            $datas['na'] = $DATA[$i]->na;

            /*log_message('error', 'id: '.$DATA[$i]->id);
            log_message('error', 'id_anexo_gral: '.$DATA[$i]->id_anexo_gral);
            log_message('error', 'sub_anexo: '.$DATA[$i]->sub_anexo);*/

            unset($datas['id']);
            if ($id==0) {
                $this->ModeloCatalogos->tabla_inserta("anexo_config",$datas);
            }else{
                $this->ModeloCatalogos->updateCatalogo($datas,'id',$id,'anexo_config');
            }            
        }
    }

    public function getUmbral(){
        $ida=$this->input->post("anexo");
        $data = $this->ModeloCatalogos->getUmbral($ida);
        echo json_encode($data);
    }

    public function getUmbralActividad(){
        $ida=$this->input->post("anexo");
        $act=$this->input->post("act");
        $data = $this->ModeloCatalogos->getUmbralActividad($ida,$act);
        echo json_encode($data);
    }

}