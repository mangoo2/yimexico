<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
    }
	public function index(){
    $perfilview=0;
    $cambio_pass=0;
    $status=0;
		//$this->load->view('login/sessionstart'); 
		if (!isset($_SESSION['perfilid'])) {
            $perfilview=0;
            redirect('/Login');
    }else{
        $perfilview=$_SESSION['perfilid'];
        $status=$_SESSION['status'];
        //$cambio_pass=$_SESSION['cambio_pass'];
        $cambio_pass=$this->session->userdata('cambio_pass');
        log_message('error', 'cambio_pass: '.$cambio_pass);
    }
   	if ($perfilview==1) {
    	redirect('/Inicio');
    }else if($perfilview==3){
      if(/*$status==0 || */ $cambio_pass=="0"){
         redirect('/Mis_datos/actualizar_cliente');
      }else if($status==1){
        redirect('/Mis_datos');
      }else if($cambio_pass=="1"){
        redirect('/Inicio_cliente');
      }
    /*}else if($perfilview==3){
        redirect('/Inicio_cliente');*/
    }else{
    	redirect('/Inicio_cliente');
    }
	}
}
