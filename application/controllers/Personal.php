<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {

	public function __construct(){
        parent::__construct();
        //$this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,1);// 2 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        $this->auxperfil=0;
        if($this->perfilid==1){
            $this->auxperfil=1;
        }
    }

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('catalogos/personal/listempleado');
        $this->load->view('templates/footer');
        $this->load->view('catalogos/personal/jsempleado');
	}
    
    function add(){
        //$data['statussucursal']=$this->auxperfil;
        //data['sucursal']=$this->ModeloCatalogos->getselectwhere('sucursal','activo',1);
        //$data['puesto']=$this->ModeloCatalogos->getselectwhere('puesto','activo',1);
        //$data['perfiles']=$this->ModeloCatalogos->getData('perfiles');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('catalogos/personal/addempleado');
        $this->load->view('templates/footer');
        $this->load->view('catalogos/personal/jsempleado');
    }

    public function registro(){
        $data = $this->input->post();
        $id = $data['personalId'];
        unset($data['personalId']); 
        unset($data['UsuarioID']); 
        unset($data['Usuario']); 
        unset($data['perfilId']); 
        unset($data['contrasena']); 
        $aux=0;
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo($data,'personalId',$id,'personal');
            $result=2;
            $aux=$id;
        }else{
            $aux=$this->ModeloCatalogos->tabla_inserta('personal',$data);
            $result=1;
        }   
        $arrayName = array('id'=>$aux,'status'=>$result);
        echo json_encode($arrayName);
    }
    public function registrouser(){ 
        $data=$this->input->post();
        $idp=$this->input->post('idpersona');
        if($idp!=""){
           $data['personalId']=$idp; 
        }else{
           unset($data['personalId']);
        }

        $id=$data['UsuarioID'];
        $pass=$data['contrasena'];
        unset($data['idpersona']);
        unset($data['UsuarioID']);  
        unset($data['idpuesto']); 
        unset($data['idsucursal']); 
        unset($data['id']); 
        unset($data['nombre']); 
        unset($data['Email']); 
        unset($data['telefono']); 
        unset($data['celular']); 
        if ($pass=='xxxxxx_xxxx') {
                unset($data['contrasena']);
        }else{
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $data['contrasena']=$pass;
        } 
        $aux=0;
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo($data,'UsuarioID',$id,'usuarios');
        }else{
            $user=$data['Usuario'];
            if($user!=''){
               $this->ModeloCatalogos->tabla_inserta('usuarios',$data);
            }
        }   
    }
    public function getlistregistro() {
        $params = $this->input->post();
        $getdata = $this->ModeloGeneral->getpersonal($params,$this->auxperfil);
        $totaldata= $this->ModeloGeneral->total_personas($params,$this->auxperfil); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function verificapass(){
        $pass = $this->input->post('pass'); 
        $verifica = $this->ModeloCatalogos->verificar_pass($pass);
        $aux = 0;
        if($verifica == 1){
            $aux=1;
        }
        if($aux==1){
            
        }
    echo $aux;
    }
    public function updateregistro(){
        $id = $this->input->post('id');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo($data,'personalId',$id,'personal');
    }
    public function verificauser()
    {   $user = $this->input->post('user');
        $aux=0;
        $result_user=$this->ModeloCatalogos->getselectwhere('usuarios','Usuario',$user);
        foreach ($result_user as $item) {
            $aux=1;
        }
    echo $aux;
    }
    public function registropuesto()
    {
        $puesto = $this->input->post('puesto');
        $data = array('puesto' =>$puesto);
        $id = $this->ModeloCatalogos->tabla_inserta('puesto',$data);
        $puesto=$this->ModeloCatalogos->getselectwhere('puesto','activo',1);
        $html='';
        foreach ($puesto as $item) { 
          if($item->idpuesto==$id){
               $selec = 'selected'; 
          }else{
               $selec = '';
          }  
          $html.='<option value="'.$item->idpuesto.'" '.$selec.'>'.$item->puesto.'</option>';   
        }
        echo $html;
    }
 
}