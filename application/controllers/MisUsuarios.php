<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MisUsuarios extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
	
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        //ira el permiso del modulo
        /*
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,8);// 2 es el id del submenu
        if ($permiso==0) {
            redirect('/Sistema');
        }
        */
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('catalogos/misusuarios/list_usuario_cliente');
        $this->load->view('templates/footer');
        $this->load->view('catalogos/misusuarios/jslist_usuario_cliente');
    }
    public function addusuarios_cliente($id=0){
        $where_sucu = array('activo'=>1,'idcliente'=>$this->idcliente);
        $data['get_tipo_vialidad']=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
        $data['get_perfiles'] = $this->ModeloCatalogos->get_perfiles();
        $data['get_sucursal'] = $this->ModeloCatalogos->getselectwherestatus('iddireccion,alias','cliente_direccion',$where_sucu);
        $data['estado_get'] = $this->ModeloCatalogos->getData('estado');
        $data['menu_get'] = $this->ModeloCatalogos->getData('menu_usuario_cliente');

        $get_act=$this->ModeloCatalogos->getselectwherestatus("idactividad","cliente_actividad",array('idcliente'=>$this->idcliente));
        foreach($get_act as $a){
            $data["id_act"] = $a->idactividad;  
        }
        
        $idcliente_usuario=0;
        $data['idcliente_usuario']=0;
        $data['nombre']='';
        $data['apellido_paterno']='';
        $data['apellido_materno']='';
        $data['funcion_puesto']='';
        $data['tipo_vialidad']='';
        $data['calle']='';
        $data['no_ext']='';
        $data['no_int']='';
        $data['colonia']='';
        $data['municipio_delegacion']='';
        $data['localidad']='';
        $data['estado']='';
        $data['pais']='';
        
        $idcliente_usuario=0;
        $data['UsuarioID']=0;
        $data['perfilId']=0;
        $data['tipo']='';
        $data['Usuario']='';
        $data['contrasena']='';
        $data['idsucursal']=0;
        $data['pais_nombre_u']='';
        $pais='';
        $where=array('idcliente' =>$this->idcliente,'activo'=>1,'activo','idcliente_usuario'=>$id);
        $result = $this->ModeloCatalogos->getselectwherestatus('*','usuarios_clientes',$where);
        foreach ($result as $item){
            $idcliente_usuario=$item->idcliente_usuario;
            $data['idcliente_usuario']=$item->idcliente_usuario;
            $data['nombre']=$item->nombre;
            $data['apellido_paterno']=$item->apellido_paterno;
            $data['apellido_materno']=$item->apellido_materno;
            $data['funcion_puesto']=$item->funcion_puesto;
            $data['tipo_vialidad']=$item->tipo_vialidad;
            $data['calle']=$item->calle;
            $data['no_ext']=$item->no_ext;
            $data['no_int']=$item->no_int;
            $data['colonia']=$item->colonia;
            $data['municipio_delegacion']=$item->municipio_delegacion;
            $data['localidad']=$item->localidad;
            $data['estado']=$item->estado;
            $pais=$item->pais;
        }    
        $result_nacimiento=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais);
        foreach ($result_nacimiento as $item){
              $data['pais']=$item->clave;
              $data['pais_nombre_u']=$item->pais;
        }
        if($idcliente_usuario!=0){
            $arrayusu = array('idcliente_usuario' =>$idcliente_usuario);
            $result = $this->ModeloCatalogos->getselectwherestatus('*','usuarios',$arrayusu);
            foreach ($result as $item) {
                $data['UsuarioID']=$item->UsuarioID;
                $data['perfilId']=$item->perfilId;
                $data['personalId']=$item->personalId;
                $data['Usuario']=$item->Usuario;
                $data['contrasena']='xxxxxx_xxxx';
                $data['idsucursal']=$item->idsucursal;
            }   
        }
        $data['get_puesto']=$this->ModeloCatalogos->get_puesto();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('catalogos/misusuarios/usuario_cliente',$data);
        $this->load->view('templates/footer');
        $this->load->view('catalogos/misusuarios/jsusuario_cliente');
    } 
    function registro_usuario_cliente(){
        $data = $this->input->post();
        $id = $data['idcliente_usuario'];
        unset($data['UsuarioID']);
        unset($data['Usuario']);
        unset($data['contrasena']);
        unset($data['perfilId']);
        unset($data['idsucursal']);
        $aux=0;
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo($data,'idcliente_usuario',$id,'usuarios_clientes');
            $result=2;
            $aux=$id;
        }else{
            $data['idcliente']=$this->idcliente;
            $aux=$this->ModeloCatalogos->tabla_inserta('usuarios_clientes',$data);
            $result=1;
        }   
        $arraydata = array('id'=>$aux,'status'=>$result);
        echo json_encode($arraydata);      
    } 
       // Funcion para verificar que no exista  el usuario
    public function verificauser(){  
        $user = $this->input->post('user');
        $aux=0;
        $result_user=$this->ModeloCatalogos->getselectwhere('usuarios','Usuario',$user);
        foreach ($result_user as $item) {
            $aux=1;
        }
        echo $aux;
      }
    public function registro_usuario_cliente_usuario(){
        $data = $this->input->post();
        $id = $data['UsuarioID'];
        unset($data['UsuarioID']);
        $data['perfilId'];
        $data['personalId']=$this->idpersonal;
        $data['idcliente']=$this->idcliente;
        $data['idcliente_usuario'];
        $data['idsucursal'];
        $data['tipo']=4;//usuarios_clientes
        $data['Usuario'];
        $pass = $data['contrasena'];
        if ($pass=='xxxxxx_xxxx') {
                unset($data['contrasena']);
        }else{
            $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
            $data['contrasena']=$pass;
        } 
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo($data,'UsuarioID',$id,'usuarios');
        }else{
            $this->ModeloCatalogos->tabla_inserta('usuarios',$data);
        }
    }
    /// Listado de la tabla
    public function getlistado_usuarios(){
        $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_usuario_clientes($params,$this->idcliente);
        $totaldata= $this->ModeloGeneral->total_usuarios_clientes($params,$this->idcliente); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function updateregistro(){
        $id = $this->input->post('id');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo($data,'idcliente_usuario',$id,'usuarios_clientes');
    }
    public function get_estados(){
        $html = '';
        $result = $this->ModeloCatalogos->getData('estado');
        $html.= '<select class="form-control" name="estado" id="estado">';
        foreach ($result as $item) {
            $html.='<option value="'.$item->clave.'">'.$item->estado.'</option>';
        }
        $html.= '</select>';
        echo $html;  
    }

    // Menu 
    public function insert_menu(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idpermiso = $DATA[$i]->idpermiso;
            $data['idcliente_usuario']=$DATA[$i]->idcliente_usuario;
            $data['id_menu']=$DATA[$i]->id_menu;
            $data['check_menu']=$DATA[$i]->check_menu;
            if($idpermiso==0){
                $idpermiso=$this->General_model->add_record('usuarios_clientes_menu',$data);
            }else{
                $this->General_model->edit_record('idpermiso',$idpermiso,$data,'usuarios_clientes_menu');
            }   
        }
        echo $idpermiso;
    }
    public function insert_menu_sub(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {

            $idpermisosub = $DATA[$i]->idpermisosub;
            log_message('error', 'idpermisosub: '.$idpermisosub);
            $data['idcliente_usuario']=$DATA[$i]->idcliente_usuario;
            $data['id_menu']=$DATA[$i]->id_menu;
            $data['id_menusub']=$DATA[$i]->id_menusub;
            $data['check_menu']=$DATA[$i]->check_menu;
            if($idpermisosub==0){
                $idpermisosub=$this->General_model->add_record('usuarios_clientes_menu_sub',$data);
            }else{
                $this->General_model->edit_record('idpermisosub',$idpermisosub,$data,'usuarios_clientes_menu_sub');
            }   
        }
        echo $idpermisosub;
    }

    public function get_usuario_menu_detalles(){
        $id = $this->input->post('id');
        $idsucursal = $this->input->post('idsucursal');
        $get_result=$this->ModeloCatalogos->get_usuario_permiso_menu($id,$idsucursal);
        echo json_encode($get_result);
    }
    /*
    public function delete_menu_registro(){
        $id = $this->input->post('id');
        $arraywhere = array('idpermiso'=>$id);
        $this->General_model->delete_record('usuarios_clientes_menu',$arraywhere);
    }
    */

}    