<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ModeloConfiguracion');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->usuarioid=$this->session->userdata('usuarioid');
            $this->idcliente=$this->session->userdata('idcliente');
            //ira el permiso del modulo
            $this->load->model('Login_model');
            $permiso=1;
            //$permiso=$this->Login_model->getviewpermiso($this->usuarioid,8);// 8 es el id del submenu
            if ($permiso==0) {
                //redirect('/Sistema');
            }
        }
    }

    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/limites/configAnexo');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/limites/configAnexojs');
    }

    public function datatable_records(){
        $datas = $this->ModeloConfiguracion->getDatosConfig($this->idcliente);
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    public function submit(){
        $data=$this->input->post();
        if($data['id']==0){ //insert
            $id=$this->ModeloCatalogos->tabla_inserta("limite_anexo_config",$data);
        }
        else{ //update
            $id=$data["id"]; unset($data["id"]);
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'limite_anexo_config');
        }
        echo $id;
    }

    public function getConfigAct(){
        $anexo=$this->input->post("anexo");
        /*log_message('error', 'usuario: '.$this->session->userdata("usuario")); 
        log_message('error', 'idpersonal: '.$this->session->userdata("idpersonal"));
        log_message('error', 'idcliente: '.$this->session->userdata("idcliente")); 
        log_message('error', 'idcliente_usuario: '.$this->session->userdata("idcliente_usuario"));*/
        
        $datas = $this->ModeloConfiguracion->getConfigAnexo($this->idcliente,$anexo);
        $permite_avance="0";
        $cont = 0;
        foreach ($datas as $k) {
            $cont++;
            $permite_avance=$k->permite_avance;
        }
        if($cont>0){
            $permite_avance=$k->permite_avance;
        }
        else{
            $permite_avance="n";
        }

        echo $permite_avance;
    }
    
}