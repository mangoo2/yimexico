<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alertas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('ModeloCatalogos');

  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        $this->usuarioid=$this->session->userdata('usuarioid');
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
  public function index(){

  }

  public function getGradoTrans(){
    $ido = $this->input->post('ido');
    $ida = $this->input->post('ida');
    if($ida==11){ //vehiculos
      $data=$this->ModeloCatalogos->getselectwherestatusAll("grado_riesgo_actividad",array("id_operacion"=>$ido,"id_actividad"=>$ida));
    }
    echo json_encode($data);
  }

  public function getPagos(){
    $idt = $this->input->post('idt');
    $ida = $this->input->post('ida');
    if($ida==11){ //vehiculos
      $data=$this->ModeloCatalogos->getselectwherestatusAll("pagos_transac_anexo_8",array("id_anexo_8"=>$idt,"status"=>1));
    }
    if($ida==15){ //anexo12a --como momentario no se puede comparar esta parte con anexo 12a, no tiene isntrum monetarios--ampoco 12b
      $data=$this->ModeloCatalogos->getselectwherestatus("instrumento_monetario","pagos_transac_anexo12a",array("idanexo12a"=>$idt,"activo"=>1));
    }

    echo json_encode($data);
  }

  
}
