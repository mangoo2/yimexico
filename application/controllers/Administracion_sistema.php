<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administracion_sistema extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('ModeloCliente');
	}	
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('catalogos/administracion_sistema/administracion_sist');
        $this->load->view('templates/footer');
        $this->load->view('catalogos/administracion_sistema/administracion_sistjs');
	}
}