<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfilamiento extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCliente');
        $this->load->model('General_model');
        $this->load->model('ModeloGeneral');
        if (!$this->session->userdata('logeado')){
              redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->idcliente=$this->session->userdata('idcliente');
            $this->tipo=$this->session->userdata('tipo');
            $this->status=$this->session->userdata('status');
            //ira el permiso del modulo
          /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,4);// 2 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
          */  
        }
	}

  public function index($desdeopera=0,$idopera=0){
    $data['perfil_v']=$this->perfilid;
    $data['get_tipo_cliente']=$this->ModeloCatalogos->getselectwhere('tipo_cliente','activo',1);
    $data['idtipo_cliente_s'] = '';
    //$data['idopera'] = '0';
    $data['desdeopera'] = $desdeopera;
    $data['idopera'] = $idopera;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/addperfilamiento',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/perfilamiento/jsperfilamiento');
  }

  public function editar($idp,$idc,$idopera=0){
    $data['perfil_v']=$this->perfilid;
    $data['get_tipo_cliente']=$this->ModeloCatalogos->getselectwhere('tipo_cliente','activo',1);
    $data['idperfilamiento'] = $idp;
    $data['idtipo_cliente_s'] = $idc;
    $data['idopera'] = $idopera;
    $data['desdeopera'] = 0;
    if($idopera>0){
      $data['desdeopera'] = 1;
    }

    if($idc!=0){
      if($idc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
      if($idc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
      if($idc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
      if($idc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
      if($idc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i';}
      if($idc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
      $data['datos']=$this->General_model->get_tableQuery($tabla,array($idt=>$idc)); 
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/addperfilamiento',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/perfilamiento/jsperfilamiento');
  }
  public function vizualizar($idp,$idc,$ido=0){
    $data['perfil_v']=$this->perfilid;
    $data['get_tipo_cliente']=$this->ModeloCatalogos->getselectwhere('tipo_cliente','activo',1);
    $data['idperfilamiento'] = $idp;
    $data['idtipo_cliente_s'] = $idc;
    $data['idopera'] = $ido;
    $data['desdeopera'] = 0;
      if($idc!=0){
        if($idc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
        if($idc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
        if($idc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
        if($idc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
        if($idc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i';}
        if($idc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
        $data['datos']=$this->General_model->get_tableQuery($tabla,array($idt=>$idc)); 
      }
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/perfilamiento/addperfilamiento',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/perfilamiento/jsperfilamiento');
  }
  
  /* ////////////////////////*/
	public function Busqueda(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/busqueda');
    $this->load->view('templates/footer');
    //$this->load->view('catalogos/perfilamiento/busqueda');
	}
	public function CalifRiesgo(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/calificar');
    $this->load->view('templates/footer');
	}
	public function Recopilacion(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/recopilacion');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/perfilamiento/jsdocumentos');
	}
	public function BenRealA(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/beneficiarioR');
    $this->load->view('templates/footer');
    //$this->load->view('catalogos/perfilamiento/jsbeneficiarioR');
	}
	public function BenRealB(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/beneficiarioB');
    $this->load->view('templates/footer');
    //$this->load->view('catalogos/perfilamiento/jsbeneficiarioR');
	}
	public function BenRealC(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/beneficiarioRC');
    $this->load->view('templates/footer');
    //$this->load->view('catalogos/perfilamiento/jsbeneficiarioR');
	}
	public function DocReal(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/docBenReal');
    $this->load->view('templates/footer');
	}
  /* ////////////////////////*/
  /// Paso 3
  public function Identificacion_beneficiario(){
  $this->load->view('templates/header');
  $this->load->view('templates/navbar');
  $this->load->view('catalogos/perfilamiento/paso3');
  $this->load->view('templates/footer');
  $this->load->view('catalogos/perfilamiento/jspaso3');
  }
  //// Paso 4
  public function Calificar_grado_de_riesgo_del_cliente(){
  $this->load->view('templates/header');
  $this->load->view('templates/navbar');
  $this->load->view('catalogos/perfilamiento/paso4');
  $this->load->view('templates/footer');
  }
  //// Paso 5
  public function Recopilacion_de_documentos($id){
  $data['tipo_cliente']=$id;
  $this->load->view('templates/header');
  $this->load->view('templates/navbar');
  $this->load->view('catalogos/perfilamiento/paso5',$data);
  $this->load->view('templates/footer');
  }
  public function Recopilacion_de_documentos_del_beneficiario_real()
  {
  $this->load->view('templates/header');
  $this->load->view('templates/navbar');
  $this->load->view('catalogos/perfilamiento/paso6');
  $this->load->view('templates/footer');
  }
  public function tipo_cliente(){
      $tipo=$this->input->post('tipo');
      $idperfilamiento=$this->input->post('idperfilamiento');
      ///////////////////////
      $get_tipo_vialidad=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      $html='<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>';
      $paso2='Datos generales';
      $get_activida_fisica=$this->ModeloCatalogos->getData('actividad_econominica_fisica');
      $estados=$this->ModeloCatalogos->getData('estado');
      if($tipo==1){
          ////////////////////////
          $idtipo_cliente_p_f_m_1=0;
          $nombre_1='';
          $apellido_paterno_1='';
          $apellido_materno_1='';
          $nombre_identificacion_1='';
          $autoridad_emite_1='';
          $numero_identificacion_1='';
          $genero_1='';
          $fecha_nacimiento_1='';
          $pais_nacimiento_1='';
          $pais_nacionalidad_1='';
          $activida_o_giro_1='';
          $otra_ocupa="";
          $tipo_vialidad_d_1='';
          $calle_d_1='';
          $no_ext_d_1='';
          $no_int_d_1='';
          $colonia_d_1='';
          $municipio_d_1='';
          $localidad_d_1='';
          $estado_d_1='';
          $cp_d_1='';
          $pais_d_1='';
          $trantadose_persona_1='';
          $tipo_vialidad_t_1='';
          $calle_t_1='';
          $no_ext_t_1='';
          $no_int_t_1='';
          $colonia_t_1='';
          $municipio_t_1='';
          $localidad_t_1='';
          $estado_t_1='';
          $cp_t_1='';
          $correo_t_1='';
          $r_f_c_t_1='';
          $curp_t_1='';
          $telefono_t_1='';
          $presente_legal_1='';
          $nombre_p_1='';
          $apellido_paterno_p_1='';
          $apellido_materno_p_1='';
          $nombre_identificacion_p_1='';
          $autoridad_emite_p_1='';
          $numero_identificacion_p_1='';
          $fecha_nac_apodera='';
          $rfc_apodera='';
          $curp_apodera='';
          $genero_p_1=0;
          $tipo_vialidad_p_1='';
          $calle_p_1='';
          $no_ext_p_1='';
          $no_int_p_1='';
          $colonia_p_1='';
          $municipio_p_1='';
          $localidad_p_1='';
          $estado_p_1='';
          $cp_p_1='';
          ////////////////////////////
          $arraywhere_1 = array('idperfilamiento'=>$idperfilamiento); 
          $tipo_cliente_p_f_m=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_m',$arraywhere_1);
          foreach ($tipo_cliente_p_f_m as $item_1) {
            $idtipo_cliente_p_f_m_1=$item_1->idtipo_cliente_p_f_m;
            $idperfilamiento_1=$item_1->idperfilamiento;
            $nombre_1=$item_1->nombre;
            $apellido_paterno_1=$item_1->apellido_paterno;
            $apellido_materno_1=$item_1->apellido_materno;
            $nombre_identificacion_1=$item_1->nombre_identificacion;
            $autoridad_emite_1=$item_1->autoridad_emite;
            $numero_identificacion_1=$item_1->numero_identificacion;
            $genero_1=$item_1->genero;
            $fecha_nacimiento_1=$item_1->fecha_nacimiento;
            $pais_nacimiento_1=$item_1->pais_nacimiento;
            $pais_nacionalidad_1=$item_1->pais_nacionalidad;
            $activida_o_giro_1=$item_1->activida_o_giro;
            $otra_ocupa=$item_1->otra_ocupa;
            $tipo_vialidad_d_1=$item_1->tipo_vialidad_d;
            $calle_d_1=$item_1->calle_d;
            $no_ext_d_1=$item_1->no_ext_d;
            $no_int_d_1=$item_1->no_int_d;
            $colonia_d_1=$item_1->colonia_d;
            $municipio_d_1=$item_1->municipio_d;
            $localidad_d_1=$item_1->localidad_d;
            $estado_d_1=$item_1->estado_d;
            $cp_d_1=$item_1->cp_d;
            $pais_d_1=$item_1->pais_d;
            $trantadose_persona_1=$item_1->trantadose_persona;
            $tipo_vialidad_t_1=$item_1->tipo_vialidad_t;
            $calle_t_1=$item_1->calle_t;
            $no_ext_t_1=$item_1->no_ext_t;
            $no_int_t_1=$item_1->no_int_t;
            $colonia_t_1=$item_1->colonia_t;
            $municipio_t_1=$item_1->municipio_t;
            $localidad_t_1=$item_1->localidad_t;
            $estado_t_1=$item_1->estado_t;
            $cp_t_1=$item_1->cp_t;
            $correo_t_1=$item_1->correo_t;
            $r_f_c_t_1=$item_1->r_f_c_t;
            $curp_t_1=$item_1->curp_t;
            $telefono_t_1=$item_1->telefono_t;
            $presente_legal_1=$item_1->presente_legal;
            $nombre_p_1=$item_1->nombre_p;
            $apellido_paterno_p_1=$item_1->apellido_paterno_p;
            $apellido_materno_p_1=$item_1->apellido_materno_p;
            $nombre_identificacion_p_1=$item_1->nombre_identificacion_p;
            $autoridad_emite_p_1=$item_1->autoridad_emite_p;
            $numero_identificacion_p_1=$item_1->numero_identificacion_p;
            $fecha_nac_apodera=$item_1->fecha_nac_apodera;
            $rfc_apodera=$item_1->rfc_apodera;
            $curp_apodera=$item_1->curp_apodera;
            $genero_p_1=$item_1->genero_p;
            $tipo_vialidad_p_1=$item_1->tipo_vialidad_p;
            $calle_p_1=$item_1->calle_p;
            $no_ext_p_1=$item_1->no_ext_p;
            $no_int_p_1=$item_1->no_int_p;
            $colonia_p_1=$item_1->colonia_p;
            $municipio_p_1=$item_1->municipio_p;
            $localidad_p_1=$item_1->localidad_p;
            $estado_p_1=$item_1->estado_p;
            $cp_p_1=$item_1->cp_p;
          }
          $aux_genero_1_1='';
          $aux_genero_1_2='';
          if($genero_1==1){
            $aux_genero_1_1='checked';
          }else if($genero_1==2){
            $aux_genero_1_2='checked';  
          }else{
            $aux_genero_1_1='';
            $aux_genero_1_2=''; 
          }
          $clave_1_p_n='';
          $pais_1_p_n='';
          $tipo_cliente_p_f_m_1_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacimiento_1);
          foreach ($tipo_cliente_p_f_m_1_pais_1 as $item_1_p) {
            $clave_1_p_n=$item_1_p->clave;
            $pais_1_p_n=$item_1_p->pais;
          }
          $clave_1_p_n_2='';
          $pais_1_p_n_2='';
          $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacionalidad_1);
          foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
            $clave_1_p_n_2=$item_1_p->clave;
            $pais_1_p_n_2=$item_1_p->pais;
          }
          $clave_1_p_1='';
          $pais_1_p_1='';
          $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_d_1);
          foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
            $clave_1_p_1=$item_1_p->clave;
            $pais_1_p_1=$item_1_p->pais;
          }
          $checke_1_d='';
          if($trantadose_persona_1=='on'){
             $checke_1_d='checked';
          }
          $checke_1_r='';
          if($presente_legal_1=='on'){
             $checke_1_r='checked';
          }
          $aux_genero_1_1_p='';
          $aux_genero_1_2_p='';
          if($genero_p_1==1){
            $aux_genero_1_1_p='checked';
          }else if($genero_p_1==2){
            $aux_genero_1_2_p='checked';  
          }else{
            $aux_genero_1_1_p='';
            $aux_genero_1_2_p=''; 
          }
          $idcomplemento_persona_fisica=0;
          $actualmente_usted_precandidato1=0;
          $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1=0;
          $afirmativa_indicar_nombre_precandidato1='';
          $indicar_relacion1=0;
          $usted_socio_algun_precandidato1=0;
          $usted_asesor_agente_representante_algun_precandidato1=0;
          //$responsable_campania=0;
          $servicio_contratar_producto_comprar_dirigido1=0;
          $tercera_persona_precandidato1=0;
          $nombre_persona_beneficiaria1='';
          $relacion_mantiene_persona_beneficiaria1='';
          $indicar_razon_motivo_realizar_tramit1='';
          $indicar_relacion_mantiene_persona_beneficiaria1='';
          $usted_responsable_campanna_politica1=0;
          

          $complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_fisica','idperfilamiento',$idperfilamiento);
          foreach ($complento_pf1 as $cpf1) {
            $idcomplemento_persona_fisica=$cpf1->id;
            $actualmente_usted_precandidato1=$cpf1->actualmente_usted_precandidato;
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1=$cpf1->familiar_primer_segundo_tercer_grado_socio_algun_precandidato;
            $afirmativa_indicar_nombre_precandidato1=$cpf1->afirmativa_indicar_nombre_precandidato;
            $indicar_relacion1=$cpf1->indicar_relacion;
            $usted_socio_algun_precandidato1=$cpf1->usted_socio_algun_precandidato;
            $usted_asesor_agente_representante_algun_precandidato1=$cpf1->usted_asesor_agente_representante_algun_precandidato;
            $servicio_contratar_producto_comprar_dirigido1=$cpf1->servicio_contratar_producto_comprar_dirigido;
            $tercera_persona_precandidato1=$cpf1->tercera_persona_precandidato;
            $nombre_persona_beneficiaria1=$cpf1->nombre_persona_beneficiaria;
            $relacion_mantiene_persona_beneficiaria1=$cpf1->relacion_mantiene_persona_beneficiaria;
            $indicar_razon_motivo_realizar_tramit1=$cpf1->indicar_razon_motivo_realizar_tramit;
            $indicar_relacion_mantiene_persona_beneficiaria1=$cpf1->indicar_relacion_mantiene_persona_beneficiaria;
            $usted_responsable_campanna_politica1=$cpf1->usted_responsable_campanna_politica;
          }
          $actualmente_usted_precandidato1x1='';
          $actualmente_usted_precandidato1x2='';
          if($actualmente_usted_precandidato1==1){
            $actualmente_usted_precandidato1x1='checked';
            $actualmente_usted_precandidato1x2='';
          }else if($actualmente_usted_precandidato1==2){
            $actualmente_usted_precandidato1x1='';
            $actualmente_usted_precandidato1x2='checked';
          }

          $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='';
          $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2='';
          if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1==1){
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='checked';
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2='';
          }else if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1==2){
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='';
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2='checked';
          }

          $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
          $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
          $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          if($indicar_relacion1==1){
            $indicar_relacion1x1='checked';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==2){
            $indicar_relacion1x1='';$indicar_relacion1x2='checked';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==3){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='checked';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==4){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='checked';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==5){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='checked';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==6){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='checked';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==7){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='checked';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==8){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='checked';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==9){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='checked';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==10){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='checked';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==11){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='checked';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==12){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='checked';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==13){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='checked';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==14){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='checked';$indicar_relacion1x15='';
          }else if($indicar_relacion1==15){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='checked';
          }

          $usted_socio_algun_precandidato1x1='';
          $usted_socio_algun_precandidato1x2='';
          if($usted_socio_algun_precandidato1==1){
            $usted_socio_algun_precandidato1x1='checked';
            $usted_socio_algun_precandidato1x2='';
          }else if($usted_socio_algun_precandidato1==2){
            $usted_socio_algun_precandidato1x1='';
            $usted_socio_algun_precandidato1x2='checked';
          }

          $usted_asesor_agente_representante_algun_precandidato1x1='';
          $usted_asesor_agente_representante_algun_precandidato1x2='';
          if($usted_asesor_agente_representante_algun_precandidato1==1){
            $usted_asesor_agente_representante_algun_precandidato1x1='checked';
            $usted_asesor_agente_representante_algun_precandidato1x2='';
          }else if($usted_asesor_agente_representante_algun_precandidato1==2){
            $usted_asesor_agente_representante_algun_precandidato1x1='';
            $usted_asesor_agente_representante_algun_precandidato1x2='checked';
          }

          $servicio_contratar_producto_comprar_dirigido1x1='';
          $servicio_contratar_producto_comprar_dirigido1x2='';
          if($servicio_contratar_producto_comprar_dirigido1==1){
            $servicio_contratar_producto_comprar_dirigido1x1='checked';
            $servicio_contratar_producto_comprar_dirigido1x2='';
          }else if($servicio_contratar_producto_comprar_dirigido1==2){
            $servicio_contratar_producto_comprar_dirigido1x1='';
            $servicio_contratar_producto_comprar_dirigido1x2='checked';
          }

          $tercera_persona_precandidato1x1='';
          $tercera_persona_precandidato1x2='';
          if($tercera_persona_precandidato1==1){
            $tercera_persona_precandidato1x1='checked';
            $tercera_persona_precandidato1x2='';
          }else if($tercera_persona_precandidato1==2){
            $tercera_persona_precandidato1x1='';
            $tercera_persona_precandidato1x2='checked';
          }

          $usted_responsable_campanna_politica1x1='';
          $usted_responsable_campanna_politica1x2='';
          if($usted_responsable_campanna_politica1==1){
            $usted_responsable_campanna_politica1x1='checked';
            $usted_responsable_campanna_politica1x2='';
          }else if($usted_responsable_campanna_politica1==2){
            $usted_responsable_campanna_politica1x1='';
            $usted_responsable_campanna_politica1x2='checked';
          }

          $html.='<div class="row firma_tipo_cliente" style="display: none">
                      <div class="col-md-12">    
                        <h3>Formato: Conoce a tu cliente</h3>
                        <h3>Fecha: '.date("m-d-Y").'</h3><br>
                      </div>
                  </div>    
                  <hr class="subtitle barra_menu">
                  <div class="row">
                      <div class="col-md-12">    
                        <h3>Persona física mexicana o extranjera con condición de estancia temporal o permanente</h3>
                      </div>
                      <div class="col-md-12">
                      <br>
                        <h3>'.$paso2.'</h3>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                        <span>Lee cuidadosamente y rellena los campos  con los datos correspondientes. En caso de ser extranjero indicar el(los) apellido(s) que corresponda(n) y nombre(s).</span>
                      </div>
                  </div>
                  <br>
                  <form class="form" method="post" role="form" id="form_tipo_cliente_p_f_m">
                    <input type="hidden" name="idtipo_cliente_p_f_m" id="idtipo_cliente_p_f_m" value="'.$idtipo_cliente_p_f_m_1.'">
                    <div class="row">
                        <div class="col-md-4 form-group">
                          <label>Nombre(s):<span class="requi">*</span></label>
                          <input onchange="preGuardar1()" class="form-control" type="text" name="nombre" id="nombre" value="'.$nombre_1.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido paterno:<span class="requi">*</span></label>
                          <input class="form-control" type="text" name="apellido_paterno" id="apellido_paterno" value="'.$apellido_paterno_1.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido materno:</label>
                          <input class="form-control" type="text" name="apellido_materno" id="apellido_materno" value="'.$apellido_materno_1.'">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                          <label>Nombre de identificación:</label>
                          <input class="form-control" type="text" name="nombre_identificacion" value="'.$nombre_identificacion_1.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Autoridad que emite la identificación:</label>
                          <input class="form-control" type="text" name="autoridad_emite" value="'.$autoridad_emite_1.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Número de identificación:</label>
                          <input class="form-control" type="text" name="numero_identificacion" value="'.$numero_identificacion_1.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Género:</label>
                          <div class="row">
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="genero" checked value="1" '.$aux_genero_1_1.'>
                                  Masculino
                                </label>
                              </div>
                            </div>
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="genero" value="2" '.$aux_genero_1_2.'>
                                  Femenino
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>

                    ';
            $html.='<div class="row">
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                        <input class="form-control" type="date" max="'.date("Y-m-d").'" id="fecha_nacimiento_t_1" name="fecha_nacimiento" value="'.$fecha_nacimiento_1.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>País de nacimiento:</label><br>
                        <select class="form-control idpais_t_2" name="pais_nacimiento">
                          <option value="MX">MEXICO</option>';
                        if($pais_nacimiento_1!=''){
                  $html.='<option value="'.$clave_1_p_n.'" selected>'.$pais_1_p_n.'</option>';      
                        }
                $html.='</select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Pais de nacionalidad:</label><br>
                        <select class="form-control idpais_t_1" name="pais_nacionalidad">
                          <option value="MX">MEXICO</option>';
                        if($pais_nacionalidad_1!=''){
                  $html.='<option value="'.$clave_1_p_n_2.'" selected>'.$pais_1_p_n_2.'</option>';      
                        }  
                $html.='</select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
                        <select class="form-control actividad_fisica_1" name="activida_o_giro">
                          <option disabled selected>Selecciona una opción</option>';
                          foreach ($get_activida_fisica as $item) {
                            $selected="";
                            if($activida_o_giro_1==$item->clave){
                              $selected="selected";
                            }
                            $html.='<option value="'.$item->clave.'" '.$selected.'>'.$item->acitividad.'</option>';  
                          }
                $html.='</select>
                      </div>
                      <!--<div class="col-md-6 form-group">
                        <label>Otra ocupación:</label>
                        <input class="form-control" type="text" name="otra_ocupa" value="'.$otra_ocupa.'">
                      </div>-->
                    </div>
                    <!--//////////////////////-->
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>Dirección</h4>
                      </div>
                    </div>
                    <div class="row">  
                    <div class="col-md-4 form-group">
                        <label>País:</label><br>
                        <select class="form-control idpais_d_1" name="pais_d">
                          <option value="MX">MEXICO</option>';
                        if($pais_d_1!=''){
                  $html.='<option value="'.$clave_1_p_1.'" selected>'.$pais_1_p_1.'</option>';      
                        }
                $html.='</select>
                      </div>
                      <div class="col-md-2 form-group">
                        <label>Tipo de vialidad:</label>
                        <select class="form-control" name="tipo_vialidad_d" id="tipo_vialidad1">
                            <option disabled selected>Selecciona un tipo</option>';
                            foreach ($get_tipo_vialidad as $item){
                              if($item->id==$tipo_vialidad_d_1){
                                $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                              }  
                            }
                $html.='</select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Calle:</label>
                        <input class="form-control" type="text" name="calle_d" value="'.$calle_d_1.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.ext:</label>
                        <input class="form-control" type="text" name="no_ext_d" value="'.$no_ext_d_1.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.int:</label>
                        <input class="form-control" type="text" name="no_int_d" value="'.$no_int_d_1.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>C.P:</label>
                        <input onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" class="form-control" type="text" name="cp_d" value="'.$cp_d_1.'" id="cp_d1">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Municipio o delegación:</label>
                        <input id="municipio_d1" class="form-control" type="text" name="municipio_d" value="'.$municipio_d_1.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Localidad:</label>
                        <input class="form-control" type="text" name="localidad_d" value="'.$localidad_d_1.'" id="localidad_d">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Estado:</label>
                        <input readonly="" type="hidden" class="estado_text_d_1" value="'.$estado_d_1.'" id="estado_d1">
                        <span class="span_div_d_1"></span>
                      </div>
                      <!--<div class="col-md-3 form-group">
                        <label>Colonia:</label>
                        <input oninput="autoComplete(this.id);" id="colonia_d1" class="form-control" type="text" name="colonia_d" value="'.$colonia_d_1.'">
                      </div>-->';
                      //$html .="ESTA ES LA CLAVE DE PAIS 1: ".$clave_1_p_1;
                      if($clave_1_p_1=="MX"){
                        $html.='<div class="col-md-3 form-group" id="cont_colo">
                          <label>Colonia:</label><br>
                          <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d1">
                            <option value=""></option>';
                            if($colonia_d_1!=''){
                              $html.='<option value="'.$colonia_d_1.'" selected>'.$colonia_d_1.'</option>';      
                            }    
                          $html.='</select>
                        </div>';
                      }else if($clave_1_p_1!="MX" && $clave_1_p_1!=""){
                        $html.='<div class="col-md-3 form-group" id="cont_colo">
                          <label>Colonia:</label>
                          <input class="form-control" type="text" name="colonia_d" value="'.$colonia_d_1.'">
                        </div>';
                      } 
                      if($clave_1_p_1==""){
                        $html.='<div class="col-md-3 form-group" id="cont_colo">
                          <label>Colonia:</label><br>
                          <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d1">
                            <option value=""></option>';
                            if($colonia_d_1!=''){
                              $html.='<option value="'.$colonia_d_1.'" selected>'.$colonia_d_1.'</option>';      
                            }    
                          $html.='</select>
                        </div>';
                      }                    
                    $html.='</div>
                    <div class="row" style="page-break-before: always">
                      <div class="col-md-12 form-group">
                        <br><br>
                      </div>
                    </div>
                    <div class="row" >
                      <div class="col-md-12 form-group">
                        <div class="form-check form-check-primary">
                          <label class="form-check-label">Tratándose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio.
                            <input type="checkbox" class="form-check-input" name="trantadose_persona" id="lugar_recidencia_1" onchange="lugar_recidencia_btn()" '.$checke_1_d.'>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="residenciaEx_1" style="display: none;">
                      <div class="row">
                        <div class="col-md-12">
                          <span>Dirección en México:</span>
                        </div>
                      </div><br>
                      <div class="row">
                        <div class="col-md-2 form-group">
                          <label>Tipo de vialidad:</label>
                          <select class="form-control" name="tipo_vialidad_t">
                            <option disabled selected>Selecciona un tipo</option>';
                            foreach ($get_tipo_vialidad as $item){
                              if($item->id==$tipo_vialidad_t_1){
                                $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                              }  
                            }
                   $html.='</select>
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Calle:</label>
                          <input class="form-control" type="text" name="calle_t" value="'.$calle_t_1.'">
                        </div>
                        <div class="col-md-2 form-group">
                          <label>No.ext:</label>
                          <input class="form-control" type="text" name="no_ext_t" value="'.$no_ext_t_1.'">
                        </div>
                        <div class="col-md-2 form-group">
                          <label>No.int:</label>
                          <input class="form-control" type="text" name="no_int_t" value="'.$no_int_t_1.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>C.P:</label>
                          <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="cp_t" value="'.$cp_t_1.'" id="cp_t2">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Municipio o delegación:</label>
                          <input id="municipio_t2" class="form-control" type="text" name="municipio_t" value="'.$municipio_t_1.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Localidad:</label>
                          <input class="form-control" type="text" name="localidad_t" value="'.$localidad_t_1.'" id="localidad_t">
                        </div>
                        <!--<div class="col-md-3 form-group">
                          <label>Estado:</label>
                          <input class="form-control" type="text" name="estado_t" value="'.$estado_t_1.'">
                        </div>-->
                        <div class="col-md-3 form-group">
                          <label>Estado:</label>
                          <select class="form-control" name="estado_t" id="estado_t2">
                            <option disabled selected>Selecciona un estado</option>';
                            foreach ($estados as $item){
                              if($item->id==$estado_t_1){
                                $html.='<option value="'.$item->id.'" selected>'.$item->estado.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->estado.'</option>';
                              }  
                            }
                  $html.='</select>
                        </div>
                        <!--<div class="col-md-3 form-group">
                          <label>Colonia:</label>
                          <input oninput="autoComplete(this.id);" id="colonia_t2" class="form-control" type="text" name="colonia_t" value="'.$colonia_t_1.'">
                        </div>-->
                        <div class="col-md-3 form-group">
                          <label>Colonia:</label><br>
                          <select onclick="autoComplete(this.id)" class="form-control colonia" name="colonia_t" id="colonia_t2">
                            <option value=""></option>';
                            if($colonia_t_1!=''){
                              $html.='<option value="'.$colonia_t_1.'" selected>'.$colonia_t_1.'</option>';      
                             }  
                        $html.='</select>
                        </div>
                        
                      </div>
                      
                      <!-- -->
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                          <label>Correo electrónico:</label>
                          <input class="form-control" type="email" name="correo_t" value="'.$correo_t_1.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>R.F.C:</label>
                          <input class="form-control rfcPerf" type="text" name="r_f_c_t" value="'.$r_f_c_t_1.'">
                          <p>*Cuando cuente con R.F.C</p>
                        </div>
                        <div class="col-md-1 form-group barra_menu">
                          <br>
                          <a href="#modal_rfc" data-toggle="modal">
                          <button type="button" class="btn btn_ayuda">
                            <i class="mdi mdi-help"></i>
                          </button></a>
                        </div>
                        <div class="col-md-3 form-group">
                          <label>CURP:</label>
                          <input class="form-control" type="text" name="curp_t" value="'.$curp_t_1.'">
                          <p>*Cuando cuente con CURP</p>
                        </div>
                      </div>
                      <!-- paso 2_2 hoja 2-->
                      <div class="row paso_2_2_hoja_telefono">
                          <div class="col-md-3 form-group">
                            <label>Número de teléfono:</label>
                            <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="telefono_t" value="'.$telefono_t_1.'">
                          </div>
                      </div>
                    <!--//////////////////////-->
                    <div class="paso_2_1_hoja_presenta_apoderado_legal">
                      <div class="row">
                        <div class="col-md-12">
                          <hr class="subtitle">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <div class="form-check form-check-primary">
                            <label class="form-check-label"> Presenta apoderado legal.
                              <input type="checkbox" class="form-check-input" name="presente_legal" id="presenta_apoderado_1" onchange="presenta_apoderado_btn()" '.$checke_1_r.'>
                            </label>
                          </div>
                        </div>
                      </div>  
                      <div class="presenta_apoderado_style_1" style="display: none"> 
                        <div class="row">
                            <div class="col-md-4 form-group">
                              <label>Nombre(s):</label>
                              <input class="form-control" type="text" name="nombre_p" value="'.$nombre_p_1.'">
                            </div>
                            <div class="col-md-4 form-group">
                              <label>Apellido paterno:</label>
                              <input class="form-control" type="text" name="apellido_paterno_p" value="'.$apellido_paterno_p_1.'">
                            </div>
                            <div class="col-md-4 form-group">
                              <label>Apellido materno:</label>
                              <input class="form-control" type="text" name="apellido_materno_p" value="'.$apellido_materno_p_1.'">
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4 form-group">
                            <label>Nombre de identificación:</label>
                            <input class="form-control" type="text" name="nombre_identificacion_p" value="'.$nombre_identificacion_p_1.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Autoridad que emite la identificación:</label>
                            <input class="form-control" type="text" name="autoridad_emite_p" value="'.$autoridad_emite_p_1.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Número de identificación:</label>
                            <input class="form-control" type="text" name="numero_identificacion_p" value="'.$numero_identificacion_p_1.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Fecha de nacimiento:</label>
                            <input class="form-control" type="date" name="fecha_nac_apodera" value="'.$fecha_nac_apodera.'">
                          </div>
                          <br>
                          <div class="col-md-3 form-group">
                            <label>RFC:</label>
                            <input class="form-control" type="text" name="rfc_apodera" value="'.$rfc_apodera.'">
                          </div>
                          <div class="col-md-1 form-group barra_menu">
                            <br>
                            <a href="#modal_rfc" data-toggle="modal">
                            <button type="button" class="btn btn_ayuda">
                              <i class="mdi mdi-help"></i>
                            </button></a>
                          </div>
                          <div class="col-md-4 form-group">
                            <label>CURP:</label>
                            <input class="form-control" type="text" name="curp_apodera" value="'.$curp_apodera.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Género:</label>
                            <div class="row">
                              <div class="col-md-6 form-group">
                                <div class="form-check form-check-success">
                                  <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="genero_p" value="1" '.$aux_genero_1_1_p.'>
                                    Masculino
                                  </label>
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <div class="form-check form-check-success">
                                  <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="genero_p" value="2" '.$aux_genero_1_2_p.'>
                                    Femenino
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-2 form-group">
                            <label>Tipo de vialidad:</label>
                            <select class="form-control" name="tipo_vialidad_p">
                              <option disabled selected>Selecciona un tipo</option>';
                              foreach ($get_tipo_vialidad as $item){
                                if($item->id==$tipo_vialidad_p_1){
                                  $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                                }else{
                                  $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                                }  
                              }
                     $html.='</select>
                          </div>
                          <div class="col-md-3 form-group">
                            <label>Calle:</label>
                            <input class="form-control" type="text" name="calle_p" value="'.$calle_p_1.'">
                          </div>
                          <div class="col-md-2 form-group">
                            <label>No.ext:</label>
                            <input class="form-control" type="text" name="no_ext_p" value="'.$no_ext_p_1.'">
                          </div>
                          <div class="col-md-2 form-group">
                            <label>No.int:</label>
                            <input class="form-control" type="text" name="no_int_p" value="'.$no_int_p_1.'">
                          </div>
                          <div class="col-md-3 form-group">
                            <label>C.P:</label>
                            <input onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" class="form-control" type="text" name="cp_p" value="'.$cp_p_1.'" id="cp_p3">
                          </div>
                          <div class="col-md-3 form-group">
                            <label>Municipio o delegación:</label>
                            <input id="municipio_p3" class="form-control" type="text" name="municipio_p" value="'.$municipio_p_1.'">
                          </div>
                          <div class="col-md-3 form-group">
                            <label>Localidad:</label>
                            <input class="form-control" type="text" name="localidad_p" value="'.$localidad_p_1.'" id="localidad_p">
                          </div>
                          <!--<div class="col-md-3 form-group">
                            <label>Estado:</label>
                            <input class="form-control" type="text" name="estado_p" value="'.$estado_p_1.'">
                          </div>-->
                          <div class="col-md-3 form-group">
                            <label>Estado:</label>
                            <select class="form-control" name="estado_p" id="estado_p3">
                              <option disabled selected>Selecciona un estado</option>';
                              foreach ($estados as $item){
                                if($item->id==$estado_p_1){
                                  $html.='<option value="'.$item->id.'" selected>'.$item->estado.'</option>';
                                }else{
                                  $html.='<option value="'.$item->id.'" >'.$item->estado.'</option>';
                                }  
                              }
                            
                    $html.='</select>
                            </div>
                            <!--<div class="col-md-3 form-group">
                              <label>Colonia:</label>
                              <input oninput="autoComplete(this.id);" id="colonia_p3" class="form-control" type="text" name="colonia_p" value="'.$colonia_p_1.'">
                            </div>-->
                            <div class="col-md-3 form-group">
                              <label>Colonia:</label><br>
                              <select onclick="autoComplete(this.id)" class="form-control colonia" name="colonia_p" id="colonia_p3">
                                <option value=""></option>';
                                if($colonia_p_1!=''){
                                  $html.='<option value="'.$colonia_p_1.'" selected>'.$colonia_p_1.'</option>';      
                                 }  
                            $html.='</select>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </form>
                  <!--/////////// Replicado de beneficiario///////////-->
                  <hr class="subtitle barra_menu">
                  <div class="row" id="espa_benes" style="display:none; page-break-before: always">
                    <div class="col-md-12">
                      <br><br><br><br>
                    </div>
                  </div>
                  <div class="ptext_beneficiario_1">
                    <div class="text_beneficiario_1">
                    </div>
                  </div>
                  <div class="row barra_menu">
                    <div class="col-md-12" align="right">
                      <button type="button" class="btn gradient_nepal2" onclick="btn_add_beneficiario_1()"><i class="fa fa-plus"></i> Agregar otro beneficiario</button>
                    </div> 
                  </div>
                  <hr class="subtitle barra_menu">
                  <form method="post" role="form" id="form_complemento_persona_fisica" >
                    <input type="hidden" name="id" value="'.$idcomplemento_persona_fisica.'">
                    <div class="row" id="preg1">
                      <div class="col-md-10">
                        <label>¿Actualmente es usted precandidato(a), candidato(a), dirigente partidista, servidor público (a)?        
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" id="actualmente_usted_precandidato1" class="form-check-input" name="actualmente_usted_precandidato" value="1" '.$actualmente_usted_precandidato1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" id="actualmente_usted_precandidato2" class="form-check-input" name="actualmente_usted_precandidato" value="2" '.$actualmente_usted_precandidato1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg2">
                      <div class="col-md-10">
                        <label>¿Es familiar en primer, segundo o tercer grado o socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?
                          <button type="button" class="btn btn_ayuda" onclick="que_es_fpg()">
                            <i class="mdi mdi-help"></i></button>                              
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="familiar_primer_segundo_tercer_grado_socio_algun_precandidato" onclick="txt_precandidato();verPreguntas()" value="1" '.$familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="familiar_primer_segundo_tercer_grado_socio_algun_precandidato" onclick="txt_precandidato();verPreguntas()" value="2" '.$familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="texto_precandidato" style="display:none">
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Nombre del precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta:</label>
                          <input class="form-control" type="text" name="afirmativa_indicar_nombre_precandidato" value="'.$afirmativa_indicar_nombre_precandidato1.'">
                        </div>
                      </div>
                      <h6>Indicar Relación</h6> 
                      <div class="row">
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="1" '.$indicar_relacion1x1.'>
                              Esposo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="2" '.$indicar_relacion1x2.'>
                              Concubino(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="3" '.$indicar_relacion1x3.'>
                              Padre
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="4" '.$indicar_relacion1x4.'>
                              Madre
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="5" '.$indicar_relacion1x5.'>
                              Hermano(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="6" '.$indicar_relacion1x6.'>
                              Hijo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="7" '.$indicar_relacion1x7.'>
                              Hijastro(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="8" '.$indicar_relacion1x8.'>
                              Padrastro
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="9" '.$indicar_relacion1x9.'>
                              Madrastra
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="10" '.$indicar_relacion1x10.'>
                              Abuelo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="11" '.$indicar_relacion1x11.'>
                              Tio(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="12" '.$indicar_relacion1x12.'>
                              Sobrino(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="13" '.$indicar_relacion1x13.'>
                              Primo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="14" '.$indicar_relacion1x14.'>
                              Suegro(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="15" '.$indicar_relacion1x15.'>
                              Cuñado(a)
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg3">
                      <div class="col-md-10">
                        <label>¿Es usted socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                          
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_socio_algun_precandidato" value="1" '.$usted_socio_algun_precandidato1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_socio_algun_precandidato" value="2" '.$usted_socio_algun_precandidato1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg4">
                      <div class="col-md-10">
                        <label>¿Es usted asesor o agente o representante de  algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                       
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_asesor_agente_representante_algun_precandidato" value="1" '.$usted_asesor_agente_representante_algun_precandidato1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_asesor_agente_representante_algun_precandidato" value="2" '.$usted_asesor_agente_representante_algun_precandidato1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg5">
                      <div class="col-md-10">
                        <label>¿Es usted responsable de campaña política, encargado financiero y/ contable de algún partido político o de algún órgano interno de los partidos políticos y/o candidatos comunes y/o frentes y/o candidatos independientes?      
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_responsable_campanna_politica" value="1" '.$usted_responsable_campanna_politica1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_responsable_campanna_politica" value="2" '.$usted_responsable_campanna_politica1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg6">
                      <div class="col-md-10">
                        <label>¿El servicio a contratar o producto a comprar está dirigido o es para el beneficio de alguna tercera persona?                     
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="servicio_contratar_producto_comprar_dirigido" onclick="txt_servicio_contratar_producto();verPreguntas()" value="1" '.$servicio_contratar_producto_comprar_dirigido1x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="servicio_contratar_producto_comprar_dirigido" onclick="txt_servicio_contratar_producto();verPreguntas()" value="2" '.$servicio_contratar_producto_comprar_dirigido1x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="txt_tercera_persona_precandidato" style="display:none">
                      <div class="row">
                        <div class="col-md-10">
                          <label>La tercera persona es precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                   
                          </label>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="row">
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="tercera_persona_precandidato" onclick="txt_tercera_persona_precandidato();verPreguntas()" value="1" '.$tercera_persona_precandidato1x1.'>
                                  Si
                                </label>
                              </div>
                            </div>
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="tercera_persona_precandidato" onclick="txt_tercera_persona_precandidato();verPreguntas()" value="2" '.$tercera_persona_precandidato1x2.'>
                                  No
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="formulario_tercera" style="display:none">
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Favor de indicar el nombre de la persona beneficiaria
                          </label>
                          <input class="form-control" type="text" name="nombre_persona_beneficiaria" value="'.$nombre_persona_beneficiaria1.'">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Indicar la relación que mantiene con la persona beneficiaria
                          </label>
                          <input class="form-control" type="text" name="relacion_mantiene_persona_beneficiaria" value="'.$relacion_mantiene_persona_beneficiaria1.'">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Indicar por qué razón o motivo acude a realizar el trámite o compra para la tercera persona
                          </label>
                          <input class="form-control" type="text" name="indicar_razon_motivo_realizar_tramit" value="'.$indicar_razon_motivo_realizar_tramit1.'">
                        </div>
                      </div>
                      <!--<div class="row">
                        <div class="col-md-12 form-group">
                          <label>Indicar la relación que mantiene con la persona beneficiaria
                          </label>
                          <input class="form-control" type="text" name="indicar_relacion_mantiene_persona_beneficiaria" value="'.$indicar_relacion_mantiene_persona_beneficiaria1.'">
                        </div>
                      </div>-->
                    </div>
                    
                  </form>  
                  ';     
          $html.='<hr class="subtitle barra_menu">
                <div class="row barra_menu"> 
                    <div class="col-md-12" align="right">';
                    if($idtipo_cliente_p_f_m_1>0){
                      $html.='<button type="button" class="btn gradient_nepal2" onclick="imprimir_formato1(1,'.$idperfilamiento.')"><i class="fa fa-print"></i> Imprimir formato conoce a tu cliente</button>';
                    }
                    $html.='<button type="button" class="btn gradient_nepal2" onclick="regresa()"><i class="fa fa-reply" aria-hidden="true"></i> Regresar</button>
                      <button type="button" class="btn gradient_nepal2" onclick="guardar_tipo_cliente_1()"><i class="fa fa-save" aria-hidden="true"></i>  Guardar</button>
                    </div>
                </div>';   
      }else if($tipo==2){
        $idtipo_cliente_p_f_e_2=0;
        $nombre_2='';
        $apellido_paterno_2='';
        $apellido_materno_2='';
        $nombre_identificacion_2='';
        $autoridad_emite_2='';
        $numero_identificacion_2='';
        $genero_2='';
        $fecha_nacimiento_2='';
        $pais_nacimiento_2='';
        $pais_nacionalidad_2='';
        $actividad_o_giro_2='';
        $otra_ocupa="";
        $tipo_vialidad_d_2='';
        $calle_d_2='';
        $no_ext_d_2='';
        $no_int_d_2='';
        $colonia_d_2='';
        $municipio_d_2='';
        $localidad_d_2='';
        $estado_d_2='';
        $cp_d_2='';
        $pais_d_2='';
        $trantadose_persona_2='';
        $tipo_vialidad_t_2='';
        $calle_t_2='';
        $no_ext_t_2='';
        $no_int_t_2='';
        $colonia_t_2='';
        $municipio_t_2='';
        $localidad_t_2='';
        $estado_t_2='';
        $cp_t_2='';
        $correo_t_2='';
        $r_f_c_t_2='';
        $curp_t_2='';
        $telefono_t_2='';
        
        $arraywhere_2 = array('idperfilamiento'=>$idperfilamiento); 
        $tipo_cliente_p_f_e=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_e',$arraywhere_2);
        foreach ($tipo_cliente_p_f_e as $item_2) {
          $idtipo_cliente_p_f_e_2=$item_2->idtipo_cliente_p_f_e;
          $nombre_2=$item_2->nombre;
          $apellido_paterno_2=$item_2->apellido_paterno;
          $apellido_materno_2=$item_2->apellido_materno;
          $nombre_identificacion_2=$item_2->nombre_identificacion;
          $autoridad_emite_2=$item_2->autoridad_emite;
          $numero_identificacion_2=$item_2->numero_identificacion;
          $genero_2=$item_2->genero;
          $fecha_nacimiento_2=$item_2->fecha_nacimiento;
          $pais_nacimiento_2=$item_2->pais_nacimiento;
          $pais_nacionalidad_2=$item_2->pais_nacionalidad;
          $actividad_o_giro_2=$item_2->actividad_o_giro;
          $otra_ocupa=$item_2->otra_ocupa;
          $tipo_vialidad_d_2=$item_2->tipo_vialidad_d;
          $calle_d_2=$item_2->calle_d;
          $no_ext_d_2=$item_2->no_ext_d;
          $no_int_d_2=$item_2->no_int_d;
          $colonia_d_2=$item_2->colonia_d;
          $municipio_d_2=$item_2->municipio_d ;
          $localidad_d_2=$item_2->localidad_d;
          $estado_d_2=$item_2->estado_d;
          $cp_d_2=$item_2->cp_d;
          $pais_d_2=$item_2->pais_d;
          $trantadose_persona_2=$item_2->trantadose_persona;
          $tipo_vialidad_t_2=$item_2->tipo_vialidad_t;
          $calle_t_2=$item_2->calle_t;
          $no_ext_t_2=$item_2->no_ext_t;
          $no_int_t_2=$item_2->no_int_t;
          $colonia_t_2=$item_2->colonia_t;
          $municipio_t_2=$item_2->municipio_t;
          $localidad_t_2=$item_2->localidad_t;
          $estado_t_2=$item_2->estado_t;
          $cp_t_2=$item_2->cp_t;
          $correo_t_2=$item_2->correo_t;
          $r_f_c_t_2=$item_2->r_f_c_t;
          $curp_t_2=$item_2->curp_t;
          $telefono_t_2=$item_2->telefono_t;
        }
          $aux_genero_2_1='';
          $aux_genero_2_2='';
          if($genero_2==1){
            $aux_genero_2_1='checked';
          }else if($genero_2==2){
            $aux_genero_2_2='checked';  
          }
          $clave_2_p_n='';
          $pais_2_p_n='';
          $tipo_cliente_p_f_e_2_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacimiento_2);
          foreach ($tipo_cliente_p_f_e_2_pais_1 as $item_2_p) {
            $clave_2_p_n=$item_2_p->clave;
            $pais_2_p_n=$item_2_p->pais;
          }
          $clave_2_p_n_2='';
          $pais_2_p_n_2='';
          $tipo_cliente_p_f_e_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacionalidad_2);
          foreach ($tipo_cliente_p_f_e_1_pais_2 as $item_1_p) {
            $clave_2_p_n_2=$item_1_p->clave;
            $pais_2_p_n_2=$item_1_p->pais;
          }
          $clave_2_p_1='';
          $pais_2_p_1='';
          $tipo_cliente_p_f_e_2_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_d_2);
          foreach ($tipo_cliente_p_f_e_2_pais_2 as $item_2p) {
            $clave_2_p_1=$item_2p->clave;
            $pais_2_p_1=$item_2p->pais;
          }
          $checke_2_d='';
          if($trantadose_persona_2=='on'){
             $checke_2_d='checked';
          }
          
          $idcomplemento_persona_fisica=0;
          $actualmente_usted_precandidato1=0;
          $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1=0;
          $afirmativa_indicar_nombre_precandidato1='';
          $indicar_relacion1=0;
          $usted_socio_algun_precandidato1=0;
          $usted_asesor_agente_representante_algun_precandidato1=0;
          $servicio_contratar_producto_comprar_dirigido1=0;
          $tercera_persona_precandidato1=0;
          $nombre_persona_beneficiaria1='';
          $relacion_mantiene_persona_beneficiaria1='';
          $indicar_razon_motivo_realizar_tramit1='';
          $indicar_relacion_mantiene_persona_beneficiaria1='';
          $usted_responsable_campanna_politica1=0;
          
          $complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_fisica','idperfilamiento',$idperfilamiento);
          foreach ($complento_pf1 as $cpf1) {
            $idcomplemento_persona_fisica=$cpf1->id;
            $actualmente_usted_precandidato1=$cpf1->actualmente_usted_precandidato;
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1=$cpf1->familiar_primer_segundo_tercer_grado_socio_algun_precandidato;
            $afirmativa_indicar_nombre_precandidato1=$cpf1->afirmativa_indicar_nombre_precandidato;
            $indicar_relacion1=$cpf1->indicar_relacion;
            $usted_socio_algun_precandidato1=$cpf1->usted_socio_algun_precandidato;
            $usted_asesor_agente_representante_algun_precandidato1=$cpf1->usted_asesor_agente_representante_algun_precandidato;
            $servicio_contratar_producto_comprar_dirigido1=$cpf1->servicio_contratar_producto_comprar_dirigido;
            $tercera_persona_precandidato1=$cpf1->tercera_persona_precandidato;
            $nombre_persona_beneficiaria1=$cpf1->nombre_persona_beneficiaria;
            $relacion_mantiene_persona_beneficiaria1=$cpf1->relacion_mantiene_persona_beneficiaria;
            $indicar_razon_motivo_realizar_tramit1=$cpf1->indicar_razon_motivo_realizar_tramit;
            $indicar_relacion_mantiene_persona_beneficiaria1=$cpf1->indicar_relacion_mantiene_persona_beneficiaria;
            $usted_responsable_campanna_politica1=$cpf1->usted_responsable_campanna_politica;
          }
          $actualmente_usted_precandidato1x1='';
          $actualmente_usted_precandidato1x2='';
          if($actualmente_usted_precandidato1==1){
            $actualmente_usted_precandidato1x1='checked';
            $actualmente_usted_precandidato1x2='';
          }else if($actualmente_usted_precandidato1==2){
            $actualmente_usted_precandidato1x1='';
            $actualmente_usted_precandidato1x2='checked';
          }

          $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='';
          $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2='';
          if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1==1){
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='checked';
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2='';
          }else if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1==2){
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='';
            $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2='checked';
          }

          $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
          $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
          $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          if($indicar_relacion1==1){
            $indicar_relacion1x1='checked';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==2){
            $indicar_relacion1x1='';$indicar_relacion1x2='checked';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==3){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='checked';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==4){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='checked';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==5){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='checked';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==6){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='checked';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==7){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='checked';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==8){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='checked';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==9){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='checked';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==10){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='checked';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==11){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='checked';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==12){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='checked';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==13){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='checked';$indicar_relacion1x14='';$indicar_relacion1x15='';
          }else if($indicar_relacion1==14){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='checked';$indicar_relacion1x15='';
          }else if($indicar_relacion1==15){
            $indicar_relacion1x1='';$indicar_relacion1x2='';$indicar_relacion1x3='';$indicar_relacion1x4='';$indicar_relacion1x5='';
            $indicar_relacion1x6='';$indicar_relacion1x7='';$indicar_relacion1x8='';$indicar_relacion1x9='';$indicar_relacion1x10='';
            $indicar_relacion1x11='';$indicar_relacion1x12='';$indicar_relacion1x13='';$indicar_relacion1x14='';$indicar_relacion1x15='checked';
          }

          $usted_socio_algun_precandidato1x1='';
          $usted_socio_algun_precandidato1x2='';
          if($usted_socio_algun_precandidato1==1){
            $usted_socio_algun_precandidato1x1='checked';
            $usted_socio_algun_precandidato1x2='';
          }else if($usted_socio_algun_precandidato1==2){
            $usted_socio_algun_precandidato1x1='';
            $usted_socio_algun_precandidato1x2='checked';
          }

          $usted_asesor_agente_representante_algun_precandidato1x1='';
          $usted_asesor_agente_representante_algun_precandidato1x2='';
          if($usted_asesor_agente_representante_algun_precandidato1==1){
            $usted_asesor_agente_representante_algun_precandidato1x1='checked';
            $usted_asesor_agente_representante_algun_precandidato1x2='';
          }else if($usted_asesor_agente_representante_algun_precandidato1==2){
            $usted_asesor_agente_representante_algun_precandidato1x1='';
            $usted_asesor_agente_representante_algun_precandidato1x2='checked';
          }

          $servicio_contratar_producto_comprar_dirigido1x1='';
          $servicio_contratar_producto_comprar_dirigido1x2='';
          if($servicio_contratar_producto_comprar_dirigido1==1){
            $servicio_contratar_producto_comprar_dirigido1x1='checked';
            $servicio_contratar_producto_comprar_dirigido1x2='';
          }else if($servicio_contratar_producto_comprar_dirigido1==2){
            $servicio_contratar_producto_comprar_dirigido1x1='';
            $servicio_contratar_producto_comprar_dirigido1x2='checked';
          }

          $tercera_persona_precandidato1x1='';
          $tercera_persona_precandidato1x2='';
          if($tercera_persona_precandidato1==1){
            $tercera_persona_precandidato1x1='checked';
            $tercera_persona_precandidato1x2='';
          }else if($tercera_persona_precandidato1==2){
            $tercera_persona_precandidato1x1='';
            $tercera_persona_precandidato1x2='checked';
          }

          $usted_responsable_campanna_politica1x1='';
          $usted_responsable_campanna_politica1x2='';
          if($usted_responsable_campanna_politica1==1){
            $usted_responsable_campanna_politica1x1='checked';
            $usted_responsable_campanna_politica1x2='';
          }else if($usted_responsable_campanna_politica1==2){
            $usted_responsable_campanna_politica1x1='';
            $usted_responsable_campanna_politica1x2='checked';
          }

        $html.='<div class="row firma_tipo_cliente" style="display: none">
                      <div class="col-md-12">    
                        <h3>Formato: Conoce a tu cliente</h3>
                        <h3>Fecha: '.date("m-d-Y").'</h3><br>
                      </div>
                  </div>  
                <hr class="subtitle barra_menu">
                  <div class="row">
                      <div class="col-md-12">    
                        <h3>Persona física extranjera con condición de estancia de visitante</h3>
                      </div>
                      <div class="col-md-12">   
                        <br> 
                        <h3>'.$paso2.'</h3>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                        <span>Nombre y apellido completos que correspondan:</span>
                      </div>
                  </div>
                  <br>
                  <form class="form" method="post" role="form" id="form_tipo_cliente_p_f_e">
                    <input type="hidden" name="idtipo_cliente_p_f_e" id="idtipo_cliente_p_f_e" value="'.$idtipo_cliente_p_f_e_2.'">
                    <div class="row">
                        <div class="col-md-4 form-group">
                          <label>Nombre(s):</label>
                          <input class="form-control" type="text" name="nombre" id="nombre" value="'.$nombre_2.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido paterno:</label>
                          <input class="form-control" type="text" name="apellido_paterno" id="apellido_paterno" value="'.$apellido_paterno_2.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido materno:</label>
                          <input class="form-control" type="text" name="apellido_materno" id="apellido_materno" value="'.$apellido_materno_2.'">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-4 form-group">
                          <label>Nombre de identificación:</label>
                          <input class="form-control" type="text" name="nombre_identificacion" value="'.$nombre_identificacion_2.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Autoridad que emite la identificación:</label>
                          <input class="form-control" type="text" name="autoridad_emite" value="'.$autoridad_emite_2.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Número de identificación:</label>
                          <input class="form-control" type="text" name="numero_identificacion" value="'.$numero_identificacion_2.'">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Género:</label>
                          <div class="row">
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="genero" checked value="1" '.$aux_genero_2_1.'>
                                  Masculino
                                </label>
                              </div>
                            </div>
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="genero" value="2" '.$aux_genero_2_2.'>
                                  Femenino
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    ';
            $html.='<div class="row">
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                        <input class="form-control" type="date" max="'.date("Y-m-d").'" name="fecha_nacimiento" value="'.$fecha_nacimiento_2.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>País de nacimiento:</label><br>
                        <select class="form-control idpais_t2_2" name="pais_nacimiento"> 
                         <option value="MX">MEXICO</option>';
                        if($pais_nacimiento_2!=''){
                  $html.='<option value="'.$clave_2_p_n.'" selected>'.$pais_2_p_n.'</option>';      
                        }
                $html.='</select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Pais de nacionalidad:</label><br>
                        <select class="form-control idpais_t2_1" name="pais_nacionalidad">
                          <option value="MX">MEXICO</option>';
                        if($pais_nacionalidad_2!=''){
                  $html.='<option value="'.$clave_2_p_n_2.'" selected>'.$pais_2_p_n_2.'</option>';      
                        }  
                $html.='</select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
                        <select class="form-control actividad_fisica_2" name="actividad_o_giro">
                          <option disabled selected>Selecciona una opción</option>';
                          foreach ($get_activida_fisica as $item) {
                            $selected="";
                            if($actividad_o_giro_2==$item->clave){
                              $selected="selected";
                            }
                            $html.='<option value="'.$item->clave.'" '.$selected.'>'.$item->acitividad.'</option>';  
                          }
                $html.='</select>
                      </div>
                      <!--<div class="col-md-6 form-group">
                        <label>Otra ocupación:</label>
                        <input class="form-control" type="text" name="otra_ocupa" value="'.$otra_ocupa.'">
                      </div>-->
                    </div>
                    <!--//////////////////////-->
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>Dirección</h4>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-md-3 form-group">
                        <label>País:</label><br>
                        <select class="form-control idpais_d_2" name="pais_d">
                          <option value="MX">MEXICO</option>';
                        if($pais_d_2!=''){
                  $html.='<option value="'.$clave_2_p_1.'" selected>'.$pais_2_p_1.'</option>';      
                        }
                $html.='</select>
                      </div> 
                      <div class="col-md-2 form-group">
                        <label>Tipo de vialidad:</label>
                        <select class="form-control" name="tipo_vialidad_d" id="tipo_vialidad1">
                            <option disabled selected>Selecciona un tipo</option>';
                            foreach ($get_tipo_vialidad as $item){
                              if($item->id==$tipo_vialidad_d_2){
                                $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                              }  
                            }
                $html.='</select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Calle:</label>
                        <input class="form-control" type="text" name="calle_d" value="'.$calle_d_2.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.ext:</label>
                        <input class="form-control" type="text" name="no_ext_d" value="'.$no_ext_d_2.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.int:</label>
                        <input class="form-control" type="text" name="no_int_d" value="'.$no_int_d_2.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>C.P:</label>
                        <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="cp_d" value="'.$cp_d_2.'" id="cp_d2">
                      </div>
                      
                      <div class="col-md-3 form-group">
                        <label>Municipio o delegación:</label>
                        <input id="municipio_d2" class="form-control" type="text" name="municipio_d" value="'.$municipio_d_2.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Localidad:</label>
                        <input class="form-control" type="text" name="localidad_d" value="'.$localidad_d_2.'" id="localidad_d">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Estado:</label>
                        <input readonly="" type="hidden" class="estado_text_d_2" value="'.$estado_d_2.'" id="estado_d1">
                        <span class="span_div_d_2"></span>
                      </div>
                      <!--<div class="col-md-3 form-group">
                        <label>Colonia:</label>
                        <input class="form-control" type="text" name="colonia_d" value="'.$colonia_d_2.'">
                      </div>-->';
                      if($pais_d_2=="MX"){
                        $html.='<div class="col-md-3 form-group" id="cont_colo">
                          <label>Colonia:</label><br>
                          <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d1">
                            <option value=""></option>';
                            if($colonia_d_2!=''){
                              $html.='<option value="'.$colonia_d_2.'" selected>'.$colonia_d_2.'</option>';      
                            }    
                          $html.='</select>
                        </div>';
                      }else if($pais_d_2!="MX" && $pais_d_2!=""){
                        $html.='<div class="col-md-3 form-group" id="cont_colo">
                          <label>Colonia:</label>
                          <input class="form-control" type="text" name="colonia_d" value="'.$colonia_d_2.'">
                        </div>';
                      }if($pais_d_2==""){
                        $html.='<div class="col-md-3 form-group" id="cont_colo">
                          <label>Colonia:</label><br>
                          <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d1">
                            <option value=""></option>';
                            if($colonia_d_2!=''){
                              $html.='<option value="'.$colonia_d_2.'" selected>'.$colonia_d_2.'</option>';      
                            }    
                          $html.='</select>
                        </div>';
                      }
                    $html.='</div>
                    <div class="row" id="espa_benes" style="display:none; page-break-before: always">
                      <div class="col-md-12">
                        <br><br><br><br>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <div class="form-check form-check-primary">
                          <label class="form-check-label">Tratándose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio.
                            <input type="checkbox" class="form-check-input" name="trantadose_persona" id="lugar_recidencia_2" onchange="lugar_recidencia_btn()" '.$checke_2_d.'>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="residenciaEx_2" style="display: none">
                      <div class="row">
                        <div class="col-md-12">
                          <span>Dirección en México.</span>
                        </div>
                      </div><br>
                      <div class="row">
                        <div class="col-md-2 form-group">
                          <label>Tipo de vialidad:</label>
                          <select class="form-control" name="tipo_vialidad_t">
                            <option disabled selected>Selecciona un tipo</option>';
                            foreach ($get_tipo_vialidad as $item){
                              if($item->id==$tipo_vialidad_t_2){
                                $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                              }  
                            }
                   $html.='</select>
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Calle:</label>
                          <input class="form-control" type="text" name="calle_t" value="'.$calle_t_2.'">
                        </div>
                        <div class="col-md-2 form-group">
                          <label>No.ext:</label>
                          <input class="form-control" type="text" name="no_ext_t" value="'.$no_ext_t_2.'">
                        </div>
                        <div class="col-md-2 form-group">
                          <label>No.int:</label>
                          <input class="form-control" type="text" name="no_int_t" value="'.$no_int_t_2.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>C.P:</label>
                          <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="cp_t" value="'.$cp_t_2.'" id="cp_t2">
                        </div>
                        
                        <div class="col-md-3 form-group">
                          <label>Municipio o delegación:</label>
                          <input id="municipio_t2" class="form-control" type="text" name="municipio_t" value="'.$municipio_t_2.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Localidad:</label>
                          <input class="form-control" type="text" name="localidad_t" value="'.$localidad_t_2.'" id="localidad_t">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Estado:</label>
                          <select class="form-control" name="estado_t" id="estado_t2">
                              <option disabled selected>Selecciona un estado</option>';
                              foreach ($estados as $item){
                                if($item->id==$estado_t_2){
                                  $html.='<option value="'.$item->id.'" selected>'.$item->estado.'</option>';
                                }else{
                                  $html.='<option value="'.$item->id.'" >'.$item->estado.'</option>';
                                }  
                              }
                            
                  $html.='</select>
                        </div>
                        <!--<div class="col-md-3 form-group">
                          <label>Colonia:</label>
                          <input class="form-control" type="text" name="colonia_t" value="'.$colonia_t_2.'">
                        </div>-->
                        <div class="col-md-3 form-group">
                          <label>Colonia:</label><br>
                          <select onclick="autoComplete(this.id)" class="form-control" name="colonia_t" id="colonia_t2">
                            <option value=""></option>';
                            if($colonia_t_2!=''){
                              $html.='<option value="'.$colonia_t_2.'" selected>'.$colonia_t_2.'</option>';      
                             }  
                        $html.='</select>
                        </div>
                      </div>
                      
                      <!-- -->
                    </div>
                    <div class="row">
                        <div class="col-md-5 form-group">
                          <label>Correo electrónico:</label>
                          <input class="form-control" type="email" name="correo_t" value="'.$correo_t_2.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>R.F.C:</label>
                          <input class="form-control rfcPerf" type="text" name="r_f_c_t" value="'.$r_f_c_t_2.'"><p>*Cuando cuente con R.F.C</p>
                        </div>
                        <div class="col-md-1 form-group barra_menu">
                          <br>
                          <a href="#modal_rfc" data-toggle="modal">
                          <button type="button" class="btn btn_ayuda">
                            <i class="mdi mdi-help"></i>
                          </button></a>
                        </div>
                        <div class="col-md-3 form-group">
                          <label>CURP:</label>
                          <input class="form-control" type="text" name="curp_t" value="'.$curp_t_2.'">
                          <p>*Cuando cuente con CURP</p>
                        </div>
                      </div>
                      <!-- paso 2_2 hoja 2-->
                      <div class="row paso_2_2_hoja_telefono">
                          <div class="col-md-3 form-group">
                            <label>Número de teléfono:</label>
                            <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="telefono_t" value="'.$telefono_t_2.'">
                          </div>
                      </div>
                    </form>
                  <!--/////////// Replicado de beneficiario///////////-->
                  <hr class="subtitle">
                  <div class="ptext_beneficiario_2">
                    <div class="text_beneficiario_2">
                    </div>
                  </div>
                  <div class="row barra_menu">
                    <div class="col-md-12" align="right">
                      <button type="button" class="btn gradient_nepal2" onclick="btn_add_beneficiario_2()"><i class="fa fa-plus"></i> Agregar otro beneficiario</button>
                    </div> 
                  </div>

                  <hr class="subtitle barra_menu">
                  <div class="row" id="espa_preg_2" style=" page-break-before: always">
                    <div class="col-md-12"><br><br><br>
                    </div>
                  </div>
                  <form method="post" role="form" id="form_complemento_persona_fisica">
                    <input type="hidden" name="id" value="'.$idcomplemento_persona_fisica.'">
                    <div class="row" id="preg1">
                      <div class="col-md-10">
                        <label>¿Actualmente es usted precandidato(a), candidato(a), dirigente partidista, servidor público (a)?        
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" id="actualmente_usted_precandidato1" class="form-check-input" name="actualmente_usted_precandidato" value="1" '.$actualmente_usted_precandidato1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" id="actualmente_usted_precandidato2" class="form-check-input" name="actualmente_usted_precandidato" value="2" '.$actualmente_usted_precandidato1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg2">
                      <div class="col-md-10">
                        <label>¿Es familiar en primer, segundo o tercer grado o socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta? 
                          <button type="button" class="btn btn_ayuda" onclick="que_es_fpg()">
                            <i class="mdi mdi-help"></i></button>               
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="familiar_primer_segundo_tercer_grado_socio_algun_precandidato" onclick="txt_precandidato();verPreguntas()" value="1" '.$familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="familiar_primer_segundo_tercer_grado_socio_algun_precandidato" onclick="txt_precandidato();verPreguntas()" value="2" '.$familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="texto_precandidato" style="display:none">
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Nombre del precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta:</label>
                          <input class="form-control" type="text" name="afirmativa_indicar_nombre_precandidato" value="'.$afirmativa_indicar_nombre_precandidato1.'">
                        </div>
                      </div>
                      <h6>Indicar Relación</h6> 
                      <div class="row">
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="1" '.$indicar_relacion1x1.'>
                              Esposo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="2" '.$indicar_relacion1x2.'>
                              Concubino(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="3" '.$indicar_relacion1x3.'>
                              Padre
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="4" '.$indicar_relacion1x4.'>
                              Madre
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="5" '.$indicar_relacion1x5.'>
                              Hermano(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="6" '.$indicar_relacion1x6.'>
                              Hijo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="7" '.$indicar_relacion1x7.'>
                              Hijastro(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="8" '.$indicar_relacion1x8.'>
                              Padrastro
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="9" '.$indicar_relacion1x9.'>
                              Madrastra
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="10" '.$indicar_relacion1x10.'>
                              Abuelo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="11" '.$indicar_relacion1x11.'>
                              Tio(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="12" '.$indicar_relacion1x12.'>
                              Sobrino(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="13" '.$indicar_relacion1x13.'>
                              Primo(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="14" '.$indicar_relacion1x14.'>
                              Suegro(a)
                            </label>
                          </div>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="indicar_relacion" value="15" '.$indicar_relacion1x15.'>
                              Cuñado(a)
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg3">
                      <div class="col-md-10">
                        <label>¿Es usted socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                          
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_socio_algun_precandidato" value="1" '.$usted_socio_algun_precandidato1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_socio_algun_precandidato" value="2" '.$usted_socio_algun_precandidato1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg4">
                      <div class="col-md-10">
                        <label>¿Es usted asesor o agente o representante de  algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                       
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_asesor_agente_representante_algun_precandidato" value="1" '.$usted_asesor_agente_representante_algun_precandidato1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_asesor_agente_representante_algun_precandidato" value="2" '.$usted_asesor_agente_representante_algun_precandidato1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg5">
                      <div class="col-md-10">
                        <label>¿Es usted responsable de campaña política, encargado financiero y/ contable de algún partido político o de algún órgano interno de los partidos políticos y/o candidatos comunes y/o frentes y/o candidatos independientes?      
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_responsable_campanna_politica" value="1" '.$usted_responsable_campanna_politica1x1.' onclick="verPreguntas()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="usted_responsable_campanna_politica" value="2" '.$usted_responsable_campanna_politica1x2.' onclick="verPreguntas()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="preg6">
                      <div class="col-md-10">
                        <label>¿El servicio a contratar o producto a comprar está dirigido o es para el beneficio de alguna tercera persona?                     
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="servicio_contratar_producto_comprar_dirigido" onclick="txt_servicio_contratar_producto();verPreguntas()" value="1" '.$servicio_contratar_producto_comprar_dirigido1x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="servicio_contratar_producto_comprar_dirigido" onclick="txt_servicio_contratar_producto();verPreguntas()" value="2" '.$servicio_contratar_producto_comprar_dirigido1x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="txt_tercera_persona_precandidato" style="display:none">
                      <div class="row">
                        <div class="col-md-10">
                          <label>La tercera persona es precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                   
                          </label>
                        </div>
                        <div class="col-md-2 form-group">
                          <div class="row">
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="tercera_persona_precandidato" onclick="txt_tercera_persona_precandidato();verPreguntas()" value="1" '.$tercera_persona_precandidato1x1.'>
                                  Si
                                </label>
                              </div>
                            </div>
                            <div class="col-md-6 form-group">
                              <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="tercera_persona_precandidato" onclick="txt_tercera_persona_precandidato();verPreguntas()" value="2" '.$tercera_persona_precandidato1x2.'>
                                  No
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="formulario_tercera" style="display:none">
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Favor de indicar el nombre de la persona beneficiaria
                          </label>
                          <input class="form-control" type="text" name="nombre_persona_beneficiaria" value="'.$nombre_persona_beneficiaria1.'">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Indicar la relación que mantiene con la persona beneficiaria
                          </label>
                          <input class="form-control" type="text" name="relacion_mantiene_persona_beneficiaria" value="'.$relacion_mantiene_persona_beneficiaria1.'">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Indicar por qué razón o motivo acude a realizar el trámite o compra para la tercera persona
                          </label>
                          <input class="form-control" type="text" name="indicar_razon_motivo_realizar_tramit" value="'.$indicar_razon_motivo_realizar_tramit1.'">
                        </div>
                      </div>
                      <!--<div class="row">
                        <div class="col-md-12 form-group">
                          <label>Indicar la relación que mantiene con la persona beneficiaria
                          </label>
                          <input class="form-control" type="text" name="indicar_relacion_mantiene_persona_beneficiaria" value="'.$indicar_relacion_mantiene_persona_beneficiaria1.'">
                        </div>
                      </div>-->
                    </div>
                    
                  </form>
                  ';   
          $html.='<hr class="subtitle barra_menu">
                <div class="row barra_menu"> 
                    <div class="col-md-12" align="right">';
                      if($idtipo_cliente_p_f_e_2>0){
                        $html.='<button type="button" class="btn gradient_nepal2" onclick="imprimir_formato1(2,'.$idperfilamiento.')"><i class="fa fa-print"></i> Imprimir formato conoce a tu cliente</button>';
                      }
                      $html.='<button type="button" class="btn gradient_nepal2" onclick="regresa()"><i class="fa fa-reply" aria-hidden="true"></i> Regresar</button>
                      <button type="button" class="btn gradient_nepal2" onclick="guardar_tipo_cliente_2()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>';          
      }else if($tipo==3){

          $idtipo_cliente_p_m_m_e_3=0;
          $razon_social_3='';
          $giro_mercantil_3='';
          $clave_registro_3='';
          $extranjero_3='';
          $pais_3='';
          $nacionalidad_3='';
          $tipo_vialidad_d_3='';
          $calle_d_3='';
          $no_ext_d_3='';
          $no_int_d_3='';
          $colonia_d_3='';
          $municipio_d_3='';
          $localidad_d_3='';
          $estado_d_3='';
          $cp_d_3='';
          $pais_d_3='';
          $telefono_d_3='';
          $correo_d_3='';
          $fecha_constitucion_d_3='';
          $numero_acta_d_3='';
          $nombre_notario_d_3='';
          $numero_identificacion_g_3='';
          $nombre_g_3='';
          $apellido_paterno_g_3='';
          $apellido_materno_g_3='';
          $nombre_identificacion_g_3='';
          $autoridad_emite_g_3='';
          $genero_g_3=0;
          $fecha_nacimiento_g_3='';
          $r_f_c_g_3='';
          $curp_g_3='';
          $arraywhere_3 = array('idperfilamiento'=>$idperfilamiento); 
          $tipo_cliente_p_m_m_e=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_m_m_e',$arraywhere_3);
          foreach ($tipo_cliente_p_m_m_e as $item_3){
            $idtipo_cliente_p_m_m_e_3=$item_3->idtipo_cliente_p_m_m_e;
            $razon_social_3=$item_3->razon_social;
            $giro_mercantil_3=$item_3->giro_mercantil;
            $clave_registro_3=$item_3->clave_registro;
            $extranjero_3=$item_3->extranjero;
            $pais_3=$item_3->pais;
            $nacionalidad_3=$item_3->nacionalidad;
            $tipo_vialidad_d_3=$item_3->tipo_vialidad_d;
            $calle_d_3=$item_3->calle_d;
            $no_ext_d_3=$item_3->no_ext_d;
            $no_int_d_3=$item_3->no_int_d;
            $colonia_d_3=$item_3->colonia_d;
            $municipio_d_3=$item_3->municipio_d;
            $localidad_d_3=$item_3->localidad_d;
            $estado_d_3=$item_3->estado_d;
            $cp_d_3=$item_3->cp_d;
            $pais_d_3=$item_3->pais_d;
            $telefono_d_3=$item_3->telefono_d;
            $correo_d_3=$item_3->correo_d;
            $fecha_constitucion_d_3=$item_3->fecha_constitucion_d;
            $numero_acta_d_3=$item_3->numero_acta_d;
            $nombre_notario_d_3=$item_3->nombre_notario_d;
            $nombre_g_3=$item_3->nombre_g;
            $apellido_paterno_g_3=$item_3->apellido_paterno_g;
            $apellido_materno_g_3=$item_3->apellido_materno_g;
            $nombre_identificacion_g_3=$item_3->nombre_identificacion_g;
            $numero_identificacion_g_3=$item_3->numero_identificacion_g;
            $autoridad_emite_g_3=$item_3->autoridad_emite_g;
            $fecha_nacimiento_g_3=$item_3->fecha_nacimiento_g;
            $genero_g_3=$item_3->genero_g;
            $r_f_c_g_3=$item_3->r_f_c_g;
            $curp_g_3=$item_3->curp_g;
          }
          $checked_t_3='';
          if($extranjero_3=='on'){
             $checked_t_3='checked';
          }
          $clave_t_3='';
          $pais_t_3='';
          $tipo_cliente_p_m_m_e=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_3);
          foreach ($tipo_cliente_p_m_m_e as $item_t_3) {
            $clave_t_3=$item_t_3->clave;
            $pais_t_3=$item_t_3->pais;
          }
          $clave_t2_3='';
          $pais_t2_3='';
          $tipo_cliente_p_m_m_e2=$this->ModeloCatalogos->getselectwhere('pais','clave',$nacionalidad_3);
          foreach ($tipo_cliente_p_m_m_e2 as $item_t2_3) {
            $clave_t2_3=$item_t2_3->clave;
            $pais_t2_3=$item_t2_3->pais;
          }
          $clave_d_t_3='';
          $pais_d_t_3='';
          $tipo_cliente_p_m_m_e_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_d_3);
          foreach ($tipo_cliente_p_m_m_e_2 as $item_td2_3) {
            $clave_d_t_3=$item_td2_3->clave;
            $pais_d_t_3=$item_td2_3->pais;
          }
          $genero_t_3='';
          $genero_t_3_2='';
          if($genero_g_3==1){
            $genero_t_3='checked';
          }else if($genero_g_3==2){
            $generoe_t_3_2='checked';
          }
          $idcomplemento_persona_moral=0;
          $empresa_persona_moral_hay_algun_apoderado_legal=0;
          $empresa_persona_moral_hay_algun_apoderado_legal_accionista=0;
          $empresa_persona_moral_existe_algun_precandidato=0;
          $empresa_persona_moral_existe_algun_responsable_campana_politica=0;
          $respondio_si_pregunta_anterior_persona_moral=0;
          
          
          $complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_moral','idperfilamiento',$idperfilamiento);
          foreach ($complento_pf1 as $cpm1) {
            $idcomplemento_persona_moral=$cpm1->id;
            $empresa_persona_moral_hay_algun_apoderado_legal=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal;
            $empresa_persona_moral_hay_algun_apoderado_legal_accionista=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal_accionista;
            $empresa_persona_moral_existe_algun_precandidato=$cpm1->empresa_persona_moral_existe_algun_precandidato;
            $empresa_persona_moral_existe_algun_responsable_campana_politica=$cpm1->empresa_persona_moral_existe_algun_responsable_campana_politica;
            $respondio_si_pregunta_anterior_persona_moral=$cpm1->respondio_si_pregunta_anterior_persona_moral;
            
          }

          $empresa_persona_moral_hay_algun_apoderado_legal3x1='';
          $empresa_persona_moral_hay_algun_apoderado_legal3x2='';
          if($empresa_persona_moral_hay_algun_apoderado_legal==1){
            $empresa_persona_moral_hay_algun_apoderado_legal3x1='checked';
            $empresa_persona_moral_hay_algun_apoderado_legal3x2='';
          }else if($empresa_persona_moral_hay_algun_apoderado_legal==2){
            $empresa_persona_moral_hay_algun_apoderado_legal3x1='';
            $empresa_persona_moral_hay_algun_apoderado_legal3x2='checked';
          }

          $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='';
          $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2='';
          if($empresa_persona_moral_hay_algun_apoderado_legal_accionista==1){
            $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='checked';
            $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2='';
          }else if($empresa_persona_moral_hay_algun_apoderado_legal_accionista==2){
            $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='';
            $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2='checked';
          }

          $empresa_persona_moral_existe_algun_precandidato3x1='';
          $empresa_persona_moral_existe_algun_precandidato3x2='';
          if($empresa_persona_moral_existe_algun_precandidato==1){
            $empresa_persona_moral_existe_algun_precandidato3x1='checked';
            $empresa_persona_moral_existe_algun_precandidato3x2='';
          }else if($empresa_persona_moral_existe_algun_precandidato==2){
            $empresa_persona_moral_existe_algun_precandidato3x1='';
            $empresa_persona_moral_existe_algun_precandidato3x2='checked';
          }


          $empresa_persona_moral_existe_algun_responsable_campana_politica3x1='';
          $empresa_persona_moral_existe_algun_responsable_campana_politica3x2='';
          if($empresa_persona_moral_existe_algun_responsable_campana_politica==1){
            $empresa_persona_moral_existe_algun_responsable_campana_politica3x1='checked';
            $empresa_persona_moral_existe_algun_responsable_campana_politica3x2='';
          }else if($empresa_persona_moral_existe_algun_responsable_campana_politica==2){
            $empresa_persona_moral_existe_algun_responsable_campana_politica3x1='';
            $empresa_persona_moral_existe_algun_responsable_campana_politica3x2='checked';
          }

          $respondio_si_pregunta_anterior_persona_moral3x1='';
          $respondio_si_pregunta_anterior_persona_moral3x2='';
          if($respondio_si_pregunta_anterior_persona_moral==1){
            $respondio_si_pregunta_anterior_persona_moral3x1='checked';
            $respondio_si_pregunta_anterior_persona_moral3x2='';
          }else if($respondio_si_pregunta_anterior_persona_moral==2){
            $respondio_si_pregunta_anterior_persona_moral3x1='';
            $respondio_si_pregunta_anterior_persona_moral3x2='checked';
          }
 
          $actividad_econominica_morales=$this->ModeloCatalogos->getData('actividad_econominica_morales');
          $html.=' <div class="row firma_tipo_cliente" style="display: none">
                      <div class="col-md-12">    
                        <h3>Formato: Conoce a tu cliente</h3>
                        <h3>Fecha: '.date("m-d-Y").'</h3><br>
                      </div>
                  </div>
                  <hr class="subtitle barra_menu">
                  <div class="row">
                      <div class="col-md-12">    
                        <h3>Persona moral mexicana y extranjera, persona moral de derecho público</h3>
                      </div>
                      <div class="col-md-12">    
                        <h3>'.$paso2.'</h3>
                      </div>
                  </div>
                  ';
            $html.='<form class="form" method="post" role="form" id="form_tipo_cliente_p_m_m_e">
                      <input type="hidden" name="idtipo_cliente_p_m_m_e" id="idtipo_cliente_p_m_m_e" value="'.$idtipo_cliente_p_m_m_e_3.'">
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label>Razón social:</label>
                          <input class="form-control" type="text" name="razon_social" value="'.$razon_social_3.'">
                        </div>
                      </div>  
                      <div class="row">
                        <div class="col-md-7 form-group">
                          <label>Giro mercantil / Actividad / Objeto social:</label>
                          <select class="form-control" name="giro_mercantil">';
                        foreach ($actividad_econominica_morales as $item2) {
                          $selected="";
                          if($giro_mercantil_3==$item2->clave){
                            $selected="selected";
                          }
                          $html.='<option value="'.$item2->clave.'" '.$selected.'>'.$item2->giro_mercantil.'</option>'; 
                        }

                    $html.='</select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          
                        </div>
                      </div><br>
                      <div class="row">
                        <div class="col-md-12 form-group">
                          <label><span>Clave del registro federal de contribuyentes con homoclave / número de identificacion fiscal en otro país (cuando aplique) y país que la asignó.</span></label>
                          <input class="form-control" type="text" name="clave_registro" value="'.$clave_registro_3.'">
                        </div>
                      </div>  
                      <div class="row">  
                        <div class="col-md-2">
                          <div class="form-check form-check-flat form-check-primary">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input" id="extranjero_cliente3" name="extranjero" '.$checked_t_3.' onchange=ocultar_pais_cliente3()>
                              Es extranjero
                            </label>
                          </div>
                        </div>
                        <div class="col-md-4 pais_cliente3">
                          <span class="pais_cliente3" style="display:none">
                            <div class="col-md-12 form-group">
                              <label>País:</label><br>
                              <select class="form-control idpais_t_3" name="pais">
                                <option value="MX">MEXICO</option>';

                            if($pais_3!=''){
                              $html.='<option value="'.$clave_t_3.'" selected>'.$pais_t_3.'</option>';      
                            }
                       $html.='</select>
                            </div>
                          </span> 
                        </div>
                      </div>
                      <div class="row">   
                        <div class="col-md-4 form-group pais_nacion3">
                          <label>Nacionalidad:</label><br>
                          <select class="form-control idpais_t2_3" name="nacionalidad">
                             <option value="MX">MEXICO</option>';
                        if($nacionalidad_3!=''){
                          $html.='<option value="'.$clave_t2_3.'" selected>'.$pais_t2_3.'</option>';      
                        }
                   $html.='</select>
                        </div>  
                      </div>';
                      //$html.="ESTE ES LA CLAVE DE PAIS: ".$pais_3;  
                      $html.='<div class="row">
                        <div class="col-md-12">
                          <hr class="subtitle">
                          <h4>Dirección</h4>
                        </div>
                      </div>
                      <div class="row">  
                        <div class="col-md-4 form-group">
                          <label>País:</label><br>
                          <select class="form-control idpais_d_3" name="pais_d">
                            <option value="MX">MEXICO</option>';
                        if($pais_d_3!=''){
                          $html.='<option value="'.$clave_d_t_3.'" selected>'.$pais_d_t_3.'</option>';      
                        }
                   $html.='</select>
                        </div>
                        <div class="col-md-2 form-group">
                          <label>Tipo de vialidad:</label>
                          <select class="form-control" name="tipo_vialidad_d" id="tipo_vialidad1">
                            <option disabled selected>Selecciona un tipo</option>';
                            foreach ($get_tipo_vialidad as $item){
                              if($item->id==$tipo_vialidad_d_3){
                                $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                              }  
                            }
                $html.='</select>
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Calle:</label>
                          <input class="form-control" type="text" name="calle_d" value="'.$calle_d_3.'">
                        </div>
                        <div class="col-md-2 form-group">
                          <label>No.ext:</label>
                          <input class="form-control" type="text" name="no_ext_d" value="'.$no_ext_d_3.'">
                        </div>
                        <div class="col-md-2 form-group">
                          <label>No.int:</label>
                          <input class="form-control" type="text" name="no_int_d" value="'.$no_int_d_3.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>C.P:</label>
                          <input onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" class="form-control" type="text" name="cp_d" value="'.$cp_d_3.'" id="cp_d3">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Municipio o delegación:</label>
                          <input id="municipio_d3" class="form-control" type="text" name="municipio_d" value="'.$municipio_d_3.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Localidad:</label>
                          <input class="form-control" type="text" name="localidad_d" value="'.$localidad_d_3.'" id="localidad_d">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Estado:</label>
                          <input readonly="" type="hidden" class="estado_text_d_3" value="'.$estado_d_3.'">
                          <span class="span_div_d_3"></span>
                        </div>
                        <!--<div class="col-md-3 form-group">
                          <label>Colonia:</label>
                          <input oninput="autoComplete(this.id);" id="colonia_d3" class="form-control" type="text" name="colonia_d" value="'.$colonia_d_3.'">
                        </div>-->';
                        if($pais_d_3=="MX"){
                          $html.='<div class="col-md-3 form-group" id="cont_colo">
                            <label>Colonia:</label><br>
                            <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d3">
                              <option value=""></option>';
                              if($colonia_d_3!=''){
                                $html.='<option value="'.$colonia_d_3.'" selected>'.$colonia_d_3.'</option>';      
                              }    
                            $html.='</select>
                          </div>';
                        }else if($pais_d_3!="" && $pais_d_3!="MX"){
                          $html.='<div class="col-md-3 form-group" id="cont_colo">
                            <label>Colonia:</label>
                            <input class="form-control" type="text" name="colonia_d" value="'.$colonia_d_3.'">
                          </div>';
                        }if($pais_d_3==""){
                          $html.='<div class="col-md-3 form-group" id="cont_colo">
                            <label>Colonia:</label><br>
                            <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d3">
                              <option value=""></option>';
                              if($colonia_d_3!=''){
                                $html.='<option value="'.$colonia_d_3.'" selected>'.$colonia_d_3.'</option>';      
                              }    
                            $html.='</select>
                          </div>';
                        }
                        $html.='
                       
                      </div>
                      <div class="row">
                        <div class="col-md-3 form-group">
                          <label>Número de teléfono:</label>
                          <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="telefono_d" value="'.$telefono_d_3.'">
                        </div>
                        <div class="col-md-9 form-group">
                          <label>Correo electrónico:</label>
                          <input class="form-control" type="email" name="correo_d" value="'.$correo_d_3.'">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3 form-group">
                          <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                          <input class="form-control" type="date" <input type="date" max="'.date("Y-m-d").'" name="fecha_constitucion_d" value="'.$fecha_constitucion_d_3.'">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Número de acta constitutiva:</label>
                          <input class="form-control" type="number" name="numero_acta_d" value="'.$numero_acta_d_3.'">
                        </div>
                        <div class="col-md-6 form-group">
                          <label>Nombre del notario:</label>
                          <input class="form-control" type="text" name="nombre_notario_d" value="'.$nombre_notario_d_3.'">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <hr class="subtitle">
                          <h5>DATOS GENERALES DEL APODERADO / REPRESENTANTE LEGAL / SERVIDORES PÚBLICOS EN CASO DE PM DE DERECHO PÚBLICO.</h5>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <span>Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4 form-group">
                            <label>Nombre(s):</label>
                            <input class="form-control" type="text" name="nombre_g" id="nombre_g" value="'.$nombre_g_3.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Apellido paterno:</label>
                            <input class="form-control" type="text" name="apellido_paterno_g" id="apellido_paterno_g" value="'.$apellido_paterno_g_3.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Apellido materno:</label>
                            <input class="form-control" type="text" name="apellido_materno_g" id="apellido_materno_g" value="'.$apellido_materno_g_3.'">
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 form-group" id="saltos_impre" style="display: none;">
                          <br><br><br>
                        </div>
                          <div class="col-md-4 form-group">
                            <label>Nombre de identificación:</label>
                            <input class="form-control" type="text" name="nombre_identificacion_g" value="'.$nombre_identificacion_g_3.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Autoridad que emite la identificación:</label>
                            <input class="form-control" type="text" name="autoridad_emite_g" value="'.$autoridad_emite_g_3.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Número de identificación:</label>
                            <input class="form-control" type="text" name="numero_identificacion_g" value="'.$numero_identificacion_g_3.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Fecha de nacimiento:</label>
                            <input class="form-control" type="date" max="'.date("Y-m-d").'" name="fecha_nacimiento_g" value="'.$fecha_nacimiento_g_3.'">
                          </div>
                          <div class="col-md-4 form-group">
                            <label>Género:</label>
                            <div class="row">
                              <div class="col-md-6 form-group">
                                <div class="form-check form-check-success">
                                  <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="genero_g" checked value="1" '.$genero_t_3.'>
                                    Masculino
                                  </label>
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <div class="form-check form-check-success">
                                  <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="genero_g" value="2" '.$genero_t_3_2.'>
                                    Femenino
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3 form-group">
                          <label>R.F.C:</label>
                          <input class="form-control rfcPerf" type="text" name="r_f_c_g" value="'.$r_f_c_g_3.'"><p>*Cuando cuente con R.F.C</p>
                        </div>
                        <div class="col-md-1 form-group">
                          <br>
                          <a href="#modal_rfc" data-toggle="modal">
                          <button type="button" class="btn btn_ayuda">
                            <i class="mdi mdi-help"></i>
                          </button></a>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>CURP:</label>
                          <input class="form-control" type="text" name="curp_g" value="'.$curp_g_3.'">
                          <p>*Cuando cuente con CURP</p>
                        </div>
                      </div>
                    </form>  
                    <hr class="subtitle barra_menu">


                  <form method="post" role="form" id="form_complemento_persona_moral">
                    <input type="hidden" name="id" value="'.$idcomplemento_persona_moral.'">
                    <div class="row" id="pregm1">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral hay algún apoderado legal, representante legal y/o accionista que se Persona Políticamente Expuesta? <button type="button" class="btn btn_ayuda" onclick="que_es_pep()">
                            <i class="mdi mdi-help"></i>
                          </button>
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal" value="1" '.$empresa_persona_moral_hay_algun_apoderado_legal3x1.' onclick="verPreguntasMoral()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal" value="2" '.$empresa_persona_moral_hay_algun_apoderado_legal3x2.' onclick="verPreguntasMoral()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="pregm2">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral hay algún apoderado legal, representante legal y/o accionista que tenga o que haya tenido un puesto político o público de alto nivel o está relacionada con alguna persona con estas características?
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal_accionista" value="1" '.$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1.' onclick="verPreguntasMoral()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal_accionista" value="2" '.$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2.' onclick="verPreguntasMoral()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row" id="pregm3">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral existe algún precandidato(a), candidato(a), dirigente partidista, militante, dirigente sindical y/o servidor(a) público(a)?
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_precandidato" value="1" '.$empresa_persona_moral_existe_algun_precandidato3x1.' onclick="verPreguntasMoral()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_precandidato" value="2" '.$empresa_persona_moral_existe_algun_precandidato3x2.' onclick="verPreguntasMoral()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row" id="pregm4">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral existe algún responsable de campaña política, encargado financiero y/ contable de algún partido político o de algún órgano interno de los partidos políticos y/o candidatos comunes y/o frentes y/o candidatos independientes?
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_responsable_campana_politica" value="1" '.$empresa_persona_moral_existe_algun_responsable_campana_politica3x1.' onclick="verPreguntasMoral()">
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_responsable_campana_politica" value="2" '.$empresa_persona_moral_existe_algun_responsable_campana_politica3x2.' onclick="verPreguntasMoral()">
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>  
                  '; 
          $html.='<hr class="subtitle barra_menu">
                <div class="row barra_menu"> 
                    <div class="col-md-12" align="right">';
                    if($idtipo_cliente_p_m_m_e_3>0){
                      $html.='<button type="button" class="btn gradient_nepal2" onclick="imprimir_formato1(3,'.$idperfilamiento.')"><i class="fa fa-print"></i> Imprimir formato conoce a tu cliente</button>';
                    }
                    $html.='<button type="button" class="btn gradient_nepal2" onclick="regresa()"><i class="fa fa-reply" aria-hidden="true"></i> Regresar</button>
                      <button type="button" class="btn gradient_nepal2" onclick="guardar_tipo_cliente_3()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>';  
      }else if($tipo==4){
        $idtipo_cliente_p_m_m_d_4=0;
        $nombre_persona_4='';
        $r_f_c_4='';
        $giro_mercantil_4='';
        $nacionalidad_4='';
        $tipo_vialidad_d_4='';
        $calle_d_4='';
        $no_ext_d_4='';
        $no_int_d_4='';
        $colonia_d_4='';
        $municipio_d_4='';
        $localidad_d_4='';
        $estado_d_4='';
        $cp_d_4='';
        $fecha_constitucion_d_4='';
        $nombre_g_4='';
        $apellido_paterno_g_4='';
        $apellido_materno_g_4='';
        $genero_g_4=0;
        $fecha_nacimiento_4='';
        $rfc_g4='';
        $curp_g4='';
        $arraywhere_4 = array('idperfilamiento'=>$idperfilamiento); 
        $tipo_cliente_p_m_m_d=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_m_m_d',$arraywhere_4);
        foreach ($tipo_cliente_p_m_m_d as $item_4){
          $idtipo_cliente_p_m_m_d_4=$item_4->idtipo_cliente_p_m_m_d;
          $nombre_persona_4=$item_4->nombre_persona;
          $r_f_c_4=$item_4->r_f_c;
          $giro_mercantil_4=$item_4->giro_mercantil;
          $nacionalidad_4=$item_4->nacionalidad;
          $tipo_vialidad_d_4=$item_4->tipo_vialidad_d;
          $calle_d_4=$item_4->calle_d;
          $no_ext_d_4=$item_4->no_ext_d;
          $no_int_d_4=$item_4->no_int_d;
          $colonia_d_4=$item_4->colonia_d;
          $municipio_d_4=$item_4->municipio_d;
          $localidad_d_4=$item_4->localidad_d;
          $estado_d_4=$item_4->estado_d;
          $cp_d_4=$item_4->cp_d;
          $fecha_constitucion_d_4=$item_4->fecha_constitucion_d;
          $nombre_g_4=$item_4->nombre_g;
          $apellido_paterno_g_4=$item_4->apellido_paterno_g;
          $apellido_materno_g_4=$item_4->apellido_materno_g;
          $genero_g_4=$item_4->genero_g;
          $fecha_nacimiento_4=$item_4->fecha_nacimiento;
          $rfc_g4=$item_4->rfc_g;
          $curp_g4=$item_4->curp_g;
        }
        $tipo_cliente_p_m_m_d_p=$this->ModeloCatalogos->getselectwhere('pais','clave',$nacionalidad_4);
          foreach ($tipo_cliente_p_m_m_d_p as $item_t_4) {
            $clave_t_4=$item_t_4->clave;
            $pais_t_4=$item_t_4->pais;
          }
        $actividad_econominica_morales=$this->ModeloCatalogos->getData('actividad_econominica_morales');
        $genero_t_4='';
        $genero_t_4_2='';
        if($genero_g_4==1){
          $genero_t_4='checked';
        }else if($genero_g_4==2){
          $genero_t_4_2='checked';
        }
        

        $idcomplemento_persona_moral=0;
        $empresa_persona_moral_hay_algun_apoderado_legal=0;
        $empresa_persona_moral_hay_algun_apoderado_legal_accionista=0;
        $empresa_persona_moral_existe_algun_precandidato=0;
        $empresa_persona_moral_existe_algun_responsable_campana_politica=0;
        $respondio_si_pregunta_anterior_persona_moral=0;
        
        
        $complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_moral','idperfilamiento',$idperfilamiento);
        foreach ($complento_pf1 as $cpm1) {
          $idcomplemento_persona_moral=$cpm1->id;
          $empresa_persona_moral_hay_algun_apoderado_legal=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal;
          $empresa_persona_moral_hay_algun_apoderado_legal_accionista=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal_accionista;
          $empresa_persona_moral_existe_algun_precandidato=$cpm1->empresa_persona_moral_existe_algun_precandidato;
          $empresa_persona_moral_existe_algun_responsable_campana_politica=$cpm1->empresa_persona_moral_existe_algun_responsable_campana_politica;
          $respondio_si_pregunta_anterior_persona_moral=$cpm1->respondio_si_pregunta_anterior_persona_moral;
          
        }

        $empresa_persona_moral_hay_algun_apoderado_legal3x1='';
        $empresa_persona_moral_hay_algun_apoderado_legal3x2='';
        if($empresa_persona_moral_hay_algun_apoderado_legal==1){
          $empresa_persona_moral_hay_algun_apoderado_legal3x1='checked';
          $empresa_persona_moral_hay_algun_apoderado_legal3x2='';
        }else if($empresa_persona_moral_hay_algun_apoderado_legal==2){
          $empresa_persona_moral_hay_algun_apoderado_legal3x1='';
          $empresa_persona_moral_hay_algun_apoderado_legal3x2='checked';
        }

        $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='';
        $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2='';
        if($empresa_persona_moral_hay_algun_apoderado_legal_accionista==1){
          $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='checked';
          $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2='';
        }else if($empresa_persona_moral_hay_algun_apoderado_legal_accionista==2){
          $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='';
          $empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2='checked';
        }

        $empresa_persona_moral_existe_algun_precandidato3x1='';
        $empresa_persona_moral_existe_algun_precandidato3x2='';
        if($empresa_persona_moral_existe_algun_precandidato==1){
          $empresa_persona_moral_existe_algun_precandidato3x1='checked';
          $empresa_persona_moral_existe_algun_precandidato3x2='';
        }else if($empresa_persona_moral_existe_algun_precandidato==2){
          $empresa_persona_moral_existe_algun_precandidato3x1='';
          $empresa_persona_moral_existe_algun_precandidato3x2='checked';
        }


        $empresa_persona_moral_existe_algun_responsable_campana_politica3x1='';
        $empresa_persona_moral_existe_algun_responsable_campana_politica3x2='';
        if($empresa_persona_moral_existe_algun_responsable_campana_politica==1){
          $empresa_persona_moral_existe_algun_responsable_campana_politica3x1='checked';
          $empresa_persona_moral_existe_algun_responsable_campana_politica3x2='';
        }else if($empresa_persona_moral_existe_algun_responsable_campana_politica==2){
          $empresa_persona_moral_existe_algun_responsable_campana_politica3x1='';
          $empresa_persona_moral_existe_algun_responsable_campana_politica3x2='checked';
        }

        $respondio_si_pregunta_anterior_persona_moral3x1='';
        $respondio_si_pregunta_anterior_persona_moral3x2='';
        if($respondio_si_pregunta_anterior_persona_moral==1){
          $respondio_si_pregunta_anterior_persona_moral3x1='checked';
          $respondio_si_pregunta_anterior_persona_moral3x2='';
        }else if($respondio_si_pregunta_anterior_persona_moral==2){
          $respondio_si_pregunta_anterior_persona_moral3x1='';
          $respondio_si_pregunta_anterior_persona_moral3x2='checked';
        }

          $html.='<div class="row firma_tipo_cliente" style="display: none">
                      <div class="col-md-12">    
                        <h3>Formato: Conoce a tu cliente</h3>
                        <h3>Fecha: '.date("m-d-Y").'</h3><br>
                      </div>
                  </div> 
                  <hr class="subtitle barra_menu">
                  <div class="row">
                      <div class="col-md-12">    
                        <h3>Persona moral mexicana de derecho público detallados en el anexo 7 bis A <button type="button" class="btn btn_ayuda barra_menu" onclick="paso2_hoja4_btn_texto_ayuda()">
                          <i class="mdi mdi-help"></i>
                        </button></h3>
                      </div>
                      <div class="col-md-12">    
                        <h3>'.$paso2.'</h3>
                      </div>
                  </div>
                  <br>
                  ';
          $html.='<form class="form" method="post" role="form" id="form_tipo_cliente_p_m_m_d">
                    <input type="hidden" name="idtipo_cliente_p_m_m_d" value="'.$idtipo_cliente_p_m_m_d_4.'">
                    <div class="row">
                      <div class="col-md-8 form-group">
                        <label>Nombre de la persona moral de derecho público:</label>
                        <input class="form-control" type="text" name="nombre_persona" value="'.$nombre_persona_4.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>R.F.C:</label>
                        <input class="form-control rfcPerf" type="text" name="r_f_c" value="'.$r_f_c_4.'">
                      </div>
                      <div class="col-md-1 form-group">
                        <br>
                        <a href="#modal_rfc" data-toggle="modal">
                        <button type="button" class="btn btn_ayuda">
                          <i class="mdi mdi-help"></i>
                        </button></a>
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-4 form-group">
                          <label>Nacionalidad:</label><br>
                          <select class="form-control idpais_t2_4" name="nacionalidad">
                             <option value="MX">MEXICO</option>';
                        if($nacionalidad_4!=''){
                          $html.='<option value="'.$clave_t_4.'" selected>'.$pais_t_4.'</option>';      
                        }
                   $html.='</select>
                      </div>  
                      <div class="col-md-7 form-group">
                          <label>Giro mercantil / Actividad / Objeto social:</label>
                          <select class="form-control" name="giro_mercantil">';
                        /*foreach ($actividad_econominica_morales as $item) {
                          $select="";
                          if($item->clave=="9100025")
                            $select = "selected";
                          $html.='<option value="'.$item->clave.'" '.$select.'>'.$item->giro_mercantil.'</option>';      
                          
                        }*/
                        foreach ($actividad_econominica_morales as $item) {
                          $selected="";
                          if($giro_mercantil_4==$item->clave){
                            $selected="selected";
                          }
                          $html.='<option value="'.$item->clave.'" '.$selected.'>'.$item->giro_mercantil.'</option>';  
                        } 
                    $html.='</select>
                        </div>  
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>Dirección</h4>
                      </div>
                    </div>
                    <div class="row">  
                      <div class="col-md-2 form-group">
                        <label>Tipo de vialidad:</label>
                        <select class="form-control" name="tipo_vialidad_d" id="tipo_vialidad1">
                            <option disabled selected>Selecciona un tipo</option>';
                            foreach ($get_tipo_vialidad as $item){
                              if($item->id==$tipo_vialidad_d_4){
                                $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                              }  
                            }
                $html.='</select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Calle:</label>
                        <input class="form-control" type="text" name="calle_d" value="'.$calle_d_4.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.ext:</label>
                        <input class="form-control" type="text" name="no_ext_d" value="'.$no_ext_d_4.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.int:</label>
                        <input class="form-control" type="text" name="no_int_d" value="'.$no_int_d_4.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>C.P:</label>
                        <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="cp_d" value="'.$cp_d_4.'" id="cp_d4">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>Municipio o delegación:</label>
                        <input class="form-control" id="municipio_d4" type="text" name="municipio_d" value="'.$municipio_d_4.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>Localidad:</label>
                        <input class="form-control" type="text" name="localidad_d" value="'.$localidad_d_4.'" id="localidad_d">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>Estado:</label>
                        <input readonly="" type="hidden" class="estado_text_d_4" value="'.$estado_d_4.'">
                        <select class="form-control" name="estado_d" id="estado_d4">
                          <option disabled >Selecciona un estado</option>';
                          foreach ($estados as $item){
                            if($item->id==$estado_d_4){
                              $html.='<option value="'.$item->id.'" selected>'.$item->estado.'</option>';
                            }else{
                              $html.='<option value="'.$item->id.'" >'.$item->estado.'</option>';
                            }  
                          }
                  $html.='</select>
                      </div>
                      <!--<div class="col-md-3 form-group">
                        <label>Colonia:</label>
                        <input class="form-control" type="text" name="colonia_d" value="'.$colonia_d_4.'">
                      </div>-->
                      <div class="col-md-3 form-group">
                        <label>Colonia:</label><br>
                        <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d4">
                          <option value=""></option>';
                        if($colonia_d_4!=''){
                          $html.='<option value="'.$colonia_d_4.'" selected>'.$colonia_d_4.'</option>';      
                                }  
                        $html.='</select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                        <input class="form-control" type="date" max="'.date("Y-m-d").'" name="fecha_constitucion_d" value="'.$fecha_constitucion_d_4.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h5>DATOS GENERALES DE LOS SERVIDORES PÚBLICOS QUE REALICEN EL ACTO O LA OPERACIÓN</h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
                      </div>
                    </div>
                    <!-- -->
                    <br>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s):</label>
                        <input class="form-control" type="text"  name="nombre_g" id="nombre_g" value="'.$nombre_g_4.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido paterno:</label>
                        <input class="form-control" type="text" name="apellido_paterno_g" id="apellido_paterno_g" value="'.$apellido_paterno_g_4.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido materno:</label>
                        <input class="form-control" type="text" name="apellido_materno_g" id="apellido_materno_g" value="'.$apellido_materno_g_4.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Género:</label>
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="genero_g" checked value="1" '.$genero_t_4.'>
                                Masculino
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="genero_g" value="2" '.$genero_t_4_2.'>
                                Femenino
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                        <input class="form-control" type="date" max="'.date("Y-m-d").'" name="fecha_nacimiento" value="'.$fecha_nacimiento_4.'">
                      </div>  
                    </div>
                  </form>
                  <!--<hr class="subtitle barra_menu">-->
                  <form method="post" role="form" id="form_complemento_persona_moral">
                    <input type="hidden" name="id" value="'.$idcomplemento_persona_moral.'">
                    <!--<div class="row">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral hay algún apoderado legal, representante legal y/o accionista que se Persona Políticamente Expuesta?
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal" value="1" '.$empresa_persona_moral_hay_algun_apoderado_legal3x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal" value="2" '.$empresa_persona_moral_hay_algun_apoderado_legal3x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral hay algún apoderado legal, representante legal y/o accionista que tenga o que haya tenido un puesto político o público de alto nivel o está relacionada con alguna persona con estas características?
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal_accionista" value="1" '.$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_hay_algun_apoderado_legal_accionista" value="2" '.$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral existe algún precandidato(a), candidato(a), dirigente partidista, militante, dirigente sindical y/o servidor(a) público(a)?
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_precandidato" value="1" '.$empresa_persona_moral_existe_algun_precandidato3x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_precandidato" value="2" '.$empresa_persona_moral_existe_algun_precandidato3x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-10">
                        <label>¿En la empresa/persona moral existe algún responsable de campaña política, encargado financiero y/ contable de algún partido político o de algún órgano interno de los partidos políticos y/o candidatos comunes y/o frentes y/o candidatos independientes?
                        </label>
                      </div>
                      <div class="col-md-2 form-group">
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_responsable_campana_politica" value="1" '.$empresa_persona_moral_existe_algun_responsable_campana_politica3x1.'>
                                Si
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="empresa_persona_moral_existe_algun_responsable_campana_politica" value="2" '.$empresa_persona_moral_existe_algun_responsable_campana_politica3x2.'>
                                No
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    -->
                  </form> 
                 ';
          $html.='<hr class="subtitle barra_menu">
                <div class="row barra_menu"> 
                    <div class="col-md-12" align="right">';
                    if($idtipo_cliente_p_m_m_d_4>0){
                      $html.='<button type="button" class="btn gradient_nepal2" onclick="imprimir_formato1(4,'.$idperfilamiento.')"><i class="fa fa-print"></i> Imprimir formato conoce a tu cliente</button>';
                    }
                    $html.='<button type="button" class="btn gradient_nepal2" onclick="regresa()"><i class="fa fa-reply" aria-hidden="true"></i> Regresar</button>
                      <button type="button" class="btn gradient_nepal2" onclick="guardar_tipo_cliente_4()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>';
      }else if($tipo==5){
        $idtipo_cliente_e_c_o_i_5=0;
        $denominacion_5='';
        $fecha_establecimiento_5='';
        $nacionalidad_5='';
        $clave_registro_5='';
        $pais_5='';
        $giro_mercantil_5='';
        $tipo_vialidad_d_5='';
        $calle_d_5='';
        $no_ext_d_5='';
        $no_int_d_5='';
        $colonia_d_5='';
        $municipio_d_5='';
        $localidad_d_5='';
        $estado_d_5='';
        $cp_d_5='';
        $pais_d_5='';
        $telefono_d_5='';
        $correo_d_5='';
        $certificado_matricula_5='';
        $nombre_g_5='';
        $apellido_paterno_g_5='';
        $apellido_materno_g_5='';
        $genero_g_5='';
        $fecha_nacimiento_g_5='';
        $nombre_identificacion_g_5='';
        $autoridad_emite_g_5='';
        $numero_identificacion_g_5='';
        $r_f_c_g_5='';
        $curp_g_5='';
        $arraywhere_5=array('idperfilamiento'=>$idperfilamiento); 
        $tipo_cliente_e_c_o_i=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_e_c_o_i',$arraywhere_5);
        foreach ($tipo_cliente_e_c_o_i as $item_5){
          $idtipo_cliente_e_c_o_i_5=$item_5->idtipo_cliente_e_c_o_i;
          $denominacion_5=$item_5->denominacion;
          $fecha_establecimiento_5=$item_5->fecha_establecimiento;
          $nacionalidad_5=$item_5->nacionalidad;
          $clave_registro_5=$item_5->clave_registro;
          $pais_5=$item_5->pais;
          $giro_mercantil_5=$item_5->giro_mercantil;
          $tipo_vialidad_d_5=$item_5->tipo_vialidad_d;
          $calle_d_5=$item_5->calle_d;
          $no_ext_d_5=$item_5->no_ext_d;
          $no_int_d_5=$item_5->no_int_d;
          $colonia_d_5=$item_5->colonia_d;
          $municipio_d_5=$item_5->municipio_d;
          $localidad_d_5=$item_5->localidad_d;
          $estado_d_5=$item_5->estado_d;
          $cp_d_5=$item_5->cp_d;
          $pais_d_5=$item_5->pais_d;
          $telefono_d_5=$item_5->telefono_d;
          $correo_d_5=$item_5->correo_d;
          $certificado_matricula_5=$item_5->certificado_matricula;
          $nombre_g_5=$item_5->nombre_g;
          $apellido_paterno_g_5=$item_5->apellido_paterno_g;
          $apellido_materno_g_5=$item_5->apellido_materno_g;
          $genero_g_5=$item_5->genero_g;
          $fecha_nacimiento_g_5=$item_5->fecha_nacimiento_g;
          $nombre_identificacion_g_5=$item_5->nombre_identificacion_g;
          $autoridad_emite_g_5=$item_5->autoridad_emite_g;
          $numero_identificacion_g_5=$item_5->numero_identificacion_g;
          $r_f_c_g_5=$item_5->r_f_c_g;
          $curp_g_5=$item_5->curp_g;
        }
        $clave_t_5='';
        $pais_t_5='';
        $tipo_cliente_e_c_o_i_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_5);
        foreach ($tipo_cliente_e_c_o_i_1 as $item_t_5) {
          $clave_t_5=$item_t_5->clave;
          $pais_t_5=$item_t_5->pais;
        }
        $clave_t5_5='';
        $pais_t5_5='';
        $tipo_cliente_e_c_o_i_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_d_5);
        foreach ($tipo_cliente_e_c_o_i_2 as $item_t5_5) {
          $clave_t5_5=$item_t5_5->clave;
          $pais_t5_5=$item_t5_5->pais;
        }
        $tipo_cliente_e_c_o_i_na=$this->ModeloCatalogos->getselectwhere('pais','clave',$nacionalidad_5);
          foreach ($tipo_cliente_e_c_o_i_na as $item_t_5) {
            $clave_t_5n=$item_t_5->clave;
            $pais_t_5n=$item_t_5->pais;
          }
        $actividad_econominica_morales=$this->ModeloCatalogos->getData('actividad_econominica_morales');  
        $genero_t_5='';
        $genero_t_5_2='';
        if($genero_g_5==1){
          $genero_t_5='checked';
        }else if($genero_g_5==2){
          $genero_t_5_2='checked';
        }
          $html.='<div class="row firma_tipo_cliente" style="display: none">
                      <div class="col-md-12">    
                        <h3>Formato: Conoce a tu cliente</h3>
                        <h3>Fecha: '.date("m-d-Y").'</h3><br>
                      </div>
                  </div>
                  <hr class="subtitle barra_menu">
                  <div class="row">
                      <div class="col-md-12">    
                        <h3>Embajadas, consulados u organismos internacionales</h3>
                      </div>
                      <div class="col-md-12">    
                        <h3>'.$paso2.'</h3>
                      </div>
                  </div>
                  <br>
                  ';
          $html.='<form class="form" method="post" role="form" id="form_tipo_cliente_e_c_o_i">
                    <input type="hidden" name="idtipo_cliente_e_c_o_i" value="'.$idtipo_cliente_e_c_o_i_5.'">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Denominación:</label>
                        <input class="form-control" type="text" name="denominacion" value="'.$denominacion_5.'">
                      </div>
                    </div>
                    <div class="row">  
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de establecimiento en Territorio Nacional:</label>
                        <input class="form-control" type="date" max="'.date("Y-m-d").'" name="fecha_establecimiento" value="'.$fecha_establecimiento_5.'">
                      </div>
                      <div class="col-md-4 form-group">
                          <label>Nacionalidad:</label><br>
                          <select class="form-control idpais_t2_5" name="nacionalidad">
                             <option value="MX">MEXICO</option>';
                        if($nacionalidad_5!=''){
                          $html.='<option value="'.$clave_t_5n.'" selected>'.$pais_t_5n.'</option>';      
                        }
                   $html.='</select>
                      </div>   
                    </div>  
                    <div class="row">  
                      <div class="col-md-7 form-group">
                        <label><span>Clave del Registro Federal de Contribuyentes con homoclave / número de identificación fiscal en otro país (cuando aplique)</span>:</label>
                        <input class="form-control" type="text" name="clave_registro" value="'.$clave_registro_5.'">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>País que asigno la clave  RFC o NIF:</label><br>
                        <select class="form-control idpais_d_5" name="pais">
                          <option value="MX">MEXICO</option>';
                        if($pais_5!=''){
                          $html.='<option value="'.$clave_t_5.'" selected>'.$pais_t_5.'</option>';      
                        }
                   $html.='</select>
                      </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-md-7 form-group">
                          <label>Giro mercantil / Actividad / Objeto social:</label>
                          <select class="form-control" name="giro_mercantil">-->';
                        /*foreach ($actividad_econominica_morales as $item) {
                          $html.='<option value="'.$item->clave.'">'.$item->giro_mercantil.'<option>';  
                          if($giro_mercantil_5!=''){
                          $html.='<option value="'.$item->clave.'" selected>'.$item->giro_mercantil.'</option>';      
                          }
                        } */
                        /*foreach ($actividad_econominica_morales as $item) {
                          $selected="";
                          if($giro_mercantil_5==$item->clave){
                            $selected="selected";
                          }
                          $html.='<option value="'.$item->clave.'" '.$selected.'>'.$item->giro_mercantil.'</option>';  
                        } */
                    $html.='<!--</select>
                        </div>
                      </div>-->
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>Dirección</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                          <label>País:</label><br>
                          <select class="form-control idpais_d5_5" name="pais_d">
                            <option value="MX">MEXICO</option>';
                          if($pais_d_5!=''){
                            $html.='<option value="'.$clave_t5_5.'" selected>'.$pais_t5_5.'</option>';      
                          }
                     $html.='</select>
                        </div>
                      <div class="col-md-2 form-group">
                        <label>Tipo de vialidad:</label>
                        <select class="form-control" name="tipo_vialidad_d" id="tipo_vialidad1">
                            <option disabled selected>Selecciona un tipo</option>';
                            foreach ($get_tipo_vialidad as $item){
                              if($item->id==$tipo_vialidad_d_5){
                                $html.='<option value="'.$item->id.'" selected>'.$item->nombre.'</option>';
                              }else{
                                $html.='<option value="'.$item->id.'" >'.$item->nombre.'</option>';
                              }  
                            }
                $html.='</select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Calle:</label>
                        <input class="form-control" type="text" name="calle_d" value="'.$calle_d_5.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.ext:</label>
                        <input class="form-control" type="text" name="no_ext_d" value="'.$no_ext_d_5.'">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.int:</label>
                        <input class="form-control" type="text" name="no_int_d" value="'.$no_int_d_5.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>C.P:</label>
                        <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="cp_d" value="'.$cp_d_5.'" id="cp_d5">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Municipio o delegación:</label>
                        <input class="form-control" id="municipio_d5" type="text" name="municipio_d" value="'.$municipio_d_5.'">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Localidad:</label>
                        <input class="form-control" type="text" name="localidad_d" value="'.$localidad_d_5.'" id="localidad_d">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Estado:</label>
                        <input readonly="" type="hidden" class="estado_text_d5_5" value="'.$estado_d_5.'">
                        <span class="span_div_d5_5"></span>
                      </div>
                      <!--<div class="col-md-3 form-group">
                        <label>Colonia:</label>
                        <input class="form-control" type="text" name="colonia_d" value="'.$colonia_d_5.'">
                      </div>-->';
                      if($pais_d_5=="MX"){
                          $html.='<div class="col-md-3 form-group" id="cont_colo">
                            <label>Colonia:</label><br>
                            <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d5">
                              <option value=""></option>';
                              if($colonia_d_5!=''){
                                $html.='<option value="'.$colonia_d_5.'" selected>'.$colonia_d_5.'</option>';      
                              }    
                            $html.='</select>
                          </div>';
                        }else if($pais_d_5!="MX" && $pais_d_5!=""){
                          $html.='<div class="col-md-3 form-group" id="cont_colo">
                            <label>Colonia:</label>
                            <input class="form-control" type="text" name="colonia_d" value="'.$colonia_d_5.'">
                          </div>';
                        }if($pais_d_5==""){
                          $html.='<div class="col-md-3 form-group" id="cont_colo">
                            <label>Colonia:</label><br>
                            <select onclick="autoComplete(this.id)" class="form-control" name="colonia_d" id="colonia_d5">
                              <option value=""></option>';
                              if($colonia_d_5!=''){
                                $html.='<option value="'.$colonia_d_5.'" selected>'.$colonia_d_5.'</option>';      
                              }    
                            $html.='</select>
                          </div>';
                        }
                      $html.='
                    </div>
                    <div class="row">  
                      <div class="col-md-3 form-group">
                        <label>Número de teléfono:</label>
                        <input class="form-control" placeholder="(Local) + Teléfono" type="text" name="telefono_d" value="'.$telefono_d_5.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Correo electrónico:</label>
                        <input class="form-control" type="email" name="correo_d" value="'.$correo_d_5.'">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Certificado de Matrícula Consular:</label>
                        <input class="form-control" type="text" name="certificado_matricula" value="'.$certificado_matricula_5.'">
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h5>DATOS GENERALES DEL APODERADO / REPRESENTANTE LEGAL / SERVIDORES PÚBLICOS EN CASO DE PM DE DERECHO PÚBLICO</h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que correspondan y nombre(s).</span>
                      </div>
                    </div>
                    <!-- -->
                    <br>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s):</label>
                        <input class="form-control" type="text" name="nombre_g" id="nombre_g" value="'.$nombre_g_5.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido paterno:</label>
                        <input class="form-control" type="text" name="apellido_paterno_g" id="apellido_paterno_g" value="'.$apellido_paterno_g_5.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido materno:</label>
                        <input class="form-control" type="text" name="apellido_materno_g" id="apellido_materno_g" value="'.$apellido_materno_g_5.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Género:</label>
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="genero_g" checked value="1" '.$genero_t_5.'>
                                Masculino
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="genero_g" value="2" '.$genero_t_5_2.'>
                                Femenino
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                        <input class="form-control" type="date" max="'.date("Y-m-d").'" name="fecha_nacimiento_g" value="'.$fecha_nacimiento_g_5.'">
                      </div>  
                    </div>
                    <div class="row" id="espa_repre5" style="display:none; page-break-before: always">
                      <div class="col-md-12"><br><br><br>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre de identificación:</label>
                        <input class="form-control" type="text" name="nombre_identificacion_g" value="'.$nombre_identificacion_g_5.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Autoridad que emite la identificación:</label>
                        <input class="form-control" type="text" name="autoridad_emite_g" value="'.$autoridad_emite_g_5.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número de identificación:</label>
                        <input class="form-control" type="text" name="numero_identificacion_g" value="'.$numero_identificacion_g_5.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2 form-group"></div>
                      <div class="col-md-4 form-group">
                        <label>R.F.C:</label>
                        <input class="form-control rfcPerf" type="text" name="r_f_c_g" value="'.$r_f_c_g_5.'"><p>*Cuando cuente con R.F.C</p>
                      </div>
                      <div class="col-md-1 form-group">
                          <br>
                          <a href="#modal_rfc" data-toggle="modal">
                          <button type="button" class="btn btn_ayuda">
                            <i class="mdi mdi-help"></i>
                          </button></a>
                        </div>
                      <div class="col-md-4 form-group">
                        <label>CURP:</label>
                        <input class="form-control" type="text" name="curp_g" value="'.$curp_g_5.'">
                        <p>*Cuando cuente con CURP</p>
                      </div>
                      <div class="col-md-2 form-group"></div>
                    </div>
                  </form> 
                 ';
          $html.='<hr class="subtitle barra_menu">
                <div class="row barra_menu"> 
                    <div class="col-md-12" align="right">';
                    if($idtipo_cliente_e_c_o_i_5>0){
                      $html.='<button type="button" class="btn gradient_nepal2" onclick="imprimir_formato1(5,'.$idperfilamiento.')"><i class="fa fa-print"></i> Imprimir formato conoce a tu cliente</button>';
                    }
                      $html.='<button type="button" class="btn gradient_nepal2" onclick="regresa()"><i class="fa fa-reply" aria-hidden="true"></i> Regresar</button>
                      <button type="button" class="btn gradient_nepal2" onclick="guardar_tipo_cliente_5()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>';
      }else if($tipo==6){
          $idtipo_cliente_f_6='';
          $denominacion_6='';
          $numero_referencia_6='';
          $clave_registro_6='';
          $fecha_nacimiento_g_6='';
          $nombre_g_6='';
          $apellido_paterno_g_6='';
          $apellido_materno_g_6='';
          $nombre_identificacion_g_6='';
          $autoridad_emite_g_6='';
          $numero_identificacion_g_6='';
          $genero_g_6='';
          $r_f_c_g_6='';
          $curp_g_6='';
          $arraywhere_6=array('idperfilamiento'=>$idperfilamiento); 
          $tipo_cliente_f=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_f',$arraywhere_6);
          foreach ($tipo_cliente_f as $item_6){
            $idtipo_cliente_f_6=$item_6->idtipo_cliente_f;
            $denominacion_6=$item_6->denominacion;
            $numero_referencia_6=$item_6->numero_referencia;
            $clave_registro_6=$item_6->clave_registro;
            $nombre_g_6=$item_6->nombre_g;
            $apellido_paterno_g_6=$item_6->apellido_paterno_g;
            $apellido_materno_g_6=$item_6->apellido_materno_g;
            $nombre_identificacion_g_6=$item_6->nombre_identificacion_g;
            $autoridad_emite_g_6=$item_6->autoridad_emite_g;
            $numero_identificacion_g_6=$item_6->numero_identificacion_g;
            $genero_g_6=$item_6->genero_g;
            $r_f_c_g_6=$item_6->r_f_c_g;
            $curp_g_6=$item_6->curp_g;
            $fecha_nacimiento_g_6=$item_6->fecha_nacimiento_g;
          }
          $genero_t_6='';
          $genero_t_6_2='';
          if($genero_g_6==1){
            $genero_t_6='checked';
          }else if($genero_g_6==2){
            $genero_t_6_2='checked';
          }
          $html.='<div class="row firma_tipo_cliente" style="display: none">
                      <div class="col-md-12">    
                        <h3>Formato: Conoce a tu cliente</h3>
                        <h3>Fecha: '.date("m-d-Y").'</h3><br>
                      </div>
                  </div> 
                  <hr class="subtitle barra_menu">
                  <div class="row">
                      <div class="col-md-12">    
                        <h3>Fideicomisos</h3>
                      </div>
                      <div class="col-md-12">    
                        <h3>'.$paso2.'</h3>
                      </div>
                  </div>
                  <br>
                  ';
          $html.='<form class="form" method="post" role="form" id="form_tipo_cliente_f">
                    <input type="hidden" name="idtipo_cliente_f" value="'.$idtipo_cliente_f_6.'">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Denominación o razón social del fiduciario:</label>
                        <input class="form-control" type="text" name="denominacion" value="'.$denominacion_6.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Número / Referencia o identicador del fideicomiso:</label>
                        <input class="form-control" type="text" name="numero_referencia" value="'.$numero_referencia_6.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-11 form-group">
                        <label>R.F.C. del fideicomiso:</label>
                        <input class="form-control" type="text" name="clave_registro" value="'.$clave_registro_6.'">
                      </div>
                      <div class="col-md-1 form-group">
                        <br>
                        <a href="#modal_rfc" data-toggle="modal">
                        <button type="button" class="btn btn_ayuda">
                          <i class="mdi mdi-help"></i>
                        </button></a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h5>DATOS GENERALES DEL APODERADO LEGAL O DELEGADO FIDUCIARIO</h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que correspondan y nombre(s).</span>
                      </div>
                    </div>
                    <!-- -->
                    <br>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s):</label>
                        <input class="form-control" type="text" name="nombre_g" id="nombre_g" value="'.$nombre_g_6.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido paterno:</label>
                        <input class="form-control" type="text" name="apellido_paterno_g" id="apellido_paterno_g" value="'.$apellido_paterno_g_6.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido materno:</label>
                        <input class="form-control" type="text" name="apellido_materno_g" id="apellido_materno_g" value="'.$apellido_materno_g_6.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre de identificación:</label>
                        <input class="form-control" type="text" name="nombre_identificacion_g" value="'.$nombre_identificacion_g_6.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Autoridad que emite la identificación:</label>
                        <input class="form-control" type="text" name="autoridad_emite_g" value="'.$autoridad_emite_g_6.'">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número de identificación:</label>
                        <input class="form-control" type="text" name="numero_identificacion_g" value="'.$numero_identificacion_g_6.'">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                        <input class="form-control" type="date" max="'.date("Y-m-d").'" name="fecha_nacimiento_g" value="'.$fecha_nacimiento_g_6.'">
                      </div> 
                      <div class="col-md-4 form-group">
                        <label>Género:</label>
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="genero_g" checked value="1" '.$genero_t_6.'>
                                Masculino
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="genero_g" value="2" '.$genero_t_6_2.'>
                                Femenino
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>  
                    </div>
                    <div class="row">
                      <div class="col-md-2 form-group"></div>
                      <div class="col-md-4 form-group">
                        <label>R.F.C:</label>
                        <input class="form-control rfcPerf" type="text" name="r_f_c_g" value="'.$r_f_c_g_6.'"><p>*Cuando cuente con R.F.C</p>
                      </div>
                      <div class="col-md-1 form-group">
                        <br>
                        <a href="#modal_rfc" data-toggle="modal">
                        <button type="button" class="btn btn_ayuda">
                          <i class="mdi mdi-help"></i>
                        </button></a>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>CURP:</label>
                        <input class="form-control" type="text" name="curp_g" value="'.$curp_g_6.'">
                        <p>*Cuando cuente con CURP</p>
                      </div>
                      <div class="col-md-2 form-group"></div>
                    </div>
                  </form>  
                  ';   
          $html.='<hr class="subtitle barra_menu">
                <div class="row barra_menu"> 
                    <div class="col-md-12" align="right">';
                    if($idtipo_cliente_f_6>0){
                      $html.='<button type="button" class="btn gradient_nepal2" onclick="imprimir_formato1(6,'.$idperfilamiento.')"><i class="fa fa-print"></i> Imprimir formato conoce a tu cliente</button>';
                    }
                    $html.='
                      <button type="button" class="btn gradient_nepal2" onclick="regresa()"><i class="fa fa-reply" aria-hidden="true"></i> Regresar</button>
                      <button type="button" class="btn gradient_nepal2" onclick="guardar_tipo_cliente_6()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>';     
      }
      
      echo $html;
  }
  public function searchpais(){
        $search = $this->input->get('search');
        $results=$this->ModeloCliente->get_select_like_pais($search);
        echo json_encode($results);    
  }
  public function get_estados(){
      $tipo = $this->input->post('tipo');
      $html = '';
      $idaux_t='';
      $idaux_t_t='';
      if($tipo==1){
        $idaux_t='estado_d';
        $idaux_t_t='estado_d_1';
      }else if($tipo==2){
        $idaux_t='estado_d';
        $idaux_t_t='estado_d_2';
      }else if($tipo==3){
        $idaux_t='estado_d';
        $idaux_t_t='estado_d_3';
      }else if($tipo==5){
        $idaux_t='estado_d';
        $idaux_t_t='estado_d5_5';
      }

      $result = $this->ModeloCatalogos->getData('estado');
      $html.= '<select class="form-control '.$idaux_t_t.'" name="'.$idaux_t.'" disabled>';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->estado.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
    /// Funciones de guardado
  public function registro_perfilamiento(){   
      $data = $this->input->post();
      $id = $data['idperfilamiento'];
      unset($data['idperfilamiento']);

      $aux=0;
      if ($id>0) {
          $this->ModeloCatalogos->updateCatalogo($data,'idperfilamiento',$id,'perfilamiento');
          $result=2;
          $aux=$id;
      }else{
          $data['idcliente']=$this->idcliente;
          $aux=$this->ModeloCatalogos->tabla_inserta('perfilamiento',$data);
          $result=1;
      }   
      $arraydata = array('id'=>$aux,'status'=>$result);
      echo json_encode($arraydata);         
  }

  public function consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion)
  {
    $Usuario='espinozabe1';
    $Password='B8019775';
    //$Usuario='paquinije'; //datos de prueba
    //$Password='389DB30E'; //datos de prueba
    $jsonData = array(
      'usuario'=> $Usuario,
      'clave' => $Password
    );
    //log_message('error', 'token_ws: '.$this->session->userdata("token_ws"));
    if($this->session->userdata("token_ws")=="0"){
      $token=str_replace('"', '', $this->getJWT());
    }else{
      $token=str_replace('"', '', $this->session->userdata("token_ws"));
    }
    //log_message('error', 'token: '.$token);
    $parametros = array(
      'apellido'=> $Apellido,
      'nombre' => $Nombre,
      'identificacion'=> $Identificacion,
      'pepsOtrosPaises' => "S",
      'satXDenominacion'=> "S",
      'documentosSimilares' => "S"
    );

    $array_list=$this->getLista($parametros,$id_hist,$token);
    $arr=json_decode($array_list);

    $status=json_encode($arr->status);
    $res=$arr->resultados;
    $respuesta = str_replace('{"resultados":', '', $res);
    $respuesta = str_replace('[', '', $respuesta);
    $respuesta = str_replace(']', '', $respuesta);
    $respuesta = trim($respuesta, "'\'");
    $respuesta=substr($respuesta, 0, -1);

    //log_message('error', 'status: '.$status);
    if($status=="401"){
      $token=str_replace('"', '', $this->getJWT());
      $array_list=$this->getLista($parametros,$id_hist,$token);
      $arr=json_decode($array_list);
      $status=json_encode($arr->status);
      $res=$arr->resultados;
      $respuesta = str_replace('{"resultados":', '', $res);
      $respuesta = str_replace('[', '', $respuesta);
      $respuesta = str_replace(']', '', $respuesta);
      $respuesta=substr($respuesta, 0, -1);
    }
    return $respuesta;
  }

  public function getJWT(){
    $Usuario='espinozabe1';
    $Password='B8019775';
    //$Usuario='paquinije'; //datos de prueba
    //$Password='389DB30E'; //datos de prueba
    $jsonData = array(
      'usuario'=> $Usuario,
      'clave' => $Password
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json'
    ));
    curl_setopt($ch, CURLOPT_URL, "https://mbalistas.prevenciondelavado.com/Login" );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_POST, 1 );
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
    $result=curl_exec ($ch);
    //echo $result;
    curl_close($ch);
    //log_message('error', 'result: '.$result);
    $data = json_decode($result);
    $token = json_encode($data->token);
    $this->session->set_userdata("token_ws",str_replace('"', '',$token));
    //log_message('error', 'token: '.$token);
    return $token;
  }

  public function getLista($parametros,$id_hist,$token){
    //log_message('error', 'token: '.$token);
    //log_message('error', 'parametros: '.var_dump($parametros));
    $autori=array('Content-Type: application/json',"Authorization: Bearer {$token} ");
    //log_message('error', 'autori: '.var_dump($autori));
    //$autori=array('Authorization: Bearer '.$token.'');
    $url="https://mbalistas.prevenciondelavado.com/listas";
    $ch2 = curl_init();
    //curl_setopt($ch2, CURLOPT_HEADER, false); 
    curl_setopt($ch2, CURLOPT_HTTPHEADER, $autori );
    curl_setopt($ch2, CURLOPT_URL, $url); 
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch2, CURLOPT_POST, 1 );
    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($parametros));
    $data2 = curl_exec($ch2); 
    //echo $data2;
    curl_close($ch2);
    //log_message('error', 'data2: '.$data2);

    $ddata = json_decode($data2);
    //log_message('error', 'sizeof array_emp: '.sizeof($array_emp));
    $status="200";
    if(strlen($data2)==17){
      $status = "200";
    }else if(strlen($data2)>17 && strlen($data2)<=162){
      $status = json_encode($ddata->status);
    }
    return json_encode(array("status"=>$status,"resultados"=>$data2));
    if($status=="200" && strlen($data2)>162){ //ok -- se hace desde cada funcion de perfilamiento
      //$this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>$data2),array("id"=>$id_hist),'historico_consulta_pb');//guarda el resultado del webservice
    }
  }


  /// Funciones de guardado
  public function registro_tipo_cliente_p_f_m(){ // tipo cliente 1 
      $data = $this->input->post();
      $id = $data['idtipo_cliente_p_f_m'];
      unset($data['idtipo_cliente_p_f_m']);
      //log_message('error', 'estado_d: '.$data['estado_d']);
      $data['estado_d'] = $this->input->post('estado_d');
      $desdeop = $data['desdeope'];
      //log_message('error', 'desdeop: '.$desdeop);
      unset($data['desdeope']);
      $idopera = $data['idopera'];
      unset($data['idopera']);
      //log_message('error', 'id TIPO DE CLIENTE: '.$id);
      //log_message('error', 'id OPERA: '.$idopera);
      $aux=0;
      $hist_cpb=0;
      if($id>0) {
        $get_perf=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_m',array("idtipo_cliente_p_f_m"=>$id));
        foreach ($get_perf as $it) {
          $nombre = $it->nombre;
          $apellido_p = $it->apellido_paterno;
          $apellido_m = $it->apellido_materno;
          $r_f_c_t = $it->r_f_c_t;
          $curp_t = $it->curp_t;
          $identificacion = $it->numero_identificacion;
        }
        /*if($desdeop==1 ){
          if($nombre!=$data['nombre'] || $apellido_p!=$data['apellido_paterno'] || $apellido_m!=$data['apellido_materno'] || $r_f_c_t!=$data['r_f_c_t'] || $curp_t!=$data['curp_t'] || $identificacion!=$data['numero_identificacion']){
            $identifica = $data['numero_identificacion'];
            if($data['r_f_c_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']; 
            }
            if($data['curp_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']."|".$data['curp_t']; 
            }
            $where = array("id_operacion"=>$idopera);
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_operacion"=>$idopera,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre'], "apellidos"=>$data['apellido_paterno']." ".$data['apellido_materno'], "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }else{
          if($nombre!=$data['nombre'] || $apellido_p!=$data['apellido_paterno'] || $apellido_m!=$data['apellido_materno'] || $r_f_c_t!=$data['r_f_c_t'] || $curp_t!=$data['curp_t'] || $identificacion!=$data['numero_identificacion']){
            $identifica = $data['numero_identificacion'];
            if($data['r_f_c_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']; 
            }
            if($data['curp_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']."|".$data['curp_t']; 
            }
            $where = array("id_perfilamiento"=>$data['idperfilamiento']);
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_perfilamiento"=>$data['idperfilamiento'],"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre'], "apellidos"=>$data['apellido_paterno']." ".$data['apellido_materno'], "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }*/
        $this->ModeloCatalogos->updateCatalogo($data,'idtipo_cliente_p_f_m',$id,'tipo_cliente_p_f_m');
        $result=2;
        $aux=$id;
      }else{
        $aux=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_f_m',$data);
        $identifica = $data['numero_identificacion'];
        if($data['r_f_c_t']!=""){
          $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']; 
        }
        if($data['curp_t']!=""){
          $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']."|".$data['curp_t']; 
        }
        $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre'], "apellidos"=>$data['apellido_paterno']." ".$data['apellido_materno'], "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$aux);
        $hist_cpb=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
        $result=1;
      }   
      //despues de crear el cliente editar la operacion, asignarle el cliente a la operacion
      if($desdeop>0 && $idopera>0){
        //$id_oc=$this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_cliente"=>$this->idcliente,"id_clientec"=>$aux,"fecha_reg"=>date("Y-m-d H:i:s")));
      }

      //log_message('error', 'id hist_cpb: '.$hist_cpb);
      $respuesta = "";
      if($hist_cpb>0){
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['apellido_paterno']." ".$data['apellido_materno'];
        $Nombre=$data['nombre'];
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';
        //$respuesta=$this->consumoAPI($hist_cpb,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$respuesta=$this->consumoAPI($hist_cpb,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);
        */
        $this->ModeloCatalogos->updateCatalogo2(array("resultado"=>$respuesta),array("id"=>$hist_cpb),'historico_consulta_pb');//guarda el resultado del webservice
      }
      $arraydata = array('id'=>$aux,'hist_cpb'=>$hist_cpb,"resultpb"=>$respuesta);
      echo json_encode($arraydata);

      //$arraydata = array('id'=>$aux,'status'=>$result,'hist_cpb'=>$hist_cpb);
      //echo json_encode($arraydata);         
  }
  public function registro_tipo_cliente_p_f_e(){   // tipo cliente 2 
      $data = $this->input->post();
      $id = $data['idtipo_cliente_p_f_e'];
      $data['estado_d'] = $this->input->post('estado_d');
      //log_message('error', 'estado_d: '.$data['estado_d']);
      unset($data['idtipo_cliente_p_f_e']);
      $desdeop = $data['desdeope'];
      //log_message('error', 'desdeop: '.$desdeop);
      unset($data['desdeope']);
      $idopera = $data['idopera'];
      unset($data['idopera']);
      $aux=0;
      $hist_cpb=0;
      if ($id>0) {
        $get_perf=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_e',array("idtipo_cliente_p_f_e"=>$id));
        foreach ($get_perf as $it) {
          $nombre = $it->nombre;
          $apellido_p = $it->apellido_paterno;
          $apellido_m = $it->apellido_materno;
          $r_f_c_t = $it->r_f_c_t;
          $curp_t = $it->curp_t;
        }
        /*if($desdeop==1){
          if($nombre!=$data['nombre'] || $apellido_p!=$data['apellido_paterno'] || $apellido_m!=$data['apellido_materno'] || $r_f_c_t!=$data['r_f_c_t'] || $curp_t!=$data['curp_t']){
            $identifica = $data['numero_identificacion'];
            if($data['r_f_c_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']; 
            }
            if($data['curp_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']."|".$data['curp_t']; 
            }
            $where = array("id_operacion"=>$idopera);
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            $where2 = array("id_operacion"=>$idopera,"id"=>$hist_cpb_get);
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre'], "apellidos"=>$data['apellido_paterno']." ".$data['apellido_materno'], "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }else{
          if($nombre!=$data['nombre'] || $apellido_p!=$data['apellido_paterno'] || $apellido_m!=$data['apellido_materno'] || $r_f_c_t!=$data['r_f_c_t'] || $curp_t!=$data['curp_t']){
            $identifica = $data['numero_identificacion'];
            if($data['r_f_c_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']; 
            }
            if($data['curp_t']!=""){
              $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']."|".$data['curp_t']; 
            }
            $where = array("id_perfilamiento"=>$data["idperfilamiento"]);
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            $where2 = array("id_perfilamiento"=>$data["idperfilamiento"],"id"=>$hist_cpb_get);
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre'], "apellidos"=>$data['apellido_paterno']." ".$data['apellido_materno'], "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }*/
        $this->ModeloCatalogos->updateCatalogo($data,'idtipo_cliente_p_f_e',$id,'tipo_cliente_p_f_e');
        $result=2;
        $aux=$id;
      }else{
        $aux=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_f_e',$data);
        $identifica = $data['numero_identificacion'];
        if($data['r_f_c_t']!=""){
          $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']; 
        }
        if($data['curp_t']!=""){
          $identifica = $data['numero_identificacion']."|".$data['r_f_c_t']."|".$data['curp_t']; 
        }
        $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre'], "apellidos"=>$data['apellido_paterno']." ".$data['apellido_materno'], "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$aux);
        $hist_cpb=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
        $result=1;
      }   
      $respuesta = "";
      if($hist_cpb>0){
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['apellido_paterno']." ".$data['apellido_materno'];
        $Nombre=$data['nombre'];
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';

        //$respuesta=$this->consumoAPI($hist_cpb,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$respuesta=$this->consumoAPI($hist_cpb,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);
        */
        $this->ModeloCatalogos->updateCatalogo2(array("resultado"=>$respuesta),array("id"=>$hist_cpb),'historico_consulta_pb');//guarda el resultado del webservice
      }
      //despues de crear el cliente editar la operacion, asignarle el cliente a la operacion
      if($desdeop>0 && $idopera>0){
        //$id_oc=$this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_cliente"=>$this->idcliente,"id_clientec"=>$aux,"fecha_reg"=>date("Y-m-d H:i:s")));
      }
      $arraydata = array('id'=>$aux,'hist_cpb'=>$hist_cpb,"resultpb"=>$respuesta);
      echo json_encode($arraydata);
      /*$arraydata = array('id'=>$aux,'status'=>$result,'hist_cpb'=>$hist_cpb);
      echo json_encode($arraydata);*/       
  }
  public function registro_tipo_cliente_p_m_m_e(){   // tipo cliente 3
      $data = $this->input->post();
      $id = $data['idtipo_cliente_p_m_m_e'];
      unset($data['idtipo_cliente_p_m_m_e']);
      $desdeop = $data['desdeope'];
      
      unset($data['desdeope']);
      $idopera = $data['idopera'];
      unset($data['idopera']);
      $aux=0;
      //log_message('error', 'id: '.$id);
      //log_message('error', 'desdeop: '.$desdeop);
      $hist_cpb=0;
      if($id>0) {
        $get_perf=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_m_m_e',array("idtipo_cliente_p_m_m_e"=>$id));
        foreach ($get_perf as $it) {
          $razon_social = $it->razon_social;
          $clave_registro = $it->clave_registro;
        }
        /*if($desdeop==1){
          if($razon_social!=$data['razon_social'] || $clave_registro!=$data['clave_registro']){
            $identifica = $data['clave_registro'];

            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $where = array("id_operacion"=>$idopera);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_operacion"=>$idopera,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['razon_social'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }else{
          if($razon_social!=$data['razon_social'] || $clave_registro!=$data['clave_registro']){
            $identifica = $data['clave_registro'];

            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $where = array("id_perfilamiento"=>$data['idperfilamiento']);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_perfilamiento"=>$data['idperfilamiento'],"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['razon_social'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }*/
        if(isset($data['extranjero'])){
          $data['extranjero']='on'; 
        }else{
          $data['extranjero']='';
        }
        $this->ModeloCatalogos->updateCatalogo($data,'idtipo_cliente_p_m_m_e',$id,'tipo_cliente_p_m_m_e');
        $result=2;
        $aux=$id;
      }else{
        $aux=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_m_m_e',$data);
        $result=1;

        $identifica=$data['clave_registro'];
        $array_pb=array("fecha_consulta"=>date("Y-m-d H:i:s"),"nombre"=>$data['razon_social'],"apellidos"=>"","identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$aux);
        $hist_cpb=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
      } 
      //log_message('error', 'hist_cpb: '.$hist_cpb);
      $respuesta = "";
      if($hist_cpb>0){
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['razon_social'];
        $Nombre="";
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';
        //$respuesta=$this->consumoAPI($hist_cpb,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$respuesta=$this->consumoAPI($hist_cpb,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);
        */
        $this->ModeloCatalogos->updateCatalogo2(array("resultado"=>$respuesta),array("id"=>$hist_cpb),'historico_consulta_pb');//guarda el resultado del webservice
      } 
      //despues de crear el cliente editar la operacion, asignarle el cliente a la operacion
      if($desdeop>0 && $idopera>0){
        //$id_oc=$this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_cliente"=>$this->idcliente,"id_clientec"=>$aux,"fecha_reg"=>date("Y-m-d H:i:s")));
      }
      $arraydata = array('id'=>$aux,'hist_cpb'=>$hist_cpb,"resultpb"=>$respuesta);
      echo json_encode($arraydata);
      /*$arraydata = array('id'=>$aux,'status'=>$result,'hist_cpb'=>$hist_cpb);
      echo json_encode($arraydata);*/         
  }
  public function registro_tipo_cliente_p_m_m_d(){   // tipo cliente 4
      $data = $this->input->post();
      $id = $data['idtipo_cliente_p_m_m_d'];
      unset($data['idtipo_cliente_p_m_m_d']);
      $desdeop = $data['desdeope'];
      //log_message('error', 'desdeop: '.$desdeop);
      unset($data['desdeope']);
      $idopera = $data['idopera'];
      unset($data['idopera']);
      $aux=0;
      $hist_cpb=0;
      if ($id>0){
        $get_perf=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_m_m_d',array("idtipo_cliente_p_m_m_d"=>$id));
        foreach ($get_perf as $it) {
          $nombre_persona = $it->nombre_persona;
        }
        /*if($desdeop==1){ 
          if($nombre_persona!=$data['nombre_persona']){
            $identifica = $data['nombre_persona'];
            $where = array("id_operacion"=>$idopera);
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            $where2 = array("id_operacion"=>$idopera,"id"=>$hist_cpb_get);
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre_persona'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }else{
          if($nombre_persona!=$data['nombre_persona']){
            $identifica = $data['nombre_persona'];
            $where = array("id_perfilamiento"=>$data['idperfilamiento']);
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            $where2 = array("id_perfilamiento"=>$data['idperfilamiento'],"id"=>$hist_cpb_get);
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre_persona'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }*/
        $this->ModeloCatalogos->updateCatalogo($data,'idtipo_cliente_p_m_m_d',$id,'tipo_cliente_p_m_m_d');
        $result=2;
        $aux=$id;
      }else{
        $aux=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_m_m_d',$data);
        $result=1;
        $identifica = $data['nombre_persona'];
        $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['nombre_persona'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$aux);
        $hist_cpb=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
      }
      $respuesta = "";
      if($hist_cpb>0){
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['nombre_persona'];
        $Nombre="";
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';

        //$respuesta=$this->consumoAPI($hist_cpb,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$respuesta=$this->consumoAPI($hist_cpb,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);
        */
        $this->ModeloCatalogos->updateCatalogo2(array("resultado"=>$respuesta),array("id"=>$hist_cpb),'historico_consulta_pb');//guarda el resultado del webservice
      } 
      //despues de crear el cliente editar la operacion, asignarle el cliente a la operacion
      if($desdeop>0 && $idopera>0){
        //$id_oc=$this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_cliente"=>$this->idcliente,"id_clientec"=>$aux,"fecha_reg"=>date("Y-m-d H:i:s")));
      }
      $arraydata = array('id'=>$aux,'hist_cpb'=>$hist_cpb,"resultpb"=>$respuesta);
      echo json_encode($arraydata);
      /*$arraydata = array('id'=>$aux,'status'=>$result,'hist_cpb'=>$hist_cpb);
      echo json_encode($arraydata);*/
  }
  public function registro_tipo_cliente_e_c_o_i(){   // tipo cliente 5
      $data = $this->input->post();
      $id = $data['idtipo_cliente_e_c_o_i'];
      unset($data['idtipo_cliente_e_c_o_i']);
      $aux=0;
      $desdeop = $data['desdeope'];
      //log_message('error', 'desdeop: '.$desdeop);
      unset($data['desdeope']);
      $idopera = $data['idopera'];
      unset($data['idopera']);
      $hist_cpb=0;
      if ($id>0){
        $get_perf=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_e_c_o_i',array("idtipo_cliente_e_c_o_i"=>$id));
        foreach ($get_perf as $it) {
          $denominacion = $it->denominacion;
          $clave_registro = $it->clave_registro;
        }
        /*if($desdeop==1){ 
          if($denominacion!=$data['denominacion'] || $clave_registro!=$data['clave_registro']){
            $identifica = $data['clave_registro'];
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $where = array("id_operacion"=>$idopera);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_operacion"=>$idopera,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['denominacion'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }else{
          if($denominacion!=$data['denominacion'] || $clave_registro!=$data['clave_registro']){
            $identifica = $data['clave_registro'];
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $where = array("id_perfilamiento"=>$data['idperfilamiento']);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_perfilamiento"=>$data['idperfilamiento'],"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['denominacion'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }*/
        $this->ModeloCatalogos->updateCatalogo($data,'idtipo_cliente_e_c_o_i',$id,'tipo_cliente_e_c_o_i');
        $result=2;
        $aux=$id;
      }else{
        $aux=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_e_c_o_i',$data);
        $result=1;
        $identifica = $data['clave_registro'];
        $array_pb=array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['denominacion'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$aux);
        $hist_cpb=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
      }   
      $respuesta = "";
      if($hist_cpb>0){
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['denominacion'];
        $Nombre="";
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';

        //$respuesta=$this->consumoAPI($hist_cpb,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$respuesta=$this->consumoAPI($hist_cpb,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);
        */
        $this->ModeloCatalogos->updateCatalogo2(array("resultado"=>$respuesta),array("id"=>$hist_cpb),'historico_consulta_pb');//guarda el resultado del webservice
      } 
      //despues de crear el cliente editar la operacion, asignarle el cliente a la operacion
      if($desdeop>0 && $idopera>0){
        //$id_oc=$this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_cliente"=>$this->idcliente,"id_clientec"=>$aux,"fecha_reg"=>date("Y-m-d H:i:s")));
      }
      $arraydata = array('id'=>$aux,'hist_cpb'=>$hist_cpb,"resultpb"=>$respuesta);
      echo json_encode($arraydata);
      /*$arraydata = array('id'=>$aux,'status'=>$result,'hist_cpb'=>$hist_cpb);
      echo json_encode($arraydata);*/     
  }
  public function registro_tipo_cliente_f(){   // tipo cliente 6
      $data = $this->input->post();
      $id = $data['idtipo_cliente_f'];
      unset($data['idtipo_cliente_f']);
      $aux=0;
      $desdeop = $data['desdeope'];
      //log_message('error', 'desdeop: '.$desdeop);
      unset($data['desdeope']);
      $idopera = $data['idopera'];
      unset($data['idopera']);
      $hist_cpb=0;
      if($id>0){
        $get_perf=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_f',array("idtipo_cliente_f"=>$id));
        foreach ($get_perf as $it) {
          $denominacion = $it->denominacion;
          $clave_registro = $it->clave_registro;
          $numero_referencia = $it->numero_referencia;
        }
        /*if($desdeop==1){ 
          if($denominacion!=$data['denominacion'] || $clave_registro!=$data['clave_registro'] || $clave_registro!=$data['numero_referencia']){
            $identifica = $data['clave_registro']."|".$data['numero_referencia'];
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $where = array("id_operacion"=>$idopera);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_operacion"=>$idopera,"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['denominacion'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }else{
          if($denominacion!=$data['denominacion'] || $clave_registro!=$data['clave_registro'] || $clave_registro!=$data['numero_referencia']){
            $identifica = $data['clave_registro']."|".$data['numero_referencia'];
            //$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            $where = array("id_perfilamiento"=>$data['idperfilamiento']);
            $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
            foreach($get_hist as $k){
              $hist_cpb_get = $k->id;
            }
            //$where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id,"id"=>$hist_cpb_get);
            $where2 = array("id_perfilamiento"=>$data['idperfilamiento'],"id"=>$hist_cpb_get);
            $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['denominacion'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$id);
            
            $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
            $hist_cpb=$hist_cpb_get;
          }
        }*/
        $this->ModeloCatalogos->updateCatalogo($data,'idtipo_cliente_f',$id,'tipo_cliente_f');
        $result=2;
        $aux=$id;
      }else{
        $aux=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_f',$data);
        $result=1;
        $identifica = $data['clave_registro']."|".$data['numero_referencia'];
        $array_pb= array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['denominacion'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['idperfilamiento'],"id_clientec"=>$aux);
        $hist_cpb=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
      }   
      $respuesta = "";
      if($hist_cpb>0){
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['denominacion'];
        $Nombre="";
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';

        //$respuesta=$this->consumoAPI($hist_cpb,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$respuesta=$this->consumoAPI($hist_cpb,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);
        */
        $this->ModeloCatalogos->updateCatalogo2(array("resultado"=>$respuesta),array("id"=>$hist_cpb),'historico_consulta_pb');//guarda el resultado del webservice
      }
      //despues de crear el cliente editar la operacion, asignarle el cliente a la operacion
      if($desdeop>0 && $idopera>0){
        //$id_oc=$this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['idperfilamiento'],"id_cliente"=>$this->idcliente,"id_clientec"=>$aux,"fecha_reg"=>date("Y-m-d H:i:s")));
      } 
      $arraydata = array('id'=>$aux,'hist_cpb'=>$hist_cpb,"resultpb"=>$respuesta);
      echo json_encode($arraydata);
      /*$arraydata = array('id'=>$aux,'status'=>$result,'hist_cpb'=>$hist_cpb);
      echo json_encode($arraydata); */        
  }
  public function verificar_perfilamiento_add(){ 
    $idperfilamiento=0;
    $idtipo_cliente=0;
    $arraywhere = array('idcliente'=>$this->idcliente); 
    $get_perfilamiento=$this->ModeloCatalogos->getselectwherestatus('*','perfilamiento',$arraywhere);
    foreach ($get_perfilamiento as $item){
      $idperfilamiento=$item->idperfilamiento;
      $idtipo_cliente=$item->idtipo_cliente;
    }
    $arraydata = array('idperfilamiento'=>$idperfilamiento,'idtipo_cliente'=>$idtipo_cliente);
    echo json_encode($arraydata);  
  }
  public function registro_beneficiario_1(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
        $id_beneficiario=$DATA[$i]->id_beneficiario;
        $data['idtipo_cliente_p_f_m']=$DATA[$i]->idtipo_cliente_p_f_m;
        $data['nombre']=$DATA[$i]->nombre;
        $data['apellido_paterno']=$DATA[$i]->apellido_paterno;
        $data['apellido_materno']=$DATA[$i]->apellido_materno;  
        $data['fecha_nacimiento']=$DATA[$i]->fecha_nacimiento;
        $data['genero']=$DATA[$i]->genero;
        $data['porcentaje']=$DATA[$i]->porcentaje;
        $data['tipo_vialidad']=$DATA[$i]->tipo_vialidad;
        $data['calle']=$DATA[$i]->calle;  
        $data['no_ext']=$DATA[$i]->no_ext;
        $data['no_int']=$DATA[$i]->no_int;
        $data['colonia']=$DATA[$i]->colonia;
        $data['municipio']=$DATA[$i]->municipio;   
        $data['localidad']=$DATA[$i]->localidad;
        $data['estado']=$DATA[$i]->estado;  
        $data['cp']=$DATA[$i]->cp;  
        if($id_beneficiario==0){
          $this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_f_m_beneficiario',$data);
        }else{
          $this->ModeloCatalogos->updateCatalogo($data,'id_beneficiario',$id_beneficiario,'tipo_cliente_p_f_m_beneficiario');
        }
    }
  }
  public function registro_beneficiario_2(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
        $id_beneficiario=$DATA[$i]->id_beneficiario;
        $data['idtipo_cliente_p_f_e']=$DATA[$i]->idtipo_cliente_p_f_e;
        $data['nombre']=$DATA[$i]->nombre;
        $data['apellido_paterno']=$DATA[$i]->apellido_paterno;
        $data['apellido_materno']=$DATA[$i]->apellido_materno;  
        $data['fecha_nacimiento']=$DATA[$i]->fecha_nacimiento;
        $data['genero']=$DATA[$i]->genero;
        $data['porcentaje']=$DATA[$i]->porcentaje;
        $data['tipo_vialidad']=$DATA[$i]->tipo_vialidad;
        $data['calle']=$DATA[$i]->calle;  
        $data['no_ext']=$DATA[$i]->no_ext;
        $data['no_int']=$DATA[$i]->no_int;
        $data['colonia']=$DATA[$i]->colonia;
        $data['municipio']=$DATA[$i]->municipio;   
        $data['localidad']=$DATA[$i]->localidad;
        $data['estado']=$DATA[$i]->estado;  
        $data['cp']=$DATA[$i]->cp;  
        if($id_beneficiario==0){
          $this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_f_e_beneficiario',$data);
        }else{
          $this->ModeloCatalogos->updateCatalogo($data,'id_beneficiario',$id_beneficiario,'tipo_cliente_p_f_e_beneficiario');
        }
    }
  }
  function get_bneficiario(){
    $id = $this->input->post('idtipo_cliente');
    $results=$this->ModeloCatalogos->getselectwhere('tipo_cliente_p_f_m_beneficiario','idtipo_cliente_p_f_m',$id);
    echo json_encode($results);
  }
  function updateregistro_beneficiario(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $this->ModeloCatalogos->updateCatalogo($data,'id_beneficiario',$id,'tipo_cliente_p_f_m_beneficiario');
  }
  function get_bneficiario_2(){
    $id = $this->input->post('idtipo_cliente');
    $results=$this->ModeloCatalogos->getselectwhere('tipo_cliente_p_f_e_beneficiario','idtipo_cliente_p_f_e',$id);
    echo json_encode($results);
  }
  function updateregistro_beneficiario_2(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $this->ModeloCatalogos->updateCatalogo($data,'id_beneficiario',$id,'tipo_cliente_p_f_e_beneficiario');
  }

  function updateConsultaPBH(){
    $id = $this->input->post('id');
    $data = $this->input->post();
    unset($data['id']);
    $data["fecha_consulta"] = date("Y-m-d H:i:s");
    $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'historico_consulta_pb');
    echo $id;
  } 
  function get_bneficiario_datos(){
    $id_bene = $this->input->post('id_bene');
    $tipo_bene = $this->input->post('tipo_bene');
    if($tipo_bene==1){
      $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_bene);
    }else if($tipo_bene==2){
      $results=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$id_bene);
    }else if($tipo_bene==3){
      $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$id_bene);
    }
    foreach ($results as $k) {
      if($tipo_bene==1){
        $nombre = $k->nombre;
        $apellidos = $k->apellido_paterno." ".$k->apellido_materno;
        $identifica = $k->numero_identificacion."|".$k->r_f_c_d."|".$k->curp_d;
      }
      if($tipo_bene==2 || $tipo_bene==3){
        $nombre = "";
        $apellidos = $k->razon_social;
        $identifica = $k->rfc;
      }
    }

    $url='https://www.prevenciondelavado.com/listas/api/busqueda';
    $Usuario='espinozabe1';
    $Password='B8019775';
    $Apellido=$apellidos;
    $Nombre=$nombre;
    $Identificacion=$identifica;
    $PEPS_otros_paises='S';
    $Incluye_SAT='S';
    $SATxDenominacion='S';
    //$respuesta=$this->consumoAPI(0,urlencode($Apellido),urlencode($Nombre),$Identificacion);
    //$respuesta=$this->consumoAPI(0,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas
    //echo $respuesta; //deshabilitada para reducir consultas
    /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
    $sesion = curl_init($url);
    // definir tipo de petición a realizar: POST
    curl_setopt ($sesion, CURLOPT_POST, 1); 
    // Le pasamos los parámetros definidos anteriormente
    curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
    // sólo queremos que nos devuelva la respuesta
    curl_setopt($sesion, CURLOPT_HEADER, false); 
    curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
    // ejecutamos la petición
    $respuesta = curl_exec($sesion); 
    $error = curl_error($sesion);
    // cerramos conexión
    curl_close($sesion); 
    $respuesta = str_replace('[', '', $respuesta);
    $respuesta = str_replace(']', '', $respuesta);
    echo $respuesta;*/
    //echo $error;
    //log_message('error', 'respuesta: '.$respuesta);

    //echo json_encode("results"=>$results);
  }

  function get_cliente_datos(){
    $id_clientec = $this->input->post('id_clientec');
    $id_perfilamiento = $this->input->post('id_perfilamiento');
    $tipo = $this->input->post('tipo');
    if($tipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';} 
    //$get_info=$this->General_model->get_tableRow($tabla,array($idt=>$data['id_clientec']));
    $results=$this->ModeloCatalogos->getselectwhere($tabla,$idt,$id_clientec);
    //echo json_encode($results);
    //log_message('error', 'tipo: '.$tipo);
    foreach ($results as $k) {

      if($tipo==1 || $tipo==2){
        $nombre = $k->nombre;
        $apellidos = $k->apellido_paterno." ".$k->apellido_materno;
        $identifica = $k->numero_identificacion;
        if($k->r_f_c_t!=""){
          $identifica =$k->numero_identificacion."|".$k->r_f_c_t; 
        }
        if($k->curp_t!=""){
          $identifica = $k->numero_identificacion."|".$k->r_f_c_t."|".$k->curp_t; 
        }
      }
      if($tipo==3){
        $nombre = $k->razon_social;
        $apellidos = "";
        $identifica =$k->clave_registro;
      }
      if($tipo==4){
        $identifica = $k->clave_registro;
        $identifica = $k->clave_registro;
        $nombre = "";
        $apellidos = $k->nombre_persona;
        $identifica = "";
      }
      if($tipo==5){
        $nombre = "";
        $apellidos = $k->denominacion;
        $identifica = $k->clave_registro;
      }
      if($tipo==6){
        $nombre = "";
        $apellidos = $k->denominacion;
        $identifica = $k->clave_registro."|".$k->numero_referencia;
      }
    }

    $url='https://www.prevenciondelavado.com/listas/api/busqueda';
    $Usuario='espinozabe1';
    $Password='B8019775';
    $Apellido=$apellidos;
    $Nombre=$nombre;
    $Identificacion=$identifica;
    $PEPS_otros_paises='S';
    $Incluye_SAT='S';
    $SATxDenominacion='S';

    //$respuesta=$this->consumoAPI(0,urlencode($Apellido),urlencode($Nombre),$Identificacion);
    //$respuesta=$this->consumoAPI(0,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas
    //echo $respuesta; //deshabilitada para reducir consultas
    /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
    $sesion = curl_init($url);
    // definir tipo de petición a realizar: POST
    curl_setopt ($sesion, CURLOPT_POST, 1); 
    // Le pasamos los parámetros definidos anteriormente
    curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
    // sólo queremos que nos devuelva la respuesta
    curl_setopt($sesion, CURLOPT_HEADER, false); 
    curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
    // ejecutamos la petición
    $respuesta = curl_exec($sesion); 
    $error = curl_error($sesion);
    // cerramos conexión
    curl_close($sesion); 
    $respuesta = str_replace('[', '', $respuesta);
    $respuesta = str_replace(']', '', $respuesta);
    echo $respuesta;
    */
    //echo $error;
    log_message('error', 'respuesta: '.$respuesta);
  }

  function updateConsultaPBHDB(){
    $id = $this->input->post('id');
    $data = $this->input->post();
    unset($data['id']); 
    $data["fecha_consulta"] = date("Y-m-d H:i:s");
    $get = $this->ModeloCliente->detalles_busquedaPB($id,$data['id_perfilamiento'],$data['id_clientec']);
    foreach ($get as $key) {
      $id_hist = $key->id;
    }
    unset($data['id_perfilamiento']); unset($data['id_clientec']);
    $this->ModeloCatalogos->updateCatalogo($data,'id',$id_hist,'historico_consulta_pb');
  } 

  public function get_estados_clientes(){
    $id_t = $this->input->post('id_t');
    $result = $this->ModeloCatalogos->getData('estado');
    $html='';
    $html.= '<select class="form-control span_div_p_'.$id_t.'" id="estado_b_1" disabled>';
    foreach ($result as $item) {
       $html.='<option value="'.$item->id.'">'.$item->estado.'</option>';
    }
    $html.= '</select>';
    echo $html;  
  }

  public function get_estados_clientes2(){
      $id_t = $this->input->post('id_t');
      $result = $this->ModeloCatalogos->getData('estado');
      $html='';
      $html.= '<select class="form-control span_div_p2_'.$id_t.'" id="estado_b_2" disabled>';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->estado.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
  public function get_tipo_vialidad(){
      $id_t = $this->input->post('id_t');
      $result=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      $html='';
      $html.= '<select class="form-control span_div_ptv_'.$id_t.'" id="tipo_vialidad_b_1">';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->nombre.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
  public function get_tipo_vialidad2(){
      $id_t = $this->input->post('id_t');
      $result=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      $html='';
      $html.= '<select class="form-control span_div_ptv2_'.$id_t.'" id="tipo_vialidad_b_2">';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->nombre.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }

  public function getDatosCP(){
    $cp =$this->input->post('cp');
    $col =$this->input->post('col');
    //log_message('error', 'cp: '.$cp);
    $results=$this->ModeloGeneral->getDatosCPEstado($cp,$col);
    //$json_data = array("data" => $results);
    echo json_encode($results);
  }

  public function getDatosCPSelect(){
    $cp =$this->input->get('cp');
    $col =$this->input->get('col');
    //log_message('error', 'cp: '.$cp);
    $results=$this->ModeloGeneral->getDatosCPEstado($cp,$col);
    //$json_data = array("data" => $results);
    echo json_encode($results);
  }

  function add_complemento_persona_fisica(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('complemento_persona_fisica',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'complemento_persona_fisica');
    }
  } 
  public function formulariopep($id,$tipo){
    $data['tipo_formulario']=1;
    $data['idperfilamiento']=$id;
    $data['idtipo_cliente']=$tipo;
    $arraywhere_1 = array('idperfilamiento'=>$id); 
    if($tipo==1){
      $tipo_cliente_p_f_m=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_m',$arraywhere_1);
      foreach ($tipo_cliente_p_f_m as $item_1) {
        $data['idtipo_cliente_p_f_m_1']=$item_1->idtipo_cliente_p_f_m;
        $data['idperfilamiento_1']=$item_1->idperfilamiento;
        $data['nombre_1']=$item_1->nombre;
        $data['apellido_paterno_1']=$item_1->apellido_paterno;
        $data['apellido_materno_1']=$item_1->apellido_materno;
        $data['nombre_identificacion_1']=$item_1->nombre_identificacion;
        $data['autoridad_emite_1']=$item_1->autoridad_emite;
        $data['numero_identificacion_1']=$item_1->numero_identificacion;
        $data['genero_1']=$item_1->genero;
        $data['fecha_nacimiento_1']=$item_1->fecha_nacimiento;
        $data['pais_nacimiento_1']=$item_1->pais_nacimiento;
        $data['pais_nacionalidad_1']=$item_1->pais_nacionalidad;
        $data['activida_o_giro_1']=$item_1->activida_o_giro;
        $data['tipo_vialidad_d_1']=$item_1->tipo_vialidad_d;
        $data['calle_d_1']=$item_1->calle_d;
        $data['no_ext_d_1']=$item_1->no_ext_d;
        $data['no_int_d_1']=$item_1->no_int_d;
        $data['colonia_d_1']=$item_1->colonia_d;
        $data['municipio_d_1']=$item_1->municipio_d;
        $data['localidad_d_1']=$item_1->localidad_d;
        //$data['r_f_c_t']=$item_1->r_f_c_t;
        //$data['estado_d_1']=$item_1->estado_d;
        if($item_1->estado_d>0){
          $estado_all=$this->ModeloCatalogos->getselectwhere('estado','id',$item_1->estado_d);
          foreach ($estado_all as $x2) {
            $data['estado_d_1']=$x2->estado;
          }
        }else{
          $data['estado_d_1']=$item_1->estado_d;
        }
        $data['cp_d_1']=$item_1->cp_d;
        $data['pais_d_1']=$item_1->pais_d;
        $data['trantadose_persona_1']=$item_1->trantadose_persona;
        $data['tipo_vialidad_t_1']=$item_1->tipo_vialidad_t;
        $data['calle_t_1']=$item_1->calle_t;
        $data['no_ext_t_1']=$item_1->no_ext_t;
        $data['no_int_t_1']=$item_1->no_int_t;
        $data['colonia_t_1']=$item_1->colonia_t;
        $data['municipio_t_1']=$item_1->municipio_t;
        $data['localidad_t_1']=$item_1->localidad_t;
        $data['estado_t_1']=$item_1->estado_t;
        $data['cp_t_1']=$item_1->cp_t;
        $data['correo_t_1']=$item_1->correo_t;
        $data['r_f_c_t_1']=$item_1->r_f_c_t;
        $data['curp_t_1']=$item_1->curp_t;
        $data['telefono_t_1']=$item_1->telefono_t;
        $data['presente_legal_1']=$item_1->presente_legal;
        $data['nombre_p_1']=$item_1->nombre_p;
        $data['apellido_paterno_p_1']=$item_1->apellido_paterno_p;
        $data['apellido_materno_p_1']=$item_1->apellido_materno_p;
        $data['nombre_identificacion_p_1']=$item_1->nombre_identificacion_p;
        $data['autoridad_emite_p_1']=$item_1->autoridad_emite_p;
        $data['numero_identificacion_p_1']=$item_1->numero_identificacion_p;
        $data['genero_p_1']=$item_1->genero_p;
        $data['tipo_vialidad_p_1']=$item_1->tipo_vialidad_p;
        $data['calle_p_1']=$item_1->calle_p;
        $data['no_ext_p_1']=$item_1->no_ext_p;
        $data['no_int_p_1']=$item_1->no_int_p;
        $data['colonia_p_1']=$item_1->colonia_p;
        $data['municipio_p_1']=$item_1->municipio_p;
        $data['localidad_p_1']=$item_1->localidad_p;
        $data['estado_p_1']=$item_1->estado_p;
        $data['cp_p_1']=$item_1->cp_p;

        $tipo_cliente_p_f_m_1_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacimiento);
        foreach ($tipo_cliente_p_f_m_1_pais_1 as $item_1_p) {
          $data['clave_1_p_n']=$item_1_p->clave;
          $data['pais_1_p_n']=$item_1_p->pais;
        }
        $clave_1_p_n_2='';
        $pais_1_p_n_2='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacionalidad);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_n_2']=$item_1_p->clave;
          $data['pais_1_p_n_2']=$item_1_p->pais;
        }
        $clave_1_p_1='';
        $pais_1_p_1='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_d);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_1']=$item_1_p->clave;
          $data['pais_1_p_1']=$item_1_p->pais;
        }

      }
    }else if($tipo==2){
      $tipo_cliente_p_f_e=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_e',$arraywhere_1);
      foreach ($tipo_cliente_p_f_e as $item_1) {
        $data['idtipo_cliente_p_f_e_1']=$item_1->idtipo_cliente_p_f_e;
        $data['idperfilamiento_1']=$item_1->idperfilamiento;
        $data['nombre_1']=$item_1->nombre;
        $data['apellido_paterno_1']=$item_1->apellido_paterno;
        $data['apellido_materno_1']=$item_1->apellido_materno;
        $data['nombre_identificacion_1']=$item_1->nombre_identificacion;
        $data['autoridad_emite_1']=$item_1->autoridad_emite;
        $data['numero_identificacion_1']=$item_1->numero_identificacion;
        $data['genero_1']=$item_1->genero;
        $data['fecha_nacimiento_1']=$item_1->fecha_nacimiento;
        $data['pais_nacimiento_1']=$item_1->pais_nacimiento;
        $data['pais_nacionalidad_1']=$item_1->pais_nacionalidad;
        //$data['activida_o_giro_1']=$item_1->activida_o_giro;
        $data['tipo_vialidad_d_1']=$item_1->tipo_vialidad_d;
        $data['calle_d_1']=$item_1->calle_d;
        $data['no_ext_d_1']=$item_1->no_ext_d;
        $data['no_int_d_1']=$item_1->no_int_d;
        $data['colonia_d_1']=$item_1->colonia_d;
        $data['municipio_d_1']=$item_1->municipio_d;
        $data['localidad_d_1']=$item_1->localidad_d;
        //$data['r_f_c_t']=$item_1->r_f_c_t;
        //$data['estado_d_1']=$item_1->estado_d;
        if($item_1->estado_d>0){
          $estado_all=$this->ModeloCatalogos->getselectwhere('estado','id',$item_1->estado_d);
          foreach ($estado_all as $x2) {
            $data['estado_d_1']=$x2->estado;
          }
        }else{
          $data['estado_d_1']=$item_1->estado_d;
        }
        $data['cp_d_1']=$item_1->cp_d;
        $data['pais_d_1']=$item_1->pais_d;
        $data['trantadose_persona_1']=$item_1->trantadose_persona;
        $data['tipo_vialidad_t_1']=$item_1->tipo_vialidad_t;
        $data['calle_t_1']=$item_1->calle_t;
        $data['no_ext_t_1']=$item_1->no_ext_t;
        $data['no_int_t_1']=$item_1->no_int_t;
        $data['colonia_t_1']=$item_1->colonia_t;
        $data['municipio_t_1']=$item_1->municipio_t;
        $data['localidad_t_1']=$item_1->localidad_t;
        $data['estado_t_1']=$item_1->estado_t;
        $data['cp_t_1']=$item_1->cp_t;
        $data['correo_t_1']=$item_1->correo_t;
        $data['r_f_c_t_1']=$item_1->r_f_c_t;
        $data['curp_t_1']=$item_1->curp_t;
        $data['telefono_t_1']=$item_1->telefono_t;

        $tipo_cliente_p_f_m_1_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacimiento);
        foreach ($tipo_cliente_p_f_m_1_pais_1 as $item_1_p) {
          $data['clave_1_p_n']=$item_1_p->clave;
          $data['pais_1_p_n']=$item_1_p->pais;
        }
        $clave_1_p_n_2='';
        $pais_1_p_n_2='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacionalidad);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_n_2']=$item_1_p->clave;
          $data['pais_1_p_n_2']=$item_1_p->pais;
        }
        $clave_1_p_1='';
        $pais_1_p_1='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_d);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_1']=$item_1_p->clave;
          $data['pais_1_p_1']=$item_1_p->pais;
        }

      }
    }
    $data['idpep']=0;
    $data['nombre_completo']='';
    $data['funcion_desempena']='';
    $data['especificar']='';
    $data['calle']='';
    $data['num_ext']='';
    $data['num_int']='';
    $data['colonia']='';
    $data['codigo_postal']='';
    $data['delegacion_municipio']='';
    $data['ciudad_poblacion']='';
    $data['estado']='';
    $data['pais']='';
    $data['lugar_nacimiento']='';
    $data['telefono']='';
    $data['fecha_nacimiento']='';
    $data['nacionalidad']='';
    $data['pais_nacionalidad']='';
    $data['genero']='';
    $data['curp']='';
    $data['email']='';
    $data['rfc_homeclave']='';
    $data['num_serie_fiel']='';
    $data['acciones_personal_moral']='';
    $data['tiempo_funcionario_pep']='';
    $data['puesto_cargo_ocupados_ultimo_anio']='';
    $data['si_esposa']='';
    $data['nombre_esposa']='';
    $data['rfc_homeclave2']='';
    $data['curp2']='';
    $data['nacionalidad2']='';
    $data['no_hijos']='';
    /*$data['nombre_hijos']='';
    $data['edad_hijos']='';
    $data['nacionalidad_hijos']='';*/
    $formulario_pep=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>0));
    foreach ($formulario_pep as $x) {
      $data['idpep']=$x->id;
      $data['idperfilamiento']=$x->idperfilamiento;
      $data['idtipo_cliente']=$x->idtipo_cliente;
      $data['nombre_completo']=$x->nombre_completo;
      $data['funcion_desempena']=$x->funcion_desempena;
      $data['especificar']=$x->especificar;
      $data['calle']=$x->calle;
      $data['num_ext']=$x->num_ext;
      $data['num_int']=$x->num_int;
      $data['colonia']=$x->colonia;
      $data['codigo_postal']=$x->codigo_postal;
      $data['delegacion_municipio']=$x->delegacion_municipio;
      $data['ciudad_poblacion']=$x->ciudad_poblacion;
      $data['estado']=$x->estado;
      $data['pais']=$x->pais;
      $data['lugar_nacimiento']=$x->lugar_nacimiento;
      $data['telefono']=$x->telefono;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['nacionalidad']=$x->nacionalidad;
      $data['pais_nacionalidad']=$x->pais_nacionalidad;
      $data['genero']=$x->genero;
      $data['curp']=$x->curp;
      $data['email']=$x->email;
      $data['rfc_homeclave']=$x->rfc_homeclave;
      $data['num_serie_fiel']=$x->num_serie_fiel;
      $data['acciones_personal_moral']=$x->acciones_personal_moral;
      $data['tiempo_funcionario_pep']=$x->tiempo_funcionario_pep;
      $data['puesto_cargo_ocupados_ultimo_anio']=$x->puesto_cargo_ocupados_ultimo_anio;
      $data['si_esposa']=$x->si_esposa;
      $data['nombre_esposa']=$x->nombre_esposa;
      $data['rfc_homeclave2']=$x->rfc_homeclave2;
      $data['curp2']=$x->curp2;
      $data['nacionalidad2']=$x->nacionalidad2;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad2);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['no_hijos']=$x->no_hijos;
      $data['nombre_hijos']=$x->nombre_hijos;
      $data['edad_hijos']=$x->edad_hijos;
      $data['nacionalidad_hijos']=$x->nacionalidad_hijos;
    }
    $data['accionista']=0;
    $formulario_pep2=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>1));
    foreach ($formulario_pep2 as $x2) {
      $data['accionista']=1;
    } 
    $data['get_pais'] = $this->ModeloCatalogos->getData('pais');
    if($data["idpep"]>0 && $data["no_hijos"]!="" && $data["no_hijos"]>0){
      $data["hijos"]=$this->ModeloCatalogos->getselectwherestatus('*','hijos_formulario_pep',array('id_cuestionariopep'=>$data["idpep"],'estatus'=>1));
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/formulario_pep',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/perfilamiento/formulario_pepjs');
  }

  public function eliminarHijo(){
    $id = $this->input->post("id");
    $this->ModeloCatalogos->updateCatalogo(array("estatus"=>0),'id',$id,'hijos_formulario_pep');
  }

  public function insert_hijos_pep(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
      $id = $DATA[$i]->id;
      $data['nombre']=$DATA[$i]->nombre;
      $data['edad']=$DATA[$i]->edad;
      $data['nacionalidad']=$DATA[$i]->nacionalidad;
      $data['id_cuestionariopep']=$DATA[$i]->id_cuestionariopep;
      if($id==0){
        $this->ModeloCatalogos->tabla_inserta('hijos_formulario_pep',$data);
      }else{
        $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'hijos_formulario_pep');
      } 
    }
  }

  public function formulario_pep($id,$tipo){
    $data['tipo_formulario']=1;
    $data['idperfilamiento']=$id;
    $data['idtipo_cliente']=$tipo;
    $arraywhere_2 = array('idperfilamiento'=>$id); 
        $tipo_cliente_p_f_e=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_e',$arraywhere_2);
        foreach ($tipo_cliente_p_f_e as $item_2) {
          $data['idtipo_cliente_p_f_e_2']=$item_2->idtipo_cliente_p_f_e;
          $data['nombre_2']=$item_2->nombre;
          $data['apellido_paterno_2']=$item_2->apellido_paterno;
          $data['apellido_materno_2']=$item_2->apellido_materno;
          $data['nombre_identificacion_2']=$item_2->nombre_identificacion;
          $data['autoridad_emite_2']=$item_2->autoridad_emite;
          $data['numero_identificacion_2']=$item_2->numero_identificacion;
          $data['genero_2']=$item_2->genero;
          $data['fecha_nacimiento_2']=$item_2->fecha_nacimiento;
          $data['pais_nacimiento_2']=$item_2->pais_nacimiento;
          $data['pais_nacionalidad_2']=$item_2->pais_nacionalidad;
          $data['actividad_o_giro_2']=$item_2->actividad_o_giro;
          $data['tipo_vialidad_d_2']=$item_2->tipo_vialidad_d;
          $data['calle_d_2']=$item_2->calle_d;
          $data['no_ext_d_2']=$item_2->no_ext_d;
          $data['no_int_d_2']=$item_2->no_int_d;
          $data['colonia_d_2']=$item_2->colonia_d;
          $data['municipio_d_2']=$item_2->municipio_d ;
          $data['localidad_d_2']=$item_2->localidad_d;
          $data['estado_d_2']='';
          $estado_all=$this->ModeloCatalogos->getselectwhere('estado','id',$item_2->estado_d);
          foreach ($estado_all as $x2) {
            $data['estado_d_2']=$x2->estado;
          }
          $data['cp_d_2']=$item_2->cp_d;
          $data['pais_d_2']=$item_2->pais_d;
          $data['trantadose_persona_2']=$item_2->trantadose_persona;
          $data['tipo_vialidad_t_2']=$item_2->tipo_vialidad_t;
          $data['calle_t_2']=$item_2->calle_t;
          $data['no_ext_t_2']=$item_2->no_ext_t;
          $data['no_int_t_2']=$item_2->no_int_t;
          $data['colonia_t_2']=$item_2->colonia_t;
          $data['municipio_t_2']=$item_2->municipio_t;
          $data['localidad_t_2']=$item_2->localidad_t;
          $data['estado_t_2']=$item_2->estado_t;
          $data['cp_t_2']=$item_2->cp_t;
          $data['correo_t_2']=$item_2->correo_t;
          $data['r_f_c_t_2']=$item_2->r_f_c_t;
          $data['curp_t_2']=$item_2->curp_t;
          $data['telefono_t_2']=$item_2->telefono_t;

          $data['clave_2_p_n']='';
          $data['pais_2_p_n']='';
          $tipo_cliente_p_f_e_2_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_2->pais_nacimiento);
          foreach ($tipo_cliente_p_f_e_2_pais_1 as $item_2_p) {
            $data['clave_2_p_n']=$item_2_p->clave;
            $data['pais_2_p_n']=$item_2_p->pais;
          }
          $data['clave_2_p_n_2']='';
          $data['pais_2_p_n_2']='';
          $tipo_cliente_p_f_e_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_2->pais_nacionalidad);
          foreach ($tipo_cliente_p_f_e_1_pais_2 as $item_1_p) {
            $data['clave_2_p_n_2']=$item_1_p->clave;
            $data['pais_2_p_n_2']=$item_1_p->pais;
          }
          $data['clave_2_p_1']='';
          $data['pais_2_p_1']='';
          $tipo_cliente_p_f_e_2_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_2->pais_d);
          foreach ($tipo_cliente_p_f_e_2_pais_2 as $item_2p) {
            $data['clave_2_p_1']=$item_2p->clave;
            $data['pais_2_p_1']=$item_2p->pais;
          }
    }

    $data['idpep']=0;
    $data['nombre_completo']='';
    $data['funcion_desempena']='';
    $data['especificar']='';
    $data['calle']='';
    $data['num_ext']='';
    $data['num_int']='';
    $data['colonia']='';
    $data['codigo_postal']='';
    $data['delegacion_municipio']='';
    $data['ciudad_poblacion']='';
    $data['estado']='';
    $data['pais']='';
    $data['lugar_nacimiento']='';
    $data['telefono']='';
    $data['fecha_nacimiento']='';
    $data['nacionalidad']='';
    $data['pais_nacionalidad']='';
    $data['genero']='';
    $data['curp']='';
    $data['email']='';
    $data['rfc_homeclave']='';
    $data['num_serie_fiel']='';
    $data['acciones_personal_moral']='';
    $data['tiempo_funcionario_pep']='';
    $data['puesto_cargo_ocupados_ultimo_anio']='';
    $data['nombre_esposa']='';
    $data['rfc_homeclave2']='';
    $data['curp2']='';
    $data['nacionalidad2']='';
    $data['no_hijos']='';
    $data['nombre_hijos']='';
    $data['edad_hijos']='';
    $data['nacionalidad_hijos']='';
    $formulario_pep=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>0));
    foreach ($formulario_pep as $x) {
      $data['idpep']=$x->id;
      $data['idperfilamiento']=$x->idperfilamiento;
      $data['idtipo_cliente']=$x->idtipo_cliente;
      $data['nombre_completo']=$x->nombre_completo;
      $data['funcion_desempena']=$x->funcion_desempena;
      $data['especificar']=$x->especificar;
      $data['calle']=$x->calle;
      $data['num_ext']=$x->num_ext;
      $data['num_int']=$x->num_int;
      $data['colonia']=$x->colonia;
      $data['codigo_postal']=$x->codigo_postal;
      $data['delegacion_municipio']=$x->delegacion_municipio;
      $data['ciudad_poblacion']=$x->ciudad_poblacion;
      $data['estado']=$x->estado;
      $data['pais']=$x->pais;
      $data['lugar_nacimiento']=$x->lugar_nacimiento;
      $data['telefono']=$x->telefono;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['nacionalidad']=$x->nacionalidad;
      $data['pais_nacionalidad']=$x->pais_nacionalidad;
      $data['genero']=$x->genero;
      $data['curp']=$x->curp;
      $data['email']=$x->email;
      $data['rfc_homeclave']=$x->rfc_homeclave;
      $data['num_serie_fiel']=$x->num_serie_fiel;
      $data['acciones_personal_moral']=$x->acciones_personal_moral;
      $data['tiempo_funcionario_pep']=$x->tiempo_funcionario_pep;
      $data['puesto_cargo_ocupados_ultimo_anio']=$x->puesto_cargo_ocupados_ultimo_anio;
      $data['nombre_esposa']=$x->nombre_esposa;
      $data['rfc_homeclave2']=$x->rfc_homeclave2;
      $data['curp2']=$x->curp2;
      $data['nacionalidad2']=$x->nacionalidad2;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad2);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['no_hijos']=$x->no_hijos;
      $data['nombre_hijos']=$x->nombre_hijos;
      $data['edad_hijos']=$x->edad_hijos;
      $data['nacionalidad_hijos']=$x->nacionalidad_hijos;
    }
    $data['accionista']=0;
    $formulario_pep2=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>1));
    foreach ($formulario_pep2 as $x2) {
      $data['accionista']=1;
    } 

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/formulario_pep2',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/perfilamiento/formulario_pepjs');
  }

  function add_formulario_pep(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $id=$this->ModeloCatalogos->tabla_inserta('formulario_pep',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'formulario_pep');
    }
    echo $id;
  } 

  function add_complemento_persona_moral(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('complemento_persona_moral',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'complemento_persona_moral');
    }
  } 

  public function formulariopep_moral($id,$tipo){
    $data['tipo_formulario']=2;
    $data['idperfilamiento']=$id;
    $data['idtipo_cliente']=$tipo;
    
    $data['idpep']=0;
    $data['nombre_completo']='';
    $data['funcion_desempena']='';
    $data['especificar']='';
    $data['calle']='';
    $data['num_ext']='';
    $data['num_int']='';
    $data['colonia']='';
    $data['codigo_postal']='';
    $data['delegacion_municipio']='';
    $data['ciudad_poblacion']='';
    $data['estado']='';
    $data['pais']='';
    $data['lugar_nacimiento']='';
    $data['telefono']='';
    $data['fecha_nacimiento']='';
    $data['nacionalidad']='';
    $data['pais_nacionalidad']='';
    $data['genero']='';
    $data['curp']='';
    $data['email']='';
    $data['rfc_homeclave']='';
    $data['num_serie_fiel']='';
    $data['acciones_personal_moral']='';
    $data['tiempo_funcionario_pep']='';
    $data['puesto_cargo_ocupados_ultimo_anio']='';
    $data['nombre_esposa']='';
    $data['rfc_homeclave2']='';
    $data['curp2']='';
    $data['nacionalidad2']='';
    $data['no_hijos']='';
    $data['nombre_hijos']='';
    $data['edad_hijos']='';
    $data['nacionalidad_hijos']='';
    $data['estado_all']=$this->ModeloCatalogos->getData('estado');
    $arraywhere = array('idperfilamiento'=>$id,'accionista'=>0);
    $formulario_pep=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',$arraywhere);
    foreach ($formulario_pep as $x) {
      $data['idpep']=$x->id;
      $data['idperfilamiento']=$x->idperfilamiento;
      $data['idtipo_cliente']=$x->idtipo_cliente;
      $data['nombre_completo']=$x->nombre_completo;
      $data['funcion_desempena']=$x->funcion_desempena;
      $data['especificar']=$x->especificar;
      $data['calle']=$x->calle;
      $data['num_ext']=$x->num_ext;
      $data['num_int']=$x->num_int;
      $data['colonia']=$x->colonia;
      $data['codigo_postal']=$x->codigo_postal;
      $data['delegacion_municipio']=$x->delegacion_municipio;
      $data['ciudad_poblacion']=$x->ciudad_poblacion;
      $data['estado']=$x->estado;
      $data['pais']=$x->pais;
      $pais_1_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais);
      foreach ($pais_1_get as $x1) {
        $data['clave1']=$x1->clave;
        $data['pais1']=$x1->pais;
      }
      $data['lugar_nacimiento']=$x->lugar_nacimiento;
      $data['telefono']=$x->telefono;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['nacionalidad']=$x->nacionalidad;
      $pais_2_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad);
      foreach ($pais_2_get as $x1) {
        $data['clave2']=$x1->clave;
        $data['pais2']=$x1->pais;
      }
      $data['pais_nacionalidad']=$x->pais_nacionalidad;
      $pais_3_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais_nacionalidad);
      foreach ($pais_3_get as $x1) {
        $data['clave3']=$x1->clave;
        $data['pais3']=$x1->pais;
      }
      $data['genero']=$x->genero;
      $data['curp']=$x->curp;
      $data['email']=$x->email;
      $data['rfc_homeclave']=$x->rfc_homeclave;
      $data['num_serie_fiel']=$x->num_serie_fiel;
      $data['acciones_personal_moral']=$x->acciones_personal_moral;
      $data['tiempo_funcionario_pep']=$x->tiempo_funcionario_pep;
      $data['puesto_cargo_ocupados_ultimo_anio']=$x->puesto_cargo_ocupados_ultimo_anio;
      $data['nombre_esposa']=$x->nombre_esposa;
      $data['rfc_homeclave2']=$x->rfc_homeclave2;
      $data['curp2']=$x->curp2;
      $data['nacionalidad2']=$x->nacionalidad2;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad2);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['no_hijos']=$x->no_hijos;
      $data['nombre_hijos']=$x->nombre_hijos;
      $data['edad_hijos']=$x->edad_hijos;
      $data['nacionalidad_hijos']=$x->nacionalidad_hijos;

    }
    $data['accionista']=0;
    $formulario_pep2=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>1));
    foreach ($formulario_pep2 as $x2) {
      $data['accionista']=1;
    } 
    $get_resp=$this->General_model->get_tableRow("complemento_persona_moral",array('idperfilamiento'=>$id));
    if(isset($get_resp)){
      $data["hay_acc_pep"]=$get_resp->empresa_persona_moral_hay_algun_apoderado_legal;
    }
    //hacer multiples hijos
    if($data["idpep"]>0 && $data["no_hijos"]!="" && $data["no_hijos"]>0){
      $data["hijos"]=$this->ModeloCatalogos->getselectwherestatus('*','hijos_formulario_pepmoral',array('id_cuestionariopep'=>$data["idpep"],'estatus'=>1));
    }
    if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}  
    if($tipo==5)
      $info=$this->General_model->get_tableRow($tabla,array("idperfilamiento"=>$id));
    else
      $info=$this->General_model->get_tableRow($tabla,array($idt=>$id));

    $denominacion=""; $nombre="";
    if($tipo==3 || $tipo==4 || $tipo==5 || $tipo==6){
      if(isset($info)){
        $nombre = $info->nombre_g." ".$info->apellido_paterno_g." ".$info->apellido_materno_g;
      }
      if($tipo==3 && isset($info)){
        $denominacion=$info->razon_social;
      }
      else if($tipo==4 && isset($info)){
        $denominacion = $info->nombre_persona;
      }
      else if($tipo==5 && isset($info) || $tipo==6 && isset($info)){
        $denominacion = $info->denominacion;
      }
    } 
    //log_message('error', 'denominacion: '.$denominacion);
    $data["denominacion"]=$denominacion;
    $data["nombre"]=$nombre;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/formulario_pep_moral',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/perfilamiento/formulario_pepmoraljs');
  }

  function add_formulariopep_moral(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('formulario_pep',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'formulario_pep');
    }
  } 

  public function eliminarHijoPEPMoral(){
    $id = $this->input->post("id");
    $this->ModeloCatalogos->updateCatalogo(array("estatus"=>0),'id',$id,'hijos_formulario_pepmoral');
  }

  public function insert_hijos_pep_moral(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
      $id = $DATA[$i]->id;
      $data['nombre']=$DATA[$i]->nombre;
      $data['edad']=$DATA[$i]->edad;
      $data['nacionalidad']=$DATA[$i]->nacionalidad;
      $data['id_cuestionariopep']=$DATA[$i]->id_cuestionariopep;
      if($id==0){
        $this->ModeloCatalogos->tabla_inserta('hijos_formulario_pepmoral',$data);
      }else{
        $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'hijos_formulario_pepmoral');
      } 
    }
  }

  public function formulariopep_accionista($id,$tipo){
    $data['idperfilamiento']=$id;
    $data['idtipo_cliente']=$tipo;

    $data['idpep']=0;
    $data['nombre_completo']='';
    $data['funcion_desempena']='';
    $data['especificar']='';
    $data['calle']='';
    $data['num_ext']='';
    $data['num_int']='';
    $data['colonia']='';
    $data['codigo_postal']='';
    $data['delegacion_municipio']='';
    $data['ciudad_poblacion']='';
    $data['estado']='';
    $data['pais']='';
    $data['lugar_nacimiento']='';
    $data['telefono']='';
    $data['fecha_nacimiento']='';
    $data['nacionalidad']='';
    $data['pais_nacionalidad']='';
    $data['genero']='';
    $data['curp']='';
    $data['email']='';
    $data['rfc_homeclave']='';
    $data['num_serie_fiel']='';
    $data['acciones_personal_moral']='';
    $data['tiempo_funcionario_pep']='';
    $data['puesto_cargo_ocupados_ultimo_anio']='';
    $data['nombre_esposa']='';
    $data['rfc_homeclave2']='';
    $data['curp2']='';
    $data['nacionalidad2']='';
    $data['no_hijos']='';
    $data['nombre_hijos']='';
    $data['edad_hijos']='';
    $data['nacionalidad_hijos']='';
    $data['estado_all']=$this->ModeloCatalogos->getData('estado');
    $arraywhere = array('idperfilamiento'=>$id,'accionista'=>1);
    $formulario_pep=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',$arraywhere);
    foreach ($formulario_pep as $x) {
      $data['idpep']=$x->id;
      $data['idperfilamiento']=$x->idperfilamiento;
      $data['idtipo_cliente']=$x->idtipo_cliente;
      $data['nombre_completo']=$x->nombre_completo;
      $data['funcion_desempena']=$x->funcion_desempena;
      $data['especificar']=$x->especificar;
      $data['calle']=$x->calle;
      $data['num_ext']=$x->num_ext;
      $data['num_int']=$x->num_int;
      $data['colonia']=$x->colonia;
      $data['codigo_postal']=$x->codigo_postal;
      $data['delegacion_municipio']=$x->delegacion_municipio;
      $data['ciudad_poblacion']=$x->ciudad_poblacion;
      $data['estado']=$x->estado;
      $data['pais']=$x->pais;
      $pais_1_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais);
      foreach ($pais_1_get as $x1) {
        $data['clave1']=$x1->clave;
        $data['pais1']=$x1->pais;
      }
      $data['lugar_nacimiento']=$x->lugar_nacimiento;
      $data['telefono']=$x->telefono;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['nacionalidad']=$x->nacionalidad;
      $pais_2_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad);
      foreach ($pais_2_get as $x1) {
        $data['clave2']=$x1->clave;
        $data['pais2']=$x1->pais;
      }
      $data['pais_nacionalidad']=$x->pais_nacionalidad;
      $pais_3_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais_nacionalidad);
      foreach ($pais_3_get as $x1) {
        $data['clave3']=$x1->clave;
        $data['pais3']=$x1->pais;
      }
      $data['genero']=$x->genero;
      $data['curp']=$x->curp;
      $data['email']=$x->email;
      $data['rfc_homeclave']=$x->rfc_homeclave;
      $data['num_serie_fiel']=$x->num_serie_fiel;
      $data['acciones_personal_moral']=$x->acciones_personal_moral;
      $data['tiempo_funcionario_pep']=$x->tiempo_funcionario_pep;
      $data['puesto_cargo_ocupados_ultimo_anio']=$x->puesto_cargo_ocupados_ultimo_anio;
      $data['nombre_esposa']=$x->nombre_esposa;
      $data['rfc_homeclave2']=$x->rfc_homeclave2;
      $data['curp2']=$x->curp2;
      $data['nacionalidad2']=$x->nacionalidad2;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad2);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['no_hijos']=$x->no_hijos;
      $data['nombre_hijos']=$x->nombre_hijos;
      $data['edad_hijos']=$x->edad_hijos;
      $data['nacionalidad_hijos']=$x->nacionalidad_hijos;
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/perfilamiento/formulario_pep_accionista',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/perfilamiento/formulario_pep_accionistajs');
  }

  public function generarPDF_PEPAccion($id,$tipo){
    $data['idperfilamiento']=$id;
    $data['idtipo_cliente']=$tipo;

    $data['idpep']=0;
    $data['nombre_completo']='';
    $data['funcion_desempena']='';
    $data['especificar']='';
    $data['calle']='';
    $data['num_ext']='';
    $data['num_int']='';
    $data['colonia']='';
    $data['codigo_postal']='';
    $data['delegacion_municipio']='';
    $data['ciudad_poblacion']='';
    $data['estado']='';
    $data['pais']='';
    $data['lugar_nacimiento']='';
    $data['telefono']='';
    $data['fecha_nacimiento']='';
    $data['nacionalidad']='';
    $data['pais_nacionalidad']='';
    $data['genero']='';
    $data['curp']='';
    $data['email']='';
    $data['rfc_homeclave']='';
    $data['num_serie_fiel']='';
    $data['acciones_personal_moral']='';
    $data['tiempo_funcionario_pep']='';
    $data['puesto_cargo_ocupados_ultimo_anio']='';
    $data['nombre_esposa']='';
    $data['rfc_homeclave2']='';
    $data['curp2']='';
    $data['nacionalidad2']='';
    $data['no_hijos']='';
    $data['nombre_hijos']='';
    $data['edad_hijos']='';
    $data['nacionalidad_hijos']='';
    $data['estado_all']=$this->ModeloCatalogos->getData('estado');
    $arraywhere = array('idperfilamiento'=>$id,'accionista'=>1);
    $formulario_pep=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',$arraywhere);
    foreach ($formulario_pep as $x) {
      $data['idpep']=$x->id;
      $data['idperfilamiento']=$x->idperfilamiento;
      $data['idtipo_cliente']=$x->idtipo_cliente;
      $data['nombre_completo']=$x->nombre_completo;
      $data['funcion_desempena']=$x->funcion_desempena;
      $data['especificar']=$x->especificar;
      $data['calle']=$x->calle;
      $data['num_ext']=$x->num_ext;
      $data['num_int']=$x->num_int;
      $data['colonia']=$x->colonia;
      $data['codigo_postal']=$x->codigo_postal;
      $data['delegacion_municipio']=$x->delegacion_municipio;
      $data['ciudad_poblacion']=$x->ciudad_poblacion;
      $data['estado']=$x->estado;
      $data['pais']=$x->pais;
      $get_edo=$this->ModeloCatalogos->getselectwhere('estado','id',$x->estado);
      foreach ($get_edo as $x1) {
        $data['estado']=$x1->estado;
      }
      $pais_1_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais);
      foreach ($pais_1_get as $x1) {
        $data['clave1']=$x1->clave;
        $data['pais1']=$x1->pais;
      }
      $data['lugar_nacimiento']=$x->lugar_nacimiento;
      $data['telefono']=$x->telefono;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['nacionalidad']=$x->nacionalidad;
      $pais_2_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad);
      foreach ($pais_2_get as $x1) {
        $data['clave2']=$x1->clave;
        $data['pais2']=$x1->pais;
      }
      $data['pais_nacionalidad']=$x->pais_nacionalidad;
      $pais_3_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais_nacionalidad);
      foreach ($pais_3_get as $x1) {
        $data['clave3']=$x1->clave;
        $data['pais3']=$x1->pais;
      }
      $data['genero']=$x->genero;
      $data['curp']=$x->curp;
      $data['email']=$x->email;
      $data['rfc_homeclave']=$x->rfc_homeclave;
      $data['num_serie_fiel']=$x->num_serie_fiel;
      $data['acciones_personal_moral']=$x->acciones_personal_moral;
      $data['tiempo_funcionario_pep']=$x->tiempo_funcionario_pep;
      $data['puesto_cargo_ocupados_ultimo_anio']=$x->puesto_cargo_ocupados_ultimo_anio;
      $data['nombre_esposa']=$x->nombre_esposa;
      $data['rfc_homeclave2']=$x->rfc_homeclave2;
      $data['curp2']=$x->curp2;
      $data['nacionalidad2']=$x->nacionalidad2;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad2);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['no_hijos']=$x->no_hijos;
      $data['nombre_hijos']=$x->nombre_hijos;
      $data['edad_hijos']=$x->edad_hijos;
      $data['nacionalidad_hijos']=$x->nacionalidad_hijos;
    }

    $this->load->view('Reportes/formato_pep_accionista',$data);
  }

  public function generarPDF_FPEPM($id,$tipo){
    $data['tipo_formulario']=2;
    $data['idperfilamiento']=$id;
    $data['idtipo_cliente']=$tipo;
    
    $data['idpep']=0;
    $data['nombre_completo']='';
    $data['funcion_desempena']='';
    $data['especificar']='';
    $data['calle']='';
    $data['num_ext']='';
    $data['num_int']='';
    $data['colonia']='';
    $data['codigo_postal']='';
    $data['delegacion_municipio']='';
    $data['ciudad_poblacion']='';
    $data['estado']='';
    $data['pais']='';
    $data['lugar_nacimiento']='';
    $data['telefono']='';
    $data['fecha_nacimiento']='';
    $data['nacionalidad']='';
    $data['pais_nacionalidad']='';
    $data['genero']='';
    $data['curp']='';
    $data['email']='';
    $data['rfc_homeclave']='';
    $data['num_serie_fiel']='';
    $data['acciones_personal_moral']='';
    $data['tiempo_funcionario_pep']='';
    $data['puesto_cargo_ocupados_ultimo_anio']='';
    $data['nombre_esposa']='';
    $data['rfc_homeclave2']='';
    $data['curp2']='';
    $data['nacionalidad2']='';
    $data['no_hijos']='';
    $data['nombre_hijos']='';
    $data['edad_hijos']='';
    $data['nacionalidad_hijos']='';
    $data['estado_all']=$this->ModeloCatalogos->getData('estado');
    $arraywhere = array('idperfilamiento'=>$id,'accionista'=>0);
    $formulario_pep=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',$arraywhere);
    foreach ($formulario_pep as $x) {
      $data['idpep']=$x->id;
      $data['idperfilamiento']=$x->idperfilamiento;
      $data['idtipo_cliente']=$x->idtipo_cliente;
      $data['nombre_completo']=$x->nombre_completo;
      $data['funcion_desempena']=$x->funcion_desempena;
      $data['especificar']=$x->especificar;
      $data['calle']=$x->calle;
      $data['num_ext']=$x->num_ext;
      $data['num_int']=$x->num_int;
      $data['colonia']=$x->colonia;
      $data['codigo_postal']=$x->codigo_postal;
      $data['delegacion_municipio']=$x->delegacion_municipio;
      $data['ciudad_poblacion']=$x->ciudad_poblacion;
      $data['estado']=$x->estado;
      $data['pais']=$x->pais;
      $pais_1_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais);
      foreach ($pais_1_get as $x1) {
        $data['clave1']=$x1->clave;
        $data['pais1']=$x1->pais;
      }
      $data['lugar_nacimiento']=$x->lugar_nacimiento;
      $data['telefono']=$x->telefono;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['nacionalidad']=$x->nacionalidad;
      $pais_2_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad);
      foreach ($pais_2_get as $x1) {
        $data['clave2']=$x1->clave;
        $data['pais2']=$x1->pais;
      }
      $data['pais_nacionalidad']=$x->pais_nacionalidad;
      $pais_3_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->pais_nacionalidad);
      foreach ($pais_3_get as $x1) {
        $data['clave3']=$x1->clave;
        $data['pais3']=$x1->pais;
      }

      $get_edo=$this->ModeloCatalogos->getselectwhere('estado','id',$x->estado);
      foreach ($get_edo as $x1) {
        $data['estado']=$x1->estado;
      }
      $data['genero']=$x->genero;
      $data['curp']=$x->curp;
      $data['email']=$x->email;
      $data['rfc_homeclave']=$x->rfc_homeclave;
      $data['num_serie_fiel']=$x->num_serie_fiel;
      $data['acciones_personal_moral']=$x->acciones_personal_moral;
      $data['tiempo_funcionario_pep']=$x->tiempo_funcionario_pep;
      $data['puesto_cargo_ocupados_ultimo_anio']=$x->puesto_cargo_ocupados_ultimo_anio;
      $data['nombre_esposa']=$x->nombre_esposa;
      $data['rfc_homeclave2']=$x->rfc_homeclave2;
      $data['curp2']=$x->curp2;
      $data['nacionalidad2']=$x->nacionalidad2;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad2);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['no_hijos']=$x->no_hijos;
      $data['nombre_hijos']=$x->nombre_hijos;
      $data['edad_hijos']=$x->edad_hijos;
      $data['nacionalidad_hijos']=$x->nacionalidad_hijos;

    }
    $data['accionista']=0;
    $formulario_pep2=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>1));
    foreach ($formulario_pep2 as $x2) {
      $data['accionista']=1;
    } 
    $get_resp=$this->General_model->get_tableRow("complemento_persona_moral",array('idperfilamiento'=>$id));
    if(isset($get_resp)){
      $data["hay_acc_pep"]=$get_resp->empresa_persona_moral_hay_algun_apoderado_legal;
    }
    //hacer multiples hijos
    if($data["idpep"]>0 && $data["no_hijos"]!="" && $data["no_hijos"]>0){
      $data["hijos"]=$this->ModeloCatalogos->getselectwherestatus('*','hijos_formulario_pepmoral',array('id_cuestionariopep'=>$data["idpep"],'estatus'=>1));
    }
    
    $this->load->view('Reportes/formato_pep_moral',$data);

  }

  public function generarPDF_FPEPPF($id,$tipo){
    $data['tipo_formulario']=1;
    $data['idperfilamiento']=$id;
    $data['idtipo_cliente']=$tipo;
    $arraywhere_1 = array('idperfilamiento'=>$id); 
    if($tipo==1){
      $tipo_cliente_p_f_m=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_m',$arraywhere_1);
      foreach ($tipo_cliente_p_f_m as $item_1) {
        $data['idtipo_cliente_p_f_m_1']=$item_1->idtipo_cliente_p_f_m;
        $data['idperfilamiento_1']=$item_1->idperfilamiento;
        $data['nombre_1']=$item_1->nombre;
        $data['apellido_paterno_1']=$item_1->apellido_paterno;
        $data['apellido_materno_1']=$item_1->apellido_materno;
        $data['nombre_identificacion_1']=$item_1->nombre_identificacion;
        $data['autoridad_emite_1']=$item_1->autoridad_emite;
        $data['numero_identificacion_1']=$item_1->numero_identificacion;
        $data['genero_1']=$item_1->genero;
        $data['fecha_nacimiento_1']=$item_1->fecha_nacimiento;
        $data['pais_nacimiento_1']=$item_1->pais_nacimiento;
        $data['pais_nacionalidad_1']=$item_1->pais_nacionalidad;
        $data['activida_o_giro_1']=$item_1->activida_o_giro;
        $data['tipo_vialidad_d_1']=$item_1->tipo_vialidad_d;
        $data['calle_d_1']=$item_1->calle_d;
        $data['no_ext_d_1']=$item_1->no_ext_d;
        $data['no_int_d_1']=$item_1->no_int_d;
        $data['colonia_d_1']=$item_1->colonia_d;
        $data['municipio_d_1']=$item_1->municipio_d;
        $data['localidad_d_1']=$item_1->localidad_d;
        //$data['r_f_c_t']=$item_1->r_f_c_t;
        //$data['estado_d_1']=$item_1->estado_d;
        if($item_1->estado_d>0){
          $estado_all=$this->ModeloCatalogos->getselectwhere('estado','id',$item_1->estado_d);
          foreach ($estado_all as $x2) {
            $data['estado_d_1']=$x2->estado;
          }
        }else{
          $data['estado_d_1']=$item_1->estado_d;
        }
        $data['cp_d_1']=$item_1->cp_d;
        $data['pais_d_1']=$item_1->pais_d;
        $data['trantadose_persona_1']=$item_1->trantadose_persona;
        $data['tipo_vialidad_t_1']=$item_1->tipo_vialidad_t;
        $data['calle_t_1']=$item_1->calle_t;
        $data['no_ext_t_1']=$item_1->no_ext_t;
        $data['no_int_t_1']=$item_1->no_int_t;
        $data['colonia_t_1']=$item_1->colonia_t;
        $data['municipio_t_1']=$item_1->municipio_t;
        $data['localidad_t_1']=$item_1->localidad_t;
        $data['estado_t_1']=$item_1->estado_t;
        $data['cp_t_1']=$item_1->cp_t;
        $data['correo_t_1']=$item_1->correo_t;
        $data['r_f_c_t_1']=$item_1->r_f_c_t;
        $data['curp_t_1']=$item_1->curp_t;
        $data['telefono_t_1']=$item_1->telefono_t;
        $data['presente_legal_1']=$item_1->presente_legal;
        $data['nombre_p_1']=$item_1->nombre_p;
        $data['apellido_paterno_p_1']=$item_1->apellido_paterno_p;
        $data['apellido_materno_p_1']=$item_1->apellido_materno_p;
        $data['nombre_identificacion_p_1']=$item_1->nombre_identificacion_p;
        $data['autoridad_emite_p_1']=$item_1->autoridad_emite_p;
        $data['numero_identificacion_p_1']=$item_1->numero_identificacion_p;
        $data['genero_p_1']=$item_1->genero_p;
        $data['tipo_vialidad_p_1']=$item_1->tipo_vialidad_p;
        $data['calle_p_1']=$item_1->calle_p;
        $data['no_ext_p_1']=$item_1->no_ext_p;
        $data['no_int_p_1']=$item_1->no_int_p;
        $data['colonia_p_1']=$item_1->colonia_p;
        $data['municipio_p_1']=$item_1->municipio_p;
        $data['localidad_p_1']=$item_1->localidad_p;
        $data['estado_p_1']=$item_1->estado_p;
        $data['cp_p_1']=$item_1->cp_p;

        $tipo_cliente_p_f_m_1_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacimiento);
        foreach ($tipo_cliente_p_f_m_1_pais_1 as $item_1_p) {
          $data['clave_1_p_n']=$item_1_p->clave;
          $data['pais_1_p_n']=$item_1_p->pais;
        }
        $clave_1_p_n_2='';
        $pais_1_p_n_2='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacionalidad);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_n_2']=$item_1_p->clave;
          $data['pais_1_p_n_2']=$item_1_p->pais;
        }
        $clave_1_p_1='';
        $pais_1_p_1='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_d);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_1']=$item_1_p->clave;
          $data['pais_1_p_1']=$item_1_p->pais;
        }

      }
    }else if($tipo==2){
      $tipo_cliente_p_f_e=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_e',$arraywhere_1);
      foreach ($tipo_cliente_p_f_e as $item_1) {
        $data['idtipo_cliente_p_f_e_1']=$item_1->idtipo_cliente_p_f_e;
        $data['idperfilamiento_1']=$item_1->idperfilamiento;
        $data['nombre_1']=$item_1->nombre;
        $data['apellido_paterno_1']=$item_1->apellido_paterno;
        $data['apellido_materno_1']=$item_1->apellido_materno;
        $data['nombre_identificacion_1']=$item_1->nombre_identificacion;
        $data['autoridad_emite_1']=$item_1->autoridad_emite;
        $data['numero_identificacion_1']=$item_1->numero_identificacion;
        $data['genero_1']=$item_1->genero;
        $data['fecha_nacimiento_1']=$item_1->fecha_nacimiento;
        $data['pais_nacimiento_1']=$item_1->pais_nacimiento;
        $data['pais_nacionalidad_1']=$item_1->pais_nacionalidad;
        //$data['activida_o_giro_1']=$item_1->activida_o_giro;
        $data['tipo_vialidad_d_1']=$item_1->tipo_vialidad_d;
        $data['calle_d_1']=$item_1->calle_d;
        $data['no_ext_d_1']=$item_1->no_ext_d;
        $data['no_int_d_1']=$item_1->no_int_d;
        $data['colonia_d_1']=$item_1->colonia_d;
        $data['municipio_d_1']=$item_1->municipio_d;
        $data['localidad_d_1']=$item_1->localidad_d;
        //$data['r_f_c_t']=$item_1->r_f_c_t;
        //$data['estado_d_1']=$item_1->estado_d;
        if($item_1->estado_d>0){
          $estado_all=$this->ModeloCatalogos->getselectwhere('estado','id',$item_1->estado_d);
          foreach ($estado_all as $x2) {
            $data['estado_d_1']=$x2->estado;
          }
        }else{
          $data['estado_d_1']=$item_1->estado_d;
        }
        $data['cp_d_1']=$item_1->cp_d;
        $data['pais_d_1']=$item_1->pais_d;
        $data['trantadose_persona_1']=$item_1->trantadose_persona;
        $data['tipo_vialidad_t_1']=$item_1->tipo_vialidad_t;
        $data['calle_t_1']=$item_1->calle_t;
        $data['no_ext_t_1']=$item_1->no_ext_t;
        $data['no_int_t_1']=$item_1->no_int_t;
        $data['colonia_t_1']=$item_1->colonia_t;
        $data['municipio_t_1']=$item_1->municipio_t;
        $data['localidad_t_1']=$item_1->localidad_t;
        $data['estado_t_1']=$item_1->estado_t;
        $data['cp_t_1']=$item_1->cp_t;
        $data['correo_t_1']=$item_1->correo_t;
        $data['r_f_c_t_1']=$item_1->r_f_c_t;
        $data['curp_t_1']=$item_1->curp_t;
        $data['telefono_t_1']=$item_1->telefono_t;

        $tipo_cliente_p_f_m_1_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacimiento);
        foreach ($tipo_cliente_p_f_m_1_pais_1 as $item_1_p) {
          $data['clave_1_p_n']=$item_1_p->clave;
          $data['pais_1_p_n']=$item_1_p->pais;
        }
        $clave_1_p_n_2='';
        $pais_1_p_n_2='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_nacionalidad);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_n_2']=$item_1_p->clave;
          $data['pais_1_p_n_2']=$item_1_p->pais;
        }
        $clave_1_p_1='';
        $pais_1_p_1='';
        $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$item_1->pais_d);
        foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
          $data['clave_1_p_1']=$item_1_p->clave;
          $data['pais_1_p_1']=$item_1_p->pais;
        }

      }
    }
    $data['idpep']=0;
    $data['nombre_completo']='';
    $data['funcion_desempena']='';
    $data['especificar']='';
    $data['calle']='';
    $data['num_ext']='';
    $data['num_int']='';
    $data['colonia']='';
    $data['codigo_postal']='';
    $data['delegacion_municipio']='';
    $data['ciudad_poblacion']='';
    $data['estado']='';
    $data['pais']='';
    $data['lugar_nacimiento']='';
    $data['telefono']='';
    $data['fecha_nacimiento']='';
    $data['nacionalidad']='';
    $data['pais_nacionalidad']='';
    $data['genero']='';
    $data['curp']='';
    $data['email']='';
    $data['rfc_homeclave']='';
    $data['num_serie_fiel']='';
    $data['acciones_personal_moral']='';
    $data['tiempo_funcionario_pep']='';
    $data['puesto_cargo_ocupados_ultimo_anio']='';
    $data['si_esposa']='';
    $data['nombre_esposa']='';
    $data['rfc_homeclave2']='';
    $data['curp2']='';
    $data['pais4']='';
    $data['nacionalidad2']='';
    $data['no_hijos']='';
    $data['nombre_hijos']='';
    $data['edad_hijos']='';
    $data['nacionalidad_hijos']='';
    $formulario_pep=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>0));
    foreach ($formulario_pep as $x) {
      $data['idpep']=$x->id;
      $data['idperfilamiento']=$x->idperfilamiento;
      $data['idtipo_cliente']=$x->idtipo_cliente;
      $data['nombre_completo']=$x->nombre_completo;
      $data['funcion_desempena']=$x->funcion_desempena;
      $data['especificar']=$x->especificar;
      $data['calle']=$x->calle;
      $data['num_ext']=$x->num_ext;
      $data['num_int']=$x->num_int;
      $data['colonia']=$x->colonia;
      $data['codigo_postal']=$x->codigo_postal;
      $data['delegacion_municipio']=$x->delegacion_municipio;
      $data['ciudad_poblacion']=$x->ciudad_poblacion;
      $data['estado']=$x->estado;
      $data['pais']=$x->pais;
      $data['lugar_nacimiento']=$x->lugar_nacimiento;
      $data['telefono']=$x->telefono;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['nacionalidad']=$x->nacionalidad;
      $data['pais_nacionalidad']=$x->pais_nacionalidad;
      $data['genero']=$x->genero;
      $data['curp']=$x->curp;
      $data['email']=$x->email;
      $data['rfc_homeclave']=$x->rfc_homeclave;
      $data['num_serie_fiel']=$x->num_serie_fiel;
      $data['acciones_personal_moral']=$x->acciones_personal_moral;
      $data['tiempo_funcionario_pep']=$x->tiempo_funcionario_pep;
      $data['puesto_cargo_ocupados_ultimo_anio']=$x->puesto_cargo_ocupados_ultimo_anio;
      $data['si_esposa']=$x->si_esposa;
      $data['nombre_esposa']=$x->nombre_esposa;
      $data['rfc_homeclave2']=$x->rfc_homeclave2;
      $data['curp2']=$x->curp2;
      $data['nacionalidad2']=$x->nacionalidad2;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad2);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['no_hijos']=$x->no_hijos;
      $data['nombre_hijos']=$x->nombre_hijos;
      $data['edad_hijos']=$x->edad_hijos;
      $data['nacionalidad_hijos']=$x->nacionalidad_hijos;
    }
    $data['accionista']=0;
    $formulario_pep2=$this->ModeloCatalogos->getselectwherestatus('*','formulario_pep',array('idperfilamiento'=>$id,'accionista'=>1));
    foreach ($formulario_pep2 as $x2) {
      $data['accionista']=1;
    } 
    $data['get_pais'] = $this->ModeloCatalogos->getData('pais');
    if($data["idpep"]>0 && $data["no_hijos"]!="" && $data["no_hijos"]>0){
      $data["hijos"]=$this->ModeloCatalogos->getselectwherestatus('*','hijos_formulario_pep',array('id_cuestionariopep'=>$data["idpep"],'estatus'=>1));
    }

    $this->load->view('Reportes/formato_pep_fisica',$data);

  }

}
/* End of file Perfilamiento.php */
/* Location: ./application/controllers/Perfilamiento.php */