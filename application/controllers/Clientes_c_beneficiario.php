<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes_c_beneficiario extends CI_Controller {

	 public function __construct(){
	    parent::__construct();
	    $this->load->model('General_model');
	    $this->load->model('ModeloCliente');
	    $this->load->model('ModeloCatalogos');
	    $this->load->model('ModeloGeneral');
	  
	    if (!$this->session->userdata('logeado')){
	          redirect('/Login');
	    }else{
	        $this->perfilid=$this->session->userdata('perfilid');
	        $this->idpersonal=$this->session->userdata('idpersonal');
	        $this->idcliente=$this->session->userdata('idcliente');
	        $this->tipo=$this->session->userdata('tipo');
	        $this->status=$this->session->userdata('status');
	    }
	    date_default_timezone_set('America/Mexico_City');
      $this->fechaactual = date('Y-m-d');
      $this->horahoy = date('g:i a');
    }
    
    public function beneficiarios($id,$tipoc,$idc,$idu){
    	/*$data['idbfi']=$id;
    	$nombre = 'Rul';*/
    	if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

    	$get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$id));
    	if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
        if($tipoc==3) $nombre = $get_per->razon_social;
        if($tipoc==4) $nombre = $get_per->nombre_persona;
        if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

    	$data['nombre']=$nombre;
  		$data['id_perf'] = $id;
  		$data['idc'] = $idc;
  		$data['tipoc'] = $tipoc;
      $data['id_union'] = $idu;
		  $this->load->view('templates/header');
	    $this->load->view('templates/navbar');
	    $this->load->view('catalogos/c_c_beneficiario/list_beneficiario',$data);
	    $this->load->view('templates/footer');
      $this->load->view('catalogos/c_c_beneficiario/jslist_beneficiario'); 
    } 

    public function beneficiariosUnion($idu){
      $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$idu));
      $nombre="";
      foreach ($get_up as $k) {
        $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
        $tipoc=$get_perf->idtipo_cliente;
        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
        if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno."/ ";
        if($tipoc==3) $nombre .= $get_per->razon_social."/ ";
        if($tipoc==4) $nombre .= $get_per->nombre_persona."/ ";
        if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion."/ ";

        $data['nombre']=$nombre;
        $data['id_union'] = $idu;
      }
    
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/c_c_beneficiario/list_beneficiario_union',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/c_c_beneficiario/jslist_beneficiario_union'); 
    } 

    public function detBene($id=0,$idc,$idp,$tipoc,$idopera=0){ //fisica
      $data['get_estado'] = $this->ModeloCatalogos->getData('estado');
      $data['get_pais'] = $this->ModeloCatalogos->getData('pais');
      $data['get_actividad'] = $this->ModeloCatalogos->getData('actividad_econominica_fisica');
      $data['get_tipo_vialidad']=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      if($idopera==0){
        $data['b']=$this->General_model->get_tableRow("beneficiario_fisica",array('id_beneficiario_fisica'=>$id));  
      }else if($idopera>0){
        $data['b']=$this->General_model->get_tableRow("beneficiario_fisica",array('id'=>$id));    
      }else if($id>0)
       $data['b']=$this->General_model->get_tableRow("beneficiario_fisica",array('id'=>$id));

      $get_pp = $this->General_model->get_tableRow("perfilamiento",array("idperfilamiento"=>$data['b']->idtipo_cliente));
      $tipoccon = $get_pp->idtipo_cliente;
      if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
      if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
      if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
      if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
      if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
      if($tipoccon==6) $tabla = "tipo_cliente_f";

      $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$data['b']->idtipo_cliente));

      if($tipoccon==1 || $tipoccon==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
      if($tipoccon==3) $nombre = $get_per->razon_social;
      if($tipoccon==4) $nombre = $get_per->nombre_persona;
      if($tipoccon==5 || $tipoccon==6) $nombre = $get_per->denominacion;

      $data['nombre']=$nombre; 

      $data['idc'] = $idc;
      $data['idcc'] = $idc;
      $data['idp'] = $idp;
      $data['tipoc'] = $tipoc;
      $data['idopera'] = $idopera;


      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/c_c_beneficiario/addBeneFisica',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/c_c_beneficiario/addBeneFisicajs');
    }

    public function addBene($id=0,$idc,$idp,$tipoc,$idopera=0,$idcc=0,$union=0){ //fisica
    	$data['get_estado'] = $this->ModeloCatalogos->getData('estado');
    	$data['get_pais'] = $this->ModeloCatalogos->getData('pais');
    	$data['get_actividad'] = $this->ModeloCatalogos->getData('actividad_econominica_fisica');
    	$data['get_tipo_vialidad']=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
    	/*if($idopera==0){
    		$data['b']=$this->General_model->get_tableRow("beneficiario_fisica",array('id_beneficiario_fisica'=>$id));	
    	}else{
    		$data['b']=$this->General_model->get_tableRow("beneficiario_fisica",array('id'=>$id));		
    	}*/
      log_message('error', 'union: '.$union);
      if($id>0)
	     $data['b']=$this->General_model->get_tableRow("beneficiario_fisica",array('id'=>$id));

      if($union==0){
        if($tipoc==0){
          $get_tipoper=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
          $tipoc=$get_tipoper->idtipo_cliente;
        }
        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
        if($tipoc==3) $nombre = $get_per->razon_social;
        if($tipoc==4) $nombre = $get_per->nombre_persona;
        if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

        $data['nombre']=$nombre;  
  	    $data['idc'] = $idc;
  	    $data['idp'] = $idp;
  	    $data['tipoc'] = $tipoc;
  	    $data['idopera'] = $idopera;
        $data['idcc'] = $idcc;
      }
      $data['id_union'] = $union;
      if($union>0){
        log_message('error', 'union: '.$union);
        $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$union));
        $nombre="";
        foreach ($get_up as $k) {
          $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
          $tipoc=$get_perf->idtipo_cliente;
          if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
          if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
          if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
          if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
          if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
          if($tipoc==6) $tabla = "tipo_cliente_f";

          $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
          if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
          if($tipoc==3) $nombre .= $get_per->razon_social;
          if($tipoc==4) $nombre .= $get_per->nombre_persona;
          if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion;

          $data['nombre']=$nombre;
          $data['id_union'] = $union;

          $data['idc'] = $k->id_cliente;
          $data['idp'] = $k->id_perfilamiento;
          $data['tipoc'] = $tipoc;
          $data['idcc'] = $k->id_clientec;
          $data['idopera'] = $idopera;
        }
      }

	    $this->load->view('templates/header');
	    $this->load->view('templates/navbar');
	    $this->load->view('catalogos/c_c_beneficiario/addBeneFisica',$data);
	    $this->load->view('templates/footer');
	    $this->load->view('catalogos/c_c_beneficiario/addBeneFisicajs');
  	}

    public function generarPDF_BeneFisica($id,$idc,$idp,$tipoc,$idopera=0,$idcc=0,$union=0,$fecha=0){ //fisica
      if($fecha==0){
        $fecha = date("Y-m-d");
      }
      if($id>0)
       $data['b']=$this->General_model->get_tableRow("beneficiario_fisica",array('id'=>$id));

      if($union==0){
        if($tipoc==0){
          $get_tipoper=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
          $tipoc=$get_tipoper->idtipo_cliente;
        }
        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
        if($tipoc==3) $nombre = $get_per->razon_social;
        if($tipoc==4) $nombre = $get_per->nombre_persona;
        if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

        $data['nombre']=$nombre;  
        $data['idc'] = $idc;
        $data['idp'] = $idp;
        $data['tipoc'] = $tipoc;
        $data['idopera'] = $idopera;
        $data['idcc'] = $idcc;
      }
      $data['id_union'] = $union;
      if($union>0){
        //log_message('error', 'union: '.$union);
        $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$union));
        $nombre="";
        foreach ($get_up as $k) {
          $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
          $tipoc=$get_perf->idtipo_cliente;
          if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
          if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
          if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
          if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
          if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
          if($tipoc==6) $tabla = "tipo_cliente_f";

          $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
          if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
          if($tipoc==3) $nombre .= $get_per->razon_social;
          if($tipoc==4) $nombre .= $get_per->nombre_persona;
          if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion;

          $data['nombre']=$nombre;
          $data['id_union'] = $union;

          $data['idc'] = $k->id_cliente;
          $data['idp'] = $k->id_perfilamiento;
          $data['tipoc'] = $tipoc;
          $data['idcc'] = $k->id_clientec;
          $data['idopera'] = $idopera;
        }
      }
      $data["estado"]="";
      $gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$data['b']->estado));
      foreach ($gete as $item){
        $data["estado"]=$item->estado; 
      }
      $data["estado_d"]="";
      $gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$data['b']->estado_d));
      foreach ($gete as $item){
        $data["estado_d"]=$item->estado; 
      }
      $data["actividad_giro"]="";
      $actm=$this->ModeloCatalogos->getselectwherestatus('acitividad','actividad_econominica_fisica',array("clave"=>$data['b']->actividad_giro));
      foreach ($actm as $item2) {
        $data["actividad_giro"]=$item2->acitividad;
      }
      $data["pais_nacimiento"]="";
      $getp=$this->ModeloCatalogos->getselectwherestatus('pais as pais_nac','pais',array("clave"=>$data['b']->pais_nacimiento));
      foreach ($getp as $i){
        $data["pais_nacimiento"]=$i->pais_nac; 
        //log_message('error', 'pais_nacimiento: '.$i->pais_nac);
      }

      $data["pais_nacionalidad"]="";
      $getp=$this->ModeloCatalogos->getselectwherestatus('pais','pais',array("clave"=>$data['b']->pais_nacionalidad));
      foreach ($getp as $item){
        $data["pais_nacionalidad"]=$item->pais; 
      }
      $data["pais"]="";
      $getp=$this->ModeloCatalogos->getselectwherestatus('pais','pais',array("clave"=>$data['b']->pais));
      foreach ($getp as $item){
        $data["pais"]=$item->pais; 
      }
      $data["tipo_vialidad"]="";
      $gettv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$data['b']->tipo_vialidad));
      foreach ($gettv as $item){
        $data["tipo_vialidad"]=$item->nombre; 
      }
      $data["tipo_vialidad_d"]="";
      $gettv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$data['b']->tipo_vialidad_d));
      foreach ($gettv as $item){
        $data["tipo_vialidad_d"]=$item->nombre; 
      }
      $data["fecha"]=$fecha;
      $this->load->view('Reportes/formatoBeneFisica',$data);
    }

    public function detBeneMoral($id=0,$idc,$idp,$tipoc,$idopera=0){ //moral
      $data['get_pais'] = $this->ModeloCatalogos->getData('pais');
      
      if($idopera==0){
        $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id_bene_moral'=>$id));  
      }else if($idopera>0){
        $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id'=>$id));   
      }else if($id>0)
       $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id_bene_moral'=>$id));

      if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
      if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
      if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
      if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
      if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
      if($tipoc==6) $tabla = "tipo_cliente_f";

      $get_pp = $this->General_model->get_tableRow($tabla,array("idperfilamiento"=>$idp));

      if($tipoc==1 || $tipoc==2) $nombre = $get_pp->nombre." ".$get_pp->apellido_paterno." ".$get_pp->apellido_materno;
      if($tipoc==3) $nombre = $get_pp->razon_social;
      if($tipoc==4) $nombre = $get_pp->nombre_persona;
      if($tipoc==5 || $tipoc==6) $nombre = $get_pp->denominacion;

      $data['nombre']=$nombre;
      $data['idc'] = $idc;
      $data['idcc'] = $idc;
      $data['idp'] = $idp;
      $data['tipoc'] = $tipoc;
      $data['idopera'] = $idopera;
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/c_c_beneficiario/addBeneMoral',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/c_c_beneficiario/addBeneMoraljs');
    }

  	public function addBeneMoral($id=0,$idc,$idp,$tipoc,$idopera=0,$idcc=0,$union=0){ //moral
    	$data['get_pais'] = $this->ModeloCatalogos->getData('pais');
	    
	    /*if($idopera==0){
    		//$data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id_bene_moral'=>$id));	
    	}else{
    		$data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id'=>$id));		
    	}*/

      if($id>0 && $idc!="o")
       $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id_bene_moral'=>$id)); 

      if($id>0 && $idc=="o")
       $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id'=>$id)); 

      if($tipoc==0 && $union==0){
        $get_tipoper=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
        $tipoc=$get_tipoper->idtipo_cliente;
      }

      if($union==0){
        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
        if($tipoc==3) $nombre = $get_per->razon_social;
        if($tipoc==4) $nombre = $get_per->nombre_persona;
        if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

        $data['nombre']=$nombre;
  	    $data['idc'] = $idc;
  	    $data['idp'] = $idp;
  	    $data['tipoc'] = $tipoc;
  	    $data['idopera'] = $idopera;
        $data['idcc'] = $idcc;
      }
      $data['id_union'] = $union;
      if($union>0){
        //log_message('error', 'union: '.$union);
        $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$union));
        $nombre="";
        foreach ($get_up as $k) {
          $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
          $tipoc=$get_perf->idtipo_cliente;
          if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
          if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
          if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
          if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
          if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
          if($tipoc==6) $tabla = "tipo_cliente_f";

          $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
          if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
          if($tipoc==3) $nombre .= $get_per->razon_social;
          if($tipoc==4) $nombre .= $get_per->nombre_persona;
          if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion;

          $data['nombre']=$nombre;
          $data['id_union'] = $union;

          $data['idc'] = $k->id_cliente;
          $data['idp'] = $k->id_perfilamiento;
          $data['tipoc'] = $tipoc;
          $data['idcc'] = $k->id_clientec;
          $data['idopera'] = $idopera;
        }
      }
	    $this->load->view('templates/header');
	    $this->load->view('templates/navbar');
	    $this->load->view('catalogos/c_c_beneficiario/addBeneMoral',$data);
	    $this->load->view('templates/footer');
	    $this->load->view('catalogos/c_c_beneficiario/addBeneMoraljs');
  	}

    public function generarPDF_BeneMoral($id=0,$idc,$idp,$tipoc,$idopera=0,$idcc=0,$union=0,$fecha=0){ //moral
      if($fecha==0){
        $fecha = date("Y-m-d");
      }
      if($id>0 && $idc!="o")
       $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id_bene_moral'=>$id)); 

      if($id>0 && $idc=="o")
       $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id'=>$id)); 

      if($id>0 && $idc>0)
       $data['b']=$this->General_model->get_tableRow("beneficiario_moral_moral",array('id'=>$id)); 

      if($tipoc==0){
        $get_tipoper=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
        $tipoc=$get_tipoper->idtipo_cliente;
      }

      if($union==0){
        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
        if($tipoc==3) $nombre = $get_per->razon_social;
        if($tipoc==4) $nombre = $get_per->nombre_persona;
        if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

        $data['nombre']=$nombre;
        $data['idc'] = $idc;
        $data['idp'] = $idp;
        $data['tipoc'] = $tipoc;
        $data['idopera'] = $idopera;
        $data['idcc'] = $idcc;
      }
      $data['id_union'] = $union;
      if($union>0){
        //log_message('error', 'union: '.$union);
        $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$union));
        $nombre="";
        foreach ($get_up as $k) {
          $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
          $tipoc=$get_perf->idtipo_cliente;
          if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
          if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
          if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
          if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
          if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
          if($tipoc==6) $tabla = "tipo_cliente_f";

          $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
          if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
          if($tipoc==3) $nombre .= $get_per->razon_social;
          if($tipoc==4) $nombre .= $get_per->nombre_persona;
          if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion;

          $data['nombre']=$nombre;
          $data['id_union'] = $union;

          $data['idc'] = $k->id_cliente;
          $data['idp'] = $k->id_perfilamiento;
          $data['tipoc'] = $tipoc;
          $data['idcc'] = $k->id_clientec;
          $data['idopera'] = $idopera;
        }
      }
      $data["pais"]="";
      $getp=$this->ModeloCatalogos->getselectwherestatus('pais','pais',array("clave"=>$data['b']->pais));
      foreach ($getp as $item){
        $data["pais"]=$item->pais; 
      }
      $data["fecha"]=$fecha;
      $this->load->view('Reportes/formatoBeneMoral',$data);
    }

    public function detBeneFide($id=0,$idc,$idp,$tipoc,$idopera=0){ //fodeicomiso
      
      if($idopera==0){
        $data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id_bene_moral'=>$id));  
      }else if($idopera>0){
        $data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id'=>$id));   
      }else if($id>0)
       $data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id_bene_moral'=>$id)); 

      if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
      if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
      if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
      if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
      if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
      if($tipoc==6) $tabla = "tipo_cliente_f";

      $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
      
      if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
      if($tipoc==3) $nombre = $get_per->razon_social;
      if($tipoc==4) $nombre = $get_per->nombre_persona;
      if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

      $data['nombre']=$nombre;
      $data['idc'] = $idc;
      $data['idcc'] = $idc;
      $data['idp'] = $idp;
      $data['tipoc'] = $tipoc;
      $data['idopera'] = $idopera;

      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/c_c_beneficiario/addBeneFide',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/c_c_beneficiario/addBeneFidejs');
    }

  	public function addBeneFide($id=0,$idc,$idp,$tipoc,$idopera=0,$idcc=0,$union=0){ //fideicomiso
	    /*if($idopera==0){
    		//$data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id_bene_moral'=>$id));	
    	}else{
    		$data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id'=>$id));		
    	}*/  

      if($id>0 && $idc!="o")
       $data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id_bene_moral'=>$id)); 

      if($id>0 && $idc=="o")
       $data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id'=>$id)); 

      $nombre="";
     if($union==0){
        $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
        $tipoc=$get_perf->idtipo_cliente;

        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        
        if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
        if($tipoc==3) $nombre = $get_per->razon_social;
        if($tipoc==4) $nombre = $get_per->nombre_persona;
        if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

        $data['nombre']=$nombre;
  	    $data['idc'] = $idc;
  	    $data['idp'] = $idp;
  	    $data['tipoc'] = $tipoc;
        $data['idcc'] = $idcc;
  	    $data['idopera'] = $idopera;
        $data['id_union'] = $union;
      }

      if($union>0){
        //log_message('error', 'union: '.$union);
        $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$union));
        $nombre="";
        foreach ($get_up as $k) {
          $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
          $tipoc=$get_perf->idtipo_cliente;
          if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
          if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
          if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
          if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
          if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
          if($tipoc==6) $tabla = "tipo_cliente_f";

          $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
          if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
          if($tipoc==3) $nombre .= $get_per->razon_social;
          if($tipoc==4) $nombre .= $get_per->nombre_persona;
          if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion;

          $data['nombre']=$nombre;
          $data['id_union'] = $union;

          $data['idc'] = $k->id_cliente;
          $data['idp'] = $k->id_perfilamiento;
          $data['tipoc'] = $tipoc;
          $data['idcc'] = $k->id_clientec;
          $data['idopera'] = $idopera;
        }
      }
      //log_message('error', 'idopera: '.$data['idopera']);
	    $this->load->view('templates/header');
	    $this->load->view('templates/navbar');
	    $this->load->view('catalogos/c_c_beneficiario/addBeneFide',$data);
	    $this->load->view('templates/footer');
	    $this->load->view('catalogos/c_c_beneficiario/addBeneFidejs');
  	}

    public function generarPDF_BeneFide($id=0,$idc,$idp,$tipoc,$idopera=0,$idcc=0,$union=0,$fecha=0){ //fideicomiso
      //log_message('error', 'id: '.$id);
      //log_message('error', 'idc: '.$idc);
      if($fecha==0){
        $fecha = date("Y-m-d");
      }
      if($id>0 && $idc!="o")
        $data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id_bene_moral'=>$id)); 

      if($id>0 && $idc=="o" || !isset($data['b']))
       $data['b']=$this->General_model->get_tableRow("beneficiario_fideicomiso",array('id'=>$id)); 

      $nombre="";
      if($union==0){
        $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
        $tipoc=$get_perf->idtipo_cliente;

        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        
        if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
        if($tipoc==3) $nombre = $get_per->razon_social;
        if($tipoc==4) $nombre = $get_per->nombre_persona;
        if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

        $data['nombre']=$nombre;
        $data['idc'] = $idc;
        $data['idp'] = $idp;
        $data['tipoc'] = $tipoc;
        $data['idcc'] = $idcc;
        $data['idopera'] = $idopera;
        $data['id_union'] = $union;
      }

      if($union>0){
        //log_message('error', 'union: '.$union);
        $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$union));
        $nombre="";
        foreach ($get_up as $k) {
          $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
          $tipoc=$get_perf->idtipo_cliente;
          if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
          if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
          if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
          if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
          if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
          if($tipoc==6) $tabla = "tipo_cliente_f";

          $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
          if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
          if($tipoc==3) $nombre .= $get_per->razon_social;
          if($tipoc==4) $nombre .= $get_per->nombre_persona;
          if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion;

          $data['nombre']=$nombre;
          $data['id_union'] = $union;

          $data['idc'] = $k->id_cliente;
          $data['idp'] = $k->id_perfilamiento;
          $data['tipoc'] = $tipoc;
          $data['idcc'] = $k->id_clientec;
          $data['idopera'] = $idopera;
        }
      }
      $data['fecha'] = $fecha;
      $this->load->view('Reportes/formatoBeneFide',$data);
    }

  	public function guardar_beneficiario_fisica(){
	    $data = $this->input->post();
	    //log_message('error', 'idopera: '.$data['idopera']);
      //$id=$data['id'];
	    $idopera= $data['idopera'];
	    unset($data['idopera']);
      $idp= $data['idp'];
      $idc= $data['id_clientec'];
      $idcc= $data['idcc'];
      $id_union= $data['id_union'];
      unset($data['idp']); unset($data['id_clientec']); unset($data['idcc']);
      $id_hist=0;
	    if(isset($data['id'])){
	    	$this->ModeloCatalogos->updateCatalogo($data,'id',$data['id'],'beneficiario_fisica');
        $id=$data['id'];
      }
	    else{
	    	$id=$this->ModeloCatalogos->tabla_inserta('beneficiario_fisica',$data);
        if($id_union>0){
          $this->ModeloCatalogos->tabla_inserta('beneficiario_union_cliente',array("id_union"=>$id_union,"id_duenio_bene"=>$id,"tipo_bene"=>1,"fecha_reg"=>date("Y-m-d H:i:s")));
        }
	    	if($idopera>0){
	    		//$this->ModeloCatalogos->updateCatalogo(array("id_duenio_bene"=>$id,"tipo_bene"=>1),'id',$idopera,'clientes_operaciones');	
          $this->ModeloCatalogos->tabla_inserta('operacion_beneficiario',array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_duenio_bene"=>$id,"tipo_bene"=>1));
	    	}
        /*$array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre_db"=>$data['nombre'], "apellidos_db"=>$data['apellido_paterno']." ".$data['apellido_materno'],"identifica_db"=>$data['numero_identificacion'],"id_bene"=>$id,"tipo_bene"=>"1");
        $where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc);
        $id_hist=$this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');*/
	    }

      //log_message('error', 'idopera: '.$idopera);
      //log_message('error', 'id: '.$id);
      if($idopera>=0){
        $get_bene=$this->ModeloCatalogos->getselectwherestatus('*','beneficiario_fisica',array("id"=>$id));
        foreach ($get_bene as $it) {
          $nombre = $it->nombre;
          $apellido_p = $it->apellido_paterno;
          $apellido_m = $it->apellido_materno;
          $identificacion = $it->numero_identificacion;
          $r_f_c_d = $it->r_f_c_d;
          $curp_d = $it->curp_d;
        }
        $get_hist=$this->ModeloCliente->detalles_busquedaPBDB($idopera,$id,1);
        foreach ($get_hist as $it) {
          $id_hist = $it->id;
        }
        //log_message('error', 'id_hist del foreach: '.$id_hist);
        //if($nombre!=$data['nombre'] || $apellido_p!=$data['apellido_paterno'] || $apellido_m!=$data['apellido_materno'] || $r_f_c_d!=$data['r_f_c_d'] || $curp_d!=$data['curp_d'] || $identificacion!=$data['numero_identificacion']){
          $identifica = $data['numero_identificacion'];
          if($data['numero_identificacion']!=""){
            $identifica = $data['numero_identificacion']; 
          }
          if($data['numero_identificacion']!="" && $data['r_f_c_d']!=""){
            $identifica = $data['numero_identificacion']."|".$data['r_f_c_d']; 
          }
          if($data['numero_identificacion']!="" && $data['r_f_c_d']!="" && $data['curp_d']!=""){
            $identifica = $data['numero_identificacion']."|".$data['r_f_c_d']."|".$data['curp_d']; 
          }
          $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre_db"=>$data['nombre'], "apellidos_db"=>$data['apellido_paterno']." ".$data['apellido_materno'],"identifica_db"=>$identifica,"id_bene"=>$id,"tipo_bene"=>"1","id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc);
          /*$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc,"id"=>$id_hist);
          $hist_cpb=$this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');*/
        //}

        /***************************** */
        /* AGREGADO PARA MULTIPLES BENES Y COMENTADO LO DE ARRIBA  */
        if($id_hist==0){
          $id_hist=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
          $hist_cpb=$id_hist;
        }else{
          $where = array("id"=>$id_hist);
          $this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');
          $id_hist=0;
        }
        /* ***************************** */
      }
      //log_message('error', 'id_hist de bene fisica: '.$id_hist);
      $respuesta = "";
      if($id_hist>0){
        $identifica = $data['numero_identificacion'];
        if($data['numero_identificacion']!=""){
          $identifica = $data['numero_identificacion']; 
        }
        if($data['numero_identificacion']!="" && $data['r_f_c_d']!=""){
          $identifica = $data['numero_identificacion']."|".$data['r_f_c_d']; 
        }
        if($data['numero_identificacion']!="" && $data['r_f_c_d']!="" && $data['curp_d']!=""){
          $identifica = $data['numero_identificacion']."|".$data['r_f_c_d']."|".$data['curp_d']; 
        }
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['apellido_paterno']." ".$data['apellido_materno'];
        $Nombre=$data['nombre'];
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';

        //$this->consumoAPI($id_hist,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$this->consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);

        $this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>$respuesta),array("id"=>$id_hist),'historico_consulta_pb');//guarda el resultado del webservice
        */
      }
	    echo $id; 
	}

	public function guardar_beneficiario_fidei(){
	    $data = $this->input->post();
	    //log_message('error', 'id: '.$data['id']);
      //$id=$data['id'];
	    $idopera= $data['idopera'];
      $id_union= $data['id_union'];
      $idp= $data['idp'];
      $idc= $data['idc'];
      $idcc= $data['idcc'];
      //$idbene= $data['id_bene_moral'];
	    unset($data['idopera']); unset($data['idp']); unset($data['idc']); unset($data['idcc']); //unset($data['id_union']);
      $id_hist=0;
	    if(isset($data['id'])){
	    	unset($data['id_bene_moral']);
        $this->ModeloCatalogos->updateCatalogo($data,'id',$data['id'],'beneficiario_fideicomiso');
        $id=$data['id'];
	    }
	    else{
	    	$id=$this->ModeloCatalogos->tabla_inserta('beneficiario_fideicomiso',$data);
        if($id_union>0){
          $this->ModeloCatalogos->tabla_inserta('beneficiario_union_cliente',array("id_union"=>$id_union,"id_duenio_bene"=>$id,"tipo_bene"=>3,"fecha_reg"=>date("Y-m-d H:i:s")));
        }
	    	if($idopera>0){
	    		//$this->ModeloCatalogos->updateCatalogo(array("id_duenio_bene"=>$id,"tipo_bene"=>3),'id',$idopera,'clientes_operaciones');
          $this->ModeloCatalogos->tabla_inserta('operacion_beneficiario',array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_duenio_bene"=>$id,"tipo_bene"=>3));
	    	}
        /*$array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre_db"=>$data['razon_social'], "apellidos_db"=>"","identifica_db"=>$data['rfc'],"id_bene"=>$id,"tipo_bene"=>"3");
        $where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc);
        $id_hist=$this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');*/
	    }
      if($idopera>=0){
        /*$get_bene=$this->ModeloCatalogos->getselectwherestatus('*','beneficiario_fideicomiso',array("id",$data['id']));
        foreach ($get_bene as $it) {
          $razon_social = $it->razon_social;
          $rfc = $it->rfc;
          $referencia = $it->referencia;
        }*/
        $get_hist=$this->ModeloCliente->detalles_busquedaPBDB($idopera,$id,3);
        foreach ($get_hist as $it) {
          $id_hist = $it->id;
        }
        //log_message('error', 'id_hist del foreach: '.$id_hist);
        //if($razon_social!=$data['razon_social'] || $rfc!=$data['rfc'] || $referencia!=$data['referencia']){
          $identifica = $data['rfc'];
          $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre_db"=>$data['razon_social'], "apellidos_db"=>"","identifica_db"=>$identifica,"id_bene"=>$id,"tipo_bene"=>"3","id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc);
          /*$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc,"id"=>$id_hist);
          $hist_cpb=$this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');*/
          //$id_hist=$hist_cpb;
        //}
        /***************************** */
        /* AGREGADO PARA MULTIPLES BENES Y COMENTADO LO DE ARRIBA  */
        if($id_hist==0){
          $id_hist=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
          $hist_cpb=$id_hist;
        }else{
          $where = array("id"=>$id_hist);
          $this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');
          $id_hist=0;
        }
        /* ***************************** */
      }
      $respuesta = "";
      if($id_hist>0){
        $identifica = $data['rfc'];
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['razon_social'];
        $Nombre="";
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';

        //$this->consumoAPI($id_hist,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$this->consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);

        $this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>$respuesta),array("id"=>$id_hist),'historico_consulta_pb');//guarda el resultado del webservice
        */
      }
	    echo $id; 
	}

  public function consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion)
  {
    $Usuario='espinozabe1';
    $Password='B8019775';
    //$Usuario='paquinije'; //datos de prueba
    //$Password='389DB30E'; //datos de prueba
    $jsonData = array(
      'usuario'=> $Usuario,
      'clave' => $Password
    );
    //log_message('error', 'token_ws: '.$this->session->userdata("token_ws"));
    if($this->session->userdata("token_ws")=="0"){
      $token=str_replace('"', '', $this->getJWT());
    }else{
      $token=str_replace('"', '', $this->session->userdata("token_ws"));
    }
    //log_message('error', 'token: '.$token);
    $parametros = array(
      'apellido'=> $Apellido,
      'nombre' => $Nombre,
      'identificacion'=> $Identificacion,
      'pepsOtrosPaises' => "S",
      'satXDenominacion'=> "S",
      'documentosSimilares' => "S"
    );

    $status=$this->getLista($parametros,$id_hist,$token);
    //log_message('error', 'status: '.$status);
    if($status=="401"){
      $token=str_replace('"', '', $this->getJWT());
      $this->getLista($parametros,$id_hist,$token);
    }
    //$this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>$data2),array("id"=>$id_hist),'historico_consulta_pb');//guarda el resultado del webservice
  }

  public function getJWT(){
    $Usuario='espinozabe1';
    $Password='B8019775';
    //$Usuario='paquinije'; //datos de prueba
    //$Password='389DB30E'; //datos de prueba
    $jsonData = array(
      'usuario'=> $Usuario,
      'clave' => $Password
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json'
    ));
    curl_setopt($ch, CURLOPT_URL, "https://mbalistas.prevenciondelavado.com/Login" );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_POST, 1 );
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
    $result=curl_exec ($ch);
    //echo $result;
    curl_close($ch);
    //log_message('error', 'result: '.$result);
    $data = json_decode($result);
    $token = json_encode($data->token);
    $this->session->set_userdata("token_ws",str_replace('"', '',$token));
    //log_message('error', 'token: '.$token);
    return $token;
  }

  public function getLista($parametros,$id_hist,$token){
    //log_message('error', 'token: '.$token);
    //log_message('error', 'parametros: '.var_dump($parametros));
    $autori=array('Content-Type: application/json',"Authorization: Bearer {$token} ");
    //log_message('error', 'autori: '.var_dump($autori));
    //$autori=array('Authorization: Bearer '.$token.'');
    $url="https://mbalistas.prevenciondelavado.com/listas";
    $ch2 = curl_init();
    //curl_setopt($ch2, CURLOPT_HEADER, false); 
    curl_setopt($ch2, CURLOPT_HTTPHEADER, $autori );
    curl_setopt($ch2, CURLOPT_URL, $url); 
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch2, CURLOPT_POST, 1 );
    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($parametros));
    $data2 = curl_exec($ch2); 
    //echo $data2;
    curl_close($ch2);
    //log_message('error', 'data2: '.$data2);

    $ddata = json_decode($data2);
    //log_message('error', 'sizeof array_emp: '.sizeof($array_emp));
    $status="200";
    if(strlen($data2)==17){
      $status = "200";
    }else if(strlen($data2)>17 && strlen($data2)<=162){
      $status = json_encode($ddata->status);
    }
    /*log_message('error', 'status: '.$status);
    log_message('error', 'data2: '.$data2);
    log_message('error', 'strlen data2: '.strlen($data2));*/
    if($status=="200" && strlen($data2)>162){ //ok
      $respuesta = str_replace('{"resultados":', '', $data2);
      $respuesta = str_replace('[', '', $respuesta);
      $respuesta = str_replace(']', '', $respuesta);
      $respuesta=substr($respuesta, 0, -1);
      $this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>$respuesta),array("id"=>$id_hist),'historico_consulta_pb');//guarda el resultado del webservice
    }
    return $status;
  }

	public function guardar_beneficiario_moral(){
	    $data = $this->input->post();
	    //log_message('error', 'id: '.$data['id']);
      //$id = $data['id'];
	    $idopera= $data['idopera'];
	    unset($data['idopera']);
      $idp= $data['idp'];
      $idc= $data['idc'];
      $idcc= $data['idcc'];
      $id_union= $data['id_union'];
      unset($data['idp']); unset($data['idc']); unset($data['idcc']);
      $id_hist=0;
	    if(isset($data['id'])){
	    	unset($data['id_bene_moral']);
	    	$this->ModeloCatalogos->updateCatalogo($data,'id',$data['id'],'beneficiario_moral_moral');
        $id=$data['id'];
	    }
	    else{
	    	$id=$this->ModeloCatalogos->tabla_inserta('beneficiario_moral_moral',$data);
        if($id_union>0){
          $this->ModeloCatalogos->tabla_inserta('beneficiario_union_cliente',array("id_union"=>$id_union,"id_duenio_bene"=>$id,"tipo_bene"=>2,"fecha_reg"=>date("Y-m-d H:i:s")));
        }
	    	if($idopera>0){
	    		//$this->ModeloCatalogos->updateCatalogo(array("id_duenio_bene"=>$id,"tipo_bene"=>2),'id',$idopera,'clientes_operaciones');	
          $this->ModeloCatalogos->tabla_inserta('operacion_beneficiario',array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_duenio_bene"=>$id,"tipo_bene"=>2));
	    	}
        /*$array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre_db"=>$data['razon_social'], "apellidos_db"=>"","identifica_db"=>$data['rfc'],"id_bene"=>$id,"tipo_bene"=>"2");
        $where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc);
        $id_hist=$this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');*/
	    }
      if($idopera>=0){
        $get_hist=$this->ModeloCliente->detalles_busquedaPBDB($idopera,$id,2);
        foreach ($get_hist as $it) {
          $id_hist = $it->id;
        }
        //log_message('error', 'id_hist del foreach: '.$id_hist);
        $identifica = $data['rfc'];
        $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre_db"=>$data['razon_social'], "apellidos_db"=>"","identifica_db"=>$identifica,"id_bene"=>$id,"tipo_bene"=>"2","id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc);
        /*$where = array("id_operacion"=>$idopera,"id_perfilamiento"=>$idp,"id_clientec"=>$idcc,"id"=>$id_hist);
        $hist_cpb=$this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');*/
        //$id_hist=$hist_cpb;

        /***************************** */
        /* AGREGADO PARA MULTIPLES BENES Y COMENTADO LO DE ARRIBA  */
        if($id_hist==0){
          $id_hist=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
          $hist_cpb=$id_hist;
        }else{
          $where = array("id"=>$id_hist);
          $this->ModeloCatalogos->updateCatalogo2($array_pb,$where,'historico_consulta_pb');
          $id_hist=0;
        }
        /* ***************************** */
      }
      $respuesta = "";
      if($id_hist>0){
        $identifica = $data['rfc'];
        $url='https://www.prevenciondelavado.com/listas/api/busqueda';
        $Usuario='espinozabe1';
        $Password='B8019775';
        $Apellido=$data['razon_social'];
        $Nombre="";
        $Identificacion=$identifica;
        $PEPS_otros_paises='S';
        $Incluye_SAT='S';
        $SATxDenominacion='S';

        //$this->consumoAPI($id_hist,urlencode($Apellido),urlencode($Nombre),$Identificacion);
        //$this->consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion); //deshabilitada para reducir consultas

        /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;

        $sesion = curl_init($url);
        // definir tipo de petición a realizar: POST
        curl_setopt ($sesion, CURLOPT_POST, 1); 
        // Le pasamos los parámetros definidos anteriormente
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
        // sólo queremos que nos devuelva la respuesta
        curl_setopt($sesion, CURLOPT_HEADER, false); 
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        // ejecutamos la petición
        $respuesta = curl_exec($sesion); 
        $error = curl_error($sesion);
        // cerramos conexión
        curl_close($sesion); 
        //echo $respuesta;
        //echo $error;
        $respuesta = str_replace('[', '', $respuesta);
        $respuesta = str_replace(']', '', $respuesta);
        log_message('error', 'respuesta: '.$respuesta);
        //log_message('error', 'Nombre: '.$nombre);
        //log_message('error', 'apellidos: '.$apellidos);
        log_message('error', 'identifica: '.$identifica);

        $this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>$respuesta),array("id"=>$id_hist),'historico_consulta_pb');//guarda el resultado del webservice
        */
      }
	    echo $id; 
	}

	public function get_estadoFisica(){
      	$id = $this->input->post('id');
      	$edo = $this->input->post('edo');
      	$html = '';
      	$result = $this->ModeloCatalogos->getData('estado');
      	$html.= '<select class="form-control estado_'.$id.'" name="estado" id="estado">';
      	foreach ($result as $item) {
      		$select="";
      		if($edo==$item->id){ $select="selected"; }
	        $html.='<option value="'.$item->id.'" '.$select.'>'.$item->estado.'</option>';
      	}
      	$html.= '</select>';
	    echo $html;  
  	} 

  	public function validar(){
    	$id = $this->input->post('id');
     	$val = $this->input->post('val');
      $tipob = $this->input->post('tipob');
      $data = array('validado' => $val,'fecha'=>$this->fechaactual);
      $this->ModeloCatalogos->updateCatalogo2($data,array('id_beneficiario'=>$id,"tipo_bene"=>$tipob),'docs_beneficiario');
  	}

    public function eliminarBene(){
    	$id = $this->input->post('id');
    	$tabla = $this->input->post('tablaBen');
      $id_union = $this->input->post('id_union');
      $id_auni = $this->input->post('id_auni');
      $tipo_bene = $this->input->post('tipo_bene');
      
    	if($tabla==1) { $tabla ="beneficiario_fisica"; $col="activo"; }
    	if($tabla==2 || $tabla==3) { $tabla ="beneficiario_moral"; $col="status"; }
      if($tabla==3) { $tabla ="beneficiario_fideicomiso"; $col="status"; }
    	$data = array($col => 0);
    	$this->ModeloCatalogos->updateCatalogo($data,'id',$id,$tabla);
      $arraywhere = array('id_union'=>$id_union,'id_duenio_bene'=>$id_auni,'tipo_bene'=>$tipo_bene );
      $arraydatabene = array('activo' => 0);;
      $this->ModeloCatalogos->updateCatalogo2($arraydatabene,$arraywhere,'beneficiario_union_cliente');
  	}

  	public function file_delete($id,$col){  
	    $array = array($col => '');
      if($col!="acuse_sin_doc")
	      $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_beneficiario');
      else
        $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'doc_sin_bene');
      $data[]="";
      echo json_encode($data);
	  }

    public function file_deleteExtra($id,$col){  
      $array = array($col => '');
      $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_extra_bene');
      $data=[];
      echo json_encode($data);
    }

  	public function verificaFoto(){
  		$id = $this->input->post('id');
    	$idben = $this->input->post('idben');
    	$tipo = $this->input->post('tipo');
    	$col = $this->input->post('col');
      $id_opera = $this->input->post('id_opera');
    	//log_message('error', 'id: '.$id);
    	if($id>0)
    		$data=$this->General_model->get_docsBenes('docs_beneficiario',array("id"=>$id),$col,$id_opera);
    	else
    		$data=$this->General_model->get_docsBenes('docs_beneficiario',array("id_beneficiario"=>$idben,"tipo_bene"=>$tipo),$col,$id_opera);

    	echo json_encode($data);
 	  }

    public function verificaFotoExtra(){
      $id = $this->input->post('id');
      $idben = $this->input->post('idb');
      $tipo = $this->input->post('tipo');
      $col = $this->input->post('col');
      $col2 = $this->input->post('col2');
      $id_opera = $this->input->post('id_opera');
      if($id>0)
        $data=$this->General_model->get_docsExtraBenes('docs_extra_bene',array("id"=>$id),$col,$col2,$id_opera);
      else
        $data=$this->General_model->get_docsExtraBenes('docs_extra_bene',array("id_beneficiario"=>$idben,"tipo_bene"=>$tipo),$col,$col2,$id_opera);

      echo json_encode($data);
    }

    /// Listado de la tabla
    public function records_benes(){
    	$id = $this->input->post('id_perf');
    	//$idc = $this->input->post('idc');
    	//$data = $this->ModeloCliente->get_benef_control($id);
    	//echo json_encode($data->result());
    	$html = '<table class="table" id="table_beneficiario">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Catálogos</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>';
        $i=0;
    	$data = $this->ModeloCliente->get_benef_control_f('beneficiario_fisica',array('idtipo_cliente'=>$id,'activo'=>1,"id_union"=>0));
    	foreach ($data as $key) { //para pintar las personas fisicas
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->nombre.' '.$key->apellido_paterno.' '.$key->apellido_materno.'
    				</td>
    				<td>
    					<button type="button" class="btn" style="color: #212b4c;" onclick="documentos('.$key->id.',1)" title="Documentos"><i class="fa fa-file-o" style="font-size: 22px;"></i></button>
              <button type="button" class="btn archivo" style="color: #212b4c;" title="Expediente total de Docs." onclick="expedientes(0,'.$key->id.',1)"> <i class="fa fa-archive" style="font-size: 22px;"></i></button>
    				</td>
    				<td>
    					<button type="button" class="btn" style="color: #212b4c;" editar" title="Editar" onclick="editar('.$key->id.',1)" style="color: white;"><i class="fa fa-edit" style="font-size: 22px;"></i></button>
                          <button type="button" class="btn delete" style="color: #212b4c;" onclick="eliminar('.$key->id.',1,'.$i.')" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>
                    </td>
    			</tr>';
    	}
    	/*$datam = $this->ModeloCliente->get_benef_control_mf($id,'beneficiario_moral_fisica');
    	foreach ($datam as $key) { //para pintar las personas morales_fisicas
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->nombre.' '.$key->apellido_paterno.' '.$key->apellido_materno.'
    				</td>
    				<td>
    					<button type="button" class="btn btn-secondary btn-sm" onclick="documentos('.$key->id.',2)" title="Documentos"><i class="fa fa-file-o"></i></button>
    				</td>
    				<td>
    					<button type="button" class="btn btn-dark btn-sm editar" title="Editar" onclick="editar('.$key->idbm.',2)" style="color: white;"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-info btn-sm delete" onclick="eliminar('.$key->idbm.',2,'.$i.')" title="Eliminar"><i class="fa fa-trash"></i></button>
                    </td>
    			</tr>';
    	}*/
    	/*$datam = $this->ModeloCliente->get_benef_control_mf($id,'beneficiario_moral_c_d');
    	foreach ($datam as $key) { //para pintar las personas morales_fisicas
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->nombre.' '.$key->app.' '.$key->apm.'
    				</td>
    				<td>
    					<button type="button" class="btn btn-secondary btn-sm" onclick="documentos('.$key->id.',2)" title="Documentos"><i class="fa fa-file-o"></i></button>
    				</td>
    				<td>
    					<button type="button" class="btn btn-dark btn-sm editar" title="Editar" onclick="editar('.$key->idbm.',2)" style="color: white;"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-info btn-sm delete" onclick="eliminar('.$key->idbm.',2,'.$i.')" title="Eliminar"><i class="fa fa-trash"></i></button>
                    </td>
    			</tr>';
    	}*/
    	$datam = $this->ModeloCliente->get_benef_control_mf($id,'beneficiario_moral_moral');
    	foreach ($datam as $key) { //para pintar las personas morales_fisicas
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->razon_social.'
    				</td>
    				<td>
    					<button type="button" class="btn" style="color: #212b4c;" onclick="documentos('.$key->idbm.',2)" title="Documentos"><i class="fa fa-file-o" style="font-size: 22px;"></i></button>
              <button type="button" class="btn archivo" style="color: #212b4c;" title="Expediente total de Docs." onclick="expedientes('.$key->idbm.','.$key->id.',2)"> <i class="fa fa-archive" style="font-size: 22px;"></i></button>
    				</td>
    				<td>
    					<button type="button" class="btn editar" style="color: #212b4c;" title="Editar" onclick="editar('.$key->idbm.',2)" style="color: white;"><i class="fa fa-edit" style="font-size: 22px;"></i></button>
                          <button type="button" class="btn delete" style="color: #212b4c;" onclick="eliminar('.$key->idbm.',2,'.$i.')" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>
                    </td>
    			</tr>';
    	}
    	$datam = $this->ModeloCliente->get_benef_control_mf($id,'beneficiario_fideicomiso');
    	foreach ($datam as $key) { //para pintar fideicomiso
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->razon_social.'
    				</td>
    				<td>
    					<button type="button" class="btn" style="color: #212b4c;" onclick="documentos('.$key->idbm.',3)" title="Documentos"><i class="fa fa-file-o" style="font-size: 22px;"></i></button>
              <button type="button" class="btn archivo" style="color: #212b4c;" title="Expediente total de Docs." onclick="expedientes('.$key->idbm.','.$key->id.',3)"> <i class="fa fa-archive" style="font-size: 22px;"></i></button>
    				</td>
    				<td>
    					<button type="button" class="btn editar" style="color: #212b4c;" title="Editar" onclick="editar('.$key->idbm.',3)" style="color: white;"><i class="fa fa-edit" style="font-size: 22px;"></i></button>
                          <button type="button" class="btn delete" style="color: #212b4c;" onclick="eliminar('.$key->idbm.',3,'.$i.')" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>
                    </td>
    			</tr>';
    	}
    	$html.="</tbody>";
    	echo $html;
    }

    public function records_benes_union(){
      $id_union = $this->input->post('id_union');
      $id_bene_aux=0;
      $tipo_bene_aux=0;
      $html = '<table class="table" id="table_beneficiario">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Catálogos</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>';
        $i=0;
      $get_bene_union=$this->ModeloCatalogos->getselectwherestatus2('*','beneficiario_union_cliente',array('id_union'=>$id_union,"activo"=>1),'tipo_bene');
      //$get_bene_union=$this->General_model->GetAllWhere_group_by('beneficiario_union_cliente',array('id_union'=>$id_union,"activo"=>1),'id_union');
      foreach ($get_bene_union as $k) {
        $id_union_aux=$k->id_union;
        $id_duenio_bene_aux=$k->id_duenio_bene;
        //var_dump($id_union_aux);
        //log_message('error', 'tipo_bene: '.$k->tipo_bene);
        if($k->tipo_bene==1){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
          $tipo_bene_aux=$k->tipo_bene;
          $id_bene_aux=$k->id_duenio_bene;
          //$tipo_bene_aux=$k->tipo_bene;
          //$data = $this->ModeloCliente->get_benef_control_f('beneficiario_fisica',array(/*'idtipo_cliente'=>$k->id_duenio_bene*/"id_union"=>$id_union,"id_union"=>!"0","activo"=>1));
          $data = $this->ModeloCliente->get_benef_control_f2('beneficiario_fisica',array("id_union"=>$id_union,"activo"=>1));
          //,'beneficiario_fisica.id AS id_auni'
          foreach ($data as $key) { //para pintar las personas fisicas
            $i++;
            $html .= '<tr class="rowt_'.$i.'">
                <td>
                  '.$key->nombre.' '.$key->apellido_paterno.' '.$key->apellido_materno.'
                </td>
                <td>
                  <button type="button" class="btn" style="color: #212b4c;" onclick="documentos('.$key->id.',1)" title="Documentos"><i class="fa fa-file-o" style="font-size: 22px;"></i></button>
                </td>
                <td>
                  <button type="button" class="btn" style="color: #212b4c;" editar" title="Editar" onclick="editar('.$key->id.',1)" style="color: white;"><i class="fa fa-edit" style="font-size: 22px;"></i></button>
                              <button type="button" class="btn delete" style="color: #212b4c;" onclick="eliminar('.$key->id.',1,'.$i.','.$id_union_aux.','.$key->id.','.$tipo_bene_aux.')" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>
                        </td>
              </tr>';
          }
        }
        if($k->tipo_bene==2){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
          $tipo_bene_aux=$k->tipo_bene;
          $id_bene_aux=$k->id_duenio_bene;
          $datam = $this->ModeloCliente->get_benef_control_mfUnion($id_union,'beneficiario_moral_moral','beneficiario_moral_moral.id AS id_auni');
          foreach ($datam as $key) { //para pintar las personas morales_fisicas
            $i++;
            $html .= '<tr class="rowt_'.$i.'">
                <td>
                  '.$key->razon_social.'
                </td>
                <td>
                  <button type="button" class="btn" style="color: #212b4c;" onclick="documentos('.$key->idbm.',2)" title="Documentos"><i class="fa fa-file-o" style="font-size: 22px;"></i></button>
                </td>
                <td>
                  <button type="button" class="btn editar" style="color: #212b4c;" title="Editar" onclick="editar('.$key->idbm.',2)" style="color: white;"><i class="fa fa-edit" style="font-size: 22px;"></i></button>
                              <button type="button" class="btn delete" style="color: #212b4c;" onclick="eliminar('.$key->idbm.',2,'.$i.','.$id_union_aux.','.$key->id_auni.','.$tipo_bene_aux.')" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>
                        </td>
              </tr>';
          }
        }
        if($k->tipo_bene==3){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
          $tipo_bene_aux=$k->tipo_bene;
          $id_bene_aux=$k->id_duenio_bene;
          $datam = $this->ModeloCliente->get_benef_control_mfUnion($id_union,'beneficiario_fideicomiso','beneficiario_fideicomiso.id AS id_auni');

          foreach ($datam as $key) { //para pintar fideicomiso
            $i++;
            $html .= '<tr class="rowt_'.$i.'">
                <td>
                  '.$key->razon_social.'
                </td>
                <td>
                  <button type="button" class="btn" style="color: #212b4c;" onclick="documentos('.$key->idbm.',3)" title="Documentos"><i class="fa fa-file-o" style="font-size: 22px;"></i></button>
                </td>
                <td>
                  <button type="button" class="btn editar" style="color: #212b4c;" title="Editar" onclick="editar('.$key->idbm.',3)" style="color: white;"><i class="fa fa-edit" style="font-size: 22px;"></i></button>
                              <button type="button" class="btn delete" style="color: #212b4c;" onclick="eliminar('.$key->idbm.',3,'.$i.','.$id_union_aux.','.$key->id_auni.','.$tipo_bene_aux.')" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>
                        </td>
              </tr>';
          }
        }
        $tipo_bene_aux=$k->tipo_bene;
        $id_bene_aux=$k->id_duenio_bene;
      }
      $html.="</tbody>";
      echo $html;
    }
    public function cargafiles_beneficiario($id,$idb,$idc,$img,$tipop,$id_opera){
	    $iddoc = $this->input->post('iddoc');

	    if($img=='formato_pb') $folder = "formato_perb";
	    if($img=='ine') $folder = "ine";
	    if($img=='cif') $folder = "cif";
	    if($img=='curp') $folder = "curp";
	    if($img=='comprobante_dom') $folder = "compro_dom";
      if($img=='formato_cc') $folder = "formato_cc";
      if($img=='imgineal') $folder = "ine_apoderado";
      if($img=='esc_ints') $folder = "escrituras";
      if($img=='poder_not_fac_serv') $folder = "poder_notarial";
	   
	    $upload_folder ='uploads_benes/'.$folder;
	    $nombre_archivo = $_FILES['foto']['name'];
	    $tipo_archivo = $_FILES['foto']['type'];
	    $tamano_archivo = $_FILES['foto']['size'];
	    $tmp_archivo = $_FILES['foto']['tmp_name'];
	    //$archivador = $upload_folder . '/' . $nombre_archivo;
	    $fecha=date('ymd-His');
	    $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
      $nombre_archivo=str_replace('-', '_', $nombre_archivo);
	    $nombre_archivo=$this->eliminar_acentos($nombre_archivo);
	    $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
	    $archivador = $upload_folder . '/'.$newfile;
	    if (!move_uploaded_file($tmp_archivo, $archivador)) {
	        $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
	    }else{
        //insertar este reg en bitacora de documentos por cliente
        $this->ModeloCatalogos->tabla_inserta('bitacora_docs_bene',array("id_beneficiario"=>$idb,"id_clientec"=>$idc,'tipo_bene'=>$tipop,"tipo_doc"=>$img,"nombre_doc"=>$newfile,"fecha"=>date("Y-m-d H:i:s"),"id_cliente"=>$this->idcliente,"id_operacion"=>$id_opera));

	        $array = array('id_beneficiario'=>$idb,'id_clientec'=>$idc,'tipo_bene'=>$tipop,$img=>$newfile,'fecha'=>$this->fechaactual,"id_operacion"=>$id_opera);
	        if($id==0){ //insert
	          $id=$this->ModeloCatalogos->tabla_inserta('docs_beneficiario',$array);
	          //log_message('error', 'inserto y regresa el id: '.$id);
	        }
	        if($id>0){ //update
	        	$this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_beneficiario');
	        	//log_message('error', 'edito y regresa el id: '.$id);
	        }
	        $return = Array('id'=>$id,'ok'=>TRUE);
	    }
	    echo json_encode($return);
  	}

    function cargafilesExtra($id,$idb,$idc,$img,$tipo,$id_opera=0){
      $iddoc = $this->input->post('iddoc');

      $upload_folder ='uploads_benes/extras';
      $nombre_archivo = $_FILES['foto']['name'];
      $tipo_archivo = $_FILES['foto']['type'];
      $tamano_archivo = $_FILES['foto']['size'];
      $tmp_archivo = $_FILES['foto']['tmp_name'];
      //$archivador = $upload_folder . '/' . $nombre_archivo;
      $fecha=date('ymd-His');
      $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
      $nombre_archivo=str_replace('-', '_', $nombre_archivo);
      $nombre_archivo=$this->eliminar_acentos($nombre_archivo);

      $newfile=$fecha.'-'.$nombre_archivo;        
      $archivador = $upload_folder . '/'.$newfile;
      if (!move_uploaded_file($tmp_archivo, $archivador)) {
          $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
      }else{
        //insertar este reg en bitacora de documentos por cliente
        $this->ModeloCatalogos->tabla_inserta('bitacora_docs_bene',array("id_beneficiario"=>$idb,"id_clientec"=>$idc,'tipo_bene'=>$tipo,"tipo_doc"=>"doc extra","nombre_doc"=>$newfile,"fecha"=>date("Y-m-d H:i:s"),"id_cliente"=>$this->idcliente,"id_operacion"=>$id_opera));

          $array = array('id_beneficiario'=>$idb,'tipo_bene'=>$tipo,'id_clientec'=>$idc,$img=>$newfile,'fecha_reg'=>date("Y-m-d H:i:s"),"id_operacion"=>$id_opera);
          if($id==0){ //insert
            $id=$this->ModeloCatalogos->tabla_inserta('docs_extra_bene',$array);
            //log_message('error', 'id: '.$iddoc);
          }
          if($id>0){ //update
            //log_message('error', 'id: '.$iddoc);
            //$id=$iddoc; //unset($data["id"]);
            $id = $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_extra_bene');
          }
          $return = Array('id'=>$id,'ok'=>TRUE);
      }
      echo json_encode($return);
    }


  	public function eliminar_acentos($cadena){
	    //Reemplazamos la A y a
	    $cadena = str_replace(
	    array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
	    array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
	    $cadena
	    );

	    //Reemplazamos la E y e
	    $cadena = str_replace(
	    array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
	    array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
	    $cadena );

	    //Reemplazamos la I y i
	    $cadena = str_replace(
	    array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
	    array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
	    $cadena );

	    //Reemplazamos la O y o
	    $cadena = str_replace(
	    array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
	    array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
	    $cadena );

	    //Reemplazamos la U y u
	    $cadena = str_replace(
	    array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
	    array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
	    $cadena );

	    //Reemplazamos la N, n, C y c
	    $cadena = str_replace(
	    array('Ç', 'ç'),
	    array('C', 'c'),
	    $cadena
	    );
	    
	    return $cadena;
	}


    public function docs_beneficiario($id_benef_m,$tipo_pers,$idperfilamiento_cliente,$idcliente,$idopera){
	    $data['id_benef_m']=$id_benef_m;
	    $data['idperfilamiento_cliente']=$idperfilamiento_cliente;
	    $data['idclientec']=$idcliente;
      $data['idopera']=$idopera;
	    $data['docs']=$this->General_model->get_tableRow('docs_beneficiario',array('id_beneficiario'=>$id_benef_m,'tipo_bene'=>$tipo_pers));
      $data['docs_ext']=$this->General_model->get_tableRow('docs_extra_bene',array('id_beneficiario'=>$id_benef_m,'id_clientec'=>$idcliente)); 
      $data['id_bene']=0;
	    $titilo_t='';
	    $tipo_text = '';
	    $data['tipo_persona']=$tipo_pers;
	    if($tipo_pers==1){
	      $titilo_t='Persona Física';
	      $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_benef_m);
	     	foreach ($get_pf as $item) { 
	      		$estado_f = '';
            $data['id_bene'] = $item->id;
	      		if($item->pais=='MX'){
	         		$get_estado=$this->ModeloCatalogos->getselectwhere('estado','clave',$item->estado);
	          		foreach ($get_estado as $item_e) {
	            		$estado_f = $item_e->estado; 
	          		}
	      		}else{
	        		$estado_f=$item->estado;
	      		}
		      	$get_pais_aux=$this->ModeloCatalogos->getselectwhere('pais','clave',$item->pais);
		      	$pais = '';
		      	foreach ($get_pais_aux as $item_a) {
		        	$pais = $item_a->pais; 
		      	}
		      	$get_tipo_v_aux=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$item->tipo_vialidad);
		      	$tipo_v = '';
		      	foreach ($get_tipo_v_aux as $item_e) {
		        	$tipo_v = $item_e->nombre; 
		      	}
		      	$tipo_text='<div class="row">
	                    <div class="col-md-12">
	                        <label>Nombre:</label>
	                        <strong> '.$item->nombre.'</strong>
	                    </div>
	                  </div>
	                  <div class="row">
	                    <div class="col-md-12">
	                      <label>Apellido paterno:</label>
	                      <strong> '.$item->apellido_paterno.'</strong>
	                    </div>
	                  </div>
	                  <div class="row">
	                    <div class="col-md-12">
	                      <label>Apellido materno:</label>
	                      <strong> '.$item->apellido_materno.'</strong>
	                    </div>
	                  </div><br>
	                  <div class="row">
	                    <div class="col-md-12">
	                      <label>Nombre de identificación:</label>
	                      <strong> '.$item->nombre_identificacion.'</strong>
	                    </div>
	                  </div>
	                  <div class="row">
	                    <div class="col-md-12">
	                      <label>Autoridad que emite la identificación:</label>
	                      <strong> '.$item->autoridad_emite.'</strong>
	                    </div>
	                  </div>
	                  <div class="row">
	                    <div class="col-md-12">
	                      <label>Número de Identificación:</label>
	                      <strong> '.$item->numero_identificacion.'</strong>
	                    </div>
	                  </div><br>
	                  <div class="row">
	                      <div class="col-md-12">
	                        <h6>Domicilio:</h6>
	                      </div>
	                      <div class="col-md-6">
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>Tipo de vialidad:</label>
	                            <strong> '.$tipo_v.'</strong>
	                          </div>
	                        </div>
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>Calle:</label>
	                            <strong> '.$item->calle.'</strong>
	                          </div>
	                        </div>
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>No. ext:</label>
	                            <strong> '.$item->no_ext.'</strong>
	                          </div>
	                        </div>
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>No. int:</label>
	                            <strong> '.$item->no_int.'</strong>
	                          </div>
	                        </div>
	                      </div>
	                      <div class="col-md-6">
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>Colonia:</label>
	                            <strong> '.$item->colonia.'</strong>
	                          </div>
	                        </div>
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>Municipio o delegación:</label>
	                            <strong> '.$item->monicipio.'</strong>
	                          </div>
	                        </div>
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>Estado:</label>
	                              <strong> '.$estado_f.'</strong>
	                          </div>
	                        </div>
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>C.P:</label>
	                            <strong>'.$item->cp.'</strong>
	                          </div>
	                        </div>
	                        <div class="row">
	                          <div class="col-md-12">
	                            <label>País:</label>
	                            <strong> '.$pais.'</strong>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                  <br>'; 
	    	}
	    }else if($tipo_pers==2){
	    	//log_message('error', 'moral: '.$tipo_pers);
		    $titilo_t='Persona Moral';
		    $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$id_benef_m);
		    foreach ($get_pm as $item) {
	        if($item->identificacion_beneficiario=='m'){
	          	if($item->tipo_persona==2){
	          		//if($idopera==0)
	            		$get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id_bene_moral',$item->id);
	            	/*else
	            		$get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$id_benef_m);*/
	            	foreach ($get_pmf as $item) {
                  $data['id_bene'] = $item->id;
	           	 		$tipo_text.='<div class="row">
                      					<div class="col-md-12">
                          					<h5>Persona Moral</h5>
                      					</div>
	                    			</div>
				                    <div class="row">
				                      <div class="col-md-12">
				                          <label>Razón social:</label>
				                          <strong> '.$item->razon_social.'</strong>
				                      </div>
				                    </div>
				                    <br>
				                    <div class="row">
				                        
				                    </div>
				        ';
            		}
          		}else{
            		$tipo_text.='';
          		}
        	}
      	}  
	    	$tipo_text.='';
	    }
	    else if($tipo_pers==3){
	    	//log_message('error', 'fideicomiso: '.$tipo_pers);
		    $titilo_t='Fideicomiso';
		    $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$id_benef_m);
		    foreach ($get_pm as $item) {
        	if($item->identificacion_beneficiario=='f'){
	          	if($item->tipo_persona==3){
	          		//if($idopera==0)
	            		$get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id_bene_moral',$item->id);
	            	/*else
	            		$get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$id_benef_m);*/
	            	  
	            	foreach ($get_pmf as $item) {
                  $data['id_bene'] = $item->id;
	           	 		$tipo_text.='<div class="row">
			                <div class="col-md-12">
			                    <label>Razón social del fiduciario:</label>
			                    <strong> '.$item->razon_social.'</strong>
			                </div>
			              </div>
			              <div class="row">
			                <div class="col-md-12">
			                  <label>R.F.C del fideicomiso:</label>
			                  <strong> '.$item->rfc.'</strong>
			                </div>
			              </div><br>
			              <div class="row">
			                <div class="col-md-12">
			                  <label>Número o referencia del fideicomiso:</label>
			                  <strong> '.$item->referencia.'</strong>
			                </div>
			              </div>
				        ';
            		}
          		}else{
            		$tipo_text.='';
          		}
        	}
      	}  
	    	$tipo_text.='';
	    }
	    $data['titulo_t']=$titilo_t;
	    $data['text']=$tipo_text;
	    $this->load->view('templates/header');
	    $this->load->view('templates/navbar');
	    $this->load->view('catalogos/cliente_cliente/docs_beneficiario2',$data);
	    $this->load->view('templates/footer');
	    $this->load->view('catalogos/cliente_cliente/dosc_beneficiciario2_js');
	  }

    function carga_archivo_sin($id,$idc,$idp,$img,$idopera){
      $iddoc = $this->input->post('iddoc');

      $upload_folder ='uploads/doc_sin_bene';
      $nombre_archivo = $_FILES['foto']['name'];
      $tipo_archivo = $_FILES['foto']['type'];
      $tamano_archivo = $_FILES['foto']['size'];
      $tmp_archivo = $_FILES['foto']['tmp_name'];
      //$archivador = $upload_folder . '/' . $nombre_archivo;

      //log_message('error', 'nombre_archivo: '.$nombre_archivo);
      $fecha=date('ymd-His');

      $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
      //$nombre_archivo=str_replace('.', '_', $nombre_archivo);
      $nombre_archivo=str_replace('-', '_', $nombre_archivo);
      $nombre_archivo=$this->eliminar_acentos($nombre_archivo);

      $newfile= $fecha.'-'.$nombre_archivo;        
      $archivador = $upload_folder . '/'.$newfile;
      if (!move_uploaded_file($tmp_archivo, $archivador)) {
        $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
      }else{
        $array = array('id_operacion'=>$idopera,'id_perfilamiento'=>$idp,'id_clientec'=>$idc,$img=>$newfile,'fecha_reg'=>date("Y-m-d H:i:s"));
        if($id==0){ //insert
          $id=$this->ModeloCatalogos->tabla_inserta('doc_sin_bene',$array);
          //log_message('error', 'id: '.$iddoc);
        }
        if($id>0){ //update
          //log_message('error', 'id: '.$iddoc);
          //$id=$iddoc; //unset($data["id"]);
          $datar['id_perfilamiento'] = $idp;
          $this->ModeloCatalogos->updateCatalogo2($array,array('id_perfilamiento'=>$idp,"id_operacion"=>$idopera),'doc_sin_bene');
        }
        $return = Array('id'=>$id,'ok'=>TRUE);
      }
      echo json_encode($return);
    }

    public function submit_extras(){
      $data=$this->input->post();
      //$this->db->trans_start();
      log_message('error', ' id: '.$data['id']);
      $get = $this->General_model->GetAllWhere("docs_extra_bene",array("id"=>$data['id']));
      $i=0;
      foreach ($get as $val) {
        $i++;
      }
      //log_message('error', ' i: '.$i);
      if($i==0){ //insert
          $id=$this->ModeloCatalogos->tabla_inserta('docs_extra_bene',$data);
      }
      if($i>0){ //update
          $id=$data["id"]; unset($data["id"]);
          $this->ModeloCatalogos->updateCatalogo2($data,array('id'=>$id),'docs_extra_bene');
          //$this->ModeloCatalogos->update_docs_extrac($id,$data['descrip']);
      }
      /*$this->db->trans_complete();
      $this->db->trans_status();*/
      echo $id;
    }


    public function verificaAcuseBene(){
      $id = $this->input->post('id');
      $idopera = $this->input->post('idopera');
      //log_message('error', 'id: '.$id);
      if($id>0)
        $data=$this->General_model->get_docsBenes('doc_sin_bene',array("id"=>$id),"acuse_sin_doc");

      echo json_encode($data);
    }

    public function verificaAcuseBeneSin(){
      $id = $this->input->post('id');
      $idopera = $this->input->post('idopera');
      //log_message('error', 'id: '.$id);
      $data="";
      if($id>0)
        $data=$this->General_model->get_tableRow('doc_sin_bene',array('id'=>$id));

      echo json_encode($data);
    }

    public function sin_beneficiario($tipoc,$idperfilamiento,$idcliente,$idopera){
      $data['idperfilamiento_cliente']=$idperfilamiento;
      $data['idclientec']=$idcliente;
      $data['idopera']=$idopera;

      if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
      if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
      if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
      if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
      if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
      if($tipoc==6) $tabla = "tipo_cliente_f";

      $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idperfilamiento));
      if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
      if($tipoc==3) $nombre = $get_per->razon_social;
      if($tipoc==4) $nombre = $get_per->nombre_persona;
      if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

      $data['nombre']=$nombre;
      $data['doc']=$this->General_model->get_tableRow('doc_sin_bene',array('id_operacion'=>$idopera,'id_perfilamiento'=>$idperfilamiento));
      $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$idopera));
      $data["folio"]=$get_fol->folio_cliente;
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/cliente_cliente/sin_beneficiario',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/cliente_cliente/sin_beneficiario_js');
    }

    public function add_benem(){
	    $data = $this->input->post();
	    $idbm=0;
	    if(!isset($data['id'])){
		    $idbm = $this->ModeloCatalogos->tabla_inserta('beneficiario_moral',array('id_bene_trans'=>0,'tipo_cliente'=>$data['id_tipo_perfil'],
		      'id_perfilamiento' => $data['id_perfilamiento'],'identificacion_beneficiario'=>$data['tipo_inciso'],'tipo_persona'=>$data['tipo_p']));
		    //log_message('error', 'idbm: '.$idbm);
  		}/*else{
  			$this->ModeloCatalogos->updateCatalogo(array('id_bene_trans'=>0,'tipo_cliente'=>$data['id_tipo_perfil'],
  		      'id_perfilamiento' => $data['id_perfilamiento'],'identificacion_beneficiario'=>$data['tipo_inciso'],'tipo_persona'=>$data['tipo_p']),'id_bene_trans',$data['id'],'beneficiario_fideicomiso');
  		}*/
	    $return = Array('idbm'=>$idbm);
	    echo json_encode($return);
	  }
  public function get_progress_doc_cc(){
      $id = $this->input->post('id');
      $idben = $this->input->post('idben');
      $tipo = $this->input->post('tipo');
      $col = $this->input->post('col');
      $result=$this->ModeloCatalogos->getselectwhere('docs_beneficiario','id',$id);
      log_message('error', 'id en cont de docs bene: '.$id);
      $aux_c1=0;
      $aux_c2=0;
      $aux_c3=0;
      $aux_c4=0;
      $aux_c5=0;

      $formato_pb='';
      $ine='';
      $cif='';
      $curp='';
      $comprobante_dom='';
      foreach ($result as $item){
        $formato_pb = $item->formato_pb;
        $ine = $item->ine;
        $cif = $item->cif;
        $curp = $item->curp;
        $comprobante_dom = $item->comprobante_dom;
      }
      //if($formato_pb!=''){
        //$aux_c1=20;
      //}
      if($ine!=''){
        $aux_c2=25;
      }
      if($cif!=''){
        $aux_c3=25;
      }
      if($curp!=''){
        $aux_c4=25;
      }
      if($comprobante_dom!=''){
        $aux_c5=25;
      }
      $suma = $aux_c1+$aux_c2+$aux_c3+$aux_c4+$aux_c5;
      echo $suma;
  }
  function formatoPesonasBloqueadas2($id_benef_m,$tipo_pers,$idopera){
      $data['fechaactual'] = date("d-m-Y",strtotime($this->fechaactual));
      $data['horahoy'] =$this->horahoy;
      $data['id_benef_m']=$id_benef_m;
      $data['idopera']=$idopera;
      $tipo_persona='';
      $tipo_text = '';
      log_message('error', 'tipo persona en formato '.$tipo_pers);
      if($tipo_pers==1){
          $tipo_persona='Persona Física';
          $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_benef_m);
        foreach ($get_pf as $item) { 
            $nombre_tipo=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
            $nombre_identificacion=$item->nombre_identificacion;
            $numero_identificacion=$item->numero_identificacion;
        }
      }else if($tipo_pers==2){
        $tipo_persona='Persona Moral';
        //$get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$id_benef_m);
        //foreach ($get_pm as $item) {
          //log_message('error', '$item->identificacion_beneficiario: '.$item->identificacion_beneficiario);
          //if($item->identificacion_beneficiario=='m'){
          //log_message('error', '$item->tipo_persona '.$item->tipo_persona);
              //if($item->tipo_persona==2){
                //if($idopera==0)
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$id_benef_m);
                /*else
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$id_benef_m);*/
                foreach ($get_pmf as $item) {
                  $nombre_tipo=$item->razon_social;
                }
              /*}else{
                $nombre_tipo.='';
              }*/
          //}
        //}  
        $nombre_identificacion='';
        $numero_identificacion='';
      }
      else if($tipo_pers==3){
        $tipo_persona='Fideicomiso';
        //$get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$id_benef_m);
        //foreach ($get_pm as $item) {
            //if($item->identificacion_beneficiario=='f'){
                //if($item->tipo_persona==3){
                  
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$id_benef_m);
                  foreach ($get_pmf as $item) {
                    $nombre_tipo=$item->razon_social;
                  }
                /*}else{
                  $nombre_tipo='';
                }*/
            //}
        //} 
        $nombre_identificacion='';
        $numero_identificacion='';
      }

      $data['tipo_persona']=$tipo_persona;
      $data['tipo_pers']=$tipo_pers;
      $data['nombre_tipo']=$nombre_tipo;
      $data['nombre_identificacion']=$nombre_identificacion;
      $data['numero_identificacion']=$numero_identificacion;
    
    $this->load->view('templates/header');
    $this->load->view('catalogos/cliente_cliente/formatopersonasbloqueadas2',$data);
    //$this->load->view('catalogos/cliente_cliente/dosc_beneficiciario_js');
  }

  public function archivo($idbm,$id_benef_m,$tipo_pers){
      $data['idbm']=$idbm;
      $data['id_benef_m']=$id_benef_m;
      $data['bitacora']=$this->ModeloCatalogos->getselectwherestatus('*','bitacora_docs_bene',array('id_beneficiario'=>$id_benef_m,'tipo_bene'=>$tipo_pers));
      $data['id_bene']=0;
      $titilo_t='';
      $tipo_text = '';
      $data['tipo_persona']=$tipo_pers;
      if($tipo_pers==1){
        $titilo_t='Persona Física';
        $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_benef_m);
        foreach ($get_pf as $item) { 
            $estado_f = '';
            $data['id_bene'] = $item->id;
            if($item->pais=='MX'){
              $get_estado=$this->ModeloCatalogos->getselectwhere('estado','clave',$item->estado);
                foreach ($get_estado as $item_e) {
                  $estado_f = $item_e->estado; 
                }
            }else{
              $estado_f=$item->estado;
            }
            $get_pais_aux=$this->ModeloCatalogos->getselectwhere('pais','clave',$item->pais);
            $pais = '';
            foreach ($get_pais_aux as $item_a) {
              $pais = $item_a->pais; 
            }
            $get_tipo_v_aux=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$item->tipo_vialidad);
            $tipo_v = '';
            foreach ($get_tipo_v_aux as $item_e) {
              $tipo_v = $item_e->nombre; 
            }
            $tipo_text='<div class="row">
                      <div class="col-md-12">
                          <label>Nombre:</label>
                          <strong> '.$item->nombre.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Apellido paterno:</label>
                        <strong> '.$item->apellido_paterno.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Apellido materno:</label>
                        <strong> '.$item->apellido_materno.'</strong>
                      </div>
                    </div><br>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Nombre de identificación:</label>
                        <strong> '.$item->nombre_identificacion.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Autoridad que emite la identificación:</label>
                        <strong> '.$item->autoridad_emite.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Número de Identificación:</label>
                        <strong> '.$item->numero_identificacion.'</strong>
                      </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                          <h6>Domicilio:</h6>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Tipo de vialidad:</label>
                              <strong> '.$tipo_v.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Calle:</label>
                              <strong> '.$item->calle.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>No. ext:</label>
                              <strong> '.$item->no_ext.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>No. int:</label>
                              <strong> '.$item->no_int.'</strong>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Colonia:</label>
                              <strong> '.$item->colonia.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Municipio o delegación:</label>
                              <strong> '.$item->monicipio.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Estado:</label>
                                <strong> '.$estado_f.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>C.P:</label>
                              <strong>'.$item->cp.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>País:</label>
                              <strong> '.$pais.'</strong>
                            </div>
                          </div>
                        </div>
                      </div>
                    <br>'; 
        }
      }else if($tipo_pers==2){
        //log_message('error', 'moral: '.$tipo_pers);
        $titilo_t='Persona Moral';
        $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$idbm);
        foreach ($get_pm as $item) {
          if($item->identificacion_beneficiario=='m'){
              if($item->tipo_persona==2){
                //if($idopera==0)
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id_bene_moral',$item->id);
                /*else
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$id_benef_m);*/
                foreach ($get_pmf as $item) {
                  $data['id_bene'] = $item->id;
                  $tipo_text.='<div class="row">
                                <div class="col-md-12">
                                    <h5>Persona Moral</h5>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <label>Razón social:</label>
                                  <strong> '.$item->razon_social.'</strong>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                                
                            </div>
                ';
                }
              }else{
                $tipo_text.='';
              }
          }
        }  
        $tipo_text.='';
      }
      else if($tipo_pers==3){
        //log_message('error', 'fideicomiso: '.$tipo_pers);
        $titilo_t='Fideicomiso';
        $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$idbm);
        foreach ($get_pm as $item) {
          if($item->identificacion_beneficiario=='f'){
              if($item->tipo_persona==3){
                //if($idopera==0)
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id_bene_moral',$item->id);
                /*else
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$id_benef_m);*/
                  
                foreach ($get_pmf as $item) {
                  $data['id_bene'] = $item->id;
                  $tipo_text.='<div class="row">
                      <div class="col-md-12">
                          <label>Razón social del fiduciario:</label>
                          <strong> '.$item->razon_social.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>R.F.C del fideicomiso:</label>
                        <strong> '.$item->rfc.'</strong>
                      </div>
                    </div><br>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Número o referencia del fideicomiso:</label>
                        <strong> '.$item->referencia.'</strong>
                      </div>
                    </div>
                ';
                }
              }else{
                $tipo_text.='';
              }
          }
        }  
        $tipo_text.='';
      }
      $data['titulo_t']=$titilo_t;
      $data['text']=$tipo_text;
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('templates/footer2_librerias');
      $this->load->view('catalogos/c_c_beneficiario/archivo',$data);
      $this->load->view('templates/footer');
    }

  function exportarBitacora($idb,$tipo,$idbm){
    if($idb==0){
      $where=array("id_cliente"=>$this->idcliente);
    }else{
      $where=array("id_beneficiario"=>$idb,"tipo_bene"=>$tipo);
    }
    $data["idbm"]=$idbm;
    $data["b"] = $this->ModeloCatalogos->getselectwherestatus("*","bitacora_docs_bene",$where);
    $this->load->view('catalogos/c_c_beneficiario/bitacora_docs',$data);
  }

  /*function exportarBitacoraDocs($idb,$tipo){
    if($idb==0){
      $where=array("id_cliente"=>$this->idcliente);
    }else{
      $where=array("id_beneficiario"=>$idb,"tipo_bene"=>$tipo);
    }
    $cont=0;
    $bit = $this->ModeloCatalogos->getselectwherestatus("*","bitacora_docs_bene",$where);
    $this->load->library('zip');
    foreach ($bit as $b) {
      //log_message('error', 'nombre_doc: '.$b->nombre_doc);
      $cont++;

      if($b->tipo_bene==1){
        $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$b->id_beneficiario);
        foreach ($get_pf as $item) { 
          $bene = $item->nombre." ".$item->apellido_paterno." ".$item->apellido_materno;
        }
      }else if($b->tipo_bene==2){
        $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$b->id_beneficiario);
        foreach ($get_pmf as $item) {
          $bene=$item->razon_social;
        }
      }else if($b->tipo_bene==3){
        $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$b->id_beneficiario);
        foreach ($get_pmf as $item) {
          $bene=$item->razon_social;
        }
      }
      if($b->tipo_doc=="formato_cc"){
        $this->zip->add_data("formato_cc/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/formato_cc/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="ine"){
        $this->zip->add_data("ine/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/ine/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="cif"){
        $this->zip->add_data("cif/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/cif/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="curp"){
        $this->zip->add_data("curp/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/curp/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="comprobante_dom"){
        $this->zip->add_data("compro_dom/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/compro_dom/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="imgineal"){
        $this->zip->add_data("ine_apoderado/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/ine_apoderado/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="esc_ints"){
        $this->zip->add_data("escrituras/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/escrituras/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="poder_not_fac_serv"){
        $this->zip->add_data("poder_notarial/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/poder_notarial/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="doc extra"){
        $this->zip->add_data("extras/".$bene."/".$b->nombre_doc."",base_url()."uploads_benes/extras/".$b->nombre_doc);
      }
    }
    
    $this->zip->archive(base_url().'docs_benes.zip');
    if($cont>0){
      $this->zip->download('docs_benes.zip');
    }
  }*/

  function exportarBitacoraDocs($idb,$tipo,$idcli=0,$anio_opera=0){
    if($idb==0){
      $where=array("bd.id_cliente"=>$this->idcliente);
      if($idcli!=0){
        $id_cli = explode("-",$idcli);
        $where = array_merge($where,array("oc.id_clientec"=>$id_cli[0]));
        if($id_cli[3]!=0){
          $where = array_merge($where,array("o.id_union"=>$id_cli[3]));
        }
      }
      if($anio_opera!=0){
        $where = array_merge($where,array("DATE_FORMAT(oc.fecha_reg, '%Y') = "=>$anio_opera));
      }
    }else{
      $where=array("id_beneficiario"=>$idb,"tipo_bene"=>$tipo);
    }
    //agregar where en caso de cliente y año de opera

    $cont=0;
    //$bit = $this->ModeloCatalogos->getselectwherestatus("*","bitacora_docs_bene",$where);
    $bit = $this->ModeloCatalogos->getBitacoraDocsDB($where);
    $this->load->library('zip');
    foreach ($bit as $b) {
      //log_message('error', 'nombre_doc: '.$b->nombre_doc);
      $cont++;
      if($b->tipo_bene==1){
        $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$b->id_beneficiario);
        foreach ($get_pf as $item) { 
          $bene = mb_strtoupper($item->nombre." ".$item->apellido_paterno." ".$item->apellido_materno,"UTF-8");
        }
      }else if($b->tipo_bene==2){
        $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$b->id_beneficiario);
        foreach ($get_pmf as $item) {
          $bene=mb_strtoupper($item->razon_social,"UTF-8");
        }
      }else if($b->tipo_bene==3){
        $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$b->id_beneficiario);
        foreach ($get_pmf as $item) {
          $bene=mb_strtoupper($item->razon_social,"UTF-8");
        }
      }
      $bene=$this->eliminar_acentos($bene);

      $id_opera=$b->id_operacion;
      if($id_opera!=0){
        //$opera_carp=$id_opera."/";
        $opera_carp="OPERA_".$b->folio_cliente."/";
      }else{
        $opera_carp="";
      }
      $filePath = FCPATH . 'uploads_benes/';

      if($b->tipo_doc=="formato_cc"){
        $name_folder="formato_cc/";
      }
      else if($b->tipo_doc=="ine"){
        $name_folder="ine/";
      }
      else if($b->tipo_doc=="cif"){
        $name_folder="cif/";
      }
      else if($b->tipo_doc=="curp"){
        $name_folder="curp/";
      }
      else if($b->tipo_doc=="comprobante_dom"){
        $name_folder="compro_dom/";
      }
      else if($b->tipo_doc=="imgineal"){
        $name_folder="ine_apoderado/";
      }
      else if($b->tipo_doc=="esc_ints"){
        $name_folder="escrituras/";
      }
      else if($b->tipo_doc=="poder_not_fac_serv"){
        $name_folder="poder_notarial/";
      }
      else if($b->tipo_doc=="doc extra"){
        $name_folder="extras/";
      }

      //cambiar por año, clientes, operaciones, tipo doc, documento
      $doc_comp= $filePath.$name_folder.$b->nombre_doc;
      if (file_exists($doc_comp)) {
        if($b->tipo_doc=="formato_cc"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."formato_cc/".$b->nombre_doc, file_get_contents($filePath."formato_cc/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="ine"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."ine/".$b->nombre_doc, file_get_contents($filePath."ine/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="cif"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."cif/".$b->nombre_doc, file_get_contents($filePath."cif/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="curp"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."curp/".$b->nombre_doc, file_get_contents($filePath."curp/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="comprobante_dom"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."compro_dom/".$b->nombre_doc, file_get_contents($filePath."compro_dom/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="imgineal"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."ine_apoderado/".$b->nombre_doc, file_get_contents($filePath."ine_apoderado/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="esc_ints"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."escrituras/".$b->nombre_doc, file_get_contents($filePath."escrituras/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="poder_not_fac_serv"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."poder_notarial/".$b->nombre_doc, file_get_contents($filePath."poder_notarial/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="doc extra"){
          $this->zip->add_data($b->anio_opera."/".$bene."/".$opera_carp."extras/".$b->nombre_doc, file_get_contents($filePath."extras/".$b->nombre_doc));
        }
      }
    }

    $wheredb = array();
    if($idcli!=0){
      $id_cli = explode("-",$idcli);
      $wheredb = array_merge($wheredb,array("oc.id_clientec"=>$id_cli[0]));
      if($id_cli[3]!=0){
        $wheredb = array_merge($wheredb,array("o.id_union"=>$id_cli[3]));
      }
    }
    if($anio_opera!=0){
      $wheredb = array_merge($wheredb,array("DATE_FORMAT(oc.fecha_reg, '%Y') = "=>$anio_opera));
    }
    $bit_sdb = $this->ModeloCatalogos->getBitacoraDocsSinDB($this->idcliente,$wheredb);
    foreach ($bit_sdb as $b) {
      //log_message('error', 'nombre_doc: '.$b->nombre_doc);
      $cont++;

      $id_opera=$b->id_operacion;
      if($id_opera!=0){
        $opera_carp="OPERA_".$b->folio_cliente."/"; //CAMBIAR POR FOLIO DE CLIENTE EJEM: OPERA FOLIO
      }else{
        $opera_carp="";
      }
      $filePath = FCPATH . 'uploads/';
      $doc_comp= $filePath."doc_sin_bene/".$b->acuse_sin_doc;
      //año, clientes, operaciones, tipo doc, documento
      if(file_exists($doc_comp)){
        $this->zip->add_data($b->anio_opera."/"."SIN DB/".$opera_carp."".$b->acuse_sin_doc, file_get_contents($filePath."doc_sin_bene/".$b->acuse_sin_doc));
      }  
    }
    
    $fecha=date("YmdGis");
    $zipFilePath = FCPATH . $fecha.'docs_benes.zip';
    //log_message("error","zipFilePath: ".$zipFilePath);
    $this->session->set_userdata("name_zip_db_".$this->idcliente,$zipFilePath);
    $this->zip->archive($zipFilePath);
    //if($cont>0){
      $this->zip->download($fecha.'docs_benes.zip');
    //}

    if (file_exists($zipFilePath)) {
      //log_message("error","file_exists zipFilePath: ".$zipFilePath);
      //unlink($zipFilePath); 
    }
  }

  public function deleteZip(){
    $zipFilePath = $this->session->userdata('name_zip_db_'.$this->idcliente);
    log_message("error", "deleteZipCli: " . $zipFilePath);
    
    if(file_exists($zipFilePath)) {
      //log_message("error", "Archivo encontrado, eliminando: " . $zipFilePath);
      unlink($zipFilePath); // Elimina el archivo ZIP del servidor
    }else {
      log_message("error", "Archivo no encontrado: " . $zipFilePath);
    }
  }

  public function exportarBenes($id,$tipoc){
    if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
    if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
    if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
    if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
    if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
    if($tipoc==6) $tabla = "tipo_cliente_f";

    $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$id));
    if($tipoc==1 || $tipoc==2) $nombre = $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno;
    if($tipoc==3) $nombre = $get_per->razon_social;
    if($tipoc==4) $nombre = $get_per->nombre_persona;
    if($tipoc==5 || $tipoc==6) $nombre = $get_per->denominacion;

    $data['nombre']=$nombre;
    $data['id_perf'] = $id;
    $data['tipoc'] = $tipoc;

    $this->load->view('catalogos/c_c_beneficiario/exportarBeneficiarios',$data);
  }

  public function exportarBenesUnion($idu){
      $get_up=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array('id_union'=>$idu));
      $nombre="";
      foreach ($get_up as $k) {
        $get_perf=$this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$k->id_perfilamiento));
        $tipoc=$get_perf->idtipo_cliente;
        if($tipoc==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoc==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoc==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoc==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoc==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoc==6) $tabla = "tipo_cliente_f";

        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$k->id_perfilamiento));
        if($tipoc==1 || $tipoc==2) $nombre .= $get_per->nombre." ".$get_per->apellido_paterno." ".$get_per->apellido_materno."/ ";
        if($tipoc==3) $nombre .= $get_per->razon_social."/ ";
        if($tipoc==4) $nombre .= $get_per->nombre_persona."/ ";
        if($tipoc==5 || $tipoc==6) $nombre .= $get_per->denominacion."/ ";

        $data['nombre']=$nombre;
        $data['id_union'] = $idu;
      }

      $this->load->view('catalogos/c_c_beneficiario/exportarBenesUnion',$data);
    } 

}


