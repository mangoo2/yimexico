<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mis_datos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
	  $this->load->model('Login_model');
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        //ira el permiso del modulo
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,6);// 2 es el id del submenu
        if ($permiso==0) {
            redirect('/Sistema');
        }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
	public function index(){
      $data['idcliente']=$this->idcliente;
      $data['get_actividad_cli']=$this->ModeloCliente->getselectactividades($this->idcliente);
      $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
      $data['tipo_vialidad']=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      $data['nombre_actividad']='';
      $data['pais_nacimiento_m']='';
      $data['pais_nacimiento_nombre_m']='';
      foreach ($get_actividad as $item) {
        //<!-- Cliente
        $data['tipo_persona']=$item->tipo_persona;
        $data['telefono']=$item->telefono;
        $data['movil']=$item->movil;
        $data['correo']=$item->correo;
        //<!-- Cliente fisica -->
        $data['idfisica']=$item->idfisica;
        $data['nombre']=$item->nombre;
        $data['apellido_paterno']=$item->apellido_paterno;
        $data['apellido_materno']=$item->apellido_materno;
        $data['rfc']=$item->rfc;
        $data['fecha_constitucionf']=$item->fecha_constitucionf;
        $pais_nacimiento=$item->pais_nacimiento;
        $pais_nacionalidad_f=$item->pais_nacionalidad_f;
        $data['curp']=$item->curp;
        //<!-- Cliente moral -->
        $data['idmoral']=$item->idmoral;
        $data['razon_social']=$item->razon_social;
        $data['r_c_f']=$item->r_c_f;
        $data['fecha_constitucion']=$item->fecha_constitucion;
        $pais_nacionalidad_m=$item->pais_nacionalidad;
        $data['nombre_l']=$item->nombre_l;
        $data['apellido_paterno_l']=$item->apellido_paterno_l;
        $data['apellido_materno_l']=$item->apellido_materno_l;
        $data['fecha_nacimiento_l']=$item->fecha_nacimiento_l;
        $data['rfc_l']=$item->rfc_l;
        $data['curp_l']=$item->curp_l;
        //<!-- Cliente fideicomiso -->
        $data['idfideicomiso']=$item->idfideicomiso;
        $data['denominacion_razon_social']=$item->denominacion_razon_social;
        $data['rfc_fideicomiso']=$item->rfc_fideicomiso;
        $data['identificador_fideicomiso']=$item->identificador_fideicomiso;
        $data['nombre_lf']=$item->nombre_lf;
        $data['apellido_paterno_lf']=$item->apellido_paterno_lf;
        $data['apellido_materno_lf']=$item->apellido_materno_lf;
        $data['fecha_nacimiento_lf']=$item->fecha_nacimiento_lf;
        $data['rfc_lf']=$item->rfc_lf;
        $data['curp_lf']=$item->curp_lf;
      }
      //<!-- Cliente fisica -->
      $result_nacimiento=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacimiento);
      foreach ($result_nacimiento as $item){
          $data['pais_nacimiento_m']=$item->clave;
          $data['pais_nacimiento_nombre_m']=$item->pais;
      } 
      $result_nacionalidad=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacionalidad_f);
      foreach ($result_nacionalidad as $item){
          $data['pais_nacionalidad_f']=$item->clave;
          $data['pais_nacionalidad_f_nombre']=$item->pais;
      }
      //<!-- Cliente moral -->
      $result_nacionalidad_m=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacionalidad_m);
      foreach ($result_nacionalidad_m as $item){
          $data['pais_nacionalidad_m']=$item->clave;
          $data['pais_nacimiento_nombre_m']=$item->pais;
      } 
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/clientes/cliente',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/clientes/jsclienteyi');
	}
  public function doc(){
      $data['idcliente']=$this->idcliente;
      $data['get_actividad_cli']=$this->ModeloCliente->getselectactividades($this->idcliente);
      $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
      $data['get_u_u']=$this->ModeloCatalogos->getselectwhererow('usuarios','idcliente',$this->idcliente);
      foreach ($get_actividad as $item) {
        //<!-- Cliente
        $data['tipo_persona']=$item->tipo_persona;
        //<!-- Cliente fisica -->
        $data['idfisica']=$item->idfisica;
        $data['nombre']=$item->nombre;
        $data['apellido_paterno']=$item->apellido_paterno;
        $data['apellido_materno']=$item->apellido_materno;
        $data['rfc']=$item->rfc;
        $data['fecha_constitucionf']=$item->fecha_constitucionf;
        $pais_nacimiento=$item->pais_nacimiento;
        $pais_nacionalidad_f=$item->pais_nacionalidad_f;
        $data['curp']=$item->curp;
        //<!-- Cliente moral -->
        $data['idmoral']=$item->idmoral;
        $data['razon_social']=$item->razon_social;
        $data['r_c_f']=$item->r_c_f;
        $data['fecha_constitucion']=$item->fecha_constitucion;
        $pais_nacionalidad_m=$item->pais_nacionalidad;
        $data['nombre_l']=$item->nombre_l;
        $data['apellido_paterno_l']=$item->apellido_paterno_l;
        $data['apellido_materno_l']=$item->apellido_materno_l;
        $data['fecha_nacimiento_l']=$item->fecha_nacimiento_l;
        $data['rfc_l']=$item->rfc_l;
        $data['curp_l']=$item->curp_l;
        //<!-- Cliente fideicomiso -->
        $data['idfideicomiso']=$item->idfideicomiso;
        $data['denominacion_razon_social']=$item->denominacion_razon_social;
        $data['rfc_fideicomiso']=$item->rfc_fideicomiso;
        $data['identificador_fideicomiso']=$item->identificador_fideicomiso;
        $data['nombre_lf']=$item->nombre_lf;
        $data['apellido_paterno_lf']=$item->apellido_paterno_lf;
        $data['apellido_materno_lf']=$item->apellido_materno_lf;
        $data['fecha_nacimiento_lf']=$item->fecha_nacimiento_lf;
        $data['rfc_lf']=$item->rfc_lf;
        $data['curp_lf']=$item->curp_lf;
      }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/mis_datos',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/mis_datosjs');
  }
  //<!----- --->
  public function actualizar_cliente(){
    $data['idcliente']=$this->idcliente;
    $data['get_actividad_cli']=$this->ModeloCliente->getselectactividades($this->idcliente);
    $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
    $data['nombre_actividad']='';
    foreach ($get_actividad as $item) {
      $data['tipo_persona']=$item->tipo_persona;
      $data['nombre']=$item->nombre;
      $data['apellido_paterno']=$item->apellido_paterno;
      $data['apellido_materno']=$item->apellido_materno;
      $data['razon_social']=$item->razon_social;
      $data['denominacion_razon_social']=$item->denominacion_razon_social;
      $data['UsuarioID']=$item->UsuarioID;
      $data['Usuario']=$item->Usuario;
      $data['contrasena']='xxxxxx_xxxx';
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/clientes/inicio_cliente',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/jsinicio_cliente');
  }
  //<!----- --->
  public function registro_clientes_usuarios_yi(){ 
    $data=$this->input->post();
    $id=$data['UsuarioID'];
    $idcli=$data['idcliente'];
    $Usuario=$data['Usuario'];
    unset($data['UsuarioID']);
    unset($data['idcliente']);
    $pass = $data['contrasena'];
    $aux=0;
  
    $respuesta = $this->Login_model->login($Usuario);
    $contrasena='';

    foreach ($respuesta as $item) {
        $contrasena =$item->contrasena;
    }
    $verificar = password_verify($pass,$contrasena);
    //En caso afirmativo, inicializamos datos de sesión
    if ($verificar){
       $aux=1;
    }else{
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena']=$pass;
        $aux=2;
        $this->ModeloCatalogos->updateCatalogo($data,'UsuarioID',$id,'usuarios');
        $where_cliente = array('status'=>1,"cambio_pass"=>1);
        $this->ModeloCatalogos->updateCatalogo($where_cliente,'idcliente',$idcli,'cliente');
        $this->session->set_userdata('cambio_pass',1);
    }
    echo $aux;  
  }
  //<!----- --->
  public function registro_cliente(){   
      $data = $this->input->post();

      $id = $data['idcliente'];
      unset($data['idcliente']);
      $aux=0;
      if ($id>0) {
        $data['cambio_pass']=1;
        $this->ModeloCatalogos->updateCatalogo($data,'idcliente',$id,'cliente');
        $result=2;
        $aux=$id;
      }else{
          $data['fecha_alta_sistema']=$this->fechaactual;
          $data['cambio_pass']=0;
          $aux=$this->ModeloCatalogos->tabla_inserta('cliente',$data);
          $result=1;
      }   
      $arraydata = array('id'=>$aux,'status'=>$result);
      echo json_encode($arraydata);         
  }
  //<!----------->
  public function registro_fisica(){   
    $data = $this->input->post();
    $id = $data['idfisica'];
    unset($data['idfisica']);
    $aux=0;
    if ($id>0) {
      $this->ModeloCatalogos->updateCatalogo($data,'idfisica',$id,'cliente_fisica');
      $result=2;
      $aux=$id;
    }else{
      $id=$this->ModeloCatalogos->tabla_inserta('cliente_fisica',$data);
      $result=1;
    } 
    echo $id;
  }
  public function registro_moral(){   
    $data = $this->input->post();
    $id = $data['idmoral'];
    unset($data['idmoral']);
    $aux=0;
    if ($id>0) {
      $this->ModeloCatalogos->updateCatalogo($data,'idmoral',$id,'cliente_moral');
      $result=2;
      $aux=$id;
    }else{
      $id=$this->ModeloCatalogos->tabla_inserta('cliente_moral',$data);
      $result=1;
    }   
    echo $id;    
  }
  public function registro_fideicomiso(){   
    $data = $this->input->post();
    $id = $data['idfideicomiso'];
    unset($data['idfideicomiso']);
    $aux=0;
    if ($id>0) {
      $this->ModeloCatalogos->updateCatalogo($data,'idfideicomiso',$id,'cliente_fideicomiso');
      $result=2;
      $aux=$id;
    }else{
      $id=$this->ModeloCatalogos->tabla_inserta('cliente_fideicomiso',$data);
      $result=1;
    }       
    echo $id;
  }
  public function registro_direccion_sucursal(){
    $datos = $this->input->post('data');

    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
        $iddireccion=$DATA[$i]->iddireccion;
        $data['idcliente']=$this->idcliente;
        $data['vialidad']=$DATA[$i]->vialidad;
        $data['calle']=$DATA[$i]->calle;
        $data['no_ext']=$DATA[$i]->no_ext;  
        $data['no_int']=$DATA[$i]->no_int;
        $data['colonia']=$DATA[$i]->colonia;
        $data['municipio']=$DATA[$i]->municipio;
        $data['localidad']=$DATA[$i]->localidad;  
        $data['estado']=$DATA[$i]->estado;
        $data['cp']=$DATA[$i]->cp;
        $data['idpais']=$DATA[$i]->idpais;
        $data['alias']=$DATA[$i]->alias;   
        if($iddireccion==0){
          $iddireccion=$this->ModeloCatalogos->tabla_inserta('cliente_direccion',$data);
        }else{
          $this->ModeloCatalogos->updateCatalogo($data,'iddireccion',$iddireccion,'cliente_direccion');
        }
    }
  }
  function get_sucursal(){
    $id = $this->input->post('idcliente');
    $results=$this->ModeloCliente->getselecsucursal($id);
    echo json_encode($results);
  }
  function updateregistro_sucursal(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $this->ModeloCatalogos->updateCatalogo($data,'iddireccion',$id,'cliente_direccion');
  }
  public function searchpais(){
        $search = $this->input->get('search');
        $results=$this->ModeloCliente->get_select_like_pais($search);
        echo json_encode($results);    
  }
  public function get_estados(){
      $id_t = $this->input->post('id_t');
      $tex_t = $this->input->post('text_t');
      $html = '';
      $result = $this->ModeloCatalogos->getData('estado');
      $html.= '<select class="form-control estado_'.$tex_t.'_'.$id_t.'" id="estado">';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->estado.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
  public function get_tipo_vialidad(){
      $id_t = $this->input->post('id_t');
      $result=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      $html='';
      $html.= '<select class="form-control span_ptv_'.$id_t.'" id="vialidad" onchange="preGuardarSuc()">';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->nombre.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
  public function edit_usuario_cliente(){ 
    $data=$this->input->post();
    $id=$data['UsuarioID'];
    $Usuario=$data['Usuario'];
    unset($data['UsuarioID']);
    $pass = $data['contrasena'];
    $aux=0;
  
    $respuesta = $this->Login_model->login($Usuario);
    $contrasena='';

    foreach ($respuesta as $item) {
        $contrasena =$item->contrasena;
    }
    $verificar = password_verify($pass,$contrasena);
    //En caso afirmativo, inicializamos datos de sesión
    if ($verificar){
       $aux=1;
    }else{
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena']=$pass;
        $aux=2;
        $this->ModeloCatalogos->updateCatalogo($data,'UsuarioID',$id,'usuarios');
    }
    echo $aux;  
  }
}

/* End of file Clientes.php */
/* Location: ./application/controllers/Clientes.php */