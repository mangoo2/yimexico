<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estadisticas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
	  $this->load->model('Login_model');
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        $this->idcliente_usuario=$this->session->userdata('idcliente_usuario');
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
    $this->mesactual = date('m');
	}
	public function index(){      
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/estadisticas/estadisticas');
      $this->load->view('templates/footer');
      $this->load->view('catalogos/estadisticas/jsestadisticas');
	}
  public function bitacora_transacciones(){
      $data['mes_a']=$this->mesactual;
      $data['clientes_c']=$this->ModeloCliente->get_clientes_all($this->idcliente);
      $data['act']=$this->ModeloCliente->getActividadesCliente($this->idcliente);
      $data['cli_union'] = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
      //$get_perfil=$this->General_model->get_tableRow("perfilamiento",array('idcliente'=>$this->idcliente,"idtipo_cliente"=>$this->tipo));
      //$data["id_perfilamiento"]=$get_perfil->idperfilamiento;
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/estadisticas/bitacora_transacciones',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/estadisticas/jsbit_trans');
  }
  public function bitacora_expedientes(){
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/estadisticas/bitacora_expedientes');
      $this->load->view('templates/footer');
      $this->load->view('catalogos/estadisticas/jsbit_trans');
  }
  /// Listado de la tabla
  public function getListado_transacion(){
    $params = $this->input->post();
    $tipo = $this->input->post("tipo");
    $where="";
    //log_message('error', 'actividad: '.$params['actividad']);
    if($tipo==1 && $params['actividad']!=15)//aviso
      $where=array("xml"=>1);
    else if($tipo==1 && $params['actividad']==15)//aviso
      $where=array("xml"=>1,"id_pago_ayude"=>0);
    else if($tipo==2 && $params['actividad']!=15)//modificatorio
      $where=array("xml"=>1,"a8avi.id >"=>0);
    else if($tipo==2 && $params['actividad']==15)//modificatorio
      $where=array("xml"=>1,"id_pago_ayude"=>0);
    else if($tipo==3)//24 horas
      $where=array("LENGTH(resultado) >"=>"78");

    if($params['actividad']==11){
      $getdata = $this->ModeloCliente->get_c_c_transaccion($params,$this->idcliente,$where); //listo para acuse de sat
      $totaldata= $this->ModeloCliente->total_c_c_trasnsaccion($params,$this->idcliente,$where);  
    }else if($params['actividad']==19){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a15avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_transaccionA15($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_trasnsaccionA15($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==17){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a13avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_donativos13($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_donativos13($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==9){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a6avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo6($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo6($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==6){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a4avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo4($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo4($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==7){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a5avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo5a($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo5a($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==12){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a9avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo9($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo9($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==5){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a3avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo3($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo3($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==2){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a2avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo2a($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo2a($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==3){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a2avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo2b($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo2b($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==10){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"an7avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo7($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo7($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==4){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a2avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo2c($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo2c($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==1){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"an1avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo1($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo1($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==20){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"an16avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo16($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo16($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==8){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a5bvi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo5b($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo5b($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==14){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a11avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo11($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo11($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==15){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a12avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo12a($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo12a($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==13){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"an10avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo10($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo10($params,$this->idcliente,$where);  
    }
    else if($params['actividad']==16){
      if($tipo==2)//modificatorio
        $where=array("xml"=>1,"a12avi.id >"=>0);
      $getdata = $this->ModeloCliente->get_c_c_anexo12b($params,$this->idcliente,$where);
      $totaldata= $this->ModeloCliente->total_c_c_anexo12b($params,$this->idcliente,$where);  
    }
    else{
      $getdata = array(); 
      $totaldata= array();  
    }
    
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  public function consulta(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/estadisticas/consulta');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/estadisticas/jsestadisticas');
  }

  public function avisos(){
    $data['idcliente'] = $this->idcliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/estadisticas/avisos',$data);
    $this->load->view('templates/footer');
    //$this->load->view('catalogos/estadisticas/avisos');
  }

  public function aviso_cero(){
    $data['actividades']=$result = $this->ModeloCliente->getActividadesCliente($this->idcliente);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/estadisticas/bitacora_avisos',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/estadisticas/bibitacora_avisosjs');
  }

  public function avisos_generados(){
    $data['mes_a']=$this->mesactual;
    $data['clientes_c']=$this->ModeloCliente->get_clientes_all($this->idcliente);
    $data['act']=$this->ModeloCliente->getActividadesCliente($this->idcliente);
    $data['cli_union'] = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
    //$get_perfil=$this->General_model->get_tableRow("perfilamiento",array('idcliente'=>$this->idcliente,"idtipo_cliente"=>$this->tipo));
    //$data["id_perfilamiento"]=$get_perfil->idperfilamiento;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/estadisticas/bitacora_generado',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/estadisticas/jsbit_genera');
  }

  public function avisos_modificatorios(){
    $data['mes_a']=$this->mesactual;
    $data['clientes_c']=$this->ModeloCliente->get_clientes_all($this->idcliente);
    $data['act']=$this->ModeloCliente->getActividadesCliente($this->idcliente);
    $data['cli_union'] = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
    //$get_perfil=$this->General_model->get_tableRow("perfilamiento",array('idcliente'=>$this->idcliente,"idtipo_cliente"=>$this->tipo));
    //$data["id_perfilamiento"]=$get_perfil->idperfilamiento;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/estadisticas/bitacora_modificatorio',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/estadisticas/jsbit_modifica');
  }

  public function avisos_24hrs(){
    $data['mes_a']=$this->mesactual;
    $data['clientes_c']=$this->ModeloCliente->get_clientes_all($this->idcliente);
    $data['act']=$this->ModeloCliente->getActividadesCliente($this->idcliente);
    $data['cli_union'] = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
    //$get_perfil=$this->General_model->get_tableRow("perfilamiento",array('idcliente'=>$this->idcliente,"idtipo_cliente"=>$this->tipo));
    //$data["id_perfilamiento"]=$get_perfil->idperfilamiento;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/estadisticas/bitacora_24horas',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/estadisticas/jsbit_24horas');
  }

  public function getlistado_aviso_cero() {
    $params = $this->input->post();
    $params['idcliente']=$this->idcliente; 
    $getdata = $this->ModeloGeneral->get_aviso_cero($params);
    $totaldata= $this->ModeloGeneral->total_aviso_cero($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function delete_acuse($id,$tipo,$tipo_acuse){
    if($tipo==1)
      $tabla="acuses_generados";
    else if($tipo==2)
      $tabla="acuses_avisos";
    else if($tipo==3)
      $tabla="acuses_24hrs";

    if($tipo_acuse==1) //xml
      $name="acuse";
    else //pdf
      $name="acuse_pdf";
    $this->ModeloCatalogos->updateCatalogo(array($name=>""),'id',$id,$tabla);
  }

  public function cargafiles($id_a8,$id_act,$id_acu,$tipo,$tipo_acuse){
    $upload_folder ='uploads/acuses_generados';
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    //$archivador = $upload_folder . '/' . $nombre_archivo;
    //$fecha=date('ym-His');
    $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
    $nombre_archivo=str_replace('-', '_', $nombre_archivo);
    $nombre_archivo=$this->eliminar_acentos($nombre_archivo);
    $newfile= $nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    if($tipo==0){
      $tabla="acuses_generados";
    }else if($tipo==1){
      $tabla="acuses_avisos";
    }
    else if($tipo==2){
      $tabla="aviso_cero";
    }else if($tipo==3){
      $tabla="acuses_24hrs";
    }
    if($tipo_acuse==1){ //xml
      if($tipo==0 || $tipo==1 || $tipo==3){
        $name_col="acuse";
      }else if($tipo==2){
        $name_col="acuse_sat"; 
      }
    }
    if($tipo_acuse==2){ //pdf
      if($tipo==0 || $tipo==1 || $tipo==3){
        $name_col="acuse_pdf";
      }else if($tipo==2){
        $name_col="acuse_sat_pdf"; 
      }
    }
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
        $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      if($tipo==0 || $tipo==1 || $tipo==3){
        $array = array($name_col=>$newfile,"id_anexo"=>$id_act,"id_anexo_tabla"=>$id_a8,"fecha_reg"=>date("Y-m-d H:i:s")); 
      }else if($tipo==2){
        $array = array($name_col=>$newfile); 
      }
      //insert
      if($id_acu==0){
        $id_doc=$this->ModeloCatalogos->tabla_inserta($tabla,$array);
      }else{
        $this->ModeloCatalogos->updateCatalogo($array,'id',$id_acu,$tabla);
        $id_doc=$id_acu;
      }
      $return = Array('id'=>$id_doc,'ok'=>TRUE);
    }
    echo json_encode($return);
  }
  public function eliminar_acentos($cadena){
    //Reemplazamos la A y a
    $cadena = str_replace(
    array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
    array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
    $cadena
    );

    //Reemplazamos la E y e
    $cadena = str_replace(
    array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
    array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
    $cadena );

    //Reemplazamos la I y i
    $cadena = str_replace(
    array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
    array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
    $cadena );

    //Reemplazamos la O y o
    $cadena = str_replace(
    array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
    array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
    $cadena );

    //Reemplazamos la U y u
    $cadena = str_replace(
    array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
    array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
    $cadena );

    //Reemplazamos la N, n, C y c
    $cadena = str_replace(
    array('Ç', 'ç'),
    array('C', 'c'),
    $cadena
    );
    
    return $cadena;
  }


}