<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransaccionAn10 extends CI_Controller {
	
	public function __construct(){
        error_reporting(0);
		parent::__construct();
        $this->load->model('ModeloCliente');
        $this->load->model('ModeloCatalogos');
        $this->load->model('General_model');
        $this->load->model('ModeloTransaccion');
        $this->idcliente=$this->session->userdata('idcliente');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->idcliente=$this->session->userdata('idcliente');
            $this->usuarioid=$this->session->userdata('usuarioid');
            $this->tipo=$this->session->userdata('tipo');
            $this->status=$this->session->userdata('status');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,5);// 2 es el id del submenu
            /*if ($permiso==0) {
                redirect('/Sistema');
            }*/
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
	}


	public function index(){
		
	}

    public function tipoMoneda($r_moneda){
        if($r_moneda==0)//pesos
            $tipo_moneda=1;
        else if($r_moneda==1)//dolar americano
            $tipo_moneda=2;
        else if($r_moneda==2)//euro
            $tipo_moneda=3;
        else if($r_moneda==3)//libra esterlina
            $tipo_moneda=51;
        else if($r_moneda==4)//dolar canadiense
            $tipo_moneda=30;
        else if($r_moneda==5)//yuan chino
            $tipo_moneda=34;
        else if($r_moneda==6)//centenario
            $tipo_moneda=159;
        else if($r_moneda==7)//onza de plata
            $tipo_moneda=175;
        else if($r_moneda==8)//onza de oro
            $tipo_moneda=176;

        return $tipo_moneda;
    }

	public function registro_anexo10(){
        $data=$this->input->post();
        $tabla=$data["tabla"];
        $id_benet=$data["id_bene_transac"];
        $idp=$data["id_perfilamiento"];
        $idopera=$data['idopera'];
        $data['monto_opera']=$this->fromLTN($data['monto_opera']);
        $monto=$data['monto_opera'];

        $datatran['fecha_reg'] = date('Y-m-d H:i:s');
        $datatran['usuario_reg'] = $this->idcliente;//usuarioycliente
        $datatran['id_perfilamiento'] = $data["id_perfilamiento"];
        $datatran['id_clientec'] = $data["id_cliente_cliente"];
        $datatran["id_actividad"]=$this->input->post("id_actividad");
        $datatran["idtransbene"] = $id_benet;
        $aviso = $data['aviso'];
        $id_aviso = $data['id_aviso'];

        unset($data["aviso"]);
        unset($data["id_aviso"]);
        unset($data["tabla"]);
        unset($data["idopera"]);
        unset($data["id_actividad"]);
        $this->db->trans_start();
        $data['id_cliente'] = $this->idcliente;
        if($aviso==0)
            if($data['id']==0){ //insert
                $id=$this->ModeloCatalogos->tabla_inserta($tabla,$data);
                $datatran['id_transaccion'] = $id;
                //$idcct = $this->ModeloCatalogos->tabla_inserta('clientesc_transacciones',$datatran);
                $getOperas=$this->General_model->GetAllWhere('operacion_cliente',array('id_operacion'=>$idopera));
                foreach ($getOperas as $key) {
                    $datatran['id_clientec'] = $key->id_clientec;
                    $datatran['id_perfilamiento'] = $key->id_perfilamiento;
                    $idcct = $this->ModeloCatalogos->tabla_inserta('clientesc_transacciones',$datatran);
                }
                if($idopera>0){
                    //$this->ModeloCatalogos->updateCatalogoCT(array("id_cliente_transac"=>$idcct),'id',$idopera,'clientes_operaciones');
                    $this->ModeloCatalogos->updateCatalogoCT(array("id_cliente_transac"=>$idcct),'id',$idopera,'operaciones');
                }
            }
            else{ //update
                $id=$data["id"]; unset($data["id"]);
                log_message('error', 'edito al id: '.$id);
                $this->ModeloCatalogos->updateCatalogo($data,'id',$id,$tabla);
                $datatran['id_transaccion'] = $id;
                $getId=$this->General_model->GetAllWhere('clientesc_transacciones',array('id_transaccion'=>$id));
                
                foreach ($getId as $key) {
                    $idcct=$key->id;
                }
                //$this->ModeloCatalogos->updateCatalogoCT($datatran,'id_transaccion',$id,'clientesc_transacciones');
            }
        else{
            if($id_aviso==0){ //insert
                $data['idanexo10'] = $data['id']; unset($data["id"]);
                //$data['referencia_modifica'] = $data['referencia']; unset($data["referencia"]);
                $data[' fecha_creacion']=date("Y-m-d");
                $id=$this->ModeloCatalogos->tabla_inserta('anexo10_aviso',$data);
                $datatran['id_transaccion'] = $id;
                //AGREGADO PARA LA NUEVA ESTRUCTURA DE VARIOS CLIENTES POR OPERACION
                $getOperas=$this->General_model->GetAllWhere('operacion_cliente',array('id_operacion'=>$idopera));
                foreach ($getOperas as $key) {
                    $datatran['id_clientec'] = $key->id_clientec;
                    $datatran['id_perfilamiento'] = $key->id_perfilamiento;
                    $idcct = $this->ModeloCatalogos->tabla_inserta('clientesc_transacciones',$datatran);
                }
                /* *******************************************/ 
                if($idopera>0){
                    //$this->ModeloCatalogos->updateCatalogoCT(array("id_cliente_transac"=>$idcct),'id',$idopera,'clientes_operaciones');
                }
            }
            else{ //update
                $id=$id_aviso;
                //log_message('error', 'edito al id: '.$id); 
                unset($data["id"]); unset($data["idanexo10"]);
                //$data['referencia_modifica'] = $data['referencia']; unset($data["referencia"]);
                $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'anexo10_aviso');
                $datatran['id_transaccion'] = $id;
                $getId=$this->General_model->GetAllWhere('clientesc_transacciones',array('id_transaccion'=>$id));
                foreach ($getId as $key) {
                    $idcct=$key->id;
                }
                //$this->ModeloCatalogos->updateCatalogoCT($datatran,'id_transaccion',$id,'clientesc_transacciones');
            } 
        }

        $bita=$this->General_model->GetAllWhere('bitacora_xml',array('id_bene_transac'=>$id_benet));
        $cont2=0;
        foreach ($bita as $key) {
            $cont2++;
        }
        //log_message('error', 'cont2: '.$cont2); 
        if($cont2==0){
            $idb=$this->ModeloCatalogos->tabla_inserta('bitacora_xml',array('id_bene_transac'=>$id_benet,'id_clientec_transac'=>$idcct,'fecha_reg'=>date('Y-m-d H:i:s'),'id_perfilamiento'=>$idp));   
            //log_message('error', 'genera bitacora: '.$idb); 
        }
        if($cont2>0){
            $idb=$this->ModeloCatalogos->updateCatalogo2(array('id_bene_transac'=>$id_benet,'id_clientec_transac'=>$idcct,'fecha_reg'=>date('Y-m-d H:i:s'),'id_perfilamiento'=>$idp),array('id_bene_transac'=>$id_benet,'id_clientec_transac'=>$idcct),'bitacora_xml');   
            //log_message('error', 'edita bitacora: '.$idb); 
        }

        $this->db->trans_complete();
        $this->db->trans_status();
        //log_message('error', 'submit->idcct: '.$idcct);
        $arraydata = array('id'=>$id,"monto"=>$monto,'ok'=>'ok','id_clientec_transac'=>$idcct);
        echo json_encode($arraydata);
    }

    public function inset_pago_anexo10(){
        $data=$this->input->post();
        if (isset($data['monto_operacion'])) {
            $data['monto_operacion']=$this->fromLTN($data['monto_operacion']);
        }
        if (isset($data['monto_operacion'])){
            $data['monto_operacion']=$this->fromLTN($data['monto_operacion']);
        }
        if (isset($data['valor_objeto'])){
            $data['valor_objeto']=$this->fromLTN($data['valor_objeto']);
        }
        $id=$data['idliq'];
        $idanexo10=$data['idanexo10'];
        unset($data['idanexo10']);
        $aviso=$data['aviso'];
        unset($data['aviso']);
        unset($data['idliq']);

        $fecha="";
        $rfc="";
        if ($data['fechapf']!=""){
            $fecha=$data['fechapf'];
        }else{
            $fecha=$data['fechapm'];
        }
        if ($data['rfcpf']!=""){
            $rfc=$data['rfcpf'];
        }elseif($data['rfcpm']!=""){
            $rfc=$data['rfcpm'];
        }else{
            $rfc=$data['rfcf'];
        }
        $data['fecha']=$fecha;
        $data['rfc']=$rfc;

        unset($data["fechapf"]);
        unset($data["fechapm"]);
        unset($data["rfcpf"]);
        unset($data["rfcpm"]);
        unset($data["rfcf"]);

        if($aviso>0){
            $data['idanexo10_aviso']=$idanexo10;
            $getId=$this->General_model->GetAllWhere('pagos_transac_anexo10_aviso',array('id'=>$id));
            $aux_10=0;
            foreach ($getId as $key){
                $aux_10=1;
                $data['idanexo10_aviso']=$idanexo10; 
                if($id==$key->id){
                    $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'pagos_transac_anexo10_aviso');
                }else{
                    $this->ModeloCatalogos->tabla_inserta('pagos_transac_anexo10_aviso',$data);
                }
            }
            if($aux_10==0){
                $data['idanexo10_aviso']=$idanexo10;
                $this->ModeloCatalogos->tabla_inserta('pagos_transac_anexo10_aviso',$data);
            }
        }else{
            $data['idanexo10']=$idanexo10;
            if($id==0){
                $this->ModeloCatalogos->tabla_inserta('pagos_transac_anexo10',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'pagos_transac_anexo10');
            }
        }    
    }

    public function anexo10_xml($id,$idp,$aviso=0,$bitacora=0,$ido=0,$id_acusado,$aviso24=0,$clave=0,$descrip=0){
        $xml='';
        $xml_cli='';
        $xml_bene="";
        $xmla="";
        $xml_clia="";
        $xml_bene_acusa="";
        $xmlopera="";
        $xml_heada="";
        $get_per=$this->General_model->GetAllWhere('operacion_cliente',array("id_operacion"=>$ido,"activo"=>1));
        foreach ($get_per as $gp) {
            $idpc=$gp->id_perfilamiento;

            $get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idpc));//obtiene datos del perfilamiento
            //log_message('error', 'idperfilamiento: '.$idpc);
            if($get->idtipo_cliente==1){ 
                $tabla = "tipo_cliente_p_f_m";
                $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
                $idcc=$get_per->idtipo_cliente_p_f_m;
            }
            if($get->idtipo_cliente==2){ 
                $tabla = "tipo_cliente_p_f_e";
                $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
                $idcc=$get_per->idtipo_cliente_p_f_e;
            }
            if($get->idtipo_cliente==3){ 
                $tabla = "tipo_cliente_p_m_m_e";
                $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
                $idcc=$get_per->idtipo_cliente_p_m_m_e;
            }
            if($get->idtipo_cliente==4){ 
                $tabla = "tipo_cliente_p_m_m_d";
                $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
                $idcc=$get_per->idtipo_cliente_p_m_m_d;
            }
            if($get->idtipo_cliente==5){ 
                $tabla = "tipo_cliente_e_c_o_i";
                $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
                $idcc=$get_per->idtipo_cliente_e_c_o_i;
            }
            if($get->idtipo_cliente==6){ 
                $tabla = "tipo_cliente_f";
                $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
                $idcc=$get_per->idtipo_cliente_f;
            }   
            //log_message('error', 'idtipo_cliente: '.$get->idtipo_cliente);     

            $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
            foreach ($get_actividad as $item) {
                //<!-- Cliente fisica -->
                if($item->rfc!=""){
                    $rfc=$item->rfc;
                }
                //<!-- Cliente moral -->
                if($item->r_c_f!=""){
                    $rfc=$item->r_c_f;
                }
                //<!-- Cliente fideicomiso -->
                if($item->rfc_fideicomiso!=""){
                    $rfc=$item->rfc_fideicomiso;
                }
            }

            $cp_sucu="";
            $sucus=$this->ModeloCliente->getselecsucursal($this->idcliente);
            foreach ($sucus as $item) {
                $cp_sucu = $item->cp;
            }

            //$get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
            $nombref='';
            $apellido_paternof='';
            $apellido_maternof='';
            $fecha_nacimientof='';
            $pais_nacimientof='';
            $activida_o_girof='';
            $colonia_df='';
            $calle_df='';
            $no_ext_df='';
            $cp_df='';
            //////////////////
            $nombrem='';
            $razon_socialm='';
            $fecha_constitucion_dm='';
            $rfcm='';
            $nacionalidadm='';
            $giro_mercantilm='';
            $nombre_gm='';
            $apellido_paterno_gm='';
            $apellido_materno_gm='';
            $fecha_nacimientom='';
            $r_f_c_gm='';
            $curp_gm='';
            ///////////////////////
            $denominacionf='';
            $numero_referenciaf='';
            $nombre_gf='';
            $apellido_paterno_gf='';
            $apellido_materno_gf='';
            $fecha_nacimientof ='';
            ////////////////////////////////////
            $r_f_c_gm_tp3="";

            $r_f_c_gf="";
            $curp_gf="";
            $colonia_dm="";
            $calle_dm="";
            $no_ext_dm="";
            $no_int_dm="";
            $cp_dm="";
            $estado_dm="";
            $pais_dm="";
            $telefono_tm="";
            $correo_tm="";
            $actividad_o_girof="";
            $no_int_df="";
            $trata_persona="";
            $pais_nacionalidadf="";
            $estado_df="";
            $telefono_tf="";
            $correo_tf="";
            $r_f_c_tf="";
            $curp_tf="";
            $extranjerom="";
            $clave_registrof="";
            $pais_df="";//agregado faltan otras act.
            //log_message('error', 'idtipo_cliente: '.$get->idtipo_cliente);     
            if($get->idtipo_cliente==1){
                $nombref .= $get_per->nombre;
                $apellido_paternof .= $get_per->apellido_paterno;
                $apellido_maternof .= $get_per->apellido_materno;
                $fecha_nacimientof .= date("Ymd", strtotime($get_per->fecha_nacimiento));
                $pais_nacimientof .= $get_per->pais_nacimiento;
                $actividad_o_girof .= $get_per->activida_o_giro;
                $colonia_df .= $get_per->colonia_d;
                $calle_df .= $get_per->calle_d;
                $no_ext_df .= $get_per->no_ext_d;
                $no_int_df .= $get_per->no_int_d;
                $cp_df .= $get_per->cp_d;
                ////////////////////////
                $trata_persona .= $get_per->trantadose_persona;
                $pais_nacionalidadf .= $get_per->pais_nacionalidad;
                $estado_df .= $get_per->estado_d;
                $pais_df = $get_per->pais_d;
                $telefono_tf .= $get_per->telefono_t;
                $correo_tf .= $get_per->correo_t;
                $r_f_c_tf.=$get_per->r_f_c_t;
                $curp_tf.=$get_per->curp_t;
                //DUEÑO BENEFICIARIO //

            }if($get->idtipo_cliente==2){
                $nombref .= $get_per->nombre;
                $apellido_paternof .= $get_per->apellido_paterno;
                $apellido_maternof .= $get_per->apellido_materno;
                $fecha_nacimientof .= date("Ymd", strtotime($get_per->fecha_nacimiento));
                $pais_nacimientof .= $get_per->pais_nacimiento;
                $actividad_o_girof .= $get_per->actividad_o_giro;
                $colonia_df .= $get_per->colonia_d;
                $calle_df .= $get_per->calle_d;
                $no_ext_df .= $get_per->no_ext_d;
                $no_int_df .= $get_per->no_int_d;
                $cp_df .= $get_per->cp_d;
                ////////////////////////
                $pais_nacionalidadf .= $get_per->pais_nacionalidad;
                $estado_df .= $get_per->estado_d;
                //$pais_df .= $get_per->pais_d;
                $pais_df .= $get_per->pais_nacimiento;

                $telefono_tf .= $get_per->telefono_t;
                $correo_tf .= $get_per->correo_t;
                $r_f_c_tf.=$get_per->r_f_c_t;
                $curp_tf.=$get_per->curp_t;
            }if($get->idtipo_cliente==3){
                $razon_socialm .= $get_per->razon_social;
                $fecha_constitucion_dm .= $get_per->fecha_constitucion_d; 
                $nacionalidadm .= $get_per->nacionalidad;
                $giro_mercantilm .= $get_per->giro_mercantil;
                ///////////////////////////////////////
                $nombre_gm .= $get_per->nombre_g;
                $apellido_paterno_gm .= $get_per->apellido_paterno_g;
                $apellido_materno_gm .= $get_per->apellido_materno_g;
                $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));
                $r_f_c_gm_tp3 .= $get_per->r_f_c_g;
                $r_f_c_gm .= $get_per->clave_registro; 
                $curp_gm .= $get_per->curp_g;
                $extranjerom .= $get_per->extranjero;
                $colonia_dm .= $get_per->colonia_d;
                $calle_dm .= $get_per->calle_d;
                $no_ext_dm .= $get_per->no_ext_d;
                $no_int_dm .= $get_per->no_int_d; 
                $cp_dm .= $get_per->cp_d;

                $estado_dm .= $get_per->estado_d;
                $pais_dm .= $get_per->pais_d;
                $telefono_tm .= $get_per->telefono_d;
                $correo_tm .= $get_per->correo_d;
            }if($get->idtipo_cliente==4){
                $razon_socialm .= $get_per->nombre_persona;
                $fecha_constitucion_dm .= $get_per->fecha_constitucion_d;
                $nacionalidadm .= $get_per->nacionalidad;/// nacionalidad Falta
                $giro_mercantilm .= $get_per->giro_mercantil;/// giro mercantil Falta
                ///////////////////////////////////////
                $nombre_gm .= $get_per->nombre_g;
                $apellido_paterno_gm .= $get_per->apellido_paterno_g;
                $apellido_materno_gm .= $get_per->apellido_materno_g;
                //$fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));
                $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_constitucion_d)); //comentados ponerlo igual en otras act.
                $r_f_c_gm .= $get_per->rfc_g;
                $curp_gm .= $get_per->curp_g;             
                $extranjerom .= "si";
                $colonia_dm .= $get_per->colonia_d;
                $calle_dm .= $get_per->calle_d;
                $no_ext_dm .= $get_per->no_ext_d;
                $no_int_dm .= $get_per->no_int_d; 
                $cp_dm .= $get_per->cp_d;
                $estado_dm .= $get_per->estado_d;
                //$pais_dm .= $get_per->pais_d;
                $pais_dm .= "MX";

            }if($get->idtipo_cliente==5){//moral tipo_cliente_e_c_o_i
                $razon_socialm .= $get_per->denominacion;
                $fecha_constitucion_dm .= $get_per->fecha_establecimiento;
                $r_f_c_gm .= $get_per->clave_registro;// es el rfc
                $nacionalidadm .= $get_per->nacionalidad;//nacionalidadm Falta este campo 
                $giro_mercantilm .= $get_per->giro_mercantil;//giro_mercantilm Falta este campo 
                $pais_dm .= $get_per->pais;
                ///////////////////////////////////////
                $nombre_gm .= $get_per->nombre_g;
                $apellido_paterno_gm .= $get_per->apellido_paterno_g;
                $apellido_materno_gm .= $get_per->apellido_materno_g;
                $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));
                $r_f_c_gm .= $get_per->r_f_c_g;
                $curp_gm .= $get_per->curp_g;
                 ///////////////// DATOS DE DOMICILIO ////////////
                $extranjerom .= "";
                $colonia_dm .= $get_per->colonia_d;
                $calle_dm .= $get_per->calle_d;
                $no_ext_dm .= $get_per->no_ext_d;
                $no_int_dm .= $get_per->no_int_d; 
                $cp_dm .= $get_per->cp_d;
                $telefono_tm .= $get_per->telefono_d;
                $correo_tm .= $get_per->correo_d;
            }if($get->idtipo_cliente==6){//fideicomiso tipo_cliente_f
                $denominacionf.=$get_per->denominacion;
                $clave_registrof.=$get_per->clave_registro;
                $numero_referenciaf.=$get_per->numero_referencia;
                $nombre_gf.=$get_per->nombre_g;
                $apellido_paterno_gf.=$get_per->apellido_paterno_g;
                $apellido_materno_gf.=$get_per->apellido_materno_g;
                $fecha_nacimientof .=$get_per->fecha_nacimiento_g;//Falta este campo-- ya se agregó
                $r_f_c_gf .=$get_per->r_f_c_g;
                $curp_gf .=$get_per->curp_g;
            }      
        if($aviso==0){
            $get_anexo=$this->ModeloCatalogos->getselectwhere('anexo10','id',$id);
        }else{
            $get_anexo=$this->ModeloCatalogos->getselectwhere('anexo10_aviso','id',$id);
        }
        if($bitacora==1){
           $get_anexo=$this->ModeloCatalogos->getselectwhere('anexo10_aviso','idanexo10',$id); 
        }
        foreach ($get_anexo as $item){
            $fecha_operacion = date("Ymd", strtotime($item->fecha_operacion));

            $idanexo10=$item->id;
            $monto_opera=$item->monto_opera;
            //$anio_acuse=date("Ymd", strtotime($item->anio_acuse));
            $anio_acuse=$item->anio_acuse;
            $mes_acuse=$item->mes_acuse;
            $num_factura=$item->num_factura;
            $acuse=$anio_acuse.$mes_acuse;
            //$tipo_operacion=$item->tipo_operacion;
           // $linea_negocio=$item->linea_negocio;
            //$medio_operacion=$item->medio_operacion;

            $id_beneficiario = $item->id_bene_transac;
            if($aviso!=0){
                $descrip_modifica=$item->descrip_modifica;
                $folio_modificatorio=$item->folio_modificatorio;
                $ref_avi = $item->referencia_modifica;
                $anio_acuse=date("Y");
                if($id_acusado>0){ //para cuando acusa un aviso modificatorio y fue ayudado por sumatoria
                    $ref_avi_ant=$ref_avi;
                    $anio_acuse_folio_modificatorio = $anio_acuse."-".$folio_modificatorio;
                    $descrip_modifica_ant = $descrip_modifica;
                }
            }else{ 

                $ref_avi = $item->referencia;
            }
        }
        $get_cliente_sucu=$this->General_model->get_tableRow('usuarios',array('UsuarioID'=>$this->usuarioid));
        $get_cliente_direc=$this->General_model->get_tableRow('cliente_direccion',array('iddireccion'=>$get_cliente_sucu->idsucursal));
        if($get_cliente_direc!=""){
            $cp_cd=$get_cliente_direc->cp;
        }else{
            $cp_cd="CLIENTE NO TIENE CP ASIGNADO";
        }
           
        /** ********* PARA OBTENER LOS CLIENTES NORMALES ********* */
        $eti_f="";
            $cierra_eti_f="";//agregados, porner en otras act
        if($get->idtipo_cliente==1 || $get->idtipo_cliente==2){//Fisica
            $xml_cli.='<persona_aviso>
            <tipo_persona>
                <persona_fisica>
                    <nombre>'.mb_strtoupper($this->eliminar_acentos($nombref)).'</nombre>
                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paternof)).'</apellido_paterno>
                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_maternof)).'</apellido_materno>
                    <fecha_nacimiento>'.mb_strtoupper($fecha_nacimientof).'</fecha_nacimiento>';
                if($r_f_c_tf!=""){
                   $xml_cli.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_tf)).'</rfc>';
                }
                if($curp_tf!=""){
                   $xml_cli.='<curp>'.mb_strtoupper($this->eliminar_acentos($curp_tf)).'</curp>';
                } 
                $xml_cli.='<pais_nacionalidad>'.$pais_nacimientof.'</pais_nacionalidad>
                    <actividad_economica>'.$actividad_o_girof.'</actividad_economica>
                </persona_fisica>
            </tipo_persona>';
            $xml_cli.='<tipo_domicilio>';
                if($get->idtipo_cliente==1){
                    $eti_f="<nacional>";
                    $cierra_eti_f="</nacional>"; 
                }
                if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                    $eti_f="<extranjero>";
                    $cierra_eti_f="</extranjero>";
                }
                $xml_cli.=$eti_f;
                $xml_cli.='<colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                    <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                    <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>';
                    if($no_int_df!=""){
                        $xml_cli.='<numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_df)).'</numero_interior>';
                    }
                        
                $xml_cli.='<codigo_postal>'.$cp_df.'</codigo_postal>';
                if($get->idtipo_cliente==1 && $trata_persona!="" || $get->idtipo_cliente==2){
                    $xml_cli.='<pais>'.$pais_nacionalidadf.'</pais>
                            <estado_provincia>'.mb_strtoupper($estado_df).'</estado_provincia>';
                        if($eti_f=="<extranjero>"){   
                           $xml_cli.='<ciudad_poblacion>'.mb_strtoupper($pais_df).'</ciudad_poblacion>';

                        }
                }    
            $xml_cli.=$cierra_eti_f;                                
            $xml_cli.='</tipo_domicilio>';
                //if($get->idtipo_cliente==2 || $trata_persona!=""){
                $xml_cli.=' <telefono>
                            <clave_pais>'.$pais_df.'</clave_pais>
                            <numero_telefono>'.str_replace(' ','',$telefono_tf).'</numero_telefono>
                            <correo_electronico>'.mb_strtoupper($correo_tf).'</correo_electronico>
                        </telefono>';  
               //}
                                              
            $xml_cli.='</persona_aviso>';
        }if($get->idtipo_cliente==3 || $get->idtipo_cliente==4 || $get->idtipo_cliente==5){//Moral
            $xml.='<persona_aviso>
                <tipo_persona>
                    <persona_moral>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($razon_socialm)).'</denominacion_razon>
                        <fecha_constitucion>'.date("Ymd", strtotime($fecha_constitucion_dm)).'</fecha_constitucion>
                        <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>
                        <pais_nacionalidad>'.$nacionalidadm.'</pais_nacionalidad>';
                        if($get->idtipo_cliente!=5){
                            $xml.='<giro_mercantil>'.$giro_mercantilm.'</giro_mercantil>';
                        }
                //$xml.='</persona_moral>
                //</tipo_persona>';
                $xml.='<representante_apoderado>
                    <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gm)).'</nombre>
                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gm)).'</apellido_paterno>
                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gm)).'</apellido_materno>
                    <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientom)).'</fecha_nacimiento>';
                    if($r_f_c_gm!="" && $get->idtipo_cliente!=3){
                       $xml.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>';
                    }else{
                        $xml.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm_tp3)).'</rfc>';
                    }
                    if($curp_gm!=""){
                       $xml.='<curp>'.mb_strtoupper($this->eliminar_acentos($curp_gm)).'</curp>';
                    }
            

            $xml.='</representante_apoderado>';
            $xml.='</persona_moral>
                </tipo_persona>';
                $xml.='<tipo_domicilio>';
                    if($extranjerom!=""){
                        $eti_tipo_redi="<extranjero>";
                        $eti_tipo_cierra="</extranjero>";
                    }
                    else{
                       $eti_tipo_redi="<nacional>";
                       $eti_tipo_cierra="</nacional>";
                    }
                    $xml.= $eti_tipo_redi.
                        '<colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_dm)).'</colonia>
                        <calle>'.mb_strtoupper($this->eliminar_acentos($calle_dm)).'</calle>
                        <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_dm)).'</numero_exterior>';
                        if($no_int_dm!=""){
                            $xml.='<numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_dm)).'</numero_interior>';
                        }
                    $xml.='<codigo_postal>'.$cp_dm.'</codigo_postal>';

                        if($extranjerom!=""){
                            $xml.='<pais>'.$nacionalidadm.'</pais>
                                    <estado_provincia>'.mb_strtoupper($this->eliminar_acentos($estado_dm)).'</estado_provincia>';
                        }
                        if($get->idtipo_cliente==4){
                            $xml.='<ciudad_poblacion>MX</ciudad_poblacion>';
                        }
                        else{
                            if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                                $xml.='<ciudad_poblacion>'.$pais_dm.'</ciudad_poblacion>'; //agregar esto a los otros xmls    
                            }else{
                                $xml.='';
                            }
                            
                        }
                        
                    $xml.= $eti_tipo_cierra.
                        '</tipo_domicilio>';
                    if($get->idtipo_cliente==3){
                        $xml.='<telefono>
                                <clave_pais>'.$pais_dm.'</clave_pais>
                                  <numero_telefono>'.str_replace(' ','',$telefono_tm).'</numero_telefono>                                
                            </telefono>';  
                    }else{
                        $xml.='<telefono>
                                  <clave_pais>'.$pais_dm.'</clave_pais>                                    
                            </telefono>';  
                    }
                 $xml.='
            </persona_aviso>';     
        }if($get->idtipo_cliente==6){//Fideicomiso
            $xml.='<persona_aviso>
                <tipo_persona>
                    <fideicomiso>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($denominacionf)).'</denominacion_razon>
                        <rfc>'.mb_strtoupper($clave_registrof).'</rfc>
                        <identificador_fideicomiso>'.mb_strtoupper($numero_referenciaf).'</identificador_fideicomiso>
                        <apoderado_delegado>
                            <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gf)).'</nombre>
                            <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gf)).'</apellido_paterno>
                            <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gf)).'</apellido_materno>
                            <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientof)).'</fecha_nacimiento>';
                        if($r_f_c_gf!=""){
                           $xml.='<rfc>'.mb_strtoupper($r_f_c_gf).'</rfc>';
                        }
                        if($curp_gf!=""){
                           $xml.='<curp>'.mb_strtoupper($curp_gf).'</curp>';
                        }

                    $xml.='</apoderado_delegado>
                    </fideicomiso>
                </tipo_persona>';
                $xml.='<tipo_domicilio>
                    <nacional>
                        <colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                        <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                        <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>
                        <codigo_postal>'.$cp_df.'</codigo_postal>
                    </nacional>
                </tipo_domicilio>
            </persona_aviso>';
        } 

        /** ********* PARA OBTENER LOS CLIENTES CON AYUDA DE SUMATORIA ********* */
        if($get->idtipo_cliente==1 || $get->idtipo_cliente==2){//Fisica
            $xml_clia.='<persona_aviso>
                <tipo_persona>
                    <persona_fisica>
                        <nombre>'.mb_strtoupper($this->eliminar_acentos($nombref)).'</nombre>
                        <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paternof)).'</apellido_paterno>
                        <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_maternof)).'</apellido_materno>
                        <fecha_nacimiento>'.mb_strtoupper($fecha_nacimientof).'</fecha_nacimiento>';
                    if($r_f_c_tf!=""){
                       $xml_clia.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_tf)).'</rfc>';
                    }
                    if($curp_tf!=""){
                       $xml_clia.='<curp>'.mb_strtoupper($this->eliminar_acentos($curp_tf)).'</curp>';
                    } 
                    $xml_clia.='<pais_nacionalidad>'.$pais_nacimientof.'</pais_nacionalidad>
                        <actividad_economica>'.$actividad_o_girof.'</actividad_economica>
                    </persona_fisica>
                </tipo_persona>';
            $xml_clia.='<tipo_domicilio>';
                    if($get->idtipo_cliente==1){
                        $eti_f="<nacional>";
                        $cierra_eti_f="</nacional>"; 
                    }
                    if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                        $eti_f="<extranjero>";
                        $cierra_eti_f="</extranjero>";
                    }
                    $xml_clia.=$eti_f;
                    $xml_clia.='<colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                        <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                        <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>';
                        if($no_int_df!=""){
                            $xml_clia.='<numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_df)).'</numero_interior>';
                        }
                            
                    $xml_clia.='<codigo_postal>'.$cp_df.'</codigo_postal>';
                    if($get->idtipo_cliente==1 && $trata_persona!="" || $get->idtipo_cliente==2){
                        $xml_clia.='<pais>'.$pais_nacionalidadf.'</pais>
                                <estado_provincia>'.mb_strtoupper($estado_df).'</estado_provincia>';
                            if($eti_f=="<extranjero>"){   
                               $xml_clia.='<ciudad_poblacion>'.mb_strtoupper($pais_df).'</ciudad_poblacion>';

                            }
                    }    
                $xml_clia.=$cierra_eti_f;                                
            $xml_clia.='</tipo_domicilio>';
                    //if($get->idtipo_cliente==2 || $trata_persona!=""){
                    $xml_clia.=' <telefono>
                                <clave_pais>'.$pais_df.'</clave_pais>
                                <numero_telefono>'.str_replace(' ','',$telefono_tf).'</numero_telefono>
                                <correo_electronico>'.mb_strtoupper($correo_tf).'</correo_electronico>
                            </telefono>';  
                   //}
                                                  
            $xml_clia.='</persona_aviso>';
        }if($get->idtipo_cliente==3 || $get->idtipo_cliente==4 || $get->idtipo_cliente==5){//Moral
            $xml_clia.='<persona_aviso>
                <tipo_persona>
                    <persona_moral>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($razon_socialm)).'</denominacion_razon>
                        <fecha_constitucion>'.date("Ymd", strtotime($fecha_constitucion_dm)).'</fecha_constitucion>
                        <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>
                        <pais_nacionalidad>'.$nacionalidadm.'</pais_nacionalidad>';
                        if($get->idtipo_cliente!=5){
                            $xml_clia.='<giro_mercantil>'.$giro_mercantilm.'</giro_mercantil>';
                        }
                //$xml.='</persona_moral>
                //</tipo_persona>';
                $xml_clia.='<representante_apoderado>
                    <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gm)).'</nombre>
                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gm)).'</apellido_paterno>
                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gm)).'</apellido_materno>
                    <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientom)).'</fecha_nacimiento>';
                    if($r_f_c_gm!="" && $get->idtipo_cliente!=3){
                       $xml_clia.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>';
                    }else{
                        $xml_clia.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm_tp3)).'</rfc>';
                    }
                    if($curp_gm!=""){
                       $xml_clia.='<curp>'.mb_strtoupper($this->eliminar_acentos($curp_gm)).'</curp>';
                    }
            

            $xml_clia.='</representante_apoderado>';
            $xml_clia.='</persona_moral>
                </tipo_persona>';
                $xml_clia.='<tipo_domicilio>';
                    if($extranjerom!=""){
                        $eti_tipo_redi="<extranjero>";
                        $eti_tipo_cierra="</extranjero>";
                    }
                    else{
                       $eti_tipo_redi="<nacional>";
                       $eti_tipo_cierra="</nacional>";
                    }
                    $xml_clia.= $eti_tipo_redi.
                        '<colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_dm)).'</colonia>
                        <calle>'.mb_strtoupper($this->eliminar_acentos($calle_dm)).'</calle>
                        <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_dm)).'</numero_exterior>';
                        if($no_int_dm!=""){
                            $xml_clia.='<numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_dm)).'</numero_interior>';
                        }
                    $xml_clia.='<codigo_postal>'.$cp_dm.'</codigo_postal>';

                        if($extranjerom!=""){
                            $xml_clia.='<pais>'.$nacionalidadm.'</pais>
                                    <estado_provincia>'.mb_strtoupper($this->eliminar_acentos($estado_dm)).'</estado_provincia>';
                        }
                        if($get->idtipo_cliente==4){
                            $xml_clia.='<ciudad_poblacion>MX</ciudad_poblacion>';
                        }
                        else{
                            if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                                $xml_clia.='<ciudad_poblacion>'.$pais_dm.'</ciudad_poblacion>'; //agregar esto a los otros xmls    
                            }else{
                                $xml_clia.='';
                            }
                            
                        }
                        
                    $xml_clia.= $eti_tipo_cierra.
                        '</tipo_domicilio>';
                    if($get->idtipo_cliente==3){
                        $xml_clia.='<telefono>
                                <clave_pais>'.$pais_dm.'</clave_pais>
                                  <numero_telefono>'.str_replace(' ','',$telefono_tm).'</numero_telefono>                                
                            </telefono>';  
                    }else{
                        $xml_clia.='<telefono>
                                  <clave_pais>'.$pais_dm.'</clave_pais>                                    
                            </telefono>';  
                    }
                 $xml_clia.='
            </persona_aviso>';     
        }if($get->idtipo_cliente==6){//Fideicomiso
            $xml_clia.='<persona_aviso>
                <tipo_persona>
                    <fideicomiso>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($denominacionf)).'</denominacion_razon>
                        <rfc>'.mb_strtoupper($clave_registrof).'</rfc>
                        <identificador_fideicomiso>'.mb_strtoupper($numero_referenciaf).'</identificador_fideicomiso>
                        <apoderado_delegado>
                            <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gf)).'</nombre>
                            <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gf)).'</apellido_paterno>
                            <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gf)).'</apellido_materno>
                            <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientof)).'</fecha_nacimiento>';
                        if($r_f_c_gf!=""){
                           $xml_clia.='<rfc>'.mb_strtoupper($r_f_c_gf).'</rfc>';
                        }
                        if($curp_gf!=""){
                           $xml_clia.='<curp>'.mb_strtoupper($curp_gf).'</curp>';
                        }

                    $xml_clia.='</apoderado_delegado>
                    </fideicomiso>
                </tipo_persona>';
                $xml_clia.='<tipo_domicilio>
                    <nacional>
                        <colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                        <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                        <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>
                        <codigo_postal>'.$cp_df.'</codigo_postal>
                    </nacional>
                </tipo_domicilio>
            </persona_aviso>';
        }   
    }//FOR DE CLIENTES

    /** ********* PARA OBTENER LOS BENEFICIARIOS ********* */
    $get_bene_t=$this->General_model->GetAllWhere('operacion_beneficiario',array('id_operacion'=>$ido,"activo"=>1));
    foreach ($get_bene_t as $item) {
        $tipob = $item->tipo_bene;
        $id_dueno = $item->id_duenio_bene;
        if($tipob==1){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fisica',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $fecha_nac = date("Ymd", strtotime($item->fecha_nacimiento));
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <persona_fisica>
                                    <nombre>'.mb_strtoupper($this->eliminar_acentos($item->nombre)).'</nombre>
                                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_paterno)).'</apellido_paterno>
                                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_materno)).'</apellido_materno>
                                    <fecha_nacimiento>'.$fecha_nac.'</fecha_nacimiento>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->r_f_c_d)).'</rfc>
                                    <curp>'.mb_strtoupper($this->eliminar_acentos($item->curp_d)).'</curp>
                                    <pais_nacionalidad>'.$item->pais_nacionalidad.'</pais_nacionalidad>
                                </persona_fisica>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==2){
            $get_bene_m=$this->General_model->GetAllWhere('beneficiario_moral_moral',array('id'=>$id_dueno));
            foreach ($get_bene_m as $item) {
                $fecha_cons = date("Ymd", strtotime($item->fecha_constitucion));
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <persona_moral>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <fecha_constitucion>'.$fecha_cons.'</fecha_constitucion>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <pais_nacionalidad>'.$item->pais.'</pais_nacionalidad>
                                </persona_moral>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==3){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fideicomiso',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <fideicomiso>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <identificador_fideicomiso>'.$item->referencia.'</identificador_fideicomiso>
                                </fideicomiso>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }

        if($tipob==1){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fisica',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $fecha_nac = date("Ymd", strtotime($item->fecha_nacimiento));
                $xml_beneD='<tipo_persona>
                                <persona_fisica>
                                    <nombre>'.mb_strtoupper($this->eliminar_acentos($item->nombre)).'</nombre>
                                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_paterno)).'</apellido_paterno>
                                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_materno)).'</apellido_materno>
                                    <fecha_nacimiento>'.$fecha_nac.'</fecha_nacimiento>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->r_f_c_d)).'</rfc>
                                    <curp>'.mb_strtoupper($this->eliminar_acentos($item->curp_d)).'</curp>
                                    <pais_nacionalidad>'.$item->pais_nacionalidad.'</pais_nacionalidad>
                                </persona_fisica>
                            </tipo_persona>';
            }  
        }
        if($tipob==2){
            $get_bene_m=$this->General_model->GetAllWhere('beneficiario_moral_moral',array('id'=>$id_dueno));
            foreach ($get_bene_m as $item) {
                $fecha_cons = date("Ymd", strtotime($item->fecha_constitucion));
                $xml_beneD='<tipo_persona>
                                <persona_moral>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <fecha_constitucion>'.$fecha_cons.'</fecha_constitucion>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <pais_nacionalidad>'.$item->pais.'</pais_nacionalidad>
                                </persona_moral>
                            </tipo_persona>';
            }  
        }
        if($tipob==3){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fideicomiso',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $xml_beneD='<tipo_persona>
                                <fideicomiso>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <identificador_fideicomiso>'.$item->referencia.'</identificador_fideicomiso>
                                </fideicomiso>
                            </tipo_persona>';
            }  
        }

        if($tipob==0){
            $xml_bene.='';
            $xml_beneD='';
        }
    }

    if($id_acusado>0){
        //log_message('error', 'id_acusado if mayor que 0 para beneficiarios: '.$id_acusado); 
        $gob=$this->ModeloTransaccion->get_operaciones_transac10($id_acusado,13,$aviso,$bitacora);
        $get_bene_t=$this->General_model->GetAllWhere('operacion_beneficiario',array('id_operacion'=>$gob->id_operacion,"activo"=>1));
        foreach ($get_bene_t as $item) {
            $tipoba = $item->tipo_bene;
            $id_duenoa = $item->id_duenio_bene;
            //log_message('error', 'tipoba: '.$tipoba); 
            //log_message('error', 'id_duenoa: '.$id_duenoa); 
            if($tipoba==1){
                $get_bene_fa=$this->General_model->GetAllWhere('beneficiario_fisica',array('id'=>$id_duenoa));
                foreach ($get_bene_fa as $item) {
                    $fecha_nac = date("Ymd", strtotime($item->fecha_nacimiento));
                    $xml_bene_acusa.='<dueno_beneficiario> 
                                <tipo_persona>
                                    <persona_fisica>
                                        <nombre>'.mb_strtoupper($this->eliminar_acentos($item->nombre)).'</nombre>
                                        <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_paterno)).'</apellido_paterno>
                                        <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_materno)).'</apellido_materno>
                                        <fecha_nacimiento>'.$fecha_nac.'</fecha_nacimiento>
                                        <rfc>'.mb_strtoupper($this->eliminar_acentos($item->r_f_c_d)).'</rfc>
                                        <curp>'.mb_strtoupper($this->eliminar_acentos($item->curp_d)).'</curp>
                                        <pais_nacionalidad>'.$item->pais_nacionalidad.'</pais_nacionalidad>
                                    </persona_fisica>
                                </tipo_persona>
                            </dueno_beneficiario>';
                }  
            }
            if($tipoba==2){
                $get_bene_ma=$this->General_model->GetAllWhere('beneficiario_moral_moral',array('id'=>$id_duenoa));
                foreach ($get_bene_ma as $item) {
                    $fecha_cons = date("Ymd", strtotime($item->fecha_constitucion));
                    $xml_bene_acusa.='<dueno_beneficiario> 
                                <tipo_persona>
                                    <persona_moral>
                                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                        <fecha_constitucion>'.$fecha_cons.'</fecha_constitucion>
                                        <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                        <pais_nacionalidad>'.$item->pais.'</pais_nacionalidad>
                                    </persona_moral>
                                </tipo_persona>
                            </dueno_beneficiario>';
                }  
            }
            if($tipoba==3){
                $get_bene_fa=$this->General_model->GetAllWhere('beneficiario_fideicomiso',array('id'=>$id_duenoa));
                foreach ($get_bene_fa as $item) {
                    $xml_bene_acusa.='<dueno_beneficiario> 
                                <tipo_persona>
                                    <fideicomiso>
                                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                        <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                        <identificador_fideicomiso>'.$item->referencia.'</identificador_fideicomiso>
                                    </fideicomiso>
                                </tipo_persona>
                            </dueno_beneficiario>';
                }  
            }
            if($tipoba==0){
                $xml_bene_acusa.='';
            }
        }
    }
        
    //$msj_mes='<mes_reportado>'.$mesreporta.'</mes_reportado>';
    //$acuse="202003";

    $descrip ="<descripcion_alerta>ALGUN TIPO DE ALERTA</descripcion_alerta>";
    $descrip_alert="";
    if($clave=="9999"){
      $descrip_alert="
        <descripcion_alerta>".$descrip."</descripcion_alerta>";
    }
    if($aviso24==1){
        $xml_alerta='<prioridad>2</prioridad>
        <alerta>
          <tipo_alerta>'.$clave.'</tipo_alerta>
          '.$descrip_alert.'
        </alerta>';
    }else{
        $xml_alerta="
            <prioridad>1</prioridad>
            <alerta>
                <tipo_alerta>100</tipo_alerta>
                <descripcion_alerta>SIN ALERTA</descripcion_alerta>
            </alerta>";  
    }
    $xml_head='<archivo xmlns="http://www.uif.shcp.gob.mx/recepcion/tcv" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.uif.shcp.gob.mx/recepcion/tcv tcv.xsd">
                <informe>
                    <mes_reportado>'.$acuse.'</mes_reportado>
                    <sujeto_obligado>
                        <clave_sujeto_obligado>'.mb_strtoupper($this->eliminar_acentos($rfc)).'</clave_sujeto_obligado>
                        <clave_actividad>TCV</clave_actividad>
                    </sujeto_obligado>
                    <aviso>';
                 $xml_head.='<referencia_aviso>'.$ref_avi.'</referencia_aviso>';
                    if($aviso==1){
                        $xml_head.='<modificatorio>
                                <folio_modificacion>'.$anio_acuse."-".$folio_modificatorio.'</folio_modificacion>
                                <descripcion_modificacion>'.mb_strtoupper($this->eliminar_acentos($descrip_modifica)).'</descripcion_modificacion>
                            </modificatorio>';
                    }
            $xml_head.= $xml_alerta;
            $xml.= $xml_cli;
            $xml.= $xml_bene;

            $xml.='<detalle_operaciones>
                       <datos_operacion>';    
                    if($aviso==1){
                        $where_b = array('idanexo10_aviso'=>$idanexo10,'activo'=>1);
                        $get_anexo_liq=$this->General_model->GetAllWhere('pagos_transac_anexo10_aviso',$where_b);
                    }else{
                        $where_b = array('idanexo10'=>$idanexo10,'activo'=>1);
                        $get_anexo_liq=$this->General_model->GetAllWhere('pagos_transac_anexo10',$where_b);
                    }
                    foreach($get_anexo_liq as $item){
                        $xml.='<fecha_operacion>'.date("Ymd", strtotime($item->fecha_operacion)).'</fecha_operacion>
                        <tipo_operacion>'.$item->tipo_operacion.'</tipo_operacion>
                        <tipo_bien>';
                        if ($item->TipoBien==1){
                        	$xml.='<datos_efectivo_instrumentos>
                        	<instrumento_monetario>'.$item->instrumento_monetario.'</instrumento_monetario>
                        	<moneda>'.$this->tipoMoneda($item->moneda).'</moneda>
                        	<monto_operacion>'.$item->monto_operacion.'</monto_operacion>
                        	</datos_efectivo_instrumentos>';
                        }else{
                        	$xml.='<tipo_valor>'.$item->tipo_valor.'</tipo_valor>
                        	<valor_objeto>'.$item->valor_objeto.'</valor_objeto>
                        	<descripcion>'.$item->descripcion.'</descripcion>';
                        }
                        $xml.='</tipo_bien>';
                        if ($item->tipo_operacion!='1002'){
                        	$xml.='<recepcion>
                        	<tipo_servicio>'.$item->tipo_servicio.'</tipo_servicio>
                        	<fecha_recepcion>'.date("Ymd", strtotime($item->fecha_recepcion)).'</fecha_recepcion>
                        	<codigo_postal>'.$item->codigo_postal_1.'</codigo_postal>
                        	</recepcion>';
                        }
                        if ($item->tipo_operacion!='1001'){
                        	$xml.='<custodia>
                        		<fecha_inicio>'.date("Ymd", strtotime($item->fecha_inicio)).'</fecha_inicio>
                        		<fecha_fin>'.date("Ymd", strtotime($item->fecha_fin)).'</fecha_fin>
                        	</custodia>';
                        	$xml.='<tipo_custodia>';
                        	if ($item->bien_tc==1){
                                $succur=$this->General_model->get_tableRow("cliente_direccion","iddireccion=".$item->id_suc);
                        		$xml.='<datos_sucursal>
                            		<codigo_postal>'.$succur->cp.'</codigo_postal>
                            	</datos_sucursal></tipo_custodia>';
                        	}else{
                        		$xml.='<datos_no_sucursal>
                            		<colonia>'.$item->colonia.'</colonia>
                            		<calle>'.$item->calle.'</calle>
                            		<numero_exterior>'.$item->numero_exterior.'</numero_exterior>
                            		<numero_interior>'.$item->numero_interior.'</numero_interior>
                            		<codigo_postal>'.$item->codigo_postal_3.'</codigo_postal>
                            	</datos_no_sucursal></tipo_custodia>';
                        	}
                        }
                        if ($item->tipo_operacion!='1002'){
                        	$comp="";
                        	if ($item->tipo_entrega==1){
                        		$comp='<nacional>
                        		<codigo_postal>'.$item->codigo_postal_3.'</codigo_postal>
                        		</nacional>';
                        	}else{
                        		$comp='<extranjero>
                        		<pais>'.$item->pais.'</pais>
                        		<estado_provincia>'.$item->estado_provincia.'</estado_provincia>
                        		<ciudad_poblacion>'.$item->ciudad_poblacion.'</ciudad_poblacion>
                        		<codigo_postal>'.$item->codigo_postal_4.'</codigo_postal>
                        		</extranjero>';
                        	}
                        	$xml.='<entrega>
                        	<fecha_entrega>'.date("Ymd", strtotime($item->fecha_entrega)).'</fecha_entrega>
                        	<tipo_entrega>'.$comp.'</tipo_entrega>
                        	</entrega>';
                        }
                        if ($item->tipo_operacion != '1002'){
                        	$xml.='<destinatario><destinatario_persona_aviso>'.$item->destinatario.'
                        	</destinatario_persona_aviso>';
                            if ($item->destinatario=="NO"){
                                $xml.='<tipo_persona>';
                                if ($item->EDPOA=="PF"){
                                    $xml.='<persona_fisica>
                                    <nombre>'.$item->nombre.'</nombre>
                                    <apellido_paterno>'.$item->apellidop.'</apellido_paterno>
                                    <apellido_materno>'.$item->apellidom.'</apellido_materno>
                                    <fecha_nacimiento>'.date("Ymd", strtotime($item->fecha)).'</fecha_nacimiento>
                                    <rfc>'.$item->rfc.'</rfc>
                                    <curp>'.$item->curp.'</curp>
                                    </persona_fisica>';
                                }elseif($item->EDPOA=="PM"){
                                    $xml.='<persona_moral>
                                    <denominacion_razon>'.$item->razons.'</denominacion_razon>
                                    <fecha_constitucion>'.date("Ymd", strtotime($item->fecha)).'</fecha_constitucion>
                                    <rfc>'.$item->rfc.'</rfc>
                                    </persona_moral>';
                                }else{
                                    $xml.='<fideicomiso>
                                    <denominacion_razon>'.$item->razons.'</denominacion_razon>
                                    <rfc>'.$item->rfc.'</rfc>
                                    <identificador_fideicomiso>'.$item->identificador.'</identificador_fideicomiso>
                                    </fideicomiso>';
                                }
                                $xml.='</tipo_persona>';
                            }
                            $xml.='</destinatario>';
                        }
                     }
                      
            $xml.='</datos_operacion>
                </detalle_operaciones>
            </aviso>';
            if($id_acusado>0){ //fue ayudado por otra operacion en sumatoria
                if($aviso>0){ //es un aviso modificatorio
                    if($bitacora==0){
                        $get_anexo_acuse=$this->General_model->get_tableRow("pagos_transac_anexo10_aviso",array('idanexo10_aviso'=>$id,"activo"=>1));
                        $id_anexo=$get_anexo_acuse->idanexo10_aviso;
                    }else{
                        $get_anexo_acuse=$this->General_model->get_tableRow("pagos_transac_anexo10",array('idanexo10'=>$id,"activo"=>1));  
                        $id_anexo=$get_anexo_acuse->idanexo10;
                    }
                    
                    //$id_anexo=$get_anexo_acuse->idanexo10_aviso;
                    $get_anexo10a=$this->ModeloCatalogos->getselectwhere('anexo10_aviso','idanexo10',$id);
                    $pagos_ayuda=$this->ModeloCatalogos->getPagosOperaActAviso10($id,$bitacora);
                    foreach ($pagos_ayuda as $k) {
                        $id_ayuda=$k->id_pago_ayuda;
                    }
                    //log_message('error', 'id_ayuda en >0: '.$id_ayuda);
                    $get_id=$this->General_model->get_tableRow("pagos_transac_anexo10",array('id'=>$id_ayuda));
                    $get_anexo10a=$this->ModeloCatalogos->getselectwhere("anexo10",'id',$get_id->idanexo10);
                }else{
                    $get_anexo_acuse=$this->General_model->get_tableRow("pagos_transac_anexo10",array('id'=>$id_acusado,"activo"=>1));
                    $id_anexo=$get_anexo_acuse->idanexo10;
                    $get_anexo10a=$this->ModeloCatalogos->getselectwhere('anexo10','id',$id_anexo);
                }
            }else{ //no fue ayudado por el momento o viene de acuse modificatorio --parece ser qye ya no se ocupa
                if($aviso>0){
                    $get_anexo_acuse=$this->General_model->get_tableRow("pagos_transac_anexo10_aviso",array('id'=>$id,"activo"=>1));
                    $id_anexo=$get_anexo_acuse->idanexo10_aviso;
                    $get_anexo10a=$this->ModeloCatalogos->getselectwhere('anexo10_aviso','id',$id_anexo);
                }else{
                    $get_anexo_acuse=$this->General_model->get_tableRow("pagos_transac_anexo10",array('idanexo10'=>$id,"activo"=>1));
                    $id_anexo=$get_anexo_acuse->idanexo10;
                    $get_anexo10a=$this->ModeloCatalogos->getselectwhere('anexo10','id',$id);
                    //log_message('error', 'id_anexo: '.$id_anexo); 
                    $pagos_ayuda=$this->ModeloTransaccion->GetPagosAvisoAyuda10($id_anexo);
                    //log_message('error', 'id_pago_ayuda: '.$pagos_ayuda->id_pago_ayuda); 
                }
            }
            if($id_acusado>0 && $aviso==1 || $id_acusado>0 && $aviso==0 || $id_acusado==0 && $aviso==0 && isset($pagos_ayuda->id_pago_ayuda) && $pagos_ayuda->id_pago_ayuda>0 || $id_acusado==0 && $aviso>0 && isset($pagos_ayuda->id_pago_ayuda) && $pagos_ayuda->id_pago_ayuda>0){
                foreach ($get_anexo10a as $item){
                    $fecha_operacion = date("Ymd", strtotime($item->fecha_operacion));
                    $idanexo10=$item->id;
                    $monto_opera=$item->monto_opera;
                    //$anio_acuse=date("Ymd", strtotime($item->anio_acuse));
                    $anio_acuse=$item->anio_acuse;
                    $mes_acuse=$item->mes_acuse;
                    $num_factura=$item->num_factura;
                    $acuse=$anio_acuse.$mes_acuse;
                    //$tipo_operacion=$item->tipo_operacion;
                    //$linea_negocio=$item->linea_negocio;
                    //$medio_operacion=$item->medio_operacion;
                    $id_beneficiario = $item->id_bene_transac;
                }
                $xml_heada.='<aviso>';
                if($aviso==1 && $id_acusado>0) //viene de una ayuda
                    $xml_heada.='<referencia_aviso>'.$ref_avi_ant.'</referencia_aviso>';
                if($aviso==1 && $id_acusado>0){ //viene de una ayuda
                    $xml_heada.='<modificatorio>
                        <folio_modificacion>'.$anio_acuse_folio_modificatorio.'</folio_modificacion>
                        <descripcion_modificacion>'.mb_strtoupper($this->eliminar_acentos($descrip_modifica_ant)).'</descripcion_modificacion>
                    </modificatorio>';
                }
                if($aviso==0 && $id_acusado>0 || $aviso==0 && $id_acusado==0){
                    $xml_heada.='<referencia_aviso>'.$ref_avi.'</referencia_aviso>';
                }
                if($aviso==1 && $id_acusado==0){
                    $xml_heada.='<modificatorio>
                        <folio_modificacion>'.$anio_acuse."-".$folio_modificatorio.'</folio_modificacion>
                        <descripcion_modificacion>'.mb_strtoupper($this->eliminar_acentos($descrip_modifica)).'</descripcion_modificacion>
                    </modificatorio>';
                }
                $xml_heada.= $xml_alerta;
                $xmla.= $xml_clia;
                $xmla.= $xml_bene_acusa;
                $xmla.='<detalle_operaciones>
                       <datos_operacion>';    
                    /*if($aviso==1){
                        $where_b = array('idanexo10_aviso'=>$id_anexo,'activo'=>1);
                        $get_anexo_liq=$this->General_model->GetAllWhere('pagos_transac_anexo10_aviso',$where_b);
                    }else{*/
                       // $where_b = array('id'=>$id_ayuda,'activo'=>1);
                       // $get_anexo_liq=$this->General_model->GetAllWhere('pagos_transac_anexo10',$where_b);
                    //}
                    if($aviso==1){
                        $where_p = array('id'=>$id_ayuda,'activo'=>1);
                        //$get_anexo_liq=$this->General_model->GetAllWhere('pagos_transac_anexo6_bien_aviso',$where_b);
                        //$get_anexo_liq2=$this->General_model->GetAllWhere('pagos_transac_anexo6_pago_aviso',$where_b);
                    }else{
                        $where_p = array('idanexo10'=>$id_anexo,'activo'=>1);
                        //$get_anexo_liq=$this->General_model->GetAllWhere('pagos_transac_anexo6_bien',$where_p);
                    }
                    $get_anexo_liq=$this->General_model->GetAllWhere('pagos_transac_anexo10',$where_p);
                    foreach($get_anexo_liq as $item){
                        $xmla.='<fecha_operacion>'.date("Ymd", strtotime($item->fecha_operacion)).'</fecha_operacion>
                        <tipo_operacion>'.$item->tipo_operacion.'</tipo_operacion>
                        <tipo_bien>';
                        if ($item->TipoBien==1){
                            $xmla.='<datos_efectivo_instrumentos>
                            <instrumento_monetario>'.$item->instrumento_monetario.'</instrumento_monetario>
                            <moneda>'.$this->tipoMoneda($item->moneda).'</moneda>
                            <monto_operacion>'.$item->monto_operacion.'</monto_operacion>
                            </datos_efectivo_instrumentos>';
                        }else{
                            $xmla.='<tipo_valor>'.$item->tipo_valor.'</tipo_valor>
                            <valor_objeto>'.$item->valor_objeto.'</valor_objeto>
                            <descripcion>'.$item->descripcion.'</descripcion>';
                        }
                        $xml.='</tipo_bien>';
                        if ($item->tipo_operacion!='1002'){
                            $xmla.='<recepcion>
                            <tipo_servicio>'.$item->tipo_servicio.'</tipo_servicio>
                            <fecha_recepcion>'.date("Ymd", strtotime($item->fecha_recepcion)).'</fecha_recepcion>
                            <codigo_postal>'.$item->codigo_postal_1.'</codigo_postal>
                            </recepcion>';
                        }
                        if ($item->tipo_operacion!='1001'){
                            $xmla.='<custodia>
                                <fecha_inicio>'.date("Ymd", strtotime($item->fecha_inicio)).'</fecha_inicio>
                                <fecha_fin>'.date("Ymd", strtotime($item->fecha_fin)).'</fecha_fin>
                            </custodia>';
                            $xmla.='<tipo_custodia>';
                            if ($item->bien_tc==1){
                                $succur=$this->General_model->get_tableRow("cliente_direccion","iddireccion=".$item->id_suc);
                                $xmla.='<datos_sucursal>
                                    <codigo_postal>'.$succur->cp.'</codigo_postal>
                                </datos_sucursal></tipo_custodia>';
                            }else{
                                $xmla.='<datos_no_sucursal>
                                    <colonia>'.$item->colonia.'</colonia>
                                    <calle>'.$item->calle.'</calle>
                                    <numero_exterior>'.$item->numero_exterior.'</numero_exterior>
                                    <numero_interior>'.$item->numero_interior.'</numero_interior>
                                    <codigo_postal>'.$item->codigo_postal_3.'</codigo_postal>
                                </datos_no_sucursal></tipo_custodia>';
                            }
                        }
                        if ($item->tipo_operacion!='1002'){
                            $comp="";
                            if ($item->tipo_entrega==1){
                                $comp='<nacional>
                                <codigo_postal>'.$item->codigo_postal_3.'</codigo_postal>
                                </nacional>';
                            }else{
                                $comp='<extranjero>
                                <pais>'.$item->pais.'</pais>
                                <estado_provincia>'.$item->estado_provincia.'</estado_provincia>
                                <ciudad_poblacion>'.$item->ciudad_poblacion.'</ciudad_poblacion>
                                <codigo_postal>'.$item->codigo_postal_4.'</codigo_postal>
                                </extranjero>';
                            }
                            $xmla.='<entrega>
                            <fecha_entrega>'.date("Ymd", strtotime($item->fecha_entrega)).'</fecha_entrega>
                            <tipo_entrega>'.$comp.'</tipo_entrega>
                            </entrega>';
                        }
                        if ($item->tipo_operacion != '1002'){
                            $xmla.='<destinatario><destinatario_persona_aviso>'.$item->destinatario.'
                            </destinatario_persona_aviso>';
                            if ($item->destinatario=="NO"){
                                $xmla.='<tipo_persona>';
                                if ($item->EDPOA=="PF"){
                                    $xmla.='<persona_fisica>
                                    <nombre>'.$item->nombre.'</nombre>
                                    <apellido_paterno>'.$item->apellidop.'</apellido_paterno>
                                    <apellido_materno>'.$item->apellidom.'</apellido_materno>
                                    <fecha_nacimiento>'.date("Ymd", strtotime($item->fecha)).'</fecha_nacimiento>
                                    <rfc>'.$item->rfc.'</rfc>
                                    <curp>'.$item->curp.'</curp>
                                    </persona_fisica>';
                                }elseif($item->EDPOA=="PM"){
                                    $xmla.='<persona_moral>
                                    <denominacion_razon>'.$item->razons.'</denominacion_razon>
                                    <fecha_constitucion>'.date("Ymd", strtotime($item->fecha)).'</fecha_constitucion>
                                    <rfc>'.$item->rfc.'</rfc>
                                    </persona_moral>';
                                }else{
                                    $xmla.='<fideicomiso>
                                    <denominacion_razon>'.$item->razons.'</denominacion_razon>
                                    <rfc>'.$item->rfc.'</rfc>
                                    <identificador_fideicomiso>'.$item->identificador.'</identificador_fideicomiso>
                                    </fideicomiso>';
                                }
                                $xmla.='</tipo_persona>';
                            }
                            $xmla.='</destinatario>';
                        }
                    }
                      
                $xmla.='</datos_operacion>
                    </detalle_operaciones>
                </aviso>';
            }


        $xmlfinal='</informe>
            </archivo>';
        //var_dump($xml);die;
        $upload_folder ='uploads/anexo1';

        $fecha=date('ymd-His');
        //$doc = file_put_contents('uploads/anexo8/'.$fecha.'.xml',$xml);
        //fclose($doc);
        //$data = array('idperfilamiento'=>$idp,'nombre'=>$fecha);
        //$this->ModeloCatalogos->tabla_inserta('anexo8_xml',$data); //verificar si es necesario

        header('Expires: 0');
        header('Cache-control: private');
        header("Content-type: text/xml"); // Archivo xml
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Disposition: attachment; filename="'.$fecha.'-tcv.xml"');
        echo $xml_head.$xml.$xml_heada.$xmla.$xmlfinal;
    }

    public function fromLTN($info){
        $r1=str_replace(',','',$info);
        $result=str_replace('$','',$r1);
        return $result;
    }

    function eliminar_acentos($cadena){
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ç', 'ç'),
        array('C', 'c'),
        $cadena );
        
        return $cadena;
    }

}

/* End of file TransaccionAn10.php */
/* Location: ./application/controllers/TransaccionAn10.php */