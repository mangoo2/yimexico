<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alta_hacienda extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
	
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        //ira el permiso del modulo
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,7);// 2 es el id del submenu
        if ($permiso==0) {
            redirect('/Sistema');
        }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
	public function index(){
      $data['get_actividad_cli']=$this->ModeloCliente->getselectactividades($this->idcliente);
      $get_actividad=$this->ModeloCliente->get_clintes_all($this->idcliente);
      foreach ($get_actividad as $item) {
        //<!-- Cliente
        $data['tipo_persona']=$item->tipo_persona;
        //<!-- Cliente fisica -->
        $data['idfisica']=$item->idfisica;
        $data['nombre']=$item->nombre;
        $data['apellido_paterno']=$item->apellido_paterno;
        $data['apellido_materno']=$item->apellido_materno;
        $data['rfc']=$item->rfc;
        //<!-- Cliente moral -->
        $data['idmoral']=$item->idmoral;
        $data['razon_social']=$item->razon_social;
        $data['r_c_f']=$item->r_c_f;
        //<!-- Cliente fideicomiso -->
        $data['idfideicomiso']=$item->idfideicomiso;
        $data['denominacion_razon_social']=$item->denominacion_razon_social;
        $data['rfc_fideicomiso']=$item->rfc_fideicomiso;
      }
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/clientes/addhacienda',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/clientes/jshacienda');
	}
  public function anexo_a($id_act){
      $data['idcliente']=$this->idcliente;
      $data['act']=$this->General_model->GetAll('actividad');
      $data['get_actividad_cli']=$this->ModeloCliente->getselectactividades($this->idcliente);
      $actividad_r=$this->ModeloCatalogos->getselectwhere('actividad','id',$id_act);
      foreach ($actividad_r as $item){
        $data['id_act']=$item->id;
        $data['n_actividad']=$item->actividad;
      }
      $get_actividad=$this->ModeloCliente->get_clintes_all($this->idcliente);
      foreach ($get_actividad as $item) {
        //<!-- Cliente
        $data['tipo_persona']=$item->tipo_persona;
        //<!-- Cliente fisica -->
        $data['idfisica']=$item->idfisica;
        $data['nombre']=$item->nombre;
        $data['apellido_paterno']=$item->apellido_paterno;
        $data['apellido_materno']=$item->apellido_materno;
        $data['rfc']=$item->rfc;
        //<!-- Cliente moral -->
        $data['idmoral']=$item->idmoral;
        $data['razon_social']=$item->razon_social;
        $data['r_c_f']=$item->r_c_f;
        //<!-- Cliente fideicomiso -->
        $data['idfideicomiso']=$item->idfideicomiso;
        $data['denominacion_razon_social']=$item->denominacion_razon_social;
        $data['rfc_fideicomiso']=$item->rfc_fideicomiso;
      }
      $data['idanexoa']=0;
      $data['fecha_vulnerable']='';
      $data['tipo_fedatario']='';
      $data['categoria_actividad']=0;
      $data['modalidad_explotacion']='';
      $data['denominacion_razon_social']='';
      $data['rfc']='';
      $data['numero_folio']=0;
      $data['fecha_emision']='';
      $data['fecha_inicio_ampara']='';
      $data['fecha_termino_ampara']='';
      $data['telefono']=0;
      $data['celular']=0;
      $data['correo']='';
      $data['nombre_r']='';
      $data['apellido_paterno_r']='';
      $data['apellido_materno_r']='';
      $data['fecha_nacimiento_r']='';
      $data['rfc_r']='';
      $data['curp_r']='';
      $data['pais_nacionalidad_r']=0;
      $data['fecha_desigacion_r']='';
      $arrayact = array('idcliente'=>$this->idcliente,'actividad_vulnerable'=>$id_act); 
      $get_anexoa=$this->ModeloCatalogos->getselectwherestatus('*','anexoa',$arrayact);
      foreach ($get_anexoa as $itema){
        $data['idanexoa']=$itema->idanexoa;
        $data['fecha_vulnerable']=$itema->fecha_vulnerable;
        $data['tipo_fedatario']=$itema->tipo_fedatario;
        $data['categoria_actividad']=$itema->categoria_actividad;
        $data['modalidad_explotacion']=$itema->modalidad_explotacion;
        $data['denominacion_razon_social']=$itema->denominacion_razon_social;
        $data['rfc']=$itema->rfc;
        $data['numero_folio']=$itema->numero_folio;
        $data['fecha_emision']=$itema->fecha_emision;
        $data['fecha_inicio_ampara']=$itema->fecha_inicio_ampara;
        $data['fecha_termino_ampara']=$itema->fecha_termino_ampara;
        $data['telefono']=$itema->telefono;
        $data['celular']=$itema->celular;
        $data['correo']=$itema->correo;
        $data['nombre_r']=$itema->nombre_r;
        $data['apellido_paterno_r']=$itema->apellido_paterno_r;
        $data['apellido_materno_r']=$itema->apellido_materno_r;
        $data['fecha_nacimiento_r']=$itema->fecha_nacimiento_r;
        $data['rfc_r']=$itema->rfc_r;
        $data['curp_r']=$itema->curp_r;
        $data['pais_nacionalidad_r']=$itema->pais_nacionalidad_r;
        $data['fecha_desigacion_r']=$itema->fecha_desigacion_r;
      }
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/clientes/anexoa',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/clientes/jshacienda');
  }
  public function anexo_b($id_act){
      $data['idcliente']=$this->idcliente;
      $data['get_tipo_vialidad']=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      $data['get_actividad_cli']=$this->ModeloCliente->getselectactividades($this->idcliente);
      $actividad_r=$this->ModeloCatalogos->getselectwhere('actividad','id',$id_act);
      foreach ($actividad_r as $item){
        $data['id_act']=$item->id;
        $data['n_actividad']=$item->actividad;
      }
      $get_actividad=$this->ModeloCliente->get_clintes_all($this->idcliente);
      foreach ($get_actividad as $item) {
        //<!-- Cliente
        $data['tipo_persona']=$item->tipo_persona;
        //<!-- Cliente fisica -->
        $data['idfisica']=$item->idfisica;
        $data['nombre']=$item->nombre;
        $data['apellido_paterno']=$item->apellido_paterno;
        $data['apellido_materno']=$item->apellido_materno;
        $data['rfc']=$item->rfc;
        //<!-- Cliente moral -->
        $data['idmoral']=$item->idmoral;
        $data['razon_social']=$item->razon_social;
        $data['r_c_f']=$item->r_c_f;
        //<!-- Cliente fideicomiso -->
        $data['idfideicomiso']=$item->idfideicomiso;
        $data['denominacion_razon_social']=$item->denominacion_razon_social;
        $data['rfc_fideicomiso']=$item->rfc_fideicomiso;
      }
      $data['idanexob']=0;
      $data['nombre']='';
      $data['apellido_paterno']='';
      $data['apellido_materno']='';
      $data['fecha_nacimiento']='';
      $data['rcf']='';
      $data['curp']='';
      $data['pais_nacionalidad']='';
      $data['fecha_designacion']='';
      $data['acepto_designacion']='';
      $data['telefono']='';
      $data['celular']='';
      $data['celular']='';
      $data['correo']='';
      $data['tipo_vialidad']='';
      $data['calle']='';
      $data['no_ext']=0;
      $data['no_int']=0;
      $data['colonia']='';
      $data['municipio']='';
      $data['localidad']='';
      $data['estado']='';
      $data['cp']=0;
      $data['idpais']=0;
      $arrayact = array('idcliente'=>$this->idcliente,'actividad_vulnerable'=>$id_act); 
      $get_anexob=$this->ModeloCatalogos->getselectwherestatus('*','anexob',$arrayact);
      foreach ($get_anexob as $itema){
        $data['idanexob']=$itema->idanexob;
        $data['nombre']=$itema->nombre;
        $data['apellido_paterno']=$itema->apellido_paterno;
        $data['apellido_materno']=$itema->apellido_materno;
        $data['fecha_nacimiento']=$itema->fecha_nacimiento;
        $data['rcf']=$itema->rcf;
        $data['curp']=$itema->curp;
        $data['pais_nacionalidad']=$itema->pais_nacionalidad;
        $data['fecha_designacion']=$itema->fecha_designacion;
        $data['acepto_designacion']=$itema->acepto_designacion;
        $data['telefono']=$itema->telefono;
        $data['celular']=$itema->celular;
        $data['celular']=$itema->celular;
        $data['correo']=$itema->correo;
        $data['tipo_vialidad']=$itema->tipo_vialidad;
        $data['calle']=$itema->calle;
        $data['no_ext']=$itema->no_ext;
        $data['no_int']=$itema->no_int;
        $data['colonia']=$itema->colonia;
        $data['municipio']=$itema->municipio;
        $data['localidad']=$itema->localidad;
        $data['estado']=$itema->estado;
        $data['cp']=$itema->cp;
        $data['idpais']=$itema->idpais;
      }
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/clientes/anexob',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/clientes/jshacienda');
  }
  function registrar_hacienda(){
    $data = $this->input->post();
    $id = $data['idhacienda'];
    unset($data['idhacienda']);
    $aux=0;
    if ($id>0) {
        $this->ModeloCatalogos->updateCatalogo($data,'idhacienda',$id,'hacienda');
        $aux=$id;
    }else{
        $data['idcliente']=$this->idcliente;
        $aux=$this->ModeloCatalogos->tabla_inserta('hacienda',$data);
    }
    $arraydata = array('id'=>$aux);
    echo json_encode($arraydata);   
  }
  function registro_anexoa(){
    $data = $this->input->post();
    $id = $data['idanexoa'];
    unset($data['idanexoa']);
    $aux=0;
    if ($id>0) {
        $this->ModeloCatalogos->updateCatalogo($data,'idanexoa',$id,'anexoa');
        $result=2;
        $aux=$id;
    }else{
        $data['idcliente']=$this->idcliente;
        $aux=$this->ModeloCatalogos->tabla_inserta('anexoa',$data);
        $result=1;
    }   
    $arraydata = array('id'=>$aux,'status'=>$result);
    echo json_encode($arraydata);      
  }
  function get_actidades(){
    //$id = $this->input->post('idcliente');
    $results=$this->General_model->GetAll('actividad');;
    echo json_encode($results);
  }
  public function registro_direccion_anexoa(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
        $iddireccion=$DATA[$i]->iddireccion;
        $data['idanexoa']=$DATA[$i]->idanexoa;
        $data['vialidad']=$DATA[$i]->vialidad;
        $data['calle']=$DATA[$i]->calle;
        $data['no_ext']=$DATA[$i]->no_ext;  
        $data['no_int']=$DATA[$i]->no_int;
        $data['colonia']=$DATA[$i]->colonia;
        $data['municipio']=$DATA[$i]->municipio;
        $data['localidad']=$DATA[$i]->localidad;  
        $data['estado']=$DATA[$i]->estado;
        $data['cp']=$DATA[$i]->cp;
        $data['idpais']=$DATA[$i]->idpais;
        $data['idactividad']=$DATA[$i]->idactividad;   
        if($iddireccion==0){
          $this->ModeloCatalogos->tabla_inserta('anexoa_direccion',$data);
        }else{
          $this->ModeloCatalogos->updateCatalogo($data,'iddireccion',$iddireccion,'anexoa_direccion');
        }
    }
  }
  function get_direcciones_anexoa(){
    $id = $this->input->post('idcliente');
    $results=$this->ModeloCliente->getselectanexoa($id);;
    echo json_encode($results);
  }
  function updateregistro_anexo_a(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $this->ModeloCatalogos->updateCatalogo($data,'iddireccion',$id,'anexoa_direccion');
  }
  function check_direccion__cliente(){
    $result=$this->ModeloCliente->get_direccion_cliente($this->idcliente);
    foreach ($result as $item) {
      $vialidad=$item->vialidad;
      $calle=$item->calle;
      $no_ext=$item->no_ext;
      $no_int=$item->no_int;
      $colonia=$item->colonia;
      $municipio=$item->municipio;
      $localidad=$item->localidad;
      $estado=$item->estado;
      $cp=$item->cp;
      $idpais=$item->idpais;
      $pais=$item->pais;
    }
    $arraydata = array('vialidad'=>$vialidad,'calle'=>$calle,'no_ext'=>$no_ext,'no_int'=>$no_int,'colonia'=>$colonia,'municipio'=>$municipio,'localidad'=>$localidad,'estado'=>$estado,'cp'=>$cp,'idpais'=>$idpais,'pais'=>$pais);
    echo json_encode($arraydata);    
  }
  //<!----- Anexo b ----->
  function registro_anexob(){
    $data = $this->input->post();
    $id = $data['idanexob'];
    unset($data['idanexob']);
    $aux=0;
    if ($id>0) {
        $this->ModeloCatalogos->updateCatalogo($data,'idanexob',$id,'anexob');
        $result=2;
        $aux=$id;
    }else{
        $data['idcliente']=$this->idcliente;
        $aux=$this->ModeloCatalogos->tabla_inserta('anexob',$data);
        $result=1;
    }   
    $arraydata = array('id'=>$aux,'status'=>$result);
    echo json_encode($arraydata);      
  }
  /// Verifica que exista hacinda
  function get_hacienda(){
    $data = $this->input->post();
    $id = $data['idactividad'];
    $arraydata = array('actividad_vulnerable' =>$id,'idcliente'=>$this->idcliente);
    $idhacienda=0;
    $proceso_hacienda='';
    $anexo_a='';
    $anexo_b='';
    $all_hacienda=$this->ModeloCatalogos->getselectwherestatus('*','hacienda',$arraydata);
    foreach ($all_hacienda as $item) {
      $idhacienda=$item->idhacienda;
      $proceso_hacienda=$item->proceso_hacienda;
      $anexo_a=$item->anexo_a;
      $anexo_b=$item->anexo_b;
    }
    $arraydata = array('idhacienda'=>$idhacienda,'proceso_hacienda'=>$proceso_hacienda,'anexo_a'=>$anexo_a,'anexo_b'=>$anexo_b);
    echo json_encode($arraydata);      
  }
  public function get_tipo_vialidad(){
      $id_t = $this->input->post('id_t');
      $result=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
      $html='';
      $html.= '<select class="form-control span_ptv_'.$id_t.'" id="vialidad">';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->nombre.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
}
