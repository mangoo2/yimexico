<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //$this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }

	public function index(){
        $data['uma_precio']='';
        $data['uma_fecha']='';
        $uma_result=$this->ModeloCatalogos->getselectwhere_orden_asc('umas',array('status'=>1),'anio');
        foreach ($uma_result as $item){
            $data['uma_precio']=$item->valor;
            $data['uma_fecha']=$item->anio;
        }
        $data['dolar_precio']='';
        $data['dolar_fecha']='';
        $dolar_americano_result1=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>1,'status'=>1),'fecha');
        foreach ($dolar_americano_result1 as $item){
            $data['dolar_precio']=$item->valor;
            $data['dolar_fecha']=$item->fecha;
        }
        $data['dolar_cprecio']='';
        $data['dolar_cfecha']='';
        $dolar_americano_result1=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>4,'status'=>1),'fecha');
        foreach ($dolar_americano_result1 as $item){
            $data['dolar_cprecio']=$item->valor;
            $data['dolar_cfecha']=$item->fecha;
        }
        $data['euro_precio']='';
        $data['euro_fecha']='';
        $dolar_americano_result2=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>2,'status'=>1),'fecha');
        foreach ($dolar_americano_result2 as $item){
            $data['euro_precio']=$item->valor;
            $data['euro_fecha']=$item->fecha;
        }
        $data['libra_precio']='';
        $data['libra_fecha']='';
        $dolar_americano_result3=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>3,'status'=>1),'fecha');
        foreach ($dolar_americano_result3 as $item){
            $data['libra_precio']=$item->valor;
            $data['libra_fecha']=$item->fecha;
        }
        $data['chino_precio']='';
        $data['chino_fecha']='';
        $dolar_americano_result3=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>5,'status'=>1),'fecha');
        foreach ($dolar_americano_result3 as $item){
            $data['chino_precio']=$item->valor;
            $data['chino_fecha']=$item->fecha;
        }
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio',$data);
        $this->load->view('templates/footer');
        $this->load->view('jsinicio');
	}
    function generarcambio(){
        $result=$this->ModeloCatalogos->getdivisascambio();
        echo json_encode($result);
    }
    function insercambiodivisa(){
        $data = $this->input->post();
        $moneda=$data['moneda'];
        $valor=$data['valor'];
        $fecha=$data['fecha'];

        $this->ModeloCatalogos->tabla_inserta('divisas',array('fecha'=>$fecha,'moneda'=>$moneda,'valor'=>$valor));
    }
    
 
}