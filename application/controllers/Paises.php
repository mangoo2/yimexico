<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paises extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
	
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        //ira el permiso del modulo
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,17);// 2 es el id del submenu
        if ($permiso==0) {
          //redirect('/Sistema'); //comentado para pruebas 
        }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
  
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/vista');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/vistajs');
	}

    public function lugares_frontera(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/lugares_frontera');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/lugares_fronterajs');
    }

    public function puertos_entrada_salida_acuerdo_sistema_portuario_nacional(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/puertos_entrada_salida_acuerdo_sistema_portuario_nacional');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/puertos_entrada_salida_acuerdo_sistema_portuario_nacionaljs');
    }

    public function indice_basilea(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/indice_basilea');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/indice_basileajs');
    }

    public function indice_secrecia_bancaria(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/indice_secrecia_bancaria');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/indice_secrecia_bancariajs');
    }

    public function indice_corrupcion(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/indice_corrupcion');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/indice_corrupcionjs');
    }
    
    public function indice_gafi(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/indice_gafi');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/indice_gafijs');
    }

    public function incidencia_delictiva(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/incidencia_delictiva');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/incidencia_delictivajs');
    }

    public function percepcion_corrupcion_estatal_zonas_urbanas(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/percepcion_corrupcion_estatal_zonas_urbanas');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/percepcion_corrupcion_estatal_zonas_urbanasjs');
    }

    public function indice_paz(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/indice_paz');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/indice_pazjs');
    }

    public function paises_guia_terrorismo(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/paises_guia_terrorismo');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/paises_guia_terrorismojs');
    }

    public function indice_global_terrorismo(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/indice_global_terrorismo');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/indice_global_terrorismojs');
    }

    public function paises_indice_corrupcion_global(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/paises_indice_corrupcion_global');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/paises_indice_corrupcion_globaljs');
    }

    public function paises_regimenes_fiscales_preferentes(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/paises/paises_regimenes_fiscales_preferentes');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/paises/paises_regimenes_fiscales_preferentesjs');
    }

    /// Listado de la tabla
    public function getlistado_lugares_frontera(){
        $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_lugares_frontera($params);
        $totaldata= $this->ModeloGeneral->total_get_lugares_frontera($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_lugares_frontera(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_lugares_frontera');
    }
    /// Listado de la tabla
    public function getlistado_puertos_entrada_salida_acuerdo_sistema_portuario_nacional(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_paises_puertos_entrada_salida_acuerdo_sistema_portuario_nacional($params);
        $totaldata= $this->ModeloGeneral->total_get_paises_puertos_entrada_salida_acuerdo_sistema_portuario_nacional($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_puertos_entrada_salida_acuerdo_sistema_portuario_nacional(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_puertos_entrada_salida_acuerdo_sistema_portuario_nacional');
    }

    /// Listado de la tabla
    public function getlistado_indice_basilea(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_indice_basilea($params);
        $totaldata= $this->ModeloGeneral->total_indice_basilea($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_indice_basilea(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        if($data['riesgo']==1){
            $data['nivel_riesgo']='Bajo Riesgo';
        }else if($data['riesgo']==2){
            $data['nivel_riesgo']='Medio Riesgo';
        }else if($data['riesgo']==3){
            $data['nivel_riesgo']='Alto Riesgo';
        }
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_basilea');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_indice_basilea',$data);
        }
        
    }

    public function update_indice_basilea(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_basilea');
    }
    
    /// Listado de la tabla
    public function getlistado_indice_secrecia_bancaria(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_indice_secrecia_bancaria($params);
        $totaldata= $this->ModeloGeneral->total_indice_secrecia_bancaria($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_indice_secrecia_bancaria(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_secrecia_bancaria');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_indice_secrecia_bancaria',$data);
        }
    }

    public function update_indice_secrecia_bancaria(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_secrecia_bancaria');
    }

    /// Listado de la tabla
    public function getlistado_indice_corrupcion(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_indice_corrupcion($params);
        $totaldata= $this->ModeloGeneral->total_indice_corrupcion($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_indice_corrupcion(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_corrupcion');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_indice_corrupcion',$data);
        }
    }
    
    public function update_indice_corrupcion(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_corrupcion');
    }
    /// Listado de la tabla
    public function getlistado_indice_gafi(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_indice_gafi($params);
        $totaldata= $this->ModeloGeneral->total_indice_gafi($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_indice_gafi(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_gafi');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_indice_gafi',$data);
        }
    }

    public function update_indice_gafi(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_gafi');
    }

    /// Listado de la tabla
    public function getlistado_incidencia_delictiva(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_incidencia_delictiva($params);
        $totaldata= $this->ModeloGeneral->total_incidencia_delictiva($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_incidencia_delictiva(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_incidencia_delictiva');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_incidencia_delictiva',$data);
        }
    }
    
    public function update_incidencia_delictiva(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_incidencia_delictiva');
    }
     /// Listado de la tabla
    public function getlistado_percepcion_corrupcion_estatal_zonas_urbanas(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_percepcion_corrupcion_estatal_zonas_urbanas($params);
        $totaldata= $this->ModeloGeneral->total_percepcion_corrupcion_estatal_zonas_urbanas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_percepcion_corrupcion_estatal_zonas_urbanas(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_percepcion_corrupcion_estatal_zonas_urbanas');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_percepcion_corrupcion_estatal_zonas_urbanas',$data);
        }
    }

    public function update_percepcion_corrupcion_estatal_zonas_urbanas(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_percepcion_corrupcion_estatal_zonas_urbanas');
    }

    /// Listado de la tabla
    public function getlistado_indice_paz(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_indice_paz($params);
        $totaldata= $this->ModeloGeneral->total_indice_paz($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_indice_paz(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_paz');
    }

    /// Listado de la tabla
    public function getlistado_paises_guia_terrorismo(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_paises_guia_terrorismo($params);
        $totaldata= $this->ModeloGeneral->total_paises_guia_terrorismo($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_paises_guia_terrorismo(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_guia_terrorismo');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_guia_terrorismo',$data);
        }
    }

    public function update_guia_terrorismo(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_guia_terrorismo');
    }

    /// Listado de la tabla
    public function getlistado_indice_global_terrorismo(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_indice_global_terrorismo($params);
        $totaldata= $this->ModeloGeneral->total_indice_global_terrorismo($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_indice_global_terrorismo(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_global_terrorismo');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_indice_global_terrorismo',$data);
        }
    }

    public function update_paises_indice_global_terrorismo(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_global_terrorismo');
    }


    public function getlistado_paises_indice_corrupcion_global(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_paises_indice_corrupcion_global($params);
        $totaldata= $this->ModeloGeneral->total_paises_indice_corrupcion_global($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_paises_indice_corrupcion_global(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_corrupcion_global');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_indice_corrupcion_global',$data);
        }
    }

    public function update_paises_indice_corrupcion_global(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_indice_corrupcion_global');
    }

    public function getlistado_paises_regimenes_fiscales_preferentes(){
    $params = $this->input->post();
        $getdata = $this->ModeloGeneral->get_paises_regimenes_fiscales_preferentes($params);
        $totaldata= $this->ModeloGeneral->total_paises_regimenes_fiscales_preferentes($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_paises_regimenes_fiscales_preferentes(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']); 
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_regimenes_fiscales_preferentes');  
        }else{
            $this->ModeloCatalogos->tabla_inserta('paises_regimenes_fiscales_preferentes',$data);
        }
    }

    public function update_paises_regimenes_fiscales_preferentes(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'paises_regimenes_fiscales_preferentes');
    }

}