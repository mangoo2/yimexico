<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aviso24 extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
	
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        $this->usuarioid=$this->session->userdata('usuarioid');
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
  public function index(){

  }

  function get_alertas(){
    $id = $this->input->post('id');
    if($id==1){
      $results=$this->General_model->GetAll('alertas_anexo1');
    }else if($id==2){
      $results=$this->General_model->GetAll('alertas_anexo2a');
    }else if($id==3){
      $results=$this->General_model->GetAll('alertas_anexo2b');
    }else if($id==4){
      $results=$this->General_model->GetAll('alertas_anexo2c');
    }else if($id==5){
      $results=$this->General_model->GetAll('alertas_anexo3');
    }else if($id==6){
      $results=$this->General_model->GetAll('alertas_anexo4');
    }else if($id==7){
      $results=$this->General_model->GetAll('alertas_anexo5a');
    }else if($id==8){
      $results=$this->General_model->GetAll('alertas_anexo5b');
    }else if($id==9){
      $results=$this->General_model->GetAll('alertas_anexo6');
    }else if($id==10){
      $results=$this->General_model->GetAll('alertas_anexo7');
    }else if($id==11){
      $results=$this->General_model->GetAll('alertas_anexo8');
    }else if($id==12){
      $results=$this->General_model->GetAll('alertas_anexo9');
    }else if($id==13){
      $results=$this->General_model->GetAll('alertas_anexo10');
    }else if($id==14){
      $results=$this->General_model->GetAll('alertas_anexo11');
    }else if($id==15){
      $results=$this->General_model->GetAll('alertas_anexo12a');
    }else if($id==16){
      $results=$this->General_model->GetAll('alertas_anexo12b');
    }else if($id==17){
      $results=$this->General_model->GetAll('alertas_anexo13');
    }else if($id==19){
      $results=$this->General_model->GetAll('alertas_anexo15');
    }else if($id==20){
      $results=$this->General_model->GetAll('alertas_anexo16');
    }
    echo json_encode($results);
  }

	public function generarAviso($ido,$idp,$idtran=0,$act=0,$idpagoa=0,$desdebit){
    $data['actividades']=$this->ModeloCliente->getActividadesCliente($this->idcliente);
    $get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
    if($get->idtipo_cliente==1) $tabla = "tipo_cliente_p_f_m";
    if($get->idtipo_cliente==2) $tabla = "tipo_cliente_p_f_e";
    if($get->idtipo_cliente==3) $tabla = "tipo_cliente_p_m_m_e";
    if($get->idtipo_cliente==4) $tabla = "tipo_cliente_p_m_m_d";
    if($get->idtipo_cliente==5) $tabla = "tipo_cliente_e_c_o_i";
    if($get->idtipo_cliente==6) $tabla = "tipo_cliente_f";
    $data['cc']=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
    $data['tipoc'] = $get->idtipo_cliente; 
    $data['idperfilamiento']=$idp;  
    $data['ido']=$ido;  

    $data["idtran"]=$idtran;
    $data["id_act"]=$act;
    $data["idpagoa"]=$idpagoa;
    $data["desdebit"]=$desdebit;

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/aviso24',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/aviso24_js');
	}
  public function claves_anexo($idact){
    if($idact==1)
      $cv="jys";
    if($idact==2)
      $cv="tsc";
    if($idact==3)
      $cv="tpp";
    if($idact==4)
      $cv="tdr";
    if($idact==5)
      $cv="chv";
    if($idact==6)
      $cv="mpc";
    if($idact==7)
      $cv="din";
    if($idact==8)
      $cv="inm";
    if($idact==9)
      $cv="mjr";
    if($idact==10)
      $cv="oba";
    if($idact==11)
      $cv="veh";
    if($idact==12)
      $cv="bli";
    if($idact==13)
      $cv="tcv";
    if($idact==14)
      $cv="spr";
    if($idact==15)
      $cv="fep";
    if($idact==16)
      $cv="fes";
    if($idact==17)
      $cv="don";
    if($idact==18)
      $cv="";
    if($idact==19)
      $cv="ari";
    if($idact==20)
      $cv="avi";

    return $cv;
  }
  public function aviso24_xml($idp,$anio_acuse,$mes_acuse,$idact,$ido,$clave,$descrip,$id_anexo=0){
    $xml='';
    $xml_cli='';
    $xml_bene="";

    $cv=$this->claves_anexo($idact);

    $get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
    if($get->idtipo_cliente==1){ 
        $tabla = "tipo_cliente_p_f_m";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_f_m;
    }
    if($get->idtipo_cliente==2){ 
        $tabla = "tipo_cliente_p_f_e";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_f_e;
    }
    if($get->idtipo_cliente==3){ 
        $tabla = "tipo_cliente_p_m_m_e";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_m_m_e;
    }
    if($get->idtipo_cliente==4){ 
        $tabla = "tipo_cliente_p_m_m_d";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_m_m_d;
    }
    if($get->idtipo_cliente==5){ 
        $tabla = "tipo_cliente_e_c_o_i";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_e_c_o_i;
    }
    if($get->idtipo_cliente==6){ 
        $tabla = "tipo_cliente_f";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_f;
    }   
    //log_message('error', 'idtipo_cliente: '.$get->idtipo_cliente);     

    $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
    foreach ($get_actividad as $item) {
      //<!-- Cliente fisica -->
      if($item->rfc!=""){
          $rfc=$item->rfc;
      }
      //<!-- Cliente moral -->
      if($item->r_c_f!=""){
          $rfc=$item->r_c_f;
      }
      //<!-- Cliente fideicomiso -->
      if($item->rfc_fideicomiso!=""){
          $rfc=$item->rfc_fideicomiso;
      }
    }

    $cp_sucu="";
    $sucus=$this->ModeloCliente->getselecsucursal($this->idcliente);
    foreach ($sucus as $item) {
        $cp_sucu = $item->cp;
    }

    //$get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
    $nombref='';
    $apellido_paternof='';
    $apellido_maternof='';
    $fecha_nacimientof='';
    $pais_nacimientof='';
    $activida_o_girof='';
    $colonia_df='';
    $calle_df='';
    $no_ext_df='';
    $cp_df='';
    //////////////////
    $nombrem='';
    $razon_socialm='';
    $fecha_constitucion_dm='';
    $rfcm='';
    $nacionalidadm='';
    $giro_mercantilm='';
    $nombre_gm='';
    $apellido_paterno_gm='';
    $apellido_materno_gm='';
    $fecha_nacimientom='';
    $r_f_c_gm='';
    $curp_gm='';
    ///////////////////////
    $denominacionf='';
    $numero_referenciaf='';
    $nombre_gf='';
    $apellido_paterno_gf='';
    $apellido_materno_gf='';
    $fecha_nacimientof ='';
    ////////////////////////////////////
    $r_f_c_gm_tp3="";

    $r_f_c_gf="";
    $curp_gf="";
    $colonia_dm="";
    $calle_dm="";
    $no_ext_dm="";
    $no_int_dm="";
    $cp_dm="";
    $estado_dm="";
    $pais_dm="";
    $telefono_tm="";
    $correo_tm="";
    $actividad_o_girof="";
    $no_int_df="";
    $trata_persona="";
    $pais_nacionalidadf="";
    $estado_df="";
    $telefono_tf="";
    $correo_tf="";
    $r_f_c_tf="";
    $curp_tf="";
    $extranjerom="";
    $clave_registrof="";
    //log_message('error', 'idtipo_cliente: '.$get->idtipo_cliente);     
    if($get->idtipo_cliente==1){
        $nombref .= $get_per->nombre;
        $apellido_paternof .= $get_per->apellido_paterno;
        $apellido_maternof .= $get_per->apellido_materno;
        $fecha_nacimientof .= date("Ymd", strtotime($get_per->fecha_nacimiento));
        $pais_nacimientof .= $get_per->pais_nacimiento;
        $actividad_o_girof .= $get_per->activida_o_giro;
        $colonia_df .= $get_per->colonia_d;
        $calle_df .= $get_per->calle_d;
        $no_ext_df .= $get_per->no_ext_d;
        $no_int_df .= $get_per->no_int_d;
        $cp_df .= $get_per->cp_d;
        ////////////////////////
        $trata_persona .= $get_per->trantadose_persona;
        $pais_nacionalidadf .= $get_per->pais_nacionalidad;
        $estado_df .= $get_per->estado_d;
        $pais_df = $get_per->pais_d;
        $telefono_tf .= $get_per->telefono_t;
        $correo_tf .= $get_per->correo_t;
        $r_f_c_tf.=$get_per->r_f_c_t;
        $curp_tf.=$get_per->curp_t;
        //DUEÑO BENEFICIARIO //

    }if($get->idtipo_cliente==2){
        $nombref .= $get_per->nombre;
        $apellido_paternof .= $get_per->apellido_paterno;
        $apellido_maternof .= $get_per->apellido_materno;
        $fecha_nacimientof .= date("Ymd", strtotime($get_per->fecha_nacimiento));
        $pais_nacimientof .= $get_per->pais_nacimiento;
        $actividad_o_girof .= $get_per->actividad_o_giro;
        $colonia_df .= $get_per->colonia_d;
        $calle_df .= $get_per->calle_d;
        $no_ext_df .= $get_per->no_ext_d;
        $no_int_df .= $get_per->no_int_d;
        $cp_df .= $get_per->cp_d;
        ////////////////////////
        $pais_nacionalidadf .= $get_per->pais_nacionalidad;
        $estado_df .= $get_per->estado_d;
        $pais_df .= $get_per->pais_d;
        $telefono_tf .= $get_per->telefono_t;
        $correo_tf .= $get_per->correo_t;
        $r_f_c_tf.=$get_per->r_f_c_t;
        $curp_tf.=$get_per->curp_t;
    }if($get->idtipo_cliente==3){
        $razon_socialm .= $get_per->razon_social;
        $fecha_constitucion_dm .= $get_per->fecha_constitucion_d; 
        $nacionalidadm .= $get_per->nacionalidad;
        $giro_mercantilm .= $get_per->giro_mercantil;
        ///////////////////////////////////////
        $nombre_gm .= $get_per->nombre_g;
        $apellido_paterno_gm .= $get_per->apellido_paterno_g;
        $apellido_materno_gm .= $get_per->apellido_materno_g;
        $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));
        $r_f_c_gm_tp3 .= $get_per->r_f_c_g;
        $r_f_c_gm .= $get_per->clave_registro; 
        $curp_gm .= $get_per->curp_g;
        $extranjerom .= $get_per->extranjero;
        $colonia_dm .= $get_per->colonia_d;
        $calle_dm .= $get_per->calle_d;
        $no_ext_dm .= $get_per->no_ext_d;
        $no_int_dm .= $get_per->no_int_d; 
        $cp_dm .= $get_per->cp_d;

        $estado_dm .= $get_per->estado_d;
        $pais_dm .= $get_per->pais_d;
        $telefono_tm .= $get_per->telefono_d;
        $correo_tm .= $get_per->correo_d;
    }if($get->idtipo_cliente==4){
        $razon_socialm .= $get_per->nombre_persona;
        $fecha_constitucion_dm .= $get_per->fecha_constitucion_d;
        $nacionalidadm .= $get_per->nacionalidad;/// nacionalidad Falta
        $giro_mercantilm .= $get_per->giro_mercantil;/// giro mercantil Falta
        ///////////////////////////////////////
        $nombre_gm .= $get_per->nombre_g;
        $apellido_paterno_gm .= $get_per->apellido_paterno_g;
        $apellido_materno_gm .= $get_per->apellido_materno_g;
        $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));;
        $r_f_c_gm .= $get_per->rfc_g;
        $curp_gm .= $get_per->curp_g;             
        $extranjerom .= "si";
        $colonia_dm .= $get_per->colonia_d;
        $calle_dm .= $get_per->calle_d;
        $no_ext_dm .= $get_per->no_ext_d;
        $no_int_dm .= $get_per->no_int_d; 
        $cp_dm .= $get_per->cp_d;
        $estado_dm .= $get_per->estado_d;
        $pais_dm .= $get_per->pais_d;

    }if($get->idtipo_cliente==5){//moral tipo_cliente_e_c_o_i
        $razon_socialm .= $get_per->denominacion;
        $fecha_constitucion_dm .= $get_per->fecha_establecimiento;
        $r_f_c_gm .= $get_per->clave_registro;// es el rfc
        $nacionalidadm .= $get_per->nacionalidad;//nacionalidadm Falta este campo 
        $giro_mercantilm .= $get_per->giro_mercantil;//giro_mercantilm Falta este campo 
        $pais_dm .= $get_per->pais;
        ///////////////////////////////////////
        $nombre_gm .= $get_per->nombre_g;
        $apellido_paterno_gm .= $get_per->apellido_paterno_g;
        $apellido_materno_gm .= $get_per->apellido_materno_g;
        $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));
        $r_f_c_gm .= $get_per->r_f_c_g;
        $curp_gm .= $get_per->curp_g;
         ///////////////// DATOS DE DOMICILIO ////////////
        $extranjerom .= "";
        $colonia_dm .= $get_per->colonia_d;
        $calle_dm .= $get_per->calle_d;
        $no_ext_dm .= $get_per->no_ext_d;
        $no_int_dm .= $get_per->no_int_d; 
        $cp_dm .= $get_per->cp_d;
        $telefono_tm .= $get_per->telefono_d;
        $correo_tm .= $get_per->correo_d;
    }if($get->idtipo_cliente==6){//fideicomiso tipo_cliente_f
        $denominacionf.=$get_per->denominacion;
        $clave_registrof.=$get_per->clave_registro;
        $numero_referenciaf.=$get_per->numero_referencia;
        $nombre_gf.=$get_per->nombre_g;
        $apellido_paterno_gf.=$get_per->apellido_paterno_g;
        $apellido_materno_gf.=$get_per->apellido_materno_g;
        $fecha_nacimientof .=$get_per->fecha_nacimiento_g;//Falta este campo-- ya se agregó
        $r_f_c_gf .=$get_per->r_f_c_g;
        $curp_gf .=$get_per->curp_g;
    }

    $get_anexo8=$this->ModeloCatalogos->getselectwhere('anexo_8','id',$id_anexo);
    foreach ($get_anexo8 as $item){
      //$fecha_opera = date("Ym", strtotime($item->fecha_opera));
      $fecha_opera = date("Ymd", strtotime($item->fecha_opera));
      //tipo_opera
      $mesreporta = date("Ym", strtotime($item->fecha_opera));
      $ref_avi = $item->referencia;
      $tipo_opera = $item->tipo_opera;
      $monto_opera = $item->monto_opera;
      //$anio_acuse=date("Ymd", strtotime($item->anio_acuse));
      // benefs transaccion
      $id_beneficiario = $item->id_beneficiario;
      //
      //Tipo vehiculo
      $tipo_vehiculo = $item->tipo_vehiculo;
      // Vehiculo terrestre
      $marcat = $item->marcat;
      $modelot = $item->modelot;
      $aniot = $item->aniot;
      $num_vehit = $item->num_vehit;
      $num_reg_pubt = $item->num_reg_pubt;
      $matriculat = $item->matriculat;
      $blindajet = $item->blindajet;
      // Vehículo Maritimo
      $marcam = $item->marcam;
      $modelom = $item->modelom;
      $aniom = $item->aniom;
      $num_seriem = $item->num_seriem;
      $banderam = $item->banderam;
      $matriculam = $item->matriculam;
      $nivel_blindajem = $item->nivel_blindajem;
      // Vehículo aereo
      $marcaa = $item->marcaa;
      $modeloa = $item->modeloa;
      $anioa = $item->anioa;
      $num_seriea = $item->num_seriea;
      $bandera_aerea = $item->bandera_aerea;
      $matricula_aerea = $item->matricula_aerea;
      $nivel_blindaje_a = $item->nivel_blindaje_a;
    }
    $get_cliente_sucu=$this->General_model->get_tableRow('usuarios',array('UsuarioID'=>$this->usuarioid));
    $get_cliente_direc=$this->General_model->get_tableRow('cliente_direccion',array('iddireccion'=>$get_cliente_sucu->idsucursal));
    //$cp_cd=$get_cliente_direc->cp;
    if($get_cliente_direc!=""){
        $cp_cd=$get_cliente_direc->cp;
    }else{
        $cp_cd="CLIENTE NO TIENE CP ASIGNADO";
    }
           
    /** ********* PARA OBTENER LOS CLIENTES NORMALES ********* */
    if($get->idtipo_cliente==1 || $get->idtipo_cliente==2){//Fisica
        $xml_cli.='
        <persona_aviso>
        <tipo_persona>
            <persona_fisica>
                <nombre>'.mb_strtoupper($this->eliminar_acentos($nombref)).'</nombre>
                <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paternof)).'</apellido_paterno>
                <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_maternof)).'</apellido_materno>
                <fecha_nacimiento>'.mb_strtoupper($fecha_nacimientof).'</fecha_nacimiento>';
            if($r_f_c_tf!=""){
               $xml_cli.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_tf)).'</rfc>';
            }
            if($curp_tf!=""){
               $xml_cli.='<curp>'.mb_strtoupper($this->eliminar_acentos($curp_tf)).'</curp>';
            } 
            $xml_cli.='<pais_nacionalidad>'.$pais_nacimientof.'</pais_nacionalidad>
                <actividad_economica>'.$actividad_o_girof.'</actividad_economica>
            </persona_fisica>
        </tipo_persona>';
        $xml_cli.='
          <tipo_domicilio>';
            if($get->idtipo_cliente==1){
                $eti_f="
                  <nacional>";
                $cierra_eti_f="
                  </nacional>"; 
            }
            if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                $eti_f="
                  <extranjero>";
                $cierra_eti_f="
                  </extranjero>";
            }
            $xml_cli.=$eti_f;
            $xml_cli.='
              <colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>';
                if($no_int_df!=""){
                    $xml_cli.='
                      <numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_df)).'</numero_interior>';
                }
                    
            $xml_cli.='
              <codigo_postal>'.$cp_df.'</codigo_postal>';
            if($get->idtipo_cliente==1 && $trata_persona!="" || $get->idtipo_cliente==2){
                $xml_cli.='<pais>'.$pais_nacionalidadf.'</pais>
                        <estado_provincia>'.mb_strtoupper($estado_df).'</estado_provincia>';
                    if($eti_f=="<extranjero>"){   
                       $xml_cli.='<ciudad_poblacion>'.mb_strtoupper($pais_df).'</ciudad_poblacion>';

                    }
            }    
        $xml_cli.=$cierra_eti_f;                                
        $xml_cli.='
          </tipo_domicilio>';
            //if($get->idtipo_cliente==2 || $trata_persona!=""){
            $xml_cli.=' <telefono>
                        <clave_pais>'.$pais_df.'</clave_pais>
                        <numero_telefono>'.$telefono_tf.'</numero_telefono>
                        <correo_electronico>'.$correo_tf.'</correo_electronico>
                    </telefono>';  
           //}
                                          
        $xml_cli.='
        </persona_aviso>';
    }if($get->idtipo_cliente==3 || $get->idtipo_cliente==4 || $get->idtipo_cliente==5){//Moral
        $xml.='
        <persona_aviso>
            <tipo_persona>
                <persona_moral>
                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($razon_socialm)).'</denominacion_razon>
                    <fecha_constitucion>'.date("Ymd", strtotime($fecha_constitucion_dm)).'</fecha_constitucion>
                    <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>
                    <pais_nacionalidad>'.$nacionalidadm.'</pais_nacionalidad>';
                    if($get->idtipo_cliente!=5){
                        $xml.='
                          <giro_mercantil>'.$giro_mercantilm.'</giro_mercantil>';
                    }
            //$xml.='</persona_moral>
            //</tipo_persona>';
            $xml.='
              <representante_apoderado>
                <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gm)).'</nombre>
                <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gm)).'</apellido_paterno>
                <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gm)).'</apellido_materno>
                <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientom)).'</fecha_nacimiento>';
                if($r_f_c_gm!="" && $get->idtipo_cliente!=3){
                   $xml.='
                    <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>';
                }else{
                    $xml.='
                      <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm_tp3)).'</rfc>';
                }
                if($curp_gm!=""){
                   $xml.='
                    <curp>'.mb_strtoupper($this->eliminar_acentos($curp_gm)).'</curp>';
                }
        

        $xml.='
          </representante_apoderado>';
        $xml.='
          </persona_moral>
        </tipo_persona>';
            $xml.='
              <tipo_domicilio>';
                if($extranjerom!=""){
                    $eti_tipo_redi="
                      <extranjero>";
                    $eti_tipo_cierra="
                      </extranjero>";
                }
                else{
                   $eti_tipo_redi="
                    <nacional>";
                   $eti_tipo_cierra="
                    </nacional>";
                }
                $xml.= $eti_tipo_redi.
                    '<colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_dm)).'</colonia>
                    <calle>'.mb_strtoupper($this->eliminar_acentos($calle_dm)).'</calle>
                    <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_dm)).'</numero_exterior>';
                    if($no_int_dm!=""){
                        $xml.='
                          <numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_dm)).'</numero_interior>';
                    }
                $xml.='
                  <codigo_postal>'.$cp_dm.'</codigo_postal>';

                    if($extranjerom!=""){
                        $xml.='
                          <pais>'.$nacionalidadm.'</pais>
                                <estado_provincia>'.mb_strtoupper($this->eliminar_acentos($estado_dm)).'</estado_provincia>';
                    }
                    if($get->idtipo_cliente==4){
                        $xml.='
                          <ciudad_poblacion>MX</ciudad_poblacion>';
                    }
                    else{
                        if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                            $xml.='
                              <ciudad_poblacion>'.$pais_dm.'</ciudad_poblacion>'; //agregar esto a los otros xmls    
                        }else{
                            $xml.='';
                        }
                        
                    }
                    
                $xml.= $eti_tipo_cierra.
                    '</tipo_domicilio>';
                if($get->idtipo_cliente==3){
                    $xml.='
                      <telefono>
                        <clave_pais>'.$pais_dm.'</clave_pais>
                        <numero_telefono>'.$telefono_tm.'</numero_telefono>                                
                      </telefono>';  
                }else{
                    $xml.='
                      <telefono>
                        <clave_pais>'.$pais_dm.'</clave_pais>                                    
                      </telefono>';  
                }
             $xml.='
        </persona_aviso>';     
    }if($get->idtipo_cliente==6){//Fideicomiso
        $xml.='
        <persona_aviso>
            <tipo_persona>
                <fideicomiso>
                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($denominacionf)).'</denominacion_razon>
                    <rfc>'.mb_strtoupper($clave_registrof).'</rfc>
                    <identificador_fideicomiso>'.mb_strtoupper($numero_referenciaf).'</identificador_fideicomiso>
                    <apoderado_delegado>
                        <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gf)).'</nombre>
                        <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gf)).'</apellido_paterno>
                        <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gf)).'</apellido_materno>
                        <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientof)).'</fecha_nacimiento>';
                    if($r_f_c_gf!=""){
                       $xml.='
                        <rfc>'.mb_strtoupper($r_f_c_gf).'</rfc>';
                    }
                    if($curp_gf!=""){
                       $xml.='
                        <curp>'.mb_strtoupper($curp_gf).'</curp>';
                    }

                $xml.='</apoderado_delegado>
                </fideicomiso>
            </tipo_persona>';
            $xml.='
              <tipo_domicilio>
                <nacional>
                    <colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                    <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                    <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>
                    <codigo_postal>'.$cp_df.'</codigo_postal>
                </nacional>
            </tipo_domicilio>
        </persona_aviso>';
    } 

    /** ********* PARA OBTENER LOS BENEFICIARIOS ********* */
    $get_bene_t=$this->General_model->GetAllWhere('operacion_beneficiario',array('id_operacion'=>$ido,"activo"=>1));
    foreach ($get_bene_t as $item) {
        $tipob = $item->tipo_bene;
        $id_dueno = $item->id_duenio_bene;
        if($tipob==1){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fisica',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $fecha_nac = date("Ymd", strtotime($item->fecha_nacimiento));
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <persona_fisica>
                                    <nombre>'.mb_strtoupper($this->eliminar_acentos($item->nombre)).'</nombre>
                                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_paterno)).'</apellido_paterno>
                                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_materno)).'</apellido_materno>
                                    <fecha_nacimiento>'.$fecha_nac.'</fecha_nacimiento>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->r_f_c_d)).'</rfc>
                                    <curp>'.mb_strtoupper($this->eliminar_acentos($item->curp_d)).'</curp>
                                    <pais_nacionalidad>'.$item->pais_nacionalidad.'</pais_nacionalidad>
                                </persona_fisica>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==2){
            $get_bene_m=$this->General_model->GetAllWhere('beneficiario_moral_moral',array('id'=>$id_dueno));
            foreach ($get_bene_m as $item) {
                $fecha_cons = date("Ymd", strtotime($item->fecha_constitucion));
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <persona_moral>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <fecha_constitucion>'.$fecha_cons.'</fecha_constitucion>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <pais_nacionalidad>'.$item->pais.'</pais_nacionalidad>
                                </persona_moral>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==3){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fideicomiso',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <fideicomiso>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <identificador_fideicomiso>'.$item->referencia.'</identificador_fideicomiso>
                                </fideicomiso>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==0){
            $xml_bene.='';
        }
    }
    
    $descrip_alert="";
    if($clave=="9999"){
      $descrip_alert="
        <descripcion_alerta>".$descrip."</descripcion_alerta>";
    }

    $acuse=$anio_acuse.$mes_acuse;
    $descrip ="<descripcion_alerta>ALGUN TIPO DE ALERTA</descripcion_alerta>";
    $xml='<archivo xmlns="http://www.uif.shcp.gob.mx/recepcion/'.$cv.'" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.uif.shcp.gob.mx/recepcion/'.$cv.' '.$cv.'.xsd">
        <informe>
          <mes_reportado>'.$acuse.'</mes_reportado>
          <sujeto_obligado>
            <clave_sujeto_obligado>'.mb_strtoupper($this->eliminar_acentos($rfc)).'</clave_sujeto_obligado>
            <clave_actividad>'.strtoupper($cv).'</clave_actividad>
          </sujeto_obligado>';
    $xml.='
    <aviso>
            <prioridad>2</prioridad>
            <alerta>
              <tipo_alerta>'.$clave.'</tipo_alerta>
              '.$descrip_alert.'
            </alerta>';
            $xml.= $xml_cli;
            $xml.= $xml_bene;
      if($id_anexo>0){
        $xml.='<detalle_operaciones>
                <datos_operacion>
                    <fecha_operacion>'.$fecha_opera.'</fecha_operacion>
                    <codigo_postal>'.$cp_sucu.'</codigo_postal>
                    <tipo_operacion>'.$tipo_opera.'</tipo_operacion>'; //debe poner solo la clave
            if($tipo_vehiculo==1){//Vehículo Terrestre
                $xml.='<tipo_vehiculo>
                        <datos_vehiculo_terrestre>
                            <marca_fabricante>'.mb_strtoupper($this->eliminar_acentos($marcat)).'</marca_fabricante>
                            <modelo>'.mb_strtoupper($this->eliminar_acentos($modelot)).'</modelo>
                            <anio>'.$aniot.'</anio>';
                            if($num_vehit!=""){
                                $xml.='<vin>'.$num_vehit.'</vin>';
                            }
                            if($num_reg_pubt!=""){
                                $xml.='<repuve>'.mb_strtoupper($this->eliminar_acentos($num_reg_pubt)).'</repuve>';
                            }
                            if($matriculat!=""){
                                $xml.='<placas>'.mb_strtoupper($this->eliminar_acentos($matriculat)).'</placas>';
                            }
                        $xml.='<nivel_blindaje>'.$blindajet.'</nivel_blindaje>
                        </datos_vehiculo_terrestre>
                    </tipo_vehiculo>';
            }else if($tipo_vehiculo==2){//Vehículo Maritimo
                $xml.='<tipo_vehiculo>
                        <datos_vehiculo_maritimo>
                            <marca_fabricante>'.mb_strtoupper($this->eliminar_acentos($marcam)).'</marca_fabricante>
                            <modelo>'.mb_strtoupper($this->eliminar_acentos($modelom)).'</modelo>
                            <anio>'.mb_strtoupper($aniom).'</anio>
                            <numero_serie>'.mb_strtoupper($num_seriem).'</numero_serie>
                            <bandera>'.mb_strtoupper($banderam).'</bandera>';
                            if($matriculam!=""){
                                $xml.='<matricula>'.mb_strtoupper($matriculam).'</matricula>';
                            }
                            $xml.='<nivel_blindaje>'.$nivel_blindajem.'</nivel_blindaje>
                        </datos_vehiculo_maritimo>
                    </tipo_vehiculo>';
            }else if($tipo_vehiculo==3){//Vehículo Aéreo
                $xml.='<tipo_vehiculo>
                        <datos_vehiculo_aereo>
                            <marca_fabricante>'.mb_strtoupper($this->eliminar_acentos($marcaa)).'</marca_fabricante>
                            <modelo>'.mb_strtoupper($this->eliminar_acentos($modeloa)).'</modelo>
                            <anio>'.$anioa.'</anio>
                            <numero_serie>'.mb_strtoupper($num_seriea).'</numero_serie>
                            <bandera>'.$bandera_aerea.'</bandera>';
                            if($matricula_aerea!=""){
                                $xml.='<matricula>'.mb_strtoupper($matricula_aerea).'</matricula>';
                            }
                             $xml.='<nivel_blindaje>'.$nivel_blindaje_a.'</nivel_blindaje>
                        </datos_vehiculo_aereo>
                    </tipo_vehiculo>';
            }   
            $pagos=$this->General_model->GetAllWhere('pagos_transac_anexo_8',array('id_anexo_8'=>$id_anexo,'status'=>1));
            foreach ($pagos as $p) {
                $xml.='<datos_liquidacion>
                        <fecha_pago>'.date("Ymd", strtotime($p->fecha_pago)).'</fecha_pago>
                        <forma_pago>'.$p->forma_pago.'</forma_pago>
                        <instrumento_monetario>'.$p->instrum_notario.'</instrumento_monetario>
                        <moneda>'.$this->tipoMoneda($p->tipo_moneda).'</moneda>
                        <monto_operacion>'.$p->monto.'</monto_operacion>
                    </datos_liquidacion>';  
            }        
            
            $xml.='</datos_operacion>
                </detalle_operaciones>';
      }
    $xml.= '
          </aviso>';
    $xml.='
        </informe>
    </archivo>';
        $fecha=date('y-His');
        header('Expires: 0');
        header('Cache-control: private');
        header ("Content-type: text/xml"); // Archivo xml
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Disposition: attachment; filename="'.$fecha.''.$cv.'.xml"');
        echo $xml;
    }

  public function aviso24_xml_14($idp,$anio_acuse,$mes_acuse,$idact,$ido,$clave,$descrip,$id_anexo=0){ //anexo 11 servicios prof.
    $xml='';
    $xml_cli='';
    $xml_bene="";

    $cv=$this->claves_anexo($idact);

    $get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idp));
    if($get->idtipo_cliente==1){ 
        $tabla = "tipo_cliente_p_f_m";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_f_m;
    }
    if($get->idtipo_cliente==2){ 
        $tabla = "tipo_cliente_p_f_e";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_f_e;
    }
    if($get->idtipo_cliente==3){ 
        $tabla = "tipo_cliente_p_m_m_e";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_m_m_e;
    }
    if($get->idtipo_cliente==4){ 
        $tabla = "tipo_cliente_p_m_m_d";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_p_m_m_d;
    }
    if($get->idtipo_cliente==5){ 
        $tabla = "tipo_cliente_e_c_o_i";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_e_c_o_i;
    }
    if($get->idtipo_cliente==6){ 
        $tabla = "tipo_cliente_f";
        $get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
        $idcc=$get_per->idtipo_cliente_f;
    }   
    //log_message('error', 'idtipo_cliente: '.$get->idtipo_cliente);     

    $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
    foreach ($get_actividad as $item) {
      //<!-- Cliente fisica -->
      if($item->rfc!=""){
          $rfc=$item->rfc;
      }
      //<!-- Cliente moral -->
      if($item->r_c_f!=""){
          $rfc=$item->r_c_f;
      }
      //<!-- Cliente fideicomiso -->
      if($item->rfc_fideicomiso!=""){
          $rfc=$item->rfc_fideicomiso;
      }
    }

    $cp_sucu="";
    $sucus=$this->ModeloCliente->getselecsucursal($this->idcliente);
    foreach ($sucus as $item) {
        $cp_sucu = $item->cp;
    }

    //$get_per=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idp));
    $nombref='';
    $apellido_paternof='';
    $apellido_maternof='';
    $fecha_nacimientof='';
    $pais_nacimientof='';
    $activida_o_girof='';
    $colonia_df='';
    $calle_df='';
    $no_ext_df='';
    $cp_df='';
    //////////////////
    $nombrem='';
    $razon_socialm='';
    $fecha_constitucion_dm='';
    $rfcm='';
    $nacionalidadm='';
    $giro_mercantilm='';
    $nombre_gm='';
    $apellido_paterno_gm='';
    $apellido_materno_gm='';
    $fecha_nacimientom='';
    $r_f_c_gm='';
    $curp_gm='';
    ///////////////////////
    $denominacionf='';
    $numero_referenciaf='';
    $nombre_gf='';
    $apellido_paterno_gf='';
    $apellido_materno_gf='';
    $fecha_nacimientof ='';
    ////////////////////////////////////
    $r_f_c_gm_tp3="";

    $r_f_c_gf="";
    $curp_gf="";
    $colonia_dm="";
    $calle_dm="";
    $no_ext_dm="";
    $no_int_dm="";
    $cp_dm="";
    $estado_dm="";
    $pais_dm="";
    $telefono_tm="";
    $correo_tm="";
    $actividad_o_girof="";
    $no_int_df="";
    $trata_persona="";
    $pais_nacionalidadf="";
    $estado_df="";
    $telefono_tf="";
    $correo_tf="";
    $r_f_c_tf="";
    $curp_tf="";
    $extranjerom="";
    $clave_registrof="";
    //log_message('error', 'idtipo_cliente: '.$get->idtipo_cliente);     
    if($get->idtipo_cliente==1){
        $nombref .= $get_per->nombre;
        $apellido_paternof .= $get_per->apellido_paterno;
        $apellido_maternof .= $get_per->apellido_materno;
        $fecha_nacimientof .= date("Ymd", strtotime($get_per->fecha_nacimiento));
        $pais_nacimientof .= $get_per->pais_nacimiento;
        $actividad_o_girof .= $get_per->activida_o_giro;
        $colonia_df .= $get_per->colonia_d;
        $calle_df .= $get_per->calle_d;
        $no_ext_df .= $get_per->no_ext_d;
        $no_int_df .= $get_per->no_int_d;
        $cp_df .= $get_per->cp_d;
        ////////////////////////
        $trata_persona .= $get_per->trantadose_persona;
        $pais_nacionalidadf .= $get_per->pais_nacionalidad;
        $estado_df .= $get_per->estado_d;
        $pais_df = $get_per->pais_d;
        $telefono_tf .= $get_per->telefono_t;
        $correo_tf .= $get_per->correo_t;
        $r_f_c_tf.=$get_per->r_f_c_t;
        $curp_tf.=$get_per->curp_t;
        //DUEÑO BENEFICIARIO //

    }if($get->idtipo_cliente==2){
        $nombref .= $get_per->nombre;
        $apellido_paternof .= $get_per->apellido_paterno;
        $apellido_maternof .= $get_per->apellido_materno;
        $fecha_nacimientof .= date("Ymd", strtotime($get_per->fecha_nacimiento));
        $pais_nacimientof .= $get_per->pais_nacimiento;
        $actividad_o_girof .= $get_per->actividad_o_giro;
        $colonia_df .= $get_per->colonia_d;
        $calle_df .= $get_per->calle_d;
        $no_ext_df .= $get_per->no_ext_d;
        $no_int_df .= $get_per->no_int_d;
        $cp_df .= $get_per->cp_d;
        ////////////////////////
        $pais_nacionalidadf .= $get_per->pais_nacionalidad;
        $estado_df .= $get_per->estado_d;
        $pais_df .= $get_per->pais_d;
        $telefono_tf .= $get_per->telefono_t;
        $correo_tf .= $get_per->correo_t;
        $r_f_c_tf.=$get_per->r_f_c_t;
        $curp_tf.=$get_per->curp_t;
    }if($get->idtipo_cliente==3){
        $razon_socialm .= $get_per->razon_social;
        $fecha_constitucion_dm .= $get_per->fecha_constitucion_d; 
        $nacionalidadm .= $get_per->nacionalidad;
        $giro_mercantilm .= $get_per->giro_mercantil;
        ///////////////////////////////////////
        $nombre_gm .= $get_per->nombre_g;
        $apellido_paterno_gm .= $get_per->apellido_paterno_g;
        $apellido_materno_gm .= $get_per->apellido_materno_g;
        $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));
        $r_f_c_gm_tp3 .= $get_per->r_f_c_g;
        $r_f_c_gm .= $get_per->clave_registro; 
        $curp_gm .= $get_per->curp_g;
        $extranjerom .= $get_per->extranjero;
        $colonia_dm .= $get_per->colonia_d;
        $calle_dm .= $get_per->calle_d;
        $no_ext_dm .= $get_per->no_ext_d;
        $no_int_dm .= $get_per->no_int_d; 
        $cp_dm .= $get_per->cp_d;

        $estado_dm .= $get_per->estado_d;
        $pais_dm .= $get_per->pais_d;
        $telefono_tm .= $get_per->telefono_d;
        $correo_tm .= $get_per->correo_d;
    }if($get->idtipo_cliente==4){
        $razon_socialm .= $get_per->nombre_persona;
        $fecha_constitucion_dm .= $get_per->fecha_constitucion_d;
        $nacionalidadm .= $get_per->nacionalidad;/// nacionalidad Falta
        $giro_mercantilm .= $get_per->giro_mercantil;/// giro mercantil Falta
        ///////////////////////////////////////
        $nombre_gm .= $get_per->nombre_g;
        $apellido_paterno_gm .= $get_per->apellido_paterno_g;
        $apellido_materno_gm .= $get_per->apellido_materno_g;
        $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));;
        $r_f_c_gm .= $get_per->rfc_g;
        $curp_gm .= $get_per->curp_g;             
        $extranjerom .= "si";
        $colonia_dm .= $get_per->colonia_d;
        $calle_dm .= $get_per->calle_d;
        $no_ext_dm .= $get_per->no_ext_d;
        $no_int_dm .= $get_per->no_int_d; 
        $cp_dm .= $get_per->cp_d;
        $estado_dm .= $get_per->estado_d;
        $pais_dm .= $get_per->pais_d;

    }if($get->idtipo_cliente==5){//moral tipo_cliente_e_c_o_i
        $razon_socialm .= $get_per->denominacion;
        $fecha_constitucion_dm .= $get_per->fecha_establecimiento;
        $r_f_c_gm .= $get_per->clave_registro;// es el rfc
        $nacionalidadm .= $get_per->nacionalidad;//nacionalidadm Falta este campo 
        $giro_mercantilm .= $get_per->giro_mercantil;//giro_mercantilm Falta este campo 
        $pais_dm .= $get_per->pais;
        ///////////////////////////////////////
        $nombre_gm .= $get_per->nombre_g;
        $apellido_paterno_gm .= $get_per->apellido_paterno_g;
        $apellido_materno_gm .= $get_per->apellido_materno_g;
        $fecha_nacimientom .= date("Ymd", strtotime($get_per->fecha_nacimiento_g));
        $r_f_c_gm .= $get_per->r_f_c_g;
        $curp_gm .= $get_per->curp_g;
         ///////////////// DATOS DE DOMICILIO ////////////
        $extranjerom .= "";
        $colonia_dm .= $get_per->colonia_d;
        $calle_dm .= $get_per->calle_d;
        $no_ext_dm .= $get_per->no_ext_d;
        $no_int_dm .= $get_per->no_int_d; 
        $cp_dm .= $get_per->cp_d;
        $telefono_tm .= $get_per->telefono_d;
        $correo_tm .= $get_per->correo_d;
    }if($get->idtipo_cliente==6){//fideicomiso tipo_cliente_f
        $denominacionf.=$get_per->denominacion;
        $clave_registrof.=$get_per->clave_registro;
        $numero_referenciaf.=$get_per->numero_referencia;
        $nombre_gf.=$get_per->nombre_g;
        $apellido_paterno_gf.=$get_per->apellido_paterno_g;
        $apellido_materno_gf.=$get_per->apellido_materno_g;
        $fecha_nacimientof .=$get_per->fecha_nacimiento_g;//Falta este campo-- ya se agregó
        $r_f_c_gf .=$get_per->r_f_c_g;
        $curp_gf .=$get_per->curp_g;
    }

    $get_anexo=$this->ModeloCatalogos->getselectwhere('anexo11','id',$id_anexo);
    foreach ($get_anexo as $item){
      //$fecha_opera = date("Ym", strtotime($item->fecha_opera));
      $fecha_operacion = date("Ymd", strtotime($item->fecha_operacion));
      //tipo_opera
      $idanexo11=$item->id; 
      $monto_opera = $item->monto_opera;
      //$anio_acuse=date("Ymd", strtotime($item->anio_acuse));
      $num_factura = $item->num_factura;
      //////// 
      $id_beneficiario = $item->id_bene_transac;
      ////////
      //=======
      ///////////////////////////////////////////////
      // Tipo actividad 
      $tipo_actividad=$item->tipo_actividad;
      $ref_avi = $item->referencia;
    }
    $get_cliente_sucu=$this->General_model->get_tableRow('usuarios',array('UsuarioID'=>$this->usuarioid));
    $get_cliente_direc=$this->General_model->get_tableRow('cliente_direccion',array('iddireccion'=>$get_cliente_sucu->idsucursal));
    //$cp_cd=$get_cliente_direc->cp;
    if($get_cliente_direc!=""){
        $cp_cd=$get_cliente_direc->cp;
    }else{
        $cp_cd="CLIENTE NO TIENE CP ASIGNADO";
    }
           
    /** ********* PARA OBTENER LOS CLIENTES NORMALES ********* */
    if($get->idtipo_cliente==1 || $get->idtipo_cliente==2){//Fisica
        $xml_cli.='
        <persona_aviso>
        <tipo_persona>
            <persona_fisica>
                <nombre>'.mb_strtoupper($this->eliminar_acentos($nombref)).'</nombre>
                <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paternof)).'</apellido_paterno>
                <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_maternof)).'</apellido_materno>
                <fecha_nacimiento>'.mb_strtoupper($fecha_nacimientof).'</fecha_nacimiento>';
            if($r_f_c_tf!=""){
               $xml_cli.='<rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_tf)).'</rfc>';
            }
            if($curp_tf!=""){
               $xml_cli.='<curp>'.mb_strtoupper($this->eliminar_acentos($curp_tf)).'</curp>';
            } 
            $xml_cli.='<pais_nacionalidad>'.$pais_nacimientof.'</pais_nacionalidad>
                <actividad_economica>'.$actividad_o_girof.'</actividad_economica>
            </persona_fisica>
        </tipo_persona>';
        $xml_cli.='
          <tipo_domicilio>';
            if($get->idtipo_cliente==1){
                $eti_f="
                  <nacional>";
                $cierra_eti_f="
                  </nacional>"; 
            }
            if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                $eti_f="
                  <extranjero>";
                $cierra_eti_f="
                  </extranjero>";
            }
            $xml_cli.=$eti_f;
            $xml_cli.='
              <colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>';
                if($no_int_df!=""){
                    $xml_cli.='
                      <numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_df)).'</numero_interior>';
                }
                    
            $xml_cli.='
              <codigo_postal>'.$cp_df.'</codigo_postal>';
            if($get->idtipo_cliente==1 && $trata_persona!="" || $get->idtipo_cliente==2){
                $xml_cli.='<pais>'.$pais_nacionalidadf.'</pais>
                        <estado_provincia>'.mb_strtoupper($estado_df).'</estado_provincia>';
                    if($eti_f=="<extranjero>"){   
                       $xml_cli.='<ciudad_poblacion>'.mb_strtoupper($pais_df).'</ciudad_poblacion>';

                    }
            }    
        $xml_cli.=$cierra_eti_f;                                
        $xml_cli.='
          </tipo_domicilio>';
            //if($get->idtipo_cliente==2 || $trata_persona!=""){
            $xml_cli.=' <telefono>
                        <clave_pais>'.$pais_df.'</clave_pais>
                        <numero_telefono>'.$telefono_tf.'</numero_telefono>
                        <correo_electronico>'.$correo_tf.'</correo_electronico>
                    </telefono>';  
           //}
                                          
        $xml_cli.='
        </persona_aviso>';
    }if($get->idtipo_cliente==3 || $get->idtipo_cliente==4 || $get->idtipo_cliente==5){//Moral
        $xml.='
        <persona_aviso>
            <tipo_persona>
                <persona_moral>
                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($razon_socialm)).'</denominacion_razon>
                    <fecha_constitucion>'.date("Ymd", strtotime($fecha_constitucion_dm)).'</fecha_constitucion>
                    <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>
                    <pais_nacionalidad>'.$nacionalidadm.'</pais_nacionalidad>';
                    if($get->idtipo_cliente!=5){
                        $xml.='
                          <giro_mercantil>'.$giro_mercantilm.'</giro_mercantil>';
                    }
            //$xml.='</persona_moral>
            //</tipo_persona>';
            $xml.='
              <representante_apoderado>
                <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gm)).'</nombre>
                <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gm)).'</apellido_paterno>
                <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gm)).'</apellido_materno>
                <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientom)).'</fecha_nacimiento>';
                if($r_f_c_gm!="" && $get->idtipo_cliente!=3){
                   $xml.='
                    <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm)).'</rfc>';
                }else{
                    $xml.='
                      <rfc>'.mb_strtoupper($this->eliminar_acentos($r_f_c_gm_tp3)).'</rfc>';
                }
                if($curp_gm!=""){
                   $xml.='
                    <curp>'.mb_strtoupper($this->eliminar_acentos($curp_gm)).'</curp>';
                }
        

        $xml.='
          </representante_apoderado>';
        $xml.='
          </persona_moral>
        </tipo_persona>';
            $xml.='
              <tipo_domicilio>';
                if($extranjerom!=""){
                    $eti_tipo_redi="
                      <extranjero>";
                    $eti_tipo_cierra="
                      </extranjero>";
                }
                else{
                   $eti_tipo_redi="
                    <nacional>";
                   $eti_tipo_cierra="
                    </nacional>";
                }
                $xml.= $eti_tipo_redi.
                    '<colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_dm)).'</colonia>
                    <calle>'.mb_strtoupper($this->eliminar_acentos($calle_dm)).'</calle>
                    <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_dm)).'</numero_exterior>';
                    if($no_int_dm!=""){
                        $xml.='
                          <numero_interior>'.mb_strtoupper($this->eliminar_acentos($no_int_dm)).'</numero_interior>';
                    }
                $xml.='
                  <codigo_postal>'.$cp_dm.'</codigo_postal>';

                    if($extranjerom!=""){
                        $xml.='
                          <pais>'.$nacionalidadm.'</pais>
                                <estado_provincia>'.mb_strtoupper($this->eliminar_acentos($estado_dm)).'</estado_provincia>';
                    }
                    if($get->idtipo_cliente==4){
                        $xml.='
                          <ciudad_poblacion>MX</ciudad_poblacion>';
                    }
                    else{
                        if($get->idtipo_cliente==2 && $extranjerom!=""){ //agregar esto y demas cambios a los otros xmls
                            $xml.='
                              <ciudad_poblacion>'.$pais_dm.'</ciudad_poblacion>'; //agregar esto a los otros xmls    
                        }else{
                            $xml.='';
                        }
                        
                    }
                    
                $xml.= $eti_tipo_cierra.
                    '</tipo_domicilio>';
                if($get->idtipo_cliente==3){
                    $xml.='
                      <telefono>
                        <clave_pais>'.$pais_dm.'</clave_pais>
                        <numero_telefono>'.$telefono_tm.'</numero_telefono>                                
                      </telefono>';  
                }else{
                    $xml.='
                      <telefono>
                        <clave_pais>'.$pais_dm.'</clave_pais>                                    
                      </telefono>';  
                }
             $xml.='
        </persona_aviso>';     
    }if($get->idtipo_cliente==6){//Fideicomiso
        $xml.='
        <persona_aviso>
            <tipo_persona>
                <fideicomiso>
                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($denominacionf)).'</denominacion_razon>
                    <rfc>'.mb_strtoupper($clave_registrof).'</rfc>
                    <identificador_fideicomiso>'.mb_strtoupper($numero_referenciaf).'</identificador_fideicomiso>
                    <apoderado_delegado>
                        <nombre>'.mb_strtoupper($this->eliminar_acentos($nombre_gf)).'</nombre>
                        <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($apellido_paterno_gf)).'</apellido_paterno>
                        <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($apellido_materno_gf)).'</apellido_materno>
                        <fecha_nacimiento>'.date("Ymd", strtotime($fecha_nacimientof)).'</fecha_nacimiento>';
                    if($r_f_c_gf!=""){
                       $xml.='
                        <rfc>'.mb_strtoupper($r_f_c_gf).'</rfc>';
                    }
                    if($curp_gf!=""){
                       $xml.='
                        <curp>'.mb_strtoupper($curp_gf).'</curp>';
                    }

                $xml.='</apoderado_delegado>
                </fideicomiso>
            </tipo_persona>';
            $xml.='
              <tipo_domicilio>
                <nacional>
                    <colonia>'.mb_strtoupper($this->eliminar_acentos($colonia_df)).'</colonia>
                    <calle>'.mb_strtoupper($this->eliminar_acentos($calle_df)).'</calle>
                    <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($no_ext_df)).'</numero_exterior>
                    <codigo_postal>'.$cp_df.'</codigo_postal>
                </nacional>
            </tipo_domicilio>
        </persona_aviso>';
    } 

    /** ********* PARA OBTENER LOS BENEFICIARIOS ********* */
    $get_bene_t=$this->General_model->GetAllWhere('operacion_beneficiario',array('id_operacion'=>$ido,"activo"=>1));
    foreach ($get_bene_t as $item) {
        $tipob = $item->tipo_bene;
        $id_dueno = $item->id_duenio_bene;
        if($tipob==1){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fisica',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $fecha_nac = date("Ymd", strtotime($item->fecha_nacimiento));
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <persona_fisica>
                                    <nombre>'.mb_strtoupper($this->eliminar_acentos($item->nombre)).'</nombre>
                                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_paterno)).'</apellido_paterno>
                                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($item->apellido_materno)).'</apellido_materno>
                                    <fecha_nacimiento>'.$fecha_nac.'</fecha_nacimiento>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->r_f_c_d)).'</rfc>
                                    <curp>'.mb_strtoupper($this->eliminar_acentos($item->curp_d)).'</curp>
                                    <pais_nacionalidad>'.$item->pais_nacionalidad.'</pais_nacionalidad>
                                </persona_fisica>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==2){
            $get_bene_m=$this->General_model->GetAllWhere('beneficiario_moral_moral',array('id'=>$id_dueno));
            foreach ($get_bene_m as $item) {
                $fecha_cons = date("Ymd", strtotime($item->fecha_constitucion));
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <persona_moral>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <fecha_constitucion>'.$fecha_cons.'</fecha_constitucion>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <pais_nacionalidad>'.$item->pais.'</pais_nacionalidad>
                                </persona_moral>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==3){
            $get_bene_f=$this->General_model->GetAllWhere('beneficiario_fideicomiso',array('id'=>$id_dueno));
            foreach ($get_bene_f as $item) {
                $xml_bene.='<dueno_beneficiario> 
                            <tipo_persona>
                                <fideicomiso>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($item->razon_social)).'</denominacion_razon>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($item->rfc)).'</rfc>
                                    <identificador_fideicomiso>'.$item->referencia.'</identificador_fideicomiso>
                                </fideicomiso>
                            </tipo_persona>
                        </dueno_beneficiario>';
            }  
        }
        if($tipob==0){
            $xml_bene.='';
        }
    }
    
    $descrip_alert="";
    if($clave=="9999"){
      $descrip_alert="
        <descripcion_alerta>".$descrip."</descripcion_alerta>";
    }

    $acuse=$anio_acuse.$mes_acuse;
    $descrip ="<descripcion_alerta>ALGUN TIPO DE ALERTA</descripcion_alerta>";
    $xml='<archivo xmlns="http://www.uif.shcp.gob.mx/recepcion/'.$cv.'" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.uif.shcp.gob.mx/recepcion/'.$cv.' '.$cv.'.xsd">
        <informe>
          <mes_reportado>'.$acuse.'</mes_reportado>
          <sujeto_obligado>
            <clave_sujeto_obligado>'.mb_strtoupper($this->eliminar_acentos($rfc)).'</clave_sujeto_obligado>
            <clave_actividad>'.strtoupper($cv).'</clave_actividad>
          </sujeto_obligado>';
    $xml.='
    <aviso>
            <prioridad>2</prioridad>
            <alerta>
              <tipo_alerta>'.$clave.'</tipo_alerta>
              '.$descrip_alert.'
            </alerta>';
            $xml.= $xml_cli;
            $xml.= $xml_bene;
      $id_aux=$id_anexo;
      if($id_anexo>0){
        $xml.='<detalle_operaciones>
                <datos_operacion>
                 <fecha_operacion>'.$fecha_operacion.'</fecha_operacion>';
            $xml.='<tipo_actividad>';

            if($tipo_actividad==1){
              $whereact1 = array('idanexo11'=>$id_aux);
              $act1=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad1',$whereact1);
                
              foreach ($act1 as $a1){
                  $xml.='<compra_venta_inmuebles>
                      <tipo_operacion>'.$a1->tipo_operacion1.'</tipo_operacion>
                      
                      <valor_pactado>'.$a1->valor_pactado1.'</valor_pactado>
                      <datos_contraparte>
                          <tipo_persona>';
                          if($a1->tipo_persona1==1){
                       $xml.='<persona_fisica>
                                  <nombre>'.mb_strtoupper($this->eliminar_acentos($a1->nombre1)).'</nombre>
                                  <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a1->apellido_paterno1)).'</apellido_paterno>
                                  <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a1->apellido_materno1)).'</apellido_materno>
                                  <fecha_nacimiento>'.date("Ymd", strtotime($a1->fecha_nacimiento1)).'</fecha_nacimiento>
                                  <rfc>'.mb_strtoupper($this->eliminar_acentos($a1->rfc1)).'</rfc>
                                  <curp>'.mb_strtoupper($this->eliminar_acentos($a1->curp1)).'</curp>
                                  <pais_nacionalidad>'.$a1->pais_nacionalidad1.'</pais_nacionalidad>

                              </persona_fisica>';
                          }else if($a1->tipo_persona1==2){    
                       $xml.='<persona_moral>
                                  <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a1->denominacion_razonm1)).'</denominacion_razon>
                                  <fecha_constitucion>'.date("Ymd", strtotime($a1->fecha_constitucionm1)).'</fecha_constitucion>
                                  <rfc>'.mb_strtoupper($this->eliminar_acentos($a1->rfcm1)).'</rfc>
                                  <pais_nacionalidad>'.$a1->pais_nacionalidadm1.'</pais_nacionalidad>
                              </persona_moral>'; 
                          }else if($a1->tipo_persona1==3){      
                       $xml.='<fideicomiso>
                                  <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a1->denominacion_razonf1)).'</denominacion_razon> 
                                  <rfc>'.mb_strtoupper($this->eliminar_acentos($a1->rfcf1)).'</rfc>
                                  <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a1->identificador_fideicomisof1)).'</identificador_fideicomiso>
                              </fideicomiso>';    
                          }        
                   $xml.='</tipo_persona>
                      </datos_contraparte>
                      <caracteristicas_inmueble>
                          <tipo_inmueble>'.$a1->tipo_inmueble1.'</tipo_inmueble>
                          <colonia>'.mb_strtoupper($this->eliminar_acentos($a1->colonia1)).'</colonia>
                          <calle>'.mb_strtoupper($this->eliminar_acentos($a1->calle1)).'</calle>
                          <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($a1->numero_exterior1)).'</numero_exterior>
                          <numero_interior>'.mb_strtoupper($this->eliminar_acentos($a1->numero_interior1)).'</numero_interior>
                          <codigo_postal>'.mb_strtoupper($this->eliminar_acentos($a1->codigo_postal1)).'</codigo_postal>
                          <dimension_terreno>'.$a1->dimension_terreno1.'</dimension_terreno>
                          <dimension_construido>'.$a1->dimension_construido1.'</dimension_construido>
                          <folio_real>'.mb_strtoupper($this->eliminar_acentos($a1->folio_real1)).'</folio_real>
                          <contrato_instrumento_publico>';
                              if($a1->datos_instrumento_publico1==1){
                       $xml.='<datos_instrumento_publico>
                                  <numero_instrumento_publico>'.mb_strtoupper($this->eliminar_acentos($a1->codigo_postal1)).'</numero_instrumento_publico>
                                  <fecha_instrumento_publico>'.date("Ymd", strtotime($a1->fecha_instrumento_publico1)).'</fecha_instrumento_publico>
                                  <notario_instrumento_publico>'.mb_strtoupper($this->eliminar_acentos($a1->notario_instrumento_publico1)).'</notario_instrumento_publico>
                                  <entidad_instrumento_publico>'.$a1->entidad_instrumento_publico1.'</entidad_instrumento_publico>
                                  <valor_referencia>'.$a1->valor_referencia1.'</valor_referencia>
                              </datos_instrumento_publico>';
                              }else if($a1->datos_instrumento_publico1==2){
                       $xml.='<contrato>
                                  <fecha_contrato>'.date("Ymd", strtotime($a1->fecha_contrato1)).'</fecha_contrato> 
                                  <valor_referencia>'.$a1->valor_referencia1_1.'</valor_referencia> 
                              </contrato>';
                              }
                   $xml.='</contrato_instrumento_publico>
                      </caracteristicas_inmueble>';
                      /*$xml.='<datos_liquidacion>
                          <fecha_pago>'.date("Ymd", strtotime($a1->fecha_pago1)).'</fecha_pago>
                          <forma_pago>'.$a1->forma_pago1.'</forma_pago>
                          <instrumento_monetario>'.$a1->instrumento_monetario1.'</instrumento_monetario>
                          <moneda>'.$a1->moneda1.'</moneda>
                          <monto_operacion>'.$a1->monto_operacion1.'</monto_operacion>
                      </datos_liquidacion>';*/
                  $xml.='</compra_venta_inmuebles>'; 
              }   
            }else if($tipo_actividad==2){
              $whereact2 = array('idanexo11'=>$id_aux);
              $act2=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad2',$whereact2);
                
              foreach ($act2 as $a2){
                $xml.='<cesion_derechos_inmuebles>
                        <figura_cliente>'.$a2->figura_cliente2.'</figura_cliente>
                        <tipo_cesion>'.$a2->tipo_cesion2.'</tipo_cesion>
                        <datos_contraparte>
                            <tipo_persona>';
                      if($a2->tipo_persona2==1){ 
                         $xml.='<persona_fisica>
                                    <nombre>'.mb_strtoupper($this->eliminar_acentos($a2->nombre2)).'</nombre>
                                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a2->apellido_paterno2)).'</apellido_paterno>
                                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a2->apellido_materno2)).'</apellido_materno>
                                    <fecha_nacimiento>'.date("Ymd", strtotime($a2->fecha_nacimiento2)).'</fecha_nacimiento>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($a2->rfc2)).'</rfc>
                                    <curp>'.mb_strtoupper($this->eliminar_acentos($a2->curp2)).'</curp>
                                    <pais_nacionalidad>'.$a2->pais_nacionalidad2.'</pais_nacionalidad>
                                <persona_fisica>';
                      }else if($a2->tipo_persona2==2){     
                         $xml.='<persona_moral>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a2->denominacion_razonm2)).'</denominacion_razon>
                                    <fecha_constitucion>'.date("Ymd", strtotime($a2->fecha_constitucionm2)).'</fecha_constitucion>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($a2->rfcm2)).'</rfc>
                                    <pais_nacionalidad>'.$a2->pais_nacionalidadm2.'</pais_nacionalidad>
                                </persona_moral>';  
                      }else if($a2->tipo_persona2==3){           
                         $xml.='<fideicomiso>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a2->denominacion_razonf2)).'</denominacion_razon>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($a2->rfcf2)).'</rfc>
                                    <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a2->identificador_fideicomisof2)).'</identificador_fideicomiso>
                                </fideicomiso>';        
                            }    
                     $xml.='</tipo_persona>
                        </datos_contraparte>
                        <caracteristicas_inmueble>
                            <tipo_inmueble>'.$a2->tipo_inmueble2.'</tipo_inmueble>
                            <valor_referencia>'.$a2->valor_referencia2.'</valor_referencia>
                            <colonia>'.mb_strtoupper($this->eliminar_acentos($a2->colonia2)).'</colonia>
                            <calle>'.mb_strtoupper($this->eliminar_acentos($a2->calle2)).'</calle>
                            <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($a2->numero_exterior2)).'</numero_exterior>
                            <numero_interior>'.mb_strtoupper($this->eliminar_acentos($a2->numero_interior2)).'</numero_interior>
                            <codigo_postal>'.mb_strtoupper($this->eliminar_acentos($a2->codigo_postal2)).'</codigo_postal>
                            <dimension_terreno>'.$a2->dimension_terreno2.'</dimension_terreno>
                            <dimension_construido>'.$a2->dimension_construido2.'</dimension_construido>
                            <folio_real>'.mb_strtoupper($this->eliminar_acentos($a2->folio_real2)).'</folio_real>
                        </caracteristicas_inmueble>';
                        /*$xml.='<datos_liquidacion>
                            <fecha_pago>'.date("Ymd", strtotime($a2->fecha_pago2)).'</fecha_pago>
                            <instrumento_monetario>'.$a2->instrumento_monetario2.'</instrumento_monetario>
                            <moneda>'.$a2->moneda2.'</moneda>
                            <monto_operacion>'.$a2->monto_operacion2.'</monto_operacion>
                        </datos_liquidacion>';*/
                    $xml.='</cesion_derechos_inmuebles>';
                }    
            }else if($tipo_actividad==3){ 
              $whereact3 = array('idanexo11'=>$id_aux);
              $act3=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad3',$whereact3);
                  
              foreach ($act3 as $a3){  
                $xml.='<administracion_recursos>
                        <tipo_activo>';
                  if($a3->tipo_activo3==1){
                      $xml.='<activo_banco>
                                <estatus_manejo>'.$a3->estatus_manejo3.'</estatus_manejo>
                                <clave_tipo_institucion>'.$a3->clave_tipo_institucion3.'</clave_tipo_institucion>
                                <nombre_institucion>'.mb_strtoupper($this->eliminar_acentos($a3->nombre_institucion3)).'</nombre_institucion>
                                <numero_cuenta>'.mb_strtoupper($this->eliminar_acentos($a3->numero_cuenta3)).'</numero_cuenta>';
                            /*$xml.='<saldo>'.$a3->saldo3.'</saldo>
                                <moneda>'.$a3->moneda3.'</moneda>';*/
                            $xml.='</activo_banco>';
                  }else  if($a3->tipo_activo3==2){
                      $xml_desc_area=""; $xml_desc_act="";
                      if($a3->descrip_otra_presta!=""){
                          $xml_desc_area='<descripcion_otro_area_servicio>'.mb_strtoupper($this->eliminar_acentos($a3->descrip_otra_presta)).'</descripcion_otro_area_servicio>';  
                      }
                      if($a3->descrip_otro_activo!=""){
                          $xml_desc_act='<descripcion_otro_activo_administrado>'.mb_strtoupper($this->eliminar_acentos($a3->descrip_otro_activo)).'</descripcion_otro_activo_administrado>';  
                      }
                     $xml.='<activo_inmobiliario>
                                <tipo_inmueble>'.$a3->tipo_inmueble3.'</tipo_inmueble>
                                <valor_referencia>'.$a3->valor_referencia3.'</valor_referencia>
                                <colonia>'.mb_strtoupper($this->eliminar_acentos($a3->colonia3)).'</colonia>
                                <calle>'.mb_strtoupper($this->eliminar_acentos($a3->calle3)).'</calle>
                                <numero_exterior>'.mb_strtoupper($this->eliminar_acentos($a3->numero_exterior3)).'</numero_exterior>
                                <numero_interior>'.mb_strtoupper($this->eliminar_acentos($a3->numero_interior3)).'</numero_interior>
                                <codigo_postal>'.mb_strtoupper($this->eliminar_acentos($a3->codigo_postal3)).'</codigo_postal>
                                <folio_real>'.mb_strtoupper($this->eliminar_acentos($a3->folio_real3)).'</folio_real>
                            </activo_inmobiliario>
                            <activo_outsourcing>
                                <area_servicio>
                                    <tipo_area_servicio>'.$a3->clave_presta.'</tipo_area_servicio>
                                    '.$xml_desc_area.'
                                </area_servicio>
                                <activo_administrado>
                                    <tipo_activo_administrado>'.$a3->clave_activo.'</tipo_activo_administrado>
                                    '.$xml_desc_act.'
                                    <numero_empleados>'.$a3->num_emplea.'</numero_empleados>
                                </activo_administrado>
                            </activo_outsourcing>';
                  }else  if($a3->tipo_activo3==3){    
                     $xml.='<activo_otros>
                                <descripcion>'.mb_strtoupper($this->eliminar_acentos($a3->descripcion3)).'</descripcion>
                                <numero_operaciones>'.$a3->num_opera.'</numero_operaciones>
                            </activo_otros>';
                  }
                  $xml.='</tipo_activo>
                  </administracion_recursos>';
              }
            }else if($tipo_actividad==4){
              $whereact4 = array('idanexo11'=>$id_aux);
              $act4=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad4',$whereact4);
                  
              foreach ($act4 as $a4){  
                $xml.='<constitucion_sociedades_mercantiles>
                      <tipo_persona_moral>'.$a4->tipo_persona_moral4.'</tipo_persona_moral>';
                  if($a4->tipo_persona_moral4){
                     $xml.=' <tipo_persona_moral_otra>'.mb_strtoupper($this->eliminar_acentos($a4->tipo_persona_moral_otra4)).'</tipo_persona_moral_otra>';
                  }
                $xml.='<denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a4->denominacion_razon4)).'</denominacion_razon>
                      <giro_mercantil>'.$a4->giro_mercantil4.'</giro_mercantil>
                      <folio_mercantil>'.mb_strtoupper($this->eliminar_acentos($a4->folio_mercantil4)).'</folio_mercantil>
                      <numero_total_acciones>'.$a4->numero_total_acciones4.'</numero_total_acciones>
                      <entidad_federativa>'.$a4->entidad_federativa4.'</entidad_federativa>
                      <consejo_vigilancia>'.$a4->consejo_vigilancia4.'</consejo_vigilancia>
                      <motivo_constitucion>'.$a4->motivo_constitucion4.'</motivo_constitucion>
                      <instrumento_publico>'.mb_strtoupper($this->eliminar_acentos($a4->instrumento_publico4)).'</instrumento_publico>';
                  $whereact4_acc = array('idanexo11'=>$id_aux,'activo'=>1);
                  $act4_acc=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_act4_constitucion_personas_morales_socios',$whereact4_acc);
                        
                foreach ($act4_acc as $a4_acc){  
                  $xml.='<datos_accionista>
                          <cargo_accionista>'.$a4_acc->cargo_accionista.'</cargo_accionista>
                            <tipo_persona>';
                        if($a4_acc->tipo_persona==1){
                           $xml.='<persona_fisica>
                                      <nombre>'.mb_strtoupper($this->eliminar_acentos($a4_acc->nombre)).'</nombre>
                                      <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a4_acc->apellido_paterno)).'</apellido_paterno>
                                      <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a4_acc->apellido_materno)).'</apellido_materno>
                                      <fecha_nacimiento>'.date("Ymd", strtotime($a4_acc->fecha_nacimiento)).'</fecha_nacimiento>
                                      <rfc>'.mb_strtoupper($this->eliminar_acentos($a4_acc->rfc)).'</rfc>
                                      <curp>'.mb_strtoupper($this->eliminar_acentos($a4_acc->curp)).'</curp>
                                      <pais_nacionalidad>'.$a4_acc->pais_nacionalidad.'</pais_nacionalidad>
                                  </persona_fisica>';
                        }else if($a4_acc->tipo_persona==2){
                           $xml.='<persona_moral>
                                      <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a4_acc->denominacion_razonm)).'</denominacion_razon>
                                      <fecha_constitucion>'.date("Ymd", strtotime($a4_acc->fecha_constitucionm)).'</fecha_constitucion>
                                      <rfc>'.mb_strtoupper($this->eliminar_acentos($a4_acc->rfcm)).'</rfc>
                                      <pais_nacionalidad>'.$a4_acc->pais_nacionalidadm.'</pais_nacionalidad>
                                  </persona_moral>';
                        }else if($a4_acc->tipo_persona==3){
                           $xml.='<fideicomiso>
                                      <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a4_acc->denominacion_razonf)).'</denominacion_razon>
                                      <rfc>'.mb_strtoupper($this->eliminar_acentos($a4_acc->rfcf)).'</rfc>
                                      <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a4_acc->identificador_fideicomisof)).'</identificador_fideicomiso>
                                      <numero_acciones>'.$a4_acc->numero_accionesf.'</numero_acciones>
                                  </fideicomiso>';
                        }
                        $xml.='</tipo_persona>
                          </datos_accionista>';
                  }
                  $xml.='<capital_social>
                        <capital_fijo>'.$a4->capital_fijo4.'</capital_fijo>
                        <capital_variable>'.$a4->capital_variable4.'</capital_variable>
                    </capital_social>
                </constitucion_sociedades_mercantiles>';
              } 
            }else if($tipo_actividad==5){
              $whereact5 = array('idanexo11'=>$id_aux);
              $act5=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad5',$whereact5);
                  
              foreach ($act5 as $a5){  
                $xml.='<organizacion_aportaciones>
                        <motivo_aportacion>'.$a5->motivo_aportacion5.'</motivo_aportacion>
                        <datos_aportacion>
                            <datos_persona_aporta>';
                      if($a5->tipo_persona5==1){
                         $xml.='<persona_fisica>
                                    <nombre>'.mb_strtoupper($this->eliminar_acentos($a5->nombre5)).'</nombre>
                                    <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a5->apellido_paterno5)).'</apellido_paterno>
                                    <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a5->apellido_materno5)).'</apellido_materno>
                                    <fecha_nacimiento>'.date("Ymd", strtotime($a5->fecha_nacimiento5)).'</fecha_nacimiento>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($a5->rfc5)).'</rfc>
                                    <curp>'.mb_strtoupper($this->eliminar_acentos($a5->curp5)).'</curp>
                                    <pais_nacionalidad>'.$a5->pais_nacionalidad5.'</pais_nacionalidad>
                                    <actividad_economica>'.$a5->actividad_economica5.'</actividad_economica>
                                </persona_fisica>';
                      }else if($a5->tipo_persona5==2){    
                         $xml.='<persona_moral>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a5->denominacion_razonm5)).'</denominacion_razon>
                                    <fecha_constitucion>'.date("Ymd", strtotime($a5->fecha_constitucionm5)).'</fecha_constitucion>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($a5->rfcm5)).'</rfc>
                                    <pais_nacionalidad>'.$a5->pais_nacionalidadm5.'</pais_nacionalidad>
                                    <giro_mercantil>'.$a5->giro_mercantil5.'</giro_mercantil>
                                </persona_moral>';
                      }else if($a5->tipo_persona5==3){        
                         $xml.='<fideicomiso>
                                    <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a5->denominacion_razonf5)).'</denominacion_razon>
                                    <rfc>'.mb_strtoupper($this->eliminar_acentos($a5->rfcf5)).'</rfc>
                                    <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a5->identificador_fideicomisof5)).'</identificador_fideicomiso>
                                </fideicomiso>';
                      }    
                        $xml.='</datos_persona_aporta>
                            <datos_tipo_aportacion>';
                      if($a5->aportacion_monetaria5==1){
                         $xml.='<aportacion_monetaria>
                                    <instrumento_monetario>'.$a5->instrumento_monetario5.'</instrumento_monetario>
                                    <moneda>'.$a5->moneda5.'</moneda>
                                    <monto_operacion>'.$a5->monto_operacion5.'</monto_operacion>
                                </aportacion_monetaria>';
                      }else if($a5->aportacion_monetaria5==2){
                         $xml.='<aportacion_inmueble>
                                    <tipo_inmueble>'.$a5->tipo_inmueble5.'</tipo_inmueble>
                                    <codigo_postal>'.mb_strtoupper($this->eliminar_acentos($a5->codigo_postal5)).'</codigo_postal>
                                    <folio_real>'.mb_strtoupper($this->eliminar_acentos($a5->folio_real5)).'</folio_real>
                                    <valor_aportacion>'.$a5->valor_aportacion5.'</valor_aportacion>
                                </aportacion_inmueble>';
                      }else if($a5->aportacion_monetaria5==3){
                         $xml.='<aportacion_otro_bien>
                                    <descripcion>'.mb_strtoupper($this->eliminar_acentos($a5->descripcion5)).'</descripcion>
                                    <valor_aportacion>'.$a5->valor_aportaciono5.'</valor_aportacion>
                                </aportacion_otro_bien>';
                      }
                     $xml.='</datos_tipo_aportacion>
                        </datos_aportacion>
                    </organizacion_aportaciones>';
              }
            }else if($tipo_actividad==6){
              $whereact6 = array('idanexo11'=>$id_aux);
              $act6=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad6',$whereact6);
                   
              foreach ($act6 as $a6){  
                $xml.='<fusion>
                        <tipo_fusion>'.$a6->tipo_fusion6.'</tipo_fusion>
                        <datos_fusionadas>
                            <datos_fusionada>
                                <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a6->denominacion_razonm6)).'</denominacion_razon>
                                <fecha_constitucion>'.date("Ymd", strtotime($a6->fecha_constitucionm6)).'</fecha_constitucion>
                                <rfc>'.mb_strtoupper($this->eliminar_acentos($a6->rfcm6)).'</rfc>
                                <pais_nacionalidad>'.$a6->pais_nacionalidadm6.'</pais_nacionalidad>
                                <giro_mercantil>'.$a6->giro_mercantil6.'</giro_mercantil>
                                <capital_social_fijo>'.$a6->capital_social_fijo6.'</capital_social_fijo>
                                <capital_social_variable>'.$a6->capital_social_variable6.'</capital_social_variable>
                                <folio_mercantil>'.mb_strtoupper($this->eliminar_acentos($a6->folio_mercantil6)).'</folio_mercantil>
                            </datos_fusionada>
                        </datos_fusionadas>
                        <datos_fusionante>
                            <fusionante_determinadas>'.$a6->fusionante_determinadas6.'</fusionante_determinadas>
                            <fusionante>
                                <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a6->denominacion_razonf6)).'</denominacion_razon>
                                <fecha_constitucion>'.date("Ymd", strtotime($a6->fecha_constitucionf6)).'</fecha_constitucion>
                                <rfc>'.mb_strtoupper($this->eliminar_acentos($a6->rfcf6)).'</rfc>
                                <pais_nacionalidad>'.$a6->pais_nacionalidadf6.'</pais_nacionalidad>
                                <giro_mercantil>'.$a6->giro_mercantilf6.'</giro_mercantil>
                                <capital_social_fijo>'.$a6->capital_social_fijof6.'</capital_social_fijo>
                                <capital_social_variable>'.$a6->capital_social_variablef6.'</capital_social_variable>
                                <folio_mercantil>'.mb_strtoupper($this->eliminar_acentos($a6->folio_mercantilf6)).'</folio_mercantil>
                                <numero_total_acciones>'.$a6->numero_total_accionesf6.'</numero_total_acciones>';
                      $whereact6_acc = array('idanexo11'=>$id_aux,'activo'=>1);
                      $act6_acc=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_act6_constitucion_personas_morales_socios',$whereact6_acc);
                    
                      foreach ($act6_acc as $a6_acc){ 
                        $xml.='<datos_accionista>
                                        <tipo_persona>';
                          if($a6_acc->tipo_persona==1){
                             $xml.='<persona_fisica>
                                        <nombre>'.mb_strtoupper($this->eliminar_acentos($a6_acc->nombre)).'</nombre>
                                        <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a6_acc->apellido_paterno)).'</apellido_paterno>
                                        <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a6_acc->apellido_materno)).'</apellido_materno>
                                        <fecha_nacimiento>'.date("Ymd", strtotime($a6_acc->fecha_nacimiento)).'</fecha_nacimiento>
                                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a6_acc->rfc)).'</rfc>
                                        <curp>'.mb_strtoupper($this->eliminar_acentos($a6_acc->curp)).'</curp>
                                        <pais_nacionalidad>'.$a6_acc->pais_nacionalidad.'</pais_nacionalidad>
                                    </persona_fisica>';
                          }else if($a6_acc->tipo_persona==2){
                             $xml.='<persona_moral>
                                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a6_acc->denominacion_razonm)).'</denominacion_razon> 
                                        <fecha_constitucion>'.date("Ymd", strtotime($a6_acc->fecha_constitucionm)).'</fecha_constitucion>
                                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a6_acc->rfcm)).'</rfc>
                                        <pais_nacionalidad>'.$a6_acc->pais_nacionalidadm.'</pais_nacionalidad>
                                    </persona_moral>';
                          }else if($a6_acc->tipo_persona==3){
                             $xml.='<fideicomiso>
                                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a6_acc->denominacion_razonf)).'</denominacion_razon>
                                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a6_acc->rfcf)).'</rfc>
                                        <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a6_acc->identificador_fideicomisof)).'</identificador_fideicomiso>
                                        <numero_acciones>'.$a6_acc->numero_accionesf.'</numero_acciones>
                                    </fideicomiso>';
                          }
                         $xml.='</tipo_persona>
                            </datos_accionista>';
                        }
                     $xml.='</fusionante>
                        </datos_fusionante>
                    </fusion>';
              }   
            }else if($tipo_actividad==7){
              $whereact7 = array('idanexo11'=>$id_aux);
              $act7=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad7',$whereact7);
                         
              foreach ($act7 as $a7){  
                $xml.='<escision>
                        <datos_escindente>
                            <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a7->denominacion_razonm7)).'</denominacion_razon>
                            <fecha_constitucion>'.date("Ymd", strtotime($a7->fecha_constitucionm7)).'</fecha_constitucion>
                            <rfc>'.mb_strtoupper($this->eliminar_acentos($a7->rfcm7)).'</rfc>
                            <pais_nacionalidad>'.$a7->pais_nacionalidadm7.'</pais_nacionalidad>
                            <giro_mercantil>'.$a7->giro_mercantil7.'</giro_mercantil>
                            <capital_social_fijo>'.$a7->capital_social_fijo7.'</capital_social_fijo>
                            <capital_social_variable>'.$a7->capital_social_variable7.'</capital_social_variable>
                            <folio_mercantil>'.mb_strtoupper($this->eliminar_acentos($a7->folio_mercantil7)).'</folio_mercantil>
                            <escindente_subsiste>'.$a7->escindente_subsiste7.'</escindente_subsiste>';
                $whereact7_acc = array('idanexo11'=>$id_aux,'activo'=>1);
                $act7_acc=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_act7_constitucion_personas_morales_socios',$whereact7_acc);
                                     
                foreach ($act7_acc as $a7_acc){  
                   $xml.='<datos_accionista_escindente>
                              <tipo_persona>';
                    if($a7_acc->tipo_persona==1){
                           $xml.='<persona_fisica>
                                      <nombre>'.mb_strtoupper($this->eliminar_acentos($a7_acc->nombre)).'</nombre>
                                      <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a7_acc->apellido_paterno)).'</apellido_paterno>
                                      <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a7_acc->apellido_materno)).'</apellido_materno>
                                      <fecha_nacimiento>'.date("Ymd", strtotime($a7_acc->fecha_nacimiento)).'</fecha_nacimiento>
                                      <rfc>'.mb_strtoupper($this->eliminar_acentos($a7_acc->rfc)).'</rfc>
                                      <curp>'.mb_strtoupper($this->eliminar_acentos($a7_acc->curp)).'</curp>
                                      <pais_nacionalidad>'.$a7_acc->pais_nacionalidad.'</pais_nacionalidad>
                                  </persona_fisica>';
                    }else if($a7_acc->tipo_persona==2){
                           $xml.='<persona_moral>
                                      <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a7_acc->denominacion_razonm)).'</denominacion_razon>
                                      <fecha_constitucion>'.date("Ymd", strtotime($a7_acc->fecha_constitucionm)).'</fecha_constitucion>
                                      <rfc>'.mb_strtoupper($this->eliminar_acentos($a7_acc->rfcm)).'</rfc>
                                      <pais_nacionalidad>'.$a7_acc->pais_nacionalidadm.'</pais_nacionalidad>
                                  </persona_moral>';
                    }else if($a7_acc->tipo_persona==3){ 
                           $xml.='<fideicomiso>
                                      <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a7_acc->denominacion_razonf)).'</denominacion_razon>
                                      <rfc>'.mb_strtoupper($this->eliminar_acentos($a7_acc->rfcf)).'</rfc>
                                      <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a7_acc->rfcf)).'</identificador_fideicomiso>
                                      <numero_acciones>'.$a7_acc->numero_accionesf.'</numero_acciones>
                                  </fideicomiso>';
                    }
                    $xml.='</tipo_persona>
                          </datos_accionista_escindente>';
                }
                $xml.='</datos_escindente>
                      <datos_escindidas>
                          <escindidas_determinadas>'.$a7->escindidas_determinadas7.'</escindidas_determinadas>
                          <dato_escindida>
                              <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a7->denominacion_razonf7)).'</denominacion_razon>
                              <fecha_constitucion>'.date("Ymd", strtotime($a7->fecha_constitucionf7)).'</fecha_constitucion>
                              <rfc>'.mb_strtoupper($this->eliminar_acentos($a7->rfcf7)).'</rfc>
                              <pais_nacionalidad>'.$a7->pais_nacionalidadf7.'</pais_nacionalidad>
                              <giro_mercantil>'.$a7->giro_mercantilf7.'</giro_mercantil>
                              <capital_social_fijo>'.$a7->capital_social_fijof7.'</capital_social_fijo>
                              <capital_social_variable>'.$a7->capital_social_variablef7.'</capital_social_variable>
                              <folio_mercantil>'.mb_strtoupper($this->eliminar_acentos($a7->folio_mercantilf7)).'</folio_mercantil>
                              <numero_total_acciones>'.$a7->numero_total_accionesf7.'</numero_total_acciones>';

                          $whereact72_acc = array('idanexo11'=>$id_aux,'activo'=>1);
                          $act72_acc=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_act72_constitucion_personas_morales_socios',$whereact72_acc);
                                        
                          foreach ($act72_acc as $a72_acc){  
                            $xml.='<datos_accionista>
                                            <tipo_persona>';
                            if($a72_acc->tipo_persona==1){
                              $xml.='<persona_fisica>
                                  <nombre>'.mb_strtoupper($this->eliminar_acentos($a72_acc->nombre)).'</nombre>
                                  <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a72_acc->apellido_paterno)).'</apellido_paterno>
                                  <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a72_acc->apellido_materno)).'</apellido_materno>
                                  <fecha_nacimiento>'.date("Ymd", strtotime($a72_acc->fecha_nacimiento)).'</fecha_nacimiento>
                                  <rfc>'.mb_strtoupper($this->eliminar_acentos($a72_acc->rfc)).'</rfc>
                                  <curp>'.mb_strtoupper($this->eliminar_acentos($a72_acc->curp)).'</curp>
                                  <pais_nacionalidad>'.$a72_acc->pais_nacionalidad.'</pais_nacionalidad>
                              </persona_fisica>';
                            }else if($a72_acc->tipo_persona==2){  
                              $xml.='<persona_moral>
                                  <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a72_acc->denominacion_razonm)).'</denominacion_razon> 
                                  <fecha_constitucion>'.date("Ymd", strtotime($a72_acc->fecha_constitucionm)).'</fecha_constitucion>
                                  <rfc>'.mb_strtoupper($this->eliminar_acentos($a72_acc->rfcm)).'</rfc>
                                  <pais_nacionalidad>'.$a72_acc->pais_nacionalidadm.'</pais_nacionalidad>
                              </persona_moral>';
                            }else if($a72_acc->tipo_persona==3){      
                              $xml.='<fideicomiso>
                                  <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a72_acc->denominacion_razonf)).'</denominacion_razon>
                                  <rfc>'.mb_strtoupper($this->eliminar_acentos($a72_acc->rfcf)).'</rfc>
                                  <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a72_acc->identificador_fideicomisof )).'</identificador_fideicomiso>
                                  <numero_acciones>'.$a72_acc->numero_accionesf.'</numero_acciones>
                                </fideicomiso>';
                            }
                           $xml.='</tipo_persona>
                              </datos_accionista>';
                          }
                          $xml.='</dato_escindida>
                            </datos_escindidas>
                          </escision>';
              }   
            }else if($tipo_actividad==8){
              $whereact8 = array('idanexo11'=>$id_aux);
              $act8=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad8',$whereact8);
                   
              foreach ($act8 as $a8){  
                $xml.='<administracion_personas_morales>
                        <tipo_administracion>'.mb_strtoupper($this->eliminar_acentos($a8->tipo_administracion8)).'</tipo_administracion>
                        <tipo_operacion>'.mb_strtoupper($this->eliminar_acentos($a8->tipo_operacion8)).'</tipo_operacion>
                        <persona_moral_aviso>'.$a8->persona_moral_aviso8.'</persona_moral_aviso>
                        <tipo_persona>';
                if($a8->tipo_persona8==2){
                     $xml.='<persona_moral>
                                <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a8->denominacion_razonm8)).'</denominacion_razon>
                                <fecha_constitucion>'.date("Ymd", strtotime($a8->fecha_constitucionm8)).'</fecha_constitucion>
                                <rfc>'.mb_strtoupper($this->eliminar_acentos($a8->rfcm8)).'</rfc>
                                <pais_nacionalidad>'.$a8->pais_nacionalidadm8.'</pais_nacionalidad>
                            </persona_moral>';
                }else if($a8->tipo_persona8==3){
                     $xml.='<fideicomiso>
                                <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a8->denominacion_razonf8)).'</denominacion_razon>
                                <rfc>'.mb_strtoupper($this->eliminar_acentos($a8->rfcf8)).'</rfc>
                                <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a8->identificador_fideicomisof8)).'</identificador_fideicomiso>
                            </fideicomiso>';
                }
                 $xml.='</tipo_persona>
                    </administracion_personas_morales>';
              }   
            }else if($tipo_actividad==9){
              $whereact9 = array('idanexo11'=>$id_aux);
              $act9=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad9',$whereact9);
                 
              foreach ($act9 as $a9){ 
                $xml.='<constitucion_fideicomiso>
                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a9->rfcf9)).'</rfc>
                        <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a9->identificador_fideicomisof9)).'</identificador_fideicomiso>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a9->denominacion_razonf9)).'</denominacion_razon>
                        <objeto_fideicomiso>'.$a9->objeto_fideicomiso9.'</objeto_fideicomiso>
                        <monto_total_patrimonio>'.$a9->monto_total_patrimonio9.'</monto_total_patrimonio>';

                $whereact9_acc = array('idanexo11'=>$id_aux,'activo'=>1);
                $act9_acc=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_act9_constitucion_personas_morales_socios',$whereact9_acc);
                               
                $xml.='<datos_fideicomitente>';
                foreach ($act9_acc as $a9_acc){  
                  $xml.='<tipo_persona>';
                  if($a9_acc->tipo_persona==1){
                    $xml.='<persona_fisica>
                              <nombre>'.mb_strtoupper($this->eliminar_acentos($a9_acc->nombre)).'</nombre>
                              <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a9_acc->apellido_paterno)).'</apellido_paterno>
                              <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a9_acc->apellido_materno)).'</apellido_materno>
                              <fecha_nacimiento>'.date("Ymd", strtotime($a9_acc->fecha_nacimiento)).'</fecha_nacimiento>
                              <rfc>'.mb_strtoupper($this->eliminar_acentos($a9_acc->rfc)).'</rfc>
                              <curp>'.mb_strtoupper($this->eliminar_acentos($a9_acc->curp)).'</curp>
                              <pais_nacionalidad>'.$a9_acc->pais_nacionalidad.'</pais_nacionalidad>
                          </persona_fisica>';
                  }else if($a9_acc->tipo_persona==2){ 
                    $xml.='<persona_moral>
                              <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a9_acc->denominacion_razonm)).'</denominacion_razon>
                              <fecha_constitucion>'.date("Ymd", strtotime($a9_acc->fecha_constitucionm)).'</fecha_constitucion>
                              <rfc>'.mb_strtoupper($this->eliminar_acentos($a9_acc->rfcm)).'</rfc>
                              <pais_nacionalidad>'.$a9_acc->pais_nacionalidadm.'</pais_nacionalidad>
                          </persona_moral>';
                  }else if($a9_acc->tipo_persona==3){ 
                    $xml.='<fideicomiso>
                              <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a9_acc->denominacion_razonf)).'</denominacion_razon>
                              <rfc>'.mb_strtoupper($this->eliminar_acentos($a9_acc->rfcf)).'</rfc>
                              <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a9_acc->identificador_fideicomisof)).'</identificador_fideicomiso>
                          </fideicomiso>';
                  }
                  $xml.='</tipo_persona>';
                }
                if($a9->aportacion_monetaria9==1){
                  $xml.='<datos_tipo_patrimonio>
                            <patrimonio_monetario>
                                <moneda>'.$a9->moneda9.'</moneda>
                                <monto_operacion>'.$a9->monto_operacion9.'</monto_operacion>
                            </patrimonio_monetario>
                        </datos_tipo_patrimonio>';
                }else if($a9->aportacion_monetaria9==2){
                  $xml.='<patrimonio_inmueble>
                            <tipo_inmueble>'.$a9->tipo_inmueble9.'</tipo_inmueble>
                            <codigo_postal>'.mb_strtoupper($this->eliminar_acentos($a9->codigo_postal9)).'</codigo_postal>
                            <folio_real>'.mb_strtoupper($this->eliminar_acentos($a9->folio_real9)).'</folio_real>
                            <importe_garantia>'.$a9->importe_garantia9.'</importe_garantia>
                        </patrimonio_inmueble>';
                }else if($a9->aportacion_monetaria9==3){
                  $xml.='<patrimonio_otro_bien>
                            <descripcion>'.mb_strtoupper($this->eliminar_acentos($a9->descripcion9)).'</descripcion>
                            <valor_bien>'.$a9->valor_aportaciono9.'</valor_bien>
                        </patrimonio_otro_bien>';
                }
                $xml.='</datos_fideicomitente>';
                $whereact92_acc = array('idanexo11'=>$id_aux,'activo'=>1);
                $act92_acc=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_act92_constitucion_personas_morales_socios',$whereact92_acc);
                          
                foreach ($act92_acc as $a92_acc){  
                  $xml.='<datos_fideicomisario>
                            <datos_fideicomisarios_determinados>'.$a92_acc->datos_fideicomisarios_determinados.'</datos_fideicomisarios_determinados>
                            <tipo_persona>';
                  if($a92_acc->tipo_persona==1){
                    $xml.='<persona_fisica>
                        <nombre>'.mb_strtoupper($this->eliminar_acentos($a92_acc->nombre)).'</nombre>
                        <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a92_acc->apellido_paterno)).'</apellido_paterno>
                        <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a92_acc->apellido_materno)).'</apellido_materno>
                        <fecha_nacimiento>'.date("Ymd", strtotime($a92_acc->fecha_nacimiento)).'</fecha_nacimiento>
                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a92_acc->rfc)).'</rfc>
                        <curp>'.mb_strtoupper($this->eliminar_acentos($a92_acc->curp)).'</curp>
                        <pais_nacionalidad>'.$a92_acc->pais_nacionalidad.'</pais_nacionalidad>
                    </persona_fisica>';
                  }else if($a92_acc->tipo_persona==2){ 
                    $xml.='<persona_moral>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a92_acc->denominacion_razonm)).'</denominacion_razon>
                        <fecha_constitucion>'.date("Ymd", strtotime($a92_acc->fecha_constitucionm)).'</fecha_constitucion>
                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a92_acc->rfcm)).'</rfc>
                        <pais_nacionalidad>'.$a92_acc->pais_nacionalidadm.'</pais_nacionalidad>
                    </persona_moral>';
                  }else if($a92_acc->tipo_persona==3){
                    $xml.='<fideicomiso>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a92_acc->denominacion_razonf)).'</denominacion_razon>
                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a92_acc->rfcf)).'</rfc>
                        <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a92_acc->identificador_fideicomisof)).'</identificador_fideicomiso>
                    </fideicomiso>';
                  }
                  $xml.='</tipo_persona>
                      </datos_fideicomisario>';
                }
                $xml.='<datos_miembro_comite_tecnico>
                        <comite_tecnico>'.$a9->comite_tecnico9.'</comite_tecnico>
                    </datos_miembro_comite_tecnico>
                </constitucion_fideicomiso>';
              }   
                    
            }else if($tipo_actividad==10){
              $whereact10 = array('idanexo11'=>$id_aux);
              $act10=$this->ModeloCatalogos->getselectwherestatus('*','anexo11_actividad10',$whereact10);
                                
              foreach ($act10 as $a10){  
                $xml_nombre_act="";
                if($a10->nombre_otro_activo!=""){
                    $xml_nombre_act.='<descripcion_activo_virtual>'.mb_strtoupper($this->eliminar_acentos($a10->nombre_otro_activo)).'</descripcion_activo_virtual>';  
                }
                $xml.='<compra_venta_entidades_mercantiles>
                    <tipo_operacion>'.$a10->tipo_operacion10.'</tipo_operacion>
                    <datos_sociedad_mercantil>
                        <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a10->denominacion_razon10)).'</denominacion_razon>
                        <giro_mercantil>'.$a10->giro_mercantil10.'</giro_mercantil>
                        <fecha_constitucion>'.date("Ymd", strtotime($a10->fecha_constitucion10)).'</fecha_constitucion>
                        <rfc>'.mb_strtoupper($this->eliminar_acentos($a10->rfc10)).'</rfc>
                        <pais_nacionalidad>'.$a10->pais_nacionalidad10.'</pais_nacionalidad>
                        <folio_mercantil>'.mb_strtoupper($this->eliminar_acentos($a10->folio_mercantil10)).'</folio_mercantil>
                        <acciones_adquiridas>'.$a10->acciones_adquiridas10.'</acciones_adquiridas>
                        <acciones_totales>'.$a10->acciones_totales10.'</acciones_totales>
                        <datos_contraparte>';
                if($a10->tipo_persona10==1){
                  $xml.='<persona_fisica>
                            <nombre>'.mb_strtoupper($this->eliminar_acentos($a10->nombre10)).'</nombre>
                            <apellido_paterno>'.mb_strtoupper($this->eliminar_acentos($a10->apellido_paterno10)).'</apellido_paterno>
                            <apellido_materno>'.mb_strtoupper($this->eliminar_acentos($a10->apellido_materno10)).'</apellido_materno>
                            <fecha_nacimiento>'.date("Ymd", strtotime($a10->fecha_nacimiento10)).'</fecha_nacimiento>
                            <rfc>'.mb_strtoupper($this->eliminar_acentos($a10->rfcfi10)).'</rfc>
                            <curp>'.mb_strtoupper($this->eliminar_acentos($a10->curp10)).'</curp>
                            <pais_nacionalidad>'.$a10->pais_nacionalidadfi10.'</pais_nacionalidad>
                        </persona_fisica>';
                }else if($a10->tipo_persona10==2){
                  $xml.='<persona_moral>
                            <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a10->denominacion_razonm10)).'</denominacion_razon>
                            <fecha_constitucion>'.date("Ymd", strtotime($a10->fecha_constitucionm10)).'</fecha_constitucion>
                            <rfc>'.mb_strtoupper($this->eliminar_acentos($a10->rfcm10)).'</rfc>
                            <pais_nacionalidad>'.$a10->pais_nacionalidadm10.'</pais_nacionalidad>
                        </persona_moral>';
                }else if($a10->tipo_persona10==3){ 
                  $xml.='<fideicomiso>
                            <denominacion_razon>'.mb_strtoupper($this->eliminar_acentos($a10->denominacion_razonf10)).'</denominacion_razon>
                            <rfc>'.mb_strtoupper($this->eliminar_acentos($a10->rfcf10)).'</rfc>
                            <identificador_fideicomiso>'.mb_strtoupper($this->eliminar_acentos($a10->identificador_fideicomisof10)).'</identificador_fideicomiso>
                        </fideicomiso>';
                }
                $xml.='</datos_contraparte>
                      </datos_sociedad_mercantil>
                      <datos_operacion_financiera>
                        <fecha_pago>'.date("Ymd", strtotime($a10->fecha_pago10)).'</fecha_pago>';
                        //$xml.='<forma_pago>'.$a10->forma_pago10.'</forma_pago>';
                        $xml.='<instrumento_monetario>'.$a10->instrumento_monetario10.'</instrumento_monetario>
                        <activo_virtual>
                            <tipo_activo_virtual>'.$a10->activo_virtual.'</tipo_activo_virtual>
                            '.$xml_nombre_act.'
                            <cantidad_activo_virtual>'.$a10->cant_activo.'</cantidad_activo_virtual>
                        </activo_virtual>
                        <moneda>'.$this->tipoMoneda($a10->moneda10).'</moneda>
                        <monto_operacion>'.$a10->monto_operacion10.'</monto_operacion>
                      </datos_operacion_financiera>
                    </compra_venta_entidades_mercantiles>';
              }   
            }
        $xml.='</tipo_actividad>';
        $xml.='</datos_operacion>
            </detalle_operaciones>';
      }
      $xml.= '
          </aviso>';
    $xml.='
        </informe>
    </archivo>';
    $fecha=date('y-His');
    header('Expires: 0');
    header('Cache-control: private');
    header ("Content-type: text/xml"); // Archivo xml
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Content-Disposition: attachment; filename="'.$fecha.''.$cv.'.xml"');
    echo $xml;
  }

  function eliminar_acentos($cadena){
    //Reemplazamos la A y a
    $cadena = str_replace(
    array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
    array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
    $cadena
    );

    //Reemplazamos la E y e
    $cadena = str_replace(
    array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
    array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
    $cadena );

    //Reemplazamos la I y i
    $cadena = str_replace(
    array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
    array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
    $cadena );

    //Reemplazamos la O y o
    $cadena = str_replace(
    array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
    array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
    $cadena );

    //Reemplazamos la U y u
    $cadena = str_replace(
    array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
    array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
    $cadena );

    //Reemplazamos la N, n, C y c
    $cadena = str_replace(
    array('Ç', 'ç'),
    array('C', 'c'),
    $cadena );
    
    return $cadena; 
  }

  public function tipoMoneda($r_moneda){
    if($r_moneda==0)//pesos
        $tipo_moneda=1;
    else if($r_moneda==1)//dolar americano
        $tipo_moneda=2;
    else if($r_moneda==2)//euro
        $tipo_moneda=3;
    else if($r_moneda==3)//libra esterlina
        $tipo_moneda=51;
    else if($r_moneda==4)//dolar canadiense
        $tipo_moneda=30;
    else if($r_moneda==5)//yuan chino
        $tipo_moneda=34;
    else if($r_moneda==6)//centenario
        $tipo_moneda=159;
    else if($r_moneda==7)//onza de plata
        $tipo_moneda=175;
    else if($r_moneda==8)//onza de oro
        $tipo_moneda=176;

    return $tipo_moneda;
  }

}
