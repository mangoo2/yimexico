<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Umas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('General_model');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->usuarioid=$this->session->userdata('usuarioid');
            //ira el permiso del modulo
            $this->load->model('Login_model');
            $permiso=1;
            //$permiso=$this->Login_model->getviewpermiso($this->usuarioid,8);// 8 es el id del submenu
            if ($permiso==0) {
                //redirect('/Sistema');
            }
        }
    }

    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/umas/lista');
        $this->load->view('templates/footer');
        $this->load->view('configuracion/umas/listajs');
    }

    public function alta($id=0){
        if($id>0){
            $data['umas'] = $this->General_model->get_tableRow("umas",array("id"=>$id));
        }
        else{
            $data="";
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/umas/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('configuracion/umas/addjs');
    }

    public function datatable_records(){
        $datas = $this->ModeloCatalogos->getselectwherestatus('*',"umas",array("status"=>1));
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    public function submit(){
        $data=$this->input->post();
        if($data['id']==0){ //insert
            $id=$this->ModeloCatalogos->tabla_inserta("umas",$data);
        }
        else{ //update
            $id=$data["id"]; unset($data["id"]);
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'umas');
        }
        echo $id;
    }

    public function eliminar(){
        $id=$this->input->post("id");
        $this->ModeloCatalogos->updateCatalogo(array("status"=>0),'id',$id,'umas');
    }

    public function getUmasAnio(){
        $anio=$this->input->post("anio");
        if(strlen($anio)>4){
            $anio= date("Y", strtotime($anio));
        }
        log_message('error', 'anio: '.$anio);
        $data = $this->ModeloCatalogos->getUmasAnio($anio);
        $cont=0;
        $valor=0;
        foreach ($data as $k) {
            $cont++;
            $valor=$k->valor;
        }
        if($cont>0){
            echo $valor;
        }else{
            $anio = $anio-1;
            $data = $this->ModeloCatalogos->getUmasAnio($anio);
            foreach ($data as $k) {
                $valor=$k->valor;
            }   
            echo $valor;
        }
    }

    
}