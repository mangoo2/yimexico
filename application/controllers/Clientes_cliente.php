<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php'; 
use Box\Spout\Reader\ReaderFactory;   
use Box\Spout\Common\Type;

class Clientes_cliente extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('General_model');
    $this->load->model('ModeloCliente');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloTransaccion');
    $this->load->library('excel'); 
  
    if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        //ira el permiso del modulo
    /*
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,9);// 2 es el id del submenu
        if ($permiso==0) {
            redirect('/Sistema');
        }
     */   
      //error_reporting(0);
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
    $this->horahoy = date('g:i a');
  }
  
  public function index(){
      $data['perfilid']=$this->perfilid; 
      $data['get_tipo_cliente']=$this->ModeloCatalogos->getselectwhere('tipo_cliente','activo',1);
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/cliente_cliente/listadocliente',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/cliente_cliente/listadocliente_js');
  }

  /*public function addCliente($id=0){
    if($id==0){
      $data['titulo']='Nuevo';  
      $data['boton']='Guardar';
    }else{
      $data['cc'] = $this->ModeloCatalogos->getselectwhere('clientes_clientes','id',$id);
    /////////=====////////   
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/form_clientec',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/form_clientec_js');
  }*/

  public function submit(){
    $data=$this->input->post();

    $this->db->trans_start();
    if(!isset($data['id'])){ //insert
        $id=$this->ModeloCatalogos->tabla_inserta('clientes_clientes',$data);
    }
    else{ //update
        $id=$data["id"]; unset($data["id"]);
        $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'clientes_clientes');
    }
    $this->db->trans_complete();
    $this->db->trans_status();
  }

  /*public function datatable_records(){
    $clientesc1 = $this->ModeloCliente->get_tipoclientes_all($this->idcliente);
    foreach ($clientesc1 as $row) {
      $clientesc = $this->ModeloCliente->get_clientes_all($row->idtipo_cliente);
      $json_data = array("data" => $clientesc);
      echo json_encode($clientesc);
    }    
  }*/

  public function datatable_records(){
    $tipo = $this->input->post('tipo');
    if($tipo=="1"){
      $clientesc = $this->ModeloCliente->get_clientes_all($this->idcliente);
    }else{
      $clientesc = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
    }
    $json_data = array("data" => $clientesc);
    echo json_encode($json_data);
  }

  public function detalleFechaOpera(){
    $id = $this->input->post('id');
    $act = $this->input->post('act');
    //log_message('error', 'id: '.$id);
    //log_message('error', 'act: '.$act);
    if($act==1) //juegos y apuestas 
      $data=$this->ModeloCatalogos->getselectwhere('anexo1','id',$id);
    else if($act==2) //tarjeta prepagadas                                                                 
      $data=$this->ModeloCatalogos->getselectwhere('anexo2a','id',$id);
    else if($act==3) //Anexo 2B - Emisión, comercialización o abono de tarjetas prepagadas, vales y/o cupones.
      $data=$this->ModeloCatalogos->getselectwhere('anexo2b','id',$id);
    else if($act==4) //Anexo 2C - Monederos y tarjetas de devoluciones y recompensas
      $data=$this->ModeloCatalogos->getselectwhere('anexo2c','id',$id);
    else if($act==5) //Anexo 3 - Cheques de viajero.
      $data=$this->ModeloCatalogos->getselectwhere('anexo3','id',$id);
    else if($act==6) //Anexo 4 - Servicios de mutuo, préstamos o créditos.
      $data=$this->ModeloCatalogos->getselectwhere('anexo4','id',$id);
    else if($act==7) //Anexo 5A - Servicios de construcción o desarrollo inmobiliario
      $data=$this->ModeloCatalogos->getselectwhere('anexo5a','id',$id);
    else if($act==8) //Anexo 5B - Desarrollos Inmobiliarios
      $data=$this->ModeloCatalogos->getselectwhere('anexo5b','id',$id);
    else if($act==9) //Anexo 6 - Comercialización de metales preciosos, joyas y relojes
      $data=$this->ModeloCatalogos->getselectwhere('anexo6','id',$id);
    else if($act==10) //Anexo 7
      $data=$this->ModeloCatalogos->getselectwhere('anexo7','id',$id);
    else if($act==11) //vehiculos
      $data=$this->ModeloCatalogos->getselectwhere('anexo_8','id',$id);
    else if($act==12) //Anexo 9
      $data=$this->ModeloCatalogos->getselectwhere('anexo9','id',$id);
    else if($act==13) //Anexo 10
      $data=$this->ModeloCatalogos->getselectwhere('anexo10','id',$id);
    else if($act==14) //Anexo 11 servicios profesionales
      $data=$this->ModeloCatalogos->getselectwhere('anexo11','id',$id);
    else if($act==15) //Anexo 12A
      $data=$this->ModeloCatalogos->getselectwhere('anexo12a','id',$id);
    else if($act==16) //Anexo 12B
      $data=$this->ModeloCatalogos->getselectwhere('anexo12b','id',$id);
    else if($act==17) //Anexo 13 - Donativos
      $data=$this->ModeloCatalogos->getselectwhere('anexo13','id',$id);
    else if($act==19) //Anexo 15 - Arrendamiento de inmueble
      $data=$this->ModeloCatalogos->getselectwhere('anexo_15','id',$id);
    else if($act==20) //Anexo 16 - Activos virtuales.
      $data=$this->ModeloCatalogos->getselectwhere('anexo16','id',$id);

    $return = Array('fecha_operacion'=>$data[0]->fecha_operacion,'mes_acuse'=>$data[0]->mes_acuse);
    echo json_encode($return);
    //echo $data[0]->fecha_operacion;
  }

  public function file_delete($id,$col){  
    $array = array($col => '');
    $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_cliente');
    $data=[];
    echo json_encode($data);
  }

  public function file_deleteExtra($id,$col){  
    $array = array($col => '');
    $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_extra_clientesc');
    $data=[];
    echo json_encode($data);
  }

  public function docs_cliente($idc,$idtipo,$idp,$idopera=0){
    $data['tipo_cliente'] = $idtipo;
    $data['id_clientec'] = $idc;
    if($idtipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($idtipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($idtipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($idtipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($idtipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($idtipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

    $data["grado"]=0; 
    $data['info']=$this->General_model->get_tableRow($tabla,array($idt=>$idc)); 
    $data['docs']=$this->General_model->get_tableRow('docs_cliente',array('id_clientec'=>$idc,'id_perfilamiento'=>$idp)); 
    $data['docs_ext']=$this->General_model->get_tableRow('docs_extra_clientesc',array('id_clientec'=>$idc,'id_perfilamiento'=>$idp)); 
    $data['idopera'] = $idopera;

    /* ********************************** */
    $es_pep=0;
    $get_fp=$this->General_model->get_tableRowC('id','formulario_pep',array('idperfilamiento'=>$idp,'accionista'=>0));
    if(isset($get_fp)){
      $es_pep = $get_fp->id;
    }
    $data["es_pep"]=$es_pep;
    /* ********************************** */
    $get_gr=$this->General_model->get_tableRowC('idtipo_cliente,id,clasificacion,grado,id_actividad','grado_riesgo_actividad',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    if(isset($get_gr)){
      $data["id_actividad"]=$get_gr->id_actividad;
    }else{
       $data["id_actividad"]=0;
    }

    $data["emb_cons_org_inter"]=0;
    if($idtipo==5){
      $data["emb_cons_org_inter"]=1;
    }
    if(isset($get_gr->idtipo_cliente)){
      $data["idtipo_cliente"]=$get_gr->idtipo_cliente;
      $data["clasificacion"]=$get_gr->clasificacion;
    }else{
      $data["idtipo_cliente"]=$idtipo;
      $data["clasificacion"]=0; 
    }
    //verificar si existen cuestioanrio pep y ampliado, si no desvalidar expediente
    if(isset($get_gr) && $get_gr->idtipo_cliente==5 || isset($get_gr) && $get_gr->idtipo_cliente==1 && isset($get_gr) && $get_gr->clasificacion>0 && $get_gr->clasificacion<=7 || isset($get_gr) && $get_gr->grado > 2.0){
      $data["grado"]=$get_gr->grado; 
      if(isset($data['docs']) && $data['docs']->cuest_pep=="" || isset($data['docs']) && $data['docs']->cuest_amp==""){
        $array = array('validado'=>0);
        $this->ModeloCatalogos->updateCatalogo2($array,array('id_clientec'=>$idc,'id_perfilamiento'=>$idp),'docs_cliente');
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/paso5',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/paso5_js');
  }

  public function verificaFoto(){
    $idcc = $this->input->post('idcc');
    $idper = $this->input->post('idper');
    $col = $this->input->post('col');
    $id_opera = $this->input->post('id_opera');
    $data=$this->General_model->get_recordFoto($idper,$idcc,'docs_cliente',$col,$id_opera);
    echo json_encode($data);
  }

  public function verificaVigencia(){
    $idcc = $this->input->post('idcc');
    $idper = $this->input->post('idper');
    $id = 0;
    $data=$this->General_model->get_tableRowC('TIMESTAMPDIFF(MONTH, fecha, CURDATE()) AS vigencia, fecha, id','docs_cliente',array('id_perfilamiento'=>$idper,"id_clientec"=>$idcc));
    if($data->vigencia>=3){
      $array = array('validado' => 0,'fecha_valida'=>"");
      $this->ModeloCatalogos->updateCatalogo($array,'id',$data->id,'docs_cliente');
      $id = $data->id;
    }
    echo $id;
    //log_message('error', 'vigencia: '.$data->vigencia);
  }

  public function verificaFotoExtra(){
    $idcc = $this->input->post('idcc');
    $idper = $this->input->post('idper');
    $col = $this->input->post('col');
    $col2 = $this->input->post('col2');
    $id_opera = $this->input->post('id_opera');
    $data=$this->General_model->get_recordFotoExtra($idper,$idcc,'docs_extra_clientesc',$col,$col2,$id_opera);
    echo json_encode($data);
  }

  public function validar(){
      $id = $this->input->post('id');
      $val = $this->input->post('val');
      $data = array('validado' => $val,'fecha_valida'=>date("Y-m-d H:i:s"));
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'docs_cliente');
  }

  public function validar_sin_bene(){
      $id = $this->input->post('id');
      $val = $this->input->post('val');
      $data = array('validado' => $val);
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'doc_sin_bene');
  }

  function cargafiles($id,$idc,$idp,$img,$id_opera=0){
    $iddoc = $this->input->post('iddoc');

    if($img=='formato_pb') $folder = "formato_perb";
    if($img=='formato_cc') $folder = "formato_cc";
    if($img=='ine' || $img=='ine_apo') $folder = "ine";
    if($img=='cif') $folder = "cif";
    if($img=='curp') $folder = "curp";
    if($img=='comprobante_dom' || $img=='comprobante_dom_apod') $folder = "compro_dom";
    if($img=='formato_duen_bene') $folder = "compro_due_ben";
    if($img=='const_est_legal' || $img=='const_est_legal2' || $img=='const_est_legal3') $folder = "constancia_legal"; 
    if($img=='carta_poder') $folder = "carta_poder";
    if($img=='esc_ints') $folder = "escrituras";
    if($img=='poder_not_fac_serv' || $img=="poder_not" || $img=="poder_not2") $folder = "poder_not"; 
    if($img=='cuest_pep') $folder = "cuestionario_pep"; 
    if($img=='cuest_amp') $folder = "cuestionario_amp"; 

    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    //$archivador = $upload_folder . '/' . $nombre_archivo;
    $fecha=date('ymd-His');
    $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
    $nombre_archivo=str_replace('-', '_', $nombre_archivo);
    $nombre_archivo=$this->eliminar_acentos($nombre_archivo);

    $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('id_perfilamiento'=>$idp,'id_clientec'=>$idc,$img=>$newfile,'fecha'=>$this->fechaactual,"id_operacion"=>$id_opera);
      //insertar este reg en bitacora de documentos por cliente
      $this->ModeloCatalogos->tabla_inserta('bitacora_docs_cliente',array("id_perfilamiento"=>$idp,"id_clientec"=>$idc,"tipo_doc"=>$img,"nombre_doc"=>$newfile,"fecha"=>date("Y-m-d H:i:s"),"id_cliente"=>$this->idcliente,"id_operacion"=>$id_opera)); //agregar idcliente
      if($id==0){ //insert
        $id=$this->ModeloCatalogos->tabla_inserta('docs_cliente',$array);
        //log_message('error', 'id: '.$iddoc);
      }
      if($id>0){ //update
        //log_message('error', 'id: '.$iddoc);
        //$id=$iddoc; //unset($data["id"]);
        $datar['id_perfilamiento'] = $idp;
        $this->ModeloCatalogos->updateCatalogo($array,'id_perfilamiento',$idp,'docs_cliente');
      }
      $return = Array('id'=>$id,'ok'=>TRUE);
    }
    echo json_encode($return);
  }

  function cargafilesExtra($id,$idc,$idp,$img,$id_opera=0){
    $iddoc = $this->input->post('iddoc');

    $upload_folder ='uploads/extras';
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    //$archivador = $upload_folder . '/' . $nombre_archivo;
    $fecha=date('ymd-His');
    $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
    $nombre_archivo=str_replace('-', '_', $nombre_archivo);
    $nombre_archivo=$this->eliminar_acentos($nombre_archivo);

    $newfile=$fecha.'-'.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
        $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      //insertar este reg en bitacora de documentos por cliente
      $this->ModeloCatalogos->tabla_inserta('bitacora_docs_cliente',array("id_perfilamiento"=>$idp,"id_clientec"=>$idc,"tipo_doc"=>"doc extra","nombre_doc"=>$newfile,"fecha"=>date("Y-m-d H:i:s"),"id_cliente"=>$this->idcliente,"id_operacion"=>$id_opera));

      $array = array('id_perfilamiento'=>$idp,'id_clientec'=>$idc,$img=>$newfile,'fecha'=>date("Y-m-d H:i:s"),"id_operacion"=>$id_opera);
      if($id==0){ //insert
        $id=$this->ModeloCatalogos->tabla_inserta('docs_extra_clientesc',$array);
        //log_message('error', 'id: '.$iddoc);
      }
      if($id>0){ //update
        //log_message('error', 'id: '.$iddoc);
        $id=$iddoc; //unset($data["id"]);
        $datar['id_perfilamiento'] = $idp;
        $id = $this->ModeloCatalogos->updateCatalogo($array,'id_perfilamiento',$idp,'docs_extra_clientesc');
      }
        $return = Array('id'=>$id,'ok'=>TRUE);
    }
    echo json_encode($return);
  }

  public function eliminar_acentos($cadena){
      //Reemplazamos la A y a
      $cadena = str_replace(
      array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
      array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
      $cadena
      );

      //Reemplazamos la E y e
      $cadena = str_replace(
      array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
      array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
      $cadena );

      //Reemplazamos la I y i
      $cadena = str_replace(
      array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
      array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
      $cadena );

      //Reemplazamos la O y o
      $cadena = str_replace(
      array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
      array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
      $cadena );

      //Reemplazamos la U y u
      $cadena = str_replace(
      array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
      array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
      $cadena );

      //Reemplazamos la N, n, C y c
      $cadena = str_replace(
      array('Ç', 'ç'),
      array('C', 'c'),
      $cadena
      );
      
      return $cadena;
  }


  public function cargarForm(){
    $inciso = $this->input->post('inciso');
    $edit = $this->input->post('edita');
    $idbene = $this->input->post('idbene');

    if(isset($edit)){
      $data['b']=$this->General_model->get_tableRow("beneficiario_moral",array('id'=>$idbene));
      if($data['b']->identificacion_beneficiario=="c")
        $data['bd']=$this->General_model->get_tableRow("beneficiario_moral_c_d",array('id_bene_moral'=>$data['b']->id));
    }
    $data['get_tipo_vialidad']=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
    $data['get_estado'] = $this->ModeloCatalogos->getData('estado');
    if($inciso=="a")
      $this->load->view('catalogos/cliente_cliente/paso3_a',$data);
    if($inciso=="b")
      $this->load->view('catalogos/cliente_cliente/paso3_b',$data);
    if($inciso=="c" || $inciso=="d")
      $this->load->view('catalogos/cliente_cliente/paso3_c_d');
  }

  public function searchpais(){
        $search = $this->input->get('search');
        $results=$this->ModeloCliente->get_select_like_pais($search);
        echo json_encode($results);    
  }

  public function updateregistro(){
      /*$id = $this->input->post('id');
      $tipo = $this->input->post('tipo');
      if($tipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m'}
      if($tipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e'}
      if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e'}
      if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d'}
      if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'}
      if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f'}

      $data = array('activo' => 0);
      $this->ModeloCatalogos->updateCatalogo($data,$idt,$id,$tabla);*/
      $id = $this->input->post('id');
      $tipo = $this->input->post('tipo');
      $union = $this->input->post('union');
      $data = array('activo' => 0);
      if($tipo==1){
        $this->ModeloCatalogos->updateCatalogo($data,'idperfilamiento',$id,'perfilamiento');
      }else{
        $this->ModeloCatalogos->updateCatalogo($data,'id',$union,'uniones');
      }  
  }                                                  //cliente_cliente  
  public function registros_transaccion($idactividad,$idciente,$idperfilam){
    $html='';
    $arrayperfilamiento = array('idperfilamiento' =>$idperfilam,'idcliente'=>$this->idcliente);
    $perfilamiento = $this->ModeloCatalogos->getselectwherestatus('*','perfilamiento',$arrayperfilamiento);
    foreach ($perfilamiento as $item) {
      $idperfilamiento=$item->idperfilamiento;
      $idcliente=$item->idcliente;
      $idtipo_cliente=$item->idtipo_cliente;
    }
    $html.='<input type="hidden" id="idperfilamiento_cliente" value="'.$idperfilamiento.'">';//Este input se ocupara para saber los beneficiarios que tiene este cliente de cliente  
    $html.='<input type="hidden" id="idtipo_cliente" value="'.$idtipo_cliente.'">';
    $html.='<input type="hidden" id="idciente" value="'.$idciente.'">';//Este input es cliente de cliente
    $html.='<input type="hidden" id="idactividad" value="'.$idactividad.'">';// Este es actividad se ocupa para detectar el anexo
    //$html.='<input type="hidden" id="idtipo_cliente" value="'.$idtipo_cliente.'">';// Es para saber el tipo de cliente
    if($idtipo_cliente==1){
      $array_tipo_cliente = array('idperfilamiento' =>$idperfilamiento);
      $tipo_cliente_p_f_m = $this->ModeloCatalogos->getselectwherestatus('nombre, apellido_paterno, apellido_materno','tipo_cliente_p_f_m',$array_tipo_cliente);
      foreach ($tipo_cliente_p_f_m as $item) {
      
      $html.='<h3 >Registro de nueva transacción de: '.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</h3>
              <hr class="subtitle">';
      }
    }else if($idtipo_cliente==2){
      $array_tipo_cliente = array('idperfilamiento' =>$idperfilamiento);
      $tipo_cliente_p_f_e = $this->ModeloCatalogos->getselectwherestatus('nombre, apellido_paterno, apellido_materno','tipo_cliente_p_f_e',$array_tipo_cliente);
      foreach ($tipo_cliente_p_f_m as $item) {
      $html.='<h3 >Registro de nueva transacción de: '.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</h3>
           <hr class="subtitle">';
      }
    }else if($idtipo_cliente==3){
      $array_tipo_cliente = array('idperfilamiento' =>$idperfilamiento);
      $tipo_cliente_p_m_m_e = $this->ModeloCatalogos->getselectwherestatus('razon_social','tipo_cliente_p_m_m_e',$array_tipo_cliente);
      foreach ($tipo_cliente_p_m_m_e as $item) {
      $html.='<h3 >Registro de nueva transacción de: '.$item->razon_social.'</h3>
           <hr class="subtitle">';
      }
    }else if($idtipo_cliente==4){
      $array_tipo_cliente = array('idperfilamiento' =>$idperfilamiento);
      $tipo_cliente_p_m_m_d = $this->ModeloCatalogos->getselectwherestatus('nombre_persona','tipo_cliente_p_m_m_d',$array_tipo_cliente);
      foreach ($tipo_cliente_p_m_m_d as $item) {
      $html.='<h3 >Registro de nueva transacción de: '.$item->nombre_persona.'</h3>
              <hr class="subtitle">';
      }
    }else if($idtipo_cliente==5){
      $array_tipo_cliente = array('idperfilamiento' =>$idperfilamiento);
      $tipo_cliente_e_c_o_i = $this->ModeloCatalogos->getselectwherestatus('denominacion','tipo_cliente_e_c_o_i',$array_tipo_cliente);
      foreach ($tipo_cliente_e_c_o_i as $item) {
      $html.='<h3 >Registro de nueva transacción de: '.$item->denominacion.'</h3>
              <hr class="subtitle">';
      }
    }else if($idtipo_cliente==6){
      $array_tipo_cliente = array('idperfilamiento' =>$idperfilamiento);
      $tipo_cliente_f = $this->ModeloCatalogos->getselectwherestatus('denominacion','tipo_cliente_f',$array_tipo_cliente);
      foreach ($tipo_cliente_f as $item) {
      $html.='<h3 >Registro de nueva transacción de: '.$item->denominacion.'</h3>
              <hr class="subtitle">';
      }
    }
    $data['html_text']=$html;
    $data['get_estado'] = $this->ModeloCatalogos->getData('estado');
    $data['get_actividad'] = $this->ModeloCatalogos->getData('actividad_econominica_fisica');
    $data['get_tipo_vialidad']=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/addtransaccion',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/transaccionjs');
  }
  public function link_actividades_clientes(){ //actividades vulnerables disponibles del usuario
    $id = $this->input->post('id');
    $iper = $this->input->post('idp');
    $id_act_cal = $this->input->post('id_act_cal');

    $html='';
    if($iper==''){
      $iper=0;
    }
    //$result=$this->ModeloCliente->getselectactividades($id);
    $result = $this->ModeloCliente->getActividadesCliente($this->idcliente);
    /*foreach ($result as $item) {
      $html.='<a href="Clientes_cliente/registros_transaccion/'.$item->id.'/'.$iper.'">'.$item->actividad.'</a>';
    }*/
    $html.='<div align="center">
             <h5>Seleccione una actividad:</h5>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <select class="form-control" id="selec_activ">';
                foreach ($result as $item) {
                  if(isset($id_act_cal) && $id_act_cal>0){
                    if($id_act_cal==$item->idactividad){
                      $html.='<option value="'.$item->idactividad.'">'.$item->nombre.'</option>';
                    }
                  }else if(!isset($id_act_cal) || $id_act_cal==0){
                    $html.='<option value="'.$item->idactividad.'">'.$item->nombre.'</option>';
                  }
                }
            $html.='</select>       
              </div>
            </div>';
    echo $html;    
  }
  public function get_beneficiarios_select(){
    $idc = $this->input->post('idc');
    $idp = $this->input->post('idp');
    $idu = $this->input->post('id_union');
    $id_opera = $this->input->post('id_opera');
    $id_duenio_bene="";
    $tipo_bene="";
    $html="";

    if(isset($_POST['id_duenio_bene'])) {
      $id_duenio_bene = $this->input->post('id_duenio_bene');
      $tipo_bene = $this->input->post('tipo_bene');
    }

    /*log_message('error', 'id_duenio_bene: '.$id_duenio_bene);
    log_message('error', 'tipo_bene: '.$tipo_bene);*/
    $html.='<div align="center">
                <h5>Seleccione al dueño beneficiario de su catálogo</h5>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <select class="form-control" id="beneficiario_" onchange="validaBenef(this.value);">';
                  $html.='<option value="">Elige un beneficiario:</option>';
    //log_message('error', 'idu: '.$idu);
    if($idu==0){
      $result_tipo1=$this->ModeloCliente->get_beneficiario_tansferenciaF($idc,$idp);
      $result_tipo2=$this->ModeloCliente->get_beneficiario_tansferenciaM($idc,$idp);
      $result_tipo3=$this->ModeloCliente->get_beneficiario_tansferenciaFide($idc,$idp);
    }else{
      $get_bu=$this->ModeloCatalogos->getselectwherestatus("*","beneficiario_union_cliente",array("id_union"=>$idu));        
      foreach ($get_bu as $k) {
        $id_boa=0;
        $tipo_bene_asigna=0;
        $dis="";
        $msj="";
        $get_bo=$this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_opera,"activo"=>1)); 
        
        if($k->tipo_bene==1){
          $result_tipo1=$this->ModeloCliente->get_beneficiario_f($k->id_duenio_bene);
        }
        else if($k->tipo_bene==2){
          $result_tipo2=$this->ModeloCliente->get_beneficiario_m($k->id_duenio_bene);
        }
        else if($k->tipo_bene==3){
          $result_tipo3=$this->ModeloCliente->get_beneficiario_fide($k->id_duenio_bene);
        }

        if($k->tipo_bene==1){
          foreach ($result_tipo1 as $item) {
            $id_boa =0;
            $tipo_bene_asigna =0;
            $dis="";
            $msj="";
            $select="";
            foreach ($get_bo as $k2) {
              $id_boa = $k2->id_duenio_bene;
              $tipo_bene_asigna = $k2->tipo_bene;
              if($k->tipo_bene==1 && $tipo_bene_asigna==1 && $id_boa==$item->idbf){
                $dis="disabled";
                $msj="- Beneficiario asignado previamente";
              }
            }
            $select="";
            if($item->beneficiario!='0'){
              //if($item->idbf!="0"){
              if($id_duenio_bene==$item->idbf && $tipo_bene=="1" || $id_duenio_bene==$item->idbf && $tipo_bene=="fisica") { 
                $select="selected";
              }
              $html.='<option '.$select.' '.$dis.' value="'.$item->idbf.'-fisica">'.$item->beneficiario.' '.$msj.'</option>';
              //}
            }
          }
        }
        if($k->tipo_bene==2){
          foreach ($result_tipo2 as $item2) {
            $id_boa =0;
            $tipo_bene_asigna =0;
            $dis="";
            $msj="";
            $select="";
            //if($item->beneficiario!='0'){
            foreach ($get_bo as $k2) {
              $id_boa = $k2->id_duenio_bene;
              $tipo_bene_asigna = $k2->tipo_bene;
              if($k->tipo_bene==2 && $tipo_bene_asigna==2 && $id_boa==$item2->idbmm){
                $dis="disabled";
                $msj="- Beneficiario asignado previamente";
              }
            }
              $select="";
              if($item2->idbmm=='0' && $item2->idbmf!='0'){
                if($id_duenio_bene==$item2->idbmf && $tipo_bene=="2" || $id_duenio_bene==$item2->idbmf && $tipo_bene=="moralfisica") { 
                  $select="selected";
                }
                $html.='<option '.$select.' '.$dis.' value="'.$item2->idbmf.'-moralfisica-'.$item2->idbmf_bm.'">'.$item2->beneficiariof.' '.$msj.'</option>';
              }
              if($item2->idbmm!='0' && $item2->idbmf!='0'){
                if($id_duenio_bene==$item2->idbmm && $tipo_bene=="2" || $id_duenio_bene==$item2->idbmm && $tipo_bene=="moralmoral") { 
                  $select="selected";
                }
                $html.='<option '.$select.' '.$dis.' value="'.$item2->idbmm.'-moralmoral-'.$item2->idbmm_bm.'">'.$item2->beneficiariom.' '.$msj.'</option>';
              }
            //}
          }
        }
        if($k->tipo_bene==3){
          $id_boa =0;
          $tipo_bene_asigna =0;
          $dis="";
          $msj="";
          $select="";
          foreach ($result_tipo3 as $item3) {
            foreach ($get_bo as $k2) {
              $id_boa = $k2->id_duenio_bene;
              $tipo_bene_asigna = $k2->tipo_bene;
              if($k->tipo_bene==3 && $tipo_bene_asigna==3 && $id_boa==$item3->idbf){
                $dis="disabled";
                $msj="- Beneficiario asignado previamente";
              }
            }
            $select="";
            if($id_duenio_bene==$item3->idbf && $tipo_bene=="3" || $id_duenio_bene==$item3->idbf && $tipo_bene=="fideicomiso") { 
              $select="selected";
            }
            if($item3->beneficiario!='0'){
                $html.='<option '.$select.' '.$dis.' value="'.$item3->idbf.'-fideicomiso-'.$item3->idbf_bm.'">'.$item3->beneficiario.' '.$msj.'</option>';
            }
          }
        }

      }
          
    }

    //if($idu==0){
      $aux=0; $id=0;
      
      /*foreach ($result_tipo1 as $item) {
         $aux=1;  
      }*/ 
      //if($result_tipo1!=null){
        /*$aux_b=0;
        foreach ($result_tipo1 as $item) {
          $aux_b=1;
        }*/
        //if($aux_b==1){
        if($idu==0){
          $get_bo=$this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_opera,"activo"=>1)); 
          foreach ($result_tipo1 as $item) {
            $id_boa=0;
            $tipo_bene_asigna=0;
            $dis="";
            $msj="";
            foreach ($get_bo as $k2) {
              $id_boa = $k2->id_duenio_bene;
              $tipo_bene_asigna = $k2->tipo_bene;
              /*log_message('error', 'item tipo_bene: '.$item->tipo_bene);
              log_message('error', 'tipo_bene_asigna ya asignado: '.$tipo_bene_asigna);
              log_message('error', 'id_boa ya asignado: '.$id_boa);
              log_message('error', 'id_duenio_bene: '.$item->idbf);*/
              if($item->tipo_bene==1 && $tipo_bene_asigna==1 && $id_boa==$item->idbf){
                $dis="disabled";
                $msj="- Beneficiario asignado previamente";
              }
            }
            $select="";
            if($item->beneficiario!='0'){
              //if($item->idbf!="0"){
              if($id_duenio_bene==$item->idbf && $tipo_bene=="1" || $id_duenio_bene==$item->idbf && $tipo_bene=="fisica") { 
                $select="selected";
              }
              $html.='<option '.$select.' '.$dis.' value="'.$item->idbf.'-fisica">'.$item->beneficiario.' '.$msj.'</option>';
              //}
            }
          }
          foreach ($result_tipo2 as $item2) {
            $id_boa=0;
            $tipo_bene_asigna=0;
            $dis2="";
            $msj2="";
            //if($item->beneficiario!='0'){
            foreach ($get_bo as $k2) {
              $id_boa = $k2->id_duenio_bene;
              $tipo_bene_asigna = $k2->tipo_bene;
              /*log_message('error', 'item tipo_bene: '.$item2->tipo_bene);
              log_message('error', 'tipo_bene_asigna ya asignado: '.$tipo_bene_asigna);
              log_message('error', 'id_boa ya asignado: '.$id_boa);
              log_message('error', 'id_duenio_bene: '.$item2->idbmm);*/
              if($item2->tipo_bene==2 && $tipo_bene_asigna==2 && $id_boa==$item2->idbmm){
                $dis2="disabled";
                $msj2="- Beneficiario asignado previamente";
                //log_message('error', 'igual al tipo2: '.$item2->idbmm);
              }
            }
            //log_message('error', 'dis2: '.$dis2);
            //log_message('error', 'msj2: '.$msj2);
              $select2="";
              if($item2->idbmm=='0' && $item2->idbmf!='0'){
                if($id_duenio_bene==$item2->idbmf && $tipo_bene=="2" || $id_duenio_bene==$item2->idbmf && $tipo_bene=="moralfisica") { 
                  $select2="selected";
                }
                $html.='<option '.$select2.' '.$dis2.' value="'.$item2->idbmf.'-moralfisica-'.$item2->idbmf_bm.'">'.$item2->beneficiariof.' '.$msj2.'</option>';
              }
              if($item2->idbmm!='0' && $item2->idbmf=='0'){
                if($id_duenio_bene==$item2->idbmm && $tipo_bene=="2" || $id_duenio_bene==$item2->idbmm && $tipo_bene=="moralmoral") { 
                  $select2="selected";
                }
                $html.='<option '.$select2.' '.$dis2.' value="'.$item2->idbmm.'-moralmoral-'.$item2->idbmm_bm.'">'.$item2->beneficiariom.' '.$msj2.'</option>';
              }
            //}
          }
          foreach ($result_tipo3 as $item3) {
            $id_boa=0;
            $tipo_bene_asigna=0;
            $dis3="";
            $msj3="";
            foreach ($get_bo as $k2) {
              $id_boa = $k2->id_duenio_bene;
              $tipo_bene_asigna = $k2->tipo_bene;
              /*log_message('error', 'item tipo_bene: '.$item3->tipo_bene);
              log_message('error', 'tipo_bene_asigna ya asignado: '.$tipo_bene_asigna);
              log_message('error', 'id_boa ya asignado: '.$id_boa);
              log_message('error', 'id_duenio_bene: '.$item3->idbf);*/
              if($item3->tipo_bene==3 && $tipo_bene_asigna==3 && $id_boa==$item3->idbf){
                $dis3="disabled";
                $msj3="- Beneficiario asignado previamente";
              }
            }
            $select="";
            if($id_duenio_bene==$item3->idbf && $tipo_bene=="3" || $id_duenio_bene==$item3->idbf && $tipo_bene=="fideicomiso") { 
              $select="selected";
            }
            if($item3->beneficiario!='0'){
                $html.='<option '.$select.' '.$dis3.' value="'.$item3->idbf.'-fideicomiso-'.$item3->idbf_bm.'">'.$item3->beneficiario.' '.$msj3.'</option>';
            }
          }

        }
    $html.='</select>       
                </div>
              </div>';
    //}
     // }
      /*$html.='<div class="row" id="cont_nvo_bene">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" id="otro_beneficiario" onchange="check_beneficiario()">
                      Registrar nuevo Dueño beneficiario
                    <i class="input-helper"></i></label>
                  </div>
                </div>
              </div>';  */
    /*}else{
    $html.='<div class="row">
              <div class="col-md-5"></div>
              <div class="col-md-4">
                <div class="form-check form-check-flat form-check-primary">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="otro_beneficiario" onchange="check_beneficiario()">
                    Otro beneficiario
                  <i class="input-helper"></i></label>
                </div>
              </div>
            </div>';  
    }*/
    echo $html;
  }
  function guardar_beneficiario_transaccion(){
    $data = $this->input->post();
    $id_opera=0;
    if(isset($data['id_beneficiario'])) $idbene = $data['id_beneficiario']; else $idbene=0;
    $array = array('id_cliente'=>$this->idcliente,'id_tipo_perfil' => $data['id_tipo_perfil'],'id_perfilamiento' => $data['id_perfilamiento'],'id_actividad' => $data['id_actividad'],'tipo_persona' => $data['tipo_persona'],'dueno_beneficiario' => $data['dueno_beneficiario'],
      'id_beneficiario'=>$idbene);
    $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_transaccion',$array);

    if(isset($data['id_cli_opera']) && $data['id_cli_opera']==0){
      $array_co = array('id_perfilamiento'=>$data['id_perfilamiento'],'id_clientec'=>$data['dueno_beneficiario'],'id_cliente'=>$this->idcliente,'id_duenio_bene'=>$idbene,'tipo_bene'=>$data['tipo_persona'],'fecha_reg'=>date('Y-m-d H:i:s'));
      //$id_opera = $this->ModeloCatalogos->tabla_inserta('clientes_operaciones',$array_co); //es necesaria?
      //aca debe ir la nueva tabla para multiples clientes en operaciones
      //$id_opera = $this->ModeloCatalogos->tabla_inserta('operaciones',$array_co); //es necesaria?
    }
    //echo $id; 
    $return = Array('id'=>$id,'id_opera'=>$id_opera);
    echo json_encode($return);
  }

  function guardar_beneficiario_transaccion_nva(){
    $data = $this->input->post();
    $id=0;
    $get_per=$this->General_model->GetAllWhere('operacion_beneficiario',array("id_operacion"=>$data["id_opera"],"activo"=>1));
    foreach ($get_per as $gp) {
      $get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$gp->id_perfilamiento));
      $array = array('id_cliente'=>$this->idcliente,'id_tipo_perfil'=>$get->idtipo_cliente,'id_perfilamiento'=>$gp->id_perfilamiento,'id_actividad'=>$data['id_actividad'],'tipo_persona'=>$gp->tipo_bene,'dueno_beneficiario'=>0,'id_beneficiario'=>$gp->id_duenio_bene);
      $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_transaccion',$array);
    }
    //echo $id; 
    $return = Array('id'=>$id,'id_opera'=>$data["id_opera"]);
    echo json_encode($return);
  }

  public function submit_beneficiarios_transaccionMultiple(){ //add 
    $data = $this->input->post('data');
    $id_opera=0;
    $DATA = json_decode($data);
    for ($i=0; $i<count($DATA); $i++) {
      if(isset($data[$i]->id_beneficiario)) $idbene = $data[$i]->id_beneficiario; else $idbene=0;
        $array = array('id_cliente'=>$this->idcliente,'id_tipo_perfil'=>$data[$i]->id_tipo_perfil,'id_perfilamiento' => $data[$i]->id_perfilamiento,'id_actividad'=>$data[$i]->id_actividad,'tipo_persona'=>$data[$i]->tipo_persona,'dueno_beneficiario'=>$data[$i]->dueno_beneficiario,'id_beneficiario'=>$idbene);
        $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_transaccion',$array);

      if(isset($data[$i]->id_cli_opera) && $data[$i]->id_cli_opera==0){
        $array_co = array('id_perfilamiento'=>$data[$i]->id_perfilamiento,'id_clientec'=>$data[$i]->dueno_beneficiario,'id_cliente'=>$this->idcliente,'id_duenio_bene'=>$idbene,'tipo_bene'=>$data[$i]->tipo_persona,'fecha_reg'=>date('Y-m-d H:i:s'));
        //$id_opera = $this->ModeloCatalogos->tabla_inserta('clientes_operaciones',$array_co); //es necesaria?
        //aca debe ir la nueva tabla para multiples clientes en operaciones
      }
    }
    $return = Array('id'=>$id,'id_opera'=>$id_opera);
    echo json_encode($return);
  }

  function guardar_beneficiario_fisica(){
    $data = $this->input->post();
    $this->db->trans_start();
    $data['id_clientec']=$this->idcliente;
    $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_fisica',$data);

    //$id_benef = $this->input->post('id_benef');
    $id_trans_bene = $this->input->post('id_beneficiario_fisica');
    $data = array('dueno_beneficiario' => $id);
    $this->ModeloCatalogos->updateCatalogo2($data,array('id'=>$id_trans_bene),'beneficiario_transaccion');
    $this->db->trans_complete();
    $this->db->trans_status();
    echo $id; 
  }

  function guardar_beneficiario_moral(){
    $data = $this->input->post();
    $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_moral',$data);
    echo $id; 
  }
  function guardar_beneficiario_moral2(){
    $data = $this->input->post();
    $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_moral_moral',$data);
    echo $id; 
  }
  function guardar_beneficiario_fideicomiso(){
    $data = $this->input->post();
    $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_fideicomiso',$data);
    echo $id; 
  }
  function editarBeneTransac(){
    $id_benef = $this->input->post('id_benef');
    $id_trans_bene = $this->input->post('id_trans_bene');
    $data = array('dueno_beneficiario' => $id_benef);
    $this->ModeloCatalogos->updateCatalogo2($data,array('id'=>$id_trans_bene),'beneficiario_transaccion');
  }

  public function add_transac_bm(){
    $data = $this->input->post();
    $this->db->trans_start();

    $array = array('id_cliente' =>$this->idcliente,'id_tipo_perfil' => $data['id_tipo_perfil'],'id_perfilamiento' => $data['id_perfilamiento'],'id_actividad' => $data['id_actividad'],'tipo_persona' => $data['tipo_persona'],'dueno_beneficiario' => $data['dueno_beneficiario']);
    $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_transaccion',$array);
    //log_message('error', 'id: '.$id);
    $idbm = $this->ModeloCatalogos->tabla_inserta('beneficiario_moral',array('id_bene_trans'=>$id,'tipo_cliente'=>$data['id_tipo_perfil'],
      'id_perfilamiento' => $data['id_perfilamiento'],'identificacion_beneficiario'=>$data['tipo_inciso'],'tipo_persona'=>$data['tipo_p']));
    //log_message('error', 'idbm: '.$idbm);
    $this->db->trans_complete();
    $this->db->trans_status();
    $return = Array('idbm'=>$idbm,'id'=>$id);
    //echo $idbm;
    echo json_encode($return);
  }

  public function guardar_bene_moral_fisica(){
    $data = $this->input->post();
    $tabla = $this->input->post('tabla'); unset($data['tabla']);
    $data['id_bene_moral']= $this->input->post('id_bene_moral');
    $data['porcentaje']= $this->input->post('porcentaje');
    if($tabla=="beneficiario_moral_moral") 
      $data['relacion_juridica']= $this->input->post('relacion_juridica');
      
      $idmf = $this->ModeloCatalogos->tabla_inserta($tabla,$data);
    $this->db->trans_complete();
    $this->db->trans_status();
    echo $idmf;
  }

  public function get_estados(){
      $id_t = $this->input->post('id_t');
      $tex_t = $this->input->post('text_t');
      $html = '';
      $result = $this->ModeloCatalogos->getData('estado');
      $html.= '<select class="form-control estado_'.$tex_t.'_'.$id_t.'" name="estado" id="estado">';
      foreach ($result as $item) {
         $html.='<option value="'.$item->clave.'">'.$item->estado.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
  /*
  public function get_beneficiarios_select(){
    $idper = $this->input->post('id');
    $result_clientes=$this->ModeloCliente->getselectwhere('perfilamiento','idperfilamiento',$idper);
    $html='';
    $idperfilam=0;
    $idtipo_cli=0;
    foreach ($result_clientes as $item) {
      $idperfilam=$item->idperfilamiento;
      $idtipo_cli=$item->idtipo_cliente;
    }
    if($idtipo_cli==1 || $idtipo_cli==2){
    $result_tipo1=$this->ModeloCliente->get_beneficiario_trasaccion($idperfilam); 
    $html.='<div align="center">
             <h5>Seleccione al dueño beneficiario previamente capturado en transacciones pasadas</h5>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <select class="form-control" id="actividad_vulnerable">';
                foreach ($result_tipo1 as $item) {
                  $html.='<option value="'.$item->nombre.'">'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</option>';
                }
            $html.='</select>       
              </div>
            </div>';
            $html.='<div class="row">
              <div class="col-md-12" align="center">
                <button type="button" class="btn btn-success" onclick="otro_beneficiario()">Otro beneficiario</button>
              </div>
            </div>';   
    }
         
    echo $html;
  }
  */
  function guardar_persona_c_d(){
    $data = $this->input->post();
    $id=$this->ModeloCatalogos->tabla_inserta('beneficiario_moral_c_d',$data);
  }

  public function get_estadoMoral(){
      $id = $this->input->post('id');
      $html = '';
      $result = $this->ModeloCatalogos->getData('estado');
      $html.= '<select class="form-control estado_'.$id.'" name="estado" id="estado">';
      foreach ($result as $item) {
         $html.='<option value="'.$item->id.'">'.$item->estado.'</option>';
      }
      $html.= '</select>';
    echo $html;  
  }
  public function tabla_xml(){
    /*$id=$this->input->post('id');
    $html='';
    $get_xml= $this->ModeloCatalogos->getselectwhere('bitacora_xml','idperfilamiento',$id);
    $html.='<table class="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>';
        foreach ($get_xml as $item) {
    $html.='<tr>
              <td>'.$item->nombre.'</td>
              <td><a class="btn btn-sm" style="color: #fff; background-color: #0072ff;" title="Descargar" <a href="'.base_url().'uploads/anexo8/'.$item->nombre.'.xml" download><i class="fa fa-cloud-download"></i></a></td>
            </tr>';
          }  
    
  $html.='</tbody>
        </table>';
    echo $html;*/
    $idp = $this->input->post('idp');
    $records = $this->ModeloCliente->getBitacoraXML($idp);
    $json_data = array("data" => $records);
    echo json_encode($records);
  }

  public function docs_beneficiario($id_benef_m,$id_trans_bene,$idactividad,$idperfilamiento_cliente,$idcliente){
    $data['id_benef_m']=$id_benef_m;
    $data['id_trans_bene']=$id_trans_bene;
    $data['idactividad']=$idactividad;
    $data['idperfilamiento_cliente']=$idperfilamiento_cliente;
    $data['idcientec']=$idcliente;
    $data['docs']=$this->General_model->get_tableRow('docs_beneficiario',array('id_beneficiario'=>$id_benef_m,'id_clientec'=>$idcliente));
    if($id_trans_bene>0){
      $benef = $this->ModeloCatalogos->getselectwhere('beneficiario_transaccion','id',$id_trans_bene);
      foreach ($benef as $item) {
        $tipo = $item->tipo_persona;
      }
    }

    $titilo_t='';
    $tipo_text = '';
    $data['tipo_persona']=$tipo;

    if($tipo==1){
      $titilo_t='Persona Física';
      $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_benef_m);
      foreach ($get_pf as $item) { 
      $estado_f = '';
      if($item->pais=='MX'){
          $get_estado=$this->ModeloCatalogos->getselectwhere('estado','clave',$item->estado);
          foreach ($get_estado as $item_e) {
            $estado_f = $item_e->estado; 
          }
      }else{
        $estado_f=$item->estado;
      }
      $get_pais_aux=$this->ModeloCatalogos->getselectwhere('pais','clave',$item->pais);
      $pais = '';
      foreach ($get_pais_aux as $item_a) {
        $pais = $item_a->pais; 
      }
      $get_tipo_v_aux=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$item->tipo_vialidad);
      $tipo_v = '';
      foreach ($get_tipo_v_aux as $item_e) {
        $tipo_v = $item_e->nombre; 
      }
      $tipo_text='<div class="row">
                    <div class="col-md-12">
                        <label>Nombre:</label>
                        <strong> '.$item->nombre.'</strong>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Apellido paterno:</label>
                      <strong> '.$item->apellido_paterno.'</strong>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Apellido materno:</label>
                      <strong> '.$item->apellido_materno.'</strong>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Nombre de identificación:</label>
                      <strong> '.$item->nombre_identificacion.'</strong>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Autoridad que emite la identificación:</label>
                      <strong> '.$item->autoridad_emite.'</strong>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Número de Identificación:</label>
                      <strong> '.$item->numero_identificacion.'</strong>
                    </div>
                  </div><br>
                  <div class="row">
                      <div class="col-md-12">
                        <h6>Domicilio:</h6>
                      </div>
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Tipo de vialidad:</label>
                            <strong> '.$tipo_v.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>Calle:</label>
                            <strong> '.$item->calle.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>No. ext:</label>
                            <strong> '.$item->no_ext.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>No. int:</label>
                            <strong> '.$item->no_int.'</strong>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Colonia:</label>
                            <strong> '.$item->colonia.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>Municipio o delegación:</label>
                            <strong> '.$item->monicipio.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>Estado:</label>
                              <strong> '.$estado_f.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>C.P:</label>
                            <strong>'.$item->cp.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>País:</label>
                            <strong> '.$pais.'</strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  <br>';
      }  
    }else if($tipo==2){
       $titilo_t='Persona Moral';
       $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id_bene_trans',$id_trans_bene);
       $id_enti_bene='';
       $t_p=0;

       foreach ($get_pm as $item){
          $id_b_m_m=$item->id;
          $id_enti_bene=$item->identificacion_beneficiario;
          $t_p=$item->tipo_persona;
       }
       if($id_enti_bene=='m'){
          if($t_p==2){
            $get_pmm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id_bene_moral',$id_b_m_m);
            foreach ($get_pmm as $item) {
            $tipo_text='<div class="row">
                    <div class="col-md-12">
                        <label>Razón social:</label>
                        <strong> '.$item->razon_social.'</strong>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                      <strong> '.$item->fecha_constitucion.'</strong>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-12">
                      <label>R.F.C:</label>
                      <strong> '.$item->rfc.'</strong>
                    </div>
                  </div>
                  <br>';
            }
          }
       }
    }else if($tipo==3){
       $titilo_t='Fideicomiso';
       $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id_bene_trans',$id_trans_bene);
       $id_enti_bene='';
       $t_p=0;

       foreach ($get_pm as $item){
          $id_b_m_m=$item->id;
          $id_enti_bene=$item->identificacion_beneficiario;
          $t_p=$item->tipo_persona;
       }
       if($id_enti_bene=='f'){
          if($t_p==3){
            $get_pmm=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id_bene_moral',$id_b_m_m);
            foreach ($get_pmm as $item) {
            $tipo_text='<div class="row">
                <div class="col-md-12">
                    <label>Razón social del fiduciario:</label>
                    <strong> '.$item->razon_social.'</strong>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <label>R.F.C del fideicomiso:</label>
                  <strong> '.$item->rfc.'</strong>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-12">
                  <label>Número o referencia del fideicomiso:</label>
                  <strong> '.$item->referencia.'</strong>
                </div>
              </div>
              <br>';
            }
          }
       }
    }
    /*
    else if($tipo==2){
      $titilo_t='Persona Moral';
      $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$id_benef_m);
      foreach ($get_pm as $item) {
        if($item->identificacion_beneficiario=='a'){
          if($item->tipo_persona==1){
            $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_fisica','id_bene_moral',$item->id);  
            foreach ($get_pmf as $item) {
              $estado_m = '';
              if($item->pais=='MX'){
                  $get_estado=$this->ModeloCatalogos->getselectwhere('estado','clave',$item->estado);
                  foreach ($get_estado as $item_e) {
                    $estado_m = $item_e->estado; 
                  }
              }else{
                $estado_f=$item->estado;
              }
              $get_pais_aux=$this->ModeloCatalogos->getselectwhere('pais','clave',$item->pais);
              $pais_m = '';
              foreach ($get_pais_aux as $item_a) {
                $pais_m = $item_a->pais; 
              }
              $get_tipo_v_aux=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$item->tipo_vialidad);
              $tipo_v = '';
              foreach ($get_tipo_v_aux as $item_e) {
                $tipo_v = $item_e->nombre; 
              } 

            $tipo_text.='<div class="row">
                      <div class="col-md-12">
                          <h5>Socio 1</h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                          <label>Nombre:</label>
                          <strong> '.$item->nombre.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Apellido paterno:</label>
                        <strong> '.$item->apellido_paterno.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Apellido materno:</label>
                        <strong> '.$item->apellido_materno.'</strong>
                      </div>
                    </div><br>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Nombre de identificación:</label>
                        <strong> '.$item->nombre_identificacion.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Autoridad que emite la identificación:</label>
                        <strong> '.$item->autoridad_emite.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Número de Identificación:</label>
                        <strong> '.$item->numero_identificacion.'</strong>
                      </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                          <h6>Domicilio:</h6>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Tipo de vialidad:</label>
                              <strong> '.$tipo_v.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Calle:</label>
                              <strong> '.$item->calle.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>No. ext:</label>
                              <strong> '.$item->no_ext.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>No. int:</label>
                              <strong> '.$item->no_int.'</strong>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Colonia:</label>
                              <strong> '.$item->colonia.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Municipio o delegación:</label>
                              <strong> '.$item->municipio.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Estado:</label>
                                <strong> '.$estado_m.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>C.P:</label>
                              <strong>'.$item->cp.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>País:</label>
                              <strong> '.$pais_m.'</strong>
                            </div>
                          </div>
                        </div>
                      </div>
                    <br>';
            }
          }else{
            $tipo_text.='';
          }
        }else if($item->identificacion_beneficiario=='b'){
          if($item->tipo_persona==1){
            $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_fisica','id_bene_moral',$item->id);  
            foreach ($get_pmf as $item) {
              $estado_m = '';
              if($item->pais=='MX'){
                  $get_estado=$this->ModeloCatalogos->getselectwhere('estado','clave',$item->estado);
                  foreach ($get_estado as $item_e) {
                    $estado_m = $item_e->estado; 
                  }
              }else{
                $estado_f=$item->estado;
              }
              $get_pais_aux=$this->ModeloCatalogos->getselectwhere('pais','clave',$item->pais);
              $pais_m = '';
              foreach ($get_pais_aux as $item_a) {
                $pais_m = $item_a->pais; 
              }
              $get_tipo_v_aux=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$item->tipo_vialidad);
              $tipo_v = '';
              foreach ($get_tipo_v_aux as $item_e) {
                $tipo_v = $item_e->nombre; 
              } 

            $tipo_text.='<div class="row">
                      <div class="col-md-12">
                          <h5>Socio 1</h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                          <label>Nombre:</label>
                          <strong> '.$item->nombre.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Apellido paterno:</label>
                        <strong> '.$item->apellido_paterno.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Apellido materno:</label>
                        <strong> '.$item->apellido_materno.'</strong>
                      </div>
                    </div><br>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Nombre de identificación:</label>
                        <strong> '.$item->nombre_identificacion.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Autoridad que emite la identificación:</label>
                        <strong> '.$item->autoridad_emite.'</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Número de Identificación:</label>
                        <strong> '.$item->numero_identificacion.'</strong>
                      </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                          <h6>Domicilio:</h6>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Tipo de vialidad:</label>
                              <strong> '.$tipo_v.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Calle:</label>
                              <strong> '.$item->calle.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>No. ext:</label>
                              <strong> '.$item->no_ext.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>No. int:</label>
                              <strong> '.$item->no_int.'</strong>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Colonia:</label>
                              <strong> '.$item->colonia.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Municipio o delegación:</label>
                              <strong> '.$item->municipio.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>Estado:</label>
                                <strong> '.$estado_m.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>C.P:</label>
                              <strong>'.$item->cp.'</strong>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label>País:</label>
                              <strong> '.$pais_m.'</strong>
                            </div>
                          </div>
                        </div>
                      </div>
                    <br>';
            }
          }else{
            $tipo_text.='';
          }
        }else if($item->identificacion_beneficiario=='c' || $item->identificacion_beneficiario=='d'){
            $get_pm_cd=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_c_d','id_bene_moral',$item->id);
            foreach ($get_pm_cd as $item) {
            $tipo_text.='<div class="row">
                          <div class="col-md-12">
                              <label>Nombre:</label>
                              <strong> '.$item->nombre.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>Apellido paterno:</label>
                            <strong> '.$item->app.'</strong>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <label>Apellido materno:</label>
                            <strong> '.$item->apm.'</strong>
                          </div>
                        </div>';
            }
        }
      }  
      $tipo_text.='';
    }
    */
    /*
    $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_benef);
    $data['get_pf']=$get_pf;
    foreach ($get_pf as $item) {
      $id_df=$item->id_beneficiario_fisica;
    }
    */
    $data['titulo_t']=$titilo_t;
    $data['text']=$tipo_text;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/docs_beneficiario',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/dosc_beneficiciario_js');
  }
  function cargafiles_beneficiario(){
    $id_beneficiario = $this->input->post('id_beneficiario');
    $id_Beneficiario_transaccion = $this->input->post('id_Beneficiario_transaccion');
    $tipo_persona = $this->input->post('tipo_persona');
    $variable = $this->input->post('variable');//Es para saber en que campo se va a guardar
    $variablet = $this->input->post('variablet');
    $id_beneficiario = $this->input->post('id_beneficiario');
    $id = $this->input->post('id');// es el id registro de la tabla para verificar se existe 
    //es para saber en que carpeta se va a guardar si es fisica o moral
    if($tipo_persona==1){
      $folder = "benefisiario_fisica";
    }else{
      $folder = "";
    }
    // If es para saber en que variable se va a guardar el archivo
    $tipo_varible='';
    $tipo_variblet='';
    if($variable==1){ $tipo_varible='formato_pb'; $tipo_variblet='formato_pbt'; }
    else if($variable==2){ $tipo_varible='formato_cc'; $tipo_variblet='formato_cct'; }
    else if($variable==3){ $tipo_varible='ine'; $tipo_variblet='inet'; }
    else if($variable==4){ $tipo_varible='cif'; $tipo_variblet='cift'; }
    else if($variable==5){ $tipo_varible='curp'; $tipo_variblet='curpt'; }
    else if($variable==6){ $tipo_varible='comprobante_dom'; $tipo_variblet='comprobante_domt'; }
    else if($variable==7){ $tipo_varible='formato_duen_bene'; $tipo_variblet='formato_duen_benet'; }
    else if($variable==8){ $tipo_varible='carta_poder'; $tipo_variblet='carta_podert'; }
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    //$archivador = $upload_folder . '/' . $nombre_archivo;
    $fecha=date('ymd-His');
    $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
    $nombre_archivo=str_replace('-', '_', $nombre_archivo);
    $nombre_archivo=$this->eliminar_acentos($nombre_archivo);
    $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    $id_r=0;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
        $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{

        $array = array('id_beneficiario'=>$id_beneficiario,'id_Beneficiario_transaccion'=>$id_Beneficiario_transaccion,'id_clientec'=>$this->idcliente,$tipo_varible=>$newfile,$tipo_variblet=>$variablet,'fecha'=>$this->fechaactual);
        if($id==0){ //insert
          $id_r=$this->ModeloCatalogos->tabla_inserta('docs_beneficiario',$array);
        }
        if($id>0){ //update
          $id = $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_beneficiario');
          $id_r=$id;
        }
        $return = Array('id'=>$id,'ok'=>TRUE);
    }
    echo $id_r;
  }


  public function beneficiario_checked_f(){
    $id_beneficiario = $this->input->post('id_beneficiario');
    $id_Beneficiario_transaccion = $this->input->post('id_Beneficiario_transaccion');
    $tipo_persona = $this->input->post('tipo_persona');
    $variable = $this->input->post('variable');//Es para saber en que campo se va a guardar
    $variablet = $this->input->post('variablet');
    $id_beneficiario = $this->input->post('id_beneficiario');
    $id = $this->input->post('id');// es el id registro de la tabla para verificar se existe 

    // If es para saber en que variable se va a guardar el archivo
    $tipo_varible='';
    $tipo_variblet='';
    if($variable==1){ $tipo_variblet='formato_pbt'; }
    else if($variable==2){ $tipo_variblet='formato_cct'; }
    else if($variable==3){ $tipo_variblet='inet'; }
    else if($variable==4){ $tipo_variblet='cift'; }
    else if($variable==5){ $tipo_variblet='curpt'; }
    else if($variable==6){ $tipo_variblet='comprobante_domt'; }
    else if($variable==7){ $tipo_variblet='formato_duen_benet'; }
    else if($variable==8){ $tipo_variblet='carta_podert'; }
    $id_r=0;
      $array = array('id_beneficiario'=>$id_beneficiario,'id_Beneficiario_transaccion'=>$id_Beneficiario_transaccion,'id_clientec'=>$this->idcliente,$tipo_variblet=>$variablet,'fecha'=>$this->fechaactual);
      if($id==0){ //insert
        $id_r=$this->ModeloCatalogos->tabla_inserta('docs_beneficiario',$array);
      }
      if($id>0){ //update
        $id = $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_beneficiario');
        $id_r=$id;
      }
    echo $id_r;
  }


/* End of file Clientes.php */
/* Location: ./application/controllers/Clientes.php 
http://localhost/yimexico/index.php/Clientes_cliente/docs_beneficiario/51/86/11/19/8
Ppersona moral 
Inciso A
http://localhost/yimexico/index.php/Clientes_cliente/docs_beneficiario/31/90/11/19/8
Inciso B
http://localhost/yimexico/index.php/Clientes_cliente/docs_beneficiario/35/94/11/19/8
*/

public function cargafiles_beneficiario_e($id,$idb,$idc,$img,$tipop){
      $iddoc = $this->input->post('iddoc');

      if($img=='formato_pb') $folder = "formato_perb";
      if($img=='ine') $folder = "ine";
      if($img=='cif') $folder = "cif";
      if($img=='curp') $folder = "curp";
      if($img=='comprobante_dom') $folder = "compro_dom";
     
      $upload_folder ='uploads_benes/'.$folder;
      $nombre_archivo = $_FILES['foto']['name'];
      $tipo_archivo = $_FILES['foto']['type'];
      $tamano_archivo = $_FILES['foto']['size'];
      $tmp_archivo = $_FILES['foto']['tmp_name'];
      //$archivador = $upload_folder . '/' . $nombre_archivo;
      $fecha=date('ymd-His');
      $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
      $archivador = $upload_folder . '/'.$newfile;
      if (!move_uploaded_file($tmp_archivo, $archivador)) {
          $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
      }else{
          $array = array('id_beneficiario'=>$idb,'id_clientec'=>$idc,'tipo_bene'=>$tipop,$img=>$newfile,'fecha'=>$this->fechaactual);
          if($id==0){ //insert
            $id=$this->ModeloCatalogos->tabla_inserta('docs_beneficiario',$array);
            log_message('error', 'inserto y regresa el id: '.$id);
          }
          if($id>0){ //update
            $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_beneficiario');
            log_message('error', 'edito y regresa el id: '.$id);
          }
          $return = Array('id'=>$id,'ok'=>TRUE);
      }
      echo json_encode($return);
    }

    public function file_delete_c($id,$col){  
      $array = array($col => '');
      $this->ModeloCatalogos->updateCatalogo($array,'id',$id,'docs_beneficiario');
    }
    public function verificaFoto_e(){
      $id = $this->input->post('id');
      $idben = $this->input->post('idben');
      $tipo = $this->input->post('tipo');
      $col = $this->input->post('col');
      if($id>0)
        $data=$this->General_model->get_docsBenes('docs_beneficiario',array("id"=>$id),$col);
      else
        $data=$this->General_model->get_docsBenes('docs_beneficiario',array("id_beneficiario"=>$idben,"tipo_bene"=>$tipo),$col);

      echo json_encode($data);
  }

  public function grado_riesgo($idcc,$idp,$tipoc,$idopera,$idact=0){
    if(!isset($idact) || $idact==0){
      $get_act=$this->ModeloCatalogos->getselectwherestatus("idactividad","cliente_actividad",array('idcliente'=>$this->idcliente));
      foreach($get_act as $a){
        $idact = $a->idactividad;  
      }
      redirect('Clientes_cliente/grado_riesgo/'.$idcc.'/'.$idp.'/'.$tipoc.'/'.$idopera.'/'.$idact); 
    }
        
    if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
    $data['tipoc']=$tipoc;
    //$data['info']=$this->General_model->get_tableRow($tabla,array($idt=>$idcc));
    $info=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 
    $data['info']=$info;
    if($tipoc<6) {
      $estadoinfo=$info->estado_d;
    }else{
      $estadoinfo='';
    }
    $giro=0;
    $data["nameact"]="";
    if($tipoc==1){
      $giro=$info->activida_o_giro;
      if($giro!=""){
        $val_act=$this->General_model->get_tableRow("actividad_econominica_fisica",array('clave'=>$giro));
        $data["nameact"]=$val_act->clave."-".$val_act->acitividad; 
      }else{
        $data["nameact"]="";
      }
    }
    if($tipoc==2){
      $giro=$info->actividad_o_giro;
      if($giro!=""){
        $val_act=$this->General_model->get_tableRow("actividad_econominica_fisica",array('clave'=>$giro));
        $data["nameact"]=$val_act->clave."-".$val_act->acitividad; 
      }else{
        $data["nameact"]="";
      }
    }
    if(/*$tipoc==2 ||*/ $tipoc==3 || $tipoc==4 /* || $tipoc==5*/){
      $giro=$info->giro_mercantil;
      if($giro!=""){
        $val_act=$this->General_model->get_tableRow("actividad_econominica_morales",array('clave'=>$giro));
        $data["nameact"]=$val_act->clave."-".$val_act->giro_mercantil; 
      }else{
        $data["nameact"]="";
      }
    }
    if($tipoc!=5 && $tipoc!=6 && $giro!=""){
      $data["grado_riesgo_giro"]=$val_act->riesgo;
    }
    if($tipoc==5 && $giro!="" || $tipoc==6 && $giro!=""){
      $data["grado_riesgo_giro"]=1;
    }
    if($giro==""){
      $data["grado_riesgo_giro"]=1;
    }

    $get_u=$this->General_model->get_tableRow("operaciones",array('id'=>$idopera));
    $data["id_union"]=$get_u->id_union;

    if($tipoc==1 || $tipoc==2){
      $data["pais_nac"]=$info->pais_nacionalidad;
    }
    else if($tipoc==3 || $tipoc==4 || $tipoc==5){
      $data["pais_nac"]=$info->nacionalidad;
    }
    else if($tipoc==6){
      $data["pais_nac"]="MX";
    }
    
    //var_dump($estadoinfo);die;
    $data['estadoinfo']=$estadoinfo;
    $data['tipo']=$tipoc;
    $data['idperfilamiento']=$idp;
    $data['idclientec']=$idcc;
    $data['idopera']=$idopera;
    $data['idact']=$idact;
    $data['gr']=$this->General_model->get_tableRow("grado_riesgo_actividad",array('id_perfilamiento'=>$idp,"id_operacion"=>$idopera));
    //$data['edos']="";
    $data['get_tipo_cliente']=$this->ModeloCatalogos->getselectwhere('tipo_cliente','activo',1);  
    if($tipoc==1 || $tipoc==2)
      $data['actividad']=$this->ModeloCatalogos->getData('actividad_econominica_fisica');
    else
     $data['actividad']=$this->ModeloCatalogos->getData('actividad_econominica_morales');

    //log_message('error', ' estado_d: '.$data['info']->estado_d);
    //log_message('error', ' tipoc: '.$tipoc);
    //echo "tipoc: ".$tipoc;
    $data['estado']="";
    if($tipoc!=6){ //ecoi no tiene columna de estado
      //log_message('error', ' estado_d: '.$data['info']->estado_d);
      if(isset($data['info']->pais_d) && $data['info']->pais_d=="MX"){
        $data['edos']=$this->General_model->get_tableRow("estado",array('id'=>$data['info']->estado_d));
      }else{
        $data['estado'] = $data['info']->estado_d;
        $data['pais_d'] = "MX";
      }
    }
    if($idact==11){
      //obtener los ultimos mov (12 meses)
      $get_anexo8=$this->ModeloCatalogos->getselectwherestatus("*","anexo_8",array("fecha_opera >= date_sub(curdate(), interval 12 month)","id_perfilamiento"=>$idp,'fecha_opera !='=>"0000-00-00")); 

    }
    $data['get_pais'] = $this->ModeloCatalogos->getData('pais');
    $data['resultestado']=$this->General_model->GetAll('estado');
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');

    //log_message('error', ' idact: '.$idact);

    $data["anexo_name"]="";
    /*if($idact==11)
      $data["anexo_name"]=" Anexo 8 - Vehiculos";
    if($idact==12)
      $data["anexo_name"]=" Anexo 9 - Blindaje";
    if($idact==15)
      $data["anexo_name"]=" Anexo 12 A - Fedatarios públicos";
    if($idact==16)
      $data["anexo_name"]=" Anexo 12 B - Servidores públicos";*/

    if($idact==1){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA1',$data);
    }
    if($idact==2 || $idact==3 || $idact==4){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA2',$data);
    }
    if($idact==5){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA3',$data);
    }
    if($idact==6){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA4',$data);
    }
    if($idact==7 || $idact==8){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA5',$data);
    }
    if($idact==9){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA6',$data);
    }
    if($idact==10){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA7',$data);
    }
    if($idact==11 || $idact==12 || $idact==15 || $idact==16){
      $this->load->view('catalogos/cliente_cliente/grado_riesgo',$data);
    }
    if($idact==13){ //traslado de valores
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA10',$data);
    }
    if($idact==14){ //servicios profesionales
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA11',$data);
    }
    if($idact==17){ //donativos
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA13',$data);
    }
    if($idact==19){ //arrendamiento
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA15',$data);
    }
    if($idact==20){ //activos virtuales
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA16',$data);
    }

    $this->load->view('templates/footer');

    if($idact==1){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA1_js');
    }
    if($idact==2 || $idact==3 || $idact==4){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA2_js');
    }
    if($idact==5){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA3_js');
    }
    if($idact==6){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA4_js');
    }
    if($idact==7 || $idact==8){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA5_js');
    }
    if($idact==9){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA6_js');
    }
    if($idact==10){
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA7_js');
    }
    if($idact==11 || $idact==12 || $idact==15 || $idact==16){
      $this->load->view('catalogos/cliente_cliente/grado_riesgo_js');
    }
    if($idact==13){ //traslado de valores
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA10_js');
    }
    if($idact==14){ //servicios profesionales
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA11_js');
    }
    if($idact==17){ //donativos
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA13_js');
    }
    if($idact==19){ //arrendamiento
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA15_js');
    }
    if($idact==20){ //activos virtuales
      $this->load->view('catalogos/cliente_cliente/grado_riesgoA16_js');
    }
  }

  public function cuestionarioAmpliadoMoral($idcc,$idp,$tipoc,$idopera,$idgr){
    if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

    $data['info']=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 
    $data['tipo']=$tipoc;
    $data['idperfilamiento']=$idp;
    $data['idclientec']=$idcc;
    $data['idopera']=$idopera;
    $data['idcgr']=$idgr;

    $data['actividad']=$this->ModeloCatalogos->getData('actividad_econominica_morales');

    $data['get_pais'] = $this->ModeloCatalogos->getData('pais');
    $data['get_edos'] = $this->ModeloCatalogos->getData('estado');

    $ca = $this->General_model->get_tableRow('cuestionario_ampliado_moral',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));

    if(isset($ca)){
      $data['ca'] = $ca;
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/cuestionario_ampliado_moral',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/cuestionario_moral_js');
  }

  function add_cuestionario_moral(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('cuestionario_ampliado_moral',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'cuestionario_ampliado_moral');
    }
    echo $id;
  }

  public function cuestionarioAmpliadoFide($idcc,$idp,$tipoc,$idopera,$idgr){
    if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

    $data['info']=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 
    $data['tipo']=$tipoc;
    $data['idperfilamiento']=$idp;
    $data['idclientec']=$idcc;
    $data['idopera']=$idopera;
    $data['idcgr']=$idgr;
    $ca = $this->General_model->get_tableRow('cuestionario_ampliado_fide',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    if(isset($ca)){
      $data['ca'] = $ca;
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/cuestionario_ampliado_fide',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/cuestionario_fide_js');
  }

  function add_cuestionario_fide(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('cuestionario_ampliado_fide',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'cuestionario_ampliado_fide');
    }
    echo $id;
  }
  
  public function cuestionarioAmpliadoEmbajada($idcc,$idp,$tipoc,$idopera,$idgr){
    if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

    $data['info']=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 
    $data['tipo']=$tipoc;
    $data['idperfilamiento']=$idp;
    $data['idclientec']=$idcc;
    $data['idopera']=$idopera;
    $data['idcgr']=$idgr;
    $ca = $this->General_model->get_tableRow('cuestionario_ampliado_embajada',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    if(isset($ca)){
      $data['ca'] = $ca;
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/cuestionario_ampliado_embajada',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/cuestionario_embajada_js');
  }

  function add_cuestionario_embajada(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('cuestionario_ampliado_embajada',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'cuestionario_ampliado_embajada');
    }
    echo $id;
  }

  public function cuestionarioAmpliado($idcc,$idp,$tipoc,$idopera,$idgr,$tcc,$clave=0){
    if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
   
    $data['info']=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 
    $data['tipo']=$tipoc;
    $data['idperfilamiento']=$idp;
    $data['idclientec']=$idcc;
    $data['idopera']=$idopera;
    $data['idcgr']=$idgr;
    $data["tcc"]=$tcc;

    if($tcc==1)
      $data["get_actividad"]=$this->ModeloCatalogos->getData('actividad_econominica_fisica');
    if($tcc==2)
      $data["get_actividad"]=$this->ModeloCatalogos->getData('actividad_econominica_morales');

    $data["act_giro"]=$clave;

    $data['id']=0;
    $data['descrip_nego']='';
    $data['propietario_empresa']='';
    $data['trabajador_autonomo']='';
    $data['es_empleado']='';
    $data['otras_actividades_generadoras_recurso']='';
    $data['cuenta_alguna_otra_fuente_ingresos']='';
    $data['fuente_ingresos_cual']='';
    $data['cual_monto_mensual_recibe_ingresos']='';
    $data['recibe_ingresos_extranjero']='';
    $data['ingresos_extranjero_de_que_pais']='';
    $data['mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta']='';
    $data['nacional_extranjera_cual']='';
    $data['nacional_extranjera_ingreso_anual']='';
    $data['efectivo']='';
    $data['transfer_otro']='';
    $data['transfer_inter']='';
    $data['tarjeta']='';
    $data['cheques']='';
    $data['usted_casado_vive_concubinato']='';
    $data['cual_nombre_esposa']='';
    $data['cual_ocupacion_espos']='';
    $data['edad']='';
    $data['nacionalidad']='';
    $data['tiene_dependientes_economicos']='';
    $data['cuantos']='';
    $data['parentesco']='';
    $data['nombre_ompleto']='';
    $data['genero']='';
    $data['fecha_nacimiento']='';
    $data['ocupacion']='';
    $data['apoyo_conyugal_parental_pareja']='';
    $data['apoyo_gubernamental']='';
    $data['herencia_donacion']='';
    $data['fondo_fiduciario']='';
    $data['prestamo_subvencion']='';
    $data['inversiones']='';
    $result = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    foreach ($result as $x) {
      $data['id']=$x->id;
      $data['descrip_nego']=$x->descrip_nego;
      $data['propietario_empresa']=$x->propietario_empresa;
      $data['trabajador_autonomo']=$x->trabajador_autonomo;
      $data['es_empleado']=$x->es_empleado;
      $data['otras_actividades_generadoras_recurso']=$x->otras_actividades_generadoras_recurso;
      $data['cuenta_alguna_otra_fuente_ingresos']=$x->cuenta_alguna_otra_fuente_ingresos;
      $data['fuente_ingresos_cual']=$x->fuente_ingresos_cual;
      $data['cual_monto_mensual_recibe_ingresos']=$x->cual_monto_mensual_recibe_ingresos;
      $data['recibe_ingresos_extranjero']=$x->recibe_ingresos_extranjero;
      $data['ingresos_extranjero_de_que_pais']=$x->ingresos_extranjero_de_que_pais;
      $data['mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta']=$x->mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta;
      $data['nacional_extranjera_cual']=$x->nacional_extranjera_cual;
      $data['nacional_extranjera_ingreso_anual']=$x->nacional_extranjera_ingreso_anual;
      $data['efectivo']=$x->efectivo;
      $data['transfer_otro']=$x->transfer_otro;
      $data['transfer_inter']=$x->transfer_inter;
      $data['tarjeta']=$x->tarjeta;
      $data['cheques']=$x->cheques;
      $data['usted_casado_vive_concubinato']=$x->usted_casado_vive_concubinato;
      $data['cual_nombre_esposa']=$x->cual_nombre_esposa;
      $data['cual_ocupacion_espos']=$x->cual_ocupacion_espos;
      $data['edad']=$x->edad;
      $data['nacionalidad']=$x->nacionalidad;
      $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad);
      foreach ($pais_4_get as $x1) {
        $data['clave4']=$x1->clave;
        $data['pais4']=$x1->pais;
      }
      $data['tiene_dependientes_economicos']=$x->tiene_dependientes_economicos;
      $data['cuantos']=$x->cuantos;
      /*$data['parentesco']=$x->parentesco;
      $data['nombre_ompleto']=$x->nombre_ompleto;
      $data['genero']=$x->genero;
      $data['fecha_nacimiento']=$x->fecha_nacimiento;
      $data['ocupacion']=$x->ocupacion;*/
      $data['apoyo_conyugal_parental_pareja']=$x->apoyo_conyugal_parental_pareja;
      $data['apoyo_gubernamental']=$x->apoyo_gubernamental;
      $data['herencia_donacion']=$x->herencia_donacion;
      $data['fondo_fiduciario']=$x->fondo_fiduciario;
      $data['prestamo_subvencion']=$x->prestamo_subvencion;
      $data['inversiones']=$x->inversiones;

      if($x->id>0 && $x->cuantos!="" && $x->cuantos>0){
        $data["get_ode"]=$this->ModeloCatalogos->getselectwherestatus('*','cuest_amp_otros_dependiente',array('id_cuest_amp'=>$x->id,"estatus"=>1));
      }
    }
    $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$idp));
    foreach ($get_pp as $g) {
      //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
      $tipoccon = $g->idtipo_cliente;
      //echo "<br>tipoccon: ".$tipoccon;
      if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
      if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
      if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
      if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
      if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
      if($tipoccon==6) $tabla = "tipo_cliente_f";
      $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
      foreach ($get_per as $g2) {
        if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
        if($tipoccon==3) $nombre = $g2->razon_social;
        if($tipoccon==4) $nombre = $g2->nombre_persona;
        if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
      }
    }
    $data["nombre"]=$nombre;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/cuestionario_ampliado2',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/cuestionario_ampliado2js');
  }

  public function eliminarOtroDepend(){
    $id = $this->input->post("id");
    $this->ModeloCatalogos->updateCatalogo(array("estatus"=>0),'id',$id,'cuest_amp_otros_dependiente');
  }

  public function guardar_cuestionario(){
    $data = $this->input->post();
    if(isset($data['id']) && $data['id']>0){
      $id=$data['id'];
      $this->ModeloCatalogos->updateCatalogo($data,'id',$data['id'],'cuestionario_ampliado');
    }
    else{
      $id=$this->ModeloCatalogos->tabla_inserta('cuestionario_ampliado',$data);
    }
    echo $id;
  }

  public function guardar_grado(){
    $data = $this->input->post();
    //log_message('error', ' id: '.$data['id']);
    //log_message('error', ' grado final: '.$data['grado']);
    if(isset($data['id']) && $data['id']>0){
      $id=$data['id'];
      $this->ModeloCatalogos->updateCatalogo($data,'id',$data['id'],'grado_riesgo_actividad');
    }
    else{
      $id=$this->ModeloCatalogos->tabla_inserta('grado_riesgo_actividad',$data);
    }
    echo $id;
  }

  public function submit_extras(){
    $data=$this->input->post();
    //$this->db->trans_start();
    //log_message('error', ' id: '.$data['id']);
    /*$get = $this->General_model->GetAllWhere("docs_extra_clientesc",array("id"=>$data['id']));
    $i=0;
    foreach ($get as $val) {
      $i++;
    }
    log_message('error', ' i: '.$i);*/
    if($data["id"]==0){ //insert
        $id=$this->ModeloCatalogos->tabla_inserta('docs_extra_clientesc',$data);
    }
    else{ //update
        $id=$data["id"]; unset($data["id"]);
        $this->ModeloCatalogos->updateCatalogo2($data,array('id_perfilamiento'=>$data["id_perfilamiento"]),'docs_extra_clientesc');
        //$this->ModeloCatalogos->update_docs_extrac($id,$data['descrip']);
    }
    /*$this->db->trans_complete();
    $this->db->trans_status();*/
    echo $id;
  }
  function formatoPesonasBloqueadas($idc,$idtipo,$idp,$idopera){
    $data['tipo_cliente'] = $idtipo;
    $data['id_clientec'] = $idc;
    if($idtipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($idtipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($idtipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($idtipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($idtipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($idtipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

    $data['info']=$this->General_model->get_tableRow($tabla,array($idt=>$idc)); 
    $data['docs']=$this->General_model->get_tableRow('docs_cliente',array('id_clientec'=>$idc,'id_perfilamiento'=>$idp)); 
    $data['docs_ext']=$this->General_model->get_tableRow('docs_extra_clientesc',array('id_clientec'=>$idc,'id_perfilamiento'=>$idp)); 
    
    $data['fechaactual'] = date("d-m-Y",strtotime($this->fechaactual));
    $data['horahoy'] =$this->horahoy;
    $data['idopera'] =$idopera;
    $data['idc'] =$idc;
    $data['idp'] =$idp;
    $this->load->view('templates/header');
    $this->load->view('catalogos/cliente_cliente/formatopersonasbloqueadas',$data);
    //$this->load->view('catalogos/cliente_cliente/dosc_beneficiciario_js');;;
  }

  public function verificaListado(){
    $id = $this->input->post('id_operacion');
    $idp = $this->input->post('id_perfilamiento');
    $idc = $this->input->post('id_clientec');
    $idb = $this->input->post('id_bene');
    $result=array(); $html=""; $resultado="";
    if(isset($idb)){
      //log_message('error', ' idb: '.$idb);
      $tipob = $this->input->post('tipo_bene');
      $data=$this->ModeloCliente->detalles_busquedaPBDB($id,$idb,$tipob);
    }else{
      //log_message('error', ' idopera: '.$id);
      $data=$this->ModeloCliente->detalles_busquedaPB($id,$idp,$idc);
    }

    foreach ($data as $key) {
      $id_cons=$key->id;
      $result[] = $key->resultado;
      $resultado = $key->resultado;
    }
    /*if($resultado==""){
      $html="<p>Cliente no se encuentra en el listado de busqueda</p>";
    }   
    else{
      $result2=json_encode($result);
      $array = json_decode($result2, true);
      foreach ($array as $i) {
        if($i['Message']!=undefined){
          $html = "<p>".$i['Message']."</p>";
        }
        else if($i['Status']=="OK"){
          $html = "<p>".$i['Message']."</p>";
        }
        else if(i.Status=="ERROR"){
          $html = "<p>".$i['Message']."</p>";
        }
        else if($i['Estado']=="ACTIVO" || $i['Estado']=="INACTIVO"){
          $fina_cargo="";
          if($i['Finalizacion_cargo']!=undefined){
            $fina_cargo=$i['Finalizacion_cargo'];  
          }
          $html="<p><strong>Cliente encontrado en listas de busqueda </strong></p>
              <p> <strong>Denominacion: </strong>".$i['Denominacion']."</p>
              <p> <strong>Identificacion: </strong>".$i['Identificacion']." </p>
              <p> <strong>Id_Tributaria: </strong>".$i['Id_Tributaria']." </p>
              <p> <strong>Otra_Identificacion: </strong>".$i['Otra_Identificacion']." </p>
              <p> <strong>Cargo: </strong>".$i['Cargo']." </p>
              <p> <strong>Lugar_Trabajo: </strong>".$i['Lugar_Trabajo']." </p>
              <p> <strong>Direccion: </strong>".$i['Direccion']." </p>
              <p> <strong>Enlace: </strong>".$i['Enlace']." </p>
              <p> <strong>Tipo: </strong>".$i['Tipo']." </p>
              <p> <strong>Estado: </strong>".$i['Estado']." </p>
              <p> <strong>Finalizacion_cargo: </strong>".$f['na_cargo']." </p>
              <p> <strong>Lista: </strong>".$i['Lista']." </p>
              <p> <strong>Pais_Lista: </strong>".$i['Pais_Lista']." </p>
              <p> <strong>Cod_Individuo: </strong>".$i['Cod_Individuo']." </p>
              <p> <strong>Exactitud_Denominacion: </strong>".$i['Exactitud_Denominacion']." </p>
              <p> <strong>Exactitud_Identificacion: </strong>".$i['Exactitud_Identificacion']." </p>";
          
        } 
      }
    }
    log_message('error', ' html: '.$html);*/
    if($id_cons>0){
      //echo json_encode(array("data"=>$data,"resultado"=>$result));
      echo json_encode($data);
    }
    else{
      echo "0";
    }
  }

  public function archivos_cliente($idc,$idtipo,$idp){
    $data['tipo_cliente'] = $idtipo;
    $data['id_clientec'] = $idc;
    if($idtipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($idtipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($idtipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($idtipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($idtipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($idtipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

    $data['info']=$this->General_model->get_tableRow($tabla,array($idt=>$idc)); 
    /*$data['docs']=$this->General_model->get_tableRow('docs_cliente',array('id_clientec'=>$idc,'id_perfilamiento'=>$idp)); 
    $data['docs_ext']=$this->General_model->get_tableRow('docs_extra_clientesc',array('id_clientec'=>$idc,'id_perfilamiento'=>$idp)); */

    $data['bitacora']=$this->ModeloCatalogos->getselectwherestatus('*','bitacora_docs_cliente',array('id_clientec'=>$idc,'id_perfilamiento'=>$idp)); 

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('templates/footer2_librerias');
    $this->load->view('catalogos/cliente_cliente/archivos',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/archivos_js');
  }

  public function union($id=0,$desdeopera=0){
    $data['perfilid']=$this->perfilid; 
    $data['clis'] = $this->ModeloCliente->get_clientes_all($this->idcliente);
    if($id>0){
      $data['band_uc']=1;
      $data['uc'] = $this->ModeloCliente->getUnionCliente($id); 
      $data["id_union"]=$id;
    }else{
      $data['band_uc']=0;
      $data["id_union"]=0;
    }
    $data["desdeopera"]=$desdeopera;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/alta_uniones',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/alta_uniones_js');
  }

  public function add_cli_union(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    $cont=0;
    for ($i=0;$i<count($DATA);$i++) { 
      $id = $DATA[$i]->id;
      $datau['id'] = $DATA[$i]->id;
      $id_union_cli = $DATA[$i]->id_union_cli;
      //$datau['id_union_cli'] = $DATA[$i]->id_union_cli;
      $datau['fecha_reg'] = date("Y-m-d H:i:s");
      $ce=explode("-",$DATA[$i]->ncli);

      $data['id_perfilamiento']= $ce[0];
      $data['id_clientec'] = $ce[1];
      $data['id_cliente']= $this->idcliente;
      $data['fecha_reg'] = date("Y-m-d H:i:s");

      if($id==0) {
        $cont++;
        if($cont==1){
          $id_insert=$this->ModeloCatalogos->tabla_inserta('uniones',$datau);
        }
        $data['id_union']=$id_insert;
        $this->ModeloCatalogos->tabla_inserta('uniones_clientes',$data);
      }else{
        $iduc=$DATA[$i]->id_union_cli;
        //unset($data['id_union_cli']);
        if($id_union_cli==0){
          $data['id_union'] = $id;
          $this->ModeloCatalogos->tabla_inserta('uniones_clientes',$data);
        }else{
          unset($data['id_union_cli']);
          $this->ModeloCatalogos->updateCatalogo($data,'id',$iduc,'uniones_clientes');
        }
        
      }            
    }
  }

  public function deleteCliUnion($id){  
    $this->ModeloCatalogos->updateCatalogo(array("activo"=>0),'id',$id,'uniones_clientes');
  }

  public function cuestionario_pep($idcc,$idp,$tipoc,$idopera,$idgr,$tcc,$clave){
    $get_gr=$this->General_model->get_tableRowC('*','grado_riesgo_actividad',array('id_operacion'=>$idopera));
    if($tcc==1 || $tcc==2)
      $data["get_actividad"]=$this->ModeloCatalogos->getData('actividad_econominica_fisica');
    //if($tcc==2)
    else
      $data["get_actividad"]=$this->ModeloCatalogos->getData('actividad_econominica_morales');

    $data["act_giro"]=$clave;
    $data["tcc"]=$tcc;

    $data['idopera']=$idopera;
    $data['id_actividad']=$get_gr->id_actividad;
    $data['idp']=$idp;
    
    $data['id']=0;
    $data['actividad_conforme_alta_fiscal']='';
    $data['indicar_ultimo_anio_pago_figura_publica']='';
    $data['dependencia_entidad_laboral']='';
    $data['ingreso_proviene_fuente_gobierno']='';
    $data['fuente_ingreso_gobierno']='';
    $data['fuente_ingreso']='';
    $data['cual_fuente_ingreso']='';
    $data['ingreso_extranjero']='';
    $data['que_pais']='';
    $data['ingresos_brutos_mensuales_totales']='';
    $data['tiene_capacidad_acceso_mover_recursos_gorbierno']='';
    $data['actividad_econominca_ocupacion_esposo']='';
    $data['actividad_economica_ocupacion_hijos']='';
    $data['explicar_razon_cual_operacion_trasanccion']='';

    $result = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado_pep',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    foreach ($result as $x) {
      $data['id']=$x->id;
      $data['actividad_conforme_alta_fiscal']=$x->actividad_conforme_alta_fiscal;
      $data['indicar_ultimo_anio_pago_figura_publica']=$x->indicar_ultimo_anio_pago_figura_publica;
      $data['dependencia_entidad_laboral']=$x->dependencia_entidad_laboral;
      $data['ingreso_proviene_fuente_gobierno']=$x->ingreso_proviene_fuente_gobierno;
      $data['fuente_ingreso_gobierno']=$x->fuente_ingreso_gobierno;
      $data['fuente_ingreso']=$x->fuente_ingreso;
      $data['cual_fuente_ingreso']=$x->cual_fuente_ingreso;
      $data['ingreso_extranjero']=$x->ingreso_extranjero;
      $data['que_pais']=$x->que_pais;
      $data['ingresos_brutos_mensuales_totales']=$x->ingresos_brutos_mensuales_totales;
      $data['tiene_capacidad_acceso_mover_recursos_gorbierno']=$x->tiene_capacidad_acceso_mover_recursos_gorbierno;
      $data['actividad_econominca_ocupacion_esposo']=$x->actividad_econominca_ocupacion_esposo;
      $data['actividad_economica_ocupacion_hijos']=$x->actividad_economica_ocupacion_hijos;
      $data['explicar_razon_cual_operacion_trasanccion']=$x->explicar_razon_cual_operacion_trasanccion;
    }
    $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$idp));
    foreach ($get_pp as $g) {
      //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
      $tipoccon = $g->idtipo_cliente;
      //echo "<br>tipoccon: ".$tipoccon;
      if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
      if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
      if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
      if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
      if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
      if($tipoccon==6) $tabla = "tipo_cliente_f";
      $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
      foreach ($get_per as $g2) {
        if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
        if($tipoccon==3) $nombre = $g2->razon_social;
        if($tipoccon==4) $nombre = $g2->nombre_persona;
        if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
      }
    }
    $data["nombre"]=$nombre;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/cuestionario_pep',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/cuestionario_pepjs');
  }
  
  function add_cuestionario_pep(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('cuestionario_ampliado_pep',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'cuestionario_ampliado_pep');
    }
  }

  public function diagnostico_alertas($idopera,$idp){
    $data['idopera']=$idopera;
    $get_gr=$this->General_model->get_tableRowC('*','grado_riesgo_actividad',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    $data['id_actividad']=$get_gr->id_actividad;
    $id_tipoc=$get_gr->idtipo_cliente;
    $clasifica=$get_gr->clasificacion;
    $data["ver_diag_pep"]=0;
    //log_message('error', 'id_tipoc : '.$id_tipoc);
    //log_message('error', 'clasifica : '.$clasifica);
    if($id_tipoc==1 && $clasifica>=1 && $clasifica<=8 && $data["id_actividad"]==15 || $id_tipoc==1 && $clasifica>=1 && $clasifica<=8 && $data["id_actividad"]==16 || $id_tipoc==5 && $data["id_actividad"]==15 || $id_tipoc==5 && $data["id_actividad"]==16){
      $data["ver_diag_pep"]=1;
    }
    //$data['id_actividad']=15;

    $data["idp"]=$idp;
    $data['id']=0;
    $data['pregunta_pep_1']='';
    $data['pregunta_pep_2']='';
    $data['pregunta_pep_3']='';
    $data['pregunta_pep_4']='';
    $data['pregunta_pep_5']='';
    $data['pregunta_pep_6']='';
    $data['pregunta_pep_7']='';
    $data['pregunta_pep_8']='';
    $data['pregunta_pep_9']='';
    $data['pregunta_pep_10']='';
    $data['pregunta_pep_11']='';
    $data['pregunta_pep_12']='';
    $data['pregunta_pep_13']='';
    $data['pregunta_pep_14']='';
    $data['pregunta_pep_15']='';
    $data['pregunta_pep_16']='';
    $data['pregunta_pep_17']='';
    $data['pregunta_pep_18']='';
    $data['pregunta_pep_19']='';
    $data['pregunta_pep_20']='';
    $data['pregunta_pep_21']='';
    $data['pregunta_pep_22']='';
    $data['pregunta_pep_23']='';
    $data['pregunta_pep_24']='';
    $data['pregunta_pep_25']='';
    $data['pregunta_1']='';
    $data['pregunta_2']='';
    $data['pregunta_3']='';
    $data['pregunta_4']='';
    $data['pregunta_5']='';
    $data['pregunta_6']='';
    $data['pregunta_7']='';
    $data['pregunta_8']='';
    $data['pregunta_9']='';
    $data['pregunta_10']='';
    $data['pregunta_11']='';
    $data['pregunta_12']='';
    $data['pregunta_13']='';
    $data['pregunta_14']='';
    $data['pregunta_15']='';
    $data['pregunta_16']='';
    $data['pregunta_17']='';
    $data['pregunta_18']='';
    $data['pregunta_19']='';
    $data['pregunta_20']='';
    $data['pregunta_21']='';
    $data['pregunta_22']='';
    $data['pregunta_23']='';
    $data['pregunta_24']='';
    $data['pregunta_25']='';
    $data['pregunta_26']='';
    $data['pregunta_27']='';
    $data['pregunta_28']='';
    $data['pregunta_29']='';
    $data['pregunta_30']='';
    $data['pregunta_31']='';
    $data['pregunta_32']='';
    $data['pregunta_33']='';
    $data['pregunta_34']='';
    $data['pregunta_34']='';
    $data['pregunta_35']='';
    $data['pregunta_36']='';
    $data['pregunta_37']='';
    $data['pregunta_38']='';
    $data['pregunta_39']='';
    $data['pregunta_40']='';
    $data['pregunta_41']='';
    $data['pregunta_42']='';
    $data['pregunta_43']='';
    $data['pregunta_44']='';
    $data['pregunta_45']='';
    $data['conclusiones']='';
    $data['conclusiones_dir']='';
    $result = $this->ModeloCatalogos->getselectwherestatus('*','diagnostico_alertas',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    foreach ($result as $x) {
      $data['id']=$x->id;
      $data['pregunta_pep_1']=$x->pregunta_pep_1;
      $data['pregunta_pep_2']=$x->pregunta_pep_2;
      $data['pregunta_pep_3']=$x->pregunta_pep_3;
      $data['pregunta_pep_4']=$x->pregunta_pep_4;
      $data['pregunta_pep_5']=$x->pregunta_pep_5;
      $data['pregunta_pep_6']=$x->pregunta_pep_6;
      $data['pregunta_pep_7']=$x->pregunta_pep_7;
      $data['pregunta_pep_8']=$x->pregunta_pep_8;
      $data['pregunta_pep_9']=$x->pregunta_pep_9;
      $data['pregunta_pep_10']=$x->pregunta_pep_10;
      $data['pregunta_pep_11']=$x->pregunta_pep_11;
      $data['pregunta_pep_12']=$x->pregunta_pep_12;
      $data['pregunta_pep_13']=$x->pregunta_pep_13;
      $data['pregunta_pep_14']=$x->pregunta_pep_14;
      $data['pregunta_pep_15']=$x->pregunta_pep_15;
      $data['pregunta_pep_16']=$x->pregunta_pep_16;
      $data['pregunta_pep_17']=$x->pregunta_pep_17;
      $data['pregunta_pep_18']=$x->pregunta_pep_18;
      $data['pregunta_pep_19']=$x->pregunta_pep_19;
      $data['pregunta_pep_20']=$x->pregunta_pep_20;
      $data['pregunta_pep_21']=$x->pregunta_pep_21;
      $data['pregunta_pep_22']=$x->pregunta_pep_22;
      $data['pregunta_pep_23']=$x->pregunta_pep_23;
      //$data['pregunta_pep_24']=$x->pregunta_pep_24;
      //$data['pregunta_pep_25']=$x->pregunta_pep_25;
      $data['pregunta_1']=$x->pregunta_1;
      $data['pregunta_2']=$x->pregunta_2;
      $data['pregunta_3']=$x->pregunta_3;
      $data['pregunta_4']=$x->pregunta_4;
      $data['pregunta_5']=$x->pregunta_5;
      $data['pregunta_6']=$x->pregunta_6;
      $data['pregunta_7']=$x->pregunta_7;
      $data['pregunta_8']=$x->pregunta_8;
      $data['pregunta_9']=$x->pregunta_9;
      $data['pregunta_10']=$x->pregunta_10;
      $data['pregunta_11']=$x->pregunta_11;
      $data['pregunta_12']=$x->pregunta_12;
      $data['pregunta_13']=$x->pregunta_13;
      $data['pregunta_14']=$x->pregunta_14;
      $data['pregunta_15']=$x->pregunta_15;
      $data['pregunta_16']=$x->pregunta_16;
      $data['pregunta_17']=$x->pregunta_17;
      $data['pregunta_18']=$x->pregunta_18;
      $data['pregunta_19']=$x->pregunta_19;
      $data['pregunta_20']=$x->pregunta_20;
      $data['pregunta_21']=$x->pregunta_21;
      $data['pregunta_22']=$x->pregunta_22;
      $data['pregunta_23']=$x->pregunta_23;
      $data['pregunta_24']=$x->pregunta_24;
      $data['pregunta_25']=$x->pregunta_25;
      $data['pregunta_26']=$x->pregunta_26;
      $data['pregunta_27']=$x->pregunta_27;
      $data['pregunta_28']=$x->pregunta_28;
      $data['pregunta_29']=$x->pregunta_29;
      $data['pregunta_30']=$x->pregunta_30;
      $data['pregunta_31']=$x->pregunta_31;
      $data['pregunta_32']=$x->pregunta_32;
      $data['pregunta_33']=$x->pregunta_33;
      $data['pregunta_34']=$x->pregunta_34;
      $data['pregunta_35']=$x->pregunta_35;
      $data['pregunta_36']=$x->pregunta_36;
      $data['pregunta_37']=$x->pregunta_37;
      $data['pregunta_38']=$x->pregunta_38;
      $data['pregunta_39']=$x->pregunta_39;
      $data['pregunta_40']=$x->pregunta_40;
      $data['pregunta_41']=$x->pregunta_41;
      $data['pregunta_42']=$x->pregunta_42;
      $data['pregunta_43']=$x->pregunta_43;
      $data['pregunta_44']=$x->pregunta_44;
      $data['pregunta_45']=$x->pregunta_45;
      $data['conclusiones']=$x->conclusiones;
      $data['conclusiones_dir']=$x->conclusiones_dir;
    }
    $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$idp));
    foreach ($get_pp as $g) {
      //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
      $tipoccon = $g->idtipo_cliente;
      //echo "<br>tipoccon: ".$tipoccon;
      if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
      if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
      if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
      if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
      if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
      if($tipoccon==6) $tabla = "tipo_cliente_f";
      $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
      foreach ($get_per as $g2) {
        if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
        if($tipoccon==3) $nombre = $g2->razon_social;
        if($tipoccon==4) $nombre = $g2->nombre_persona;
        if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
      }
    }
    $data["nombre"]=$nombre;
    $conf_alert=$this->General_model->get_tableRow("config_alerta",array("id_cliente"=>$this->idcliente));
    if(isset($conf_alert))
      $data["avanza"]=$conf_alert->avance;
    else
      $data["avanza"]=1;

    $get_co=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$idopera,"activo"=>1));
    foreach ($get_co as $key) {
      $cont_ge=0; $cont_gmalto=0;
      $get_gra=$this->ModeloTransaccion->getGradoCli($key->id_perfilamiento,$idopera);
      foreach ($get_gra as $key2) {
        $cont_ge++;
        if($key2->grado>0 && $key2->grado!=""){
          $data["grado"][$cont_ge]=$key2->grado;
          //log_message('error', 'cont_ge : '.$cont_ge);
        }
      }
    }

    //falta verificar si es cliente PEP desde el alta de clientes (perfilamiento)
    $data["pep_comp"]=0;
    //log_message('error', 'tipoccon : '.$tipoccon);
    if($data["id_actividad"]==15 || $data["id_actividad"]==16){
      if($tipoccon==1 || $tipoccon==2){
        $get_com_pep=$this->ModeloCatalogos->getselectwherestatus("*","complemento_persona_fisica",array("idperfilamiento"=>$idp));
        foreach ($get_com_pep as $k) {
          if($k->actualmente_usted_precandidato==1 || $k->familiar_primer_segundo_tercer_grado_socio_algun_precandidato==1 || $k->usted_socio_algun_precandidato==1 || $k->usted_asesor_agente_representante_algun_precandidato==1 || $k->usted_responsable_campanna_politica==1 || $k->servicio_contratar_producto_comprar_dirigido==1)
          $data["pep_comp"]=1;
        }
      }
      if($tipoccon==3){
        $get_com_pep=$this->ModeloCatalogos->getselectwherestatus("*","complemento_persona_moral",array("idperfilamiento"=>$idp));
        foreach ($get_com_pep as $k) {
          if($k->empresa_persona_moral_hay_algun_apoderado_legal==1 || $k->empresa_persona_moral_hay_algun_apoderado_legal_accionista==1 || $k->empresa_persona_moral_existe_algun_precandidato==1 || $k->empresa_persona_moral_existe_algun_responsable_campana_politica==1){
            $data["pep_comp"]=1;
          }
        }
      }
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/diagnostico_alertas',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/diagnostico_alertasjs');
  }

  function add_diagnostico_alertas(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $this->ModeloCatalogos->tabla_inserta('diagnostico_alertas',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'diagnostico_alertas');
    }
  }

  function add_cuestionario(){
    $data = $this->input->post();
    $id = $data['id'];
    if($id==0){
      $id=$this->ModeloCatalogos->tabla_inserta('cuestionario_ampliado',$data);
    }else{ 
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'cuestionario_ampliado');
    }
    echo $id;
  }

  public function insert_otros_dependientes(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) { 
      $id = $DATA[$i]->id;
      $id_cuest_amp=$DATA[$i]->id_cuest_amp;
      $data['parentesco']=$DATA[$i]->parentesco;
      $data['nombre']=$DATA[$i]->nombre;
      $data['genero']=$DATA[$i]->genero;
      $data['fecha_nacimiento']=$DATA[$i]->fecha_nacimiento;
      $data['ocupacion']=$DATA[$i]->ocupacion;
      $data['id_cuest_amp']=$id_cuest_amp;
      if($id==0){
        $this->ModeloCatalogos->tabla_inserta('cuest_amp_otros_dependiente',$data);
      }else{
        $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'cuest_amp_otros_dependiente');
      } 
    }
  }

  /*public function datatable_records_cli_union(){
    $clientesc = $this->ModeloCliente->get_clientes_union_all($this->idcliente);
    $json_data = array("data" => $clientesc);
    echo json_encode($json_data);
  }*/

  public function get_clientes_select(){
    $clientesc = $this->ModeloCliente->get_clientes_all($this->idcliente);
    $html="";
    $html.='<div class="">
      <div class="col-md-12 form-group">
        <label>Cliente:</label>
        <select class="form-control" id="id_cliente_select">';
        $html.='<option value="0">Todos</option>';
        foreach ($clientesc as $item) {
          if($item->idtipo_cliente==1){
            $idtipo_cliente=$item->idtipo_cliente_p_f_m;
          }else if($item->idtipo_cliente==2){
            $idtipo_cliente=$item->idtipo_cliente_p_f_e;
          }else if($item->idtipo_cliente==3){
            $idtipo_cliente=$item->idtipo_cliente_p_m_m_e;
          }else if($item->idtipo_cliente==4){
            $idtipo_cliente=$item->idtipo_cliente_p_m_m_d;
          }else if($item->idtipo_cliente==5){
            $idtipo_cliente=$item->idtipo_cliente_e_c_o_i;
          }else if($item->idtipo_cliente==6){
            $idtipo_cliente=$item->idtipo_cliente_f;
          }
          /* ****** Nombres ******* */
          if($item->idtipo_cliente==1){
            $nombre=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
          }else if($item->idtipo_cliente==2){
            $nombre=$item->nombre2.' '.$item->apellido_paterno2.' '.$item->apellido_materno2;
          }else if($item->idtipo_cliente==3){
            $nombre=$item->razon_social;
          }else if($item->idtipo_cliente==4){
            $nombre=$item->nombre_persona;
          }else if($item->idtipo_cliente==5){
            $nombre=$item->denominacion2;
          }else if($item->idtipo_cliente==6){
            $nombre=$item->denominacion;
          }
          $html.='<option value="'.$idtipo_cliente.'-'.$item->idperfilamientop.'-'.$item->idtipo_cliente.'-0">'.$nombre.'</option>';
        }

    $cli_union = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
    foreach ($cli_union as $item) {
      if($item->cliente!=null){
        $html.='<option value="'.$item->id_cliente_tipo.'-'.$item->idperfilamientop.'-'.$item->idtipo_cliente.'-'.$item->id_union.'">'.$item->cliente.'</option>';
      }
    }
    $html.='</select>       
      </div>
    </div>';

    echo $html;
  }

  function exportarBitacora($idp=0){
    if($idp==0){
      $where=array("id_cliente"=>$this->idcliente);
    }else{
      $where=array("id_perfilamiento"=>$idp);
    }
    $data["b"] = $this->ModeloCatalogos->getselectwherestatus("*","bitacora_docs_cliente",$where);
    $this->load->view('catalogos/cliente_cliente/bitacora_docs_cliente',$data);
  }

  /*function exportarBitacoraDocs($idp=0){
    if($idp==0){
      $where=array("id_cliente"=>$this->idcliente);
    }else{
      $where=array("id_perfilamiento"=>$idp);
    }
    //log_message("error","where: ".$where);
    //$bit = $this->ModeloCatalogos->getselectwherestatus("*","bitacora_docs_cliente",$where);
    $bit = $this->ModeloCatalogos->getBitocoraDocs($where);
    $idperf=0;
    $this->load->library('zip');
    foreach ($bit as $b) {
      //log_message('error', 'nombre_doc: '.$b->nombre_doc);
      $tipoc=$b->idtipo_cliente;

      //log_message('error', 'tipoc: '.$tipoc);
      if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
      if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
      if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
      if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
      if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
      if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
      
      $info=$this->General_model->get_tipoCliente($tabla,array($idt=>$b->id_clientec));
      if($tipoc==6){ 
        $cliente=mb_strtoupper($info->denominacion);
      }
      if($tipoc==3){
        $cliente=mb_strtoupper($info->razon_social);  
      }
      if($tipoc==4){
        $cliente=mb_strtoupper($info->nombre_persona);
      }
      if($tipoc==5){
        $cliente=mb_strtoupper($info->denominacion);
      }
      if($tipoc==1 || $tipoc==2){
        $cliente=mb_strtoupper($info->nombre.' '.$info->apellido_paterno.' '.$info->apellido_materno);  
      }
      //log_message('error', 'cliente: '.$cliente);

      if($b->tipo_doc=="formato_cc"){
        $this->zip->add_data("formato_cc/".$cliente."/".$b->nombre_doc."",base_url()."uploads/formato_cc/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="ine"){
        $this->zip->add_data("ine/".$cliente."/".$b->nombre_doc."",base_url()."uploads/ine/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="cif"){
        $this->zip->add_data("cif/".$cliente."/".$b->nombre_doc."",base_url()."uploads/cif/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="curp"){
        $this->zip->add_data("curp/".$cliente."/".$b->nombre_doc."",base_url()."uploads/curp/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="comprobante_dom"){
        $this->zip->add_data("compro_dom/".$cliente."/".$b->nombre_doc."",base_url()."uploads/compro_dom/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="formato_duen_bene"){
        $this->zip->add_data("compro_due_ben/".$cliente."/".$b->nombre_doc."",base_url()."uploads/compro_due_ben/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="const_est_legal"){
        $this->zip->add_data("constancia_legal/".$cliente."/".$b->nombre_doc."",base_url()."uploads/constancia_legal/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="carta_poder"){
        $this->zip->add_data("carta_poder/".$cliente."/".$b->nombre_doc."",base_url()."uploads/carta_poder/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="const_est_legal2"){
        $this->zip->add_data("constancia_legal/".$cliente."/".$b->nombre_doc."",base_url()."uploads/constancia_legal/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="comprobante_dom_apod"){
        $this->zip->add_data("compro_dom/".$cliente."/".$b->nombre_doc."",base_url()."uploads/compro_dom/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="esc_ints"){
        $this->zip->add_data("escrituras/".$cliente."/".$b->nombre_doc."",base_url()."uploads/escrituras/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="poder_not_fac_serv"){
        $this->zip->add_data("poder_not/".$cliente."/".$b->nombre_doc."",base_url()."uploads/poder_not/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="poder_not"){
        $this->zip->add_data("poder_not/".$cliente."/".$b->nombre_doc."",base_url()."uploads/poder_not/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="ine_apo"){
        $this->zip->add_data("ine/".$cliente."/".$b->nombre_doc."",base_url()."uploads/ine/".$b->nombre_doc."/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="const_est_legal3"){
        $this->zip->add_data("constancia_legal/".$cliente."/".$b->nombre_doc."",base_url()."uploads/constancia_legal/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="poder_not2"){
        $this->zip->add_data("poder_not/".$cliente."/".$b->nombre_doc."",base_url()."uploads/poder_not/".$b->nombre_doc);
      }
      else if($b->tipo_doc=="doc extra"){
        $this->zip->add_data("extras/".$cliente."/".$b->nombre_doc."",base_url()."uploads/extras/".$b->nombre_doc);
      }
      $idperf=$b->id_perfilamiento;
    }
    
    $this->zip->archive(base_url().'docs_client.zip');
    $this->zip->download('docs_client.zip');
  }*/

  function exportarBitacoraDocs($idp=0,$idcli=0,$anio_opera=0){
    //log_message("error", "idcli: ".$idcli);
    if($idp==0){
      $where=array("bd.id_cliente"=>$this->idcliente);
      if($idcli!=0){
        $id_cli = explode("-",$idcli);
        $where = array_merge($where,array("bd.id_clientec"=>$id_cli[0]));
        if($id_cli[3]!=0){
          $where = array_merge($where,array("id_union"=>$id_cli[3]));
        }
      }
      if($anio_opera!=0){
        $where = array_merge($where,array("DATE_FORMAT(bd.fecha, '%Y') = "=>$anio_opera));
      }
    }else{
      $where=array("id_perfilamiento"=>$idp);
    }

    //log_message("error", "where: ".json_encode($where));
    $bit = $this->ModeloCatalogos->getBitocoraDocs($where);
    $idperf=0;
    $this->load->library('zip');
    foreach ($bit as $b) {
      //log_message('error', 'nombre_doc: '.$b->nombre_doc);
      $tipoc=$b->idtipo_cliente;

      //log_message('error', 'tipoc: '.$tipoc);
      if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
      if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
      if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
      if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
      if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
      if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
      
      $info=$this->General_model->get_tipoCliente($tabla,array($idt=>$b->id_clientec));
      //o eliminar acentos -- mejor para nombre de cliente
      if($tipoc==6){ 
        $cliente=mb_strtoupper($info->denominacion,"UTF-8");
      }
      if($tipoc==3){
        $cliente=mb_strtoupper($info->razon_social,"UTF-8"); 
      }
      if($tipoc==4){
        $cliente=mb_strtoupper($info->nombre_persona,"UTF-8");
      }
      if($tipoc==5){
        $cliente=mb_strtoupper($info->denominacion,"UTF-8");
      }
      if($tipoc==1 || $tipoc==2){
        $cliente=mb_strtoupper($info->nombre.' '.$info->apellido_paterno.' '.$info->apellido_materno,"UTF-8");
      }
      $cliente=$this->eliminar_acentos($cliente);

      //log_message('error', 'cliente: '.$cliente);
      $id_opera=$b->id_operacion;
      if($id_opera!=0){
        //$opera_carp=$id_opera."/"; //CAMBIAR POR FOLIO DE CLIENTE EJEM: OPERA FOLIO
        $opera_carp="OPERA_".$b->folio_cliente."/";
      }else{
        $opera_carp="";
      }
      $filePath = FCPATH . 'uploads/';

      if($b->tipo_doc=="formato_cc"){
        $name_folder="formato_cc/";
      }
      else if($b->tipo_doc=="ine"){
        $name_folder="ine/";
      }
      else if($b->tipo_doc=="cif"){
        $name_folder="cif/";
      }
      else if($b->tipo_doc=="curp"){
        $name_folder="curp/";
      }
      else if($b->tipo_doc=="comprobante_dom"){
        $name_folder="compro_dom/";
      }
      else if($b->tipo_doc=="formato_duen_bene"){
        $name_folder="compro_due_ben/";
      }
      else if($b->tipo_doc=="const_est_legal"){
        $name_folder="constancia_legal/";
      }
      else if($b->tipo_doc=="carta_poder"){
        $name_folder="carta_poder/";
      }
      else if($b->tipo_doc=="const_est_legal2"){
        $name_folder="constancia_legal/";
      }
      else if($b->tipo_doc=="comprobante_dom_apod"){
        $name_folder="compro_dom/";
      }
      else if($b->tipo_doc=="esc_ints"){
        $name_folder="escrituras/";
      }
      else if($b->tipo_doc=="poder_not_fac_serv"){
        $name_folder="poder_not/";
      }
      else if($b->tipo_doc=="poder_not"){
        $name_folder="poder_not/";
      }
      else if($b->tipo_doc=="ine_apo"){
        $name_folder="ine/".$b->nombre_doc."/";
      }
      else if($b->tipo_doc=="const_est_legal3"){
        $name_folder="constancia_legal/";
      }
      else if($b->tipo_doc=="poder_not2"){
        $name_folder="poder_not/";
      }
      else if($b->tipo_doc=="doc extra"){
        $name_folder="extras/";
      }

      //año, clientes, operaciones, tipo doc, documento
      $doc_comp= $filePath.$name_folder.$b->nombre_doc;
      if (file_exists($doc_comp)) {
        if($b->tipo_doc=="formato_cc"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."formato_cc/".$b->nombre_doc, file_get_contents($filePath."formato_cc/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="ine"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."ine/".$b->nombre_doc, file_get_contents($filePath."ine/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="cif"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."cif/".$b->nombre_doc, file_get_contents($filePath."cif/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="curp"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."curp/".$b->nombre_doc, file_get_contents($filePath."curp/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="comprobante_dom"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."compro_dom/".$b->nombre_doc, file_get_contents($filePath."compro_dom/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="formato_duen_bene"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."compro_due_ben/".$b->nombre_doc, file_get_contents($filePath."compro_due_ben/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="const_est_legal"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."constancia_legal/".$b->nombre_doc, file_get_contents($filePath."constancia_legal/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="carta_poder"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."carta_poder/".$b->nombre_doc, file_get_contents($filePath."carta_poder/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="const_est_legal2"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."constancia_legal/".$b->nombre_doc, file_get_contents($filePath."constancia_legal/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="comprobante_dom_apod"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."compro_dom/".$b->nombre_doc, file_get_contents($filePath."compro_dom/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="esc_ints"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."escrituras/".$b->nombre_doc, file_get_contents($filePath."escrituras/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="poder_not_fac_serv"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."poder_not/".$b->nombre_doc, file_get_contents($filePath."poder_not/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="poder_not"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."poder_not/".$b->nombre_doc, file_get_contents($filePath."poder_not/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="ine_apo"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."ine/".$b->nombre_doc, file_get_contents($filePath."ine/".$b->nombre_doc."/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="const_est_legal3"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."constancia_legal/".$b->nombre_doc, file_get_contents($filePath."constancia_legal/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="poder_not2"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."poder_not/".$b->nombre_doc, file_get_contents($filePath."poder_not/".$b->nombre_doc));
        }
        else if($b->tipo_doc=="doc extra"){
          $this->zip->add_data($b->anio_opera."/".$cliente."/".$opera_carp."extras/".$b->nombre_doc, file_get_contents($filePath."extras/".$b->nombre_doc));
        }
      }
      $idperf=$b->id_perfilamiento;
    }
    
    $fecha=date("YmdGis");
    $zipFilePath = FCPATH . $fecha.'docs_client.zip';
    //log_message("error","name_zip_: ".$zipFilePath);
    $this->session->set_userdata("name_zip_".$this->idcliente,$zipFilePath);

    $this->zip->archive($zipFilePath);
    $this->zip->download($fecha.'_docs_client.zip');
  }

  public function deleteZipCli() {
    $zipFilePath = $this->session->userdata('name_zip_'.$this->idcliente);
    //log_message("error", "deleteZipCli: " . $zipFilePath);
    
    if(file_exists($zipFilePath)) {
      //log_message("error", "Archivo encontrado, eliminando: " . $zipFilePath);
      unlink($zipFilePath); // Elimina el archivo ZIP del servidor
    }else {
      log_message("error", "Archivo no encontrado: " . $zipFilePath);
    }
  }

  function exportarExpedientes(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/cliente_cliente/btns_export');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/cliente_cliente/btns_exportjs'); //agregar js para intentar eliminar zip despues de 6 seg de descargar (click)
  }

  public function exportarClientes(){
    $data["c"] = $this->ModeloCliente->get_clientes_all($this->idcliente);
    $data["cu"] = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
    $this->load->view('catalogos/cliente_cliente/exportar_clientes',$data);
  }

  public function exportarClientesBenes(){
    $data["c"] = $this->ModeloCliente->get_clientes_all($this->idcliente);
    $data["cu"] = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
    $this->load->view('catalogos/cliente_cliente/exportar_clientesBenes',$data);
  }

  public function generarMalla($idtp){
    if($idtp==1)
      $this->load->view('catalogos/cliente_cliente/mallas_cargas/malla_pfme');
    else if($idtp==2)
      $this->load->view('catalogos/cliente_cliente/mallas_cargas/malla_pfe');
    else if($idtp==3)
      $this->load->view('catalogos/cliente_cliente/mallas_cargas/malla_pme');
    else if($idtp==4)
      $this->load->view('catalogos/cliente_cliente/mallas_cargas/malla_pmmd');
    else if($idtp==5)
      $this->load->view('catalogos/cliente_cliente/mallas_cargas/malla_ecoi');
    else if($idtp==6)
      $this->load->view('catalogos/cliente_cliente/mallas_cargas/malla_fide');
  }

  function cargaMasivaDoc($idtc){
    $configUpload['upload_path'] = FCPATH.'fileExcel/';
    $configUpload['allowed_types'] = 'csv';

    $this->load->library('upload', $configUpload);
    $this->upload->do_upload('inputFile');  
    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
    $file_name = $upload_data['file_name']; //uploded file name
    $extension=$upload_data['file_ext'];    // uploded file extension
    $id=0;
    //log_message('error', 'file_name: '.$file_name);

     //===========================================
    $objReader= PHPExcel_IOFactory::createReader('CSV'); // For cvs delimitado por comas  
    $objReader->setReadDataOnly(true);          
      //Load excel file
    $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);      
    $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
    $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
    //loop from first data untill last data

    if($idtc==1 || $idtc==2){
      for($i=2;$i<=$totalrows;$i++){
        $Nombre = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
        $ApellidoP = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
        $ApellidoM = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
        $Nombreide = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
        $Autoridad = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
        $num_iden = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
        $genero = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        $fecha_nac = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
        $pais_nac = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();

        $pais_nacion = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
        $actividad = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
        $pais_dir = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
        $tipo_vial = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
        $calle = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
        $num_ext = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
        $num_int = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
        $cp = $objWorksheet->getCellByColumnAndRow(16,$i)->getValue();

        $municipio = $objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
        $localidad = $objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
        $estado = $objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
        $colonia = $objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
        $Tratándose = $objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
        $tipo_vial_ext = $objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
        $calle_ext = $objWorksheet->getCellByColumnAndRow(23,$i)->getValue();
        $num_ext_ext = $objWorksheet->getCellByColumnAndRow(24,$i)->getValue();

        $num_int_ext = $objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
        $cp_ext = $objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
        $municipio_ext = $objWorksheet->getCellByColumnAndRow(27,$i)->getValue();
        $localidad_ext = $objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
        $estado_ext = $objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
        $col_ext = $objWorksheet->getCellByColumnAndRow(30,$i)->getValue();
        $email_ext = $objWorksheet->getCellByColumnAndRow(31,$i)->getValue();
        $rfc_ext = $objWorksheet->getCellByColumnAndRow(32,$i)->getValue();

        $curp_ext = $objWorksheet->getCellByColumnAndRow(33,$i)->getValue();
        $num_tel_ext = $objWorksheet->getCellByColumnAndRow(34,$i)->getValue();
        
        if($idtc==1){
          $Presenta = $objWorksheet->getCellByColumnAndRow(35,$i)->getValue();
          $nombre_apo = $objWorksheet->getCellByColumnAndRow(36,$i)->getValue();
          $app_apo = $objWorksheet->getCellByColumnAndRow(37,$i)->getValue();
          $apm_apo = $objWorksheet->getCellByColumnAndRow(38,$i)->getValue();
          $nombreide_apo = $objWorksheet->getCellByColumnAndRow(39,$i)->getValue();
          $autoridad_apo = $objWorksheet->getCellByColumnAndRow(40,$i)->getValue();
          $num_iden_apo = $objWorksheet->getCellByColumnAndRow(41,$i)->getValue();
          $fecha_nac_apo = $objWorksheet->getCellByColumnAndRow(42,$i)->getValue();
          $rfc_apo = $objWorksheet->getCellByColumnAndRow(43,$i)->getValue();
          $curp_apo = $objWorksheet->getCellByColumnAndRow(44,$i)->getValue();
          $genero_apo = $objWorksheet->getCellByColumnAndRow(45,$i)->getValue();
          $tipo_vial_apo = $objWorksheet->getCellByColumnAndRow(46,$i)->getValue();
          $calle_apo = $objWorksheet->getCellByColumnAndRow(47,$i)->getValue();
          $num_ext_apo = $objWorksheet->getCellByColumnAndRow(48,$i)->getValue();
          $num_int_apo = $objWorksheet->getCellByColumnAndRow(49,$i)->getValue();
          $cp_apo = $objWorksheet->getCellByColumnAndRow(50,$i)->getValue();
          $municipio_apo = $objWorksheet->getCellByColumnAndRow(51,$i)->getValue();
          $localidad_apo = $objWorksheet->getCellByColumnAndRow(52,$i)->getValue();
          $estado_apo = $objWorksheet->getCellByColumnAndRow(53,$i)->getValue();
          $colonia_apo = $objWorksheet->getCellByColumnAndRow(54,$i)->getValue();

          $nombre_bene = $objWorksheet->getCellByColumnAndRow(55,$i)->getValue();
          $app_bene = $objWorksheet->getCellByColumnAndRow(56,$i)->getValue();
          $apm_bene = $objWorksheet->getCellByColumnAndRow(57,$i)->getValue();
          $fecha_nac_bene = $objWorksheet->getCellByColumnAndRow(58,$i)->getValue();
          $genero_bene = $objWorksheet->getCellByColumnAndRow(59,$i)->getValue();
          $porc_bene = $objWorksheet->getCellByColumnAndRow(60,$i)->getValue();
          $tipo_vial_bene = $objWorksheet->getCellByColumnAndRow(61,$i)->getValue();
          $calle_bene = $objWorksheet->getCellByColumnAndRow(62,$i)->getValue();
          $num_ext_bene = $objWorksheet->getCellByColumnAndRow(63,$i)->getValue();
          $num_int_bene = $objWorksheet->getCellByColumnAndRow(64,$i)->getValue();
          $cp_bene = $objWorksheet->getCellByColumnAndRow(60,$i)->getValue();
          $municipio_bene = $objWorksheet->getCellByColumnAndRow(61,$i)->getValue();
          $localidad_bene = $objWorksheet->getCellByColumnAndRow(62,$i)->getValue();
          $estado_bene = $objWorksheet->getCellByColumnAndRow(63,$i)->getValue();
          $colonia_bene = $objWorksheet->getCellByColumnAndRow(64,$i)->getValue();
        }
        if($idtc==2){
          $nombre_bene = $objWorksheet->getCellByColumnAndRow(35,$i)->getValue();
          $app_bene = $objWorksheet->getCellByColumnAndRow(36,$i)->getValue();
          $apm_bene = $objWorksheet->getCellByColumnAndRow(37,$i)->getValue();
          $fecha_nac_bene = $objWorksheet->getCellByColumnAndRow(38,$i)->getValue();
          $genero_bene = $objWorksheet->getCellByColumnAndRow(39,$i)->getValue();
          $porc_bene = $objWorksheet->getCellByColumnAndRow(40,$i)->getValue();
          $tipo_vial_bene = $objWorksheet->getCellByColumnAndRow(41,$i)->getValue();
          $calle_bene = $objWorksheet->getCellByColumnAndRow(42,$i)->getValue();
          $num_ext_bene = $objWorksheet->getCellByColumnAndRow(43,$i)->getValue();
          $num_int_bene = $objWorksheet->getCellByColumnAndRow(44,$i)->getValue();
          $cp_bene = $objWorksheet->getCellByColumnAndRow(45,$i)->getValue();
          $municipio_bene = $objWorksheet->getCellByColumnAndRow(46,$i)->getValue();
          $localidad_bene = $objWorksheet->getCellByColumnAndRow(47,$i)->getValue();
          $estado_bene = $objWorksheet->getCellByColumnAndRow(48,$i)->getValue();
          $colonia_bene = $objWorksheet->getCellByColumnAndRow(49,$i)->getValue();
        }
        
        if($idtc==1){
          $nom_act='activida_o_giro';
        }else if($idtc==2){
          $nom_act='actividad_o_giro';
        }
        $data = array(
          'nombre' => $Nombre,
          'apellido_paterno' => $ApellidoP,
          'apellido_materno' => $ApellidoM,
          'nombre_identificacion' => $Nombreide,
          'autoridad_emite' => $Autoridad,
          'numero_identificacion' => $num_iden,
          'genero' => $genero,
          'fecha_nacimiento' => $fecha_nac,
          'pais_nacimiento' => $pais_nac,
          'pais_nacionalidad' => $pais_nacion,
           $nom_act => $actividad,
          'otra_ocupa' => $pais_dir,
          'tipo_vialidad_d' => $tipo_vial,
          'calle_d' => $calle,
          'no_ext_d' => $num_ext,
          'no_int_d' => $num_int,
          'colonia_d' => $colonia,
          'municipio_d' => $municipio,
          'localidad_d' => $localidad,
          'estado_d' => $estado,

          'cp_d' => $cp,
          'pais_d' => $pais_dir,
          'trantadose_persona' => $Tratándose,
          'tipo_vialidad_t' => $tipo_vial_ext,
          'calle_t' => $calle_ext,
          'no_ext_t' => $num_ext_ext,
          'no_int_t' => $num_int_ext,
          'colonia_t' => $col_ext,
          'municipio_t' => $municipio_ext,
          'localidad_t' => $localidad_ext,

          'estado_t' => $estado_ext,
          'cp_t' => $cp_ext,
          'correo_t' => $email_ext,
          'r_f_c_t' => $rfc_ext,
          'curp_t' => $curp_ext,
          'telefono_t' => $num_tel_ext,
        ); 
        if($idtc==1){//apoderado 
          $data2 = array( 
            'presente_legal' => $Presenta, //aca hay yn error cuando inserta tipo2
            'nombre_p' => $nombre_apo,
            'apellido_paterno_p' => $app_apo,
            'apellido_materno_p' => $apm_apo,
            'nombre_identificacion_p' => $nombreide_apo,
            'autoridad_emite_p' => $autoridad_apo,
            'numero_identificacion_p' => $num_iden_apo,
            'fecha_nac_apodera' => $fecha_nac_apo,
            'rfc_apodera' => $rfc_apo,
            'curp_apodera' => $curp_apo,
            'genero_p' => $genero_apo,
            'tipo_vialidad_p' => $tipo_vial_apo,
            'calle_p' => $calle_apo,
            'no_ext_p' => $num_ext_apo,
            'no_int_p' => $num_int_apo,
            'colonia_p' => $colonia_apo,
            'municipio_p' => $municipio_apo,
            'localidad_p' => $localidad_apo,
            'estado_p' => $estado_apo,
            'cp_p' => $cp_apo
          ); 
          if($idtc==1){
            array_merge($data, $data2);
          }
        }

        //log_message('error', 'fecha_vence: '.$newDate);
        $datap['idcliente']=$this->idcliente;
        if($idtc==1){
          $datap["idtipo_cliente"]=1;
        }else if($idtc==2){
          $datap["idtipo_cliente"]=2;
        }
        $data["idperfilamiento"]=$this->ModeloCatalogos->tabla_inserta('perfilamiento',$datap);
        if($idtc==1){
          $id=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_f_m',$data);
          $data_bene = array(
            'idtipo_cliente_p_f_m'=>$id,
            'nombre' => $nombre_bene,
            'apellido_paterno' => $app_bene,
            'apellido_materno' => $apm_bene,
            'fecha_nacimiento' => $fecha_nac_bene,
            'genero' => $genero_bene,
            'porcentaje' => $porc_bene,
            'tipo_vialidad' => $tipo_vial_bene,
            'calle' => $calle_bene,
            'no_ext' => $num_ext_bene,
            'no_int' => $num_int_bene,
            'cp' => $cp_bene,
            'municipio' => $municipio_bene,
            'localidad' => $localidad_bene,
            'estado' => $estado_bene,
            'colonia' => $colonia_bene
          );
          $this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_f_m_beneficiario',$data_bene);
        }else if($idtc==2){
          $id=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_f_e',$data);
        }
      }
    }//if de cliente tipo 1 or 2
    else if($idtc==3){ //tipo cliente 3
      for($i=2;$i<=$totalrows;$i++){
        $razon = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
        $giro = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
        $clave = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
        $extranjero = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
        $pais = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
        $nacionalidad = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
        $pais_dir = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        $tipo_vial = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
        $calle = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
        $num_ext = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
        $num_int = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
        $cp = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
        $municipio = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
        $localidad = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
        $estado = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
        $colonia = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
        $tel = $objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
        $email = $objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
        $fecha_const = $objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
        $num_acta = $objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
        $nom_nota = $objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
        $Nombre_apo = $objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
        $app_apo = $objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
        $apm_apo = $objWorksheet->getCellByColumnAndRow(23,$i)->getValue();
        $nom_ide_apo = $objWorksheet->getCellByColumnAndRow(24,$i)->getValue();
        $autoridad_apo = $objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
        $num_iden_apo = $objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
        $fecha_nac_apo = $objWorksheet->getCellByColumnAndRow(27,$i)->getValue();
        $genero_apo = $objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
        $rfc_apo = $objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
        $curp_apo = $objWorksheet->getCellByColumnAndRow(30,$i)->getValue();
        $data = array(
          'razon_social' => $razon,
          'giro_mercantil' => $giro,
          'clave_registro' => $clave,
          'extranjero' => $extranjero,
          'pais' => $pais,
          'nacionalidad' => $nacionalidad,
          'tipo_vialidad_d' => $tipo_vial,
          'calle_d' => $calle,
          'no_ext_d' => $num_ext,
          'no_int_d' => $num_int,
          'colonia_d' => $colonia,
          'municipio_d' => $municipio,
          'localidad_d' => $localidad,
          'estado_d' => $estado,
          'cp_d' => $cp,
          'pais_d' => $pais_dir,
          'telefono_d' => $tel,
          'correo_d' => $email,
          'fecha_constitucion_d' => $fecha_const,
          'numero_acta_d' => $num_acta,
          'nombre_notario_d' => $nom_nota,
          'nombre_g' => $Nombre_apo,
          'apellido_paterno_g' => $app_apo,
          'apellido_materno_g' => $apm_apo,
          'nombre_identificacion_g' => $nom_ide_apo,
          'autoridad_emite_g' => $autoridad_apo,
          'numero_identificacion_g' => $num_iden_apo,
          'fecha_nacimiento_g' => $fecha_nac_apo,
          'genero_g' => $genero_apo,
          'r_f_c_g' => $rfc_apo,
          'curp_g' => $curp_apo
        ); 

        $datap['idcliente']=$this->idcliente;
        $datap["idtipo_cliente"]=3;
        $data["idperfilamiento"]=$this->ModeloCatalogos->tabla_inserta('perfilamiento',$datap);
        $id=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_m_m_e',$data);
      }
    }//final de if de tipo 3
    else if($idtc==4){ //tipo cliente 4
      for($i=2;$i<=$totalrows;$i++){
        $nombre = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
        $nacionalidad = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
        $giro = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
        $tipo_vial = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
        $calle = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
        $num_ext = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
        $num_int = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        $cp = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
        $municipio = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
        $localidad = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
        $estado = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
        $colonia = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
        $fecha_const = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
        $nom_apo = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
        $app_apo = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
        $apm_apo = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
        $genero_apo = $objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
        $fecha_nac_apo = $objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
        $data = array(
          'nombre_persona' => $nombre,
          'nacionalidad' => $nacionalidad,
          'giro_mercantil' => $giro,
          'tipo_vialidad_d' => $tipo_vial,
          'calle_d' => $calle,
          'no_ext_d' => $num_ext,
          'no_int_d' => $num_int,
          'colonia_d' => $colonia,
          'municipio_d' => $municipio,
          'localidad_d' => $localidad,
          'estado_d' => $estado,
          'cp_d' => $cp,
          'fecha_constitucion_d' => $fecha_const,
          'nombre_g' => $nom_apo,
          'apellido_paterno_g' => $app_apo,
          'apellido_materno_g' => $apm_apo,
          'genero_g' => $genero_apo,
          'fecha_nacimiento_g' => $fecha_nac_apo 
        ); 

        $datap['idcliente']=$this->idcliente;
        $datap["idtipo_cliente"]=4;
        $data["idperfilamiento"]=$this->ModeloCatalogos->tabla_inserta('perfilamiento',$datap);
        $id=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_p_m_m_d',$data);
      }
    }//final de if de tipo 4
    else if($idtc==5){ //tipo cliente 5
      for($i=2;$i<=$totalrows;$i++){
        $denomina = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
        $fecha_esta = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
        $nacionalidad = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
        $clave = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
        $pais = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
        $pais_dir = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
        $tipo_vial = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        $calle = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
        $num_ext = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
        $num_int = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
        $cp = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
        $municipio = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
        $localidad = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
        $estado = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
        $colonia = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
        $tel = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
        $email = $objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
        $certifica = $objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
        $Nombre_apo = $objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
        $app_apo = $objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
        $apm_apo = $objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
        $genero_apo = $objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
        $fecha_nac_apo = $objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
        $nom_ide_apo = $objWorksheet->getCellByColumnAndRow(23,$i)->getValue();
        $autoridad_apo = $objWorksheet->getCellByColumnAndRow(24,$i)->getValue();
        $num_iden_apo = $objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
        $rfc_apo = $objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
        $curp_apo = $objWorksheet->getCellByColumnAndRow(27,$i)->getValue();
        $data = array(
          'denominacion' => $denomina,
          'fecha_establecimiento' => $fecha_esta,
          'nacionalidad' => $nacionalidad,
          'clave_registro' => $clave,
          'pais' => $pais,
          'tipo_vialidad_d' => $tipo_vial,
          'calle_d' => $calle,
          'no_ext_d' => $num_ext,
          'no_int_d' => $num_int,
          'colonia_d' => $colonia,
          'municipio_d' => $municipio,
          'localidad_d' => $localidad,
          'estado_d' => $estado,
          'cp_d' => $cp,
          'pais_d' => $pais_dir,
          'telefono_d' => $tel,
          'correo_d' => $email,
          'certificado_matricula' => $certifica,
          'nombre_g' => $Nombre_apo,
          'apellido_paterno_g' => $app_apo,
          'apellido_materno_g' => $apm_apo,
          'genero_g' => $genero_apo,
          'fecha_nacimiento_g' => $fecha_nac_apo,
          'nombre_identificacion_g' => $nom_ide_apo,
          'autoridad_emite_g' => $autoridad_apo,
          'numero_identificacion_g' => $num_iden_apo,
          'r_f_c_g' => $rfc_apo,
          'curp_g' => $curp_apo
        ); 

        $datap['idcliente']=$this->idcliente;
        $datap["idtipo_cliente"]=5;
        $data["idperfilamiento"]=$this->ModeloCatalogos->tabla_inserta('perfilamiento',$datap);
        $id=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_e_c_o_i',$data);
      }
    }//final de if de tipo 5
    else if($idtc==6){ //tipo cliente 6
      for($i=2;$i<=$totalrows;$i++){
        $denomina = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
        $referencia = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
        $rfc = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
        $Nombre_apo = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
        $app_apo = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
        $apm_apo = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
        $genero_apo = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        $fecha_nac_apo = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
        $nom_ide_apo = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
        $autoridad_apo = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
        $num_iden_apo = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
        $rfc_apo = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
        $curp_apo = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
        $data = array(
          'denominacion' => $denomina,
          'numero_referencia' => $referencia,
          'clave_registro' => $rfc,
          'nombre_g' => $Nombre_apo,
          'apellido_paterno_g' => $app_apo,
          'apellido_materno_g' => $apm_apo,
          'nombre_identificacion_g' => $nom_ide_apo,
          'autoridad_emite_g' => $autoridad_apo,
          'numero_identificacion_g' => $num_iden_apo,
          'genero_g' => $genero_apo,
          'r_f_c_g' => $rfc_apo,
          'curp_g' => $curp_apo,
          'fecha_nacimiento_g' => $fecha_nac_apo
        ); 

        $datap['idcliente']=$this->idcliente;
        $datap["idtipo_cliente"]=6;
        $data["idperfilamiento"]=$this->ModeloCatalogos->tabla_inserta('perfilamiento',$datap);
        $id=$this->ModeloCatalogos->tabla_inserta('tipo_cliente_f',$data);
      }
    }//final de if de tipo 6

    unlink('fileExcel/'.$file_name); //File Deleted After uploading in database . 
    //===========================================
    echo json_encode($id);
  }

  public function generarPDF($tipo,$idp,$fecha = 0){
    $data["idperfilamiento"]=$idp;

    if (strtotime($fecha) !== false) {      
      $data["fecha"] = date("d-m-Y", strtotime($fecha));
    } else {
      $data["fecha"] = date("d-m-Y");
    }
    
    $data["label_a8"]="";
    $get_act=$this->ModeloCatalogos->getselectwherestatus("idactividad","cliente_actividad",array('idcliente'=>$this->idcliente,"activo"=>1));
    foreach($get_act as $a){
      $idact = $a->idactividad;  
    }
    if($idact==11){
      $data["label_a8"]="<tr>
          <td>Manifiesto que no existe relación de negocios entre el cliente y <b>".$this->session->userdata("nombre_user_log").".</b></td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr><td>Manifiesto que no requiero que se facture la Unidad con mi RFC, y solicito que se genere con el RFC genérica.</td>
        </tr>";
    }
    
    if($tipo==1){
      $this->load->view('Reportes/formatoPFME',$data);
    }else if($tipo==2){
      $this->load->view('Reportes/formatoPFE',$data);
    }else if($tipo==3){
      $this->load->view('Reportes/formatoPMME',$data);
    }else if($tipo==4){
      $this->load->view('Reportes/formatoPMDP',$data);
    }else if($tipo==5){
      $this->load->view('Reportes/formatoECOI',$data);
    }else if($tipo==6){
      $this->load->view('Reportes/formatoFIDE',$data);
    }
  }

  public function generarPDF_CA($idcc,$idp,$tipo,$ido,$idcgr,$tc){
    $data["idcc"]=$idcc;
    $data["idp"]=$idp;
    $data["tipoc"]=$tipo;
    $data["idopera"]=$ido;
    $data["idcgr"]=$idcgr;
    $data["tcc"]=$tc;
    $this->load->view('Reportes/formatoCA',$data);
  }

  public function generarPDF_CAE($idcc,$idp,$tipoc,$idopera,$idgr){
    $ca = $this->General_model->get_tableRow('cuestionario_ampliado_embajada',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
    if(isset($ca)){
      $data['ca'] = $ca;
    }
    $this->load->view('Reportes/formatoCAE',$data);
  }

  public function generarPDF_CAPM($idcc,$idp,$tipoc,$idopera,$idgr){
    $data['tipoc']=$tipoc;
    $data['tipo']=$tipoc;
    $data['idp']=$idp;
    $data['idclientec']=$idcc;
    $data['idopera']=$idopera;
    $data['idcgr']=$idgr;
    $data['idcc']=$idcc;

    $this->load->view('Reportes/formatoCAPM',$data);
  }

  public function generarPDF_CAFIDE($idcc,$idp,$tipoc,$idopera,$idgr){
    $data['tipoc']=$tipoc;
    $data['idp']=$idp;
    $data['idcc']=$idcc;
    $data['idopera']=$idopera;
    $this->load->view('Reportes/formatoCAFIDE',$data);
  }

  public function generarPDF_busqueda(){
    $data['nombreC'] = $this->input->post('nombreC');
    $data['fechaC'] = $this->input->post('fechaC');
    $data['folioC'] = $this->input->post('folioC');
    $tipoC = $this->input->post('tipoC');

    if($tipoC == 1){
      $this->load->view('Reportes/formatoBusquedaSE',$data);
    }else{
      $this->load->view('Reportes/formatoBusquedaNE',$data);
    }    

  }

}