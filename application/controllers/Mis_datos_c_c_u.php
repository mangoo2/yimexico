<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mis_datos_c_c_u extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
		$this->load->model('ModeloCliente');
		$this->load->model('ModeloCatalogos');
    $this->load->model('ModeloGeneral');
	  $this->load->model('Login_model');
  	if (!$this->session->userdata('logeado')){
          redirect('/Login');
    }else{
        $this->perfilid=$this->session->userdata('perfilid');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->idcliente=$this->session->userdata('idcliente');
        $this->tipo=$this->session->userdata('tipo');
        $this->status=$this->session->userdata('status');
        $this->idcliente_usuario=$this->session->userdata('idcliente_usuario');
        //ira el permiso del modulo
      /* 
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,6);// 2 es el id del submenu
        if ($permiso==0) {
            redirect('/Sistema');
        }
      */  
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechaactual = date('Y-m-d');
	}
	public function index(){
      $data['get_u_c']=$this->ModeloCatalogos->getselectwhererow('usuarios_clientes','idcliente_usuario',$this->idcliente_usuario);
      $data['get_u_u']=$this->ModeloCatalogos->getselectwhererow('usuarios','idcliente_usuario',$this->idcliente_usuario);
      $data['get_u_p']=$this->ModeloCatalogos->getselectwhererow('perfiles','perfilId',$data['get_u_u']->perfilId);
      if($data['get_u_c']->tipo_vialidad!=''){
        $data['get_t_v']=$this->ModeloCatalogos->getselectwhererow('tipo_vialidad','id',$data['get_u_c']->tipo_vialidad);
        $data['get_t_v2']=1;
      }else{
        $data['get_t_v']='';
        $data['get_t_v2']=0;
      }
      $data['get_p']=$this->ModeloCatalogos->getselectwhererow('pais','clave',$data['get_u_c']->pais);
      $get_e=$this->ModeloCatalogos->getselectwhererow('estado','clave',$data['get_u_c']->estado);
      if($data['get_u_c']->pais=='MX'){
        $data['estado']=$get_e->estado;
      }else{
        $data['estado']=$data['get_u_c']->estado;
      }
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('catalogos/usuarios_cliente_cliente/mis_datos',$data);
      $this->load->view('templates/footer');
      $this->load->view('catalogos/usuarios_cliente_cliente/jsmis_datos');
	}
  public function registro_clientes_c_usuario(){ 
    $data=$this->input->post();
    $id=$data['UsuarioID'];
    $Usuario=$data['Usuario'];
    unset($data['UsuarioID']);
    $pass = $data['contrasena'];
    $aux=0;
  
    $respuesta = $this->Login_model->login($Usuario);
    $contrasena='';

    foreach ($respuesta as $item) {
        $contrasena =$item->contrasena;
    }
    $verificar = password_verify($pass,$contrasena);
    //En caso afirmativo, inicializamos datos de sesión
    if ($verificar){
       $aux=1;
    }else{
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena']=$pass;
        $aux=2;
        $this->ModeloCatalogos->updateCatalogo($data,'UsuarioID',$id,'usuarios');
    }
    echo $aux;  
  }
}