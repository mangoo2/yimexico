<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfiles extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //Permiso del modulo
           /*/ $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,3);// 2 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }/*/
        }
    }

	public function index(){   
        $data['menu1']=$this->ModeloCatalogos->getDatamenu(1);
        $data['menu_sub1']=$this->ModeloCatalogos->getDatasudmenu(1);
        $data['menu2']=$this->ModeloCatalogos->getDatamenu(2);
        $data['menu_sub2']=$this->ModeloCatalogos->getDatasudmenu(2);
        $data['menu3']=$this->ModeloCatalogos->getDatamenu(3);
        $data['menu_sub3']=$this->ModeloCatalogos->getDatasudmenu(3);
        $data['menu4']=$this->ModeloCatalogos->getDatamenu(4);
        $data['menu_sub4']=$this->ModeloCatalogos->getDatasudmenu(4);

		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/perfiles/listperfil',$data);
        $this->load->view('templates/footer');
        $this->load->view('configuracion/perfiles/jsperfil');
	}

    public function getlistregistro() {
        $params = $this->input->post();
        $getdata = $this->ModeloGeneral->getperfiles($params);
        $totaldata= $this->ModeloGeneral->total_perfiles($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro(){
        $data = $this->input->post();
        $aux = 0;
        $id = $data['perfilId'];
        unset($data['perfilId']); 
        if ($id>0) {
            $where = array('perfilId'=>$id);
            $this->ModeloCatalogos->updateCatalogo($data,'perfilId',$id,'perfiles');
            $this->ModeloCatalogos->getdeletewheren('perfiles_detalles',$where);
            $aux = $id; 
            $result=2;
        }else{
            $aux = $this->ModeloCatalogos->tabla_inserta('perfiles',$data);
            $result=1;
        }   
        $arrayName = array('id'=>$aux,'status'=>$result);
        echo json_encode($arrayName);
    }
    function visualizar_permisos(){
       $id = $this->input->get('id');
       $getdatos = $this->ModeloCatalogos->getselectwhere('perfiles_detalles','perfilId',$id);
       echo json_encode($getdatos);
    }
    public function guardar_perfil_detalles(){
        $submenu1 = $this->input->post('submenu1');
        $submenu2 = $this->input->post('submenu2');
        $submenu3 = $this->input->post('submenu3');
        $submenu4 = $this->input->post('submenu4');
        $DATA1 = json_decode($submenu1);
        $DATA2 = json_decode($submenu2);
        $DATA3 = json_decode($submenu3);
        $DATA4 = json_decode($submenu4);
        for ($i=0;$i<count($DATA1);$i++) { 
            $data['perfilId']= $DATA1[$i]->perfilId;
            $data['MenusubId']= $DATA1[$i]->MenusubId;
            $this->ModeloCatalogos->tabla_inserta('perfiles_detalles',$data);
        }
        for ($i=0;$i<count($DATA2);$i++) { 
            $data['perfilId']= $DATA2[$i]->perfilId;
            $data['MenusubId']= $DATA2[$i]->MenusubId;
            $this->ModeloCatalogos->tabla_inserta('perfiles_detalles',$data);
        }
        for ($i=0;$i<count($DATA3);$i++) { 
            $data['perfilId']= $DATA3[$i]->perfilId;
            $data['MenusubId']= $DATA3[$i]->MenusubId;
            $this->ModeloCatalogos->tabla_inserta('perfiles_detalles',$data);
        }
        for ($i=0;$i<count($DATA4);$i++) { 
            $data['perfilId']= $DATA4[$i]->perfilId;
            $data['MenusubId']= $DATA4[$i]->MenusubId;
            $this->ModeloCatalogos->tabla_inserta('perfiles_detalles',$data);
        }
    }
    public function verificapass(){
        $pass = $this->input->post('pass'); 
        $verifica = $this->ModeloCatalogos->verificar_pass($pass);
        $aux = 0;
        if($verifica == 1){
            $aux=1;
        }
        if($aux==1){
            
        }
    echo $aux;
    }
    public function deleteregistro(){
        $id = $this->input->post('id');
        $where = array('perfilId'=>$id);
        $this->ModeloCatalogos->getdeletewheren('perfiles',$where);
        $this->ModeloCatalogos->getdeletewheren('perfiles_detalles',$where);
    }
}