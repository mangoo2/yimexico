<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operaciones extends CI_Controller {

	public function __construct(){
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('ModeloCliente');
	  $this->load->model('Login_model');
    $this->load->model('General_model');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModeloCliente');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloTransaccion');
  	if (!$this->session->userdata('logeado')){
      redirect('/Login');
    }else{
      $this->perfilid=$this->session->userdata('perfilid');
      $this->idpersonal=$this->session->userdata('idpersonal');
      $this->idcliente=$this->session->userdata('idcliente');
      $this->tipo=$this->session->userdata('tipo');
      $this->status=$this->session->userdata('status');
      $this->permiso_avanza=$this->session->userdata('permiso_avanza');
      $this->idcliente_usuario=$this->session->userdata('idcliente_usuario');
      $this->idperfilamiento = $this->General_model->get_tableRowC('idperfilamiento','perfilamiento',array('idcliente'=>$this->idcliente));
    }
    date_default_timezone_set('America/Mexico_City');
	}
	public function index(){      
    $data['idcliente'] = $this->idcliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/lista_operaciones',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/lista_operaciones_js');
	}

  public function creaOperacion(){
    $data = $this->input->post();
    $data['id_cliente'] = $this->idcliente;
    $data['fecha_reg'] = date("Y-m-d H:i:s");
    $get_fol = $this->ModeloCliente->get_folio_cli($this->idcliente);
    $data["folio_cliente"] = $get_fol->folio_cliente + 1;
    $id=$this->ModeloCatalogos->tabla_inserta('operaciones',$data);
    echo $id;
  }

  public function submit_operacion(){
    $data = $this->input->post();
    //log_message('error', 'id: '.$data['id']);
    $data['id_cliente'] = $this->idcliente;
    $id_hist = $data['id_hist'];
    unset($data['id_hist']);
    $idunion = $data['idunion'];
    unset($data['idunion']);
    if($data['id']>0){
      $id=$data['id'];
      //$this->ModeloCatalogos->updateCatalogo($data,'id',$data['id'],'clientes_operaciones');
      //$this->ModeloCatalogos->updateCatalogo($data,'id',$data['id'],'operaciones'); //no deberia actualizar nada
      if($idunion==0){
        $this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$id,"id_perfilamiento"=>$data["id_perfilamiento"],"id_cliente"=>$this->idcliente,"id_clientec"=>$data["id_clientec"],"fecha_reg"=>date('Y-m-d H:i:s')));
      }else{
        $get_cu=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array("id_union"=>$idunion));
        foreach ($get_cu as $k) {
          $this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$id,"id_perfilamiento"=>$k->id_perfilamiento,"id_cliente"=>$this->idcliente,"id_clientec"=>$k->id_clientec,"fecha_reg"=>date('Y-m-d H:i:s')));
        }
      }
    }
    else{
      $data['fecha_reg'] = date('Y-m-d H:i:s');
      //$id=$this->ModeloCatalogos->tabla_inserta('clientes_operaciones',$data);
      $data['id_union'] = $idunion;
      $get_fol = $this->ModeloCliente->get_folio_cli($this->idcliente);
      if(isset($get_fol)){
        $data["folio_cliente"] = $get_fol->folio_cliente + 1;
      }else{
        $data["folio_cliente"] = 1; 
      }
      $id=$this->ModeloCatalogos->tabla_inserta('operaciones',$data);
      if($idunion==0){
        $this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$id,"id_perfilamiento"=>$data["id_perfilamiento"],"id_cliente"=>$this->idcliente,"id_clientec"=>$data["id_clientec"],"fecha_reg"=>date('Y-m-d H:i:s')));
      }else{
        $get_cu=$this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array("id_union"=>$idunion));
        foreach ($get_cu as $k) {
          $this->ModeloCatalogos->tabla_inserta('operacion_cliente',array("id_operacion"=>$id,"id_perfilamiento"=>$k->id_perfilamiento,"id_cliente"=>$this->idcliente,"id_clientec"=>$k->id_clientec,"fecha_reg"=>date('Y-m-d H:i:s')));
        }
      }
    }

    log_message('error', 'id_hist: '.$id_hist);
    if(isset($id_hist)){
      $get_co=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$id));
      foreach ($get_co as $k) {
        $id_hist=0;
        $get_tipop=$this->General_model->get_tableRowC('idtipo_cliente','perfilamiento',array('idperfilamiento'=>$k->id_perfilamiento));
        $tipo=$get_tipop->idtipo_cliente;
        if($tipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
        if($tipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
        if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
        if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
        if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
        if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';} 
        $get_info=$this->General_model->get_tableRow($tabla,array($idt=>$k->id_clientec));
        //log_message('error', 'tipo: '.$tipo);
        if($tipo==1 || $tipo==2){
          $nombre = $get_info->nombre;
          $apellidos = $get_info->apellido_paterno." ".$get_info->apellido_materno;
          $identifica = $get_info->numero_identificacion;
          if($get_info->r_f_c_t!=""){
            $identifica = $get_info->numero_identificacion."|".$get_info->r_f_c_t; 
          }
          if($get_info->curp_t!=""){
            $identifica = $get_info->numero_identificacion."|".$get_info->r_f_c_t."|".$get_info->curp_t; 
          }
        }
        if($tipo==3){
          $nombre = "";
          $apellidos = $get_info->razon_social;
          $identifica = $get_info->clave_registro;
        }
        if($tipo==4){
          $nombre = "";
          $apellidos = $get_info->nombre_persona;
          $identifica = "";
        }
        if($tipo==5){
          $nombre = "";
          $apellidos = $get_info->denominacion;
          $identifica = $get_info->clave_registro;
        }
        if($tipo==6){
          $nombre = "";
          $apellidos = $get_info->denominacion;
          $identifica = $get_info->clave_registro."|".$get_info->numero_referencia;
        }
        if($id_hist==0){
          $array_pb= array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$nombre, "apellidos"=>$apellidos, "identificacion"=>$identifica,"id_perfilamiento"=>$k->id_perfilamiento,"id_clientec"=>$k->id_clientec,"id_operacion"=>$id);
          $hist_cpb=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
          $id_hist = $hist_cpb;
        }
        /*if($id_hist==1){
          $where = array("id_operacion"=>$id,"id_perfilamiento"=>$data['id_perfilamiento'],"id_clientec"=>$data['id_clientec']);
          $get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
          foreach($get_hist as $k){
            $hist_cpb_get = $k->id;
          }
          $where2 = array("id_operacion"=>$idopera,"id_perfilamiento"=>$data['id_perfilamiento'],"id_clientec"=>$data['id_clientec'],"id"=>$hist_cpb_get);
          $array_pb = array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre"=>$data['denominacion'], "apellidos"=>"", "identificacion"=>$identifica,"id_perfilamiento"=>$data['id_perfilamiento'],"id_clientec"=>$data['id_clientec']);
          
          $this->ModeloCatalogos->updateCatalogo2($array_pb,$where2,'historico_consulta_pb');
          $id_hist=$hist_cpb_get;
        }*/    
        //log_message('error', 'id_hist desde consulta a pb: '.$id_hist);
        $respuesta = "";
        if($id_hist>0){
          $url='https://www.prevenciondelavado.com/listas/api/busqueda';
          $Usuario='espinozabe1';
          $Password='B8019775';
          $Apellido=$apellidos;
          $Nombre=$nombre;
          $Identificacion=$identifica;
          $PEPS_otros_paises='S';
          $Incluye_SAT='S';
          $SATxDenominacion='S';

          //$respuesta=$this->consumoAPI($id_hist,urlencode($Apellido),urlencode($Nombre),$Identificacion);
          if($Nombre!="" && $id>0){
            //log_message('error', 'ejecuta consulta de consumoAPI: '.$Nombre);
            $respuesta=$this->consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion);
          }

          /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
          $sesion = curl_init($url);
          // definir tipo de petición a realizar: POST
          curl_setopt ($sesion, CURLOPT_POST, 1); 
          // Le pasamos los parámetros definidos anteriormente
          curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
          // sólo queremos que nos devuelva la respuesta
          curl_setopt($sesion, CURLOPT_HEADER, false); 
          curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
          // ejecutamos la petición
          $respuesta = curl_exec($sesion); 
          $error = curl_error($sesion);
          // cerramos conexión
          curl_close($sesion); 
          //echo $respuesta;
          //echo $error;
          $respuesta = str_replace('[', '', $respuesta);
          $respuesta = str_replace(']', '', $respuesta);
          */
        }
        $this->ModeloCatalogos->updateCatalogo(array("id_operacion"=>$id,"resultado"=>$respuesta),'id',$id_hist,'historico_consulta_pb');
        $arraydata = array('id'=>$id,"hist_cpb"=>$id_hist,"resultpb"=>$respuesta);
        //echo json_encode($arraydata);

      }
    }
    //echo $id;
    //$arraydata = array('id'=>$id,'hist_cpb'=>$hist_cpb);
    //echo json_encode($arraydata);
    /*log_message('error', 'id_hist desde consulta a pb: '.$id_hist);
    $respuesta = "";
    if($id_hist>0){
      $url='https://www.prevenciondelavado.com/listas/api/busqueda';
      $Usuario='espinozabe1';
      $Password='B8019775';
      $Apellido=$apellidos;
      $Nombre=$nombre;
      $Identificacion=$identifica;
      $PEPS_otros_paises='S';
      $Incluye_SAT='S';
      $SATxDenominacion='S';

      $parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
      $sesion = curl_init($url);
      // definir tipo de petición a realizar: POST
      curl_setopt ($sesion, CURLOPT_POST, 1); 
      // Le pasamos los parámetros definidos anteriormente
      curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
      // sólo queremos que nos devuelva la respuesta
      curl_setopt($sesion, CURLOPT_HEADER, false); 
      curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
      // ejecutamos la petición
      $respuesta = curl_exec($sesion); 
      $error = curl_error($sesion);
      // cerramos conexión
      curl_close($sesion); 
      //echo $respuesta;
      //echo $error;
      $respuesta = str_replace('[', '', $respuesta);
      $respuesta = str_replace(']', '', $respuesta);
    }
    $this->ModeloCatalogos->updateCatalogo(array("id_operacion"=>$id),'id',$id_hist,'historico_consulta_pb');
    $arraydata = array('id'=>$id,"hist_cpb"=>$id_hist,"resultpb"=>$respuesta);*/
    echo json_encode($arraydata);
  }

  public function consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion)
  {
    $Usuario='espinozabe1';
    $Password='B8019775';
    //$Usuario='paquinije'; //datos de prueba
    //$Password='389DB30E'; //datos de prueba
    $jsonData = array(
      'usuario'=> $Usuario,
      'clave' => $Password
    );
    //log_message('error', 'token_ws: '.$this->session->userdata("token_ws"));
    if($this->session->userdata("token_ws")=="0"){
      $token=str_replace('"', '', $this->getJWT());
    }else{
      $token=str_replace('"', '', $this->session->userdata("token_ws"));
    }
    //log_message('error', 'token: '.$token);
    $parametros = array(
      'apellido'=> $Apellido,
      'nombre' => $Nombre,
      'identificacion'=> $Identificacion,
      'pepsOtrosPaises' => "S",
      'satXDenominacion'=> "S",
      'documentosSimilares' => "S"
    );

    $array_list=$this->getLista($parametros,$id_hist,$token);
    $arr=json_decode($array_list);

    $status=json_encode($arr->status);
    $res=$arr->resultados;
    $respuesta = str_replace('{"resultados":', '', $res);
    $respuesta = str_replace('[', '', $respuesta);
    $respuesta = str_replace(']', '', $respuesta);
    $respuesta = trim($respuesta, "'\'");
    $respuesta=substr($respuesta, 0, -1);

    //log_message('error', 'status: '.$status);
    if($status=="401"){
      $token=str_replace('"', '', $this->getJWT());
      $array_list=$this->getLista($parametros,$id_hist,$token);
      $arr=json_decode($array_list);
      $status=json_encode($arr->status);
      $res=$arr->resultados;
      $respuesta = str_replace('{"resultados":', '', $res);
      $respuesta = str_replace('[', '', $respuesta);
      $respuesta = str_replace(']', '', $respuesta);
      $respuesta = trim($respuesta, "'\'");
      $respuesta=substr($respuesta, 0, -1);
    }
    return $respuesta;
  }

  public function getJWT(){
    $Usuario='espinozabe1';
    $Password='B8019775';
    $token="";
    //$Usuario='paquinije'; //datos de prueba
    //$Password='389DB30E'; //datos de prueba
    $jsonData = array(
      'usuario'=> $Usuario,
      'clave' => $Password
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json'
    ));
    curl_setopt($ch, CURLOPT_URL, "https://mbalistas.prevenciondelavado.com/Login" );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_POST, 1 );
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
    $result=curl_exec ($ch);
    //echo $result;
    curl_close($ch);
    //log_message('error', 'result: '.$result);
    $data = json_decode($result);
    $token = json_encode($data->token);
    $this->session->set_userdata("token_ws",str_replace('"', '',$token));
    //log_message('error', 'token: '.$token);
    return $token;
  }

  public function getLista($parametros,$id_hist,$token){
    //log_message('error', 'token: '.$token);
    //log_message('error', 'parametros: '.var_dump($parametros));
    $autori=array('Content-Type: application/json',"Authorization: Bearer {$token} ");
    //log_message('error', 'autori: '.var_dump($autori));
    //$autori=array('Authorization: Bearer '.$token.'');
    $url="https://mbalistas.prevenciondelavado.com/listas";
    $ch2 = curl_init();
    //curl_setopt($ch2, CURLOPT_HEADER, false); 
    curl_setopt($ch2, CURLOPT_HTTPHEADER, $autori );
    curl_setopt($ch2, CURLOPT_URL, $url); 
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch2, CURLOPT_POST, 1 );
    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($parametros));
    $data2 = curl_exec($ch2); 
    //echo $data2;
    curl_close($ch2);
    //log_message('error', 'data2: '.$data2);

    $ddata = json_decode($data2);
    //log_message('error', 'sizeof array_emp: '.sizeof($array_emp));
    $status="200";
    if(strlen($data2)==17){
      $status = "200";
    }else if(strlen($data2)>17 && strlen($data2)<=162){
      $status = json_encode($ddata->status);
    }
    return json_encode(array("status"=>$status,"resultados"=>$data2));
    if($status=="200" && strlen($data2)>162){ //ok --desde aca se debe editar historico
      //$this->ModeloCatalogos->updateCatalogo2(array("resultado"=>$data2),array("id"=>$id_hist),'historico_consulta_pb');//guarda el resultado del webservice
    }
  }

  public function submit_operacionDB(){
    $data = $this->input->post();
    $tipo_bene = $this->input->post('tipo_bene');
    $id=$data['id'];
    unset($data['id']);
    $id_ob=$data['id_ob'];
    unset($data['id_ob']);
    $id_perfilamiento=$data['id_perfilamiento'];
    unset($data['id_perfilamiento']);
    $id_cliente=$data['id_cliente'];
    unset($data['id_cliente']);
    $id_bene=$data['id_duenio_bene'];
    if($tipo_bene=="fisica")
      $tipo_bene="1";
    if($tipo_bene=="moralmoral")
      $tipo_bene="2";
    if($tipo_bene=="fideicomiso")
      $tipo_bene="3";
    //$this->ModeloCatalogos->updateCatalogo($data,'id',$id,'clientes_operaciones'); //acá ya deberia ser la nueva tabla de operacion_benes
    $array = array("id_perfilamiento"=>$id_perfilamiento,"id_operacion"=>$id,"id_duenio_bene"=>$id_bene,"tipo_bene"=>$tipo_bene,"fecha_reg"=>date("Y-m-d H:i:s"));
    if($id_ob==0){
      $this->ModeloCatalogos->tabla_inserta('operacion_beneficiario',$array);
    }else{
      $this->ModeloCatalogos->updateCatalogo($data,'id',$id_ob,'operacion_beneficiario');  
    }
    unset($data['id_duenio_bene']);
    $nombre = "";
    $apellidos = "";
    $identifica = "";
    
    //log_message('error', 'id_bene: '.$id_bene);
    //log_message('error', 'tipo_bene: '.$tipo_bene);
    if($id_bene>0){
      if($tipo_bene==1){
        $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$id_bene);
      }else if($tipo_bene==2){
        $results=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$id_bene);
      }else if($tipo_bene==3){
        $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$id_bene);
      }
      foreach ($results as $k) {
        if($tipo_bene==1){
          $nombre = $k->nombre;
          $apellidos = $k->apellido_paterno." ".$k->apellido_materno;
          $identifica = $k->numero_identificacion."|".$k->r_f_c_d."|".$k->curp_d;
        }
        if($tipo_bene==2 || $tipo_bene==3){
          $nombre = "";
          $apellidos = $k->razon_social;
          $identifica = $k->rfc;
        }
      }
    }else{
      $nombre = "";
      $apellidos = "";
      $identifica = "";
    }
    $id_hist=0;
    $array_pb=array("fecha_consulta"=>date("Y-m-d H:i:s"), "nombre_db"=>$nombre, "apellidos_db"=>$apellidos, "identifica_db"=>$identifica,"tipo_bene"=>$tipo_bene,"id_bene"=>$id_bene,"id_operacion"=>$id,"id_perfilamiento"=>$id_perfilamiento,"id_clientec"=>$id_cliente);
    $where = array("id_operacion"=>$id,"id_perfilamiento"=>$id_perfilamiento,"id_clientec"=>$id_cliente);
    /*log_message('error', ': id_perfilamiento'.$id_perfilamiento);
    log_message('error', ': id_cliente'.$id_cliente);
    log_message('error', ': id_operacion'.$id);*/

    /*$get_hist=$this->ModeloCatalogos->getselectwherestatus('*','historico_consulta_pb',$where);
    foreach($get_hist as $k){
      $hist_cpb_get = $k->id;
    }      
    $this->ModeloCatalogos->updateCatalogo2($array_pb,array("id"=>$hist_cpb_get),'historico_consulta_pb');
    $id_hist=$hist_cpb_get;*/

    //AGREGADO Y COMENTADO LO DE ARRIBA POR LOS MULTIPLES BENEFICIARIOS EN LAS OPERACIONES
    /*$id_hist=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
    $hist_cpb_get=$id_hist;*/
    /* ******************************************/ 

    //log_message('error', 'id_hist desde consulta a pbDB: '.$id_hist);
    $respuesta = "";
    if($id_bene>0){
      //AGREGADO Y COMENTADO LO DE ARRIBA POR LOS MULTIPLES BENEFICIARIOS EN LAS OPERACIONES
      $id_hist=$this->ModeloCatalogos->tabla_inserta('historico_consulta_pb',$array_pb);
      $hist_cpb_get=$id_hist;

      $url='https://www.prevenciondelavado.com/listas/api/busqueda';
      $Usuario='espinozabe1';
      $Password='B8019775';
      $Apellido=$apellidos;
      $Nombre=$nombre;
      $Identificacion=$identifica;
      $PEPS_otros_paises='S';
      $Incluye_SAT='S';
      $SATxDenominacion='S';

      //$respuesta=$this->consumoAPI($id_hist,urlencode($Apellido),urlencode($Nombre),$Identificacion);
      if($Nombre!="" && $id>0){
        //log_message('error', 'ejecuta consulta de consumoAPI beneficario: '.$Nombre);
        $respuesta=$this->consumoAPI($id_hist,$Apellido,$Nombre,$Identificacion);
      }

      /*$parametros_post='Usuario='.$Usuario.'&Password='.$Password.'&Apellido='.urlencode($Apellido).'&Nombre='.urlencode($Nombre).'&Identificacion='.$Identificacion.'&PEPS_otros_paises='.$PEPS_otros_paises.'&Incluye_SAT='.$Incluye_SAT.'&SATxDenominacion='.$SATxDenominacion;
      $sesion = curl_init($url);
      // definir tipo de petición a realizar: POST
      curl_setopt ($sesion, CURLOPT_POST, 1); 
      // Le pasamos los parámetros definidos anteriormente
      curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post); 
      // sólo queremos que nos devuelva la respuesta
      curl_setopt($sesion, CURLOPT_HEADER, false); 
      curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
      // ejecutamos la petición
      $respuesta = curl_exec($sesion); 
      $error = curl_error($sesion);
      // cerramos conexión
      curl_close($sesion); 
      //echo $respuesta;
      //echo $error;
      $respuesta = str_replace('[', '', $respuesta);
      $respuesta = str_replace(']', '', $respuesta);
      log_message('error', 'respuesta: '.$respuesta);
      log_message('error', 'Nombre: '.$nombre);
      log_message('error', 'apellidos: '.$apellidos);
      log_message('error', 'identifica: '.$identifica);
      */
      $this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>$respuesta),array("id"=>$hist_cpb_get),'historico_consulta_pb');//guarda el resultado del webservice
      
    }
    if($id_bene==0){ //no debe actualizar porque no busca a declarado sin dueño beneficiario
      //$this->ModeloCatalogos->updateCatalogo2(array("resultado_bene"=>""),array("id"=>$hist_cpb_get),'historico_consulta_pb');
    }

    $arraydata = array('id'=>$id,'hist_cpb'=>$id_hist,"resultpb"=>$respuesta);
    echo json_encode($arraydata);
  }

  public function eliminarOperacion(){
    $id = $this->input->post('id');
    //$this->ModeloCatalogos->updateCatalogo2(array("activo"=>0),array('id'=>$id),'clientes_operaciones');
    $this->ModeloCatalogos->updateCatalogo2(array("activo"=>0),array('id'=>$id),'operaciones');
  }

  public function records_operaciones(){
    /*$params = $this->input->post();
    $getdata = $this->ModeloCliente->get_operaciones($params);
    $totaldata= $this->ModeloCliente->get_total_operaciones($params); 
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);*/
    $datas = $this->ModeloCliente->operaciones_vista($this->idcliente);
    $json_data = array("data" => $datas);
    echo json_encode($json_data);
  }

  public function proceso_operacion($ido,$idp,$idcc){
    $data['id'] = $ido;
    $data['idperfilamiento'] = $idp;
    $data['idcliente'] = $this->idcliente;
    $data['idclientec'] = $idcc;

    $data['tipo_cliente']=$this->General_model->get_tableRowC('idtipo_cliente','perfilamiento',array('idperfilamiento'=>$idp));
    $tipo=$data['tipo_cliente']->idtipo_cliente;

    if($tipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}  
    $data['info']=$this->General_model->get_tableRow($tabla,array($idt=>$idcc));
    
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/detalles_proceso',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/detalles_proceso_js');
  }

  public function procesoInicial($ido=0){
    $data["id_operacion"]=$ido;
    $data["cont_db"]=0;
    $data["grado"]=[];
    $data["id_docs_sinb"]=0;
    $data["valida_sin_bene"]=0;
    $data["valida_cli"]=0;
    $data["valida_bene"]=0;
    $data["acuse"]="";
    $data["avanza"]=1;
    $data["tipo_bene"]=0;

    $data["cont_diag"]=0;
    $data["cont_gmalto"]=0;
    $data["cont_ca"]=0;
    $data["cont_alto"]=0;
    $data["cont_co"] = 0;

    $data["idtipo_cliente"]=0;
    $data["autorizado"]=0;
    if($ido>0){
      $cont_db=0;
      $get_ou=$this->General_model->get_tableRow("operaciones",array("id"=>$ido));
      $get_tc=$this->General_model->get_tableRow("perfilamiento",array("idperfilamiento"=>$get_ou->id_perfilamiento));
      $data["id_union"]=$get_ou->id_union;
      $data["idtipo_cliente"]=$get_tc->idtipo_cliente;
      if($get_tc->idtipo_cliente!="6"){ //se requiere asignar dueño y expediente de dueño beneficiario
        $get_db=$this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$ido,"activo"=>1));
        foreach ($get_db as $key) {
          $cont_db++;
          $data["cont_db"]=1;
          $data["tipo_bene"]=$key->tipo_bene;
          if($key->tipo_bene==0){ //es declarado sin sueño beneficiario
            $data["valida_bene"]=1;
            $get_doc=$this->General_model->get_tableRow("doc_sin_bene",array("id_operacion"=>$ido,'id_perfilamiento'=>$key->id_perfilamiento));
            if(isset($get_doc)){
              $data["id_docs_sinb"]=$get_doc->id;
              $data["acuse"]=$get_doc->acuse_sin_doc;
              $data["valida_sin_bene"]=$get_doc->validado;
              //log_message('error', 'valida_sin_bene : '.$get_doc->validado);
            }
          }else if($key->tipo_bene>0){
            $data["id_docs_sinb"]=1;
            $data["valida_sin_bene"]=1;
            $data["valida_bene"]=1;
          }
        }
      }
      
      $get_co=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
      foreach ($get_co as $key) {
        $data["cont_co"]++;
        $cont_ge=0; $cont_gmalto=0; $cont_alto=0; $cont_diag=0; $cont_ca=0; $id_act_calc=0;
        $get_gra=$this->ModeloTransaccion->getGradoCli($key->id_perfilamiento,$ido);
        foreach ($get_gra as $key2) {
          $cont_ge++;
          $id_act_calc = $key2->id_actividad;
          if($key2->grado>0 && $key2->grado!=""){
            $data["grado"][$cont_ge]=$key2->grado;
            //log_message('error', 'cont_ge : '.$cont_ge);
          }
          if($key2->grado>=1.8 && $key2->grado!=""){
            $cont_gmalto++;
          }
          if($key2->grado>2.0 && $key2->grado!=""){
            $cont_alto++;
          }
          
          if($key2->grado>=1.8){ //desde grado medio 
            $get_da=$this->ModeloCatalogos->getselectwherestatus("id","diagnostico_alertas",array("id_operacion"=>$ido,"id_perfilamiento"=>$key->id_perfilamiento));
            foreach ($get_da as $da) {
              $cont_diag++;
            }
          }
                    
          if($key2->grado>2.0){ //desde grado alto  -- personas fisicas
            $get_ca=$this->ModeloCatalogos->getselectwherestatus("id","cuestionario_ampliado",array("id_operacion"=>$ido,"id_perfilamiento"=>$key->id_perfilamiento));
            foreach ($get_ca as $ca) {
              $cont_ca++;
            }
          }
          if($key2->grado>2.0){ //desde grado alto  -- personas morales
            $get_ca=$this->ModeloCatalogos->getselectwherestatus("id","cuestionario_ampliado_moral",array("id_operacion"=>$ido,"id_perfilamiento"=>$key->id_perfilamiento));
            foreach ($get_ca as $ca) {
              $cont_ca++;
            }
          }
          if($key2->grado>2.0){ //desde grado alto  -- fideicomisos
            $get_ca=$this->ModeloCatalogos->getselectwherestatus("id","cuestionario_ampliado_fide",array("id_operacion"=>$ido,"id_perfilamiento"=>$key->id_perfilamiento));
            foreach ($get_ca as $ca) {
              $cont_ca++;
            }
          }
          if($key2->grado>2.0){ //desde grado alto  -- embajadas
            $get_ca=$this->ModeloCatalogos->getselectwherestatus("id","cuestionario_ampliado_embajada",array("id_operacion"=>$ido,"id_perfilamiento"=>$key->id_perfilamiento));
            foreach ($get_ca as $ca) {
              $cont_ca++;
            }
          }

          if($key2->grado>2.0){ //desde grado alto  personas PEP
            $get_ca=$this->ModeloCatalogos->getselectwherestatus("id","cuestionario_ampliado_pep",array("id_operacion"=>$ido,"id_perfilamiento"=>$key->id_perfilamiento));
            foreach ($get_ca as $ca) {
              $cont_ca++;
            }
          }
        }

        /* ********************** */
        $result = $this->ModeloCatalogos->getselectwherestatus('*','diagnostico_alertas',array('id_operacion'=>$ido,"id_perfilamiento"=>$key->id_perfilamiento));
        $data["calificacion"]=0;
        foreach ($result as $x) {
          $data['calificacion']=$x->calificacion;
        }
        /////////////////////////////
      }
      //log_message('error', 'cont_diag : '.$cont_diag);
      //log_message('error', 'cont_gmalto : '.$cont_gmalto);
      $data["cont_diag"]=$cont_diag;
      $data["cont_gmalto"]=$cont_gmalto;
      $data["cont_ca"]=$cont_ca;
      $data["cont_alto"]=$cont_alto;
      $data["id_act_calc"]=$id_act_calc;
      /*$get_val=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
      foreach ($get_val as $k) {
        $get_vc=$this->ModeloCatalogos->getselectwherestatus("*","docs_cliente",array("id_perfilamiento"=>$k->id_perfilamiento));
        foreach ($get_vc as $kc) {  
          //log_message('error', 'validado cliente: '.$kc->validado);
          if($kc->validado==1){
            $data["valida_cli"]=1; 
          }else{
            $data["valida_cli"]=0;
          }
          //log_message('error', 'valida_cli: '.$data["valida_cli"]);
        }
      }

      $get_val=$this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$ido,"activo"=>1));
      foreach ($get_val as $k) {
        $get_vb=$this->ModeloCatalogos->getselectwherestatus("*","docs_beneficiario",array("id_beneficiario"=>$k->id_duenio_bene,"tipo_bene"=>$k->tipo_bene));
        foreach ($get_vb as $kb) { 
          //log_message('error', 'validado: '.$kb->validado);
          if($kb->validado==1){
            $data["valida_bene"]=1; 
          }else{
            $data["valida_bene"]=0;
          }
          //log_message('error', 'valida_bene: '.$data["valida_bene"]);
        }
      }*/

      $conf_alert=$this->General_model->get_tableRow("config_alerta",array("id_cliente"=>$this->idcliente));
      if(isset($conf_alert))
        $data["avanza"]=$conf_alert->avance;
      else
        $data["avanza"]=1;
      $get_auto=$this->General_model->get_tableRow("autoriza_operacion",array("id_operacion"=>$ido));
      if(isset($get_auto))
        $data["autorizado"]=$get_auto->id;
      else
        $data["autorizado"]=0;      
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/operacionInicio',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/operacion_inicio_js');
  }

  public function validaUSerAutoriza(){
    $user = $this->input->post("user");
    $pass = $this->input->post("pass");
    
    //validar si el usuario y la contraseña es correcta
    $count = 0;
    $respuesta = $this->Login_model->login($user);
    $contrasena='';
    $permiso_avanza=0;
    $usuarioid=0;
    foreach ($respuesta as $item) {
      $contrasena =$item->contrasena;
      $usuarioid=$item->UsuarioID;
      $permiso_avanza=$item->permiso_avanza;
    }
    if($permiso_avanza==1){ //tiene permiso para autorizar
      $verificar = password_verify($pass,$contrasena);
      if ($verificar) 
      {
        $count=1;
      }else{
        $count=0;
      }
    }else{ //no tiene permiso para autorizar
      $count=2;
    }

    $arraydata = array('usuarioid'=>$usuarioid,"count"=>$count);
    echo json_encode($arraydata);
  }

  public function cambiaEstatusAutoriza(){
    $motivo = $this->input->post("motivo");
    $ido = $this->input->post("ido");
    $idu = $this->input->post("idu");
    $where = array("id_operacion"=>$ido,"id_user_autoriza"=>$idu,"motivo"=>$motivo,"fecha_reg"=>date("Y-m-d H:i:s"));
    $this->ModeloCatalogos->tabla_inserta('autoriza_operacion',$where);
  }

  public function validaAutoriza(){
    $ido = $this->input->post("ido");
    $verv=$this->ModeloCatalogos->getselectwherestatus("*","autoriza_operacion",array("id_operacion"=>$ido));
    $id=0;
    $motivo="";
    foreach ($verv as $key) {
      $id=$key->id;
      $motivo=$key->motivo;
    }
    $arraydata = array('id'=>$id,"motivo"=>$motivo);
    echo json_encode($arraydata);
  }

  public function existente($ido=0){
    $data["id_operacion"]=$ido;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/operacion_clientex',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/operacion_clientex_js');
  }

  public function asignarCliente($ido){
    $data["id_operacion"]=$ido;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/operacion_clientemas',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/operacion_clientex_js');
  }

  public function clientesOperacion($ido){
    $data["info"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
    $data["info_db"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$ido,"activo"=>1));
    $cont_db=0;
    foreach ($data["info_db"] as $key) {
      $cont_db++;
    } 
    $data["cont_db"]=$cont_db;
    $get_id=$this->General_model->get_tableRowC('id_union','operaciones',array('id'=>$ido));
    $data["id_operacion"]=$ido;
    $data["id_union"]=$get_id->id_union;
    $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$ido));
    $data["folio"]=$get_fol->folio_cliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/opera_clie_asigna',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/opera_bene_asignajs');
  }

  public function clientesOperacionBenes($ido){
    $data["info"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
    $data["info_db"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$ido,"activo"=>1));
    $cont_db=0;
    foreach ($data["info_db"] as $key) {
      $cont_db++;
      $data["tipo_bene_ctrl"]=$key->tipo_bene;
    } 
    $data["cont_db"]=$cont_db;
    $get_id=$this->General_model->get_tableRowC('id_union','operaciones',array('id'=>$ido));
    $data["id_operacion"]=$ido;
    $data["id_union"]=$get_id->id_union;
    $nombre="";
    if($get_id->id_union>0){
      $get_clis = $this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array("id_union"=>$get_id->id_union,"activo"=>1));
      foreach ($get_clis as $gc) {
        $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$gc->id_perfilamiento));
        //log_message('error', 'gc->id_perfilamiento: '.$gc->id_perfilamiento);
        foreach ($get_pp as $gc2) {
          //log_message('error', 'gc2->idtipo_cliente: '.$gc2->idtipo_cliente);
          $tipoccon = $gc2->idtipo_cliente;
          if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
          if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
          if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
          if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
          if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
          if($tipoccon==6) $tabla = "tipo_cliente_f";
          //echo "<br>tabla: ".$tabla;
          //log_message('error', 'tabla: '.$tabla);
          
          $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$gc2->idperfilamiento));
          foreach ($get_per as $g2) {
            if($tipoccon==1 || $tipoccon==2) $nombre .= $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno." / ";
            if($tipoccon==3) $nombre .= $g2->razon_social." / ";
            if($tipoccon==4) $nombre .= $g2->nombre_persona." / ";
            if($tipoccon==5 || $tipoccon==6) $nombre .= $g2->denominacion." / ";
            
          }
          $data["nombre_mancomunado"] = $nombre;
        } 
      }
    }
    $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$ido));
    $data["folio"]=$get_fol->folio_cliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/opera_clie_asigna_bene',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/opera_bene_asignajs');
  }

  public function asignarBene($ido,$idcc,$idp,$tipoc,$idunion=0){
    $data["id_operacion"]=$ido;
    $data["id_clientec"]=$idcc;
    $data["id_perfilamiento"]=$idp;
    $data["tipo_cliente"]=$tipoc;
    $data["id_cliente"]=$this->idcliente;
    $data["id_union"]=$idunion;
    $data['tipo_cliente_info']=$this->General_model->get_tableRowC('idtipo_cliente','perfilamiento',array('idperfilamiento'=>$idp));
    $tipo=$data['tipo_cliente_info']->idtipo_cliente;
    if($tipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}  
    $data['info']=$this->General_model->get_tableRow($tabla,array($idt=>$idcc));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/operacion_benex',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/operacion_clientex_js');
  }

  public function getNames(){
    $tipo = $this->input->post("tipo");
    $idcc = $this->input->post("idcc");
    if($tipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}  
    $info=$this->General_model->get_tableRow($tabla,array($idt=>$idcc));
    if($tipo==1 || $tipo==2)
        $nombre = $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno;
    if($tipo==3 || $tipo==4 || $tipo==5 || $tipo==6)
      $nombre = $info->nombre_g." ".$info->apellido_paterno_g." ".$info->apellido_materno_g;

    echo $nombre;
  }

  public function generarPDF_SDB($idcc,$tipo,$ido,$fecha){

    if($tipo==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
    if($tipo==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
    if($tipo==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
    if($tipo==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
    if($tipo==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
    if($tipo==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}  
    $info=$this->General_model->get_tableRow($tabla,array($idt=>$idcc));
    /*if($tipo==1 || $tipo==2)
        $nombre = $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno;
    if($tipo==3 || $tipo==4 || $tipo==5 || $tipo==6){
      $nombre = $info->nombre_g." ".$info->apellido_paterno_g." ".$info->apellido_materno_g;
      $denominacion = $info->denominacion;
    }

    $data["nombre"]=$nombre;
    $data["denominacion"]=$denominacion;*/
    $data["id_operacion"]=$ido;
    $data["tipo"]=$tipo;
    $data["info"]=$info;
    $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$ido));
    $data["folio"]=$get_fol->folio_cliente;
    $data["fecha"]=$fecha;
    $this->load->view('Reportes/formato_sindb',$data);
  }

  public function getNameUnion(){
    $ido = $this->input->post("ido");
    $nombre="";
    $info = $this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
    foreach ($info as $gp) {
      $idpc=$gp->id_perfilamiento;
      $get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>$idpc));//obtiene datos del perfilamiento
      if($get->idtipo_cliente==1){ 
          $tabla = "tipo_cliente_p_f_m";
          $gg=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
          $idcc=$gg->idtipo_cliente_p_f_m;
      }
      if($get->idtipo_cliente==2){ 
          $tabla = "tipo_cliente_p_f_e";
          $gg=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
          $idcc=$gg->idtipo_cliente_p_f_e;
      }
      if($get->idtipo_cliente==3){ 
          $tabla = "tipo_cliente_p_m_m_e";
          $gg=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
          $idcc=$gg->idtipo_cliente_p_m_m_e;
      }
      if($get->idtipo_cliente==4){ 
          $tabla = "tipo_cliente_p_m_m_d";
          $gg=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
          $idcc=$gg->idtipo_cliente_p_m_m_d;
      }
      if($get->idtipo_cliente==5){ 
          $tabla = "tipo_cliente_e_c_o_i";
          $gg=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
          $idcc=$gg->idtipo_cliente_e_c_o_i;
      }
      if($get->idtipo_cliente==6){ 
          $tabla = "tipo_cliente_f";
          $gg=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>$idpc));
          $idcc=$gg->idtipo_cliente_f;
      }

      if($get->idtipo_cliente==1 || $get->idtipo_cliente==2)
          $nombre .= $gg->nombre." ".$gg->apellido_paterno." ".$gg->apellido_materno. " en unión con ";
      if($get->idtipo_cliente==3 || $get->idtipo_cliente==4 || $get->idtipo_cliente==5 || $get->idtipo_cliente==6)
        $nombre .= $gg->nombre_g." ".$gg->apellido_paterno_g." ".$gg->apellido_materno_g;
    }
    echo $nombre;
  }
  

  public function benesOperacion($ido){
    //$data["info"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$ido));
    $data["info"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
    $data["id_operacion"]=$ido;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/opera_bene_asigna',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/opera_bene_asignajs');
  }

  public function beneficiario_existente(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/operacion_clientex');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/operacion_clientex_js');
  }

  public function resultadosBusqueda($ido){
    $data["infob"] = $this->ModeloCatalogos->get_result_bo($ido);
    $data["infoc"] = $this->ModeloCatalogos->get_result_co($ido);
    $data["id_operacion"]=$ido;
    $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$ido));
    $data["folio"]=$get_fol->folio_cliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/opera_result_pb',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/operacion_resultado_js');
  }

  public function resultado_clientes_data(){
    $ido = $this->input->post("ido");
    $datas = $this->ModeloCatalogos->get_result_co2($ido);
    //$json_data = array("data" => $datas);
    //echo json_encode(array("data" => $datas));

    $deno = array();
    /*
    foreach ($datas as $k) {
      $cosas = explode(",", $k->resultado);
      for($i=0; $i<$k->total_comas; $i++){
        //$Denominacion = $k->resultado[$i];
        //log_message('error', 'Denominacion: '.$Denominacion);
        $deno[] = array("Denominacion"=>$cosas[$i],"Identificacion"=>$cosas[$i],"Id_Tributaria"=>$cosas[$i],"Otra_Identificacion"=>$cosas[$i],"Cargo"=>$cosas[$i],"Lugar_Trabajo"=>$cosas[$i],"Direccion"=>$cosas[$i],"Enlace"=>$cosas[$i],"Tipo"=>$cosas[$i],"Sub_Tipo"=>$cosas[$i],"Estado"=>$cosas[$i],"Lista"=>$cosas[$i],"Pais_Lista"=>$cosas[$i],"Cod_Individuo"=>$cosas[$i],"Exactitud_Denominacion"=>$cosas[$i],"Exactitud_Identificacion"=>$cosas[$i]);
      }
    }
    */
    $con_ressult=0;
    foreach ($datas as $item) {
      $resultado='['.$item->resultado.']';
      //log_message('error', '$resultado: '.$resultado);
      if($item->resultado!=""){
        
        $resultadoo=json_decode($resultado);
        foreach ($resultadoo as $itemarray) {
          $con_ressult++;
          if(isset($itemarray->denominacion) && $itemarray->denominacion!=""){
            //log_message('error', '$resultado: '.$itemarray->Denominacion);
            $deno['data'][] = array(
                      "Denominacion"=>$itemarray->denominacion,
                      "Identificacion"=>$itemarray->identificacion,
                      "Id_Tributaria"=>$itemarray->idTributaria,
                      "Otra_Identificacion"=>$itemarray->otraIdentificacion,
                      "Cargo"=>$itemarray->cargo,
                      "Lugar_Trabajo"=>$itemarray->lugarTrabajo,
                      "Direccion"=>$itemarray->direccion,
                      "Enlace"=>$itemarray->enlace,
                      "Tipo"=>$itemarray->tipo,
                      "Sub_Tipo"=>$itemarray->subTipo,
                      "Estado"=>$itemarray->estado,
                      "Lista"=>$itemarray->lista,
                      "Pais_Lista"=>$itemarray->paisLista,
                      "Cod_Individuo"=>$itemarray->codigoIndividuo,
                      "Exactitud_Denominacion"=>$itemarray->exactitudDenominacion,
                      "Exactitud_Identificacion"=>$itemarray->exactitudIdentificacion,
                      "id_perfilamiento"=>$item->id_perfilamiento,
                    );
          }/*else if(isset($itemarray->Status) && $itemarray->Status=="OK" || $resultado==""){
            //$deno['data'][] = array();
            if($con_ressult==1){
              log_message('error', 'con_ressult: '.$con_ressult);
              log_message('error', 'Status: '.$itemarray->Status);
              $deno['data'][] = array(
                        "Denominacion"=>"",
                        "Identificacion"=>"",
                        "Id_Tributaria"=>"",
                        "Otra_Identificacion"=>"",
                        "Cargo"=>"",
                        "Lugar_Trabajo"=>"",
                        "Direccion"=>"",
                        "Enlace"=>"",
                        "Tipo"=>"",
                        "Sub_Tipo"=>"",
                        "Estado"=>"",
                        "Lista"=>"",
                        "Pais_Lista"=>"",
                        "Cod_Individuo"=>"",
                        "Exactitud_Denominacion"=>"",
                        "Exactitud_Identificacion"=>"",
                        "id_perfilamiento"=>""
                );
            }
          }*/
        }
      }
      
    }
    echo json_encode($deno);
  }

  public function resultado_beneficiario_data(){
    $ido = $this->input->post("ido");
    $datas = $this->ModeloCatalogos->get_result_bo2($ido);
    $deno = array();
    foreach ($datas as $item) {
      $resultado='['.$item->resultado_bene.']';
      //log_message('error', '$resultado: '.$resultado);
      if($item->resultado_bene!=""){
        //log_message('error', 'resultado: '.$resultado);
        $resultadoo=json_decode($resultado);
        //log_message('error', 'resultadoo: '.$resultadoo);
        foreach ($resultadoo as $itemarray) {
          //log_message('error', '$resultado: '.$itemarray->Denominacion);
          if(isset($itemarray->denominacion) && $itemarray->denominacion!=""){
            $deno['data'][] = array(
                      "Denominacion"=>$itemarray->denominacion,
                      "Identificacion"=>$itemarray->identificacion,
                      "Id_Tributaria"=>$itemarray->idTributaria,
                      "Otra_Identificacion"=>$itemarray->otraIdentificacion,
                      "Cargo"=>$itemarray->cargo,
                      "Lugar_Trabajo"=>$itemarray->lugarTrabajo,
                      "Direccion"=>$itemarray->direccion,
                      "Enlace"=>$itemarray->enlace,
                      "Tipo"=>$itemarray->tipo,
                      "Sub_Tipo"=>$itemarray->subTipo,
                      "Estado"=>$itemarray->estado,
                      "Lista"=>$itemarray->lista,
                      "Pais_Lista"=>$itemarray->paisLista,
                      "Cod_Individuo"=>$itemarray->codigoIndividuo,
                      "Exactitud_Denominacion"=>$itemarray->exactitudDenominacion,
                      "Exactitud_Identificacion"=>$itemarray->exactitudIdentificacion,
                      //"id_perfilamiento"=>$item->id_perfilamiento,
                    );
          }/*else{
            //$deno['data'][]=array();
            $deno['data'][]=array(
                      "Denominacion"=>"",
                      "Identificacion"=>"",
                      "Id_Tributaria"=>"",
                      "Otra_Identificacion"=>"",
                      "Cargo"=>"",
                      "Lugar_Trabajo"=>"",
                      "Direccion"=>"",
                      "Enlace"=>"",
                      "Tipo"=>"",
                      "Sub_Tipo"=>"",
                      "Estado"=>"",
                      "Lista"=>"",
                      "Pais_Lista"=>"",
                      "Cod_Individuo"=>"",
                      "Exactitud_Denominacion"=>"",
                      "Exactitud_Identificacion"=>"",
                      //"id_perfilamiento"=>$item->id_perfilamiento,
                    );
          }*/
        }
      }
        
    }
    echo json_encode($deno);
  }

  public function gradoRiesgo($ido,$desde_cal=0,$idp=0,$desdec=0){
    $data["info"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
    $data["config"]=$this->General_model->get_tableRow("config_alerta",array("id_cliente"=>$this->idcliente));
    $data["id_operacion"]=$ido;
    $data["desde_cal"]=$desde_cal;

    //foreach ($data["info"] as $k) {
    if($desde_cal==1){
      $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$idp));
      foreach ($get_pp as $g) {
        $tipoccon = $g->idtipo_cliente;
        //echo "<br>tipoccon: ".$tipoccon;
        if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
        if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
        if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
        if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
        if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
        if($tipoccon==6) $tabla = "tipo_cliente_f";
        //echo "<br>tabla: ".$tabla;
        $act_clic=0;
        $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
        foreach ($get_per as $g2) {
          //echo "<br>k->id_cliente: ".$k->id_cliente;
          if($tipoccon==1){ $idcc = $g2->idtipo_cliente_p_f_m; $tipocc=1; $act_clic=$g2->activida_o_giro;}
          if($tipoccon==2){ $idcc = $g2->idtipo_cliente_p_f_e; $tipocc=1; $act_clic=$g2->actividad_o_giro;}
          if($tipoccon==3){ $idcc = $g2->idtipo_cliente_p_m_m_e; $tipocc=2;}
          if($tipoccon==4){ $idcc = $g2->idtipo_cliente_p_m_m_d; $tipocc=2; $act_clic=$g2->giro_mercantil;}
          if($tipoccon==5){ $idcc = $g2->idtipo_cliente_e_c_o_i; $tipocc=0; $act_clic=$g2->giro_mercantil;}
          if($tipoccon==6){ $idcc = $g2->idtipo_cliente_f; $tipocc=3;}
          if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
          if($tipoccon==3) $nombre = $g2->razon_social;
          if($tipoccon==4) $nombre = $g2->nombre_persona;
          if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
          //$btn_ca="";
          $get_gr=$this->General_model->get_tableRowC('grado,id,id_actividad,grado','grado_riesgo_actividad',array('id_operacion'=>$ido,"id_perfilamiento"=>$g->idperfilamiento));
          if(isset($get_gr->grado) && $get_gr->grado>2.0){ //AQIU DEBE IR QUE TIPO DE CLIENTE FUE -- 
            $get_gr=$this->General_model->get_tableRowC('idtipo_cliente,id','grado_riesgo_actividad',array('id_operacion'=>$ido,"id_perfilamiento"=>$g->idperfilamiento));
            if(isset($get_gr)){ //ESTE ES PARA CLIENTES NO PEPS - agregar validacion que sea tipo no pep
              if($get_gr->idtipo_cliente==5){
                //$btn_ca.="<a href='".base_url()."Clientes_cliente/cuestionario_pep/".$ido."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i></a>";
                redirect(base_url()."Clientes_cliente/cuestionario_pep/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic);
              //// validar si es anexo 12a 0 126
              }else if($tipoccon==1 || $tipoccon==2){
                $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliado/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i></a>";
                redirect(base_url()."Clientes_cliente/cuestionarioAmpliado/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic);
              }
              //else if($tipoccon==3 || $tipoccon==4){ //morales
              else if($tipoccon==3){ //morales
                $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliadoMoral/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i></a>";
                redirect(base_url()."Clientes_cliente/cuestionarioAmpliadoMoral/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic);
              }
              else if($tipoccon==6){ //fideicomiso
                $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliadoFide/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i></a>";
                redirect(base_url()."Clientes_cliente/cuestionarioAmpliadoFide/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic);
              }
              else if($tipoccon==4 || $tipoccon==5){ //embajadas
                $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliadoEmbajada/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i></a>";
                redirect(base_url()."Clientes_cliente/cuestionarioAmpliadoEmbajada/".$idcc."/".$idp."/".$tipoccon."/".$ido."/".$get_gr->id."/".$tipocc."/".$act_clic);
              }
              //$btn_ca.=" <a href='".base_url()."Clientes_cliente/diagnostico_alertas/".$ido."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'>Diagnóstico de alertas</a>";
              //redirect('".base_url()."Clientes_cliente/diagnostico_alertas/".$ido."');
            }
          }
        }
      }
    }else if($desde_cal==2){
      $get_gr=$this->General_model->get_tableRowC('grado,id,id_actividad,grado','grado_riesgo_actividad',array('id_operacion'=>$ido,"id_perfilamiento"=>$idp));
      if(isset($get_gr->grado) && $get_gr->grado>2.0){
        redirect(base_url()."Clientes_cliente/diagnostico_alertas/".$ido."/".$idp);
      }
    }
    $get_auto=$this->General_model->get_tableRow("autoriza_operacion",array("id_operacion"=>$ido));
    if(isset($get_auto))
      $data["autorizado"]=$get_auto->id;
    else
      $data["autorizado"]=0;
    //}

    $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$ido));
    $data["folio"]=$get_fol->folio_cliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/calcula_grado',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/calcula_gradojs');
  }

  public function documentosCliente($ido){
    //traer id de union de operacion
    $getu=$this->General_model->get_tableRow('operaciones',array("id"=>$ido));
    $data["info"] = $this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$ido,"activo"=>1));
    $data["id_operacion"]=$ido;
    $data["id_union"]=$getu->id_union;
    $data["folio"]=$getu->folio_cliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/opera_exp_cli',$data);
    $this->load->view('templates/footer');
  }

  public function documentosBenes($ido){
    $data["infob"] = $this->ModeloCatalogos->get_benes_opera_detalles($ido);
    $data["id_operacion"]=$ido;
    $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$ido));
    $data["folio"]=$get_fol->folio_cliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/opera_exp_bene',$data);
    $this->load->view('templates/footer');
    //$this->load->view('catalogos/operaciones/opera_clien_asignajs');
  }

  public function Transaccion($ido){
    $data["id_operacion"]=$ido;
    $get_fol=$this->General_model->get_tableRowC('folio_cliente','operaciones',array('id'=>$ido));
    $data["folio"]=$get_fol->folio_cliente;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/opera_transac',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/opera_trans_js');
  }

  public function verificaOperaGrado(){
    $idcc = $this->input->post('idcc');
    $idp = $this->input->post('idp');
    $id_opera = $this->input->post('id_opera');
    $band = $this->input->post('band');
    if($band==0)
      $data=$this->General_model->get_tableRow('grado_riesgo_actividad',array("id_clientec"=>$idcc,"id_perfilamiento"=>$idp,"id_operacion"=>$id_opera));
    else
      $data=$this->General_model->get_tableRow('grado_riesgo_actividad',array("id_operacion"=>$id_opera));

    if(isset($data))
      echo json_encode($data);
    else
      echo 0;
  }

  public function operaGradoAct(){
    $id_opera = $this->input->post('id_opera');
    $data=$this->General_model->get_tableRow('grado_riesgo_actividad',array("id_operacion"=>$id_opera));
    if(isset($data))
      echo json_encode($data);
    else
      echo 0;
  }

  public function getOpera(){ //acá hacer toda la consulta operacions de id opera a clintes transacciones, 
    $id_opera = $this->input->post('id_opera');
    $id_act = $this->input->post('id_act');
    $id_union = $this->input->post('id_union');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $id_per = $this->input->post('id_perfilamiento');

    if($id_act==1){ //juegos y apuestas
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct1($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso1($id_anexo,0);
      }
    } 
    else if($id_act==2){ //tarjetas prepagadas -2a
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct2a($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso2a($id_anexo,0);
      }
    }  
    else if($id_act==3){ //Anexo 2B - Emisión, comercialización o abono de tarjetas prepagadas, vales y/o cupones.
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct2b($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso2b($id_anexo,0);
      }
    } 
    else if($id_act==4){ //Anexo 2C - Monederos y tarjetas de devoluciones y recompensas
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct2c($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso2c($id_anexo,0);
      }
    } 
    else if($id_act==5){ //Anexo 3 - cheques de viajero
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct3($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso3($id_anexo,0);
      }
    }  
    else if($id_act==6){ //Anexo 4 - Servicios de mutuo, préstamos o créditos.
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct4($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso4($id_anexo,0);
      }
    }  
    else if($id_act==7){ //Anexo 5A - Servicios de construcción o desarrollo inmobiliario
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct5a($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso5a($id_anexo,0);
      }
    }  
    else if($id_act==8){ //Anexo 5B - Desarrollos Inmobiliarios
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct5b($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso5b($id_anexo,0);
      }
    }
    else if($id_act==9){ //Anexo 6 - Comercialización de metales preciosos, joyas y relojes
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct6($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso6($id_anexo,0);
      }
    }   
    else if($id_act==10){ //Anexo 7 - 
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct7($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso6($id_anexo,0);
      }
    }    
    else if($id_act==11){ //vehiculos
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso($id_anexo,0);
      }
    }
    else if($id_act==12){ //Anexo 9
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct9($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso9($id_anexo,0);
      }
    }
    else if($id_act==13){ //Anexo 10
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct10($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso10($id_anexo,0);
      }
    }
    else if($id_act==15){ //Anexo 12A
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct12a($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso12a($id_anexo,0);
      }
    }
    else if($id_act==17){ //Anexo 13 - Donativos
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct13($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso13($id_anexo,0);
      }
    }
    else if($id_act==19){ //Anexo 15 - Arrendamiento de inmueble
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct15($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso15($id_anexo,0);
      }
    }
    else if($id_act==20){ //Anexo 16 - Activos virtuales.
      if($aviso==0){
        $data=$this->ModeloCatalogos->getPagosOperaAct16($id_opera,$id_act,$id_union,$id_anexo,$aviso,$id_per);
      }else{
        $data=$this->ModeloCatalogos->getPagosOperaActAviso16($id_anexo,0);
      }
    }
    $arraydata = array('data'=>$data);
    echo json_encode($arraydata);
  }

  public function getOperaActividad(){ //acá hacer toda la consulta operacions de id opera a clintes transacciones, para 12a y su tipo de act 
    $id_opera = $this->input->post('id_opera');
    $id_act = $this->input->post('id_act');
    $id_union = $this->input->post('id_union');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $tipo = $this->input->post('tipo');
    $id_per = $this->input->post('id_perfilamiento');

     //Anexo 12A
    if($aviso==0){
      $data=$this->ModeloCatalogos->getPagosOperaAct12a($id_opera,$id_act,$id_union,$id_anexo,$aviso,$tipo,$id_per);
    }else{
      $data=$this->ModeloCatalogos->getPagosOperaActAviso12a($id_anexo,0,$tipo);
    }
    
    $arraydata = array('data'=>$data);
    echo json_encode($arraydata);
  }

  public function getPagosA12a(){ //trae los pagos de la act6 del anexo 12a
    $id = $this->input->post('id');

    $data=$this->ModeloCatalogos->getPagosOperaAct12aAct6($id);
    $arraydata = array('data'=>$data);
    echo json_encode($arraydata);
  }

  public function getOperaActividad12b(){ //acá hacer toda la consulta operacions de id opera a clintes transacciones, para 12b y su tipo de act 
    $id_opera = $this->input->post('id_opera');
    $id_act = $this->input->post('id_act');
    $id_union = $this->input->post('id_union');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $tipo = $this->input->post('tipo');
    $id_per = $this->input->post('id_perfilamiento');

     //Anexo 12B
    //log_message('error', 'aviso: '.$aviso);
    if($aviso==0){
      $data=$this->ModeloCatalogos->getPagosOperaAct12b($id_opera,$id_act,$id_union,$id_anexo,$aviso,$tipo,$id_per);
    }else{
      $data=$this->ModeloCatalogos->getPagosOperaActAviso12b($id_anexo,0,$tipo);
    }

    $arraydata = array('data'=>$data);
    echo json_encode($arraydata);
  }

  public function acusaPago(){ //cambia el estatus de acusado al pago 
    $id = $this->input->post('id');
    $id_act = $this->input->post('act');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $pago_ayuda = $this->input->post('pago_ayuda');
    //log_message('error', 'id_act: '.$id_act);

    if($id_act==1){ //juegos y apuestas
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo1');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo1"=>$id_anexo,"activo"=>1),'pagos_transac_anexo1');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo1_aviso');
      }
    }else if($id_act==2){ //tarjeta prepagadas
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo2a');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo2a"=>$id_anexo,"activo"=>1),'pagos_transac_anexo2a');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo2a_aviso');
      }
    }else if($id_act==3){ //anexo 2b
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo2b');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo2b"=>$id_anexo,"activo"=>1),'pagos_transac_anexo2b');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo2b_aviso');
      }
    }else if($id_act==4){ //anexo 2c
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo2c');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo2c"=>$id_anexo,"activo"=>1),'pagos_transac_anexo2c');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo2c_aviso');
      }
    }
    else if($id_act==5){ //anexo 3 cheques de viajero
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo3');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo3"=>$id_anexo,"activo"=>1),'pagos_transac_anexo3');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo3_aviso');
      }
    }
    else if($id_act==6){ //anexo 4 - Servicios de mutuo, préstamos o créditos.
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo4');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo4"=>$id_anexo,"activo"=>1),'pagos_transac_anexo4');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo4_aviso');
      }
    }
    else if($id_act==7){ //Anexo 5A - Servicios de construcción o desarrollo inmobiliario
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo5a');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo5a"=>$id_anexo,"activo"=>1),'pagos_transac_anexo5a');//el de auyuda a sumatoria - otra operacion que ayudó
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo5a');
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo5a_aviso');
      }
    }
    else if($id_act==8){ //Anexo 5B - Desarrollos Inmobiliarios
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id),'anexo5b');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id"=>$id_anexo),'anexo5b');//el de auyuda a sumatoria - otra operacion que ayudó
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id_anexo"=>$id_anexo),'terceros_5b');//es para el pago de la operacion propia
      }else{
        if($pago_ayuda==1){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'anexo5b_aviso');
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id_anexo"=>$id),'terceros_5b');//es para el pago de la operacion propia
        }
      }
    }
    else if($id_act==9){ //Anexo 6 - Comercialización de metales preciosos, joyas y relojes
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo6_pago');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo6"=>$id_anexo),'pagos_transac_anexo6_pago');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo6_pago_aviso');
      }
    }
    else if($id_act==10){ //Anexo 7 - 
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo_7');//es para el pago de la operacion propia
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo7"=>$id_anexo),'pagos_transac_anexo_7');//el de auyuda a sumatoria - otra operacion que ayudó
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo7_aviso');
      }
    }
    else if($id_act==11){ //vehiculos
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo_8');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id_anexo_8"=>$id_anexo,"status"=>1),'pagos_transac_anexo_8');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo_8_aviso');
      }
    }
    else if($id_act==12){ //anexo 9
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo9');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo9"=>$id_anexo,"activo"=>1),'pagos_transac_anexo9');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo9_aviso');
      }
    }
    else if($id_act==13){ //anexo 10
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo10');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo10"=>$id_anexo,"activo"=>1),'pagos_transac_anexo10');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo10_aviso');
      }
    }
    else if($id_act==15){ //anexo 12A
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("idanexo12a"=>$id),'pagos_transac_anexo12a');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo12a"=>$id_anexo,"activo"=>1),'pagos_transac_anexo12a');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo12a_aviso');
      }
    }
    else if($id_act==17){ //anexo 13 - Donativos
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo13_num');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo13"=>$id_anexo,"activo"=>1),'pagos_transac_anexo13_num');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo13_num_aviso');
      }
    }
    else if($id_act==19){ //anexo 15 - Arrendamiento de inmueble
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'pagos_transac_anexo_15');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id_anexo_15"=>$id_anexo,"status"=>1),'pagos_transac_anexo_15');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo_15_aviso');
      }
    }
    else if($id_act==20){ //anexo 16 - Activos virtuales.
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"/*,"xml"=>"1"*/),array("id"=>$id),'completar_anexo16');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id"=>$id_anexo,"Activo"=>1),'anexo16');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'completar_anexo16_aviso');
      }
    }
    echo $id;
  }

  public function acusaNoPago(){ //cambia el estatus de acusado al pago 
    $ids = $this->input->post('data');
    $id_anexo = $this->input->post('id');
    $aviso = $this->input->post('aviso');
    $DATAid = json_decode($ids);
    log_message('error', 'count de data: '.count($DATAid));
    $id=0;
    for($i=0; $i<count($DATAid); $i++){
      $id=$DATAid[$i]->id_desa;
      /*$id_anexo=$DATAid[$i]->id_anexo;
      $aviso=$DATAid[$i]->aviso;*/
      log_message('error', 'id: '.$id);
      log_message('error', 'id_anexo: '.$id_anexo);
       //anexo 12A
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0","xml"=>"0"),array("idanexo12a"=>$id),'pagos_transac_anexo12a');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0","xml"=>"0","id_pago_ayuda"=>"0"),array("idanexo12a"=>$id_anexo,"activo"=>1),'pagos_transac_anexo12a');//el de auyuda a sumatoria
      }else{
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0"),array("id"=>$id),'pagos_transac_anexo12a_aviso');
      }
    }
    
    echo $id;
  }

  public function acusaPagoArray(){ //cambia el estatus de acusado al pago 
    $ids = $this->input->post('data');
    $id_act = $this->input->post('act');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $pago_ayuda = $this->input->post('pago_ayuda');
    $DATAid = json_decode($ids);
    log_message('error', 'count de data: '.count($DATAid));
    $id=0;
    for($i=0; $i<count($DATAid); $i++){
      $id=$DATAid[$i]->id_desa;
      /*$id_anexo=$DATAid[$i]->id_anexo;
      $aviso=$DATAid[$i]->aviso;*/
      log_message('error', 'id: '.$id);
      log_message('error', 'id_anexo: '.$id_anexo);
      if($aviso==0){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo12a"=>$id),'pagos_transac_anexo12a');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("idanexo12a"=>$id_anexo,"activo"=>1),'pagos_transac_anexo12a');//el de auyuda a sumatoria
      }else{
        if($pago_ayuda==1)
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo12a_aviso');
      }
    }
    
    echo $id;
  }

  public function acusaPagoActividad12Array(){ //cambia el estatus de acusado al pago 
    $ids = $this->input->post('data');
    $id_act = $this->input->post('act');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $pago_ayuda = $this->input->post('pago_ayuda');
    $id_ant = $this->input->post('id_ant');
    //log_message('error', 'id_act: '.$id_act);
    //log_message('error', 'id: '.var_dump($id));
    //anexo 12A
    $DATAid = json_decode($ids);
    log_message('error', 'count de data: '.count($DATAid));
    for($i=0; $i<count($DATAid); $i++){
      $id=$DATAid[$i]->id_desa;
      log_message('error', 'id: '.$id);
      log_message('error', 'id_anexo: '.$id_anexo);
      if($aviso==0){
      //for($i=0; $i<count($id); $i++){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id),'anexo12a');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id"=>$id_anexo,"activo"=>1),'anexo12a');//el de auyuda a sumatoria
        $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>$id_anexo),array("id"=>$id,"activo"=>1),'anexo12a');// a quien ayudó
        if($id_ant>0){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayude"=>$id_anexo),array("id"=>$id_ant,"activo"=>1),'anexo12a');//el de auyuda a sumatoria -ant
          //$this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>$id_anexo),array("id"=>$id_ant,"activo"=>1),'anexo12a');// a quien ayudó - ant
        }
        $id=$id;
      //}
      }else{
        if($pago_ayuda==1){
        //for($i=0; $i<count($id); $i++){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'anexo12a_aviso');
          if($id_ant>0){
            $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id_ant),'anexo12a_aviso');
          }
          $id=$id;
        //}
        }
      }
    }
    echo $id;
  }

  public function acusaPagoActividad12(){ //cambia el estatus de acusado al pago 
    $id = $this->input->post('id');
    $id_act = $this->input->post('act');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $pago_ayuda = $this->input->post('pago_ayuda');
    $id_ant = $this->input->post('id_ant');
    //log_message('error', 'id_act: '.$id_act);
    //log_message('error', 'id: '.var_dump($id));
    //anexo 12A
    if($aviso==0){
      //for($i=0; $i<count($id); $i++){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id),'anexo12a');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id"=>$id_anexo,"activo"=>1),'anexo12a');//el de auyuda a sumatoria
        $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>$id_anexo),array("id"=>$id,"activo"=>1),'anexo12a');// a quien ayudó
        if($id_ant>0){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id_ant,"activo"=>1),'anexo12a');//el de auyuda a sumatoria -ant
          $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>$id_anexo),array("id"=>$id_ant,"activo"=>1),'anexo12a');// a quien ayudó - ant
        }
        $id=$id;
      //}
    }else{
      if($pago_ayuda==1){
        //for($i=0; $i<count($id); $i++){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'anexo12a_aviso');
          if($id_ant>0){
            $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id_ant),'anexo12a_aviso');
          }
          $id=$id;
        //}
      }
    }
    
    echo $id;
  }

  public function acusaNoPagoActividad12(){ //cambia el estatus de acusado al pago 
    $ids = $this->input->post('data');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $id=0;
    $DATAid = json_decode($ids);
    log_message('error', 'id_anexo: '.$id_anexo);
    log_message('error', 'aviso: '.$aviso);
    log_message('error', 'count de data: '.count($DATAid));
    
    for($i=0; $i<count($DATAid); $i++){
      $id=$DATAid[$i]->id_desa;
      /*$id_anexo=$DATAid[$i]->id_anexo;
      $aviso=$DATAid[$i]->aviso;*/
      log_message('error', 'id: '.$id);
      
      if($aviso==0){
        //for($i=0; $i<count($id); $i++){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0","xml"=>"0"),array("id"=>$id),'anexo12a');
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0","xml"=>"0","id_pago_ayuda"=>"0"),array("id"=>$id_anexo,"activo"=>1),'anexo12a');//el de auyuda a sumatoria
          $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>"0"),array("id"=>$id,"activo"=>1),'anexo12a');// a quien ayudó
          if($id>0){
            $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0","xml"=>"0"),array("id"=>$id,"activo"=>1),'anexo12a');//el de auyuda a sumatoria -ant
            $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>"0"),array("id"=>$id,"activo"=>1),'anexo12a');// a quien ayudó - ant
          }
          $id=$id;
        //}
      }else{
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0"),array("id"=>$id),'anexo12a_aviso');
        if($id>0){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"0"),array("id"=>$id),'anexo12a_aviso');
        }
        $id=$id;
      }
    }
    
    echo $id;
  }

  public function acusaPagoActividad12bArray(){ //cambia el estatus de acusado al pago 
    $ids = $this->input->post('data');
    $id_act = $this->input->post('act');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $pago_ayuda = $this->input->post('pago_ayuda');
    $id_ant = $this->input->post('id_ant');
    //log_message('error', 'id_act: '.$id_act);
    //log_message('error', 'id: '.var_dump($id));
    //anexo 12A
    $DATAid = json_decode($ids);
    log_message('error', 'count de data: '.count($DATAid));
    for($i=0; $i<count($DATAid); $i++){
      $id=$DATAid[$i]->id_desa;
      log_message('error', 'id: '.$id);
      log_message('error', 'id_anexo: '.$id_anexo);
      if($aviso==0){
      //for($i=0; $i<count($id); $i++){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id),'anexo12b');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id"=>$id_anexo,"activo"=>1),'anexo12b');//el de auyuda a sumatoria
        $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>$id_anexo),array("id"=>$id,"activo"=>1),'anexo12b');// a quien ayudó
        if($id_ant>0){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayude"=>$id_anexo),array("id"=>$id_ant,"activo"=>1),'anexo12b');//el de auyuda a sumatoria -ant
        }
        $id=$id;
      //}
      }else{
        if($pago_ayuda==1){
        //for($i=0; $i<count($id); $i++){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'anexo12b_aviso');
          if($id_ant>0){
            $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id_ant),'anexo12b_aviso');
          }
          $id=$id;
        //}
        }
      }
    }
    echo $id;
  }

  public function acusaPagoActividad5aArray(){ //cambia el estatus de acusado al pago 
    $ids = $this->input->post('data');
    $id_act = $this->input->post('act');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $pago_ayuda = $this->input->post('pago_ayuda');
    $id_ant = $this->input->post('id_ant');
    //log_message('error', 'id_act: '.$id_act);
    //log_message('error', 'id: '.var_dump($id));
    //anexo 12A
    $DATAid = json_decode($ids);
    //log_message('error', 'count de data: '.count($DATAid));
    for($i=0; $i<count($DATAid); $i++){
      $id=$DATAid[$i]->id_desa;
      //log_message('error', 'id: '.$id);
      //log_message('error', 'id_anexo: '.$id_anexo);
      if($aviso==0){
      //for($i=0; $i<count($id); $i++){
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo5a');
        //$this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id),array("id"=>$id_anexo,"activo"=>1),'pagos_transac_anexo5a');//el de auyuda a sumatoria --comentado, genera problemas en pruebas de 07-09-2023
        $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>$id_anexo),array("id"=>$id,"activo"=>1),'pagos_transac_anexo5a');// a quien ayudó 
        /*if($id_ant>0){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayude"=>$id_anexo),array("id"=>$id_ant,"activo"=>1),'pagos_transac_anexo5a');//el de auyuda a sumatoria -ant
        }*/
        $id=$id;
      //}
      }else{
        if($pago_ayuda==1){
        //for($i=0; $i<count($id); $i++){
          $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id),'pagos_transac_anexo5a_aviso');
          if($id_ant>0){
            $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id_ant),'pagos_transac_anexo5a_aviso');
          }
          $id=$id;
        //}
        }
      }
    }
    echo $id;
  }

  public function acusaPagoActividad5bArray(){ //cambia el estatus de acusado al pago 
    $ids = $this->input->post('data');
    $id_act = $this->input->post('act');
    $id_anexo = $this->input->post('id_anexo');
    $aviso = $this->input->post('aviso');
    $pago_ayuda = $this->input->post('pago_ayuda');
    $id_ant = $this->input->post('id_ant');
    $DATAid = json_decode($ids);
    //log_message('error', 'count de data: '.count($DATAid));
    for($i=0; $i<count($DATAid); $i++){
      $id=$DATAid[$i]->id_desa;
      $id_t=$DATAid[$i]->id_desa_t;

      //log_message('error', 'id del anexo: '.$id);
      //log_message('error', 'id del pago (tercero) : '.$id_t);

      if($aviso==0){
      //for($i=0; $i<count($id); $i++){

        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id_t),'terceros_5b');
        $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1","id_pago_ayuda"=>$id_t),array("id"=>$id_t,"estatus"=>1),'terceros_5b');//el de auyuda a sumatoria
        
        //acá me quedo, modifiqué "id"=>$id_ x "id"=>$id del array where, verificar funcionalidad
        $this->ModeloCatalogos->updateCatalogo2(array("id_pago_ayude"=>$id_anexo),array("id"=>$id_t,"estatus"=>1),'terceros_5b');// a quien ayudó
        $id=$id;
      //}
      }else{
        if($pago_ayuda==1){
        //for($i=0; $i<count($id); $i++){
          //$this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id_t),'terceros_5b');
          if($id_ant>0){
            //$this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1"),array("id"=>$id_ant),'terceros_5b');
          }
          $id=$id;
        //}
        }
      }
    }
    echo $id;
  }
  public function acusaPago2(){ //cambia el estatus de acusado al pago 
    $id = $this->input->post('id');
    $act = $this->input->post('act');
    if($act==1) //juegos y apuestas                                                                 liquidacion numeraria
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo1"=>$id,"activo"=>1/*,"tipo_liquidacion"=>1*/),'pagos_transac_anexo1');
    else if($act==2) //tarjeta prepagadas                                                                 
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo2a"=>$id,"activo"=>1),'pagos_transac_anexo2a');
    else if($act==3) //Anexo 2B - Emisión, comercialización o abono de tarjetas prepagadas, vales y/o cupones.
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo2b"=>$id,"activo"=>1),'pagos_transac_anexo2b');
    else if($act==4) //Anexo 2C - Monederos y tarjetas de devoluciones y recompensas
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo2c"=>$id,"activo"=>1),'pagos_transac_anexo2c');
    else if($act==5) //Anexo 3 - Cheques de viajero.
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo3"=>$id,"activo"=>1),'pagos_transac_anexo3');
    else if($act==6) //Anexo 4 - Servicios de mutuo, préstamos o créditos.
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo4"=>$id,"activo"=>1),'pagos_transac_anexo4');
    else if($act==7) //Anexo 5A - Servicios de construcción o desarrollo inmobiliario
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo5a"=>$id,"activo"=>1),'pagos_transac_anexo5a');
    else if($act==8) //Anexo 5B - Desarrollos Inmobiliarios
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id),'anexo5b');
    else if($act==9) //Anexo 6 - Comercialización de metales preciosos, joyas y relojes
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo6"=>$id,"activo"=>1),'pagos_transac_anexo6_pago');
    else if($act==10) //Anexo 7
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo7"=>$id,"activo"=>1),'pagos_transac_anexo_7');
    else if($act==11) //vehiculos
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id_anexo_8"=>$id,"status"=>1),'pagos_transac_anexo_8');
    else if($act==12) //Anexo 9
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo9"=>$id,"activo"=>1),'pagos_transac_anexo9');
    else if($act==13) //Anexo 10
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo10"=>$id,"activo"=>1),'pagos_transac_anexo10');
    else if($act==14) //Anexo 11 servicios profesionales
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id,"activo"=>1),'anexo11');
    else if($act==15) //Anexo 12A
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id,"activo"=>1),'anexo12a');
    else if($act==16) //Anexo 12B
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id,"activo"=>1),'anexo12b');
    else if($act==17) //Anexo 13 - Donativos
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo13"=>$id,"activo"=>1),'pagos_transac_anexo13_num');
    else if($act==19) //Anexo 15 - Arrendamiento de inmueble
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id_anexo_15"=>$id,"status"=>1),'pagos_transac_anexo_15');
    else if($act==20) //Anexo 16 - Activos virtuales.
      $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("idanexo16"=>$id,"Activo"=>1),'completar_anexo16');
  }

  public function acusaPago2Liquida(){ //cambia el estatus de acusado al pago 
    $id = $this->input->post('id');
    $act = $this->input->post('act');
    //Anexo 12A
    $this->ModeloCatalogos->updateCatalogo2(array("acusado"=>"1","xml"=>"1"),array("id"=>$id,"activo"=>1),'pagos_transac_anexo12a');;
  }

  public function get_clientes_select(){
    $clientesc = $this->ModeloCliente->get_clientes_all($this->idcliente);
    $html="";
    $html.='<div class="row">
              <div class="col-md-12 form-group">
                <select class="form-control" id="cliente_">';
                //$html.='<option value="">Elige un cliente:</option>';
                foreach ($clientesc as $item) {
                  if($item->idtipo_cliente==1){
                    $idtipo_cliente=$item->idtipo_cliente_p_f_m;
                  }else if($item->idtipo_cliente==2){
                    $idtipo_cliente=$item->idtipo_cliente_p_f_e;
                  }else if($item->idtipo_cliente==3){
                    $idtipo_cliente=$item->idtipo_cliente_p_m_m_e;
                  }else if($item->idtipo_cliente==4){
                    $idtipo_cliente=$item->idtipo_cliente_p_m_m_d;
                  }else if($item->idtipo_cliente==5){
                    $idtipo_cliente=$item->idtipo_cliente_e_c_o_i;
                  }else if($item->idtipo_cliente==6){
                    $idtipo_cliente=$item->idtipo_cliente_f;
                  }
                  /* ****** Nombres ******* */
                  if($item->idtipo_cliente==1){
                    $nombre=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
                  }else if($item->idtipo_cliente==2){
                    $nombre=$item->nombre2.' '.$item->apellido_paterno2.' '.$item->apellido_materno2;
                  }else if($item->idtipo_cliente==3){
                    $nombre=$item->razon_social;
                  }else if($item->idtipo_cliente==4){
                    $nombre=$item->nombre_persona;
                  }else if($item->idtipo_cliente==5){
                    $nombre=$item->denominacion2;
                  }else if($item->idtipo_cliente==6){
                    $nombre=$item->denominacion;
                  }
                  $html.='<option value="'.$idtipo_cliente.'-'.$item->idperfilamientop.'-'.$item->idtipo_cliente.'-0">'.$nombre.'</option>';
                }

        $cli_union = $this->ModeloCliente->lista_union_cli_vista($this->idcliente);
        foreach ($cli_union as $item) {
          /*if($item->idtipo_cliente==1){
            $idtipo_cliente=$item->idtipo_cliente_p_f_m;
          }else if($item->idtipo_cliente==2){
            $idtipo_cliente=$item->idtipo_cliente_p_f_e;
          }else if($item->idtipo_cliente==3){
            $idtipo_cliente=$item->idtipo_cliente_p_m_m_e;
          }else if($item->idtipo_cliente==4){
            $idtipo_cliente=$item->idtipo_cliente_p_m_m_d;
          }else if($item->idtipo_cliente==5){
            $idtipo_cliente=$item->idtipo_cliente_e_c_o_i;
          }else if($item->idtipo_cliente==6){
            $idtipo_cliente=$item->idtipo_cliente_f;
          }*/
          /* ****** Nombres ******* */
          /*if($item->idtipo_cliente==1){
            $nombre=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
          }else if($item->idtipo_cliente==2){
            $nombre=$item->nombre2.' '.$item->apellido_paterno2.' '.$item->apellido_materno2;
          }else if($item->idtipo_cliente==3){
            $nombre=$item->razon_social;
          }else if($item->idtipo_cliente==4){
            $nombre=$item->nombre_persona;
          }else if($item->idtipo_cliente==5){
            $nombre=$item->denominacion2;
          }else if($item->idtipo_cliente==6){
            $nombre=$item->denominacion;
          }*/
          if($item->cliente!=null){
            $html.='<option value="'.$item->id_cliente_tipo.'-'.$item->idperfilamientop.'-'.$item->idtipo_cliente.'-'.$item->id_union.'">'.$item->cliente.'</option>';
          }
        }

        $html.='</select>       
          </div>
        </div>';

        echo $html;
  }

  public function get_benes_opera(){
    $id = $this->input->post('id');
    $idp = $this->input->post('idp');
    $dataa=$this->ModeloCliente->verificaOperacionBenes($id,$idp);

    foreach ($dataa as $k) {
      $tipo_bene = $k->tipo_bene_opera;
      $id_duenio_bene_nvo=$k->id_duenio_bene_nvo;
    
      if($tipo_bene=="2"){
        $data=$this->ModeloCliente->verificaOperacionMoral($id);
      }
      if($tipo_bene=="3"){
        $data=$this->ModeloCliente->verificaOperacionFide($id);
      }

      if($id_duenio_bene_nvo!=""){
        $this->ModeloCatalogos->updateCatalogo2(array("id_docs_bene"=>$id_duenio_bene_nvo),array('id'=>$id),'operacion_beneficiario');
      }
    }
    $arraydata = array('dataa'=>$dataa);
    echo json_encode($arraydata);
  }

  public function verificaProceso(){
    $id = $this->input->post('id');
    $this->db->trans_start();
    $data=$this->ModeloCliente->verificaOperacion($id);
    $ct="";
    $nombre=array();
    foreach ($data as $key) {
      $idDC = $key->idDC;
      //$idDB = $key->idDB;
      $tipo_bene = $key->tipo_bene;
      $id_cliente_transac = $key->id_cliente_transac;
      //$id_duenio_bene_nvo=$key->id_duenio_bene_nvo;

      /*$data2=$this->ModeloCliente->verificaOperacionBenes($id,$key->id_perfilamiento);
      foreach ($data2 as $k) {
        $tipo_bene = $k->tipo_bene_opera;
        $id_duenio_bene_nvo=$k->id_duenio_bene_nvo;
      }*/

      if($key->tipo_cliente==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
      if($key->tipo_cliente==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
      if($key->tipo_cliente==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
      if($key->tipo_cliente==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
      if($key->tipo_cliente==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i';}
      if($key->tipo_cliente==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
      $cli_datos=$this->General_model->get_tableQueryRow($tabla,array($idt=>$key->id_clientec)); 

      if($key->tipo_cliente==1 || $key->tipo_cliente==2){
        //$nombre[] = $cli_datos->nombre." ".$cli_datos->apellido_paterno." ".$cli_datos->apellido_materno;
      }
      if($key->tipo_cliente==3 || $key->tipo_cliente==4 || $key->tipo_cliente==5 || $key->tipo_cliente==6){
        //$nombre[] = $cli_datos->nombre_g." ".$cli_datos->apellido_paterno_g." ".$cli_datos->apellido_materno_g;
      }
      //$data["clienteName"]=$nombre;
      if($tipo_bene=="2"){
        $data=$this->ModeloCliente->verificaOperacionMoral($id);
      }
      if($tipo_bene=="3"){
        $data=$this->ModeloCliente->verificaOperacionFide($id);
      }

      if($idDC!=""){
        $this->ModeloCatalogos->updateCatalogo2(array("id_docs_cliente"=>$idDC),array('id'=>$id),'operacion_cliente');
      }

      /*if($id_duenio_bene_nvo!=""){
        $this->ModeloCatalogos->updateCatalogo2(array("id_docs_bene"=>$id_duenio_bene_nvo),array('id'=>$id),'operacion_beneficiario');
      }*/

      if($id_cliente_transac!=""){
        $ct=$this->General_model->GetAllWhere("clientesc_transacciones",array("id"=>$id_cliente_transac)); //debe traer las transacciones que hicieron los clientes
      }
    }//foreach

    /*if($tipo_bene=="2"){
      $data=$this->ModeloCliente->verificaOperacionMoral($id);
    }
    if($tipo_bene=="3"){
      $data=$this->ModeloCliente->verificaOperacionFide($id);
    }

    if($idDC!=""){
      $this->ModeloCatalogos->updateCatalogo2(array("id_docs_cliente"=>$idDC),array('id'=>$id),'clientes_operaciones');
    }

    if($idDB!=""){
      $this->ModeloCatalogos->updateCatalogo2(array("id_docs_bene"=>$idDB),array('id'=>$id),'clientes_operaciones');
    }

    if($id_cliente_transac!=""){
      $ct=$this->General_model->GetAllWhere("clientesc_transacciones",array("id"=>$id_cliente_transac));
    }*/
    $this->db->trans_complete();
    //$this->db->trans_status();
    //echo json_encode($data);
    $arraydata = array('data'=>$data,'ct'=>$ct,"clienteName"=>$nombre);
    echo json_encode($arraydata);

  }

  public function get_operacion_detalles(){
    $id = $this->input->post('id');
    $this->db->trans_start();
      $data=$this->General_model->get_tableRow('clientes_operaciones',array("id"=>$id));
      $befs = $this->ModeloCliente->get_proceso_detalle($data->id_duenio_bene); //verificar si existe reg de benef docs
      $arraydata = array('docs'=>$befs,'tipo_bene'=>$data->tipo_bene,'id_bene'=>$data->id_duenio_bene);
      echo json_encode($arraydata);
    $this->db->trans_complete();
    //$this->db->trans_status();
  }

  public function get_deta_bene_moral(){
    $id_bene_clone = $this->input->post('id_bene_clone');
    $data=$this->General_model->get_tableRow('clientes_operaciones',array("id"=>$id));
    echo json_encode($arraydata);
  }

  public function get_operacion_detallesBenes(){
    $id = $this->input->post('id_operacion');
    $idcc = $this->input->post('idcc');
    $id_p = $this->input->post('id_p');
    $this->db->trans_start();
      $data=$this->ModeloGeneral->get_opera_benes($id,$idcc,$id_p); //obtiene los dueños beneficiarios de la operacion y del perfila x cc
      foreach ($data as $k) {
        $befs = $this->ModeloCliente->get_proceso_detalle($k->id_duenio_bene); //verificar si existe reg de benef docs  
        $arraydata = array('docs'=>$befs,'tipo_bene'=>$k->tipo_bene,'id_bene'=>$k->id_duenio_bene);
      }
      echo json_encode($arraydata);
    $this->db->trans_complete();
  }
  public function generacionxml(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/generacionxml');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/generacionxmljs');
  }
  public function aviso_cero($id=0){
    //error_reporting(0);
    $data['actividades']=$this->ModeloCliente->getActividadesCliente($this->idcliente);
    
    //var_dump(intval($this->idperfilamiento->idperfilamiento));die;
    //log_message('error', 'idcliente: '.$this->idcliente);
    //log_message('error', 'idperfilamiento: '.$this->idperfilamiento->idperfilamiento);
    if(!isset($this->idperfilamiento->idperfilamiento)){
      //redirect(base_url()."Operaciones");
    }

    /*$get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>intval($this->idperfilamiento->idperfilamiento)));//obtiene datos del perfilamiento
    if($get->idtipo_cliente==1) $tabla = "tipo_cliente_p_f_m";
    if($get->idtipo_cliente==2) $tabla = "tipo_cliente_p_f_e";
    if($get->idtipo_cliente==3) $tabla = "tipo_cliente_p_m_m_e";
    if($get->idtipo_cliente==4) $tabla = "tipo_cliente_p_m_m_d";
    if($get->idtipo_cliente==5) $tabla = "tipo_cliente_e_c_o_i";
    if($get->idtipo_cliente==6) $tabla = "tipo_cliente_f"; 
    $data['cc']=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>intval($this->idperfilamiento->idperfilamiento)));
    $data['tipoc'] = $get->idtipo_cliente;  */

    $data['tipoc']=0;
    $data['idperfilamiento']=0; 
    //$data['idperfilamiento']=intval($this->idperfilamiento->idperfilamiento);  
    $data['id']=0;
    $data['anio_acuse']='';
    $data['mes_acuse']='';
    $data['actividad']='';
    $data['informe']='';
    if($id!=0){
      $result_aviso=$this->ModeloCatalogos->getselectwhere('aviso_cero','id',$id);
      foreach ($result_aviso as $item) {
        $data['id']=$item->id;
        $data['anio_acuse']=$item->anio_acuse;
        $data['mes_acuse']=$item->mes_acuse;
        $data['actividad']=$item->actividad;
        $data['informe']=$item->informe;
      }
    }
    $data["exento"]=0;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/aviso_cero',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/aviso_cerojs');
  }
  public function aviso_exento($id=0){
    $data['actividades']=$this->ModeloCliente->getActividadesCliente($this->idcliente);
    
    //var_dump(intval($this->idperfilamiento->idperfilamiento));die;
    if(!isset($this->idperfilamiento->idperfilamiento)){
      //redirect(base_url()."Operaciones");
    }
    /*$get = $this->General_model->get_tableRow("perfilamiento",array('idperfilamiento'=>intval($this->idperfilamiento->idperfilamiento)));//obtiene datos del perfilamiento
    if($get->idtipo_cliente==1) $tabla = "tipo_cliente_p_f_m";
    if($get->idtipo_cliente==2) $tabla = "tipo_cliente_p_f_e";
    if($get->idtipo_cliente==3) $tabla = "tipo_cliente_p_m_m_e";
    if($get->idtipo_cliente==4) $tabla = "tipo_cliente_p_m_m_d";
    if($get->idtipo_cliente==5) $tabla = "tipo_cliente_e_c_o_i";
    if($get->idtipo_cliente==6) $tabla = "tipo_cliente_f";
    $data['cc']=$this->General_model->get_tableRow($tabla,array('idperfilamiento'=>intval($this->idperfilamiento->idperfilamiento)));*/
    //$data['tipoc'] = $get->idtipo_cliente; 
    $data['tipoc']=0;
    $data['idperfilamiento']=0; 
    //$data['idperfilamiento']=intval($this->idperfilamiento->idperfilamiento);
    $data['id']=0;
    $data['anio_acuse']='';
    $data['mes_acuse']='';
    $data['actividad']='';
    $data['informe']='';
    if($id!=0){
      $result_aviso=$this->ModeloCatalogos->getselectwhere('aviso_cero','id',$id);
      foreach ($result_aviso as $item) {
        $data['id']=$item->id;
        $data['anio_acuse']=$item->anio_acuse;
        $data['mes_acuse']=$item->mes_acuse;
        $data['actividad']=$item->actividad;
        $data['informe']=$item->informe;
      }
    }
    $data["exento"]=1;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/operaciones/aviso_cero',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/operaciones/aviso_cerojs');
  }
  public function guadar_aviso_cero(){
      $data = $this->input->post();
      $id = $data['id'];
      /*if(isset($data['informe'])){
        $data['informe']=1;
      }else{
        $data['informe']=0;
      }*/
      unset($data['id']);
      $aux=0;
      if ($id>0) {
          $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'aviso_cero');
          $aux=$id;
      }else{
          $data['idcliente']=$this->idcliente;
          $aux=$this->ModeloCatalogos->tabla_inserta('aviso_cero',$data);
      }
      echo $aux;   
  }

  public function aviso_cero_xml($tipo,$id){
        $result_aviso=$this->ModeloCatalogos->getselectwhere('aviso_cero','id',$id);
        foreach ($result_aviso as $item) {
          $anio_acuse=$item->anio_acuse;
          $mes_acuse=$item->mes_acuse;
          $actividad=$item->actividad;
          $informe=$item->informe;
        }
        if($actividad==1) //1
          $cv="jys";
        if($actividad==2) //2a
          $cv="tsc";
        if($actividad==3) //2b
          $cv="tpp";
        if($actividad==4) //2c
          $cv="tdr";
        if($actividad==5) //3
          $cv="chv";
        if($actividad==6) //a
          $cv="mpc";
        if($actividad==7) //5a
          $cv="inm";
        if($actividad==8) //5b
          $cv="din";
        if($actividad==9) //6
          $cv="mjr";
        if($actividad==10) //7
          $cv="oba";
        if($actividad==11) //8
          $cv="veh";
        if($actividad==12) //9
          $cv="bli";
        if($actividad==13) //10 
          $cv="tcv";
        if($actividad==14) //11
          $cv="spr";
        if($actividad==15)
          $cv="fep";
        if($actividad==16)
          $cv="fes";
        if($actividad==17)
          $cv="don";
        if($actividad==18)
          $cv="";
        if($actividad==19)
          $cv="ari";
        if($actividad==20)
          $cv="avi";

        $rfc="";
        $get_actividad=$this->ModeloCliente->get_actividad($this->idcliente);
        foreach ($get_actividad as $item) {
            //<!-- Cliente fisica -->
            if($item->rfc!=""){
                $rfc=$item->rfc;
            }
            //<!-- Cliente moral -->
            if($item->r_c_f!=""){
                $rfc=$item->r_c_f;
            }
            //<!-- Cliente fideicomiso -->
            if($item->rfc_fideicomiso!=""){
                $rfc=$item->rfc_fideicomiso;
            }
        }
        $mes_acusem=$anio_acuse.$mes_acuse;
        $xml='<?xml version="1.0" encoding="UTF-8"?>
          <archivo xsi:schemaLocation="http://www.uif.shcp.gob.mx/recepcion/'.$cv.' '.$cv.'.xsd" xmlns="http://www.uif.shcp.gob.mx/recepcion/'.$cv.'" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <informe>
              <mes_reportado>'.$mes_acusem.'</mes_reportado>
              <sujeto_obligado>
                <clave_sujeto_obligado>'.$rfc.'</clave_sujeto_obligado>
                <clave_actividad>'.strtoupper($cv).'</clave_actividad>';
                if($informe==0){
                  $xml.='<exento>1</exento>';
                }
            $xml.='</sujeto_obligado>
            </informe>
          </archivo>';

        $fecha=date('ymd-His');
        $name="aviso";
        if($tipo==0)
          $name="aviso_en_cero";
        else
          $name="aviso_exento";
        header('Expires: 0');
        header('Cache-control: private');
        header ("Content-type: text/xml"); // Archivo xml
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Disposition: attachment; filename="'.$fecha.'_'.$name.'.xml"');
        echo $xml;

    }  

  public function eliminaCO(){
    $id = $this->input->post('id');
    $this->ModeloCatalogos->updateCatalogo2(array("activo"=>0),array('id'=>$id),'operacion_cliente');
  }
  public function eliminaBO(){
    $id = $this->input->post('id');
    $this->ModeloCatalogos->updateCatalogo2(array("activo"=>0),array('id'=>$id),'operacion_beneficiario');
  }

  public function validar_exite_operacion(){
    $id = $this->input->post('idop');
    $id_clientec = $this->input->post('id_clientec');
    $validar=0;
    if($id==0){
      $validar=0;
    }else{
      $arraywhere = array('id_operacion' =>$id,'id_clientec' =>$id_clientec);
      $result=$this->ModeloCatalogos->getselectwherestatus('*','operacion_cliente',$arraywhere);
      foreach ($result as $x){
        $validar=1;
      }
    }
    echo $validar;
  }
}