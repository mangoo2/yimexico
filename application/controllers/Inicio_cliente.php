<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio_cliente extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        //$this->load->helper('url');
        //$this->load->model('ModeloCatalogos');
        $this->load->model('General_model');
        $this->load->model('ModeloCliente');
    }
	public function index()
    {
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio_cliente');
        $this->load->view('templates/footer');
        $this->load->view('jsinicio_cliente');
	}
}