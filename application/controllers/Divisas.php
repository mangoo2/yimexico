<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class Divisas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('General_model');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->usuarioid=$this->session->userdata('usuarioid');
            //ira el permiso del modulo
            $this->load->model('Login_model');
            $permiso=1;
            //$permiso=$this->Login_model->getviewpermiso($this->usuarioid,8);// 8 es el id del submenu
            if ($permiso==0) {
                //redirect('/Sistema');
            }
        }
    }

    public function index(){
        $data['dolar_precio']='';
        $data['dolar_fecha']='';
        $dolar_americano_result1=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>1,'status'=>1),'fecha');
        foreach ($dolar_americano_result1 as $item){
            $data['dolar_precio']=$item->valor;
            $data['dolar_fecha']=$item->fecha;
        }
        $data['dolar_cprecio']='';
        $data['dolar_cfecha']='';
        $dolar_americano_result1=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>4,'status'=>1),'fecha');
        foreach ($dolar_americano_result1 as $item){
            $data['dolar_cprecio']=$item->valor;
            $data['dolar_cfecha']=$item->fecha;
        }
        $data['euro_precio']='';
        $data['euro_fecha']='';
        $dolar_americano_result2=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>2,'status'=>1),'fecha');
        foreach ($dolar_americano_result2 as $item){
            $data['euro_precio']=$item->valor;
            $data['euro_fecha']=$item->fecha;
        }
        $data['libra_precio']='';
        $data['libra_fecha']='';
        $dolar_americano_result3=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>3,'status'=>1),'fecha');
        foreach ($dolar_americano_result3 as $item){
            $data['libra_precio']=$item->valor;
            $data['libra_fecha']=$item->fecha;
        }
        $data['chino_precio']='';
        $data['chino_fecha']='';
        $dolar_americano_result3=$this->ModeloCatalogos->getselectwhere_orden_asc('divisas',array('moneda'=>5,'status'=>1),'fecha');
        foreach ($dolar_americano_result3 as $item){
            $data['chino_precio']=$item->valor;
            $data['chino_fecha']=$item->fecha;
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/divisas/lista',$data);
        $this->load->view('templates/footer');
        $this->load->view('configuracion/divisas/listajs');
    }

    public function alta($id=0){
        if($id>0){
            $data['d'] = $this->General_model->get_tableRow("divisas",array("id"=>$id));
        }
        else{
            $data="";
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('configuracion/divisas/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('configuracion/divisas/addjs');
    }

    public function datatable_records(){
        $params=$this->input->post();
        $datas = $this->ModeloCatalogos->getDivisas($params);
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    public function submit(){
        $data=$this->input->post();
        if($data['id']==0){ //insert
            $id=$this->ModeloCatalogos->tabla_inserta("divisas",$data);
        }
        else{ //update
            $id=$data["id"]; unset($data["id"]);
            $this->ModeloCatalogos->updateCatalogo($data,'id',$id,'divisas');
        }
        echo $id;
    }

    public function eliminar(){
        $id=$this->input->post("id");
        $this->ModeloCatalogos->updateCatalogo(array("status"=>0),'id',$id,'divisas');
    }

    /*public function getDivisaTipo(){
        $tipo=$this->input->post("tipo");
        $fecha=$this->input->post("fecha");
        //$data = $this->ModeloCatalogos->getDivisa($tipo,date("Y-m-d"));
        $fecha_ant= date("Y-m-d", strtotime('-1 day', strtotime($fecha)));
        $data = $this->ModeloCatalogos->getDivisa($tipo,$fecha_ant);
        $cont=0;
        $valor=0;
        foreach ($data as $k) {
            $cont++;
            $valor=$k->valor;
        }
        echo $valor; 
    }*/

    public function getDivisaTipo(){ //se cambia el de arriba, ahora traerá el ultimo registro disponible
        $tipo=$this->input->post("tipo");
        $fecha=$this->input->post("fecha");
        log_message('error', 'tipo : '.$tipo);
        log_message('error', 'fecha : '.$fecha);
        
        $cont=0;
        $valor=0;

        if($fecha!=""){
            $data = $this->ModeloCatalogos->getDivisa($tipo,$fecha);
            foreach ($data as $k) {
                $cont++;
                $valor=$k->valor;
                log_message('error', 'valor de foreach: '.$valor);
            }

            if($cont>0){
                echo $valor; 
            }else{
                //echo 0;
                $data = $this->ModeloCatalogos->getDivisaUltimo($tipo);
                echo $data->valor;
            }
        }else{
            echo 0;
        }
    }
    
}