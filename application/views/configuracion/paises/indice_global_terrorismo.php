<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-8">
            <h3 class=""> Indice global de terrorismo </h3>
          </div>
          <div class="col-lg-4" align="right">
            <a type="button" class="btn gradient_nepal2" onclick="nuevo_registro()"><i class="fa fa-plus-circle"></i> Nuevo </a>
            <a type="button" class="btn gradient_nepal2" href="<?php echo base_url() ?>Paises"><i class="fa fa-arrow-left"></i> Regresar</a>
          </div>
        </div>
        
        <hr class="subtitle">
        <div class="table-responsive">
          <table class="table" id="table_datos" width="100%">
            <thead>
              <tr>
                <th>País</th>
                <th>Riesgo</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!--- Modal -->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editar País</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form method="post" id="form_registro">
            <input type="hidden" name="id" id="id_registro">
            <div class="row">
              <div class="col-md-12">
                <label>País:</label>
                <input class="form-control" type="text" name="pais" id="pais">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label>Consideración de riesgo:</label>
                <select class="form-control" name="riesgo" id="riesgo">
                </select>
              </div>
            </div> 
          </form>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="editar()">Guardar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Deseas eliminar este registro? Los cambios son irreversibles</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>