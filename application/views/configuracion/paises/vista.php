<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 class="">Catalogo de riesgos</h3>
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-12">
            <select class="form-control" id="riesgo" onchange="tipo_riesgo()">
              <option>Selecciona una opción</option> 
              <option value="1">Lugares de frontera</option>
              <option value="2">Puertos de Entrada y Salida de acuerdo al Sistema Portuario Nacional</option>
              <option value="3">Indice Basilea</option>
              <option value="4">Indice de Secrecía Bancaria</option>
              <option value="5">Indice de Corrupción</option>
              <option value="6">Indice GAFI</option>
              <option value="7">Incidencia delictiva</option>
              <option value="8">Percepción de corrupción estatal en las zonas urbanas</option>
              <option value="9">Indice de Paz 2020</option>
              <option value="10">Países - guia terrorismo</option>
              <option value="11">Indice global de terrorismo</option>
              <option value="12">Países indice de corrupción global</option>
              <option value="13">Países regímenes fiscales preferentes</option>
            </select>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
