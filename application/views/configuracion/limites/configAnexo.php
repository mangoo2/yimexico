<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">

        <div class="col-md-3 form-group">
          <div class="d-flex mb-2">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <h3 class="">Configurar mis actividades vulnerables</h3>
            </div>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal_ayuda">
              <button type="button" class="btn btn-warning btn-rounded btn-icon"> <i class="mdi mdi-help"></i> </button>
            </a>              
          </div>
        </div>

        <hr class="subtitle">
        <div class="table-responsive">
          <table class="table" id="table_datos" width="100%">
            <thead>
              <tr>
                <th>Actividad económica</th>
                <th>Limite de efectivo (Permitir avance)</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Administracion_sistema"><i class="fa fa-reply"></i> Regresar</a>
      </div>
    </div>
  </div>
</div>
<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editar configuración de actividad económica</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form method="post" id="form_actividad">
            <input type="hidden" name="id" id="id">
            <input type="hidden" name="id_anexo" id="id_anexo">
            <input type="hidden" name="id_cliente" id="id_cliente">
            <div class="row">
              <div class="col-md-12">
                <h4 style="color: black">Actividad: <span class="actividad_nombre"></span></h4> 
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-12">
                <label>Permitir avanzar después de superar limite de efectivo:</label>
                <select class="form-control" name="permite_avance" id="permite_avance">
                </select>
              </div>
            </div> 
          </form>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="editar()">Guardar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Configurar efectivo</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>En esta pantalla el administrador puede restringir el continuar con la captura de la transacción/operación, cuando
           se reciba como medio de pago efectivo, por montos superiores permitidos por la Ley.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
