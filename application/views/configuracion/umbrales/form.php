<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #2b254e;
  }
</style>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3 >Umbrales</h3>
          <hr class="subtitle">
          <form class="form" method="post" role="form" id="form_umbrales">
            <?php if($u->id_anexo_gral!=15 && $u->id_anexo_gral!=16) { ?>
            <?php if(isset($u)) { ?>
            <input type="hidden" name="id" value="<?php echo $u->id ?>">
            <?php }else{ ?>
              <input type="hidden" name="id" value="0">
            <?php } 
              foreach ($info as $a) { ?>
                <?php if(isset($u) && $a->id==$u->id_anexo_gral) 
                  echo "<h3>".$a->nombre?></h3><br>
              <?php } ?>
              <div class="row">
                <div class="col-md-3 form-group" style="display: none">
                  <label>Anexo:</label>
                  <select class="form-control" name="id_anexo_gral" id="id_anexo_gral">
                    <?php foreach ($info as $a) {?>
                        <option value="<?php echo $a->id; ?>" <?php if(isset($u) && $a->id==$u->id_anexo_gral) { echo 'selected'; } ?> ><?php echo $a->nombre?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Identificación (UMAS)</label>
                    <input type="text" class="form-control" name="identificacion" value="<?php if(isset($u)) echo $u->identificacion ?>">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Aviso (UMAS)</label>
                    <input type="text" class="form-control" name="aviso" value="<?php if(isset($u)) echo $u->aviso ?>">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Límite de transacción en efectivo (UMAS)</label>
                    <input type="text" class="form-control" name="limite_efectivo" value="<?php if(isset($u)) echo $u->limite_efectivo ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-0 form-group">

                </div>
                <div class="col-md-2">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">Siempre
                      <input type="checkbox" class="form-check-input" id="siempre_iden" name="siempre_iden" <?php if(isset($u) && $u->siempre_iden!="0") echo "checked" ?>>
                    </label>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">Siempre
                      <input type="checkbox" class="form-check-input" id="siempre_avi" name="siempre_avi" <?php if(isset($u) && $u->siempre_avi!="0") echo "checked" ?>>
                    </label>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">NA
                      <input type="checkbox" class="form-check-input" id="na" name="na" <?php if(isset($u) && $u->na!="0") echo "checked" ?>>
                    </label>
                  </div>
                </div>
              </div>
            <?php } ?>
            <?php if(isset($u) && $u->id_anexo_gral==15 || isset($u) && $u->id_anexo_gral==16) { 

                foreach ($info as $a) { ?>
                  <?php if(isset($u) && $a->id==$u->id_anexo_gral) 
                    echo "<h3>".$a->nombre?></h3><br>
                <?php } 

              $getdat=$this->ModeloCatalogos->getselectwherestatus('*',"anexo_config",array("id_anexo_gral"=>$u->id_anexo_gral));
              $i=0;
              foreach ($getdat as $k) {
                $i++;
            ?>  
            <hr>
              <div class="row" id="anexoconfig_12s" class="anexoconfig_12s_<?php echo $k->sub_anexo; ?>">
                <input type="hidden" name="id" id="id" value="<?php echo $k->id ?>">
                <?php if($i==1) { ?>
                  <div class="col-md-12 form-group" style="display: none">
                    <div class="col-md-5 form-group">
                      <label>Anexo:</label>
                      <select class="form-control" name="id_anexo_gral" id="id_anexo_gral">
                        <?php foreach ($info as $a) {?>
                            <option value="<?php echo $a->id; ?>" <?php if(isset($u) && $a->id==$idanexo) { echo 'selected'; } ?> ><?php echo $a->nombre?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php } ?>
                <?php if($idanexo==15) { ?>
                  <?php if(isset($u) && $k->sub_anexo==1) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_1" class="form-control" rows="3" cols="70">Otorgamiento de poder irrevocable para actos de administración o actos de dominio</textarea>
                  </div> 
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="1" data-val="1" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="2" cols="70">El otorgamiento de poderes para actos de administración o dominio otorgados con carácter irrevocable</textarea>
                  </div>
                  <?php } ?>

                  <?php if(isset($u) && $k->sub_anexo==2) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_2" class="form-control" rows="1" cols="70">Constitución de personas morales</textarea>
                    <br>
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_2" class="form-control" rows="3" cols="70">Modificación patrimonial por aumento de capital o por disminución de capital</textarea>
                    <br>
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_2" class="form-control" rows="1" cols="70">Fusión</textarea>
                    <br>
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_2" class="form-control" rows="1" cols="70">Escisión</textarea>
                    <br>
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_2" class="form-control" rows="2" cols="70">Compra o venta de acciones o partes sociales</textarea>
                  </div> 
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="2" data-val="2" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="3" cols="70">La constitución de personas morales, su modificación patrimonial derivada de aumento o disminución de capital social, fusión o escisión, así como la compraventa de acciones y partes sociales de tales personas</textarea>
                  </div>
                  <?php } ?>

                  <?php if(isset($u) && $k->sub_anexo==3) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_3" class="form-control" rows="3" cols="70">Constitución o modificación de fideicomiso traslativo de dominio o en garantía sobre inmuebles</textarea>
                    <br>
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_3" class="form-control" rows="2" cols="70">Cesión de derechos de fideicomitente
                    </textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="3" data-val="3" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="3" cols="70">La constitución o modificación de fideicomisos traslativos de dominio o de garantía sobre inmuebles, salvo los que se constituyan para garantizar algún crédito a favor de instituciones del sistema financiero u organismos públicos de vivienda</textarea>
                  </div>
                  <?php } ?>

                  <?php if(isset($u) && $k->sub_anexo==4) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_4" class="form-control" rows="3" cols="70">Otorgamiento de contratos de mutuo o crédito con o sin garantía
                    </textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="4" data-val="4" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="3" cols="70">El otorgamiento de contratos de mutuo o crédito, con o sin garantía, en los que el acreedor no forme parte del sistema financiero o no sea un organismo público de vivienda</textarea>
                  </div>
                  <?php } ?>

                  <?php if(isset($u) && $k->sub_anexo==5) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_5" class="form-control" rows="1" cols="70">Realización de avalúos</textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="5" data-val="5" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="1" cols="70">La realización de avalúos sobre bienes</textarea>
                  </div>
                  <?php } ?>

                <?php } else { ?>
                  <?php if(isset($u) && $k->sub_anexo==1) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_5" class="form-control" rows="2" cols="70">Constitución o transmisión de derechos reales sobre inmuebles
                    </textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="1" data-val="1" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="3" cols="70">La transmisión o constitución de derechos reales sobre inmuebles, salvo las garantías que se constituyan en favor de instituciones del sistema financiero u organismos públicos de vivienda</textarea>
                  </div>
                  <?php } ?>
                  <?php if(isset($u) && $k->sub_anexo==2) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_5" class="form-control" rows="3" cols="70">Otorgamiento de poder irrevocable para actos de administración o para actos de dominio
                    </textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="2" data-val="2" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="2" cols="70">El otorgamiento de poderes para actos de administración o dominio otorgados con carácter irrevocable
                    </textarea>
                  </div>
                  <?php } ?>
                  <?php if(isset($u) && $k->sub_anexo==3) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_5" class="form-control" rows="1" cols="70">Constitución de personas morales
                    </textarea><br>
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_5" class="form-control" rows="3" cols="70">Modificación patrimonial por aumento de capital o por disminución de capital
                    </textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="3" data-val="3" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="6" cols="70">La constitución de personas morales mercantiles, su modificación patrimonial derivada de aumento o disminución de capital social, fusión o escisión, así como la compraventa de acciones y partes sociales de personas morales mercantiles / La constitución, modificación o cesión de derechos de fideicomiso, en los que de acuerdo con la legislación aplicable puedan actuar
                    </textarea>
                  </div>
                  <?php } ?>
                  <?php if(isset($u) && $k->sub_anexo==4) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_5" class="form-control" rows="2" cols="70">Otorgamiento de contratos de mutuo o crédito con o sin garantía
                    </textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="4" data-val="4" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="3" cols="70">El otorgamiento de contratos de mutuo mercantil o créditos mercantiles en los que de acuerdo con la legislación aplicable puedan actuar y en los que el acreedor no forme parte del sistema financiero
                    </textarea>
                  </div>
                  <?php } ?>
                  <?php if(isset($u) && $k->sub_anexo==5) { ?>
                  <div class="col-md-2 form-group">
                    <label>Operación:</label>
                    <textarea value="1" readonly id="opera_anexo_5" class="form-control" rows="1" cols="70">Realización de avalúos
                    </textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Subanexo:</label>
                    <textarea value="4" data-val="4" readonly name="sub_anexo" id="sub_anexo" class="form-control" rows="1" cols="70">La realización de avalúos sobre bienes
                    </textarea>
                  </div>
                  <?php } ?>
                <?php } ?>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Identificación (UMAS)</label>
                    <input type="text" class="form-control" id="identificacion_<?php echo $k->sub_anexo;?>" name="identificacion" value="<?php if(isset($k)) echo $k->identificacion; ?>">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Aviso (UMAS)</label>
                    <input type="text" class="form-control" id="aviso_<?php echo $k->sub_anexo;?>" name="aviso" value="<?php if(isset($k)) echo $k->aviso; ?>">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Límite de transacción en efectivo (UMAS)</label>
                    <input type="text" class="form-control" id="limite_efectivo_<?php echo $k->sub_anexo;?>" name="limite_efectivo" value="<?php if(isset($k)) echo $k->limite_efectivo; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">

                </div>
                <div class="col-md-2">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">Siempre
                      <input type="checkbox" class="form-check-input" id="siempre_iden_<?php echo $k->sub_anexo;?>" name="siempre_iden" <?php if(isset($u) && $k->siempre_iden!="0") echo "checked" ?>>
                    </label>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">Siempre
                      <input type="checkbox" class="form-check-input" id="siempre_avi_<?php echo $k->sub_anexo;?>" name="siempre_avi" <?php if(isset($u) && $k->siempre_avi!="0") echo "checked" ?>>
                    </label>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">NA
                      <input type="checkbox" class="form-check-input" id="na_<?php echo $k->sub_anexo;?>" name="na" <?php if(isset($u) && $k->na!="0") echo "checked" ?>>
                    </label>
                  </div>
                </div>
              </div>
              <hr>
            <?php } 
            } ?>
            <div class="modal-footer">
              <button type="submit" class="btn gradient_nepal2 guardarregistro"><i class="fa fa-save"></i> Guardar</button>
              <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Umbrales"><i class="fa fa-arrow-left"></i> Regresar</a>
            </div>
          </form>
          
        </div>
      </div>
    </div>
</div>
