<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 class="">Configuración - Anexos </h3>
        <hr class="subtitle">

        <div class="table-responsive">
          <table class="table" id="table_datos">
            <thead>
              <tr>
                <th></th>
                <th>Anexo</th>
                <th>Identificación (UMAS)</th>
                <th>Aviso (UMAS)</th>
                <th>Límite de transacción en efectivo (UMAS)</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>