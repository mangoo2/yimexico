<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #2b254e;
  }
</style>

<div style="display: none">
<input type="text" name="usuario">
<input type="password" name="password">
</div>
<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3 class="">Perfiles</h3>
          <hr class="subtitle">
          <div class="table-responsive">
            <table class="table" id="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th></th>  
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3 class=""><span class="titulo_text">Nuevo</span> Perfil</h3>
          <hr class="subtitle">
          <form class="form" method="post" role="form" id="form_perfil">
              <input type="hidden" name="perfilId" id="perfilId">
              <div class="row">  
                <div class="col-md-12">
                  <div class="form-group">
                      <label>Nombre de perfil</label>
                      <input type="text" class="form-control" name="perfil" id="perfil">
                  </div>
                </div>
              </div>
          </form> 
          <form>
                <div class="row" style="display: none">
                  <div class="col-md-6">
                    <?php foreach ($menu1 as $item) { ?>
                    <h5><?php echo $item->Nombre ?></h5>    
                    <?php } ?>  
                    <?php foreach ($menu_sub1 as $item) { ?>
                      <div class="form-group submenu1">
                        <div class="form-check form-check-primary">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input check_s_<?php echo $item->MenusubId ?>" id="MenusubId" value="<?php echo $item->MenusubId ?>">
                            <?php echo $item->Nombre ?>
                          </label>
                        </div>
                      </div>
                    <?php } ?>
                    <?php foreach ($menu3 as $item) { ?>
                    <h5><?php echo $item->Nombre ?></h5>    
                    <?php } ?>  
                    <?php foreach ($menu_sub3 as $item) { ?>
                      <div class="form-group submenu3">
                        <div class="form-check form-check-primary">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input check_s_<?php echo $item->MenusubId ?>" id="MenusubId" value="<?php echo $item->MenusubId ?>">
                            <?php echo $item->Nombre ?>
                          </label>
                        </div>
                      </div>
                    <?php } ?>
                    <?php foreach ($menu4 as $item) { ?>
                    <h5><?php echo $item->Nombre ?></h5>    
                    <?php } ?>  
                    <?php foreach ($menu_sub4 as $item) { ?>
                      <div class="form-group submenu4">
                        <div class="form-check form-check-primary">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input check_s_<?php echo $item->MenusubId ?>" id="MenusubId" value="<?php echo $item->MenusubId ?>">
                            <?php echo $item->Nombre ?>
                          </label>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                  <div class="col-md-6"> 
                    <?php foreach ($menu2 as $item) { ?>
                    <h5><?php echo $item->Nombre ?></h5>    
                    <?php } ?>  
                    <?php foreach ($menu_sub2 as $item) { ?>
                      <div class="form-group submenu2">
                        <div class="form-check form-check-primary">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input check_s_<?php echo $item->MenusubId ?>" id="MenusubId" value="<?php echo $item->MenusubId ?>">
                            <?php echo $item->Nombre ?>
                          </label>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>  
          </form>
          <div class="row">
            <div class="col-md-3">
              <button type="button" class="btn gradient_nepal2 guardarregistro"><span class="btn_text">Guardar Perfil</span></button>
            </div>
            <div class="col-md-9"><div class="btn_cancelar"></div></div>  
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="modaleliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Deseas eliminar el empleado <span id="titulo_texto"></span>?</h5>
      </div>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <div class="form-group" align="center">
            <label for="exampleInputUsername1">Contraseña</label>
            <input id="password" type="password" class="form-control">
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>
      <input type="hidden" id="perfilId_e">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>