<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        
        <div class="col-md-2 form-group">
          <div class="d-flex mb-1">
            <div class="input-group mb-1 mr-sm-1 mb-sm-0">
              <h3 class="">Autotización de Diagnostico de Alerta</h3>
            </div>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal_ayuda">
              <button type="button" class="btn btn-warning btn-rounded btn-icon"> <i class="mdi mdi-help"></i> </button>
            </a>              
          </div>
        </div>

        <hr class="subtitle">

          <h5 class="">Autorizar cuestionario diagnóstico de alertas</h5>
          <form id="form_alerta" class="forms-sample">
            <?php if(isset($config)){ ?>
              <input type="hidden" name="id" value="<?php echo $config->id; ?>">
            <?php } else { ?>
              <input type="hidden" name="id" value="0">
            <?php }?>
            <div class="row">
              <div class="col-lg-2">
                <label>Continuar con proceso:</label>
                <select class="form-control" id="avance" name="avance">
                  <option value=""></option>
                  <option <?php if(isset($config) && $config->avance=="1") echo "selected"; ?> value="1">Si</option>
                  <option <?php if(isset($config) && $config->avance=="0") echo "selected"; ?> value="0">No</option>
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Administracion_sistema"><i class="fa fa-reply"></i> Regresar</a>
              <button class="btn gradient_nepal2" id="btn_submit" type="submit"><i class="fa fa-save"></i> Guardar</button>
            </div>
          </form>

      
        <br><br><br><br><br>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Autorizar Diagnostico de Alerta</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>El software incluye una guía de alertamiento de conductas sospechosas, para clientes que sean catalogados como de alto y  riesgo medio. <br>
            En esta sección, el administrador decide, si algún tercero diferente al usuario que está realizando el proceso de identificación y captura del cliente, autoriza el cuestionario “diagnóstico de alertas”.

            <br>Si el administrador selecciona “si” continuar con el proceso, significa que el mismo usuario puede continuar con el proceso de identificación del cliente, sin necesidad que ningún tercero valide el cuestionario de alertamiento.
            
            <br>Si el administrador selecciona “no” significa que un tercero debe de validar y autorizar el continuar con el proceso de identificación del cliente. Si no se cuenta con dicha autorización, el usuario no puede concluir con el proceso de identificación del cliente y captura de la liquidación de la operación.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>