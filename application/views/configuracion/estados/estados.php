<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 class="">Configuración - Estados de México (Para grado de riesgo)</h3>
        <hr class="subtitle">
        <div class="table-responsive">
          <table class="table" id="table_datos">
            <thead>
              <tr>
                <th>Clave estado</th>
                <th>Nombre del estado</th>
                <th>Calificación de riesgo</th>
                <th>Consideración de riesgo</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editar Estado</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form method="post" id="form_estado">
            <input type="hidden" name="clave" id="clave">
            <div class="row">
              <div class="col-md-12">
                <h4 style="color: black">Estado: <span class="estado_nombre"></span></h4> 
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-12">
                <label>Calificación de riesgo:</label>
                <input class="form-control" type="number" name="calificacion" id="calificacion">
              </div>
            </div> 
            <div class="row">
              <div class="col-md-12">
                <label>Consideración de riesgo:</label>
                <select class="form-control" name="riesgo" id="riesgo">
                </select>
              </div>
            </div> 
          </form>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="editar()">Guardar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>