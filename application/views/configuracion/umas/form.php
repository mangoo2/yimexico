<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3 >UMAS</h3>
          <hr class="subtitle">
          <form class="form" method="post" role="form" id="form_umas">
            <?php if(isset($umas)) { ?>
          	<input type="hidden" name="id" value="<?php echo $umas->id ?>">
            <?php }else{ ?>
              <input type="hidden" name="id" value="0">
            <?php } ?>
            <div class="row">
              <!--<div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputUsername1">Año</label>
                  <input type="text" pattern="\d*" minlength="4" maxlength="4" class="form-control" name="anio" value="<?php if(isset($umas)) echo $umas->anio ?>">
                </div>
              </div>-->
              <div class="col-md-3 form-group">
                <label>Año</label>
                <select class="form-control" id="anio" name="anio">
                </select>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputUsername1">Valor:</label>
                  <input type="number" class="form-control" name="valor" value="<?php if(isset($umas)) echo $umas->valor ?>">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn gradient_nepal2 guardarregistro"><i class="fa fa-save"></i> Guardar</button>
              <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Umas"><i class="fa fa-arrow-left"></i> Regresar</a>
            </div>
          </form>
          
        </div>
      </div>
    </div>
</div>
