<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 class="">Configuración - UMAS </h3>
        <hr class="subtitle">
        <div class="row">
          <div class="col-lg-12">
            <p class="card-description">
              <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Umas/alta"><i class="fa fa-gears"></i> Nueva UMA</a>
            </p>
          </div>
        </div>
        
        <div class="table-responsive">
          <table class="table" id="table_datos">
            <thead>
              <tr>
                <th>Año</th>
                <th>Valor</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>