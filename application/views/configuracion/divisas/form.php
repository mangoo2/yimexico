<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3 >Divisas</h3>
          <hr class="subtitle">
          <form class="form" method="post" role="form" id="form_divisas">
            <?php if(isset($d)) { ?>
          	<input type="hidden" name="id" value="<?php echo $d->id ?>">
            <?php }else{ ?>
              <input type="hidden" name="id" value="0">
            <?php } ?>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputUsername1">Fecha</label>
                  <input type="date" class="form-control" name="fecha" value="<?php if(isset($d)) echo $d->fecha ?>">
                </div>
              </div>
              <div class="col-md-4 form-group">
                <label>Tipo Moneda</label>
                <select class="form-control" name="moneda">
                  <option value="1" <?php if(isset($d) && $d->moneda=="1"){ echo "selected"; }?>>Dólar Americano</option>
                  <option value="2" <?php if(isset($d) && $d->moneda=="2"){ echo "selected"; }?>>Euro</option>
                  <option value="3" <?php if(isset($d) && $d->moneda=="3"){ echo "selected"; }?>>Libra Esterlina</option>
                  <option value="4" <?php if(isset($d) && $d->moneda=="4"){ echo "selected"; }?>>Dólar canadiense</option>
                  <option value="5" <?php if(isset($d) && $d->moneda=="5"){ echo "selected"; }?>>Yuan Chino</option>
                  <option value="6" <?php if(isset($d) && $d->moneda=="6"){ echo "selected"; }?>>Centenario</option>
                  <option value="7" <?php if(isset($d) && $d->moneda=="7"){ echo "selected"; }?>>Onza de Plata</option>
                  <option value="8" <?php if(isset($d) && $d->moneda=="8"){ echo "selected"; }?>>Onza de Oro</option>
                </select>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Valor</label>
                  <input type="text" class="form-control" name="valor" value="<?php if(isset($d)) echo $d->valor ?>">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn gradient_nepal2 guardarregistro"><i class="fa fa-save"></i> Guardar</button>
              <a class="btn gradient_nepal2 pull-right" href="<?php echo base_url(); ?>Divisas"><i class="fa fa-arrow-left"></i> Regresar</a>
            </div>
          </form>
          
        </div>
      </div>
    </div>
</div>
