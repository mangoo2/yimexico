<style type="text/css">
  .owl-dots{
    display:none;
  }
</style>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-3">
            <h3 class="">Configuración - Divisas </h3>
          </div>
          <div class="col-lg-9" align="right">
            <?php if($this->session->userdata("tipo")==1){ ?>
              <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Divisas/alta"><i class="fa fa-usd"></i> Registro manual</a>
            <?php } ?>
          </div> 
        </div>

        <hr class="subtitle">
        <!-- -->
        <div class="row grid-margin">
          <div class="col-lg-12">
            <div class="owl-carousel owl-theme loop">
              <div class="item">
              <!-- 1 Dólar Americano -->
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-lg-3">
                            <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                            <i class="flag-icon flag-icon-us" style="font-size: 35px" title="us" id="us"></i>
                        </div>
                        <div class="col-md-9" align="center">
                            <b class="text_yi">Dólar Americano </b>

                        </div>
                        <div class="col-lg-12" align="center">
                          <br>
                          <b>$<?php echo $dolar_precio ?></b> <span> MNX</span>
                          <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                          <span><?php if($dolar_fecha!='') echo date('d-m-Y',strtotime($dolar_fecha)) ?></span>
                          <br>
                          <span style="font-size: 9px;">Última actualización </span>
                        </div>  
                      </div>  
                    </div>
                  </div> 
              <!-- -->        
              </div>
              <div class="item">
              <!-- 4 Dólar canadiense -->
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-3">
                          <!--<i style="font-size: 41px" class="fa fa-dollar text_yi2"></i>-->
                          <i class="flag-icon flag-icon-ca" style="font-size: 35px" title="ca" id="ca"></i>
                      </div>
                      <div class="col-md-9" align="center">
                          <b class="text_yi2">Dólar canadiense </b>
                      </div>
                      <div class="col-lg-12" align="center">
                        <br>
                        <b>$<?php echo $dolar_cprecio ?></b> <span> MNX</span>
                        <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                        <span><?php if($dolar_cfecha!='') echo date('d-m-Y',strtotime($dolar_cfecha)) ?></span>
                        <br>
                        <span style="font-size: 9px;">Última actualización </span>
                      </div>  
                    </div>  
                  </div>
                </div>
              <!-- -->
              </div>
              <div class="item">
              <!-- 2 Euro -->
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-3">
                          <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                          <i class="flag-icon flag-icon-es" style="font-size: 35px" title="fm" id="fm"></i>
                      </div>
                      <div class="col-md-9" align="center">
                          <b class="text_yi">Euro </b>
                      </div>
                      <div class="col-lg-12" align="center">
                        <br>
                        <b>$<?php echo $euro_precio ?></b> <span> MNX</span>
                        <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                        <span><?php if($euro_fecha!='') echo date('d-m-Y',strtotime($euro_fecha)) ?></span>
                        <br>
                        <span style="font-size: 9px;">Última actualización </span>
                      </div>  
                    </div>  
                  </div>
                </div>
              <!-- -->
              </div>
              <div class="item">
              <!-- 3 Libra Esterlina -->
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-3">
                          <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                          <i class="flag-icon flag-icon-gb" style="font-size: 35px" title="gb" id="gb"></i>
                      </div>
                      <div class="col-md-9" align="center">
                          <b class="text_yi">Libra Esterlina </b>
                      </div>
                      <div class="col-lg-12" align="center">
                        <br>
                        <b>$<?php echo $libra_precio ?></b> <span> MNX</span>
                        <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                        <span><?php if($libra_fecha!='') echo date('d-m-Y',strtotime($libra_fecha)) ?></span>
                        <br>
                        <span style="font-size: 9px;">Última actualización </span>
                      </div>  
                    </div>  
                  </div>
                </div>
              <!-- -->
              </div>
              <div class="item">
              <!-- 5 Yuan Chino -->
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-3">
                          <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                          <i class="flag-icon flag-icon-cn" style="font-size: 35px" title="gb" id="gb"></i>
                      </div>
                      <div class="col-md-9" align="center">
                          <b class="text_yi">Yuan Chino </b>
                      </div>
                      <div class="col-lg-12" align="center">
                        <br>
                        <b>$<?php echo $chino_precio ?></b> <span> MNX</span>
                        <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                        <span><?php if($chino_fecha!='') echo date('d-m-Y',strtotime($chino_fecha)) ?></span>
                        <br>
                        <span style="font-size: 9px;">Última actualización </span>
                      </div>  
                    </div>  
                  </div>
                </div>
              <!-- -->
              </div>
              
            </div>
          </div>
        </div> 
        <!-- -->
        

        <div class="table-responsive">
          <div class="col-md-3 form-group">
            <label>Tipo Moneda</label>
            <select class="form-control" id="moneda">
              <option value="1">Dólar Americano</option>
              <option value="2">Euro</option>
              <option value="3">Libra Esterlina</option>
              <option value="4">Dólar canadiense</option>
              <option value="5">Yuan Chino</option>
              <option value="6">Centenario</option>
              <option value="7">Onza de Plata</option>
              <option value="8">Onza de Oro</option>
              <option value="0">Todos</option>
            </select>
          </div>
          <table class="table" id="table_datos">
            <thead>
              <tr>
                <th>#</th>
                <th>Fecha</th>
                <th>Moneda</th>
                <th>Valor</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Estadisticas"><i class="fa fa-reply"></i> Regresar</a>
        </div>
      </div>
    </div>
  </div>
</div>