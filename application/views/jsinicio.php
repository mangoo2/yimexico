<link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
<script src="<?php echo base_url(); ?>template/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>template/vendors/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/inicio.js?v=<?php echo date('Ymdgis') ?>" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/js/divcambio.js?v=<?php echo date('Ymdgis') ?>" type="text/javascript"></script>