<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Yi México</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
   <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/select2/select2.min.css">
  <link href="<?php echo base_url(); ?>template/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>template/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/css/vertical-layout-light/style.css">
  <!--estilo oxxo -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/css/estilo_css.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>template/images/iconpro.png" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/dist/sweetalert.css">
  <!-- 24-03-2021 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/flag-icon-css/css/flag-icon.min.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/owl-carousel-2/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/owl-carousel-2/owl.theme.default.min.css">
  <!-- 24-03-2021 fin -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <!-- Animation Css -->
  <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@X.X.X/dist/select2-bootstrap4.min.css">

  <script type="text/javascript">
    var RutaBase="<?php echo base_url(); ?>";
  </script>
</head>
<style>
  table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting{
  background-color: #b07e49;
  color:white;
  font-weight: bold;
  }
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #5e2572 ;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<style type="text/css">
  .gradient_nepal {
    background-image: linear-gradient(177deg, #b17f4a, #212b4c) !important;
    background-repeat: repeat-x !important;
    border-radius: 60px;
    color: white!important;
    font-size: 24px !important;
    border-color: #b17f4a !important;
  }
  .gradient_nepal2 {
    background-image: linear-gradient(177deg, #b17f4a, #212b4c) !important;
    background-repeat: repeat-x !important;
    border-radius: 60px;
    color: white!important;
    border-color: #b17f4a !important;
    font-weight: bold !important;
  }
  .icon_yi{
    font-size: 55px !important;
  }
  .icon_yi2{
    font-size: 35px !important;
  }
  .texto_centro{
    align-items: center;display: flex;
  }
  .tam_btn{
    width: 277px; height: 100px
  }
  .tam_btn2{
    width: 293px; height: 100px
  }
  .btn_borrar{
    color: #fff;
    background-color: #b17f4a;
    border-color: #b17f4a;
    border-radius: 23px;
  }
  .btn_borrar2{
    color: #fff;
    background-color: #b17f4a;
    border-color: #b17f4a;
    border-radius: 23px;
  }
  .card{
    border: 3px solid #212b4c;
    border-radius: 15px;
  }
  .sidebar {
    background: #293353 !important;
  }
  .sidebar .nav .nav-item.active > .nav-link {
      background: #b17f4a;
  }
  .sidebar .nav:not(.sub-menu) > .nav-item:hover > .nav-link {
    background: #b17f4a;
  }
   
  .btn_yimexicov2 {
    color: #fff;
    background-color: #2b254e;
    border-color: #000000;
    border-radius: 60px;
  } 

  input{
    border-radius: 24px !important;
    border-color: #b57532 !important;
  }
  h3{
    font-size: 19px !important;
  }
  h4 {
    font-size: 19px !important;
  }
  select{
    color: #b57532 !important;
    border-radius: 60px !important;
    border-color: #b57532 !important;
    outline: 0px solid #e9e8ef00 !important; 
    border: 1px solid #b57532 !important;
  }
  .menu-icon{
    font-size: 30px !important;
  }
  @media (min-width: 992px){
      .sidebar-icon-only .sidebar .nav .nav-item .nav-link {
      padding-right: 2.2rem !important;
  }
  .sidebar-icon-only .sidebar .nav .nav-item.hover-open .collapse, .sidebar-icon-only .sidebar .nav .nav-item.hover-open .collapsing {
    background: #293353;
  }
  .sidebar-icon-only .sidebar .nav .nav-item.hover-open .nav-link:hover .menu-title {
    background: #293353;
  }
  .form-check .form-check-label input[type="radio"]:checked + .input-helper:before {
    background: #293353;
  }
  .form-check .form-check-label input[type="radio"] + .input-helper:before {
      border: solid #293353;
  }
  .text_yi{
    color: #b17f4a;
  }
  .text_yi2{
    color: #212b4c;
  }
  .btn_yi_admin_tm{
    width: 231px;
    height: 60px;
  }
  .px_letra{
    font-size: 19px !important;
  }
  .btn-warning {
      color: white !important;
      background-color: #b17f4a !important;
      border-color: #b17f4a !important;
  }
  .btn-warning, .btn-warning:hover {
      box-shadow: 0 2px 2px 0 rgb(37 41 76), 0 3px 1px -2px rgb(37 41 76), 0 1px 5px 0 rgb(50 53 86);
  }
  .btn_ayuda{
    width: 42px;
    height: 42px;
    padding: 0;
    border-radius: 50px;
    color: white;
    background-color: #b17f4a !important;
    border-color: #b17f4a;
  }
  .vd_green{
    color: green;
  }
  .select2-selection{
    color: #b57532 !important;
    border-radius: 60px !important;
    border-color: #b57532 !important;
    outline: 0px solid #e9e8ef00 !important; 
    border: 1px solid #b57532 !important;
  }
  .select2-selection__rendered{
    color: #b57532 !important;
  }
  .select2-container--open{
    color: #b57532 !important;
  }
</style>

<body class="sidebar-icon-only">
<div style="display: none">
<input type="text" name="usuario">
<input type="password" name="password">
<input type="hidden" id="tipo" value="<?php echo $this->session->userdata('tipo'); ?>">
</div>
  <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
  <div class="container-scroller">

