
  <!-- plugins:js -->
  <script src="<?php echo base_url(); ?>template/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <script src="<?php echo base_url(); ?>template/dist/sweetalert.js"></script>
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?php echo base_url(); ?>template/js/off-canvas.js"></script>
  <script src="<?php echo base_url(); ?>template/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url(); ?>template/js/template.js"></script>
  <script src="<?php echo base_url(); ?>template/js/settings.js"></script>
  <script src="<?php echo base_url(); ?>template/js/todolist.js"></script>

  <script src="<?php echo base_url(); ?>template/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/select2/select2.min.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/inputmask/jquery.inputmask.bundle.js"></script>
  <!-- endinject -->
  <script src="<?php echo base_url(); ?>template/fileinput/fileinput.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>template/vendors/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
  <!-- Custom js for this page-->
  <script src="<?php echo base_url(); ?>template/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>

  <!-- 24-03-2021-->
  
  <!-- 24-03-2021 fin-->
  <!-- End custom js for this page-->
  <script src="<?php echo base_url(); ?>public/js/alert_cliente_cliente.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/owl-carousel-2/owl.carousel.js"></script>
</body>

</html>