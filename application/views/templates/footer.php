    </div>
    <footer class="footer barra_menu">
      <div style="text-align: center;">
        <span>Copyright © <?php echo date("Y") ?> <a href="http://mangoo.mx/" target="_blank"> <img style="width: 100px; " src="<?php echo base_url(); ?>public/img/logo2.jpg"> </a> Se recomienda uso de navegador Chrome para su mejor funcionamiento, PLD V 3.1</span>
      </div>
    </footer>
    <!-- partial -->
  </div>
  <!-- main-panel ends -->
</div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url(); ?>template/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <script src="<?php echo base_url(); ?>template/dist/sweetalert.js"></script>
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?php echo base_url(); ?>template/js/off-canvas.js"></script>
  <script src="<?php echo base_url(); ?>template/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url(); ?>template/js/template.js"></script>
  <script src="<?php echo base_url(); ?>template/js/settings.js"></script>
  <script src="<?php echo base_url(); ?>template/js/todolist.js"></script>

  <script src="<?php echo base_url(); ?>template/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/select2/select2.min.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/inputmask/jquery.inputmask.bundle.js"></script>
  <!-- endinject -->
  <script src="<?php echo base_url(); ?>template/fileinput/fileinput.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>template/vendors/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
  <!-- Custom js for this page-->
  <script src="<?php echo base_url(); ?>template/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>

  <!-- 24-03-2021-->
  
  <!-- 24-03-2021 fin-->
  <!-- End custom js for this page-->
  <script src="<?php echo base_url(); ?>public/js/alert_cliente_cliente.js"></script>
  <script src="<?php echo base_url(); ?>template/vendors/owl-carousel-2/owl.carousel.js"></script>
  <script src="https://momentjs.com/downloads/moment.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
  
  <script >
    $(".table").addClass("table-striped table-bordered dt-responsive");
    var p1=0;
    var p1_aux=0;
    function pregunta1(){
      if(p1==p1_aux){
        p1_aux=1;
        $('.txt_pregunta1').show(1000);
      }else{
        p1_aux=0;
        $('.txt_pregunta1').hide(1000);
      }
    } 
    var p2=0;
    var p2_aux=0;
    function pregunta2(){
      if(p2==p2_aux){
        p2_aux=1;
        $('.txt_pregunta2').show(1000);
      }else{
        p2_aux=0;
        $('.txt_pregunta2').hide(1000);
      }
    } 
    
    if ($('.loop').length) {
      $('.loop').owlCarousel({
        center: true,
        items: 2,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 8500,
        responsive: {
          600: {
            items: 4
          }
        },
        pagination:true
      });
    }
    
  </script>

</body>

</html>