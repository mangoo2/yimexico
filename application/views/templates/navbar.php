<?php
  if (!$this->session->userdata('logeado')) {
    redirect('Login');
    $perfilid =0;
  }else{
    $perfilid=$this->session->userdata('perfilid');
    $personalId=$this->session->userdata('idpersonal');
    $idcliente=$this->session->userdata('idcliente');
    $idcliente_usuario=$this->session->userdata('idcliente_usuario');
    $tipo=$this->session->userdata('tipo');
    $tipo_persona=$this->session->userdata('tipo_persona');
    //echo "tipo : ".$tipo;
    $menu=$this->Login_model->getMenus($perfilid);
    $persona=$this->Login_model->getpersona($personalId,$idcliente,$idcliente_usuario,$tipo);
    if($tipo==1){
      foreach ($persona as $item) {
        $nombre=$item->nombre;
        if($item->name_emp!=""){
          $nombre=$item->name_emp."<br>".$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }if($item->razon_social!=""){
          $nombre=$item->razon_social."<br>".$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }if($item->denominacion_razon_social!=""){
          $nombre=$item->denominacion_razon_social."<br>".$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }
        $perfil=$item->perfil;
        $funcion_puesto = $item->funcion_puesto;
        if($item->idcliente_usuario==0){
          $funcion_puesto="Director General";
        }
    }
    }else if($tipo==3){  //usuario admin de usuarios
      if($tipo_persona==1){
        foreach ($persona as $item) { 
          $nombre=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
          $perfil=$item->perfil;
          $funcion_puesto = $item->funcion_puesto;
          if($item->idcliente_usuario==0){
            $funcion_puesto="Director General";
          }
        }
      }else if($tipo_persona==2){
        foreach ($persona as $item) { 
          $nombre=$item->razon_social;
          $perfil=$item->perfil;
          $funcion_puesto = $item->funcion_puesto;
          if($item->idcliente_usuario==0){
            $funcion_puesto="Director General";
          }
        }
      }else if($tipo_persona==3){
        foreach ($persona as $item) { 
          $nombre=$item->denominacion_razon_social;
          $perfil=$item->perfil;
          $funcion_puesto = $item->funcion_puesto;
          if($item->idcliente_usuario==0){
            $funcion_puesto="Director General";
          }
        }
      }  
    }else if($tipo==4){
      foreach ($persona as $item) {
        //log_message('error', 'razon_social: '.$item->razon_social); 
        $nombre=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        if($item->name_emp!=""){
          $nombre=$item->name_emp."<br>".$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }if($item->razon_social!=""){
          $nombre=$item->razon_social."<br>".$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }if($item->denominacion_razon_social!=""){
          $nombre=$item->denominacion_razon_social."<br>".$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }
        $perfil=$item->perfil;
        $funcion_puesto = $item->funcion_puesto;
        if($item->idcliente_usuario==0){
          $funcion_puesto="Director General";
        }
      }
    }
    $data = array(
          'nombre_user_log' => $nombre
      );
      $this->session->set_userdata($data);
  }
  ////////////////////////////////////////////////////////////////////////////////////
  /*
  69-
  64-
  65-
  68-
  66-
  67-
  */
  ////////////////////////////////////////////////////////////////////////////////////
?>
<style type="text/css">
  @media (min-width: 991px){
    .logopos{
      position: absolute;
      margin-left: 20px;
    }
  }
  /*
  .texto_icono{
      position: absolute;
      margin-top: 29px;
    }
  */
</style>
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row barra_menu">
  <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between barra_menu">
    <ul class="navbar-nav mr-lg-2 d-none d-lg-flex barra_menu">
      <li class="nav-item nav-toggler-item">
      <?php if($perfilid<3){ ?>
        <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
      <?php  } ?> 
      </li>
    </ul>
    <?php
      $direccion=''; 
      if($perfilid<3){ 
          $direccion='Inicio';
      }else{
          $direccion='Inicio_cliente';
      } ?> 
      <!-- style="height: 64px !important;" -->
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center barra_menu logopos" >
      <a class="navbar-brand brand-logo barra_menu" id="logo_men" href="<?php echo base_url()?><?php echo $direccion ?>"><img style="height: 64px !important;" src="<?php echo base_url()?>public/img/crm.png" alt="logo"/></a>
      <a class="navbar-brand brand-logo-mini barra_menu" id="logo_men" href="<?php echo base_url()?> "><img style="height: 64px !important;" src="<?php echo base_url()?>public/img/logo2.jpg" alt="logo"/></a>
    </div>
    <ul class="navbar-nav navbar-nav-right barra_menu">
    <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==8 || $perfilid==9 || $perfilid==10){//Director // R.cumplimiento //Supervisor // E.comercil // E.operacion  ?>
      <li class="nav-item dropdown">
        <img style="width: 150px;" src="<?php echo base_url();?>public/img/not.png">
        <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
          <i class="mdi mdi-bell-outline mx-0"></i>
          <span class="count"></span>
        </a>
        <?php 
            $aux = 0;
            
              date_default_timezone_set('America/Mexico_City');
              $this->fechainicio = date('Y-m-d');
              //sumo 3 día
              $fechafin = date("Y-m-d",strtotime($this->fechainicio."+ 3 days")); 
              $get_transaccion = $this->Login_model->verificar_trasacciones($idcliente,$this->fechainicio,$fechafin);
              foreach ($get_transaccion as $item) {
                $aux++; 
              }
        ?>    
        <div style="height:555px; overflow: scroll;" class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
          <a class="dropdown-item">
            <p class="mb-0 font-weight-normal float-left">Tienes <?php echo $aux ?> notificación<?php if ($aux>1) echo 'es'?></p>
            <span class="badge badge-pill badge-warning float-right">Ver todo</span>
          </a> 
          
          <?php 
           if ($this->session->userdata('logeado')==true) {
            //ver si hay operaciones con grado algo por autorizar si es que no permite avanzar la config de diag de alertas
            $conf_alert=$this->General_model->get_tableRow("config_alerta",array("id_cliente"=>$idcliente));
            if(isset($conf_alert->avance)){
              $avanza=$conf_alert->avance;  
            }else{
              $avanza=0;
            }
            //$get_op=$this->ModeloCliente->operaciones_vista($this->idcliente);
            if($avanza==0){
              $get_op=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_cliente"=>$idcliente,"activo"=>1));
              $opera_aux=0;
              foreach ($get_op as $k) {
                $get_cg=$this->ModeloCatalogos->getselectwherestatus("*","grado_riesgo_actividad",array("id_operacion"=>$k->id_operacion,"id_perfilamiento"=>$k->id_perfilamiento));
                foreach ($get_cg as $k2) {
                  if($k2->grado>2){
                    $get_ao=$this->General_model->get_tableRow("autoriza_operacion",array("id_operacion"=>$k2->id_operacion));
                    if(!isset($get_ao) && $k2->id_operacion!=$opera_aux){
                      echo '<div class="dropdown-divider"></div>
                      <a class="dropdown-item preview-item" href="'.base_url().'Operaciones/procesoInicial/'.$k2->id_operacion.'">
                        <div class="preview-thumbnail">
                          <div class="preview-icon" style="background-color: #0ca7d6; color:white">
                            <i class="fa fa-usd mx-0"></i>
                          </div>
                        </div>
                        <div class="preview-item-content">
                          <h6 class="preview-subject font-weight-medium">Operaciones pendientes de autorizar</h6>
                          <p class="font-weight-light small-text mb-0">
                            Operación: '.$k2->id_operacion.'

                          </p>
                        </div>
                      </a>';
                    }
                    $opera_aux=$k2->id_operacion;
                  }

                }
              }//foreach operaciones lista
            }//if de no permite avanzar
          }
          ?>
              
              <?php //if($perfil==8 || $perfil==9 || $perfil==10){//Supervisor // E.comercil // E.operacion
              foreach ($get_transaccion as $item) { 
                if($item->validado==0){
                  ?>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item" onclick="href_cliente_cliente()">
                    <div class="preview-thumbnail">
                      <div class="preview-icon" style="background-color: #4a71de; color:white">
                        <i class="fa fa-briefcase mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-medium">Faltan documentos por validar</h6>
                      <p class="font-weight-light small-text mb-0">
                      <!-- -->
                      <?php
                      $auxc = 'Cliente: ';
                      if($item->idtipo_cliente==1){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_f_m = $this->ModeloCatalogos->getselectwherestatus('nombre, apellido_paterno, apellido_materno','tipo_cliente_p_f_m',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_f_m as $item) {
                        echo $auxc.' '.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
                      }
                      }else if($item->idtipo_cliente==2){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_f_e = $this->ModeloCatalogos->getselectwherestatus('nombre, apellido_paterno, apellido_materno','tipo_cliente_p_f_e',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_f_e as $item) {
                        echo $auxc.' '.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
                        }
                      }else if($item->idtipo_cliente==3){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_m_m_e = $this->ModeloCatalogos->getselectwherestatus('razon_social','tipo_cliente_p_m_m_e',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_m_m_e as $item) {
                        echo $auxc.' '.$item->razon_social;
                        }
                      }else if($item->idtipo_cliente==4){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_m_m_d = $this->ModeloCatalogos->getselectwherestatus('nombre_persona','tipo_cliente_p_m_m_d',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_m_m_d as $item) {
                        echo $auxc.' '.$item->nombre_persona;
                        }
                      }else if($item->idtipo_cliente==5){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_e_c_o_i = $this->ModeloCatalogos->getselectwherestatus('denominacion','tipo_cliente_e_c_o_i',$array_tipo_cliente);
                        foreach ($tipo_cliente_e_c_o_i as $item) {
                        echo $auxc.' '.$item->denominacion;
                        }
                      }else if($item->idtipo_cliente==6){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_f = $this->ModeloCatalogos->getselectwherestatus('denominacion','tipo_cliente_f',$array_tipo_cliente);
                        foreach ($tipo_cliente_f as $item) {
                        echo $auxc.' '.$item->denominacion;
                        }
                      }
                    ?>
                      <!-- -->
                      </p>
                    </div>
                  </a> 
              <?php }else{ ?>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item preview-item" onclick="href_cliente_cliente()">
                    <div class="preview-thumbnail">
                      <div class="preview-icon" style="background-color: #0ca7d6; color:white">
                        <i class="fa fa-usd mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-medium">Ya puedes hacer una transacción</h6>
                      <p class="font-weight-light small-text mb-0">
                      <!-- -->
                      <?php
                      $auxc = 'Cliente: ';
                      if($item->idtipo_cliente==1){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_f_m = $this->ModeloCatalogos->getselectwherestatus('nombre, apellido_paterno, apellido_materno','tipo_cliente_p_f_m',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_f_m as $item) {
                        echo $auxc.' '.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
                        }
                      }else if($item->idtipo_cliente==2){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_f_e = $this->ModeloCatalogos->getselectwherestatus('nombre, apellido_paterno, apellido_materno','tipo_cliente_p_f_e',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_f_m as $item) {
                        echo $auxc.' '.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
                        }
                      }else if($item->idtipo_cliente==3){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_m_m_e = $this->ModeloCatalogos->getselectwherestatus('razon_social','tipo_cliente_p_m_m_e',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_m_m_e as $item) {
                        echo $auxc.' '.$item->razon_social;
                        }
                      }else if($item->idtipo_cliente==4){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_p_m_m_d = $this->ModeloCatalogos->getselectwherestatus('nombre_persona','tipo_cliente_p_m_m_d',$array_tipo_cliente);
                        foreach ($tipo_cliente_p_m_m_d as $item) {
                        echo $auxc.' '.$item->nombre_persona;
                        }
                      }else if($item->idtipo_cliente==5){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_e_c_o_i = $this->ModeloCatalogos->getselectwherestatus('denominacion','tipo_cliente_e_c_o_i',$array_tipo_cliente);
                        foreach ($tipo_cliente_e_c_o_i as $item) {
                        echo $auxc.' '.$item->denominacion;
                        }
                      }else if($item->idtipo_cliente==6){
                        $array_tipo_cliente = array('idperfilamiento' =>$item->idperfilamiento);
                        $tipo_cliente_f = $this->ModeloCatalogos->getselectwherestatus('denominacion','tipo_cliente_f',$array_tipo_cliente);
                        foreach ($tipo_cliente_f as $item) {
                        echo $auxc.' '.$item->denominacion;
                        }
                      }
                    ?>
                      <!-- -->
                      </p>
                    </div>
                  </a> 
              <?php }
              }
            //}
          ?>
         
        </div>
      </li>
    <?php } ?>
      <li class="nav-item nav-profile dropdown barra_menu">
        <a class="nav-link" href="<?php echo base_url()?>Divisas" target="_blanck">
          <span class="nav-profile-name"><i class="fa fa-money fa-3x" arian-hidden="true"></i> Divisas</span>
        </a>
      </li>
      <li class="nav-item nav-profile dropdown barra_menu">
        <a class="nav-link dropdown-toggle iconpro" href="#" data-toggle="dropdown" id="profileDropdown">
           <img src="<?php echo base_url()?>template/images/iconpro.png" alt="profile"/>
          <!--<span class="nav-profile-name nombre_user_log"><?php echo $nombre.' ' ?> </span><span>| <?php echo $perfil ?></span>-->
          <span class="nav-profile-name nombre_user_log"><?php echo $nombre.' ' ?> </span><span>| <?php echo $funcion_puesto ?></span>
          
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Login/cerrar_sesion">
            <i class="mdi mdi-logout text-primary"></i>
            Cerrar Sesión
          </a>
        </div>
      </li>
      <li class="nav-item nav-toggler-item-right d-lg-none">
        <button class="navbar-toggler align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </li>
    </ul>
  </div>
</nav>
<!-- partial -->
<div class="container-fluid page-body-wrapper">
  <!-- partial:partials/_settings-panel.html -->
  <div class="theme-setting-wrapper barra_menu">
    <div id="settings-trigger" style="width: 99px;"> 
      <i class="mdi mdi-settings"></i>
      <b style="color: white">Soporte</b>
    </div>


    <div id="theme-settings" class="settings-panel">
      <div class="row">
        <div class="col-lg-1" align="center"></div>
        <div class="col-lg-9" align="center">
          <img style="height: 64px !important;" src="<?php echo base_url()?>public/img/logo2.jpg" alt="logo"/>
        </div>
        <div class="col-lg-1">
          <i class="settings-close mdi mdi-close"></i>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
          <p style="padding: 16px 0 13px;
            font-size: 0.875rem;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            line-height: 1;
            color: rgba(0, 0, 0, 0.9);
            opacity: 0.9;
            margin-bottom: 0;
            border-top: 1px solid #e9e8ef;
            border-bottom: 1px solid #e9e8ef;">
            Soporte Yi México:
          </p>
        </div>
      </div>  
      <!-- 
        
      -->
      <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10"><br>
          <a href="mailto:soporte@yimexico.mx" target="_block">Correo: soporte@yimexico.mx</a>
          <br><br>
          <a href="https://api.whatsapp.com/send?phone=+5219531251381" target="_block" title="9531251381">
            <img style="width: 25px;" src="<?php echo base_url() ?>public/img/whatsapp.png"> 
            <span>9531251381</span>
          </a>
          <br><br>
          <a href="https://api.whatsapp.com/send?phone=+5212224231004" target="_block" title="2224231004">
            <img style="width: 25px;" src="<?php echo base_url() ?>public/img/whatsapp.png">
            <span>2224231004</span>
          </a>
          <br><br>
        </div>
      </div> 
      <div class="col-lg-10"><br>
        <p style="padding: 16px 0 13px;
            font-size: 0.875rem;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            line-height: 1;
            color: rgba(0, 0, 0, 0.9);
            opacity: 0.9;
            margin-bottom: 0;
            border-top: 1px solid #e9e8ef;
            border-bottom: 1px solid #e9e8ef;">
            Consulta valores:
          
          <a class="nav-link" href="<?php echo base_url()?>Divisas" target="_blanck">
            <span class="nav-profile-name"><i class="fa fa-money fa-2x" arian-hidden="true"></i> Divisas</span>
          </a> 
        </p>
      </div>
      <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
          <p style="padding: 16px 0 13px;
            font-size: 0.875rem;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            line-height: 1;
            color: rgba(0, 0, 0, 0.9);
            opacity: 0.9;
            margin-bottom: 0;
            border-top: 1px solid #e9e8ef;
            border-bottom: 1px solid #e9e8ef;">
            Preguntas frecuentes:
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10" style="overflow-x: hidden;">
          <br>
          <a type="button" style="cursor: pointer;" onclick="pregunta1()"><b><i class="fa fa-bullseye"></i> ¿Cómo realizar una transacción?</b></a>
            <p class="txt_pregunta1" style="display: none">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          <br><br>  
          <a type="button" style="cursor: pointer;" onclick="pregunta2()"><b><i class="fa fa-bullseye"></i> ¿Cómo descargar un XML?</b></a> 
          <p class="txt_pregunta2" style="display: none">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          
        </div>

      </div>  
    </div>
  </div>
  <!-- partial -->
  <!-- partial:partials/_sidebar.html -->
<?php if($perfilid<3){ ?>
  <nav class="sidebar sidebar-offcanvas barra_menu" id="sidebar">
    <ul class="nav">
      <?php foreach ($menu as $item_menu) { 
          $menusub=$this->Login_model->submenus($perfilid,$item_menu->MenuId);
        ?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#menu_<?php echo $item_menu->MenuId;?>" aria-expanded="false" aria-controls="menu_<?php echo $item_menu->MenuId;?>">
              <i class="mdi <?php echo $item_menu->Icon;?> menu-icon"></i>
              <span class="menu-title"><?php echo $item_menu->Nombre;?></span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="menu_<?php echo $item_menu->MenuId;?>">
              <ul class="nav flex-column sub-menu">
                <?php foreach ($menusub as $item_menusub) { ?>
                  <li class="nav-item">
                    <a class="nav-link" style="<?php echo $item_menusub->activar ?>" href="<?php echo base_url().$item_menusub->Pagina;?>"><i style="font-size: 20px" class="<?php echo $item_menusub->Icon;?>"></i>&nbsp;&nbsp;&nbsp;<?php echo $item_menusub->Nombre;?></a>
                  </li>
                <?php  } ?>
              </ul>
            </div>
          </li>
      <?php  } ?>
    </ul>
  </nav>
<?php }
$aux_text = ""; 
if($perfilid<3){ ?>
<?php }else{ ?>
  <style type="text/css">
    .sidebar-icon-only .main-panel {
    width: calc(100%);
    }
  </style>
<?php }  ?>
  <!-- partial -->

  <div class="main-panel">
      <div class="content-wrapper">
