<style type="text/css">
  .auth .login-half-bg {
    background: url("<?php echo base_url(); ?>public/img/back.jpg");
    background-size: cover;
    background-position: center;
  }
  .content-wrapper{
    background: white;
  }
</style>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
        <div class="row flex-grow">
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="brand-logo">
                <img src="<?php echo base_url(); ?>public/img/logo2.png" whidth="150px" height="150px" alt="logo">
              </div>
              <h4>¡Bienvenido!</h4>
              <h6 class="font-weight-light"></h6>
              <form class="pt-4" id="login-form" >
                <div class="form-group">
                  <label for="exampleInputEmail">Usuario</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control form-control-lg border-left-0" id="txtUsuario" placeholder="Usuario">
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword">Contraseña</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-lock-outline text-primary"></i>
                      </span>
                    </div>
                    <input type="password" class="form-control form-control-lg border-left-0" id="txtPass" placeholder="Contraseña">                        
                  </div>
                </div>
                
                <div class="my-3">
                  <button type="submit" class="btn btn-block btn-yimexico btn-lg">LOGIN</button>
                </div>
                <div class="alert alert-fill-success messagecorrecto" role="alert" style="display: none;">
                  <i class="mdi mdi-alert-circle"></i>
                  Inicio de Sesión correcto, redireccionando...
                </div>
                <div class="alert alert-fill-danger messageerror" role="alert" style="display: none;">
                  <i class="mdi mdi-alert-circle"></i>
                  El nombre de Usuario y/o Contraseña es incorrecto.
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-6 login-half-bg d-flex flex-row sysbackground">
            <p class="font-weight-medium text-center flex-grow align-self-end" style="color: #2b254e">Copyright &copy; <?php echo $fecha ?> <a href="http://mangoo.mx/" target="_blank" style="color: yellow;"> Mangoo </a>Software</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>