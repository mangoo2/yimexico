<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Yi México</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/css/vertical-layout-light/style.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>template/css/estilo_css.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>template/images/iconpro.png" />
</head>

<body>
 <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly> 