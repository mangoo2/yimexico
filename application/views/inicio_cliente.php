<?php
  if (!$this->session->userdata('logeado')) {
    redirect('Login');
    $perfilid =0;
  }else{
    $perfilid=$this->session->userdata('perfilid');
    $personalId=$this->session->userdata('idpersonal');
    $idcliente=$this->session->userdata('idcliente');
    $idcliente_usuario=$this->session->userdata('idcliente_usuario');
    $tipo=$this->session->userdata('tipo');
    $tipo_persona=$this->session->userdata('tipo_persona');
 
    //$menu=$this->Login_model->getMenus($perfilid);
    $persona=$this->Login_model->getpersona($personalId,$idcliente,$idcliente_usuario,$tipo);
    if($tipo==1){
      foreach ($persona as $item) {
          $nombre=$item->nombre;
          $perfil=$item->perfil;
      }
    }else if($tipo==3){
      if($tipo_persona==1){
        foreach ($persona as $item) { 
          $nombre=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
          $perfil=$item->perfil;
        }
      }else if($tipo_persona==2){
        foreach ($persona as $item) { 
          $nombre=$item->razon_social;
          $perfil=$item->perfil;
        }
      }else if($tipo_persona==3){
        foreach ($persona as $item) { 
          $nombre=$item->denominacion_razon_social;
          $perfil=$item->perfil;
        }
      }  
    }else if($tipo==4){
      foreach ($persona as $item) { 
        $nombre=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        $perfil=$item->perfil;
      }
    }
    $menu=$this->Login_model->getmenus_permiso($idcliente_usuario,$tipo);

    $pinta_adm=0;
    $submenu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,2);
    foreach ($submenu as $x2) { 
      //echo "id_menusub: ".$x2->id_menusub;
      //echo "check_menu: ".$x2->check_menu."<br>";
      if($x2->id_menusub==2 && $x2->check_menu==1 || $x2->id_menusub==4 && $x2->check_menu==1 || $x2->id_menusub==11 && $x2->check_menu==1 || $x2->id_menusub==12 && $x2->check_menu==1){ 
        $pinta_adm++;
      }
    }
  }
?>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <!--
        style="align-items: center;display: flex;"
      -->
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Bienvenido: <span><?php echo $nombre.' ' ?> </span></h3>
          </div>
        </div>
        <hr class="subtitle">
        <div class="row" style="height: 315px !important; align-items: center;display: flex;">
          <div class="col-md-12" align="center" >
            <?php 
            if($perfilid==3){
              // Mis datos
                  echo '<button class="btn gradient_nepal tam_btn" onclick="href_mis_datos_admin()">
                      <div class="row texto_centro">
                        <div class="col-md-5" align="right">
                          <i class="fa fa-address-book icon_yi"></i>
                        </div>
                        <div class="col-md-7" align="left">
                          <span>Mis datos</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;';
              // Administración del Sistema
                  echo '<button class="btn gradient_nepal tam_btn" onclick="href_admin_sis()">
                    <div class="row texto_centro">
                      <div class="col-md-3">
                        <i class="fa fa-linode icon_yi"></i>
                      </div>
                      <div class="col-md-9">
                        <span>Administración del Sistema</span>
                      </div>
                    </div>  
                  </button>&nbsp;&nbsp;&nbsp;&nbsp;';
              // Bitácoras y Estadísticas
                  echo '<button class="btn gradient_nepal tam_btn" onclick="href_mis_bitacoras()">
                          <div class="row texto_centro" >
                            <div class="col-md-4" align="right">
                              <i class="fa fa-bar-chart-o icon_yi"></i>
                            </div>
                            <div class="col-md-8" align="center">
                              <span>Bitácoras y Estadísticas</span>
                            </div>
                          </div>     
                      </button>&nbsp;&nbsp;&nbsp;&nbsp;';
              // Operaciones
                  echo '<button class="btn gradient_nepal tam_btn" onclick="hreft_operaciones()">
                    <div class="row texto_centro">
                        <div class="col-md-4">
                          <i class="fa fa-briefcase icon_yi"></i>
                        </div>
                        <div class="col-md-8">
                          <span>Operaciones</span>
                        </div>
                      </div>  
                  </button>&nbsp;&nbsp;&nbsp;&nbsp;';
              // Divisas
                  /*echo '<button class="btn gradient_nepal tam_btn" onclick="href_divisas()">
                          <div class="row texto_centro">
                            <div class="col-md-4">
                              <i class="fa fa-money icon_yi"></i>
                            </div>
                            <div class="col-md-8">
                              <span>Divisas</span>
                            </div>
                          </div>  
                        </button>';*/
            }else{
              foreach ($menu as $x) { 
                if($x->id_menu==1){ // Mis datos
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_mis_datos_usuario()">
                        <div class="row texto_centro">
                          <div class="col-md-5" align="right">
                            <i class="fa fa-address-book icon_yi"></i>
                          </div>
                          <div class="col-md-7" align="left">
                            <span>Mis datos</span>
                          </div>
                        </div>  
                      </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                if($x->id_menu==2){ // Administración del Sistema -- se regresa el boton para todos, cuando tenga el permiso
                  if($x->check_menu==1 && $pinta_adm>0){
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_admin_sis()">
                      <div class="row texto_centro">
                        <div class="col-md-3">
                          <i class="fa fa-linode icon_yi"></i>
                        </div>
                        <div class="col-md-9">
                          <span>Administración del Sistema</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                if($x->id_menu==3){ // Bitácoras y Estadísticas
                  if($x->check_menu==1){
                  echo '<button class="btn gradient_nepal tam_btn" onclick="href_mis_bitacoras()">
                          <div class="row texto_centro" >
                            <div class="col-md-4" align="right">
                              <i class="fa fa-bar-chart-o icon_yi"></i>
                            </div>
                            <div class="col-md-8" align="center">
                              <span>Bitácoras y Estadísticas</span>
                            </div>
                          </div>     
                      </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                if($x->id_menu==4){ // Operaciones
                  if($x->check_menu==1){
                  echo '<button class="btn gradient_nepal tam_btn" onclick="hreft_operaciones()">
                    <div class="row texto_centro">
                        <div class="col-md-4">
                          <i class="fa fa-briefcase icon_yi"></i>
                        </div>
                        <div class="col-md-8">
                          <span>Operaciones</span>
                        </div>
                      </div>  
                  </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                /*if($x->id_menu==5){ // Divisas
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_divisas()">
                          <div class="row texto_centro">
                            <div class="col-md-4">
                              <i class="fa fa-money icon_yi"></i>
                            </div>
                            <div class="col-md-8">
                              <span>Divisas</span>
                            </div>
                          </div>  
                        </button>';
                  }
                }*/
              } 
            }?>
    
          <?php /*
          <div class="col-md-12" align="center" >
            <?php if($perfilid==3) { /* ?><!-- Administrador -->
              <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px" onclick="href_mis_datos()">
                <i class="mdi mdi-account btn-icon-prepend mdi-36px"></i><br>
                <span class="d-inline-block text-left">
                  <h4>Mis datos</h4> 
                </span>
              </button>
              <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px" onclick="mis_usuarios()">
                <i class="mdi mdi-account-multiple btn-icon-prepend mdi-36px"></i><br>
                <span class="d-inline-block text-left">
                  <h4>Mis usuarios</h4>
                </span>
              </button>
              <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px" onclick="alta_hacienda()">
                <i class="mdi mdi-note btn-icon-prepend mdi-36px"></i>
                <span class="d-inline-block text-left"><br>
                  <h4>Alta de Hacienda</h4>
                </span>
              </button>
            <?php  */ /* } ?>
            <!-- /////////////////////////////////////////////////////////////////////////////////////////////// -->
            <!-- 6-Director General 7-Responsable Cumplimiento 8-Supervisor 9-Ejecutivo Comercial 10-Ejecutivo de Operación -->
            <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==8 || $perfilid==9 || $perfilid==10){ ?>
              <?php if($perfilid==3){ ?>
                <button class="btn gradient_nepal tam_btn" onclick="href_mis_datos_admin()">
                  <div class="row texto_centro">
                    <div class="col-md-5" align="right">
                      <i class="fa fa-address-book icon_yi"></i>
                    </div>
                    <div class="col-md-7" align="left">
                      <span>Mis datos</span>
                    </div>
                  </div>  
                </button>&nbsp;&nbsp;&nbsp;&nbsp;
              <?php }else{ ?>              
                <button class="btn gradient_nepal tam_btn" onclick="href_mis_datos_usuario()">
                  <div class="row texto_centro">
                    <div class="col-md-5" align="right">
                      <i class="fa fa-address-book icon_yi"></i>
                    </div>
                    <div class="col-md-7" align="left">
                      <span>Mis datos</span>
                    </div>
                  </div>  
                </button>&nbsp;&nbsp;&nbsp;&nbsp;
            <?php } 
            } ?> 
            <!-- 6-Director General 7-Responsable Cumplimiento 8-Supervisor 9-Ejecutivo Comercial 10-Ejecutivo de Operación -->
            <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==8){ ?>
              <button class="btn gradient_nepal tam_btn" onclick="href_admin_sis()">
                <div class="row texto_centro">
                  <div class="col-md-3">
                    <i class="fa fa-linode icon_yi"></i>
                  </div>
                  <div class="col-md-9">
                    <span>Administración del Sistema</span>
                  </div>
                </div>  
              </button>&nbsp;&nbsp;&nbsp;&nbsp;
            <?php } ?> 
            <!-- 6-Director General 7-Responsable Cumplimiento 8-Supervisor 9-Ejecutivo Comercial 10-Ejecutivo de Operación -->
            <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==8 || $perfilid==9 || $perfilid==10){ ?>
              <button class="btn gradient_nepal tam_btn" onclick="href_mis_bitacoras()">
                  <div class="row texto_centro" >
                    <div class="col-md-4" align="right">
                      <i class="fa fa-bar-chart-o icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="center">
                      <span>Bitácoras y Estadísticas</span>
                    </div>
                  </div>  
                      
              </button>&nbsp;&nbsp;&nbsp;&nbsp;
            <?php } ?> 
            <!-- 6-Director General 7-Responsable Cumplimiento 8-Supervisor 9-Ejecutivo Comercial 10-Ejecutivo de Operación -->
            <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==9 || $perfilid==10){ ?>
              <button class="btn gradient_nepal tam_btn" onclick="hreft_operaciones()">
                <div class="row texto_centro">
                    <div class="col-md-4">
                      <i class="fa fa-briefcase icon_yi"></i>
                    </div>
                    <div class="col-md-8">
                      <span>Operaciones</span>
                    </div>
                  </div>  
              </button>
            <?php } ?>
            <button class="btn gradient_nepal tam_btn" onclick="href_divisas()">
              <div class="row texto_centro">
                <div class="col-md-4">
                  <i class="fa fa-money icon_yi"></i>
                </div>
                <div class="col-md-8">
                  <span>Divisas</span>
                </div>
              </div>  
            </button>

             </div>  
            */ ?>
         
          </div>  
        </div>
      </div>
    </div>
  </div>          
</div>