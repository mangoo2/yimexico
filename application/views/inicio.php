<style type="text/css">
  .owl-dots{
    display:none;
  }
</style>



<div class="row">
  <div class="col-lg-8 grid-margin d-flex flex-column">
    <div class="row">
    <!--  -->
      <div class="col-lg-12">
        <div class="owl-carousel owl-theme loop">
          <div class="item">
          <!-- 1 Dólar Americano -->
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-3">
                        <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                        <i class="flag-icon flag-icon-us" style="font-size: 35px" title="us" id="us"></i>
                    </div>
                    <div class="col-md-9" align="center">
                        <b class="text_yi">Dólar Americano </b>

                    </div>
                    <div class="col-lg-12" align="center">
                      <br>
                      <b>$<?php echo $dolar_precio ?></b> <span> MNX</span>
                      <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                      <span><?php if($dolar_fecha!='') echo date('d-m-Y',strtotime($dolar_fecha)) ?></span>
                      <br>
                      <span style="font-size: 9px;">Última actualización </span>
                    </div>  
                  </div>  
                </div>
              </div> 
          <!-- -->        
          </div>
          <div class="item">
          <!-- 4 Dólar canadiense -->
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-3">
                      <!--<i style="font-size: 41px" class="fa fa-dollar text_yi2"></i>-->
                      <i class="flag-icon flag-icon-ca" style="font-size: 35px" title="ca" id="ca"></i>
                  </div>
                  <div class="col-md-9" align="center">
                      <b style="font-size: 15px" class="text_yi">Dólar canadiense </b>
                  </div>
                  <div class="col-lg-12" align="center">
                    <br>
                    <b>$<?php echo $dolar_cprecio ?></b> <span> MNX</span>
                    <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                    <span><?php if($dolar_cfecha!='') echo date('d-m-Y',strtotime($dolar_cfecha)) ?></span>
                    <br>
                    <span style="font-size: 9px;">Última actualización </span>
                  </div>  
                </div>  
              </div>
            </div>
          <!-- -->
          </div>
          <div class="item">
          <!-- 2 Euro -->
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-3">
                      <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                      <i class="flag-icon flag-icon-es" style="font-size: 35px" title="fm" id="fm"></i>
                  </div>
                  <div class="col-md-9" align="center">
                      <b class="text_yi">Euro </b>
                      <br><br>
                  </div>
                  <div class="col-lg-12" align="center">
                    <br>
                    <b>$<?php echo $euro_precio ?></b> <span> MNX</span>
                    <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                    <span><?php if($euro_fecha!='') echo date('d-m-Y',strtotime($euro_fecha)) ?></span>
                    <br>
                    <span style="font-size: 9px;">Última actualización </span>
                  </div>  
                </div>  
              </div>
            </div>
          <!-- -->
          </div>
          <div class="item">
          <!-- 3 Libra Esterlina -->
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-3">
                      <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                      <i class="flag-icon flag-icon-gb" style="font-size: 35px" title="gb" id="gb"></i>
                  </div>
                  <div class="col-md-9" align="center">
                      <b class="text_yi">Libra Esterlina </b>
                  </div>
                  <div class="col-lg-12" align="center">
                    <br>
                    <b>$<?php echo $libra_precio ?></b> <span> MNX</span>
                    <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                    <span><?php if($libra_fecha!='') echo date('d-m-Y',strtotime($libra_fecha)) ?></span>
                    <br>
                    <span style="font-size: 9px;">Última actualización </span>
                  </div>  
                </div>  
              </div>
            </div>
          <!-- -->
          </div>
          <div class="item">
          <!-- 5 Yuan Chino -->
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-3">
                      <!--<i style="font-size: 41px" class="fa fa-dollar text_yi"></i>-->
                      <i class="flag-icon flag-icon-cn" style="font-size: 35px" title="gb" id="gb"></i>
                  </div>
                  <div class="col-md-9" align="center">
                      <b class="text_yi">Yuan Chino </b>
                  </div>
                  <div class="col-lg-12" align="center">
                    <br>
                    <b>$<?php echo $chino_precio ?></b> <span> MNX</span>
                    <p style="border-top: 1px solid rgba(0, 0, 0, 0.1);"></p>
                    <span><?php if($chino_fecha!='') echo date('d-m-Y',strtotime($chino_fecha)) ?></span>
                    <br>
                    <span style="font-size: 9px;">Última actualización </span>
                  </div>  
                </div>  
              </div>
            </div>
          <!-- -->
          </div>
          
        </div>
      </div>

    <!--  -->
    </div>
    <br>
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">

        <div class="card">
          <div class="card-body">
            <h4>Últimos clientes activados</h4>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>
                      Nombre
                    </th>
                    <th>
                      Fecha de activación 
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Carlos Perez Juarez
                    </td>
                    <td>
                      13-03-2021
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row flex-grow-1">
      <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4>Citas del mes</h4>
            <div id="inline-datepicker-example" class="datepicker"></div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4>Avisos generados</h4>
            <canvas id="areaChart"></canvas> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 grid-margin stretch-card">
    <div class="card d-flex flex-column justify-content-between">
      <div class="card-body" align="center">
        <h4 >Accesos directos</h4>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Clientes">
          <i class="fa fa-handshake-o px_letra"></i> <span class="px_letra"> Clientes </span></a>
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Umbrales">
          <i class="fa fa-sliders px_letra"></i> <span class="px_letra"> Anexos </span></a> 
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Divisas">
          <i class="fa fa-dollar px_letra"></i> <span class="px_letra"> Divisas </span></a> 
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Estados">
          <i class="fa fa-flag px_letra"></i> <span class="px_letra"> Estados </span></a> 
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Giros_fiscales">
          <i class="fa fa-address-card-o px_letra"></i> <span class="px_letra"> Giros Físicos </span></a> 
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Giros_morales">
          <i class="fa fa-building-o px_letra"></i> <span class="px_letra"> Giros Morales </span></a> 
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Perfiles">
          <i class="fa fa-unlock-alt px_letra"></i> <span class="px_letra"> Permisos Perfiles </span></a> 
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Umas">
          <i class="fa fa-money px_letra"></i> <span class="px_letra"> Umas </span></a>
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Configuracion"><i class="fa fa-money px_letra"></i> <span class="px_letra">  Avance limite efect.</span></a><br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>ListaNegra">
          <i class="fa fa-ban px_letra"></i> <span class="px_letra"> Listas Bloqueados </span></a>
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>ConfigAlerta">
          <i class="fa fa-bell px_letra"></i> <span class="px_letra"> Diagnostico Alerta </span></a>
        <br><br>
        <a class="btn gradient_nepal2 btn_yi_admin_tm" href="<?php echo base_url(); ?>Bitacora">
          <i class="fa fa-line-chart  px_letra"></i> <span class="px_letra"> Bitácora de Busqueda </span></a>

      </div>
    </div>
  </div>
</div>

 