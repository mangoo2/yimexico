<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}

if($tipo==1 || $tipo==2){
  $nombre = $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno;
}
if($tipo==3){
  $nombre = $info->razon_social;
}
if($tipo==4){
  $nombre = $info->nombre_persona;
}
if($tipo==5 || $tipo==6){
  $nombre = $info->denominacion;
}
if($tipo==3 || $tipo==4 || $tipo==5 || $tipo==6){
  $nombre_rep = $info->nombre_g." ".$info->apellido_paterno_g." ".$info->apellido_materno_g;
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Sin Dueño Beneficiario');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Operación');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '12');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='<div class="row firma_tipo_cliente">
          <div class="col-md-12">    
            <h3>Formato: Acuse de inexistencia de dueño beneficiario </h3>
            <h3>Fecha: '.$fecha.'</h3><br>
            <span>DECLARA LA NO EXISTENCIA DEL DUEÑO BENEFICIARIO PARA LA OPERACIÓN CON NÚMERO <strong>'.$folio.' </strong> A REALIZARSE CON <strong> '.mb_strtoupper($nombre,"UTF-8").'</strong></span>
          </div>
        </div> 

        <div class="firma_tipo_cliente">
          <br><br><br><br><br><br>
          <div class="firma_tipo_cliente">
            <div class="row">
              <div class="col-md-12" style="text-align: justify !important;">
                <hr>';
              if($tipo==1 || $tipo==2){
                $html.='<p style="text-align:center">'.mb_strtoupper($nombre,"UTF-8").'</p>';
              }else{
                $html.='<p style="text-align:center">'.mb_strtoupper($nombre_rep,"UTF-8").'</p>';
              }
              $html.='Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
              </div>
            </div>
          </div>
        </div>';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('SIN_DUEÑO_BENE.pdf', 'I');

?>