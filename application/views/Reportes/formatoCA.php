<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

  if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
  if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
  if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
  if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
  if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
  if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}
 
  $info=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 

  $id=0;
  $descrip_nego='';
  $propietario_empresa='';
  $trabajador_autonomo='';
  $es_empleado='';
  $otras_actividades_generadoras_recurso='';
  $cuenta_alguna_otra_fuente_ingresos='';
  $fuente_ingresos_cual='';
  $cual_monto_mensual_recibe_ingresos='';
  $recibe_ingresos_extranjero='';
  $ingresos_extranjero_de_que_pais='';
  $mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta='';
  $nacional_extranjera_cual='';
  $nacional_extranjera_ingreso_anual='';
  $efectivo='';
  $transfer_otro='';
  $transfer_inter='';
  $tarjeta='';
  $cheques='';
  $usted_casado_vive_concubinato='';
  $cual_nombre_esposa='';
  $cual_ocupacion_espos='';
  $edad='';
  $nacionalidad='';
  $tiene_dependientes_economicos='';
  $cuantos='';
  $parentesco='';
  $nombre_ompleto='';
  $genero='';
  $fecha_nacimiento='';
  $ocupacion='';
  $apoyo_conyugal_parental_pareja='';
  $apoyo_gubernamental='';
  $herencia_donacion='';
  $fondo_fiduciario='';
  $prestamo_subvencion='';
  $inversiones='';
  $result = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));
  foreach ($result as $x) {
    $id=$x->id;
    $descrip_nego=$x->descrip_nego;
    $propietario_empresa=$x->propietario_empresa;
    $trabajador_autonomo=$x->trabajador_autonomo;
    $es_empleado=$x->es_empleado;
    $otras_actividades_generadoras_recurso=$x->otras_actividades_generadoras_recurso;
    $cuenta_alguna_otra_fuente_ingresos=$x->cuenta_alguna_otra_fuente_ingresos;
    $fuente_ingresos_cual=$x->fuente_ingresos_cual;
    $cual_monto_mensual_recibe_ingresos=$x->cual_monto_mensual_recibe_ingresos;
    $recibe_ingresos_extranjero=$x->recibe_ingresos_extranjero;
    $ingresos_extranjero_de_que_pais=$x->ingresos_extranjero_de_que_pais;
    $mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta=$x->mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta;
    $nacional_extranjera_cual=$x->nacional_extranjera_cual;
    $nacional_extranjera_ingreso_anual=$x->nacional_extranjera_ingreso_anual;
    $efectivo=$x->efectivo;
    $transfer_otro=$x->transfer_otro;
    $transfer_inter=$x->transfer_inter;
    $tarjeta=$x->tarjeta;
    $cheques=$x->cheques;
    $usted_casado_vive_concubinato=$x->usted_casado_vive_concubinato;
    $cual_nombre_esposa=$x->cual_nombre_esposa;
    $cual_ocupacion_espos=$x->cual_ocupacion_espos;
    $edad=$x->edad;
    $nacionalidad=$x->nacionalidad;
    $pais_4_get=$this->ModeloCatalogos->getselectwhere('pais','clave',$x->nacionalidad);
    foreach ($pais_4_get as $x1) {
      $clave4=$x1->clave;
      $pais4=$x1->pais;
    }
    $tiene_dependientes_economicos=$x->tiene_dependientes_economicos;
    $cuantos=$x->cuantos;
    $apoyo_conyugal_parental_pareja=$x->apoyo_conyugal_parental_pareja;
    $apoyo_gubernamental=$x->apoyo_gubernamental;
    $herencia_donacion=$x->herencia_donacion;
    $fondo_fiduciario=$x->fondo_fiduciario;
    $prestamo_subvencion=$x->prestamo_subvencion;
    $inversiones=$x->inversiones;

    if($x->id>0 && $x->cuantos!="" && $x->cuantos>0){
      $get_ode=$this->ModeloCatalogos->getselectwherestatus('*','cuest_amp_otros_dependiente',array('id_cuest_amp'=>$x->id,"estatus"=>1));
    }
  }
  $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$idp));
  foreach ($get_pp as $g) {
    //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
    $tipoccon = $g->idtipo_cliente;
    //echo "<br>tipoccon: ".$tipoccon;
    if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
    if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
    if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
    if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
    if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
    if($tipoccon==6) $tabla = "tipo_cliente_f";
    $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
    foreach ($get_per as $g2) {
      if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
      if($tipoccon==3) $nombre = $g2->razon_social;
      if($tipoccon==4) $nombre = $g2->nombre_persona;
      if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
    }
  }

  $propietario_empresatx1='';
  if($propietario_empresa==1){
    $propietario_empresatx1='Si';
  }else if($propietario_empresa==2){
    $propietario_empresatx1='No';
  }
  $trabajador_autonomotx1='';
  if($trabajador_autonomo==1){
    $trabajador_autonomotx1='Si';
  }else if($trabajador_autonomo==2){
    $trabajador_autonomotx1='No';
  }
  $es_empleadotx1='';
  if($es_empleado==1){
    $es_empleadotx1='Si';
  }else if($es_empleado==2){
    $es_empleadotx1='No';
  }
  $otras_actividades_generadoras_recursox1='';
  if($es_empleado==1){
    $otras_actividades_generadoras_recursox1='Ama de casa';
  }else if($es_empleado==1){
    $otras_actividades_generadoras_recursox1='Estudiante';
  }else if($es_empleado==3){
    $otras_actividades_generadoras_recursox1='Menor de Edad';
  }else if($es_empleado==4){
    $otras_actividades_generadoras_recursox1='Otra actividad';
  }
  $cuenta_alguna_otra_fuente_ingresostx1='';
  if($cuenta_alguna_otra_fuente_ingresos==1){
    $cuenta_alguna_otra_fuente_ingresostx1='Si';
  }else if($cuenta_alguna_otra_fuente_ingresos==2){
    $cuenta_alguna_otra_fuente_ingresostx1='No';
  }
  $recibe_ingresos_extranjerotx1='';
  if($recibe_ingresos_extranjero==1){
    $recibe_ingresos_extranjerotx1='Si';
  }else if($recibe_ingresos_extranjero==2){
    $recibe_ingresos_extranjerotx1='No';
  }
  $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1='';
  if($mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta==1){
    $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1='Si';
  }else if($mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta==2){
    $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1='No';
  }
  $usted_casado_vive_concubinatotx1='';
  if($usted_casado_vive_concubinato==1){
    $usted_casado_vive_concubinatotx1='Si';
  }else if($usted_casado_vive_concubinato==2){
    $usted_casado_vive_concubinatotx1='No';
  }
  $tiene_dependientes_economicostx1='';
  if($tiene_dependientes_economicos==1){
    $tiene_dependientes_economicostx1='Si';
  }else if($tiene_dependientes_economicos==2){
    $tiene_dependientes_economicostx1='No';
  }

  $title="";
  if($tipoccon==1) $title = "Persona fisica mexicana o extranjera con condición de estancia temporal permanente.";
  if($tipoccon==2) $title = "Persona fisica extranjera con condición de estancia de visitante.";

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Cuestionario Ampliado');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Personas Fisicas');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '12');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='
          <h3>Cuestionario ampliado -  '.$nombre.'</h3>
          <h3>'.$title.'</h3>
          <hr class="subtitle">
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>¿Es propietario de empresa?</label>
                <span><strong> '.$propietario_empresatx1.'</strong></span>
              </td>
              <td>
                <label>¿Es trabajador autónomo?</label>
                <span><strong> '.$trabajador_autonomotx1.'</strong></span>
              </td>
              <td>
                <label>¿Es empleado?</label>
                <span><strong> '.$es_empleadotx1.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>';
          if($otras_actividades_generadoras_recursox1!=""){
            $html.='<div class="col-md-4">
              <label>Otras actividades no generadoras de recurso:</label>
              <span><strong> '.$otras_actividades_generadoras_recursox1.'</strong></span>
            </div>';
          }

          if($otras_actividades_generadoras_recursox1!=""){
              $html.='
              <div id="div_cont_riqueza" >
                <h3>Origen de la riqueza para actividades no generadoras de ingresos</h3>
                <div class="subtitle" style="height: 3px !important;background-color: #2b254e;"></div>
                <br>
                <table width="100%">
                  <tr>
                    <td>
                      <label>Apoyo conyugal, parental o de pareja </label>
                      <span><strong> '.$apoyo_conyugal_parental_pareja.'</strong></span>
                    </td>
                    <td>
                      <label>Apoyo gubernamental</label>
                      <span><strong> '.$apoyo_gubernamental.'</strong></span>
                    </td>
                    <td>
                      <label>Herencia o donación</label>
                      <span><strong> '.$herencia_donacion.'</strong></span>
                    </td>
                    
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>Fondo fiduciario</label>
                      <span><strong> '.$fondo_fiduciario.'</strong></span>
                    </td>
                    <td>
                      <label>Préstamo o subvención</label>
                      <span><strong> '.$prestamo_subvencion.'</strong></span>
                    </td>
                    <td>
                      <label>Inversiones</label>
                      <span><strong> '.$inversiones.'</strong></span>
                    </td>
                  </tr>
                </table>
                <div class="subtitle" style="height: 3px !important;background-color: #2b254e;"></div>
              </div>';
          }
            $html.='
              <table width="100%">
                <tr>
                  <td>
                    <label>¿Cuenta con alguna otra fuente de ingresos?</label>
                    <span><strong> '.$cuenta_alguna_otra_fuente_ingresostx1.'</strong></span>
                  </td>';
              if($cuenta_alguna_otra_fuente_ingresostx1=="Si"){
                $html.='
                  <td>
                    <label>¿Cuál?</label>
                    <span><strong> '.$fuente_ingresos_cual.'</strong></span>
                  </td>';
              }
            $html.='</tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>¿Cuál es el monto mensual que recibe de ingresos (considerando todas las fuentes de ingresos)</label>
                    <span><strong>$ '.$cual_monto_mensual_recibe_ingresos.'</strong></span>
                  </td>
                  <td>
                    <label>¿Recibe ingresos del extranjero?</label>
                    <span><strong> '.$recibe_ingresos_extranjerotx1.'</strong></span>
                  </td>
                ';
            if($recibe_ingresos_extranjerotx1=="Si"){
              $html.='<td>
                  <label>¿De qué país(es)?</label>
                  <span><strong> '.$ingresos_extranjero_de_que_pais.'</strong></span>
                </td>';
            }
            $html.='</tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>¿Mantiene relaciones comerciales con alguna dependencia o entidad gubernamental, nacional o extranjera?</label>
                    <span><strong> '.$mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1.'</strong></span>
                  </td>';
              if($mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1=="Si"){
                $html.='<td>
                    <label>¿Cuál(es)?</label>
                    <span><strong> '.$nacional_extranjera_cual.'</strong></span>
                  </td>';
              }
              $html.='</tr>
                </table>';

            if($propietario_empresatx1=="Si"){
              $html.='<table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <div class="col-md-12 form-group">
                  <label>Del total de pagos que recibe de sus clientes, favor de desglosar en porcentaje, ¿qué forma de pago utilizan?</label>
                </div>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>Efectivo</label>
                      <span><strong> '.$efectivo.'</strong></span>
                    </td>
                    <td>
                      <label>Transferencia de otros bancos </label>
                      <span><strong> '.$transfer_otro.'</strong></span>
                    </td>
                    <td>
                      <label>Transferencias internacionales </label>
                      <span><strong> '.$transfer_inter.'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>Pago con tarjeta de crédito/debito </label>
                      <span><strong> '.$tarjeta.'</strong></span>
                    </td>
                    <td>
                      <label>Cheques</label>
                      <span><strong> '.$cheques.'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
              ';
            }
            $html.='<h3>Estado Civil y Dependientes Económicos</h3>
            <div class="subtitle" style="height: 3px !important;background-color: #2b254e;"></div>
              <table width="100%">
                <tr>
                  <td>
                    <label>¿Es usted casado o vive en concubinato?</label>
                    <span><strong> '.$usted_casado_vive_concubinatotx1.'</strong></span>
                  </td>
                </tr>
              </table>';
            if($usted_casado_vive_concubinatotx1=="Si"){
              $html.='<table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>¿Cuál es el nombre de su esposa(o)?</label>
                    <span><strong> '.$cual_nombre_esposa.'</strong></span>
                  </td>
                  <td>
                    <label>¿Cuál es la ocupación de su espos(a)?</label>
                    <span><strong> '.$cual_ocupacion_espos.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>Edad:</label>
                    <span><strong> '.$edad.'</strong></span>
                  </td>
                  <td>
                    <label>Nacionalidad:</label>
                    <span><strong> '.$nacionalidad.'</strong></span>
                  </td>
                </tr>
              </table>';
            }
            $html.='
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>¿Tiene dependientes económicos?</label>
                    <span><strong> '.$tiene_dependientes_economicostx1.'</strong></span>
                  </td>';
            if($tiene_dependientes_economicostx1=="Si"){
              $html.='
                    <td>
                      <label>¿Cuántos?:</label>
                      <span><strong> '.$cuantos.'</strong></span>
                    </td>
                  </tr>
                </table>';
                  $gen="";
                  foreach($get_ode as $k){ 
                    if($k->genero==1)
                      $gen="Masculino";
                    else if($k->genero==2)
                      $gen="Femenino";

                    $html.='
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td>
                            <label>Parentesco</label>
                            <span><strong> '.$k->parentesco.'</strong></span>
                          </td>
                          <td>
                            <label>Nombre Completo</label>
                            <span><strong> '.$k->nombre.'</strong></span>
                          </td>
                          <td>
                            <label>Género</label>
                            <span><strong> '.$gen.'</strong></span>
                          </td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td>
                            <label>Fecha Nacimiento</label>
                            <span><strong> '.$k->fecha_nacimiento.'</strong></span>
                          </td>
                          <td>
                            <label>Ocupación</label>
                            <span><strong> '.$k->ocupacion.'</strong></span>
                          </td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td><hr></td>
                        </tr>
                      </table>';
                  }
            }else{
              $html.='</tr>
              </table>';
            }
          $html.='
            <br><br><br><br>
            <div class="row">
              <table width="100%">
                <tr>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <p style="text-align:center">'.$nombre.'</p>
                      <h3 style="color: black; text-align:center">Firma Cliente</h3>
                      Doy fé que los datos  proporcionados son veridicos y están vigentes
                  </div>
                  </th>
                  <th width="4%"> </th>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                   <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
                      <h3 style="color: black; text-align:center">Firma Ejecutivo Comercial / Vendedor</h3>
                      Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial 
                  </div> 
                  </th>
                </tr>
              </table>
            </div> ';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Cuestionario Ampliado.pdf', 'I');

?>