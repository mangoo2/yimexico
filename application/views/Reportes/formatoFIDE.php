<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

if (strtotime($fecha) !== false) {
	$fecha_formato = date("d-m-Y", strtotime($fecha));
} else {
	$fecha_formato = date("d-m-Y");
}

  $idtipo_cliente_f_6='';
  $denominacion_6='';
  $numero_referencia_6='';
  $clave_registro_6='';
  $fecha_nacimiento_g_6='';
  $nombre_g_6='';
  $apellido_paterno_g_6='';
  $apellido_materno_g_6='';
  $nombre_identificacion_g_6='';
  $autoridad_emite_g_6='';
  $numero_identificacion_g_6='';
  $genero_g_6='';
  $r_f_c_g_6='';
  $curp_g_6='';
  $arraywhere_6=array('idperfilamiento'=>$idperfilamiento); 
  $tipo_cliente_f=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_f',$arraywhere_6);
  foreach ($tipo_cliente_f as $item_6){
    $idtipo_cliente_f_6=$item_6->idtipo_cliente_f;
    $denominacion_6=$item_6->denominacion;
    $numero_referencia_6=$item_6->numero_referencia;
    $clave_registro_6=$item_6->clave_registro;
    $nombre_g_6=$item_6->nombre_g;
    $apellido_paterno_g_6=$item_6->apellido_paterno_g;
    $apellido_materno_g_6=$item_6->apellido_materno_g;
    $nombre_identificacion_g_6=$item_6->nombre_identificacion_g;
    $autoridad_emite_g_6=$item_6->autoridad_emite_g;
    $numero_identificacion_g_6=$item_6->numero_identificacion_g;
    $genero_g_6=$item_6->genero_g;
    $r_f_c_g_6=$item_6->r_f_c_g;
    $curp_g_6=$item_6->curp_g;
    $fecha_nacimiento_g_6=$item_6->fecha_nacimiento_g;
  }
  $genero_t_6='';
  if($genero_g_6==1){
    $genero_t_6='Masculino';
  }else if($genero_g_6==2){
    $genero_t_6='Femenino';
  }

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce a tu Cliente');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Fideicomisos');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '12');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.=' <div class="row firma_tipo_cliente">
            <div class="col-md-12">    
	        	<h4>Formato: Conoce a tu cliente</h4>
	        	<h4>Fecha: '.$fecha_formato.'</h4>
	        	<hr class="subtitle barra_menu">
            <h4>Fideicomisos</h4>
           	<h4>Datos Generales</h4>';

          $html.='
            <table width="100%">
              <tr>
                <td>
                  <label>Denominación o razón social del fiduciario:</label>
                  <span><strong> '.$denominacion_6.'</strong></span>
                </td>
                <td>
                  <label>Número / Referencia o identicador del fideicomiso:</label>
                  <span><strong> '.$numero_referencia_6.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>R.F.C. del fideicomiso:</label>
                  <span><strong> '.$clave_registro_6.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
              <div class="row">
                <div class="col-md-12">
                  <hr class="subtitle">
                  <h5>DATOS GENERALES DEL APODERADO LEGAL O DELEGADO FIDUCIARIO</h5>
                </div>
                <table width="100%">
                  <tr>
                    <td>
                      <label>Nombre(s):</label>
                      <span><strong> '.$nombre_g_6.'</strong></span>
                    </td>
                    <td>
                      <label>Apellido paterno:</label>
                      <span><strong> '.$apellido_paterno_g_6.'</strong></span>
                    </td>
                    <td>
                      <label>Apellido materno:</label>
                      <span><strong> '.$apellido_materno_g_6.'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>Nombre de identificación:</label>
                      <span><strong> '.$nombre_identificacion_g_6.'</strong></span>
                    </td>
                    <td>
                      <label>Autoridad que emite la identificación:</label>
                      <span><strong> '.$autoridad_emite_g_6.'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td width="40%">
                      <label>Número de identificación:</label>
                      <span><strong> '.$numero_identificacion_g_6.'</strong></span>
                    </td>
                    <td width="40%">
                      <label>Fecha de nacimiento:</label>
                      <span><strong> '.$fecha_nacimiento_g_6.'</strong></span>
                    </td> 
                    <td width="20%">
                      <label>Género:</label>
                      <span><strong> '.$genero_t_6.'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>R.F.C:</label>
                      <span><strong> '.$r_f_c_g_6.'</strong></span>
                    </td>
                    <td>
                      <label>CURP:</label>
                      <span><strong> '.$curp_g_6.'</strong></span>
                    </td>
                  </tr>
                </table>
                <div class="col-md-2 form-group"></div>
              </div>
           
                  
            <div class="firma_tipo_cliente">
              <br><br><br>
              <br>
              <div class="firma_tipo_cliente">
                <table width="100%">
                  '.$label_a8.'
                  <tr>
                    <th width="48%">
                    <div class="col-md-6" style="text-align: justify !important;">
                      <hr>
                      <p style="text-align:center">'.$nombre_g_6.' '.$apellido_paterno_g_6.' '.$apellido_materno_g_6.'</p>
                        Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
                    </div>
                    </th>
                    <th width="4%"> </th>
                    <th width="48%">
                    <div class="col-md-6" style="text-align: justify !important;">
                      <hr>
                      <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
                        Declaro bajo protesta de decir verdad, que los datos asentados en la presente solicitud ha sido proporcionados directamente por el cliente.
                    </div> 
                    </th>
                  </tr>
                </table>
              </div>
            </div>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('formatoConoceCliente.pdf', 'I');

?>