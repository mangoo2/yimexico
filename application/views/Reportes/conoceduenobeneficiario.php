<?php
//require_once('/TCPDF/examples/tcpdf_include.php'); 
require_once('/TCPDF4/tcpdf.php');

//
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
    $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('beneficiario');
$pdf->SetTitle('beneficiario');
$pdf->SetSubject('beneficiario');
$pdf->SetKeywords('beneficiario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '10');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 10);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='<table border="0">';
$html.='<tr>
          <td width="70%"></td>
          <td width="30%" rowspan="2"><img style="width: 150px" src="'.base_url().'public/img/logo2.jpg"></td>
        </tr>';
$html.='<tr>
          <td >Formato: Conoce a tu dueño beneficiario</td>
        </tr>';
$html.='<tr>
          <td >Fecha:</td><td></td>
        </tr>
        <tr>
          <td >Tipo de dueño beneficiario:'.$tipocliente.'</td><td></td>
        </tr>
        <tr>
          <td >Nombre del Dueño beneficiario:'.$nombrebeneficiario.'</td><td></td>
        </tr>
        <tr>
          <td >Datos generales:</td><td></td>
        </tr>
        <tr>
          <td>
            <b>Identificacion</b>: '.$nombre_identificacion.': '.$numero_identificacion.'<br>
            <b>Fecha nacimiento</b>: '.$fecha_nacimiento.'<br>
            <b>Nacionalidad</b>: '.$pais_nacionalidad.'<br>
            <b>Domicilio</b>: '.$domicilio.'<br>
            <b>Correo</b>: '.$correo_d.'<br>
            <b>RFC</b>: '.$r_f_c_d.'<br>
            <b>CURP</b>: '.$curp_d.'<br>
          </td><td></td>
        </tr>';
$html.='</table >';
$html.='<table border="0" align="center" cellpadding="10">
        <tr>
          <td  style="height:100px" colspan="2"></td>
        </tr>
        <tr>
          <td>____________________________________</td>
          <td>____________________________________</td>
        </tr>
        <tr>
          <td>Cliente</td>
          <td>Ejecutivo Comercial</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td style="font-size:12px;">
            Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
          </td>
          <!--
          <td style="font-size:12px;">
            Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados.
          </td>
          -->
        </tr>
        <tr>
          <td  style="height:100px" colspan="2"></td>
        </tr>
        <tr>
          <td>____________________________________</td>
          <td>____________________________________</td>
        </tr>
        <tr>
          <td>Cliente</td>
          <td>Ejecutivo Comercial</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td style="font-size:12px;">
            Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
          </td>
          <td style="font-size:12px;">
            Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados.
          </td>
        </tr>
        </table >';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$html='';
$html.='<table border="0">';
$html.='<tr>
          <td >Formato: Conoce a tu dueño</td>
        </tr>';
$html.='<tr>
          <td >Fecha:</td>
        </tr>
        <tr>
          <td >DECLARA LA NO EXISTENCIA DEL DUEÑO BENEFICIARIO PARA LA OPERACIÓN CON NÚMERO_______A REALIZARSE CON________________</td>
        </tr>
        ';
$html.='</table >';
$html.='<table border="0" align="center" cellpadding="10">
        <tr>
          <td  style="height:100px" colspan="2"></td>
        </tr>
        <tr>
          <td>____________________________________</td>
          <td>____________________________________</td>
        </tr>
        <tr>
          <td>Cliente</td>
          <td>Ejecutivo Comercial</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td style="font-size:12px;">
            Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
          </td>
          <td style="font-size:12px;">
            Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados.
          </td>
        </tr>
        <tr>
          <td  style="height:100px" colspan="2"></td>
        </tr>
        <tr>
          <td>____________________________________</td>
          <td>____________________________________</td>
        </tr>
        <tr>
          <td>Cliente</td>
          <td>Ejecutivo Comercial</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td style="font-size:12px;">
            Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
          </td>
          <td style="font-size:12px;">
            Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados.
          </td>
        </tr>
        </table >';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->Output('Beneficiario.pdf', 'I');

?>