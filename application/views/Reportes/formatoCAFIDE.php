<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

  if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
  if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
  if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
  if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
  if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
  if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

  $info=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 
  $ca = $this->General_model->get_tableRow('cuestionario_ampliado_fide',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));


class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Cuestionario Ampliado');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Fideicomisos');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '15');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='
  <div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
          <h3>CUESTIONARIO AMPLIADO (PARA FIDEICOMISOS)</h3>
          <hr class="subtitle">';

          $html.='<div align="left" class="row">
              <div class="col-md-12 form-group">
                <label>Actividad conforme a su Alta Fiscal: </label>
                <span><strong> '.$ca->actividad.'</strong></span>
              </div>

              <div class="col-md-6 form-group">
                <label>¿Tipo de fideicomiso?</label>
                <span><strong> '.$ca->tipo_fide.'</strong></span>
              </div>
              <div class="col-md-6 form-group">
                <label>¿Cuál es el propósito del fideicomiso?</label>
                <span><strong> '.$ca->proposito.'</strong></span>
              </div>

              <div class="col-md-6 form-group">
                <label>¿Cuál es el patrimonio del fideicomiso?</label>
                <span><strong> '.$ca->patrimonio.'</strong></span>
              </div>
              <div class="col-md-6 form-group">
                <label>¿Cuántos fideicomitentes tiene el fideicomiso?</label>
                <span><strong> '.$ca->fideicomitentes.'</strong></span>
              </div>
              <div class="col-md-6 form-group">
                <label>¿Cuántos fideicomisarios existen en el fideicomiso?</label>
                <span><strong> '.$ca->fideicomisarios.'</strong></span>
              </div>
              <div class="col-md-6 form-group">
                <label>¿Quién es el fiduciario?</label>
                <span><strong> '.$ca->fiduciario.'</strong></span>
              </div>
              <div class="col-md-12 form-group">
                <label>Motivo por el cual el fideicomiso solicita la transacción / operación:</label>
                <span><strong> '.$ca->motivo_transac.'</strong></span>
              </div>
            </div>';

          $html.='<div>
            <br><br><br><br><br><br><br><br>
            <div class="row">
              <table width="100%">
                <tr>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_cli_imp"></span></center>
                      <h3 style="color: black; text-align:center">Firma Cliente</h3>
                      Doy fé que los datos  proporcionados son veridicos y están vigentes
                  </div>
                  </th>
                  <th width="4%"> </th>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
                      <h3 style="color: black; text-align:center">Firma Ejecutivo Comercial / Vendedor</h3>
                      Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial 
                  </div> 
                  </th>
                </tr>
              </table>
            </div> ';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Cuestionario Ampliado.pdf', 'I');

?>