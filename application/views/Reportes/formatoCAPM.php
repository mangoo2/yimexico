<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

  if($tipoc==1) {$tabla = 'tipo_cliente_p_f_m'; $idt='idtipo_cliente_p_f_m';}
  if($tipoc==2) {$tabla = 'tipo_cliente_p_f_e'; $idt='idtipo_cliente_p_f_e';}
  if($tipoc==3) {$tabla = 'tipo_cliente_p_m_m_e'; $idt='idtipo_cliente_p_m_m_e';}
  if($tipoc==4) {$tabla = 'tipo_cliente_p_m_m_d'; $idt='idtipo_cliente_p_m_m_d';}
  if($tipoc==5) {$tabla = 'tipo_cliente_e_c_o_i'; $idt='idtipo_cliente_e_c_o_i'; }
  if($tipoc==6) {$tabla = 'tipo_cliente_f'; $idt='idtipo_cliente_f';}

  $info=$this->General_model->get_tipoCliente($tabla,array($idt=>$idcc)); 
  $actividad=$this->ModeloCatalogos->getData('actividad_econominica_morales');

  $get_pais = $this->ModeloCatalogos->getData('pais');
  $get_edos = $this->ModeloCatalogos->getData('estado');
  $ca = $this->General_model->get_tableRow('cuestionario_ampliado_moral',array('id_operacion'=>$idopera,"id_perfilamiento"=>$idp));

  $ing_extran="";
  if($ca->ingreso_extran==1)
    $ing_extran="Si";
  else{
    $ing_extran="No";
  }
  $rela_extran="";
  if($ca->relacion_extran==1)
    $rela_extran="Si";
  else
    $rela_extran="No";

  $sucur="";
  if($ca->sucursales==1)
    $sucur="Si";
  else
    $sucur="No";

  $sucur_otro="";
  if($ca->sucursal_otro_pais==1)
    $sucur_otro="Si";
  else
    $sucur_otro="No";

  $emp_grup="";
  if($ca->empresa_grupo==1)
    $emp_grup="Si";
  else
    $emp_grup="No";

  $con_adm="";
  if($ca->consejo_admin==1)
    $con_adm="Consejo de Administración";
  else
    $con_adm="Administrador Único";

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Cuestionario Ampliado');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('personas Morales');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '15');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='

          <h3>CUESTIONARIO AMPLIADO (PARA PERSONAS MORALES MEXICANAS Y EXTRANJERAS)</h3>
          <hr class="subtitle">';
          if($tipo==3){ 
            $nombre=$info->razon_social;
            $html.='
            
                <h4>Razón social:
                '.$info->razon_social.'</h4>
                <h4>Fecha de constitución:
                '.$fechan=$info->fecha_constitucion_d.'</h4>
              ';
          }
          if($tipo==4){
            $nombre=$info->nombre_persona;
            $html.='
              
                <h4>Razón social:
                '.$info->nombre_persona.'</h4>
                <h4>Fecha de constitución:
                '.$fechan=$info->fecha_constitucion_d.'</h4>
              ';
          }
          $html.='<div align="left" class="row">
              <div class="col-md-12 form-group">
                <label>Actividad conforme a su Alta Fiscal: </label>
                <span><strong> '.$ca->actividad.'</strong></span>
              </div>

              <div class="col-md-6 form-group">
                <label>¿Cuál es la fuente principal de ingresos?</label>
                <span><strong> '.$ca->fuente_ingreso.'</strong></span>
              </div>
              <div class="col-md-6 form-group">
                <label>Idica el % que representa esta fuente del total de los ingresos</label>
                <span><strong> '.$ca->porcentaje.'%</strong></span>
              </div>

              <div class="col-md-6 form-group">
                <label>Si la fuente  principal de ingresos represente menos del 50% del total, describe cuales son  las otras fuentes de ingreso</label>
                <span><strong> '.$ca->otras_fuentes.'</strong></span>
              </div>
              <div class="col-md-6 form-group">
                <label>¿Cuál es el monto mensual de ventas (considerando todas las fuentes de ingresos)?</label>
                <span><strong> $'.number_format($ca->monto_ventas_mes,2,".",",").'</strong></span>
              </div>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>¿Recibe ingresos del extranjero?</label>
                    <span><strong> '.$ing_extran.'</strong></span>
                  </td>
                ';
              if($ing_extran=="Si"){
                $html.='
                  <td>
                    <label>¿De qué país(es)?</label>
                    <span><strong> '.$ca->pais_ingreso.'</strong></span>
                  </td>';
              }

              $html.='</tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <div class="col-md-5 form-group">
                <label>¿Mantiene relaciones comerciales con alguna dependencia o entidad gubernamental, nacional o extranjera?</label>
                <span><strong> '.$rela_extran.'</strong></span>
              </div>';
              if($rela_extran=="Si"){
                $html.='<div id="col-md-4">
                  <div class="col-md-12 form-group" id="div_relacion">
                    <label>¿Cuál(es)?</label>
                    <span><strong> '.$ca->cual_relacion.'</strong></span>
                  </div>
                </div>';
              }

              $html.='
                <div class="col-md-12 form-group">
                  <label>Del total de pagos que recibe de sus clientes, favor de desglosar en porcentaje, ¿qué forma de pago utilizan?</label>
                </div>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>Efectivo</label>
                      <span><strong> '.$ca->efectivo.'</strong></span>
                    </td>
                    <td>
                      <label>Transferencia de otros bancos </label>
                      <span><strong> '.$ca->transfer_otro.'</strong></span>
                    </td>
                    <td>
                      <label>Transferencias internacionales </label>
                      <span><strong> '.$ca->transfer_inter.'</strong></span>
                    </td>
                  </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td width="40%">
                    <label>Pago con tarjeta de crédito/debito </label>
                    <span><strong> '.$ca->tarjeta.'</strong></span>
                  </td>
                  <td width="20%">
                    <label>Cheques</label>
                    <span><strong> '.$ca->cheques.'</strong></span>
                  </td>
                  <td width="40%">
                    <label>Otras (describir)</label>
                    <span><strong> '.$ca->otros.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>¿La entidad tiene sucursales en el país?</label>
                    <span><strong> '.$sucur.'</strong></span>
                  </td>';
              if($sucur=="Si"){
                $html.='
                      <td>
                        <label>¿Cuántas?</label>
                        <span><strong> '.$ca->cuantas.'</strong></span>
                      </td>
                    </tr>
                  </table>
                  <table width="100%">
                    <tr>
                      <td><br></td>
                    </tr>
                  </table>
                  <table width="100%">
                    <tr>
                      <td>
                        <label>¿En qué ciudades y estados?</label>
                        <span><strong> '.$ca->pais_estados.'</strong></span>
                      </td>
                    </tr>
                  </table>';
              }else{
                $html.='</tr>
                  </table>';
              }
              
              $html.='<table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                    <label>¿La entidad tiene sucursales en otros países?</label>
                    <span><strong> '.$sucur_otro.'</strong></span>
                  </td>';
              if($sucur_otro=="Si"){
                $html.='
                      <td>
                        <label>¿Cuántas?</label>
                        <span><strong> '.$ca->cuantas_otro.'</strong></span>
                      </td>
                    </tr>
                  </table>
                  <table width="100%">
                    <tr>
                      <td><br></td>
                    </tr>
                  </table>
                  <table width="100%">
                    <tr>
                      <td>
                        <label>¿En qué países?</label>
                        <span><strong> '.$ca->paises_otro.'</strong></span>
                      </td>
                    </tr>
                  </table>';
              }else{
                $html.='</tr>
                  </table>';
              }

              $html.='<table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>¿La(s) ubicación(es) de la entidad, son propias o rentadas?</label>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>       
                    <td>
                      <label>Propia(s):</label>
                      <span><strong> '.$ca->propias.'</strong></span>
                    </td>
                    <td>
                      <label>Rentada(s):</label>
                      <span><strong> '.$ca->rentadas.'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>   
                    <td>
                      <label>Número de empleados </label>
                      <span><strong> '.$ca->num_emplea.'</strong></span>
                    </td>
                    <td>
                      <label>Número de clientes</label>
                      <span><strong> '.$ca->num_cli.'</strong></span>
                    </td>
                    <td>
                      <label>Número de proveedores</label>
                      <span><strong> '.$ca->num_prov.'</strong></span>
                    </td>
                  </tr>
                </table>

              <div class="col-md-4 form-group">
                <br>
                <label>¿La empresa pertenece a algún grupo?</label>
                <span><strong> '.$emp_grup.'</strong></span>
              </div>';
              if($emp_grup=="Si"){
                $html.='<div class="col-md-3 form-group">
                  <div class="col-md-12 form-group div_cta_emp">
                    <label>¿Cuántas empresas conforman el grupo empresarial?</label>
                    <span><strong> '.$ca->cuantas_empre.'</strong></span>
                  </div>
                </div>';
              }
              $html.='<div class="col-md-6 form-group">
                <label>¿La empresa tiene Consejo de Administración o Administrador Único?</label>
                <span><strong> '.$con_adm.'</strong></span>
              </div>
              <div class="col-md-12 form-group">
                <label>Motivo por el cual la entidad solicita la transacción / operación:</label>
                <span><strong> '.$ca->motivo_transac.'</strong></span>
              </div>
            </div>';

          $html.='<div>
            <br><br>
            <div class="row">
              <table width="100%">
               <tr>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <p style="text-align:center">'.$nombre.'</p>
                      <h3 style="color: black; text-align:center">Firma Cliente</h3>
                      Doy fé que los datos  proporcionados son veridicos y están vigentes
                  </div>
                  </th>
                  <th width="4%"> </th>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                   <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
                      <h3 style="color: black; text-align:center">Firma Ejecutivo Comercial / Vendedor</h3>
                      Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial 
                  </div> 
                  </th>
                </tr>
              </table>
            </div> ';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Cuestionario Ampliado.pdf', 'I');

?>