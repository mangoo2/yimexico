<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

//$GLOBALS['empleado'] = $empleado;
foreach ($empleado as $item) {
  $empleado = $item->nombre;
  $ciudad = $item->ciudad;
  $nom_ent = $item->nom_ent;
  $puesto = $item->puesto;
  $fecha = $item->fecha;
  $dia = date('d',strtotime($item->fecha));
  $mes = date('m',strtotime($item->fecha));
  $anio = date('Y',strtotime($item->fecha));
  if($mes==1){
      $mes_t = 'Enero';
  }else if($mes==2){
      $mes_t = 'Febrero';
  }else if($mes==3){
      $mes_t = 'Marzo';
  }else if($mes==4){
      $mes_t = 'Abril';
  }else if($mes==5){
      $mes_t = 'Mayo';
  }else if($mes==6){
      $mes_t = 'Junio';
  }else if($mes==7){
      $mes_t = 'Julio';
  }else if($mes==8){
      $mes_t = 'Agosto';
  }else if($mes==9){
      $mes_t = 'Septiembre';
  }else if($mes==10){
      $mes_t = 'Octubre';
  }else if($mes==11){
      $mes_t = 'Noviembre';
  }else if($mes==12){
      $mes_t = 'Diciembre';
  }
  $hora=$item->hora;
  $hora_e = date("g:i A", strtotime($item->hora));
  $evento = $item->evento;
  $comentario = $item->comentario;
}

//
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
    $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '10');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
// Logo
$html.='<img style="width: 150px" src="'.base_url().'public/img/cedis.jpg">';
// Texto
$html.='<br><div align="center" style="font-weight: bold; font-size:17px;">CARTA COMPROMISO<div>';
$html.='<div style="font-weight: bold; font-size:13px; text-align:justify;">En la Ciudad de <u>'.$ciudad.'</u>, del estado de <u>'.$nom_ent.'</u>, siendo las <u>'.$hora_e.'</u> horas del día <u>'.$dia.'</u> del mes de <u>'.$mes_t.'</u> del año <u>'.$anio.'</u>, en la empresa Cadena Comercial Oxxo S.A de CV se levanta la presente acta carta compromiso a fin de hacer constar el compromiso que adquiere <u>'.$empleado.'</u>, quien desempeñan el puesto de <u>'.$puesto.'</u> y que consiste en lo siguiente:
        </div>';
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;">'.$evento.'
        </div>';
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;">Se anexa el desempeño de los meses del año '.$anio.'.
        </div>';
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;">Así mismo, en este acto el C.<u>'.$empleado.'</u> manifiesta lo siguiente: 
        </div>';
  
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;"><u>'.$comentario.'</u>
          </div><br><br>';
  
  $html.='<table width="100%">
            <tr><td style="font-weight: bold; font-size:13px; width: 50%;">SR.________________________________</td><td style="font-weight: bold; font-size:13px; width: 50%;">SR.________________________________</td></tr>
            <tr><td style="width: 100%;"></td></tr> 
            <tr><td style="width: 100%;"></td></tr> 
            <tr><td style="font-weight: bold; font-size:13px; width: 50%;">SR.________________________________</td><td style="font-weight: bold; font-size:13px; width: 50%;">SR.________________________________</td></tr>
          </table><br>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Captura.pdf', 'I');

?>