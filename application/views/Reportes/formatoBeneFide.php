<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce al dueño beneficiario');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('fideicomisos');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '12');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='
        <div class="col-md-12">    
          <h3>Formato: Conoce al dueño beneficiario</h3>
          <h3>Fecha: '.$fecha.'</h3>
          <h3>Fideicomiso</h3>
        </div> 
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-12">    
            <h3 class="barra_menu">Datos del Fideicomiso</h3>
          </div>
          <div class="col-md-10 form-group">
            <label>Razón social del fiduciario:</label>
            <span><strong> '.$b->razon_social.'</strong></span>
          </div>
          <div class="col-md-5 form-group">
            <label>R.F.C del fideicomiso:</label>
            <span><strong> '.$b->rfc.'</strong></span>
          </div>
          <div class="col-md-5 form-group">
            <label>Número o referencia del fideicomiso:</label>
            <span><strong> '.$b->referencia.'</strong></span>
          </div>
        </div>

        <div class="firma_tipo_cliente">
          <br><br><br><br><br><br>
          <div class="firma_tipo_cliente">
            <div class="row">
              <div class="col-md-12" style="text-align: justify !important;">
                <hr>
                <p style="text-align:center">'.mb_strtoupper($nombre,"UTF-8").'</p>
                  Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
              </div>
            </div>
          </div>
        </div>';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Cuestionario beneficiario.pdf', 'I');

?>