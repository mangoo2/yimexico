<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

$estados=$this->ModeloCatalogos->getData('estado');

if (strtotime($fecha) !== false) {
	$fecha_formato = date("d-m-Y", strtotime($fecha));
} else {
	$fecha_formato = date("d-m-Y");
}
  ////////////////////////
  $GLOBALS["idtipo_cliente_p_f_m_1"]=0;
  $GLOBALS["nombre_1"]='';
  $GLOBALS["apellido_paterno_1"]='';
  $GLOBALS["apellido_materno_1"]='';
  $GLOBALS["nombre_identificacion_1"]='';
  $GLOBALS["autoridad_emite_1"]='';
  $GLOBALS["numero_identificacion_1"]='';
  $GLOBALS["genero_1"]='';
  $GLOBALS["fecha_nacimiento_1"]='';
  $GLOBALS["pais_nacimiento_1"]='';
  $GLOBALS["pais_nacionalidad_1"]='';
  $GLOBALS["activida_o_giro_1"]='';
  $GLOBALS["otra_ocupa"]="";
  $GLOBALS["tipo_vialidad_d_1"]='';
  $GLOBALS["calle_d_1"]='';
  $GLOBALS["no_ext_d_1"]='';
  $GLOBALS["no_int_d_1"]='';
  $GLOBALS["colonia_d_1"]='';
  $GLOBALS["municipio_d_1"]='';
  $GLOBALS["localidad_d_1"]='';
  $GLOBALS["estado_d_1"]='';
  $GLOBALS["cp_d_1"]='';
  $GLOBALS["pais_d_1"]='';
  $GLOBALS["trantadose_persona_1"]='';
  $GLOBALS["tipo_vialidad_t_1"]='';
  $GLOBALS["calle_t_1"]='';
  $GLOBALS["no_ext_t_1"]='';
  $GLOBALS["no_int_t_1"]='';
  $GLOBALS["colonia_t_1"]='';
  $GLOBALS["municipio_t_1"]='';
  $GLOBALS["localidad_t_1"]='';
  $GLOBALS["estado_t_1"]='';
  $GLOBALS["cp_t_1"]='';
  $GLOBALS["correo_t_1"]='';
  $GLOBALS["r_f_c_t_1"]='';
  $GLOBALS["curp_t_1"]='';
  $GLOBALS["telefono_t_1"]='';
  $GLOBALS["presente_legal_1"]='';
  $GLOBALS["nombre_p_1"]='';
  $GLOBALS["apellido_paterno_p_1"]='';
  $GLOBALS["apellido_materno_p_1"]='';
  $GLOBALS["nombre_identificacion_p_1"]='';
  $GLOBALS["autoridad_emite_p_1"]='';
  $GLOBALS["numero_identificacion_p_1"]='';
  $GLOBALS["fecha_nac_apodera"]='';
  $GLOBALS["rfc_apodera"]='';
  $GLOBALS["curp_apodera"]='';
  $GLOBALS["genero_p_1"]=0;
  $GLOBALS["tipo_vialidad_p_1"]='';
  $GLOBALS["calle_p_1"]='';
  $GLOBALS["no_ext_p_1"]='';
  $GLOBALS["no_int_p_1"]='';
  $GLOBALS["colonia_p_1"]='';
  $GLOBALS["municipio_p_1"]='';
  $GLOBALS["localidad_p_1"]='';
  $GLOBALS["estado_p_1"]='';
  $GLOBALS["cp_p_1"]='';
  ////////////////////////////
  $arraywhere_1 = array('idperfilamiento'=>$idperfilamiento); 
  $tipo_cliente_p_f_m=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_m',$arraywhere_1);
  foreach ($tipo_cliente_p_f_m as $item_1) {
    $GLOBALS["idtipo_cliente_p_f_m_1"]=$item_1->idtipo_cliente_p_f_m;
    $GLOBALS["idperfilamiento_1"]=$item_1->idperfilamiento;
    $GLOBALS["nombre_1"]=$item_1->nombre;
    $GLOBALS["apellido_paterno_1"]=$item_1->apellido_paterno;
    $GLOBALS["apellido_materno_1"]=$item_1->apellido_materno;
    $GLOBALS["nombre_identificacion_1"]=$item_1->nombre_identificacion;
    $GLOBALS["autoridad_emite_1"]=$item_1->autoridad_emite;
    $GLOBALS["numero_identificacion_1"]=$item_1->numero_identificacion;
    $GLOBALS["genero_1"]=$item_1->genero;
    $GLOBALS["fecha_nacimiento_1"]=$item_1->fecha_nacimiento;
    $GLOBALS["pais_nacimiento_1"]=$item_1->pais_nacimiento;
    $GLOBALS["pais_nacionalidad_1"]=$item_1->pais_nacionalidad;
    $GLOBALS["activida_o_giro_1"]=$item_1->activida_o_giro;
    $GLOBALS["otra_ocupa"]=$item_1->otra_ocupa;
    $GLOBALS["tipo_vialidad_d_1"]=$item_1->tipo_vialidad_d;
    $GLOBALS["calle_d_1"]=$item_1->calle_d;
    $GLOBALS["no_ext_d_1"]=$item_1->no_ext_d;
    $GLOBALS["no_int_d_1"]=$item_1->no_int_d;
    $GLOBALS["colonia_d_1"]=$item_1->colonia_d;
    $GLOBALS["municipio_d_1"]=$item_1->municipio_d;
    $GLOBALS["localidad_d_1"]=$item_1->localidad_d;
    $GLOBALS["estado_d_1"]=$item_1->estado_d;
    $GLOBALS["cp_d_1"]=$item_1->cp_d;
    $GLOBALS["pais_d_1"]=$item_1->pais_d;
    $GLOBALS["trantadose_persona_1"]=$item_1->trantadose_persona;
    $GLOBALS["tipo_vialidad_t_1"]=$item_1->tipo_vialidad_t;
    $GLOBALS["calle_t_1"]=$item_1->calle_t;
    $GLOBALS["no_ext_t_1"]=$item_1->no_ext_t;
    $GLOBALS["no_int_t_1"]=$item_1->no_int_t;
    $GLOBALS["colonia_t_1"]=$item_1->colonia_t;
    $GLOBALS["municipio_t_1"]=$item_1->municipio_t;
    $GLOBALS["localidad_t_1"]=$item_1->localidad_t;
    $GLOBALS["estado_t_1"]=$item_1->estado_t;
    $GLOBALS["cp_t_1"]=$item_1->cp_t;
    $GLOBALS["correo_t_1"]=$item_1->correo_t;
    $GLOBALS["r_f_c_t_1"]=$item_1->r_f_c_t;
    $GLOBALS["curp_t_1"]=$item_1->curp_t;
    $GLOBALS["telefono_t_1"]=$item_1->telefono_t;
    $GLOBALS["presente_legal_1"]=$item_1->presente_legal;
    $GLOBALS["nombre_p_1"]=$item_1->nombre_p;
    $GLOBALS["apellido_paterno_p_1"]=$item_1->apellido_paterno_p;
    $GLOBALS["apellido_materno_p_1"]=$item_1->apellido_materno_p;
    $GLOBALS["nombre_identificacion_p_1"]=$item_1->nombre_identificacion_p;
    $GLOBALS["autoridad_emite_p_1"]=$item_1->autoridad_emite_p;
    $GLOBALS["numero_identificacion_p_1"]=$item_1->numero_identificacion_p;
    $GLOBALS["fecha_nac_apodera"]=$item_1->fecha_nac_apodera;
    $GLOBALS["rfc_apodera"]=$item_1->rfc_apodera;
    $GLOBALS["curp_apodera"]=$item_1->curp_apodera;
    $GLOBALS["genero_p_1"]=$item_1->genero_p;
    $GLOBALS["tipo_vialidad_p_1"]=$item_1->tipo_vialidad_p;
    $GLOBALS["calle_p_1"]=$item_1->calle_p;
    $GLOBALS["no_ext_p_1"]=$item_1->no_ext_p;
    $GLOBALS["no_int_p_1"]=$item_1->no_int_p;
    $GLOBALS["colonia_p_1"]=$item_1->colonia_p;
    $GLOBALS["municipio_p_1"]=$item_1->municipio_p;
    $GLOBALS["localidad_p_1"]=$item_1->localidad_p;
    $GLOBALS["estado_p_1"]=$item_1->estado_p;
    $GLOBALS["cp_p_1"]=$item_1->cp_p;
  }
  $GLOBALS["aux_genero_1_1"]='';
  if($GLOBALS["genero_1"]==1){
    $GLOBALS["aux_genero_1_1"]='Masculino';
  }else if($GLOBALS["genero_1"]==2){
    $GLOBALS["aux_genero_1_1"]='Femenino';  
  }else{
    $GLOBALS["aux_genero_1_1"]='';
  }
  $GLOBALS["clave_1_p_n"]='';
  $GLOBALS["pais_1_p_n"]='';
  $tipo_cliente_p_f_m_1_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$GLOBALS["pais_nacimiento_1"]);
  foreach ($tipo_cliente_p_f_m_1_pais_1 as $item_1_p) {
    $GLOBALS["clave_1_p_n"]=$item_1_p->clave;
    $GLOBALS["pais_1_p_n"]=$item_1_p->pais;
  }
  $GLOBALS["clave_1_p_n_2"]='';
  $GLOBALS["pais_1_p_n_2"]='';
  $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$GLOBALS["pais_nacionalidad_1"]);
  foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
    $GLOBALS["clave_1_p_n_2"]=$item_1_p->clave;
    $GLOBALS["pais_1_p_n_2"]=$item_1_p->pais;
  }
  $GLOBALS["clave_1_p_1"]='';
  $GLOBALS["pais_1_p_1"]='';
  $tipo_cliente_p_f_m_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$GLOBALS["pais_d_1"]);
  foreach ($tipo_cliente_p_f_m_1_pais_2 as $item_1_p) {
    $GLOBALS["clave_1_p_1"]=$item_1_p->clave;
    $GLOBALS["pais_1_p_1"]=$item_1_p->pais;
  }
  $GLOBALS["checke_1_d"]='';
  if($GLOBALS["trantadose_persona_1"]=='on'){
     $GLOBALS["checke_1_d"]='checked';
  }
  $GLOBALS["checke_1_r"]='';
  if($GLOBALS["presente_legal_1"]=='on'){
     $GLOBALS["checke_1_r"]='checked';
  }
  $GLOBALS["aux_genero_1_1_p"]='';
  $GLOBALS["aux_genero_1_2_p"]='';
  if($GLOBALS["genero_p_1"]==1){
    $GLOBALS["aux_genero_1_1_p"]='Masculino';
  }else if($GLOBALS["genero_p_1"]==2){
    $GLOBALS["aux_genero_1_1_p"]='Femenino';  
  }else{
    $GLOBALS["aux_genero_1_1_p"]='';
  }
  $GLOBALS["idcomplemento_persona_fisica"]=0;
  $GLOBALS["actualmente_usted_precandidato1"]=0;
  $GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1"]=0;
  $GLOBALS["afirmativa_indicar_nombre_precandidato1"]='';
  $GLOBALS["indicar_relacion1"]=0;
  $GLOBALS["usted_socio_algun_precandidato1"]=0;
  $GLOBALS["usted_asesor_agente_representante_algun_precandidato1"]=0;
  //$responsable_campania=0;
  $GLOBALS["servicio_contratar_producto_comprar_dirigido1"]=0;
  $GLOBALS["tercera_persona_precandidato1"]=0;
  $GLOBALS["nombre_persona_beneficiaria1"]='';
  $GLOBALS["relacion_mantiene_persona_beneficiaria1"]='';
  $GLOBALS["indicar_razon_motivo_realizar_tramit1"]='';
  $GLOBALS["indicar_relacion_mantiene_persona_beneficiaria1"]='';
  $GLOBALS["usted_responsable_campanna_politica1"]=0;
  

  $complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_fisica','idperfilamiento',$idperfilamiento);
  foreach ($complento_pf1 as $cpf1) {
    $GLOBALS["idcomplemento_persona_fisica"]=$cpf1->id;
    $GLOBALS["actualmente_usted_precandidato1"]=$cpf1->actualmente_usted_precandidato;
    $GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1"]=$cpf1->familiar_primer_segundo_tercer_grado_socio_algun_precandidato;
    $GLOBALS["afirmativa_indicar_nombre_precandidato1"]=$cpf1->afirmativa_indicar_nombre_precandidato;
    $GLOBALS["indicar_relacion1"]=$cpf1->indicar_relacion;
    $GLOBALS["usted_socio_algun_precandidato1"]=$cpf1->usted_socio_algun_precandidato;
    $GLOBALS["usted_asesor_agente_representante_algun_precandidato1"]=$cpf1->usted_asesor_agente_representante_algun_precandidato;
    $GLOBALS["servicio_contratar_producto_comprar_dirigido1"]=$cpf1->servicio_contratar_producto_comprar_dirigido;
    $GLOBALS["tercera_persona_precandidato1"]=$cpf1->tercera_persona_precandidato;
    $GLOBALS["nombre_persona_beneficiaria1"]=$cpf1->nombre_persona_beneficiaria;
    $GLOBALS["relacion_mantiene_persona_beneficiaria1"]=$cpf1->relacion_mantiene_persona_beneficiaria;
    $GLOBALS["indicar_razon_motivo_realizar_tramit1"]=$cpf1->indicar_razon_motivo_realizar_tramit;
    $GLOBALS["indicar_relacion_mantiene_persona_beneficiaria1"]=$cpf1->indicar_relacion_mantiene_persona_beneficiaria;
    $GLOBALS["usted_responsable_campanna_politica1"]=$cpf1->usted_responsable_campanna_politica;
  }
  $GLOBALS["actualmente_usted_precandidato1x1"]='';

  if($GLOBALS["actualmente_usted_precandidato1"]==1){
    $GLOBALS["actualmente_usted_precandidato1x1"]='Si';
  }else if($GLOBALS["actualmente_usted_precandidato1"]==2){
    $GLOBALS["actualmente_usted_precandidato1x1"]='No';
  }

  $GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1"]='';
  if($GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1"]==1){
    $GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1"]='Si';
  }else if($GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1"]==2){
    $GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1"]='No';  }

  $indicar_relacion1x1='';
  if($GLOBALS["indicar_relacion1"]==1){
    $indicar_relacion1x1='Esposo(a)';
  }else if($GLOBALS["indicar_relacion1"]==2){
    $indicar_relacion1x1='Concubino(a) ';
  }else if($GLOBALS["indicar_relacion1"]==3){
    $indicar_relacion1x1='Padre';
  }else if($GLOBALS["indicar_relacion1"]==4){
    $indicar_relacion1x1='Madre';
  }else if($GLOBALS["indicar_relacion1"]==5){
    $indicar_relacion1x1='Hermano(a)';
  }else if($GLOBALS["indicar_relacion1"]==6){
    $indicar_relacion1x1='Hijo(a) ';
  }else if($GLOBALS["indicar_relacion1"]==7){
    $indicar_relacion1x1='Hijastro(a)';
  }else if($GLOBALS["indicar_relacion1"]==8){
    $indicar_relacion1x1='Padrastro';
  }else if($GLOBALS["indicar_relacion1"]==9){
    $indicar_relacion1x1='Madrastra';
  }else if($GLOBALS["indicar_relacion1"]==10){
    $indicar_relacion1x1='Abuelo(a)';
  }else if($GLOBALS["indicar_relacion1"]==11){
    $indicar_relacion1x1='Tio(a) ';
  }else if($GLOBALS["indicar_relacion1"]==12){
    $indicar_relacion1x1='Sobrino(a)';
  }else if($GLOBALS["indicar_relacion1"]==13){
    $indicar_relacion1x1='Primo(a)'; 
  }else if($GLOBALS["indicar_relacion1"]==14){
    $indicar_relacion1x1='Suegro(a) ';
  }else if($GLOBALS["indicar_relacion1"]==15){
    $indicar_relacion1x1='Cuñado(a)';
  }

  $usted_socio_algun_precandidato1x1='';
  if($GLOBALS["usted_socio_algun_precandidato1"]==1){
    $usted_socio_algun_precandidato1x1='Si';
  }else if($GLOBALS["usted_socio_algun_precandidato1"]==2){
    $usted_socio_algun_precandidato1x1='No';
  }

  $usted_asesor_agente_representante_algun_precandidato1x1='';
  if($GLOBALS["usted_asesor_agente_representante_algun_precandidato1"]==1){
    $usted_asesor_agente_representante_algun_precandidato1x1='Si';
  }else if($GLOBALS["usted_asesor_agente_representante_algun_precandidato1"]==2){
    $usted_asesor_agente_representante_algun_precandidato1x1='No';
  }

  $servicio_contratar_producto_comprar_dirigido1x1='';
  if($GLOBALS["servicio_contratar_producto_comprar_dirigido1"]==1){
    $servicio_contratar_producto_comprar_dirigido1x1='Si';
  }else if($GLOBALS["servicio_contratar_producto_comprar_dirigido1"]==2){
    $servicio_contratar_producto_comprar_dirigido1x1='No';
  }

  $tercera_persona_precandidato1x1='';
  if($GLOBALS["tercera_persona_precandidato1"]==1){
    $tercera_persona_precandidato1x1='Si';
  }else if($GLOBALS["tercera_persona_precandidato1"]==2){
    $tercera_persona_precandidato1x1='No';
  }

  $usted_responsable_campanna_politica1x1='';
  $usted_responsable_campanna_politica1x2='';
  if($GLOBALS["usted_responsable_campanna_politica1"]==1){
    $usted_responsable_campanna_politica1x1='Si';
  }else if($GLOBALS["usted_responsable_campanna_politica1"]==2){
    $usted_responsable_campanna_politica1x1='No';
  }

  $actf=$this->ModeloCatalogos->getselectwherestatus('acitividad','actividad_econominica_fisica',array("clave"=>$GLOBALS["activida_o_giro_1"]));
  foreach ($actf as $item) {
    $GLOBALS["activida_o_giro_1"]=$item->acitividad;
  }
  $get_tipo_vialidad=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$GLOBALS["tipo_vialidad_d_1"]));
  foreach ($get_tipo_vialidad as $item){
    $GLOBALS["tipo_vialidad_d_1"] = $item->nombre; 
  }
  $gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$GLOBALS["estado_d_1"]));
  foreach ($gete as $item){
    $GLOBALS["estado_d_1"]=$item->estado; 
  }
  $gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$GLOBALS["estado_t_1"]));
  foreach ($gete as $item){
    $GLOBALS["estado_t_1"]=$item->estado; 
  }
  $gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$GLOBALS["estado_p_1"]));
  foreach ($gete as $item){
    $GLOBALS["estado_p_1"]=$item->estado; 
  }
//

class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
    $html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce a tu Cliente');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Persona física mexicana o extranjera con condición de estancia temporal o permanente');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '12');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';

$html.='   
        <h4>Formato: Conoce a tu cliente</h4>
        <h4>Fecha: '.$fecha_formato.'</h4>
        <hr class="subtitle barra_menu">

        <h4>Persona física mexicana o extranjera con condición de estancia temporal o permanente</h4>
        <h4>Datos Generales</h4>
        <div class="row">
          <table width="100%">
            <tr>
              <td>
                <label>Nombre(s):</label>
                <span><strong> '.$GLOBALS["nombre_1"].'</strong></span>
              </td>
              <td>
                <label>Apellido paterno:</label>
                <span><strong>'.$GLOBALS["apellido_paterno_1"].'</strong></span>
              </td>
              <td>
                <label>Apellido materno:</label>
                <span><strong>'.$GLOBALS["apellido_materno_1"].'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>Nombre de identificación:</label>
                <span><strong>'.$GLOBALS["nombre_identificacion_1"].'</span></strong>
              </td>
              <td>
                <label>Autoridad que la emite:</label>
                <span><strong>'.$GLOBALS["autoridad_emite_1"].'</span></strong>
              </td>
              <td>
                <label>Número de identificación:</label>
                <span><strong>'.$GLOBALS["numero_identificacion_1"].'</span></strong>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>Género:</label>
                <span><strong>'.$GLOBALS["aux_genero_1_1"].'</span></strong>
              </td>
              <td>
                <label>Fecha de nacimiento:</label>
                <span><strong>'.$GLOBALS["fecha_nacimiento_1"].'</span></strong>
              </td>
              <td>
                <label>País de nacimiento:</label>
                <span><strong>'.$GLOBALS["pais_1_p_n"].'</span></strong>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>Pais de nacionalidad:</label>
                <span><strong>'.$GLOBALS["pais_1_p_n_2"].'</span></strong>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <div class="col-md-12 form-group">
            <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
            <span><strong>' .$GLOBALS["activida_o_giro_1"].'</span></strong>
          </div>
        </div>
  
        <div class="row">
          <div class="col-md-12">
            <hr class="subtitle">
            <h4>Dirección</h4>
          </div>
          <table width="100%">
            <tr>
              <td width="25%">
                <label>País:</label>
                <span><strong> '.$GLOBALS["pais_1_p_1"].'</strong></span>
              </td>
              <td width="30%">
                <label>Tipo de vialidad:</label>
                <span><strong> '.$GLOBALS["tipo_vialidad_d_1"].'</strong></span>
              </td>
              <td width="30%">
                <label>Calle:</label>
                <span><strong> '.$GLOBALS["calle_d_1"].'</strong></span>
              </td>
              <td width="15%">
                <label>No.ext:</label>
                <span><strong> '.$GLOBALS["no_ext_d_1"].'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>No.int:</label>
                <span><strong> '.$GLOBALS["no_int_d_1"].'</strong></span>
              </td>
              <td>
                <label>C.P:</label>
                <span><strong> '.$GLOBALS["cp_d_1"].'</strong></span>
              </td>
              <td>
                <label>Municipio o delegación:</label>
                <span><strong> '.$GLOBALS["municipio_d_1"].'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>Localidad:</label>
                <span><strong> '.$GLOBALS["localidad_d_1"].'</strong></span>
              </td>
              <td>
                <label>Estado:</label>
                <span><strong> '.$GLOBALS["estado_d_1"].'</strong></span>
              </td>
              <td>
                <label>Colonia:</label>
                <span><strong> '.$GLOBALS["colonia_d_1"].'</strong></span>
              </td> 
            </tr>
          </table>              
        </div>';
        $dis = "none";
        if($GLOBALS["checke_1_d"]=="checked"){
          $dis = "block";
          $html.='
          <div class="row" >
            <div class="col-md-12 form-group">
              <div class="form-check form-check-primary">
                <label class="form-check-label">Tratándose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio.
                </label>
              </div>
              <div class="residenciaEx_1" style="display: '.$dis.';">
                <hr class="subtitle">
                <div class="col-md-12">
                  <h4>Dirección en México:</h4>
                </div>

                <table width="100%">
                  <tr>
                    <td>
                      <label>Tipo de vialidad:</label>';
                        $get_tipo_vialidad=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$GLOBALS["tipo_vialidad_t_1"]));
                          foreach ($get_tipo_vialidad as $item){
                            $GLOBALS["tipo_vialidad_t_1"] = $item->nombre; 
                          }
                      $html.='<span><strong> '.$GLOBALS["tipo_vialidad_t_1"].'</strong></span>
                    </td>
                    <td>
                      <label>Calle:</label>
                      <span><strong> '.$GLOBALS["calle_t_1"].'</strong></span>
                    </td>
                    <td>
                      <label>No.ext:</label>
                      <span><strong> '.$GLOBALS["no_ext_t_1"].'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <label>No.int:</label>
                      <span><strong> '.$GLOBALS["no_int_t_1"].'</strong></span>
                    </td>
                    <td>
                      <label>C.P:</label>
                      <span><strong> '.$GLOBALS["cp_t_1"].'</strong></span>
                    </td>
                    <td>
                      <label>Municipio o delegación:</label>
                      <span><strong> '.$GLOBALS["municipio_t_1"].'</strong></span>
                    </td>
                  </tr>
                </table>
                <table width="100%">
                  <tr>
                    <td><br></td>
                  </tr>
                </table> 
                <table width="100%">
                  <tr>
                    <td>
                      <label>Localidad:</label>
                      <span><strong> '.$GLOBALS["localidad_t_1"].'</strong></span>
                    </td>
                    <td>
                      <label>Estado:</label>
                      <span><strong> '.$GLOBALS["estado_t_1"].'</strong></span>
                    </td>
                    <td>
                      <label>Colonia:</label>
                      <span><strong> '.$GLOBALS["colonia_t_1"].'</strong></span>
                    </td>    
                  </tr>
                </table>  
                <table width="100%">
                  <tr>
                    <td><br><hr></td>
                  </tr>
                </table>      
              </div>
            </div>
            
          </div>';
        }

        $html.='
          <table width="100%">
            <tr>
              <td>
                <label>Correo electrónico:</label>
                <span><strong> '.$GLOBALS["correo_t_1"].'</strong></span>
              </td>
              <td>
                <label>R.F.C:</label>
                <span><strong> '.$GLOBALS["r_f_c_t_1"].'</strong></span>
              </td>
            </tr>
          </table>  
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table> 
          <table width="100%">
            <tr>
              <td>
                <label>CURP:</label>
                <span><strong> '.$GLOBALS["curp_t_1"].'</strong></span>
              </td>
              <td>
                <label>Número de teléfono:</label>
                <span><strong> '.$GLOBALS["telefono_t_1"].'</strong></span>
              </td>
            </tr>
          </table> 
        
          <!--//////////////////////-->
        ';
          $dis2 = "none";
          if($GLOBALS["checke_1_r"]=="checked"){
            $dis2 = "block";
            $html.='<div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                      </div>
                      <div class="col-md-12 form-group">
                        <label class="form-check-label">Presenta apoderado legal.</label>
                      </div>

                      <table width="100%">
                        <tr>
                          <td>
                            <label>Nombre(s):</label>
                            <span><strong> '.$GLOBALS["nombre_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>Apellido paterno:</label>
                            <span><strong> '.$GLOBALS["apellido_paterno_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>Apellido materno:</label>
                            <span><strong> '.$GLOBALS["apellido_materno_p_1"].'</strong></span>
                          </td>
                        </tr>
                      </table>  
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table> 
                      <table width="100%">
                        <tr>
                          <td>
                            <label>Nombre de identificación:</label>
                            <span><strong> '.$GLOBALS["nombre_identificacion_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>Autoridad que emite la identificación:</label>
                            <span><strong> '.$GLOBALS["autoridad_emite_p_1"].'</strong></span>
                          </td>
                        </tr>
                      </table>  
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table> 
                      <table width="100%">
                        <tr>
                          <td>
                            <label>Número de identificación:</label>
                            <span><strong> '.$GLOBALS["numero_identificacion_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>Fecha de nacimiento:</label>
                            <span><strong> '.$GLOBALS["fecha_nac_apodera"].'</strong></span>
                          </td>
                        </tr>
                      </table>  
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td>
                            <label>RFC:</label>
                            <span><strong> '.$GLOBALS["rfc_apodera"].'</strong></span>
                          </td>
                          <td>
                            <label>CURP:</label>
                            <span><strong> '.$GLOBALS["curp_apodera"].'</strong></span>
                          </td>
                          <td>
                            <label>Género:</label>
                            <span><strong> '.$GLOBALS["aux_genero_1_1_p"].'</strong></span>
                          </td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table>

                      <table width="100%">
                        <tr>
                          <td>
                            <label>Tipo de vialidad:</label>';
                            $get_tipo_vialidad=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$GLOBALS["tipo_vialidad_p_1"]));
                            foreach ($get_tipo_vialidad as $item){
                              $GLOBALS["tipo_vialidad_p_1"] = $item->nombre; 
                            }
                            $html.='<span><strong> '.$GLOBALS["tipo_vialidad_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>Calle:</label>
                            <span><strong> '.$GLOBALS["calle_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>No.ext:</label>
                            <span><strong> '.$GLOBALS["no_ext_p_1"].'</strong></span>
                          </td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td width="25%">
                            <label>No.int:</label>
                            <span><strong> '.$GLOBALS["no_int_p_1"].'</strong></span>
                          </td>
                          <td width="25%">
                            <label>C.P:</label>
                            <span><strong> '.$GLOBALS["cp_p_1"].'</strong></span>
                          </td>
                          <td width="50%">
                            <label>Mcpio. o delegación:</label>
                            <span><strong> '.$GLOBALS["municipio_p_1"].'</strong></span>
                          </td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td>
                            <label>Localidad:</label>
                            <span><strong> '.$GLOBALS["localidad_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>Estado:</label>
                            <span><strong> '.$GLOBALS["estado_p_1"].'</strong></span>
                          </td>
                          <td>
                            <label>Colonia:</label>
                            <span><strong> '.$GLOBALS["colonia_p_1"].'</strong></span>
                          </td>
                        </tr>
                      </table>          
                    
                    
                    </div>';
          }
          //$html.='<div class="presenta_apoderado_style_1" >
          
        $html.='
      <!--/////////// Replicado de beneficiario///////////-->
      
      <div class="ptext_beneficiario_1">
        <hr class="subtitle barra_menu">
        <div class="text_beneficiario_1">';
          $contb=0;
          $results=$this->ModeloCatalogos->getselectwhere('tipo_cliente_p_f_m_beneficiario','idtipo_cliente_p_f_m',$GLOBALS["idtipo_cliente_p_f_m_1"]);
          foreach($results as $b){
            $contb++;
          }
          if($contb>0){
            $html.='<h4>Beneficiarios</h4>';
          }
          $genero="";
          foreach($results as $b){
            if($b->genero==1)
              $genero="Masculino";
            else
              $genero="Femenino";

            $tipov="";
            $get_tv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$b->tipo_vialidad));
            foreach ($get_tv as $item){
              $tipov = $item->nombre; 
            }
            $estadob="";
            $gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$b->estado));
            foreach ($gete as $item){
              $estadob=$item->estado; 
            }
          
            $html.='<div class="row">
              <table width="100%">
                <tr>
                  <td>
                    <label>Nombre(s):</label>
                    <span><strong> '.$b->nombre.'</strong></span>
                  </td>
                  <td>
                    <label>Apellido paterno:</label>
                    <span><strong> '.$b->apellido_paterno.'</strong></span>
                  </td>
                  <td>
                    <label>Apellido materno:</label>
                    <span><strong> '.$b->apellido_materno.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td width="40%">
                    <label> Fecha de nacimiento:</label>
                    <span><strong> '.$b->fecha_nacimiento.'</strong></span>
                  </td>
                  <td width="20%">
                    <label>Género:</label>
                    <span><strong> '.$genero.'</strong></span>
                  </td>
                  <td width="40%">
                    <label>% Porcentaje de propiedad:</label>
                    <span><strong> '.$b->porcentaje.'%</strong></span>
                  </td>
                </tr>
              </table>
  
              <div class="col-md-12 form-group"><h4>Dirección</h4></div>
              <table width="100%">
                <tr>
                  <td>
                    <label>Tipo de vialidad:</label>
                    <span><strong> '.$tipov.'</strong></span>
                  </td>
                  <td>
                    <label>Calle:</label>
                    <span><strong> '.$b->calle.'</strong></span>
                  </td>
                  <td>
                    <label>No.ext:</label>
                    <span><strong> '.$b->no_ext.'</strong></span>
                  </td>
                  <td>
                    <label>No.int:</label>
                    <span><strong> '.$b->no_int.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>

              <table width="100%">
                <tr>
                  <td>
                    <label>C.P:</label>
                    <span><strong> '.$b->cp.'</strong></span>
                  </td>
                  <td>
                    <label>Mcpio. o delegación:</label>
                    <span><strong> '.$b->municipio.'</strong></span>
                  </td>
                  <td>
                    <label>Localidad:</label>
                    <span><strong> '.$b->localidad.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>Estado:</label>
                    <span><strong> '.$estadob.'</strong></span>
                  </td>
                  <td>
                    <label>Colonia:</label>
                    <span><strong> '.$b->colonia.'</strong></span>
                  </td>
                </tr>
              </table>
              <div class="col-md-12 barra_menu"><hr class="subsubtitle"></div>';
          }
          $html.='</div>
        </div>
      </div>';
        if($GLOBALS["actualmente_usted_precandidato1x1"]!=""){
          $html.='<div class="col-md-10">
            <label>¿Actualmente es usted precandidato(a), candidato(a), dirigente partidista, servidor público (a)?        
            </label>
          </div>
          <div class="col-md-2 form-group">
            <span><strong> '.$GLOBALS["actualmente_usted_precandidato1x1"].'</strong></span>
          </div>';
        }
        if($GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1"]!=""){
          $html.='<div class="col-md-10">
            <label>¿Es familiar en primer, segundo o tercer grado o socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                          
            </label>
          </div>
          <div class="col-md-2 form-group">
            <span><strong> '.$GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1"].'</strong></span>
          </div>';
        }
       
        if($GLOBALS["familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1"]=="Si"){
          $html.='<div class="col-md-12 form-group">
            <label>Nombre del precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta:</label>
            <span><strong> '.$GLOBALS["afirmativa_indicar_nombre_precandidato1"].'</strong></span>
          </div>
          
          <div class="col-md-2 form-group">
            <label>Relación:</label> 
            <span><strong> '.$indicar_relacion1x1.'</strong></span>
          </div>';
        }
        if($usted_socio_algun_precandidato1x1!=""){
          $html.='<div class="row" id="preg3">
            <div class="col-md-10">
              <label>¿Es usted socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                          
              </label>
            </div>
            <div class="col-md-2 form-group">
              <span><strong> '.$usted_socio_algun_precandidato1x1.'</strong></span>
            </div>
          </div>';
        }
        if($usted_asesor_agente_representante_algun_precandidato1x1!=""){
          $html.='<div class="col-md-10">
            <label>¿Es usted asesor o agente o representante de  algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                       
            </label>
          </div>
          <div class="col-md-2 form-group">
            <span><strong> '.$usted_asesor_agente_representante_algun_precandidato1x1.'</strong></span>
          </div>';
        }
        if($usted_responsable_campanna_politica1x1!=""){
          $html.='<div class="col-md-10">
            <label>¿Es usted responsable de campaña política, encargado financiero y/ contable de algún partido político o de algún órgano interno de los partidos políticos y/o candidatos comunes y/o frentes y/o candidatos independientes?      
            </label>
          </div>
          <div class="col-md-2 form-group">
            <span><strong> '.$usted_responsable_campanna_politica1x1.'</strong></span>
          </div>';
        }
        
        if($servicio_contratar_producto_comprar_dirigido1x1!=""){
          $html.='<div class="col-md-10">
            <label>¿El servicio a contratar o producto a comprar está dirigido o es para el beneficio de alguna tercera persona?                     
            </label>
          </div>
          <div class="col-md-2 form-group">
            <span><strong> '.$servicio_contratar_producto_comprar_dirigido1x1.'</strong></span>
          </div>';
        }
        
        if($servicio_contratar_producto_comprar_dirigido1x1=="Si"){
          $html.='
              <div class="col-md-10">
                <label>La tercera persona es precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                   
                </label>
              </div>
              <div class="col-md-2 form-group">
                <span><strong> '.$tercera_persona_precandidato1x1.'</strong></span>
              </div>';
        }
        if($tercera_persona_precandidato1x1=="Si"){
          $html.='
              <div class="col-md-12 form-group">
                <label>Favor de indicar el nombre de la persona beneficiaria
                </label>
                <span><strong> '.$GLOBALS["nombre_persona_beneficiaria1"].'</strong></span>
              </div>
          
              <div class="col-md-12 form-group">
                <label>Indicar la relación que mantiene con la persona beneficiaria
                </label>
                <span><strong> '.$GLOBALS["relacion_mantiene_persona_beneficiaria1"].'</strong></span>
              </div>
   
              <div class="col-md-12 form-group">
                <label>Indicar por qué razón o motivo acude a realizar el trámite o compra para la tercera persona
                </label>
                <span><strong> '.$GLOBALS["indicar_razon_motivo_realizar_tramit1"].'</strong></span>
            </div>';
        }
        
      $html.='
        <div class="firma_tipo_cliente">
          <br>
          <br>
          <br>
          <div class="firma_tipo_cliente">
            <table width="100%">
              '.$label_a8.'
              <tr>
                <th width="48%">
                <div class="col-md-6" style="text-align: justify !important;">
                  <hr>
                  <p style="text-align:center">'.$GLOBALS["nombre_1"].' '.$GLOBALS["apellido_paterno_1"].' '.$GLOBALS["apellido_materno_1"].'</p>
                    Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
                </div>
                </th>
                <th width="4%"> </th>
                <th width="48%">
                <div class="col-md-6" style="text-align: justify !important;">
                  <hr>
                  <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
                    Declaro bajo protesta de decir verdad, que los datos asentados en la presente solicitud ha sido proporcionados directamente por el cliente.
                </div> 
                </th>
              </tr>
            </table>
          </div>
        </div>';     

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('formatoConoceCliente.pdf', 'I');

?>