<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

$gen="";
if($b->genero==1)
  $gen="Masculino";
else
  $gen="Femenino";

class MYPDF extends TCPDF {
    //Page header
    public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
    }

    // Page footer
    public function Footer() {
      $html = '';
      $html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
    }
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce al dueño beneficiario');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Personas Fisicas');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '15');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='
        <h3>Formato: Conoce al dueño beneficiario</h3>
        <h3>Fecha: '.$fecha.'</h3>
        <h3>Persona Fisica</h3>

        <hr class="subtitle">
        <div class="row">
          <table width="100%">
            <tr>
              <td>
                <label>Nombre(s):</label>
                <span><strong> '.$b->nombre.'</strong></span>
              </td>
              <td>
                <label>Apellido paterno:</label>
                <span><strong> '.$b->apellido_paterno.'</strong></span>
              </td>
              <td>
                <label>Apellido materno:</label>
                <span><strong> '.$b->apellido_materno.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label width="35%">Nombre de identificación:</label>
                <span><strong> '.$b->nombre_identificacion.'</strong></span>
              </td>
              <td>
                <label width="25%">Autoridad que la emite:</label>
                <span><strong> '.$b->autoridad_emite.'</strong></span>
              </td>
              <td>
                <label width="40%">Número de identificación:</label>
                <span><strong> '.$b->numero_identificacion.'</strong></span>
              </td>
            </tr>
          </table>

          <div class="col-md-7 form-group">
            <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
            <span><strong> '.$actividad_giro.'</strong></span>
          </div>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>Género:</label>
                <span><strong> '.$gen.'</strong></span>
              </td>  
              <td>
                <label>Fecha de nacimiento:</label>
                <span><strong> '.$b->fecha_nacimiento.'</strong></span>
              </td>
              <td>
                <label>País de nacimiento:</label>
                <span><strong> '.$pais_nacimiento.'</strong></span>
              </td>
              <td>
                <label>País de nacionalidad:</label>
                <span><strong> '.$pais_nacionalidad.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br><hr></td>
            </tr>
          </table>
          <div class="col-md-12">
            <h4>Dirección</h4>
          </div>
          <table width="100%">
            <tr>
              <td>
                <label>Tipo de vialidad:</label>
                <span><strong> '.$tipo_vialidad.'</strong></span>
              </td>
              <td>
                <label>Calle:</label>
                <span><strong> '.$b->calle.'</strong></span>
              </td>
              <td>
                <label>No.ext:</label>
                <span><strong> '.$b->no_ext.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td width="20%">
                <label>No.int:</label>
                <span><strong> '.$b->no_int.'</strong></span>
              </td>
              <td width="20%">
                <label>C.P:</label>
                <span><strong> '.$b->cp.'</strong></span>
              </td>
              <td width="30%">
                <label>Mcpio. o delegación:</label>
                <span><strong> '.$b->monicipio.'</strong></span>
              </td>
              <td width="30%">
                <label>Localidad:</label>
                <span><strong> '.$b->localidad.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>Estado:</label>
                <span><strong> '.$estado.'</strong></span>
              </td>
              <td>
                <label>Colonia:</label>
                <span><strong> '.$b->colonia.'</strong></span>
              </td>
              <td>
                <label>País:</label>
                <span><strong> '.$pais.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br><hr></td>
            </tr>
          </table>
        </div>';
        if($b->trantadose_persona=="on"){
          $html.='
          <div class="row">
            <div class="col-md-12 form-group">
                <label class="form-check-label">Tratándose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio.
                </label>
            </div>

            <div class="col-md-12">
              <h4>Dirección en México.</h4>
            </div>
            <table width="100%">
              <tr>
                <td width="30%">
                  <label>Tipo de vialidad:</label>
                  <span><strong> '.$tipo_vialidad_d.'</strong></span>
                </td>
                <td width="50%">
                  <label>Calle:</label>
                  <span><strong> '.$b->calle_d.'</strong></span>
                </td>
                <td width="20%">
                  <label>No.ext:</label>
                  <span><strong> '.$b->no_ext_d.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td width="20%">
                  <label>No.int:</label>
                  <span><strong> '.$b->no_int_d.'</strong></span>
                </td>
                <td width="20%">
                  <label>C.P:</label>
                  <span><strong> '.$b->cp_d.'</strong></span>
                </td>
                <td width="30%">
                  <label>Mcpio. o delegación:</label>
                  <span><strong> '.$b->municipio_d.'</strong></span>
                </td>
                <td width="30%">
                  <label>Localidad:</label>
                  <span><strong> '.$b->localidad_d.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>Estado:</label>
                  <span><strong> '.$estado_d.'</strong></span>
                </td>
                <td>
                  <label>Colonia:</label>
                  <span><strong> '.$b->colonia_d.'</strong></span>
                </td>
              </tr>
            </table>
          </div>';
        }

        $html.='<hr>
        <div class="row">
          <table width="100%">
            <tr>
              <td width="40%">
                <label>Correo electrónico:</label>
                <span><strong> '.$b->correo_d.'</strong></span>
              </td>
              <td width="30%">
                <label>R.F.C:</label>
                <span><strong> '.$b->r_f_c_d.'</strong></span>
              </td>
              <td width="30%">
                <label>CURP:</label>
                <span><strong> '.$b->curp_d.'</strong></span>
              </td>
            </tr>
          </table>
        </div>
    
        <div class="firma_tipo_cliente">
          <br><br>
          <div class="firma_tipo_cliente">
            <div class="row">
              <div class="col-md-12" style="text-align: justify !important;">
                <hr>
                <p style="text-align:center">'.mb_strtoupper($nombre,"UTF-8").'</p>
                  Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
              </div>
            </div>
          </div>
        </div>';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Cuestionario beneficiario.pdf', 'I');

?>