<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

if (strtotime($fecha) !== false) {
	$fecha_formato = date("d-m-Y", strtotime($fecha));
} else {
	$fecha_formato = date("d-m-Y");
}

	$idtipo_cliente_p_m_m_d_4=0;
    $nombre_persona_4='';
    $r_f_c_4='';
    $giro_mercantil_4='';
    $nacionalidad_4='';
    $tipo_vialidad_d_4='';
    $calle_d_4='';
    $no_ext_d_4='';
    $no_int_d_4='';
    $colonia_d_4='';
    $municipio_d_4='';
    $localidad_d_4='';
    $estado_d_4='';
    $cp_d_4='';
    $fecha_constitucion_d_4='';
    $nombre_g_4='';
    $apellido_paterno_g_4='';
    $apellido_materno_g_4='';
    $genero_g_4=0;
    $fecha_nacimiento_4='';
    $rfc_g4='';
    $curp_g4='';
    $arraywhere_4 = array('idperfilamiento'=>$idperfilamiento); 
    $tipo_cliente_p_m_m_d=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_m_m_d',$arraywhere_4);
    foreach ($tipo_cliente_p_m_m_d as $item_4){
      $idtipo_cliente_p_m_m_d_4=$item_4->idtipo_cliente_p_m_m_d;
      $nombre_persona_4=$item_4->nombre_persona;
      $r_f_c_4=$item_4->r_f_c;
      $giro_mercantil_4=$item_4->giro_mercantil;
      $nacionalidad_4=$item_4->nacionalidad;
      $tipo_vialidad_d_4=$item_4->tipo_vialidad_d;
      $calle_d_4=$item_4->calle_d;
      $no_ext_d_4=$item_4->no_ext_d;
      $no_int_d_4=$item_4->no_int_d;
      $colonia_d_4=$item_4->colonia_d;
      $municipio_d_4=$item_4->municipio_d;
      $localidad_d_4=$item_4->localidad_d;
      $estado_d_4=$item_4->estado_d;
      $cp_d_4=$item_4->cp_d;
      $fecha_constitucion_d_4=$item_4->fecha_constitucion_d;
      $nombre_g_4=$item_4->nombre_g;
      $apellido_paterno_g_4=$item_4->apellido_paterno_g;
      $apellido_materno_g_4=$item_4->apellido_materno_g;
      $genero_g_4=$item_4->genero_g;
      $fecha_nacimiento_4=$item_4->fecha_nacimiento;
      $rfc_g4=$item_4->rfc_g;
      $curp_g4=$item_4->curp_g;
    }
    $tipo_cliente_p_m_m_d_p=$this->ModeloCatalogos->getselectwhere('pais','clave',$nacionalidad_4);
      foreach ($tipo_cliente_p_m_m_d_p as $item_t_4) {
        $clave_t_4=$item_t_4->clave;
        $pais_t_4=$item_t_4->pais;
      }
    $actividad_econominica_morales=$this->ModeloCatalogos->getData('actividad_econominica_morales');
    $genero_t_4='';
    if($genero_g_4==1){
      $genero_t_4='Masculino';
    }else if($genero_g_4==2){
      $genero_t_4='Femenino';
    }
  
    $idcomplemento_persona_moral=0;
    $empresa_persona_moral_hay_algun_apoderado_legal=0;
    $empresa_persona_moral_hay_algun_apoderado_legal_accionista=0;
    $empresa_persona_moral_existe_algun_precandidato=0;
    $empresa_persona_moral_existe_algun_responsable_campana_politica=0;
    $respondio_si_pregunta_anterior_persona_moral=0;
    
    
    $complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_moral','idperfilamiento',$idperfilamiento);
    foreach ($complento_pf1 as $cpm1) {
      $idcomplemento_persona_moral=$cpm1->id;
      $empresa_persona_moral_hay_algun_apoderado_legal=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal;
      $empresa_persona_moral_hay_algun_apoderado_legal_accionista=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal_accionista;
      $empresa_persona_moral_existe_algun_precandidato=$cpm1->empresa_persona_moral_existe_algun_precandidato;
      $empresa_persona_moral_existe_algun_responsable_campana_politica=$cpm1->empresa_persona_moral_existe_algun_responsable_campana_politica;
      $respondio_si_pregunta_anterior_persona_moral=$cpm1->respondio_si_pregunta_anterior_persona_moral;
      
    }

    $actm=$this->ModeloCatalogos->getselectwherestatus('giro_mercantil','actividad_econominica_morales',array("clave"=>$giro_mercantil_4));
	foreach ($actm as $item2) {
		$giro_mercantil_4=$item2->giro_mercantil;
	}

	$gettv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$tipo_vialidad_d_4));
	foreach ($gettv as $item){
		$tipo_vialidad_d_4=$item->nombre; 
	}

	$gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$estado_d_4));
	foreach ($gete as $item){
        $estado_d_4=$item->estado; 
    }

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce a tu Cliente');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Persona moral mexicana de derecho público detallados en el anexo 7 bis A');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '13');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.=' <div class="row firma_tipo_cliente">
            <div class="col-md-12">    
	        	<h4>Formato: Conoce a tu cliente</h4>
	        	<h4>Fecha: '.$fecha_formato.'</h4>
	        	<hr class="subtitle barra_menu">

            	<h4>Persona moral mexicana de derecho público detallados en el anexo 7 bis A</h4>
           		<h4>Datos Generales</h4>';

          $html.='
                  <div class="row">
                    <table width="100%">
                      <tr>
                        <td width="60%">
                          <label>Nombre de la persona moral de derecho público:</label>
                          <span><strong> '.$nombre_persona_4.'</strong></span>
                        </td>
                        <td width="40%">
                          <label>R.F.C:</label>
                          <span><strong> '.$r_f_c_4.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td width="40%">
                          <label>Nacionalidad:</label>
                          <span><strong> '.$pais_t_4.'</strong></span>
                        </td>  
                        <td width="60%">
                          <label>Giro mercantil / Actividad / Objeto social:</label>
                          <span><strong> '.$giro_mercantil_4.'</strong></span>
                        </td> 
                      </tr>
                    </table> 
  
                    <div class="col-md-12">
                      <hr class="subtitle">
                      <h4>Dirección</h4>
                    </div>
                    <table width="100%">
                      <tr>
                        <td width="30%">
                          <label>Tipo de vialidad:</label>
                          <span><strong> '.$tipo_vialidad_d_4.'</strong></span>
                        </td>
                        <td width="40%">
                          <label>Calle:</label>
                          <span><strong> '.$calle_d_4.'</strong></span>
                        </td>
                        <td width="15%">
                          <label>No.ext:</label>
                          <span><strong> '.$no_ext_d_4.'</strong></span>
                        </td>
                        <td width="15%">
                          <label>No.int:</label>
                          <span><strong> '.$no_int_d_4.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td>
                          <label>C.P:</label>
                          <span><strong> '.$cp_d_4.'</strong></span>
                        </td>
                        <td>
                          <label>Mcpio. o delegación:</label>
                          <span><strong> '.$municipio_d_4.'</strong></span>
                        </td>
                        <td>
                          <label>Localidad:</label>
                          <span><strong> '.$localidad_d_4.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td width="30%">
                          <label>Estado:</label>
                          <span><strong> '.$estado_d_4.'</strong></span>
                        </td>
                        <td width="30%">
                          <label>Colonia:</label>
                          <span><strong> '.$colonia_d_4.'</strong></span>
                        </td>
                        <td width="40%">
                          <label>Fecha de constitución:</label>
                          <span><strong> '.$fecha_constitucion_d_4.'</strong></span>
                        </td>
                      </tr>
                    </table>
                  </div>
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>DATOS GENERALES DE LOS SERVIDORES PÚBLICOS QUE REALICEN EL ACTO O LA OPERACIÓN</h4>
                      </div>
                      <table width="100%">
                        <tr>
                          <td>
                            <label>Nombre(s):</label>
                            <span><strong> '.$nombre_g_4.'</strong></span>
                          </td>
                          <td>
                            <label>Apellido paterno:</label>
                            <span><strong> '.$apellido_paterno_g_4.'</strong></span>
                          </td>
                          <td>
                            <label>Apellido materno:</label>
                            <span><strong> '.$apellido_materno_g_4.'</strong></span>
                          </td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td><br></td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr>
                          <td>
                            <label>Género:</label>
                            <span><strong> '.$genero_t_4.'</strong></span>
                          </td>
                          <td>
                            <label>Fecha de nacimiento:</label>
                            <span><strong> '.$fecha_nacimiento_4.'</strong></span>
                          </td>
                        </tr>
                      </table>
                    </div>

            <div class="firma_tipo_cliente">
	          <br>
	          <br>
	          <br> <br>
	          <br>
	          <div class="firma_tipo_cliente">
	            <table width="100%">
                '.$label_a8.'
	              <tr>
	                <th width="48%">
	                <div class="col-md-6" style="text-align: justify !important;">
	                  <hr>
	                  <p style="text-align:center">'.$nombre_g_4.' '.$apellido_paterno_g_4.' '.$apellido_materno_g_4.'</p>
	                    Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
	                </div>
	                </th>
	                <th width="4%"> </th>
	                <th width="48%">
	                <div class="col-md-6" style="text-align: justify !important;">
	                  <hr>
	                  <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
	                    Declaro bajo protesta de decir verdad, que los datos asentados en la presente solicitud ha sido proporcionados directamente por el cliente.
	                </div> 
	                </th>
	              </tr>
	            </table>
	          </div>
	        </div>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('formatoConoceCliente.pdf', 'I');

?>