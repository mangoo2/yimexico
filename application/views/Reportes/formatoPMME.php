<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

if (strtotime($fecha) !== false) {
	$fecha_formato = date("d-m-Y", strtotime($fecha));
} else {
	$fecha_formato = date("d-m-Y");
}

$idtipo_cliente_p_m_m_e_3=0;
$razon_social_3='';
$giro_mercantil_3='';
$clave_registro_3='';
$extranjero_3='';
$pais_3='';
$nacionalidad_3='';
$tipo_vialidad_d_3='';
$calle_d_3='';
$no_ext_d_3='';
$no_int_d_3='';
$colonia_d_3='';
$municipio_d_3='';
$localidad_d_3='';
$estado_d_3='';
$cp_d_3='';
$pais_d_3='';
$telefono_d_3='';
$correo_d_3='';
$fecha_constitucion_d_3='';
$numero_acta_d_3='';
$nombre_notario_d_3='';
$numero_identificacion_g_3='';
$nombre_g_3='';
$apellido_paterno_g_3='';
$apellido_materno_g_3='';
$nombre_identificacion_g_3='';
$autoridad_emite_g_3='';
$genero_g_3=0;
$fecha_nacimiento_g_3='';
$r_f_c_g_3='';
$curp_g_3='';
$arraywhere_3 = array('idperfilamiento'=>$idperfilamiento); 
$tipo_cliente_p_m_m_e=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_m_m_e',$arraywhere_3);
foreach ($tipo_cliente_p_m_m_e as $item_3){
	$idtipo_cliente_p_m_m_e_3=$item_3->idtipo_cliente_p_m_m_e;
	$razon_social_3=$item_3->razon_social;
	$giro_mercantil_3=$item_3->giro_mercantil;
	$clave_registro_3=$item_3->clave_registro;
	$extranjero_3=$item_3->extranjero;
	$pais_3=$item_3->pais;
	$nacionalidad_3=$item_3->nacionalidad;
	$tipo_vialidad_d_3=$item_3->tipo_vialidad_d;
	$calle_d_3=$item_3->calle_d;
	$no_ext_d_3=$item_3->no_ext_d;
	$no_int_d_3=$item_3->no_int_d;
	$colonia_d_3=$item_3->colonia_d;
	$municipio_d_3=$item_3->municipio_d;
	$localidad_d_3=$item_3->localidad_d;
	$estado_d_3=$item_3->estado_d;
	$cp_d_3=$item_3->cp_d;
	$pais_d_3=$item_3->pais_d;
	$telefono_d_3=$item_3->telefono_d;
	$correo_d_3=$item_3->correo_d;
	$fecha_constitucion_d_3=$item_3->fecha_constitucion_d;
	$numero_acta_d_3=$item_3->numero_acta_d;
	$nombre_notario_d_3=$item_3->nombre_notario_d;
	$nombre_g_3=$item_3->nombre_g;
	$apellido_paterno_g_3=$item_3->apellido_paterno_g;
	$apellido_materno_g_3=$item_3->apellido_materno_g;
	$nombre_identificacion_g_3=$item_3->nombre_identificacion_g;
	$numero_identificacion_g_3=$item_3->numero_identificacion_g;
	$autoridad_emite_g_3=$item_3->autoridad_emite_g;
	$fecha_nacimiento_g_3=$item_3->fecha_nacimiento_g;
	$genero_g_3=$item_3->genero_g;
	$r_f_c_g_3=$item_3->r_f_c_g;
	$curp_g_3=$item_3->curp_g;
}
$checked_t_3='';
if($extranjero_3=='on'){
	$checked_t_3='checked';
}
$clave_t_3='';
$pais_t_3='';
$tipo_cliente_p_m_m_e=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_3);
foreach ($tipo_cliente_p_m_m_e as $item_t_3) {
	$clave_t_3=$item_t_3->clave;
	$pais_t_3=$item_t_3->pais;
}
$clave_t2_3='';
$pais_t2_3='';
$tipo_cliente_p_m_m_e2=$this->ModeloCatalogos->getselectwhere('pais','clave',$nacionalidad_3);
foreach ($tipo_cliente_p_m_m_e2 as $item_t2_3) {
	$clave_t2_3=$item_t2_3->clave;
	$pais_t2_3=$item_t2_3->pais;
}
$clave_d_t_3='';
$pais_d_t_3='';
$tipo_cliente_p_m_m_e_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_d_3);
foreach ($tipo_cliente_p_m_m_e_2 as $item_td2_3) {
	$clave_d_t_3=$item_td2_3->clave;
	$pais_d_t_3=$item_td2_3->pais;
}
$genero_t_3='';
if($genero_g_3==1){
	$genero_t_3='Masculino';
}else if($genero_g_3==2){
	$genero_t_3='Femenino';
}
$idcomplemento_persona_moral=0;
$empresa_persona_moral_hay_algun_apoderado_legal=0;
$empresa_persona_moral_hay_algun_apoderado_legal_accionista=0;
$empresa_persona_moral_existe_algun_precandidato=0;
$empresa_persona_moral_existe_algun_responsable_campana_politica=0;
$respondio_si_pregunta_anterior_persona_moral=0;


$complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_moral','idperfilamiento',$idperfilamiento);
foreach ($complento_pf1 as $cpm1) {
	$idcomplemento_persona_moral=$cpm1->id;
	$empresa_persona_moral_hay_algun_apoderado_legal=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal;
	$empresa_persona_moral_hay_algun_apoderado_legal_accionista=$cpm1->empresa_persona_moral_hay_algun_apoderado_legal_accionista;
	$empresa_persona_moral_existe_algun_precandidato=$cpm1->empresa_persona_moral_existe_algun_precandidato;
	$empresa_persona_moral_existe_algun_responsable_campana_politica=$cpm1->empresa_persona_moral_existe_algun_responsable_campana_politica;
	$respondio_si_pregunta_anterior_persona_moral=$cpm1->respondio_si_pregunta_anterior_persona_moral;

}

$empresa_persona_moral_hay_algun_apoderado_legal3x1='';
if($empresa_persona_moral_hay_algun_apoderado_legal==1){
	$empresa_persona_moral_hay_algun_apoderado_legal3x1='Si';
}else if($empresa_persona_moral_hay_algun_apoderado_legal==2){
	$empresa_persona_moral_hay_algun_apoderado_legal3x1='No';
}

$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='';
if($empresa_persona_moral_hay_algun_apoderado_legal_accionista==1){
	$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='Si';
}else if($empresa_persona_moral_hay_algun_apoderado_legal_accionista==2){
	$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1='No';
}

$empresa_persona_moral_existe_algun_precandidato3x1='';
if($empresa_persona_moral_existe_algun_precandidato==1){
	$empresa_persona_moral_existe_algun_precandidato3x1='Si';
}else if($empresa_persona_moral_existe_algun_precandidato==2){
	$empresa_persona_moral_existe_algun_precandidato3x1='No';
}

$empresa_persona_moral_existe_algun_responsable_campana_politica3x1='';
if($empresa_persona_moral_existe_algun_responsable_campana_politica==1){
	$empresa_persona_moral_existe_algun_responsable_campana_politica3x1='Si';
}else if($empresa_persona_moral_existe_algun_responsable_campana_politica==2){
	$empresa_persona_moral_existe_algun_responsable_campana_politica3x1='No';
}

$respondio_si_pregunta_anterior_persona_moral3x1='';
if($respondio_si_pregunta_anterior_persona_moral==1){
	$respondio_si_pregunta_anterior_persona_moral3x1='Si';
}else if($respondio_si_pregunta_anterior_persona_moral==2){
	$respondio_si_pregunta_anterior_persona_moral3x1='No';
}

//$actividad_econominica_morales=$this->ModeloCatalogos->getData('actividad_econominica_morales');
//$get_tipo_vialidad=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','activo',1);
//$estados=$this->ModeloCatalogos->getData('estado');

$actm=$this->ModeloCatalogos->getselectwherestatus('giro_mercantil','actividad_econominica_morales',array("clave"=>$giro_mercantil_3));
foreach ($actm as $item2) {
	$giro_mercantil_3=$item2->giro_mercantil;
}

$gettv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$tipo_vialidad_d_3));
foreach ($gettv as $item){
	$tipo_vialidad_d_3=$item->nombre; 
}

$gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$estado_d_3));
	foreach ($gete as $item){
        $estado_d_3=$item->estado; 
    }

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce a tu Cliente');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Persona moral mexicana y extranjera, persona moral de derecho público');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("25");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '25');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.=' <div class="row firma_tipo_cliente">
            <div class="col-md-12">    
	        	<h4>Formato: Conoce a tu cliente</h4>
	        	<h4>Fecha: '.$fecha_formato.'</h4>
	        	<hr class="subtitle barra_menu">

            	<h4>Persona moral mexicana y extranjera, persona moral de derecho público</h4>
           		<h4>Datos Generales</h4>';

            $html.='<table>
                      <div class="row">
                        <table width="100%">
						              <tr>
						                <td>
		                          <label>Razón social:</label>
		                          <span><strong> '.$razon_social_3.'</strong></span>
		                        </td>
		                        <td>
		                          <label>Giro mercantil / Actividad / Objeto social:</label>
		                          <span><strong> '.$giro_mercantil_3.'</strong></span>
		                        </td>
		                       </tr>
		                      </table>
		                      <table width="100%">
						              <tr>
						                <td><br></td>
						              </tr>
						            </table>
                        <div class="col-md-12 form-group">
                          <label><span>Clave del registro federal de contribuyentes con homoclave / número de identificacion fiscal en otro país (cuando aplique) y país que la asignó.</span></label>
                          <span><strong> '.$clave_registro_3.'</strong></span>
                        </div>';

 										if($checked_t_3=="checked"){
                        $html.='<div class="col-md-2">
	                            <label class="form-check-label">
	                              Es extranjero
	                            </label>
	                        </div>
                          <div class="col-md-12 form-group">
                            <label>País:</label>
                            <span><strong> '.$pais_t_3.'</strong></span>
                          </div>';
                    }else{
	                   	$html.=' 
	                        <div class="col-md-4 form-group pais_nacion3">
	                          <label>Nacionalidad:</label>
	                          <span><strong> '.$pais_t2_3.'</strong></span>
	                        </div>';
	                	} 
                    $html.='
                    	<div class="row">
                        <div class="col-md-12">
                          <hr class="subtitle">
                          <h4>Dirección</h4>
                        </div>

                      	<table width="100%">
						              <tr>
						                <td>
		                          <label>País:</label>
		                          <span><strong> '.$pais_t2_3.'</strong></span>
                        		</td>
		                        <td>
		                          <label>Tipo de vialidad:</label>
		                          <span><strong> '.$tipo_vialidad_d_3.'</strong></span>
		                        </td>
		                        <td>
		                          <label>Calle:</label>
		                          <span><strong> '.$calle_d_3.'</strong></span>
		                        </td>
		                        <td>
		                          <label>No.ext:</label>
		                          <span><strong> '.$no_ext_d_3.'</strong></span>
		                        </td>
		                      </tr>
                     		</table>
                     		<table width="100%">
						              <tr>
						                <td><br></td>
						              </tr>
						            </table>
                     		<table width="100%">
						              <tr>
		                        <td width="20%">
		                          <label>No.int:</label>
		                          <span><strong> '.$no_int_d_3.'</strong></span>
		                        </td>
		                        <td width="20%">
		                          <label>C.P:</label>
		                          <span><strong> '.$cp_d_3.'</strong></span>
		                        </td>
		                        <td width="30%">
		                          <label>Mcpio o delegación:</label>
		                          <span><strong> '.$municipio_d_3.'</strong></span>
		                        </td>
		                        <td width="30%">
		                          <label>Localidad:</label>
		                          <span><strong> '.$localidad_d_3.'</strong></span>
		                        </td>
		                      </tr>
                     		</table>
                     		<table width="100%">
						              <tr>
						                <td><br></td>
						              </tr>
						            </table>
                     		<table width="100%">
						              <tr>
		                        <td>
		                          <label>Estado:</label>
		                          <span><strong> '.$estado_d_3.'</strong></span>
		                        </td>
	                        	<td>
		                          <label>Colonia:</label>
		                          <span><strong> '.$colonia_d_3.'</strong></span>
	                        	</td>
		                        <td>
		                          <label>Número de teléfono:</label>
		                          <span><strong> '.$telefono_d_3.'</strong></span>
		                        </td>
		                      </tr>
                     		</table>
                     		<table width="100%">
						              <tr>
						                <td><br></td>
						              </tr>
						            </table>
                     		<table width="100%">
						              <tr>
		                        <td width="60%">
		                          <label>Correo electrónico:</label>
		                          <span><strong> '.$correo_d_3.'</strong></span>
		                        </td>
		                        <td width="40%">
		                          <label> Fecha de constitución:</label>
		                          <span><strong> '.$fecha_constitucion_d_3.'</strong></span>
		                        </td>
		                      </tr>
                     		</table>
                     		<table width="100%">
						              <tr>
						                <td><br></td>
						              </tr>
						            </table>
                     		<table width="100%">
						              <tr>
		                        <td width="40%">
		                          <label>Número de acta constitutiva:</label>
		                          <span><strong> '.$numero_acta_d_3.'</strong></span>
		                        </td>
		                        <td width="60%">
		                          <label>Nombre del notario:</label>
		                          <span><strong> '.$nombre_notario_d_3.'</strong></span>
		                        </td>
		                      </tr>
                     		</table>
	                    </div>
	                    
                      <div class="col-md-12">
                      	<hr class="subtitle">
                      	<h4>DATOS GENERALES DEL APODERADO / REPRESENTANTE LEGAL / SERVIDORES PÚBLICOS EN CASO DE PM DE DERECHO PÚBLICO.</h4>
                      </div>
                
                      <table width="100%">
					              <tr>
	                        <td>
		                        <label>Nombre(s):</label>
		                        <span><strong> '.$nombre_g_3.'</strong></span>
		                      </td>
		                      <td>
		                        <label>Apellido paterno:</label>
		                        <span><strong> '.$apellido_paterno_g_3.'</strong></span>
		                      </td>
		                      <td>
		                        <label>Apellido materno:</label>
		                        <span><strong> '.$apellido_materno_g_3.'</strong></span>
		                      </td>
		                    </tr>
		                  </table>
              				<table width="100%">
					              <tr>
					                <td><br></td>
					              </tr>
					            </table>
						          <table width="100%">
						            <tr>
		                      <td>
		                        <label>Nombre de identificación:</label>
		                        <span><strong> '.$nombre_identificacion_g_3.'</strong></span>
		                      </td>
		                      <td>
		                        <label>Autoridad que emite la identificación:</label>
		                        <span><strong> '.$autoridad_emite_g_3.'</strong></span>
		                      </td>
		                      
		                    </tr>
		                  </table>
		                  <table width="100%">
						              <tr>
						                <td><br></td>
						              </tr>
						            </table>
						          <table width="100%">
						            <tr>
						            	<td>
		                        <label>Número de identificación:</label>
		                        <span><strong> '.$numero_identificacion_g_3.'</strong></span>
		                      </td>
		                      <td>
		                        <label>Fecha de nacimiento:</label>
		                        <span><strong> '.$fecha_nacimiento_g_3.'</strong></span>
		                      </td>
		                      <td>
		                        <label>Género:</label>
		                        <span><strong> '.$genero_t_3.'</strong></span>
		                      </td>
		                    </tr>
		                  </table>
		                  <table width="100%">
						              <tr>
						                <td><br></td>
						              </tr>
						            </table>
						          <table width="100%">
						            <tr>
		                      <td>
		                        <label>R.F.C:</label>
		                        <span><strong> '.$r_f_c_g_3.'</strong></span>
		                      </td>
		                      <td>
		                        <label>CURP:</label>
		                        <span><strong> '.$curp_g_3.'</strong></span>
		                      </td>
		                    </tr>
		                  </table>
                      
                  	</div>
                	</table>  
                	<hr class="subtitle barra_menu">

                  	<table><br>';
	                    if($empresa_persona_moral_hay_algun_apoderado_legal3x1!=""){
		                    $html.='<div class="col-md-10">
		                        <label>¿En la empresa/persona moral hay algún apoderado legal, representante legal y/o accionista que se Persona Políticamente Expuesta?
		                        </label>
		                    </div>
		                    <div class="col-md-2 form-group">
		                      	<span><strong> '.$empresa_persona_moral_hay_algun_apoderado_legal3x1.'</strong></span>
		                    </div>';
		                  }
	                    if($empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1!=""){
		                    $html.='<div class="col-md-10">
		                        <label>¿En la empresa/persona moral hay algún apoderado legal, representante legal y/o accionista que tenga o que haya tenido un puesto político o público de alto nivel o está relacionada con alguna persona con estas características?
		                        </label>
		                    </div>
		                    <div class="col-md-2 form-group">
		                      	<span><strong> '.$empresa_persona_moral_hay_algun_apoderado_legal_accionista3x1.'</strong></span>
		                    </div>';
		                  }
	                   	if($empresa_persona_moral_existe_algun_precandidato3x1!=""){
		                    $html.='
			                    <div class="col-md-10">
		                        <label>¿En la empresa/persona moral existe algún precandidato(a), candidato(a), dirigente partidista, militante, dirigente sindical y/o servidor(a) público(a)?
		                        </label>
			                   	</div>
			                    <div class="col-md-2 form-group">
			                      	<span><strong> '.$empresa_persona_moral_existe_algun_precandidato3x1.'</strong></span>
			                    </div>';
			                 }

	                    if($empresa_persona_moral_existe_algun_responsable_campana_politica3x1!=""){
		                    $html.='<div class="col-md-10">
	                        <label>¿En la empresa/persona moral existe algún responsable de campaña política, encargado financiero y/ contable de algún partido político o de algún órgano interno de los partidos políticos y/o candidatos comunes y/o frentes y/o candidatos independientes?
	                        </label>
		                    </div>
		                    <div class="col-md-2 form-group">
		                      	<span><strong> '.$empresa_persona_moral_existe_algun_responsable_campana_politica3x1.'</strong></span>
		                    </div>';
		                  }
                  $html.='</table>
            <div class="firma_tipo_cliente">
	          <br><br><br><br><br><br><br>
	          <br><br><br><br>
	          <div class="firma_tipo_cliente">
	            <table width="100%">
	            	'.$label_a8.'
	              <tr>
	                <th width="48%">
	                <div class="col-md-6" style="text-align: justify !important;">
	                  <hr>
	                  <p style="text-align:center">'.$nombre_g_3.' '.$apellido_paterno_g_3.' '.$apellido_materno_g_3.'</p>
	                    Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
	                </div>
	                </th>
	                <th width="4%"> </th>
	                <th width="48%">
	                <div class="col-md-6" style="text-align: justify !important;">
	                  <hr>
	                  <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
	                    Declaro bajo protesta de decir verdad, que los datos asentados en la presente solicitud ha sido proporcionados directamente por el cliente.
	                </div> 
	                </th>
	              </tr>
	            </table>
	          </div>
	        </div>'; 


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('formatoConoceCliente.pdf', 'I');

?>