<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

//$GLOBALS['empleado'] = $empleado;
foreach ($empleado as $item) {
  $id = $item->id;
  $empleado = $item->nombre;
  $ciudad = $item->ciudad;
  $nom_ent = $item->nom_ent;
  $puesto = $item->puesto;
  $fecha = $item->fecha;
  $comentario = $item->comentario;
  $direccion = $item->direccion;
  $dia = date('d',strtotime($item->fecha));
  $mes = date('m',strtotime($item->fecha));
  $anio = date('Y',strtotime($item->fecha));
  if($mes==1){
      $mes_t = 'Enero';
  }else if($mes==2){
      $mes_t = 'Febrero';
  }else if($mes==3){
      $mes_t = 'Marzo';
  }else if($mes==4){
      $mes_t = 'Abril';
  }else if($mes==5){
      $mes_t = 'Mayo';
  }else if($mes==6){
      $mes_t = 'Junio';
  }else if($mes==7){
      $mes_t = 'Julio';
  }else if($mes==8){
      $mes_t = 'Agosto';
  }else if($mes==9){
      $mes_t = 'Septiembre';
  }else if($mes==10){
      $mes_t = 'Octubre';
  }else if($mes==11){
      $mes_t = 'Noviembre';
  }else if($mes==12){
      $mes_t = 'Diciembre';
  }
  $hora=$item->hora;
  $hora_e = date("g:i A", strtotime($item->hora));
  $encargado=$item->encargado;
  $puesto_encargado=$item->puesto_encargado;
  $delegado=$item->delegado;
  $puesto_delegado=$item->puesto_delegado;
  $coordinador=$item->coordinador;
  $puesto_coordinador=$item->puesto_coordinador;

}

//
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
    $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '10');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
// Logo
$html.='<img style="width: 150px" src="'.base_url().'public/img/cedis.jpg">';
// Texto
$html.='<br><div align="center" style="font-weight: bold; font-size:17px;">CONSTANCIA DE HECHOS<div>';
$html.='<div style="font-weight: bold; font-size:13px; text-align:justify;">En la Ciudad de <u>'.$ciudad.'</u>, <u>' .$dia.'</u> de mes de <u>'.$mes_t.'</u> de año <u>'.$anio.'</u> , el las oficinas de la empresa Cadena Comercial Oxxo, S.A de C.V . Ubicada en '.$direccion.', por una parte '.$encargado.' en su caracter de encargado de '.$encargado.' respectivamente en Oxxo Cedis '.$ciudad.' y por la otra, se levanta la presente Constancia de Hechos a fin de hacer constar el siguiente hecho en el cual participa el empleado <u>'.$empleado.'</u> con numero de trabajador <u>'.$id.'</u> quien se desempeño como <u>'.$puesto.'</u> y que consiste en lo siguiente:
        </div>';
  $html.='<div align="center" style="font-weight: bold; font-size:17px;">HECHOS<div>';
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;">'.$comentario.'</div>';
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;">En usu de la palabra el empleado <u>'.$empleado.'</u> comenta:</div>';
  $html.='<div align="center" style="font-weight: bold; font-size:17px;">ACCIONES Y COMPROMISOS<div>';
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;">Capitulo XI Medidas disciplinarias: el trabajador está obligado a seguir las medidas de seguridad establecidas por la empresa, para su protección y la de sus compañeros. En caso de incumplimiento se evaluará la gravedad de la fañta, reincidencia y antecedentes de cada empleado para determinar el tipo de sanción a aplicar, pudiendo ser desde una llamada de atención verbal hasta la rescisión de la relación laboral.</div>';
  $html.='<div style="font-size:14px; text-align:justify; font-weight: normal;"><u>>Apego al manejo seguro de Equipo móvil</u><br><u>>Respetar a los peatones</u><br><u>>Apegarse a los reentrenamientos por parte del entrenador practivo</u><br><u>>Se aplicará sanción de acuerdo a reglamento  interior de trabjo, medidas de seguridad</u></div><br><br><br>';
  $html.='<table width="100%">
            <tr><td style="font-weight: bold; font-size:13px; width: 50%;">'.$empleado.'<br>________________________________<br>'.$puesto.'</td><td style="font-weight: bold; font-size:13px; width: 50%;">'.$encargado.'<br>________________________________<br>'.$puesto_encargado.'</td></tr>
            <tr><td style="width: 100%;"></td></tr> 
            <tr><td style="width: 100%;"></td></tr> 
            <tr><td style="width: 100%;"></td></tr> 
            <tr><td style="font-weight: bold; font-size:13px; width: 50%;">'.$delegado.'<br>________________________________<br>'.$puesto_delegado.'</td><td style="font-weight: bold; font-size:13px; width: 50%;">'.$coordinador.'<br>________________________________<br>'.$puesto_coordinador.'</td></tr>
          </table>';
  $html.='<div style="text-align: left;">< style="font-weight: bold; font-size:15px;" span>NOTA:</span> <span style="font-size:14px; text-align:justify; font-weight: normal;">Si alguien se niega a firmar, debe asentarse tal hecho.</span><div>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Captura.pdf', 'I');

?>