<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

if (strtotime($fecha) !== false) {
	$fecha_formato = date("d-m-Y", strtotime($fecha));
} else {
	$fecha_formato = date("d-m-Y");
}

$idtipo_cliente_p_f_e_2=0;
$nombre_2='';
$apellido_paterno_2='';
$apellido_materno_2='';
$nombre_identificacion_2='';
$autoridad_emite_2='';
$numero_identificacion_2='';
$genero_2='';
$fecha_nacimiento_2='';
$pais_nacimiento_2='';
$pais_nacionalidad_2='';
$actividad_o_giro_2='';
$otra_ocupa="";
$tipo_vialidad_d_2='';
$calle_d_2='';
$no_ext_d_2='';
$no_int_d_2='';
$colonia_d_2='';
$municipio_d_2='';
$localidad_d_2='';
$estado_d_2='';
$cp_d_2='';
$pais_d_2='';
$trantadose_persona_2='';
$tipo_vialidad_t_2='';
$calle_t_2='';
$no_ext_t_2='';
$no_int_t_2='';
$colonia_t_2='';
$municipio_t_2='';
$localidad_t_2='';
$estado_t_2='';
$cp_t_2='';
$correo_t_2='';
$r_f_c_t_2='';
$curp_t_2='';
$telefono_t_2='';

$arraywhere_2 = array('idperfilamiento'=>$idperfilamiento); 
$tipo_cliente_p_f_e=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_p_f_e',$arraywhere_2);
foreach ($tipo_cliente_p_f_e as $item_2) {
  $idtipo_cliente_p_f_e_2=$item_2->idtipo_cliente_p_f_e;
  $nombre_2=$item_2->nombre;
  $apellido_paterno_2=$item_2->apellido_paterno;
  $apellido_materno_2=$item_2->apellido_materno;
  $nombre_identificacion_2=$item_2->nombre_identificacion;
  $autoridad_emite_2=$item_2->autoridad_emite;
  $numero_identificacion_2=$item_2->numero_identificacion;
  $genero_2=$item_2->genero;
  $fecha_nacimiento_2=$item_2->fecha_nacimiento;
  $pais_nacimiento_2=$item_2->pais_nacimiento;
  $pais_nacionalidad_2=$item_2->pais_nacionalidad;
  $actividad_o_giro_2=$item_2->actividad_o_giro;
  $otra_ocupa=$item_2->otra_ocupa;
  $tipo_vialidad_d_2=$item_2->tipo_vialidad_d;
  $calle_d_2=$item_2->calle_d;
  $no_ext_d_2=$item_2->no_ext_d;
  $no_int_d_2=$item_2->no_int_d;
  $colonia_d_2=$item_2->colonia_d;
  $municipio_d_2=$item_2->municipio_d ;
  $localidad_d_2=$item_2->localidad_d;
  $estado_d_2=$item_2->estado_d;
  $cp_d_2=$item_2->cp_d;
  $pais_d_2=$item_2->pais_d;
  $trantadose_persona_2=$item_2->trantadose_persona;
  $tipo_vialidad_t_2=$item_2->tipo_vialidad_t;
  $calle_t_2=$item_2->calle_t;
  $no_ext_t_2=$item_2->no_ext_t;
  $no_int_t_2=$item_2->no_int_t;
  $colonia_t_2=$item_2->colonia_t;
  $municipio_t_2=$item_2->municipio_t;
  $localidad_t_2=$item_2->localidad_t;
  $estado_t_2=$item_2->estado_t;
  $cp_t_2=$item_2->cp_t;
  $correo_t_2=$item_2->correo_t;
  $r_f_c_t_2=$item_2->r_f_c_t;
  $curp_t_2=$item_2->curp_t;
  $telefono_t_2=$item_2->telefono_t;
}
  $aux_genero_2_1='';
  if($genero_2==1){
    $aux_genero_2_1='Masculino';
  }else if($genero_2==2){
    $aux_genero_2_1='Femenino';  
  }
  $clave_2_p_n='';
  $pais_2_p_n='';
  $tipo_cliente_p_f_e_2_pais_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacimiento_2);
  foreach ($tipo_cliente_p_f_e_2_pais_1 as $item_2_p) {
    $clave_2_p_n=$item_2_p->clave;
    $pais_2_p_n=$item_2_p->pais;
  }
  $clave_2_p_n_2='';
  $pais_2_p_n_2='';
  $tipo_cliente_p_f_e_1_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_nacionalidad_2);
  foreach ($tipo_cliente_p_f_e_1_pais_2 as $item_1_p) {
    $clave_2_p_n_2=$item_1_p->clave;
    $pais_2_p_n_2=$item_1_p->pais;
  }
  $clave_2_p_1='';
  $pais_2_p_1='';
  $tipo_cliente_p_f_e_2_pais_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_d_2);
  foreach ($tipo_cliente_p_f_e_2_pais_2 as $item_2p) {
    $clave_2_p_1=$item_2p->clave;
    $pais_2_p_1=$item_2p->pais;
  }
  $checke_2_d='';
  if($trantadose_persona_2=='on'){
    $checke_2_d='checked';
  }else{
    $checke_2_d='';
  }
  
  $idcomplemento_persona_fisica=0;
  $actualmente_usted_precandidato1=0;
  $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1=0;
  $afirmativa_indicar_nombre_precandidato1='';
  $indicar_relacion1=0;
  $usted_socio_algun_precandidato1=0;
  $usted_asesor_agente_representante_algun_precandidato1=0;
  $servicio_contratar_producto_comprar_dirigido1=0;
  $tercera_persona_precandidato1=0;
  $nombre_persona_beneficiaria1='';
  $relacion_mantiene_persona_beneficiaria1='';
  $indicar_razon_motivo_realizar_tramit1='';
  $indicar_relacion_mantiene_persona_beneficiaria1='';
  $usted_responsable_campanna_politica1=0;
  
  $complento_pf1=$this->ModeloCatalogos->getselectwhere('complemento_persona_fisica','idperfilamiento',$idperfilamiento);
  foreach ($complento_pf1 as $cpf1) {
    $idcomplemento_persona_fisica=$cpf1->id;
    $actualmente_usted_precandidato1=$cpf1->actualmente_usted_precandidato;
    $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1=$cpf1->familiar_primer_segundo_tercer_grado_socio_algun_precandidato;
    $afirmativa_indicar_nombre_precandidato1=$cpf1->afirmativa_indicar_nombre_precandidato;
    $indicar_relacion1=$cpf1->indicar_relacion;
    $usted_socio_algun_precandidato1=$cpf1->usted_socio_algun_precandidato;
    $usted_asesor_agente_representante_algun_precandidato1=$cpf1->usted_asesor_agente_representante_algun_precandidato;
    $servicio_contratar_producto_comprar_dirigido1=$cpf1->servicio_contratar_producto_comprar_dirigido;
    $tercera_persona_precandidato1=$cpf1->tercera_persona_precandidato;
    $nombre_persona_beneficiaria1=$cpf1->nombre_persona_beneficiaria;
    $relacion_mantiene_persona_beneficiaria1=$cpf1->relacion_mantiene_persona_beneficiaria;
    $indicar_razon_motivo_realizar_tramit1=$cpf1->indicar_razon_motivo_realizar_tramit;
    $indicar_relacion_mantiene_persona_beneficiaria1=$cpf1->indicar_relacion_mantiene_persona_beneficiaria;
    $usted_responsable_campanna_politica1=$cpf1->usted_responsable_campanna_politica;
  }
  $actualmente_usted_precandidato1x1='';
  if($actualmente_usted_precandidato1==1){
    $actualmente_usted_precandidato1x1='Si';
  }else if($actualmente_usted_precandidato1==2){
    $actualmente_usted_precandidato1x1='No';
  }

  $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='';
  if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1==1){
    $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='Si';
  }else if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1==2){
    $familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1='No';
  }

  $indicar_relacion1x1='';
  if($indicar_relacion1==1){
    $indicar_relacion1x1='Esposo(a)';
  }else if($indicar_relacion1==2){
    $indicar_relacion1x1='Concubino(a) ';
  }else if($indicar_relacion1==3){
    $indicar_relacion1x1='Padre';
  }else if($indicar_relacion1==4){
    $indicar_relacion1x1='Madre';
  }else if($indicar_relacion1==5){
    $indicar_relacion1x1='Hermano(a)';
  }else if($indicar_relacion1==6){
    $indicar_relacion1x1='Hijo(a) ';
  }else if($indicar_relacion1==7){
    $indicar_relacion1x1='Hijastro(a)';
  }else if($indicar_relacion1==8){
    $indicar_relacion1x1='Padrastro';
  }else if($indicar_relacion1==9){
    $indicar_relacion1x1='Madrastra';
  }else if($indicar_relacion1==10){
    $indicar_relacion1x1='Abuelo(a)';
  }else if($indicar_relacion1==11){
    $indicar_relacion1x1='Tio(a) ';
  }else if($indicar_relacion1==12){
    $indicar_relacion1x1='Sobrino(a)';
  }else if($indicar_relacion1==13){
    $indicar_relacion1x1='Primo(a)'; 
  }else if($indicar_relacion1==14){
    $indicar_relacion1x1='Suegro(a) ';
  }else if($indicar_relacion1==15){
    $indicar_relacion1x1='Cuñado(a)';
  }

  $usted_socio_algun_precandidato1x1='';
  if($usted_socio_algun_precandidato1==1){
    $usted_socio_algun_precandidato1x1='Si';
  }else if($usted_socio_algun_precandidato1==2){
    $usted_socio_algun_precandidato1x1='No';
  }

  $usted_asesor_agente_representante_algun_precandidato1x1='';
  if($usted_asesor_agente_representante_algun_precandidato1==1){
    $usted_asesor_agente_representante_algun_precandidato1x1='Si';
  }else if($usted_asesor_agente_representante_algun_precandidato1==2){
    $usted_asesor_agente_representante_algun_precandidato1x1='No';
  }

  $servicio_contratar_producto_comprar_dirigido1x1='';
  if($servicio_contratar_producto_comprar_dirigido1==1){
    $servicio_contratar_producto_comprar_dirigido1x1='Si';
  }else if($servicio_contratar_producto_comprar_dirigido1==2){
    $servicio_contratar_producto_comprar_dirigido1x1='No';
  }

  $tercera_persona_precandidato1x1='';
  if($tercera_persona_precandidato1==1){
    $tercera_persona_precandidato1x1='Si';
  }else if($tercera_persona_precandidato1==2){
    $tercera_persona_precandidato1x1='No';
  }

  $usted_responsable_campanna_politica1x1='';
  if($usted_responsable_campanna_politica1==1){
    $usted_responsable_campanna_politica1x1='Si';
  }else if($usted_responsable_campanna_politica1==2){
    $usted_responsable_campanna_politica1x1='No';
  }


	$actf=$this->ModeloCatalogos->getselectwherestatus('acitividad','actividad_econominica_fisica',array("clave"=>$actividad_o_giro_2));
	foreach ($actf as $item) {
		$actividad_o_giro_2=$item->acitividad;
	}

	$gettv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$tipo_vialidad_d_2));
	foreach ($gettv as $item) {
		$tipo_vialidad_d_2=$item->nombre;
	}

	$gettv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$tipo_vialidad_t_2));
	foreach ($gettv as $item) {
		$tipo_vialidad_t_2=$item->nombre;
	}

	$gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$estado_t_2));
	foreach ($gete as $item){
        $estado_t_2=$item->estado; 
    }
    $gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$estado_d_2));
	foreach ($gete as $item){
        $estado_d_2=$item->estado; 
    }


class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce a tu Cliente');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Persona física extranjera con condición de estancia de visitante');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '15');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';

$html.='

        	<h4>Formato: Conoce a tu cliente</h4>
        	<h4>Fecha: '.$fecha_formato.'</h4>
        	<hr class="subtitle barra_menu">

        	<h4>Persona física extranjera con condición de estancia de visitante</h4>
       		<h4>Datos Generales</h4>
          <table width="100%">
            <tr>
              <td>
                <label>Nombre(s):</label>
                <span><strong> '.$nombre_2.'</strong></span>
              </td>
              <td>
                <label>Apellido paterno:</label>
                <span><strong> '.$apellido_paterno_2.'</strong></span>
              </td>
              <td>
                <label>Apellido materno:</label>
                <span><strong> '.$apellido_materno_2.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td>
                <label>Nombre de identificación:</label>
                <span><strong> '.$nombre_identificacion_2.'</strong></span>
              </td>
              <td>
                <label>Autoridad que emite la identificación:</label>
                <span><strong> '.$autoridad_emite_2.'</strong></span>
              </td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td width="30%">
                <label>Número de identificación:</label>
                <span><strong> '.$numero_identificacion_2.'</strong></span>
              </td>
              <td width="30%">
                <label>Género:</label>
                <span><strong> '.$aux_genero_2_1.'</strong></span>
              </td>
          	  <td width="40%">
              	<label>Fecha de nacimiento:</label>
              	<span><strong> '.$fecha_nacimiento_2.'</strong></span>
            	</td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>
          <table width="100%">
            <tr>
            	<td>
              	<label>País de nacimiento:</label>
              	<span><strong> '.$pais_2_p_n.'</strong></span>
            	</td>
            	<td>
              	<label>Pais de nacionalidad:</label>
              	<span><strong> '.$pais_2_p_n_2.'</strong></span>
            	</td>
            </tr>
          </table>
          <table width="100%">
            <tr>
              <td><br></td>
            </tr>
          </table>  
        	<div class="col-md-6 form-group">
          	<label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
          	<span><strong> '.$actividad_o_giro_2.'</strong></span>
        	</div>
        </div>
          <!--//////////////////////-->
          <div class="row">
          	<div class="col-md-12">
            	<hr class="subtitle">
            	<h4>Dirección</h4>
            </div>
            <table width="100%">
              <tr>
          	    <td width="25%">
                	<label>País:</label>
                	<span><strong> '.$pais_2_p_1.'</strong></span>
            	  </td> 
              	<td width="30%">
                	<label>Tipo de vialidad:</label>
                	<span><strong> '.$tipo_vialidad_d_2.'</strong></span>
              	</td>
             	  <td width="25%">
                	<label>Calle:</label>
                	<span><strong> '.$calle_d_2.'</strong></span>
              	</td>
              	<td width="20%">
                	<label>No.ext:</label>
                	<span><strong> '.$no_ext_d_2.'</strong></span>
              	</td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
              	<td>
                	<label>No.int:</label>
                	<span><strong> '.$no_int_d_2.'</strong></span>
              	</td>
              	<td>
                	<label>C.P:</label>
                	<span><strong> '.$cp_d_2.'</strong></span>
              	</td>
              	<td>
                	<label>Mcpio. o delegación:</label>
                	<span><strong> '.$municipio_d_2.'</strong></span>
              	</td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
              	<td>
                	<label>Localidad:</label>
                	<span><strong> '.$localidad_d_2.'</strong></span>
              	</td>
              	<td>
                	<label>Estado:</label>
                	<span><strong> '.$estado_d_2.'</strong></span>
              	</td>
              	<td>
                	<label>Colonia:</label>
                	<span><strong> '.$colonia_d_2.'</strong></span>
              	</td>
              </tr>
            </table>
          </div>';
          //log_message('error', 'checke_2_d: '.$checke_2_d);
          if($checke_2_d=="checked"){
            $html.='<div class="row">
                <hr>
              	<div class="col-md-12 form-group">
                	<div class="form-check form-check-primary">
                  		<label>Tratándose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio.
                  		</label>
                	</div>
             	</div>

              <div class="col-md-12">
                <h4>Dirección en México.</h4>
              </div>
              <table width="100%">
                <tr>
              	  <td width="30%">
                		<label>Tipo de vialidad:</label>
                		<span><strong> '.$tipo_vialidad_t_2.'</strong></span>
                	</td>
                  <td width="30%">
                  	<label>Calle:</label>
                  	<span><strong> '.$calle_t_2.'</strong></span>
                  </td>
                  <td width="20%">
                    <label>No.ext:</label>
                    <span><strong> '.$no_ext_t_2.'</strong></span>
                  </td>
                  <td width="20%">
                    <label>No.int:</label>
                    <span><strong> '.$no_int_t_2.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>C.P:</label>
                    <span><strong> '.$cp_t_2.'</strong></span>
                  </td>
                  <td>
                    <label>Municipio o delegación:</label>
                    <span><strong> '.$municipio_t_2.'</strong></span>
                  </td>
                  <td>
                    <label>Localidad:</label>
                    <span><strong> '.$localidad_t_2.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td>
                    <label>Estado:</label>
                    <span><strong> '.$estado_t_2.'</strong></span>
                  </td>
              	  <td>
                		<label>Colonia:</label>
                		<span><strong> '.$colonia_t_2.'</strong></span>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br><hr></td>
                </tr>
              </table>
            </div>';
      	  }  
            
          $html.='<div class="row">
            <table width="100%">
              <tr>
                <td>
                  <label>Correo electrónico:</label>
                  <span><strong> '.$correo_t_2.'</strong></span>
                </td>
                <td>
                  <label>R.F.C:</label>
                  <span><strong> '.$r_f_c_t_2.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>CURP:</label>
                  <span><strong> '.$curp_t_2.'</strong></span>
                </td>
                <td>
                  <label>Número de teléfono:</label>
                  <span><strong> '.$telefono_t_2.'</strong></span>
                </td>
              </tr>
            </table>
          </div>
        </div>
     
      
        <hr class="subtitle">
        <div class="ptext_beneficiario_2">
          <div class="text_beneficiario_2">';
          	$results=$this->ModeloCatalogos->getselectwhere('tipo_cliente_p_f_e_beneficiario','idtipo_cliente_p_f_e',$idtipo_cliente_p_f_e_2);
						$genero=""; $cont_be=0;
            foreach($results as $b){
              $cont_be++;
            }
            if($cont_be>0){
              $html.='<h4>Beneficiarios</h4>';
            }
						foreach($results as $b){
  						if($b->genero==1)
  						  $genero="Masculino";
  						else
  						  $genero="Femenino";

  						$tipov="";
  						$get_tv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$b->tipo_vialidad));
  						foreach ($get_tv as $item){
  						  $tipov = $item->nombre; 
  						}
  						$estadob="";
  						$gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$b->estado));
  						foreach ($gete as $item){
  				      $estadob=$item->estado; 
  				    }

						  $html.='<div class="row">
              <table width="100%">
                <tr>
    						  <td>
    						    <label>Nombre(s):</label>
    						    <span><strong> '.$b->nombre.'</strong></span>
    						  </td>
    						  <td>
    						    <label>Apellido paterno:</label>
    						    <span><strong> '.$b->apellido_paterno.'</strong></span>
    						  </td>
    						  <td>
    						    <label>Apellido materno:</label>
    						    <span><strong> '.$b->apellido_materno.'</strong></span>
    						  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
    						  <td width="40%">
    						    <label> Fecha de nacimiento:</label>
    						    <span><strong> '.$b->fecha_nacimiento.'</strong></span>
    						  </td>
    						  <td width="20%">
    						    <label>Género:</label>
    						    <span><strong> '.$genero.'</strong></span>
    						  </td>
    						  <td width="40%">
    						    <label>% Porcentaje de propiedad:</label>
    						    <span><strong> '.$b->porcentaje.'%</strong></span>
    						  </td>
                </tr>
              </table>

						  <div class="col-md-12 form-group"><h4>Dirección</h4></div>
              <table width="100%">
                <tr>
    						  <td>
    						    <label>Tipo de vialidad:</label>
    						    <span><strong> '.$tipov.'</strong></span>
    						  </td>
    						  <td>
    						    <label>Calle:</label>
    						    <span><strong> '.$b->calle.'</strong></span>
    						  </td>
    						  <td>
    						    <label>No.ext:</label>
    						    <span><strong> '.$b->no_ext.'</strong></span>
    						  </td>
    						  <td>
    						    <label>No.int:</label>
    						    <span><strong> '.$b->no_int.'</strong></span>
    						  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
    						  <td>
    						    <label>C.P:</label>
    						    <span><strong> '.$b->cp.'</strong></span>
    						  </td>
    						  <td>
    						    <label>Municipio o delegación:</label>
    						    <span><strong> '.$b->municipio.'</strong></span>
    						  </td>
    						  <td>
    						    <label>Localidad:</label>
    						    <span><strong> '.$b->localidad.'</strong></span>
    						  </td>
                </tr>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <table width="100%">
                <tr>
    						  <td>
    						    <label>Estado:</label>
    						    <span><strong> '.$estadob.'</strong></span>
    						  </td>
    						  <td>
    						    <label>Colonia:</label>
    						    <span><strong> '.$b->colonia.'</strong></span>
    						  </td>
                </tr>
              </table>
						  <div class="col-md-12 barra_menu"><hr class="subsubtitle"></div>';
						}
          $html.='</div>
              </div>';
            if($actualmente_usted_precandidato1x1!=""){
			        $html.='<div class="col-md-10">
			          <label>¿Actualmente es usted precandidato(a), candidato(a), dirigente partidista, servidor público (a)?        
			          </label>
			        </div>
			        <div class="col-md-2 form-group">
			          <span><strong> '.$actualmente_usted_precandidato1x1.'</strong></span>
			        </div>';
            }
			      if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1!=""){
              $html.='<div class="col-md-10">
			          <label>¿Es familiar en primer, segundo o tercer grado o socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                          
			          </label>
			        </div>
			        <div class="col-md-2 form-group">
			          <span><strong> '.$familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1.'</strong></span>
			        </div>';
            }
			       
			        if($familiar_primer_segundo_tercer_grado_socio_algun_precandidato1x1=="Si"){
			          $html.='<div class="col-md-12 form-group">
			            <label>Nombre del precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta:</label>
			            <span><strong> '.$afirmativa_indicar_nombre_precandidato1.'</strong></span>
			          </div>
			          
			          <div class="col-md-2 form-group">
			            <label>Relación:</label> 
			            <span><strong> '.$indicar_relacion1x1.'</strong></span>
			          </div>';
			        }

			       if($usted_socio_algun_precandidato1x1!=""){
              $html.='<div class="row" id="preg3">
			          <div class="col-md-10">
			            <label>¿Es usted socio de algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                          
			            </label>
			          </div>
			          <div class="col-md-2 form-group">
			            <span><strong> '.$usted_socio_algun_precandidato1x1.'</strong></span>
			          </div>
			        </div>';
            }
			     
            if($usted_asesor_agente_representante_algun_precandidato1x1!=""){
              $html.='<div class="col-md-10">
			          <label>¿Es usted asesor o agente o representante de  algún precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                       
			          </label>
			        </div>
			        <div class="col-md-2 form-group">
			          <span><strong> '.$usted_asesor_agente_representante_algun_precandidato1x1.'</strong></span>
			        </div>';
            }
			      if($usted_responsable_campanna_politica1x1!=""){
              $html.='<div class="col-md-10">
			          <label>¿Es usted responsable de campaña política, encargado financiero y/ contable de algún partido político o de algún órgano interno de los partidos políticos y/o candidatos comunes y/o frentes y/o candidatos independientes?      
			          </label>
			        </div>
			        <div class="col-md-2 form-group">
			          <span><strong> '.$usted_responsable_campanna_politica1x1.'</strong></span>
			        </div>';
            }
			      
			      if($servicio_contratar_producto_comprar_dirigido1x1!=""){
              $html.='<div class="col-md-10">
			          <label>¿El servicio a contratar o producto a comprar está dirigido o es para el beneficio de alguna tercera persona?                     
			          </label>
			        </div>
			        <div class="col-md-2 form-group">
			          <span><strong> '.$servicio_contratar_producto_comprar_dirigido1x1.'</strong></span>
			        </div>';
            }
			        
			        if($servicio_contratar_producto_comprar_dirigido1x1=="Si"){
			          $html.='
			              <div class="col-md-10">
			                <label>La tercera persona es precandidato(a), candidato(a), dirigente partidista, servidor público (a), persona políticamente expuesta?                   
			                </label>
			              </div>
			              <div class="col-md-2 form-group">
			                <span><strong> '.$tercera_persona_precandidato1x1.'</strong></span>
			              </div>';
			        }
			        if($tercera_persona_precandidato1x1=="Si"){
			          $html.='
			              <div class="col-md-12 form-group">
			                <label>Favor de indicar el nombre de la persona beneficiaria
			                </label>
			                <span><strong> '.$nombre_persona_beneficiaria1.'</strong></span>
			              </div>
			          
			              <div class="col-md-12 form-group">
			                <label>Indicar la relación que mantiene con la persona beneficiaria
			                </label>
			                <span><strong> '.$relacion_mantiene_persona_beneficiaria1.'</strong></span>
			              </div>
			   
			              <div class="col-md-12 form-group">
			                <label>Indicar por qué razón o motivo acude a realizar el trámite o compra para la tercera persona
			                </label>
			                <span><strong> '.$indicar_razon_motivo_realizar_tramit1.'</strong></span>
			            </div>';
			        }
        
      $html.='
	      	<div class="firma_tipo_cliente">
	          <br>
	          <br>
	          <br>
	          <div class="firma_tipo_cliente">
	            <table width="100%">
                '.$label_a8.'
	              <tr>
	                <th width="48%">
	                <div class="col-md-6" style="text-align: justify !important;">
	                  <hr>
	                  <p style="text-align:center">'.$nombre_2.' '.$apellido_paterno_2.' '.$apellido_materno_2.'</p>
	                    Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
	                </div>
	                </th>
	                <th width="4%"> </th>
	                <th width="48%">
	                <div class="col-md-6" style="text-align: justify !important;">
	                  <hr>
	                  <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
	                    Declaro bajo protesta de decir verdad, que los datos asentados en la presente solicitud ha sido proporcionados directamente por el cliente.
	                </div> 
	                </th>
	              </tr>
	            </table>
	          </div>
	        </div>';   

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('formatoConoceCliente.pdf', 'I');

?>