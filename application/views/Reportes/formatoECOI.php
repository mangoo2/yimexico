<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

if (strtotime($fecha) !== false) {
	$fecha_formato = date("d-m-Y", strtotime($fecha));
} else {
	$fecha_formato = date("d-m-Y");
}

	$idtipo_cliente_e_c_o_i_5=0;
  $denominacion_5='';
  $fecha_establecimiento_5='';
  $nacionalidad_5='';
  $clave_registro_5='';
  $pais_5='';
  $giro_mercantil_5='';
  $tipo_vialidad_d_5='';
  $calle_d_5='';
  $no_ext_d_5='';
  $no_int_d_5='';
  $colonia_d_5='';
  $municipio_d_5='';
  $localidad_d_5='';
  $estado_d_5='';
  $cp_d_5='';
  $pais_d_5='';
  $telefono_d_5='';
  $correo_d_5='';
  $certificado_matricula_5='';
  $nombre_g_5='';
  $apellido_paterno_g_5='';
  $apellido_materno_g_5='';
  $genero_g_5='';
  $fecha_nacimiento_g_5='';
  $nombre_identificacion_g_5='';
  $autoridad_emite_g_5='';
  $numero_identificacion_g_5='';
  $r_f_c_g_5='';
  $curp_g_5='';
  $arraywhere_5=array('idperfilamiento'=>$idperfilamiento); 
  $tipo_cliente_e_c_o_i=$this->ModeloCatalogos->getselectwherestatus('*','tipo_cliente_e_c_o_i',$arraywhere_5);
  foreach ($tipo_cliente_e_c_o_i as $item_5){
    $idtipo_cliente_e_c_o_i_5=$item_5->idtipo_cliente_e_c_o_i;
    $denominacion_5=$item_5->denominacion;
    $fecha_establecimiento_5=$item_5->fecha_establecimiento;
    $nacionalidad_5=$item_5->nacionalidad;
    $clave_registro_5=$item_5->clave_registro;
    $pais_5=$item_5->pais;
    $giro_mercantil_5=$item_5->giro_mercantil;
    $tipo_vialidad_d_5=$item_5->tipo_vialidad_d;
    $calle_d_5=$item_5->calle_d;
    $no_ext_d_5=$item_5->no_ext_d;
    $no_int_d_5=$item_5->no_int_d;
    $colonia_d_5=$item_5->colonia_d;
    $municipio_d_5=$item_5->municipio_d;
    $localidad_d_5=$item_5->localidad_d;
    $estado_d_5=$item_5->estado_d;
    $cp_d_5=$item_5->cp_d;
    $pais_d_5=$item_5->pais_d;
    $telefono_d_5=$item_5->telefono_d;
    $correo_d_5=$item_5->correo_d;
    $certificado_matricula_5=$item_5->certificado_matricula;
    $nombre_g_5=$item_5->nombre_g;
    $apellido_paterno_g_5=$item_5->apellido_paterno_g;
    $apellido_materno_g_5=$item_5->apellido_materno_g;
    $genero_g_5=$item_5->genero_g;
    $fecha_nacimiento_g_5=$item_5->fecha_nacimiento_g;
    $nombre_identificacion_g_5=$item_5->nombre_identificacion_g;
    $autoridad_emite_g_5=$item_5->autoridad_emite_g;
    $numero_identificacion_g_5=$item_5->numero_identificacion_g;
    $r_f_c_g_5=$item_5->r_f_c_g;
    $curp_g_5=$item_5->curp_g;
  }
  $clave_t_5='';
  $pais_t_5='';
  $tipo_cliente_e_c_o_i_1=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_5);
  foreach ($tipo_cliente_e_c_o_i_1 as $item_t_5) {
    $clave_t_5=$item_t_5->clave;
    $pais_t_5=$item_t_5->pais;
  }
  $clave_t5_5='';
  $pais_t5_5='';
  $tipo_cliente_e_c_o_i_2=$this->ModeloCatalogos->getselectwhere('pais','clave',$pais_d_5);
  foreach ($tipo_cliente_e_c_o_i_2 as $item_t5_5) {
    $clave_t5_5=$item_t5_5->clave;
    $pais_t5_5=$item_t5_5->pais;
  }
  $tipo_cliente_e_c_o_i_na=$this->ModeloCatalogos->getselectwhere('pais','clave',$nacionalidad_5);
    foreach ($tipo_cliente_e_c_o_i_na as $item_t_5) {
      $clave_t_5n=$item_t_5->clave;
      $pais_t_5n=$item_t_5->pais;
    }
  $actividad_econominica_morales=$this->ModeloCatalogos->getData('actividad_econominica_morales');  
  $genero_t_5='';
  if($genero_g_5==1){
    $genero_t_5='Masculino';
  }else if($genero_g_5==2){
    $genero_t_5='Femenino';
  }

	$gettv=$this->ModeloCatalogos->getselectwherestatus('nombre','tipo_vialidad',array("id"=>$tipo_vialidad_d_5));
	foreach ($gettv as $item){
		$tipo_vialidad_d_5=$item->nombre; 
	}

	$gete=$this->ModeloCatalogos->getselectwherestatus('estado','estado',array("id"=>$estado_d_5));
	foreach ($gete as $item){
    $estado_d_5=$item->estado; 
  }

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Conoce a tu Cliente');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Embajadas, consulados u organismos internacionales');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '15');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.=' <div class="row firma_tipo_cliente">
          <div class="col-md-12">    
	        	<h4>Formato: Conoce a tu cliente</h4>
	        	<h4>Fecha: '.$fecha_formato.'</h4>
	        	<hr class="subtitle barra_menu">
            <h4>Embajadas, consulados u organismos internacionales</h4>
           	<h4>Datos Generales</h4>';

          $html.='<table width="100%">
                    <tr>
                      <td width="40%">
                        <label>Denominación:</label>
                        <span><strong> '.$denominacion_5.'</strong></span>
                      </td>
                      <td width="60%">
                        <label><i class="fa fa-calendar"></i> Fecha de establecimiento en Territorio Nacional:</label>
                        <span><strong> '.$fecha_establecimiento_5.'</strong></span>
                      </td>
                    </tr>
                  </table>
                  <table width="100%">
                    <tr>
                      <td><br></td>
                    </tr>
                  </table>
                  <table width="100%">
                    <tr>
                      <td>
                        <label>Nacionalidad:</label>
                        <span><strong> '.$pais_t_5n.'</strong></span>
                      </td>
                    </tr>
                  </table>
                  <table width="100%">
                    <tr>
                      <td><br></td>
                    </tr>
                  </table>
                    <div class="col-md-7 form-group">
                      <label><span>Clave del Registro Federal de Contribuyentes con homoclave / número de identificación fiscal en otro país</span>:</label>
                      <span><strong> '.$clave_registro_5.'</strong></span>
                    </div>
                    <div class="col-md-5 form-group">
                      <label>País que asigno la clave  RFC o NIF:</label>
                      <span><strong> '.$pais_t_5.'</strong></span>
                    </div>
   
                  <div class="row">
                    <div class="col-md-12">
                      <hr class="subtitle">
                      <h4>Dirección</h4>
                    </div>
                    <table width="100%">
                      <tr>
                        <td width="20%">
                          <label>País:</label>
                          <span><strong> '.$pais_t5_5.'</strong></span>
                        </td>
                        <td width="30%">
                          <label>Tipo de vialidad:</label>
                          <span><strong> '.$tipo_vialidad_d_5.'</strong></span>
                        </td>
                        <td width="25%">
                          <label>Calle:</label>
                          <span><strong> '.$calle_d_5.'</strong></span>
                        </td>
                        <td width="25%">
                          <label>No.ext:</label>
                          <span><strong> '.$no_ext_d_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td width="15%">
                          <label>No.int:</label>
                          <span><strong> '.$no_int_d_5.'</strong></span>
                        </td>
                        <td width="15%">
                          <label>C.P:</label>
                          <span><strong> '.$cp_d_5.'</strong></span>
                        </td>
                        <td width="40%">
                          <label>Mcpio. o delegación:</label>
                          <span><strong> '.$municipio_d_5.'</strong></span>
                        </td>
                        <td width="30%">
                          <label>Localidad:</label>
                          <span><strong> '.$localidad_d_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>

                    <table width="100%">
                      <tr>
                        <td>
                          <label>Estado:</label>
                          <span><strong> '.$estado_d_5.'</strong></span>
                        </td>
                        <td>
                          <label>Colonia:</label>
                          <span><strong> '.$colonia_d_5.'</strong></span>
                        </td>
                        <td>
                          <label>Número de teléfono:</label>
                          <span><strong> '.$telefono_d_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td>
                          <label>Correo electrónico:</label>
                          <span><strong> '.$correo_d_5.'</strong></span>
                        </td>
                        <td>
                          <label>Certificado de Matrícula Consular:</label>
                          <span><strong> '.$certificado_matricula_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                  </div>  
                  <div class="row">
                    <div class="col-md-12">
                      <hr class="subtitle">
                      <h4>DATOS GENERALES DEL APODERADO / REPRESENTANTE LEGAL / SERVIDORES PÚBLICOS EN CASO DE PM DE DERECHO PÚBLICO</h4>
                    </div>
                    <table width="100%">
                      <tr>
                        <td>
                          <label>Nombre(s):</label>
                          <span><strong> '.$nombre_g_5.'</strong></span>
                        </td>
                        <td>
                          <label>Apellido paterno:</label>
                          <span><strong> '.$apellido_paterno_g_5.'</strong></span>
                        </td>
                        <td>
                          <label>Apellido materno:</label>
                          <span><strong> '.$apellido_materno_g_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td>
                          <label>Género:</label>
                          <span><strong> '.$genero_t_5.'</strong></span>
                        </td>
                        <td>
                          <label> Fecha de nacimiento:</label>
                          <span><strong> '.$fecha_nacimiento_g_5.'</strong></span>
                        </td>  
                        <td>
                          <label>Nombre de identificación:</label>
                          <span><strong> '.$nombre_identificacion_g_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td>
                          <label>Autoridad que emite la identificación:</label>
                          <span><strong> '.$autoridad_emite_g_5.'</strong></span>
                        </td>
                        <td>
                          <label>Número de identificación:</label>
                          <span><strong> '.$numero_identificacion_g_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td><br></td>
                      </tr>
                    </table> 
                    <table width="100%">
                      <tr>
                        <td>
                          <label>R.F.C:</label>
                          <span><strong> '.$r_f_c_g_5.'</strong></span>
                        </td>
                        <td>
                          <label>CURP:</label>
                          <span><strong> '.$curp_g_5.'</strong></span>
                        </td>
                      </tr>
                    </table>
                    <div class="col-md-2 form-group"></div>
                  </div>
                 
                  <div class="firma_tipo_cliente">
                    <div class="firma_tipo_cliente">
                      <table width="100%">
                        '.$label_a8.'
                        <tr>
                          <th width="48%">
                          <div class="col-md-6" style="text-align: justify !important;">
                            <hr>
                            <p style="text-align:center">'.$nombre_g_5.' '.$apellido_paterno_g_5.' '.$apellido_materno_g_5.'</p>
                              Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
                          </div>
                          </th>
                          <th width="4%"> </th>
                          <th width="48%">
                          <div class="col-md-6" style="text-align: justify !important;">
                            <hr>
                            <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
                              Declaro bajo protesta de decir verdad, que los datos asentados en la presente solicitud ha sido proporcionados directamente por el cliente.
                          </div> 
                          </th>
                        </tr>
                      </table>
                    </div>
                  </div>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('formatoConoceCliente.pdf', 'I');

?>