<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

$GLOBALS['nombreC'] = $nombreC;
$GLOBALS['fechaC'] = $fechaC;
$GLOBALS['folioC'] = $folioC;

class MYPDF extends TCPDF
{
  //Page header
  public function Header()
  {
    $backgroundImage = FCPATH . "public/pdf/img/resultadoS.png";

		$bMargin = $this->getBreakMargin();
		$auto_page_break = $this->AutoPageBreak;
		$this->SetAutoPageBreak(false, 0);
		$this->Image($backgroundImage, 0, 0, $this->getPageWidth(), $this->getPageHeight(), 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		$this->setPageMark();

		$this->SetY(14);
		$html = '
            <table width="100%" border="0" RULES="rows" cellpadding="1" style="padding: 2px; font-size: 12px; text-align: left;">
              <tr>
								<td width="6%">Nombre:</td>
                <td width="94%">'.$GLOBALS['nombreC'].'</td>
              </tr>
              <tr>
								<td width="10%">Fecha consulta:</td>
                <td width="90%">'.$GLOBALS['fechaC'].'</td>
              </tr>
              <tr>
								<td width="12%">Folio de búsqueda: </td>
                <td width="88%">'.$GLOBALS['folioC'].'</td>
              </tr>
            </table>';

		$this->writeHTML($html, true, false, true, false, '');
  }

  // Page footer
  public function Footer()
  {
    /*
    $html = '';
    $this->writeHTML($html, true, false, true, false, '');
    */
  }
}
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Formato');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('15');

// set auto page breaks
$pdf->SetAutoPageBreak(true, '12');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);

$height = $pdf->getPageHeight();
$width = $pdf->getPageWidth();

// add a page
$pdf->AddPage('L', 'LETTER');

//$pdf->Output(FCPATH . 'public/pdf/formatoBusquedaSE.pdf', 'FI');
$pdf->Output(FCPATH . 'public/pdf/formatoBusquedaSE.pdf', 'F');