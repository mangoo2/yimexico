<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

class MYPDF extends TCPDF {
  	//Page header
  	public function Header() {
      /// datos completos
      $html = '';      
      $this->writeHTML($html, true, false, true, false, '');
  	}

    // Page footer
  	public function Footer() {
    	$html = '';
    	$html .= '<table width="100%" border="0">
                  <tr>
                    <td width="85%"></td>
                    <td width="15%" align="right" class="footerpage"> '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
    	$this->writeHTML($html, true, false, true, false, '');
  	}
}

  $gene="";
  if($genero==1) $gene="Masculino";
  else $gene="Femenino";


$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Formato Cuestionario PEP ');
$pdf->SetSubject('Formato');
$pdf->SetKeywords('Personas Fisica Accionista');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('15', '15', '15');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '12');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8.5);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='
        <div class="col-md-12">    
          <h3>Formato PEP</h3>
          <h3>Fecha: '.date("m-d-Y").'</h3>
          <h3>Formulario para Persona Políticamente Expuesta y/o Persona Relacionada </h3>
        </div> 
        <hr class="subtitle">
        <table>
          <div class="row">
            <div class="col-md-12 form-group">
              <label>Nombre completo del PEP:</label>
              <span><strong> '.$nombre_completo.'</strong></span>
            </div>
          </div>';
            $html.='
         
              <div class="col-md-12 form-group">
                <label>¿Qué función desempeña en la empresa el funcionario público (PEP), precandidato(a), candidato(a), dirigente partidista, militante, dirigente sindical y/o servidor(a) público(a) ? (señalar las opciones que correspondan):</label>
              </div>
            ';
        
          $funcion_desempena1='';
          if($funcion_desempena==1){
            $funcion_desempena1='Accionista';
          }else if($funcion_desempena==2){
            $funcion_desempena1='Administrador Único';
          }else if($funcion_desempena==3){
            $funcion_desempena1='Directivo de algún área';
          }else if($funcion_desempena==4){
            $funcion_desempena1='Miembro del Consejo de Administración';
          }else if($funcion_desempena==5){
            $funcion_desempena1='Director General';
          }else if($funcion_desempena==6){
            $funcion_desempena1='Acreedor';
          }else if($funcion_desempena==7){
            $funcion_desempena1='Fundador';
          }else if($funcion_desempena==8){
            $funcion_desempena1='Beneficiario';
          }else if($funcion_desempena==9){
            $funcion_desempena1='Otra';
          }

            $html.='
              <div class="col-md-2 form-group">
                <span><strong> '.$funcion_desempena1.'</strong></span>
              </div>';
            if($funcion_desempena1=="Otra"){
              $html.='<div class="col-md-4 form-group">
                <label>Especificar:</label>
                <span><strong> '.$especificar.'</strong></span>
              </div>';
            }
          
          $html.='<hr class="subtitle barra_menu">
          <h3>Domicilio</h3>
          <div class="row">
            <table width="100%">
              <tr>
                <td>
                  <label>Calle:</label>
                  <span><strong> '.$calle.'</strong></span>
                </td>
                <td>
                  <label>Número Ext:</label>
                  <span><strong> '.$num_ext.'</strong></span>
                </td>
                <td>
                  <label>Número Int:</label>
                  <span><strong> '.$num_int.'</strong></span>
                </td>
                <td>
                  <label>Código Postal:</label>
                  <span><strong> '.$codigo_postal.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>Colonia:</label>
                  <span><strong> '.$colonia.'</strong></span>
                </td>
                <td>
                  <label>Delegación o Municipio:</label>
                  <span><strong> '.$delegacion_municipio.'</strong></span>
                </td>
                <td>
                  <label>Ciudad o Población:</label>
                  <span><strong> '.$ciudad_poblacion.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>Estado:</label>
                  <span><strong> '.$estado.'</strong></span>
                </td>
                <td>
                  <label>País:</label>
                  <span><strong> '.$pais.'</strong></span> 
                </td>
                <td>
                  <label>Lugar de Nacimiento:</label>
                  <span><strong> '.$lugar_nacimiento.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>Teléfonos:</label>
                  <span><strong> '.$telefono.'</strong></span>
                </td>
                <td>
                  <label>Fecha de Nacimiento:</label>
                  <span><strong> '.$fecha_nacimiento.'</strong></span>
                </td>
                <td>
                  <label>Nacionalidad:</label>
                  <span><strong> '.$nacionalidad.'</strong></span> 
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>País de Nacimiento:</label>
                  <span><strong> '.$pais_nacionalidad.'</strong></span>
                </td>
                <td>
                  <label>Género:</label>
                  <span><strong> '.$gene.'</strong></span>
                </td>
                <td>
                  <label>CURP:</label>
                  <span><strong> '.$curp.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>E-mail:</label>
                  <span><strong> '.$email.'</strong></span>
                </td>
                <td>
                  <label>RFC con Homoclave:</label>
                  <span><strong> '.$rfc_homeclave.'</strong></span>
                </td>
                <td>
                  <label>No de Serie FIEL:</label>
                  <span><strong> '.$num_serie_fiel.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <div class="col-md-12 form-group">
              <label>Tiempo de ser funcionario público (PEP) o tiempo de que el familiar ha sido funcionario público:</label>
              <span><strong> '.$tiempo_funcionario_pep.'</strong></span>
            </div>
            <div class="col-md-12 form-group">
              <label>Puestos o cargos ocupados por el funcionario público en el último año, empezando por el más reciente:</label>
              <span><strong> '.$puesto_cargo_ocupados_ultimo_anio.'</strong></span>
            </div>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>Nombre de esposa(o):</label>
                  <span><strong> '.$nombre_esposa.'</strong></span>
                </td>
                <td>
                  <label>RFC con Homoclave:</label>
                  <span><strong> '.$rfc_homeclave2.'</strong></span>
                </td>
                <td>
                  <label>CURP:</label>
                  <span><strong> '.$curp2.'</strong></span>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td><br></td>
              </tr>
            </table>
            <table width="100%">
              <tr>
                <td>
                  <label>Nacionalidad:</label>
                  <span><strong> '.$nacionalidad2.'</strong></span>
                </td>
                <td>
                  <label>No de hijos:</label>
                  <span><strong> '.$no_hijos.'</strong></span>
                </td>
              </tr>
            </table>
          </div>';

          //if(isset($hijos)){ 
            //foreach($hijos as $k){  //ACA ME QUEDO 
              $html.='
                <table width="100%">
                  <tr>
                    <td>
                      <label>Nombre del hijo:</label>
                      <span><strong> '.$nombre_hijos.'</strong></span>
                    </td>
                    <td>
                      <label>Edad del hijo:</label>
                      <span><strong> '.$edad_hijos.'</strong></span>
                    </td>
                    <td>
                      <label>Nacionalidad del hijo:</label>
                      <span><strong> '.$nacionalidad_hijos.'</strong></span>
                    </td>
                  </tr>
                </table>';
            //} 
          //}
            
          $html.='
        </table> 

          <div>
            <br><br><br><br>
            <div class="row">
              <table width="100%">
                <tr>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <p style="text-align:center">'.$nombre_completo.'</p>
                      <h3 style="color: black; text-align:center">Firma Cliente</h3>
                      Doy fé que los datos  proporcionados son veridicos y están vigentes
                  </div>
                  </th>
                  <th width="4%"> </th>
                  <th width="48%">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <p style="text-align:center">'.$this->session->userdata("nombre_user_log").'</p>
                      <h3 style="color: black; text-align:center">Firma Ejecutivo Comercial / Vendedor</h3>
                      Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial 
                  </div> 
                  </th>
                </tr>
              </table>
            </div> ';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Cuestionario PEP Moral.pdf', 'I');

?>