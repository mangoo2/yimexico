<style type="text/css">
    .col-yi{
        color: white;
    }
    .bg_yi{
        background-color: #ffffff;
        color: #212b4c;
        border-radius: 15px !important;
        width: 144px  !important;
    }
    .login-page {
        background-color: white !important;
    }
    .cx3 {
        background-color: rgb(33 43 76) !important;
    }
    .cx2 {
        background-color: rgb(176 126 73) !important
    }
    .cx1 {
        background-color: rgb(33 43 76)!important;
    }
    .card {
        background: #fff0 !important;
    }
    h2{
        color: white
    }
    h5{
        color: white
    }
    i{
        color: white
    }
    .input-group .input-group-addon .material-icons {
        color: white !important;
    }
    input{
        border-radius: 20px !important;
    }
    .form-group .form-line {
        border-bottom: 7px solid #ddd0 !important;
    }
    #txtUsuario{
      text-align: center;
    }
    #txtPass{
      text-align: center;
    }
    .card{
        box-shadow: 0 2px 10px rgb(0 0 0 / 0%) !important;
    }
</style>
<body class="login-page">
    <div class="cx3"></div>
    <div class="cx2"></div>
  <div class="cx1"></div>
    <div class="login-box">
        <div class="logo">
            <br>
            <!--<small></small>-->
        </div>
        <div class="card">
            <div class="body">
                <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly> 
                <form id="login-form" action="#" role="form">
                    <div class="row align-center">
                        <div class="col-md-6 " >
                           <img src="<?php echo base_url();?>public/img/LogoPlasta.png" width="100%" class="m-t-90"  />
                       </div>

                       <div class="col-md-6" style="border-left: 3px solid white;">
                        <i class="material-icons font-40 m-t-20 col-yi">lock</i>
                           <h2>Accede a tu cuenta</h2>
                            <h5>Estamos contentos de que regreses</h5>
                            <br>
                           <div class="form-group input-group">
                            <span class="input-group-addon">
                                <i class="material-icons" style="font-size: 35px;">person</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" id="txtUsuario" placeholder="Usuario">
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon">
                                <i class="material-icons" style="font-size: 35px;">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="txtPass" id="txtPass" placeholder="Contraseña" required>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-xs-12">
                                <button class="btn btn-block bg_yi waves-effect" type="submit" id="login-submit">INICIAR SESIÓN</button>
                            </div>
                        </div>

                        
                    </div>
                </div>
            </form>
            <div class="alert bg-light-green messagecorrecto" style="display: none">
                <i class="material-icons ">done</i> <strong>Acceso Correcto!</strong> Será redirigido al sistema 
            </div>

            <div class="alert bg-pink messageerror" style="display: none">
                <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
            </div>
        </div>
    </div>
    <div class="text-center" id="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>

</div>
