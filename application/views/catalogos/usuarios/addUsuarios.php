<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3 ><?php echo $titulo ?> de Sucursal</h3>
          <hr class="subtitle">
          <form class="form" method="post" role="form" id="form_sucursal">
          	<input type="hidden" name="idsucursal" value="<?php echo $idsucursal ?>">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label for="exampleInputUsername1">Nombre</label>
                  <input type="text" class="form-control"  name="sucursal" value="<?php echo $sucursal ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputUsername1">Teléfono:</label>
                  <input type="number" class="form-control" name="telefono" value="<?php echo $telefono ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputUsername1">Cuidad</label>
                  <input type="text" class="form-control"  name="ciudad" value="<?php echo $ciudad ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputUsername1">Estado:</label>
                  <select class="select2 w-100" name="estado" id="estado">
                    <?php foreach ($estados as $item) { ?>
                      <option value="<?php echo $item->cve_ent ?>" <?php if($estado == $item->cve_ent) echo 'selected' ?>><?php echo $item->nom_ent ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row"> 
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputUsername1">Correo electrónico:</label>
                  <input type="email" class="form-control" name="correo" value="<?php echo $correo ?>">
                </div>
              </div> 
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputUsername1">Dirección</label>
                  <textarea rows="2" class="form-control" name="direccion"><?php echo $direccion ?></textarea>
                </div>
              </div>
            </div>
          </form>
          <button type="button" class="btn btn-danger btn-rounded btn-fw guardarregistro"><?php echo $boton ?></button>
          <a class="btn btn-secondary btn-rounded btn-fw" href="<?php echo base_url(); ?>Sucursales">Regresar</a>
        </div>
      </div>
    </div>
</div>
