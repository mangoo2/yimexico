<div style="display: none">
<input type="text" name="usuario">
<input type="password" name="password">
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h3 class="">Sucursales</h3>
                  <hr class="subtitle">
                  <p class="card-description">
                    <a class="btn btn-danger btn-rounded btn-fw" href="<?php echo base_url(); ?>Sucursales/add">Agregar</a>
                  </p>
                  <div class="table-responsive">
                    <table class="table" id="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <td>Dirección</td>
                          <td>Telefono</td>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
</div>
<div class="modal fade" id="modaleliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Deseas eliminar la sucursal <span id="titulo_texto"></span>?</h5>
      </div>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <div class="form-group" align="center">
            <label for="exampleInputUsername1">Contraseña</label>
            <input id="password" type="password" class="form-control">
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>
      <input type="hidden" id="idsucursal">
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn btn-secondary btn-rounded btn-fw" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>