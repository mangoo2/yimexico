<div class="row">
  <div class="col-md-12 form-group">
    <label>Razón social:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>  
<div class="row">
  <div class="col-md-7 form-group">
    <label>Giro mercantil / Actividad / Objetivo social:</label>
    <select class="form-control">  
    </select>
  </div>
  <div class="col-md-2"><br>
    <div class="form-check form-check-flat form-check-primary">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="check_paso2_3" onchange="especifique_btn_hoja_paso2_3()">
        Otro, especifique:
      </label>
    </div>
  </div>
  <div class="col-md-3 form-group"><br>
    <input class="form-control" type="text" style="display: none" name="" id="especifique_paso2_3" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <span>Clave del registro federal de contribuyentes con homoclave / # identificacion fiscal en otro país (cuando aplique) y país que la asigno.</span>
  </div>
</div><br>
<div class="row">
  <div class="col-md-6 form-group">
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3">
    <div class="form-check form-check-flat form-check-primary">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="">
        Es extranjero
      </label>
    </div>
  </div>
  <div class="col-md-3 form-group">
    <label>Pais:</label><br>
    <select class="form-control idpais" name="" id="">
       <option value="MX">MEXICO</option>
    </select>
  </div> 
  <div class="col-md-3 form-group">
    <label>Nacionalidad:</label><br>
    <select class="form-control idpais" name="" id="">
       <option value="MX">MEXICO</option>
    </select>
  </div>  
</div>
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h4>Dirección</h4>
  </div>
</div>
<div class="row">  
  <div class="col-md-2 form-group">
    <label>Tipo de vialidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Calle:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.ext:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.int:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Colonia:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Municipio o delegación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Localidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Estado:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>C.P:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>País:</label><br>
    <select class="form-control idpais">
      <option value="MX">MEXICO</option>
    </select>
  </div>
</div>
<div class="row">
  <div class="col-md-3 form-group">
    <label>Número de teléfono:</label>
    <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="" value="">
  </div>
  <div class="col-md-9 form-group">
    <label>Correo electrónico:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-3 form-group">
    <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
    <input class="form-control" type="date" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Número de acta constitutiva:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-6 form-group">
    <label>Nombre del notario:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h4>DATOS GENERALES DEL APODERADO / REPRESENTANTE LEGAL / SERVIDORES PÚBLICOS EN CASO DE PM DE DERECHO PÚBLICO.</h4>
  </div>
</div>