<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h3>¿Cuenta con propietario/beneficiario real?</h3>
      	<div class="row">
      		<div class="col-md-12">
      			<hr class="subtitle">
      		</div>
      		<div class="col-md-12 form-group">
      			<label>Razón Social</label>
      			<input class="form-control" type="text" name="" value="">
      		</div>
      		<div class="col-md-12">
      			<hr class="subsubtitle">
      		</div>
      		<div class="col-md-6 form-group">
  	 				<label>Cuenta con propietario/beneficiario real</label>
            <div class="row">
             <div class="col-md-6 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                  Si
                </label>
              </div>
             </div>
             <div class="col-md-6 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                  No
                </label>
              </div>
             </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 form-group">
          <label>Cuenta con propietario/beneficiario real</label>
          <div class="row">
           <div class="col-md-12 form-group">
            <div class="form-check form-check-success">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                A) Nombrar a la(s) persona(s) que directa o indirectamente, posea el 25% o más de la compocisión accionaria o del capital social de la Persona Moral
              </label>
            </div>
           </div>
           <div class="col-md-12 form-group">
            <div class="form-check form-check-success">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                B) En caso de no tener accionistas que ostenten mas del 25% de la composicion accionaria de la persona moral en forma individual o conjunta, favor de enlistar a todos los accionistas personas fisicas o morales, siempre y cuando mantengan alguna relacion juridica
              </label>
            </div>
           </div>
           <div class="col-md-12 form-group">
            <div class="form-check form-check-success">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                No
              </label>
            </div>
           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
