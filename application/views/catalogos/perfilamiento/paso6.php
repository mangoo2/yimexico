<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
		  <div class="card-body">
		  	<div class="row">
		      <div class="col-md-12">  
		        <h4>Documentación de beneficiario real.</h4>	
		        <h3>Paso 6. Recopilación de documentos del beneficiario real</h3>
		      </div>
		    </div>  
		    <hr class="subtitle"> 
		    <!--- -->
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
						  	<div class="row">
					            <div class="col-md-12">
					                <label>Nombre:</label>
					            </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Apellido paterno:</label>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Apellido materno:</label>
				              </div>
				            </div><br>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Nombre de identificación:</label>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Autoridad que emite la identificación:</label>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Número de Identificación:</label>
				              </div>
				            </div>
				            <br>
				            <div class="row">
				              <div class="col-md-12">
				                <label>R.F.C:</label>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>CURP:</label>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <h6>Domicilio reportado:</h6>
				              </div>
				              <div class="col-md-6">
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Tipo de calle:</label>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Calle:</label>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>No. ext:</label>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>No. int:</label>
				                  </div>
				                </div>
				              </div>
				              <div class="col-md-6">
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Colonia:</label>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Municipio o delegación:</label>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Estado</label>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>C.P</label>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>País</label>
				                  </div>
				                </div>
				              </div>
				            </div>
	    
						</div>
					</div>	    
		        </div>	
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
					    <!-- -->
                           <div class="row">
				              <div class="col-md-12">
				                <h5>Documentos a recolectar</h5>
				              </div>
				            </div>
				            <div class="col-md-12">
				            	<span>*Por favor, recolecte una copia digital y coteje contra original los siguientes documentos.</span>
				            </div>
				            <div class="col-md-12 form-group" id="cont1">
				              <div class="form-check form-check-purple">
				                <label class="form-check-label">Formato "Consulta en listado de personas bloqueadas":
				                  <input type="checkbox" class="form-check-input" id="Ch1" value="">
				                </label>
				              </div>
				              <input type="file" id="Fl1" name="">
				            </div>
				            <div class="col-md-12 form-group" id="cont3">
				              <div class="form-check form-check-purple">
				                <label class="form-check-label">Identificación oficial:
				                  <input type="checkbox" class="form-check-input" id="Ch3" value="">
				                </label>
				              </div>
				              <input type="file"  id="Fl3" name="">
				            </div>
				            <div class="col-md-12 form-group" id="cont4">
				              <div class="form-check form-check-purple">
				                <label class="form-check-label">Cédula de identificación fiscal:
				                  <input type="checkbox" class="form-check-input" id="Ch4" value="">
				                </label>
				              </div>
				              <input type="file"  id="Fl4" name="">
				            </div>
				            <div class="col-md-12 form-group" id="cont9">
				              <div class="form-check form-check-purple">
				                <label class="form-check-label">CURP:
				                  <input type="checkbox" class="form-check-input" id="Ch9" value="">
				                </label>
				              </div>
				              <input type="file"  id="Fl9" name="">
				            </div> 
				            <div class="col-md-12 form-group" id="cont10">
				              <div class="form-check form-check-purple">
				                <label class="form-check-label">Comprobante de domicilio:
				                  <input type="checkbox" class="form-check-input" id="Ch10" value="">
				                </label>
				              </div>
				              <input type="file"  id="Fl10" name="">
				            </div>
                        <!-- -->
					    </div>
					</div>    	
	        	</div>
	        </div>
            <!-- -->
	    </div>
	</div>
</div>
