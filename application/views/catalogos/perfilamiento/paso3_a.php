<div class="row">
  <div class="col-md-12 form-group">
    <label>Razón social:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div> 
<hr class="subtitle"> 
<div class="row">
  <div class="col-md-12" align="center">
    <span>¿Tiene conocimiento de la existencia del dueño beneficiario</span>
  </div>
</div>
<div class="row">
	<div class="col-md-5 form-group"></div>
	<div class="col-md-4 form-group">
      <div class="row">
        <div class="col-md-6 form-group">
          <div class="form-check form-check-success">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Si
            </label>
          </div>
        </div>
        <div class="col-md-6 form-group">
          <div class="form-check form-check-success">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            No
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3 form-group"></div>
</div>
<div class="row">
  <div class="col-md-12" align="center">
    <span>¿Es persona fisica o moral?</span>
  </div>
</div>
<div class="row">
	<div class="col-md-4 form-group"></div>
	<div class="col-md-4 form-group">
      <div class="row">
        <div class="col-md-6 form-group">
          <div class="form-check form-check-success">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Persona fisica 
            </label>
          </div>
        </div>
        <div class="col-md-6 form-group">
          <div class="form-check form-check-success">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Persona moral
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 form-group"></div>
</div>
<div class="row">
  <div class="col-md-12">    
    <h3>IDENTIFICACIÓN DEL BENEFICIO CONTROLADOR</h3>
  </div>
</div> 
<div class="row">
  <div class="col-md-12 form-group">
    <div class="form-check form-check-primary">
      <label class="form-check-label">A) Nombrar a la(s) persona(s) fisica(s) que directa o indirectamente, posea el 25% o más de la composición accionaria o dl capital social de la persona moral.   
        <input type="checkbox" class="form-check-input" id="accionaria_socioa_a" onchange="accionaria_socioa_a_btn()">
      </label>
    </div>
  </div>
</div>
<div class="row accion_socio_a" style="display: none; border: 1px solid black; border-radius: 10px; box-shadow: 2px 2px 10px #666;">
	<div class="col-md-12">
		<!-- -->
		<div class="row">
			<div class="col-md-12">
				<h5>Socio 1</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<hr class="subtitle"> 
			</div>
		</div>
        <div class="row">
	        <div class="col-md-6 form-group">
	          <div class="form-check form-check-success">
	            <label class="form-check-label">
	              <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
	              Persona fisica
	            </label>
	          </div>
	          <div class="form-check form-check-success">
	            <label class="form-check-label">
	              <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
	              Persona moral
	            </label>
	          </div>
	        </div>
	        <div class="col-md-2 form-group"></div>
	        <div class="col-md-4 form-group">
	           <label>% de occiones:</label>
               <input class="form-control" type="text" name="" value="">
	        </div>
		</div>
        <div class="row">
          <div class="col-md-4 form-group">
            <label>Nombre(s):</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido paterno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido materno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
        </div>
        <div class="row">
	      <div class="col-md-4 form-group">
	        <label>Nombre de identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	      <div class="col-md-4 form-group">
	        <label>Autoridad que emite la identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	      <div class="col-md-4 form-group">
	        <label>Número de identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	    </div>
        <div class="row">
        	<div class="col-md-4">
		        <label>Género:</label>
		        <div class="row">
		          <div class="col-md-6 form-group">
		            <div class="form-check form-check-success">
		              <label class="form-check-label">
		                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
		                Masculino
		              </label>
		            </div>
		          </div>
		          <div class="col-md-6 form-group">
		            <div class="form-check form-check-success">
		              <label class="form-check-label">
		                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
		                Femenino
		              </label>
		            </div>
		          </div>
		        </div>
		    </div>    
        </div>
        <div class="row">
		  <div class="col-md-4 form-group">
		    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
		    <input class="form-control" type="date" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País de nacimiento:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Pais de nacionalidad:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
        <div class="row">
		  <div class="col-md-12">
		    <h4>Dirección</h4>
		  </div>
		</div>
		<div class="row">
		  <div class="col-md-2 form-group">
		    <label>Tipo de vialidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Calle:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.ext:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.int:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Colonia:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Municipio o delegación:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Localidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Estado:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>C.P:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
        <div class="row">
		  <div class="col-md-12 form-group">
		    <div class="form-check form-check-primary">
		      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
		        <input type="checkbox" class="form-check-input" id="lugar_recidencia_socio" onchange="lugar_recidencia_btn_socio()">
		      </label>
		    </div>
		  </div>
		</div>
        <div class="residencia_extranjero" style="display: none">
		  <div class="row">
		    <div class="col-md-12">
		      <span>Dirección en México.</span>
		    </div>
		  </div><br>
		  <div class="row">
		    <div class="col-md-2 form-group">
		      <label>Tipo de vialidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Calle:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.ext:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.int:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Colonia:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Municipio o delegación:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Localidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Estado:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>C.P:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-6 form-group">
		      <label>Correo electrónico:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>R.F.C:</label>
		      <input class="form-control" type="text" name="" value=""><span>*Cuando cuente con R.F.C</span>
		    </div>
		    <div class="col-md-3 form-group">
		      <label>CURP:</label>
		      <input class="form-control" type="text" name="" value="">
		      <span>*Cuando cuente con CURP</span>
		    </div>
		  </div>
		</div>
		<!-- -->
	</div>
</div>
<br>	
<div class="row accion_socio_a" style="display: none; border: 1px solid black; border-radius: 10px; box-shadow: 2px 2px 10px #666;">
	<div class="col-md-12">
		<!-- -->
		<div class="row">
			<div class="col-md-12">
				<h5>Socio 2</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<hr class="subtitle"> 
			</div>
		</div>
        <div class="row">
	        <div class="col-md-6 form-group">
	          <div class="form-check form-check-success">
	            <label class="form-check-label">
	              <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
	              Persona fisica
	            </label>
	          </div>
	          <div class="form-check form-check-success">
	            <label class="form-check-label">
	              <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
	              Persona moral
	            </label>
	          </div>
	        </div>
	        <div class="col-md-2 form-group"></div>
	        <div class="col-md-4 form-group">
	           <label>% de occiones:</label>
               <input class="form-control" type="text" name="" value="">
	        </div>
		</div>
		<div class="row">
		  <div class="col-md-12 form-group">
		    <label>Razón social:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		</div> 
		<div class="row">
		  <div class="col-md-12">
		    <span>Clave del registro federal de contribuyentes con homoclave / # identificacion fiscal en otro país (cuando aplique) y país que la asigno.</span>
		  </div>
		</div><br>
		<div class="row">
		  <div class="col-md-6 form-group">
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3">
		    <div class="form-check form-check-flat form-check-primary">
		      <label class="form-check-label">
		        <input type="checkbox" class="form-check-input" id="">
		        Es extranjero
		      </label>
		    </div>
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Pais:</label><br>
		    <select class="form-control idpais" name="" id="">
		       <option value="MX">MEXICO</option>
		    </select>
		  </div> 
		  <div class="col-md-3 form-group">
		    <label>Nacionalidad:</label><br>
		    <select class="form-control idpais" name="" id="">
		       <option value="MX">MEXICO</option>
		    </select>
		  </div>  
		</div>
        <div class="row">
		  <div class="col-md-12">
		    <h4>Dirección</h4>
		  </div>
		</div>
		<div class="row">
		  <div class="col-md-2 form-group">
		    <label>Tipo de vialidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Calle:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.ext:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.int:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Colonia:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Municipio o delegación:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Localidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Estado:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>C.P:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
        <div class="row">
          <div class="col-md-3 form-group">
		    <label>Número de teléfono:</label>
		    <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="" value="">
		  </div>
		  <div class="col-md-5 form-group">
		    <label>Correo electrónico:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>	
        </div>
        <div class="row">
		  <div class="col-md-3 form-group">
		    <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
		    <input class="form-control" type="date" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Número de acta constitutiva:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-6 form-group">
		    <label>Nombre del notario:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		</div>
        <hr class="subtitle"> 
        <div class="row">
        	<div class="col-md-12" align="center">
        		<span>Definir estructura accionaria</span>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-8">
        	  <p>Socio 1:</p>	
        	</div>
            <div class="col-md-4">
              <label>% de occiones:</label>
              <input class="form-control" type="text" name="" value="">
            </div>	
        </div>
        <div class="row">
          <div class="col-md-4 form-group">
            <label>Nombre(s):</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido paterno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido materno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
        </div>
        <div class="row">
	      <div class="col-md-4 form-group">
	        <label>Nombre de identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	      <div class="col-md-4 form-group">
	        <label>Autoridad que emite la identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	      <div class="col-md-4 form-group">
	        <label>Número de identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	    </div> 
        <div class="row">
        	<div class="col-md-4">
		        <label>Género:</label>
		        <div class="row">
		          <div class="col-md-6 form-group">
		            <div class="form-check form-check-success">
		              <label class="form-check-label">
		                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
		                Masculino
		              </label>
		            </div>
		          </div>
		          <div class="col-md-6 form-group">
		            <div class="form-check form-check-success">
		              <label class="form-check-label">
		                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
		                Femenino
		              </label>
		            </div>
		          </div>
		        </div>
		    </div>    
        </div>
        <div class="row">
		  <div class="col-md-4 form-group">
		    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
		    <input class="form-control" type="date" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País de nacimiento:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Pais de nacionalidad:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
        <div class="row">
		  <div class="col-md-12">
		    <h4>Dirección</h4>
		  </div>
		</div>
		<div class="row">
		  <div class="col-md-2 form-group">
		    <label>Tipo de vialidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Calle:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.ext:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.int:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Colonia:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Municipio o delegación:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Localidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Estado:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>C.P:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
        <div class="row">
		  <div class="col-md-12 form-group">
		    <div class="form-check form-check-primary">
		      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
		        <input type="checkbox" class="form-check-input" id="lugar_recidencia_socio_2_a" onchange="lugar_recidencia_btn_socio_2_accion_a()">
		      </label>
		    </div>
		  </div>
		</div>
        <div class="residencia_extranjero_socio2_accion_a" style="display: none">
		  <div class="row">
		    <div class="col-md-12">
		      <span>Dirección en México.</span>
		    </div>
		  </div><br>
		  <div class="row">
		    <div class="col-md-2 form-group">
		      <label>Tipo de vialidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Calle:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.ext:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.int:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Colonia:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Municipio o delegación:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Localidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Estado:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>C.P:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-6 form-group">
		      <label>Correo electrónico:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>R.F.C:</label>
		      <input class="form-control" type="text" name="" value=""><span>*Cuando cuente con R.F.C</span>
		    </div>
		    <div class="col-md-3 form-group">
		      <label>CURP:</label>
		      <input class="form-control" type="text" name="" value="">
		      <span>*Cuando cuente con CURP</span>
		    </div>
		  </div>
		</div>

		<div class="row">
	        <div class="col-md-6 form-group">
	          <p>Socio 2:</p>
	        </div>
	        <div class="col-md-2 form-group"></div>
	        <div class="col-md-4 form-group">
	           <label>% de occiones:</label>
               <input class="form-control" type="text" name="" value="">
	        </div>
		</div>
		<div class="row">
          <div class="col-md-4 form-group">
            <label>Nombre(s):</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido paterno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido materno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
        </div>
        <div class="row">
	      <div class="col-md-4 form-group">
	        <label>Nombre de identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	      <div class="col-md-4 form-group">
	        <label>Autoridad que emite la identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	      <div class="col-md-4 form-group">
	        <label>Número de identificación:</label>
	        <input class="form-control" type="text" name="" value="">
	      </div>
	    </div>
        <div class="row">
        	<div class="col-md-4">
		        <label>Género:</label>
		        <div class="row">
		          <div class="col-md-6 form-group">
		            <div class="form-check form-check-success">
		              <label class="form-check-label">
		                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
		                Masculino
		              </label>
		            </div>
		          </div>
		          <div class="col-md-6 form-group">
		            <div class="form-check form-check-success">
		              <label class="form-check-label">
		                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
		                Femenino
		              </label>
		            </div>
		          </div>
		        </div>
		    </div>    
        </div>
        <div class="row">
		  <div class="col-md-4 form-group">
		    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
		    <input class="form-control" type="date" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País de nacimiento:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Pais de nacionalidad:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
        <div class="row">
		  <div class="col-md-12">
		    <h4>Dirección</h4>
		  </div>
		</div>
		<div class="row">
		  <div class="col-md-2 form-group">
		    <label>Tipo de vialidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Calle:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.ext:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.int:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Colonia:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Municipio o delegación:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Localidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Estado:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>C.P:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
        <div class="row">
		  <div class="col-md-12 form-group">
		    <div class="form-check form-check-primary">
		      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
		        <input type="checkbox" class="form-check-input" id="lugar_recidencia_socio2_accion_a" onchange="lugar_recidencia_btn_socio_accion_a_socio2()">
		      </label>
		    </div>
		  </div>
		</div>
        <div class="residencia_extranjero_accion_a_socio2" style="display: none">
		  <div class="row">
		    <div class="col-md-12">
		      <span>Dirección en México.</span>
		    </div>
		  </div><br>
		  <div class="row">
		    <div class="col-md-2 form-group">
		      <label>Tipo de vialidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Calle:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.ext:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.int:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Colonia:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Municipio o delegación:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Localidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Estado:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>C.P:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-6 form-group">
		      <label>Correo electrónico:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>R.F.C:</label>
		      <input class="form-control" type="text" name="" value=""><span>*Cuando cuente con R.F.C</span>
		    </div>
		    <div class="col-md-3 form-group">
		      <label>CURP:</label>
		      <input class="form-control" type="text" name="" value="">
		      <span>*Cuando cuente con CURP</span>
		    </div>
		  </div>
		</div>
		<!-- -->
	</div>
</div>	
