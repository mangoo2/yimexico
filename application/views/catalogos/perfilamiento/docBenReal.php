<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h3>Recopilacion de Documentos</h3>
        <div id="contenedor" class="row">
          <div class="col-md-8">
            <div id="Persona-Moral">
              <div class="row">
                <div class="col-md-3">
                  <label id="Change1">Razón Social</label>
                </div>
                <div class="col-md-9">
                  <label id="nombre"></label>
                </div>
              </div>
            </div>
            <div id="Persona-Fisica">
            <div class="row">
              <div class="col-md-3">
                <label>Nombre</label>
              </div>
              <div class="col-md-9">
                <label id="nombre"></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label>Apellido Paterno</label>
              </div>
              <div class="col-md-9">
                <label id="ApellidoP"></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label>Apellido Materno</label>
              </div>
              <div class="col-md-9">
                <label id="ApellidoM"></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label>Nombre de Identificación:</label>
              </div>
              <div class="col-md-9">
                <label id="identificacion"></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label>Autoridad que Emite la Identificación</label>
              </div>
              <div class="col-md-9">
                <label id="autoridad"></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label>Numero de Identificación</label>
              </div>
              <div class="col-md-9">
                <label id="Nidentificacion"></label>
              </div>
            </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label>RFC</label>
              </div>
              <div class="col-md-9">
                <label id="rfc"></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label>CURP</label>
              </div>
              <div class="col-md-9">
                <label id="curp"></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h6>Domicilio Reportado</h6>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-3">
                    <label>Tipo de calle</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label>Calle</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label>No. Ext</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label>No. int</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-3">
                    <label>Colonia</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>Municipio o delegación</label>
                  </div>
                  <div class="col-md-6">
                    <label></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label>Estado</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label>C.P</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label>País</label>
                  </div>
                  <div class="col-md-9">
                    <label></label>
                  </div>
                </div>
              </div>
            </div>
            <div class="container contenedor">
              <div class="row">
                <div class="col-md-12">
                  <br>
                </div>
                <div class="col-md-3">
                  <label>Nombre</label>
                </div>
                <div class="col-md-9">
                  <label id="nombre"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label>Apellido Paterno</label>
                </div>
                <div class="col-md-9">
                  <label id="ApellidoP"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label>Apellido Materno</label>
                </div>
                <div class="col-md-9">
                  <label id="ApellidoM"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label>Nombre de Identificación:</label>
                </div>
                <div class="col-md-9">
                  <label id="identificacion"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label>Autoridad que Emite la Identificación</label>
                </div>
                <div class="col-md-9">
                  <label id="autoridad"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label>Numero de Identificación</label>
                </div>
                <div class="col-md-9">
                  <label id="Nidentificacion"></label>
                </div>
              </div>

              <div class="row">
                <div class="col-md-3">
                  <label>RFC</label>
                </div>
                <div class="col-md-9">
                  <label id="rfc"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label>CURP</label>
                </div>
                <div class="col-md-9">
                  <label id="curp"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h6>Domicilio Reportado</h6>
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-3">
                      <label>Tipo de calle</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label>Calle</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label>No. Ext</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label>No. int</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-3">
                      <label>Colonia</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <label>Municipio o delegación</label>
                    </div>
                    <div class="col-md-6">
                      <label></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label>Estado</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label>C.P</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label>País</label>
                    </div>
                    <div class="col-md-9">
                      <label></label>
                    </div>
                    <div class="col-md-12">
                      <br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
           <form>
            <div class="row">
              <div class="col-md-12">
                <h6>Documentos a recolectar</h6>
                <label>*Por favor, recolecte una copia dijital y coteje con original los siguientes documentos:</label>
              </div>
            </div>
            <div class="col-md-12 form-group" id="cont1">
              <div class="form-check form-check-danger">
                <label class="form-check-label">Identificación Oficial
                  <input type="checkbox" class="form-check-input" id="Ch1" value="">
                </label>
              </div>
              <input class="form-control full" type="file" id="Fl1" name="">
            </div>
            <div class="col-md-12 form-group" id="cont2">
              <div class="form-check form-check-danger">
                <label class="form-check-label">Cedula de identificación fiscal
                  <input type="checkbox" class="form-check-input" id="Ch2" value="">
                </label>
              </div>
              <input class="form-control full" type="file"  id="Fl2" name="">
            </div>
            <div class="col-md-12 form-group" id="cont3">
              <div class="form-check form-check-danger">
                <label class="form-check-label">CURP
                  <input type="checkbox" class="form-check-input" id="Ch3" value="">
                </label>
              </div>
              <input class="form-control full" type="file"  id="Fl3" name="">
            </div>
            <div class="col-md-12 form-group" id="cont4">
              <div class="form-check form-check-danger">
                <label class="form-check-label">Comprobante de domicilio
                  <input type="checkbox" class="form-check-input" id="Ch4" value="">
                </label>
              </div>
              <input class="form-control full" type="file"  id="Fl4" name="">
            </div>
            <div class="col-md-12 form-group" id="cont6">
              <div class="form-check form-check-danger">
                <label class="form-check-label">Carta poder
                  <input type="checkbox" class="form-check-input" id="Ch5" value="">
                </label>
              </div>
              <input class="form-control full" type="file"  id="Fl5" name="">
            </div>
            <div class="col-md-12 form-group" id="cont7">
              <div class="form-check form-check-danger">
                <label class="form-check-label">Identificación oficial de apoderado legal
                  <input type="checkbox" class="form-check-input" id="Ch6" value="">
                </label>
              </div>
              <input class="form-control full" type="file"  id="Fl6" name="">
            </div>
            <div class="col-md-12 form-group" id="cont8">
              <div class="form-check form-check-danger">
                <label class="form-check-label">Comprobante de domicilio de apoderado legal
                  <input type="checkbox" class="form-check-input" id="Ch7" value="">
                </label>
              </div>
              <input class="form-control full" type="file"  id="Fl7" name="">
            </div>
           </form>
          </div>
        </div>
    	</div>
    </div>
  </div>
</div>