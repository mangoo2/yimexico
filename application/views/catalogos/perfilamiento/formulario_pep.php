<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css"><style type="text/css">
  .check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c; -webkit-print-color-adjust: exact;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
      background: #212b4c; -webkit-print-color-adjust: exact;
  }
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
      border-color: #212b4c; -webkit-print-color-adjust: exact;
  }
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
      background: #25294c; -webkit-print-color-adjust: exact;
  }
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
      border-color: #25294c; -webkit-print-color-adjust: exact;
  }
</style>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 >Formulario PEP / Persona relacionada
          <a data-toggle="modal" data-target="#modal_ayuda"><button type="button" class="btn btn-warning btn-rounded btn-icon" >
            <i class="mdi mdi-help"></i>
          </button></a> 
        </h3>
        <hr class="subtitle">
        <!---------------->
        <form class="form" method="post" role="form" id="form_formulario_pep">
          <input type="hidden" name="idperfilamiento" id="idperfilamiento" value="<?php echo $idperfilamiento ?>">
          <input type="hidden" name="idtipo_cliente" id="idtipo_cliente" value="<?php echo $idtipo_cliente ?>">
          <input type="hidden" name="id" id="id" value="<?php echo $idpep ?>">
          <div class="row">
            <div class="col-md-12 form-group">
              <label>Nombre completo:</label>
              <input class="form-control" type="text" readonly value="<?php echo $nombre_1.' '.$apellido_paterno_1.' '.$apellido_materno_1 ?>">
            </div>
          </div>
          <div class="row" style="display: none">
            <div class="col-md-12 form-group">
              <label>¿Qué función desempeña en la empresa el funcionario público (PEP), precandidato(a), candidato(a), dirigente partidista, militante, dirigente sindical y/o servidor(a) público(a) ? (señalar las opciones que correspondan):</label>
            </div>
          </div>
          <?php 
            //echo "hay_acc_pep: ".$hay_acc_pep;
            $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='';
            $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='';
            if($funcion_desempena==1){
              $funcion_desempena1='checked';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='';
              $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='';
            }else if($funcion_desempena==2){
              $funcion_desempena1='';$funcion_desempena2='checked';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='';
              $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='';
            }else if($funcion_desempena==3){
              $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='checked';$funcion_desempena4='';$funcion_desempena5='';
              $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='';
            }else if($funcion_desempena==4){
              $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='checked';$funcion_desempena5='';
              $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='';
            }else if($funcion_desempena==5){
              $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='checked';
              $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='';
            }else if($funcion_desempena==6){
              $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='';
              $funcion_desempena6='checked';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='';
            }else if($funcion_desempena==7){
              $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='';
              $funcion_desempena6='';$funcion_desempena7='checked';$funcion_desempena8='';$funcion_desempena9='';
            }else if($funcion_desempena==8){
              $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='';
              $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='checked';$funcion_desempena9='';
            }else if($funcion_desempena==9){
              $funcion_desempena1='';$funcion_desempena2='';$funcion_desempena3='';$funcion_desempena4='';$funcion_desempena5='';
              $funcion_desempena6='';$funcion_desempena7='';$funcion_desempena8='';$funcion_desempena9='checked';
            }
          ?>
          <div class="row" style="display: none">  
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="1" <?php echo $funcion_desempena1 ?>>
                  Accionista
                </label>
              </div>
            </div>
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="2" <?php echo $funcion_desempena2 ?>>
                  Administrador Único
                </label>
              </div>
            </div>
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="3" <?php echo $funcion_desempena3 ?>>
                  Directivo de algún área
                </label>
              </div>
            </div>
          </div>
          <div class="row" style="display: none">  
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="4" <?php echo $funcion_desempena4 ?>> 
                  Miembro del Consejo de Administración
                </label>
              </div>
            </div>
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="5" <?php echo $funcion_desempena5 ?>>
                  Director General
                </label>
              </div>
            </div>
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="6" <?php echo $funcion_desempena6 ?>>
                  Acreedor
                </label>
              </div>
            </div>
          </div>
          <div class="row" style="display: none">  
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="7" <?php echo $funcion_desempena7 ?>>
                  Fundador
                </label>
              </div>
            </div>
            <div class="col-md-3 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="8" <?php echo $funcion_desempena8 ?>>
                  Beneficiario
                </label>
              </div>
            </div>
            <div class="col-md-2 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="funcion_desempena" onclick="btn_existe_otro_accionista()" value="9" <?php echo $funcion_desempena9 ?>>
                  Otra (especificar)
                </label>
              </div>
            </div>
            <div class="col-md-4 form-group">
              <div class="text_espicificar" style="display: none">
                <label>Especificar:</label>
                <input class="form-control" type="text" name="especificar" value="<?php echo $especificar ?>">
              </div>
            </div>
          </div>
          <hr class="subtitle barra_menu">
          <h3>Domicilio</h3>
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Calle:</label>
              <input class="form-control" type="text" readonly value="<?php echo $calle_d_1 ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>Número Ext:</label>
              <input class="form-control" type="text" readonly value="<?php echo $no_ext_d_1 ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>Número Int:</label>
              <input class="form-control" type="text" readonly value="<?php echo $no_int_d_1 ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>Código Postal:</label>
              <input class="form-control" type="text" readonly value="<?php echo $cp_d_1 ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Colonia:</label>
              <input class="form-control" type="text" readonly value="<?php echo $colonia_d_1 ?>">
            </div>
            
          </div>

          <div class="row">
            <div class="col-md-3 form-group">
              <label>Delegación o Municipio:</label>
              <input class="form-control" type="text" readonly value="<?php echo $municipio_d_1 ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>Ciudad o Población:</label>
              <input class="form-control" type="text" readonly value="<?php echo $estado_d_1 ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>Estado:</label>
              <input class="form-control" type="text" readonly value="<?php echo $estado_d_1 ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>País:</label>
              <input class="form-control" type="text" readonly value="<?php echo $pais_1_p_1 ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Lugar de Nacimiento:</label>
              <input class="form-control" type="text" readonly value="<?php echo $pais_1_p_n ?>">
            </div>
          </div>

          <div class="row">
            <div class="col-md-3 form-group">
              <label>Teléfonos:</label>
              <input class="form-control" type="text" readonly value="<?php echo $telefono_t_1 ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Fecha de Nacimiento:</label>
              <input class="form-control" type="date" readonly value="<?php echo $fecha_nacimiento_1 ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Nacionalidad:</label>
              <input class="form-control" type="text" readonly value="<?php echo $pais_1_p_n_2 ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>País de Nacimiento:</label>
              <input class="form-control" type="text" readonly value="<?php echo $pais_1_p_n_2 ?>">
            </div>
          </div>

          <div class="row">
            <div class="col-md-3 form-group">
              <label>Género:</label>
              <?php if($genero_1==1){
                    $genero_1tx='Masculino';
                  }else{
                    $genero_1tx='Femenino';
                  }
              ?>
              <input class="form-control" type="text" readonly value="<?php echo $genero_1tx ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>CURP:</label>
              <input class="form-control" type="text" readonly value="<?php echo $curp_t_1 ?>">
            </div>
            <div class="col-md-6 form-group">
              <label>E-mail:</label>
              <input class="form-control" type="email" readonly value="<?php echo $correo_t_1 ?>">
            </div>
          </div>

          <div class="row">
            <div class="col-md-4 form-group">
              <label>RFC con Homoclave:</label>
              <input class="form-control" type="text" readonly name="rfc_homeclave" value="<?php echo $r_f_c_t_1 ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Número de Serie FIEL:</label>
              <input class="form-control" type="text" name="num_serie_fiel" value="<?php echo $num_serie_fiel ?>">
            </div>
            <div class="col-md-4 form-group" style="display: none">
              <label>% de acciones de la persona moral:</label>
              <input class="form-control" type="text" name="acciones_personal_moral" value="<?php echo $acciones_personal_moral ?>">
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12 form-group">
              <label>Tiempo de ser funcionario público (PEP) o tiempo de que el familiar ha sido funcionario público:</label>
              <input class="form-control" type="text" name="tiempo_funcionario_pep" value="<?php echo $tiempo_funcionario_pep ?>">
            </div>
          </div>  

          <div class="row">
            <div class="col-md-12 form-group">
              <label>Puestos o cargos ocupados por el funcionario público en el último año, empezando por el más reciente:</label>
              <input class="form-control" type="text" name="puesto_cargo_ocupados_ultimo_anio" value="<?php echo $puesto_cargo_ocupados_ultimo_anio ?>">
            </div>
          </div>  
          <div class="row">
            <div class="col-md-12 form-group">
              <div class="form-check form-check-primary">
                <label class="form-check-label"> Si tengo conyugue
                  <input type="checkbox" class="form-check-input" name="si_esposa" id="si_esposa" onchange="presenta_esposa()" <?php if($si_esposa==1) echo "checked"; ?>>
                </label>
              </div>
            </div>
          </div> 
          <div id="cont_conyugue" style="display: none">
            <div class="row">
              <div class="col-md-12 form-group">
                <label>Nombre de conyugue:</label>
                <input class="form-control" type="text" name="nombre_esposa" value="<?php echo $nombre_esposa ?>">
              </div>
            </div>  
            <div class="row">
              <div class="col-md-3 form-group">
                <label>RFC con Homoclave:</label>
                <input class="form-control" type="text" name="rfc_homeclave2" value="<?php echo $rfc_homeclave2 ?>">
              </div>
              <div class="col-md-3 form-group">
                <label>CURP:</label>
                <input class="form-control" type="text" name="curp2" value="<?php echo $curp2 ?>">
              </div>
              <div class="col-md-6 form-group">
                <label>Nacionalidad:</label><br>
                <!--<input class="form-control" type="text" name="nacionalidad2" value="<?php echo $nacionalidad2 ?>">-->
                <select class="form-control pais_1" name="nacionalidad2" id="pais_4">
                  <?php if(isset($nacionalidad2) && $nacionalidad2!=""){
                      foreach ($get_pais as $p){
                        $select="";
                        if($nacionalidad2==$p->clave){ $select="selected"; }
                          echo "<option value='$p->clave' $select>$p->pais</option>";
                      }
                    }else{
                      echo "<option value='MX'>MEXICO</option>";
                    }
                  ?>
                </select>  
              </div>
            </div>

            

            <!--<div class="row">
              <div class="col-md-4 form-group">
                <label>Edad de los hijos:</label>
                <input class="form-control" type="text" name="edad_hijos" value="<?php echo $edad_hijos ?>">
              </div>
              <div class="col-md-8 form-group">
                <label>Nacionalidad de los hijos:</label>
                <input class="form-control" type="text" name="nacionalidad_hijos" value="<?php echo $nacionalidad_hijos ?>">
              </div>
            </div>-->
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>No de hijos:</label>
              <input class="form-control" type="number" name="no_hijos" id="no_hijos" value="<?php echo $no_hijos ?>">
            </div>
            <!--<div class="col-md-8 form-group">
              <label>Nombre de los hijos:</label>
              <input class="form-control" type="text" name="nombre_hijos" value="<?php echo $nombre_hijos ?>">
            </div>-->
          </div>
          <div class="row_hijos">
            <div class="datos_hijos">
              <?php if(isset($hijos)){ ?>
                <?php foreach($hijos as $k){ ?>
                  <div class="row" id="row_<?php echo $k->id; ?>">
                    <input class="form-control" type="hidden" id="id" value="<?php echo $k->id; ?>">
                    <div class="col-md-4 form-group">
                      <label>Nombre del hijo:</label>
                      <input class="form-control" type="text" id="nombre" value="<?php echo $k->nombre; ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Edad del hijo:</label>
                      <input class="form-control" type="text" id="edad" value="<?php echo $k->edad; ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Nacionalidad del hijo:</label>
                      <input class="form-control" type="text" id="nacionalidad" value="<?php echo $k->nacionalidad; ?>">
                    </div>
                 
                    <div class="col-md-1 form-group">
                      <label style="color:transparent">eliminar</label>
                      <button type="button" class="btn btn_borrar" onclick="eliminaHijoId(<?php echo $k->id; ?>)"><i class="fa fa-minus"></i> </button>
                    </div>
                  </div>
              <?php  } ?>
            <?php } ?>
            
            </div>
          </div>
        </form> 
        <div class="texto_firma" style="display: none">
          <div class="row">
            <div class="col-md-6" align="center">
              <b style="color: black; -webkit-print-color-adjust: exact;">_______________________________________</b>
              <h3 style="color: black;">Nombre y firma del cliente</h3>
              
            </div>
            <div class="col-md-6" align="center">
              <b style="color: black; -webkit-print-color-adjust: exact;">________________________________________</b>
              <h3 style="color: black;">Nombre y firma del vendedor / ejecutivo</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6" align="center">
              <h6 style="color: black;">Doy fé que los datos  proporcionados son veridicos y están vigentes</h6>
              
            </div>
            <div class="col-md-6" align="center">
              <h6 style="color: black;">Doy fé que el cliente respondió directa y personalmente las preguntas del formulario</h6>
            </div>
          </div>
          <br>
        </div> 
        <div class="btn_guardar_text">
          <div class="row">
            <div class="col-md-12" align="right">
              <?php if($idpep>0){ ?>
                <button type="button" class="btn gradient_nepal2" onclick="imprimir_for()"><i class="fa fa-print"></i> Imprimir formato</button>
              <?php } ?>
              <!--<a href="javascript:history.back()" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</a>-->
              <button type="button" id="regresar" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</button>
              <button type="button" class="btn gradient_nepal2" onclick="guardar_formulario_pep()"><i class="fa  fa-floppy-o" ></i> Guardar</button>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </div>
</div>  

<div class="modal fade" id="modalimprimir" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" align="center">
            <label>¿Deseas imprimir el formulario?                
            </label>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4 form-group">
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="1">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="2" checked>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_registro" onclick="imprimir_formato1()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_accionista" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
            <label>¿Existe algún otro accionista, apoderado legal o representante legal que sea PEP?
            </label>
          </div>
          <?php
            $accionistax1='';
            $accionistax2='';
            if($accionista==1){
              $accionistax1='checked';
              $accionistax2='';
            }else if($accionista==0){
              $accionistax1='';
              $accionistax2='checked';
            }
          ?>
          
          <div class="col-md-4 form-group">
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="existe_otro_accionista" value="1" <?php echo $accionistax1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="existe_otro_accionista" value="2" <?php echo $accionistax2 ?>>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_registro" onclick="guardar_formulario_pep2()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5></h5>
       <span style="text-align : justify; font-size: 12px">
          <li>Persona Políticamente Expuesta (PEP) -  individuo que desempeña o ha desempeñado funciones públicas destacadas en un país extranjero o en territorio nacional.</li>
          <li>Persona Relacionada: El cónyuge, los ascendientes, descendientes y colaterales por consanguinidad o afinidad hasta el segundo grado inclusive, socios, las sociedades en que éstos participen y empleados.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>