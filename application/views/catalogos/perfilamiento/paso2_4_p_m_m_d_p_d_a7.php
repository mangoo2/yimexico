<div class="row">
  <div class="col-md-8 form-group">
    <label>Nombre de la persona moral de derecho público:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>R.F.C:</label>
    <select class="form-control">  
    </select>
  </div>
</div>  
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h4>Dirección</h4>
  </div>
</div>
<div class="row">  
  <div class="col-md-2 form-group">
    <label>Tipo de vialidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Calle:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.ext:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.int:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Colonia:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Municipio o delegación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Localidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Estado:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>C.P:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
    <input class="form-control" type="date" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h6>DATOS GENERALES DE LOS SERVIDORES PÚBLICOS QUE REALICEN EL ACTO O LA OPERACIÓN</h6>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
  </div>
</div>
<!-- -->
<br>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Nombre(s):</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido paterno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido materno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Género:</label>
    <div class="row">
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Masculino
          </label>
        </div>
      </div>
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Femenino
          </label>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 form-group">
    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
    <input class="form-control" type="date" name="" value="">
  </div>  
</div>