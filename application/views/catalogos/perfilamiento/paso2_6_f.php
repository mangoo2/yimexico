<div class="row">
  <div class="col-md-12 form-group">
    <label>Denominación o razón social del fiduciario:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-12 form-group">
    <label>Número / Referencia o identicador del fideicomiso:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-12 form-group">
    <label>Clave del registro federal de contribuyentes con homoclave del fideicomiso:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h6>DATOS GENERALES DEL APODERADO LEGAL O DELEGADO FIDUCIARIO</h6>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
  </div>
</div>
<!-- -->
<br>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Nombre(s):</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido paterno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido materno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Nombre de identificación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Autoridad que emite la identificación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Número de identificación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Género:</label>
    <div class="row">
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Masculino
          </label>
        </div>
      </div>
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Femenino
          </label>
        </div>
      </div>
    </div>
  </div>  
</div>
<div class="row">
  <div class="col-md-2 form-group"></div>
  <div class="col-md-4 form-group">
    <label>R.F.C:</label>
    <input class="form-control" type="text" name="" value=""><span>*Cuando cuente con R.F.C</span>
  </div>
  <div class="col-md-4 form-group">
    <label>CURP:</label>
    <input class="form-control" type="text" name="" value="">
    <span>*Cuando cuente con CURP</span>
  </div>
  <div class="col-md-2 form-group"></div>
</div>