<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h3>Perfilamiento del cliente</h3>
      	 <div class="row">
      	 	<div class="col-md-3">
      	 		<label>Tipo Persona</label>
      	 	</div>
      	 	<div class="col-md-9">
      	 		<label id="tipoPer"></label>
      	 	</div>
      	 	<div class="col-md-3">
      	 		<label>Nombre</label>
      	 	</div>
      	 	<div class="col-md-9">
      	 		<label id="nombre"></label>
      	 	</div>
      	 	<div class="col-md-3">
      	 		<label>Fecha de nacimiento</label>
      	 	</div>
      	 	<div class="col-md-9">
      	 		<label id="fechaN"></label>
      	 	</div>
      	 	<div class="col-md-3">
      	 		<label>RFC</label>
      	 	</div>
      	 	<div class="col-md-9">
      	 		<label id="rfc"></label>
      	 	</div>
      	 	<div class="col-md-3">
      	 		<label>Domicilio reportado</label>
      	 	</div>
      	 	<div class="col-md-9">
      	 		<label id="domRep"></label>
      	 	</div>
      	 </div>
      	</div>
      </div>
    </div>
  </div>