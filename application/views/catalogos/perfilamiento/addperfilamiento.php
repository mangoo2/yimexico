
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<style type="text/css">
#resultado {
    color: red;
    font-weight: bold;
}
#resultado.ok {
    color: green; 
}
#colonia_d1{
  text-transform: uppercase;
}
#tipo_vialidad1{
  text-transform: uppercase;
}
.check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c; -webkit-print-color-adjust: exact;
}
.form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c; -webkit-print-color-adjust: exact;
}
.form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #212b4c; -webkit-print-color-adjust: exact;
}
.form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c; -webkit-print-color-adjust: exact;
}
.form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c; -webkit-print-color-adjust: exact;
}
</style>
<span class="perfil_v" hidden><?php echo $perfil_v ?></span>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card" >
    <div class="card">
      <div class="card-body">
        <div class="barra_menu"> 
          <div class="row">
            <div class="col-md-8">
              <h3>Paso 1. Perfilamiento del cliente</h3>
            </div>
            <div class="col-md-4" align="right">
              <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
            </div>         
          </div>
          <hr class="subtitle">
          <input type="hidden" name="idopera" id="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" id="desde_opera" value="<?php echo $desdeopera ?>">
          <form class="form" method="post" role="form" id="form_perfilamiento">
            <?php if(isset($idperfilamiento)) {?>
              <input type="hidden" name="idperfilamiento" id="idperfilamiento" value="<?php echo $idperfilamiento ?>">
            <?php }else{ ?>
              <input type="hidden" name="idperfilamiento" id="idperfilamiento">
            <?php } ?>
            <div class="row">
              <div class="col-md-10 form-group">
                <label>Identifique el tipo de cliente:</label>
                <?php /*
                <!--<select name="idtipo_cliente" id="idtipo_cliente" class="form-control" onchange="tipo_cliente_select()">
                    <option selected="" disabled="" value="0">Seleccione el tipo de cliente</option>
                    <?php foreach ($get_tipo_cliente as $item){ ?>
                       <option value="<?php echo $item->idtipo_cliente ?>"><?php echo $item->tipo ?></option>
                    <?php } ?>
                </select>-->
                */?>
                <input type="hidden" id="idtipo_cliente_s" value="<?php echo $idtipo_cliente_s ?>">
                <select style="font-weight: bold;" class="form-control" name="idtipo_cliente" id="idtipo_cliente" onchange="tipo_cliente_select()">
                  <option selected="" disabled="" value="0">Seleccione el tipo de cliente</option>
                  <?php foreach ($get_tipo_cliente as $i) {
                    //$select="";
                    //if(isset($datos) && $datos->idperfilamiento==$i->idtipo_cliente){ $select="selected"; } ?>
                      <option value="<?php echo $i->idtipo_cliente ?>"><?php echo $i->tipo ?></option>
                   <?php } ?>
                </select>
              </div>
              <div class="col-md-2 form-group">
                <br>
                <button type="button" class="btn btn_ayuda" onclick="paso2_hoja1_btn_texto_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>
              </div>
            </div>  
          </form>
        </div>
        <!--------------Tipo de cliente------->
        <span class="text_tipo_cliente"></span>
        <!------------------------------------>
        <div class="firma_tipo_cliente">
          <br>
          <br>
          <hr class="subtitle">
          <div class="firma_tipo_cliente" style="display: none;">
            <div class="row">
              <div class="col-md-6" style="text-align: justify !important;">
                <hr>
                <center><span id="nombre_cli_imp"></span></center><br>
                  Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
              </div>
            
              <div class="col-md-6" style="text-align: justify !important;">
                <hr>
                <center><span id="nombre_ejec_imp"></span></center><br>
                  Declaro bajo protesta de decir verdad, que los datos asentados en la presente solicitud ha sido proporcionados directamente por el cliente.
              </div> 
              
            </div>
          </div>
        </div> 
    </div>
  </div>
</div>
<!--------------Modal-------------->
<div class="modal fade" id="modalpaso2_4_ayudad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <span style="text-align : justify;">
         Persona moral mexicana de derecho público detallados en el anexo 7 Bis A son las Secretarias de Estado: Secretaria de Gorbernación, Secretaria de Relaciones Exteriores, Secretaria de Defensa Nacional, Secretaria de Marina, Secretaria de Hacienda y Crédito Público, Secretaria de Comunicaciones y Transportes, Secretaria de la Función Pública, Centro de Investigación y Seguridad Nacional, Instituto Nacional de Migración, Secretaria Ténica del Consejo de Coordinación para la Implementación del Sistema de Justicia Penal y Servicio de Administración Tributaria.
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--------------Modal-------------->
<div class="modal fade" id="modalpaso1_1_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <span style="text-align : justify; font-size: 12px">
           <li>Persona física mexicana o extranjera con condición de estancia temporal o permanente: En caso de persona extranjera, de acuerdo con el articulo 52 de la ley de migración.</li>
           <li>La estancia permanente autoriza al extranjero a residir en México de forma indefinida (Formato Migratorio FM2).</li>
           <li>La estancia temporal es para estudiantes (autoriza al extrajero a permanecer en el país por el tiempo que duren los estudios).</li>
           <li>Persona física extranjera con condición de estancia de visitante. De acuerdo con el articulo 52 de la ley de migración, el 
           extranjero puede permanecer en el país por un periodo de hasta 180 días ininterrumpidos y puede o no contar con permiso para
           realizar actividades remuneradas.</li>
           <li>Persona moral mexicana y extranjera. Es cualquier organización de individuos de nacionalidad mexicana o extranjera. </li>
           <li>Persona moral de derecho público: Son dependencias y entidades públicas, federales, municipales y estatales.</li>
           <li>Persona moral mexicana de derecho público detallados en el anexo 7 Bis A son las Secretarías de Estado: Secretaría de Gobernación,
           Secretaría de Relaciones Exteriores, Secretaría de Defensa Nacional, Secretaría de Marina, Secretaría de Hacienda y Crédito Público,
           Secretaría de Comunicaciones y Transportes, Secretaría de la Función Pública, Centro de Investigación y Seguridad Nacional,
           Instituto Nacional de Migración, Secretaría Técnica del Consejo de Coordinación para la implementación del Sistema de Justicia Penal
           y Servicio de Administración Tributaria.</li>
           <li>Embajadas, Consulados u Organismos Internacionales: Cualquier Entidad formada por sujetos de derecho internacional público con 
           sede o residencia en México.</li>
           <li>Fideicomiso: Cualquier fideicomiso.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- Modal banco eliminar -->
<div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Seguro que deseas eliminar este beneficiario?</h5>
      </div>
      <input type="hidden" id="id_beneficiario_e_1">
      <input type="hidden" id="delete_div_beneficiario_1">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_eliminar_2" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Seguro que deseas eliminar este beneficiario?</h5>
      </div>
      <input type="hidden" id="id_beneficiario_e_2">
      <input type="hidden" id="delete_div_beneficiario_2">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="eliminar_2()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- Modal banco eliminar -->
<div class="modal fade" id="modal_spinner" tabindex="-1" role="dialog" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-color: #fff0 !important; top: 200px !important; border: 1px solid rgba(0, 0, 0, 0)!important;">
      <div class="modal-body" style="background: #4042580a !important;" align="center">
        <div class="loader" style="border-top: 16px solid #212b4c !important;border: 16px solid #b17f4a;"></div>
        <h1 style="color: white">Cargando por favor espere...</h1>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_ayuda_pep" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>¿Qué es PEP?</h5>
        <h5>Persona Políticamente Expuesta definida como aquel individuo que desempeña o ha desempeñado funciones públicas destacadas en un país extranjero o en territorio nacional, considerando entre otros, a los jefes de estado o de gobierno, líderes políticos, funcionarios gubernamentales, judiciales o militares de alta jerarquía, altos ejecutivos de empresas estatales o funcionarios o miembros importantes de partidos políticos y organizaciones internacionales.</h5>
      </div>
      <input type="hidden" id="id_beneficiario_e_2">
      <input type="hidden" id="delete_div_beneficiario_2">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_ayuda_fpg" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>¿Qué es Familiar primer grado?</h5>
        <h5>Familiar primer grado: esposa(o), hijo(a), padre, madre / familiar segundo grado: abuelo(a), tío(a), sobrino(a), primo(a) / familiar político: suegro(a), cuñado(a), concuño(a), nuera, yerno .</h5>
      </div>
      <input type="hidden" id="id_beneficiario_e_2">
      <input type="hidden" id="delete_div_beneficiario_2">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--- Modal rfc -->
<div class="modal fade" id="modal_rfc" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5><strong>Formato de RFC</strong></h5>
        <h5>En caso de existir un RFC a ingresar debe contar con un formato valido real, o en formato genérico, de caso contrario dejar campo en blanco.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<!--- Modal FechaFormato -->
<div class="modal fade" id="modal_fecha" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Fecha de formato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" width="100%" id="tipo_f" readonly>
        <input type="hidden" width="100%" id="idp_f" readonly>
        <input type="date" style="width: 100%" id="fecha_f" class="form-control">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal" onclick="imprimir_formato1_1()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>