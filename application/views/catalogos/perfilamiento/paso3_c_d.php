<div class="row">
  <div class="col-md-12 form-group">
    <div class="form-check form-check-primary">
      <label class="form-check-label">C) Nombre del presidente del consejo de administración o administrador unico (según aplique, proviniente del acta constitutiva poderes).   
        <input type="checkbox" class="form-check-input" id="accionaria_socioa_c" onchange="accionaria_socioa_c_btn()">
      </label>
    </div>
  </div>
</div>
<div class="accion_socio_c" style="display: none"> 
	<div class="row">
		<div class="col-md-2 form-group">
		  <div class="form-check form-check-success">
		    <label class="form-check-label">
		      <input type="radio" class="form-check-input" name="tipo_persona" id="ExampleRadio2">
		      Persona fisica
		    </label>
		  </div>
		</div>  
	</div>	
	<div class="row">
	  <div class="col-md-4 form-group">
	    <label>Nombre(s):</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	  <div class="col-md-4 form-group">
	    <label>Apellido paterno:</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	  <div class="col-md-4 form-group">
	    <label>Apellido materno:</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	</div>
	<div class="row">
		<div class="col-md-12 form-group">
		  <div class="form-check form-check-success">
		    <label class="form-check-label">
		      <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
		      Si el administrador es una persona moral o fideicomiso, indicar el nombre de la persona fisica nombrada por el administrador de la persona moral o fideicomiso. 
		    </label>
		  </div>
		</div>
	</div>	
	<div class="row">
	  <div class="col-md-4 form-group">
	    <label>Nombre(s):</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	  <div class="col-md-4 form-group">
	    <label>Apellido paterno:</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	  <div class="col-md-4 form-group">
	    <label>Apellido materno:</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	</div>
</div>
<div class="row">
  <div class="col-md-12 form-group">
    <div class="form-check form-check-primary">
      <label class="form-check-label">D) Nombre del director general(proviniente del documento de identificación).   
        <input type="checkbox" class="form-check-input" id="accionaria_socioa_d" onchange="accionaria_socioa_d_btn()">
      </label>
    </div>
  </div>
</div>
<div class="accion_socio_d" style="display: none"> 	
	<div class="row">
	  <div class="col-md-4 form-group">
	    <label>Nombre(s):</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	  <div class="col-md-4 form-group">
	    <label>Apellido paterno:</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	  <div class="col-md-4 form-group">
	    <label>Apellido materno:</label>
	    <input class="form-control" type="text" name="" value="">
	  </div>
	</div>
</div>