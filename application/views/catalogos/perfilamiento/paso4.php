<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
	<div class="card">
	  <div class="card-body">
	  	<div class="row">
	      <div class="col-md-12">    
	        <h3>Paso 4. Calificar grado de riesgo del cliente</h3>
	      </div>
	    </div>  
	    <hr class="subtitle"> 
	    <div class="row">
		  <div class="col-md-12">
		    <span >Entidad a calificar.</span>
		  </div>
		</div>
		<div class="row">
      <div class="col-md-12 form-group">
        <label>Tipo de persona: <span>Persona fisica mexicana o extranjera con condición de estancia temporal o permanente</span></label>
      </div>
    </div>  
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Nombre: <span> Juan Perez</span></label>
      </div>
      <div class="col-md-4 form-group">
      	<label>Fecha de nacimiento: 01-enero-1990</label>
      </div>
    </div> 
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Giro del cliente:</label>
        <select class="form-control ">
        </select>
      </div>
      <div class="col-md-4 form-group">
        <label>Forma de pago:</label>
        <select class="form-control ">
          </select>
      </div>
      <div class="col-md-4 form-group">
        <label>Domicilio del cliente(Ciudad):</label>
        <select class="form-control ">
          </select>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Tipo de  cliente</label>
        <select class="form-control ">
          </select>
      </div>
    </div> 
    <hr class="subtitle">
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Grado de riesgo del cliente</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-4 form-group"></div>
      <div class="col-md-4 form-group">
      	<label>Escala de calificación</label><br>
      	<table>
      		<thead class="table"></thead>
      		<tbody>
      		   <tr>
      		   	<td>hasta 130</td>
      		   	<td>bajo</td>
      		   </tr>	
      		   <tr>
      		   	<td>arriba 130</td>
      		   	<td>alto</td>
      		   </tr>
      		</tbody>
      	</table>
      </div>
    </div> 
    <hr class="subtitle">
      <div class="row"> 
        <div class="col-md-12" align="right">
          <a class="btn btn-yimexico" href="<?php echo base_url(); ?>Perfilamiento/Recopilacion_de_documentos/1">Guardar</a>
        </div>
      </div>
	  </div>
	</div>  
</div>