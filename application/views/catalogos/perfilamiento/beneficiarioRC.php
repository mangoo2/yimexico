<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h3>¿Cuenta con propietario/beneficiario real?</h3>
      	<div class="row">
      		<div class="col-md-12">
      			<hr class="subtitle">
      		</div>
      		<div class="col-md-12 form-group">
      			<label>Razón Social</label>
      			<input class="form-control" type="text" name="" value="">
      		</div>
      		<div class="col-md-12">
      			<hr class="subsubtitle">
      		</div>
      		<div class="col-md-6 form-group">
  	 				<label>Cuenta con propietario/beneficiario real</label>
            <div class="row">
             <div class="col-md-6 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                  Si
                </label>
              </div>
             </div>
             <div class="col-md-6 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                  No
                </label>
              </div>
             </div>
            </div>
          </div>
        </div>
        <form id="datosGenerales">
          <div class="row">
            <div class="col-md-12 form-group">
              <label>Nombre Apellido paterno, materno y nombre(s)/ En caso de ser extranjero los apellidos completos que corresponda y nombre(s)</label>
            </div>
            <div class="col-md-12 form-group">
              <label>Nombre</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-6 form-group">
              <label>Apellido Paterno</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-6 form-group">
              <label>Apellido Materno</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-4 form-group">
              <label>No,bre de identificación</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-4 form-group">
              <label>Autoridad que emite la identificación</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-4 form-group">
              <label>Numero de identificación</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-6 form-group">
            <label>Sexo</label>
            <div class="row">
             <div class="col-md-6 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                  Masculino
                </label>
              </div>
             </div>
             <div class="col-md-6 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
                  Femenino
                </label>
              </div>
             </div>
            </div>
          </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha de nacimiento</label>
              <input class="form-control" type="date" name="" value="">
            </div>
            <div class="col-md-4 form-group">
              <label>País de nacimiento</label>
              <select class="form-control">
                <option>México</option>
                <option>Estador unidos</option>
                <option>Canada</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Pais de nacionalidad</label>
              <select class="form-control">
                <option>México</option>
                <option>Estador unidos</option>
                <option>Canada</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 form-group">
              <label>Correo electrónico</label>
              <input class="form-control" type="email" name="" value="">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 form-group">
              <label>RFC</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-6 form-group">
              <label>CURP</label>
              <input class="form-control" type="text" name="" value="">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h6>Direccion:</h6>
            </div>
            <div class="col-md-2 form-group">
              <label>Tipo Calle</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Calle</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-2 form-group">
              <label>No.Ext</label>
              <input class="form-control" type="number" name="" value="">
            </div>
            <div class="col-md-2 form-group">
              <label>No.Int</label>
              <input class="form-control" type="number" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Colonia</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Municipio o Delegación</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Estado</label>
              <select class="form-control">
                <option>Puebla</option>
                <option>Culiacan</option>
                <option>Quéretaro</option>
              </select>
            </div>
            <div class="col-md-2 form-group">
              <label>CP</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-4 form-group">
              <label>País</label>
              <select class="form-control">
                <option>México</option>
                <option>Estador unidos</option>
                <option>Canada</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 form-group">
              <div class="form-check form-check-danger">
                <label class="form-check-label">Tratandose de personas qie tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia diriguida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio
                  <input type="checkbox" class="form-check-input" id="" value="">
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h6>Direccion:</h6>
            </div>
            <div class="col-md-2 form-group">
              <label>Tipo Calle</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Calle</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-2 form-group">
              <label>No.Ext</label>
              <input class="form-control" type="number" name="" value="">
            </div>
            <div class="col-md-2 form-group">
              <label>No.Int</label>
              <input class="form-control" type="number" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Colonia</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Municipio o Delegación</label>
              <input class="form-control" type="text" name="" value="">
            </div>
            <div class="col-md-3 form-group">
              <label>Estado</label>
              <select class="form-control">
                <option>Puebla</option>
                <option>Culiacan</option>
                <option>Quéretaro</option>
              </select>
            </div>
            <div class="col-md-2 form-group">
              <label>CP</label>
              <input class="form-control" type="text" name="" value="">
            </div>
          </div>
        </form>
        <div class="row">
          <div class="col-md-12 pull-right">
            <button class="btn btn-yimexico pull-right" type="">Imprimir formato para firma</button>
            <button class="btn btn-info pull-right" type="">Guardar</button>
          </div>
          <div class="col-md-12">
            <button class="btn btn-danger pull-right" type="">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
