<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
	<div class="card">
	  <div class="card-body">
	  	<div class="row">
	      <div class="col-md-12">    
	        <h3>Paso 3. Identificación del dueño beneficiario</h3>
	      </div>
	    </div>  
	    <hr class="subtitle"> 
	    <div class="row">
		  <div class="col-md-12">
		    <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
		  </div>
		</div><br>
		<div class="row">
          <div class="col-md-4 form-group">
            <label>Nombre(s):</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido paterno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido materno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
        </div>
        <hr class="subtitle">
        <div class="row">
		  <div class="col-md-12" align="center">
		    <span>¿Tiene conocimiento de la existencia del dueño beneficiario?</span>
		  </div>
        </div>
	    <div class="row">
	    	<div class="col-md-5 form-group"></div>
	    	<div class="col-md-4 form-group">
              <div class="row">
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                      Si
                    </label>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                    No
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 form-group"></div>
	    </div>
	    <div class="row">
		  <div class="col-md-12" align="center">
		    <span>¿Es persona fisica o moral?</span>
		  </div>
        </div>
	    <div class="row">
	    	<div class="col-md-4 form-group"></div>
	    	<div class="col-md-4 form-group">
              <div class="row">
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                      Persona fisica
                    </label>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                      Persona moral
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 form-group"></div>
	    </div>
	    <div class="row">
		  <div class="col-md-12">
		    <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
		  </div>
		</div><br>
		<div class="row">
          <div class="col-md-4 form-group">
            <label>Nombre(s):</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido paterno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
          <div class="col-md-4 form-group">
            <label>Apellido materno:</label>
            <input class="form-control" type="text" name="" value="">
          </div>
        </div>
        <div class="row">
	        <div class="col-md-4 form-group">
	          <label>Nombre de identificación:</label>
	          <input class="form-control" type="text" name="" value="">
	        </div>
	        <div class="col-md-4 form-group">
	          <label>Autoridad que emite la identificación:</label>
	          <input class="form-control" type="text" name="" value="">
	        </div>
	        <div class="col-md-4 form-group">
	          <label>Número de identificación:</label>
	          <input class="form-control" type="text" name="" value="">
	        </div>
	    </div>
        <div class="row">
            <div class="col-md-7 form-group">
              <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
              <select class="form-control ">
              </select>
            </div>
            <div class="col-md-2"><br>
              <div class="form-check form-check-flat form-check-primary">
                <label class="form-check-label">
                  <input type="checkbox" class="form-check-input" id="check_direccion_a" onchange="especifique_btnpaso3()">
                  Otro, especifique:
                </label>
              </div>
            </div>
            <div class="col-md-3 form-group"><br>
              <input class="form-control" type="text" style="display: none" name="" id="especifique" value="">
            </div>
        </div>  	
        <div class="row">
            <div class="col-md-4 form-group">
              <label>Género:</label>
              <div class="row">
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                      Masculino
                    </label>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                      Femenino
                    </label>
                  </div>
                </div>
              </div>
            </div>	
        </div>
        <div class="row"> 
          <div class="col-md-4 form-group">
		    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
		    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País de nacimiento:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Pais de nacionalidad:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>  
        <div class="row">
		  <div class="col-md-12">
		    <h4>Dirección</h4>
		  </div>
		</div>
		<div class="row">  
		  <div class="col-md-2 form-group">
		    <label>Tipo de vialidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Calle:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.ext:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-2 form-group">
		    <label>No.int:</label>
		    <input class="form-control" type="number" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Colonia:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Municipio o delegación:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Localidad:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>Estado:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-3 form-group">
		    <label>C.P:</label>
		    <input class="form-control" type="text" name="" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>País:</label><br>
		    <select class="form-control idpais">
		      <option value="MX">MEXICO</option>
		    </select>
		  </div>
		</div>
		<div class="row">
		  <div class="col-md-12 form-group">
		    <div class="form-check form-check-primary">
		      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
		        <input type="checkbox" class="form-check-input" id="lugar_recidencia" onchange="lugar_recidencia_btn_paso3()">
		      </label>
		    </div>
		  </div>
		</div>
		<div class="residenciaEx" style="display: none">
		  <div class="row">
		    <div class="col-md-12">
		      <span>Dirección en México.</span>
		    </div>
		  </div><br>
		  <div class="row">
		    <div class="col-md-2 form-group">
		      <label>Tipo de vialidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Calle:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.ext:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-2 form-group">
		      <label>No.int:</label>
		      <input class="form-control" type="number" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Colonia:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Municipio o delegación:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Localidad:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>Estado:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>C.P:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-6 form-group">
		      <label>Correo electrónico:</label>
		      <input class="form-control" type="text" name="" value="">
		    </div>
		    <div class="col-md-3 form-group">
		      <label>R.F.C:</label>
		      <input class="form-control" type="text" name="" value=""><span>*Cuando cuente con R.F.C</span>
		    </div>
		    <div class="col-md-3 form-group">
		      <label>CURP:</label>
		      <input class="form-control" type="text" name="" value="">
		      <span>*Cuando cuente con CURP</span>
		    </div>
		  </div>
		</div>
        <div class="row">
		  <div class="col-md-12" align="right">
		    <button type="button" class="btn btn-dorado"><i class="fa fa-plus"></i> Agregar otro beneficiario</button>
		  </div> 
		</div>
        <hr class="subtitle"> 
        <?php include('paso3_a.php'); ?> 
	    <?php include('paso3_b.php'); ?> 
	    <?php include('paso3_c_d.php'); ?> 
		<hr class="subtitle"> 
		<div class="row">
		  <div class="col-md-12" align="center">
		  	<span></span>
		  </div>	
		</div>
        <hr class="subtitle">
	    <div class="row"> 
	      <div class="col-md-12" align="right">
	          <button type="button" class="btn btn-secondary "><i class="fa fa-print"></i> Imprimir formato para firmar</button>
	        <a class="btn btn-yimexico" href="<?php echo base_url(); ?>Perfilamiento/Calificar_grado_de_riesgo_del_cliente">Guardar</a>
	      </div>
	    </div>
    </div>
  </div>
</div>
