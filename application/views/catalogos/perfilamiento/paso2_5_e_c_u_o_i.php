<div class="row">
  <div class="col-md-12 form-group">
    <label>Denominación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">  
  <div class="col-md-4 form-group">
    <label><i class="fa fa-calendar"></i> Fecha de establecimiento en Territorio Nacinal:</label>
    <input class="form-control" type="date" name="" value="">
  </div>
</div>  
<div class="row">
  <div class="col-md-12 form-group">
    <span>Clave del Registro Federal de Constribuyentes con homoclave / # identificación fiscal en otro pais (cuando aplique) y pais que la asigno</span>
  </div>
</div>
<div class="row">  
  <div class="col-md-5 form-group"><br>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>País:</label><br>
    <select class="form-control idpais">
      <option value="MX">MEXICO</option>
    </select>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h4>Dirección</h4>
  </div>
</div>
<div class="row">  
  <div class="col-md-2 form-group">
    <label>Tipo de vialidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Calle:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.ext:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.int:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Colonia:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Municipio o delegación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Localidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Estado:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>C.P:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>País:</label><br>
    <select class="form-control idpais">
      <option value="MX">MEXICO</option>
    </select>
  </div>
</div>
<div class="row">  
  <div class="col-md-3 form-group">
    <label>Número de teléfono:</label>
    <input class="form-control" placeholder="(Local) + Teléfono" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Correo electrónico:</label>
    <input class="form-control" type="email" name="" value="">
  </div>
  <div class="col-md-5 form-group">
    <label>Certificado de Matricula Consular:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>  
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h6>DATOS GENERALES DEL APODERADO / REPRESENTANTE LEGAL / SERVIDORES PÚBLICOS EN CASO DE PM DE DERECHO PÚBLICO</h6>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
  </div>
</div>
<!-- -->
<br>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Nombre(s):</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido paterno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido materno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Género:</label>
    <div class="row">
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Masculino
          </label>
        </div>
      </div>
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Femenino
          </label>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 form-group">
    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
    <input class="form-control" type="date" name="" value="">
  </div>  
</div>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Nombre de identificación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Autoridad que emite la identificación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Número de identificación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-2 form-group"></div>
  <div class="col-md-4 form-group">
    <label>R.F.C:</label>
    <input class="form-control" type="text" name="" value=""><span>*Cuando cuente con R.F.C</span>
  </div>
  <div class="col-md-4 form-group">
    <label>CURP:</label>
    <input class="form-control" type="text" name="" value="">
    <span>*Cuando cuente con CURP</span>
  </div>
  <div class="col-md-2 form-group"></div>
</div>