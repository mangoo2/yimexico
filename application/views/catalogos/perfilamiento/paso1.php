<h3>Paso 1. Perfilamiento del cliente</h3>
<hr class="subtitle">
<div class="row">
	<div class="col-md-12 form-group">
		<label>Identifique el tipo de cliente</label>
		<select id="perfilCli" class="form-control" onchange="tipo_cliente_select()">
			<option selected="" disabled="">Seleccione el tipo de cliente</option>
			<option value="1">Persona fisica mexicana o extranjera con condición de estancia temporal o permanente</option>
		    <option value="2">Persona fisica extranjera con condición de estancia de visitante</option>
		    <option value="3">Persona moral mexicana y extranjera, persona moral de derecho público</option>
		    <option value="4">Persona moral mexicana de derecho público detallados en el anexo 7 bis A</option>
		    <option value="5">Embajadas, consulados u organismos internacionales</option>
		    <option value="6">Fideicomisos</option>
		</select>
	</div>
</div>
