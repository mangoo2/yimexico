<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h3>Calificar Grado de riesgo del cliente</h3>
      	 <div class="row">
          <div class="col-md-3">
            <label>Tipo de persona</label>
          </div>
          <div class="col-md-9">
            <label id="tipoPersona"></label>
          </div>
          <div class="col-md-1">
            <label>Nombre</label>
          </div>
          <div class="col-md-7">
            <label id="NombrePersona"></label>
          </div>
          <div class="col-md-2">
            <label>Fecha de Nacimiento</label>
          </div>
          <div class="col-md-2">
            <label id="tipoPersona">12-12-90</label>
          </div>
      	 </div>
         <form id="form_riesgo">
           <div class="row">
             <div class="col-md-12 form-group">
              <label>Giro del Cliente</label>
              <select class="form-control">
                <option value="0">Giro ejemplo 1</option>
                <option value="1">Giro ejemplo 2</option>
                <option value="2">Giro ejemplo 3</option>
                <option value="3">Giro ejemplo 4</option>
                <option value="4">Giro ejemplo 5</option>
                <option value="5">Giro ejemplo 6</option>
                <option value="6">Giro ejemplo 7</option>
              </select>
            </div>
            <div class="col-md-12 form-group">
              <label>Forma de Pago</label>
              <select class="form-control">
                <option value="0">Efectivo</option>
                <option value="1">Cheque</option>
                <option value="2">Transferencia</option>
              </select>
            </div>
            <div class="col-md-12 form-group">
              <label>Domicilio del Cliente (Ciudad)</label>
              <select class="form-control">
                <option value="0">Ciudad ejemplo 1</option>
                <option value="1">Ciudad ejemplo 2</option>
                <option value="2">Ciudad ejemplo 3</option>
                <option value="3">Ciudad ejemplo 4</option>
                <option value="4">Ciudad ejemplo 5</option>
                <option value="5">Ciudad ejemplo 6</option>
                <option value="6">Ciudad ejemplo 7</option>
              </select>
            </div>
            <div class="col-md-12 form-group">
              <label>Tipo de Cliente</label>
              <select class="form-control">
                <option value="0"></option>
                <option value="1">PEP'S (Persona Politicamente Expuesta)</option>
                <option value="2">Fideicomisos</option>
                <option value="3">Personas Fisicas con dueños Beneficiarios</option>
                <option value="4">Otro tipo de Cliente</option>
              </select>
            </div>
           </div>
           <div class="row">
             <div class="col-md-12">
               <hr class="subsubtitle">
             </div>
           </div>
           <div class="row">
             <div class="col-md-4">
               <label>Grado de Riesgo Del Cliente</label>
               <input class="form-control" id="GradoR" type="text" name="" value="">
             </div>
             <div class="col-md-2">
               <label id="GradoRL"></label>
             </div>
           </div>
         </form>
    	</div>
    </div>
  </div>
</div>