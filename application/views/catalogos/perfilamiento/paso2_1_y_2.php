<div class="row">
  <div class="col-md-4 form-group">
    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
    <input class="form-control" type="date" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>País de nacimiento:</label><br>
    <select class="form-control idpais">
      <option value="MX">MEXICO</option>
    </select>
  </div>
  <div class="col-md-4 form-group">
    <label>Pais de nacionalidad:</label><br>
    <select class="form-control idpais">
      <option value="MX">MEXICO</option>
    </select>
  </div>
</div>
<div class="row">
  <div class="col-md-7 form-group">
    <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
    <select class="form-control ">
    </select>
  </div>
  <div class="col-md-2"><br>
    <div class="form-check form-check-flat form-check-primary">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" id="check_direccion_a" onchange="especifique_btn()">
        Otro, especifique:
      </label>
    </div>
  </div>
  <div class="col-md-3 form-group"><br>
    <input class="form-control" type="text" style="display: none" name="" id="especifique" value="">
  </div>
</div>
<!--//////////////////////-->
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h4>Dirección</h4>
  </div>
</div>
<div class="row">  
  <div class="col-md-2 form-group">
    <label>Tipo de vialidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Calle:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.ext:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.int:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Colonia:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Municipio o delegación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Localidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Estado:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>C.P:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>País:</label><br>
    <select class="form-control idpais">
      <option value="MX">MEXICO</option>
    </select>
  </div>
</div>
<div class="row">
  <div class="col-md-12 form-group">
    <div class="form-check form-check-primary">
      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
        <input type="checkbox" class="form-check-input" id="lugar_recidencia" onchange="lugar_recidencia_btn()">
      </label>
    </div>
  </div>
</div>
<div class="residenciaEx" style="display: none">
  <div class="row">
    <div class="col-md-12">
      <span>Dirección en México.</span>
    </div>
  </div><br>
  <div class="row">
    <div class="col-md-2 form-group">
      <label>Tipo de vialidad:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
    <div class="col-md-3 form-group">
      <label>Calle:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
    <div class="col-md-2 form-group">
      <label>No.ext:</label>
      <input class="form-control" type="number" name="" value="">
    </div>
    <div class="col-md-2 form-group">
      <label>No.int:</label>
      <input class="form-control" type="number" name="" value="">
    </div>
    <div class="col-md-3 form-group">
      <label>Colonia:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
    <div class="col-md-3 form-group">
      <label>Municipio o delegación:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
    <div class="col-md-3 form-group">
      <label>Localidad:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
    <div class="col-md-3 form-group">
      <label>Estado:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
    <div class="col-md-3 form-group">
      <label>C.P:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 form-group">
      <label>Correo electrónico:</label>
      <input class="form-control" type="text" name="" value="">
    </div>
    <div class="col-md-3 form-group">
      <label>R.F.C:</label>
      <input class="form-control" type="text" name="" value=""><span>*Cuando cuente con R.F.C</span>
    </div>
    <div class="col-md-3 form-group">
      <label>CURP:</label>
      <input class="form-control" type="text" name="" value="">
      <span>*Cuando cuente con CURP</span>
    </div>
  </div>
  <!-- paso 2_2 hoja 2-->
  <div class="row paso_2_2_hoja_telefono">
      <div class="col-md-3 form-group">
        <label>Número de teléfono:</label>
        <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="" value="">
      </div>
  </div>
  <!-- -->
</div>
<!--//////////////////////-->
<div class="paso_2_1_hoja_presenta_apoderado_legal">
  <div class="row">
    <div class="col-md-12">
      <hr class="subtitle">
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 form-group">
      <div class="form-check form-check-primary">
        <label class="form-check-label"> Presenta apoderado legal.
          <input type="checkbox" class="form-check-input" id="presenta_apoderado" onchange="presenta_apoderado_btn()">
        </label>
      </div>
    </div>
  </div>  
  <div class="presenta_apoderado_style" style="display: none"> 
    <div class="row">
        <div class="col-md-4 form-group">
          <label>Nombre(s):</label>
          <input class="form-control" type="text" name="" value="">
        </div>
        <div class="col-md-4 form-group">
          <label>Apellido paterno:</label>
          <input class="form-control" type="text" name="" value="">
        </div>
        <div class="col-md-4 form-group">
          <label>Apellido materno:</label>
          <input class="form-control" type="text" name="" value="">
        </div>
    </div>
    <div class="row">
      <div class="col-md-4 form-group">
        <label>Nombre de identificación:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-4 form-group">
        <label>Autoridad que emite la identificación:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-4 form-group">
        <label>Número de identificación:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-4 form-group">
        <label>Género:</label>
        <div class="row">
          <div class="col-md-6 form-group">
            <div class="form-check form-check-success">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                Masculino
              </label>
            </div>
          </div>
          <div class="col-md-6 form-group">
            <div class="form-check form-check-success">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
                Femenino
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2 form-group">
        <label>Tipo de vialidad:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-3 form-group">
        <label>Calle:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-2 form-group">
        <label>No.ext:</label>
        <input class="form-control" type="number" name="" value="">
      </div>
      <div class="col-md-2 form-group">
        <label>No.int:</label>
        <input class="form-control" type="number" name="" value="">
      </div>
      <div class="col-md-3 form-group">
        <label>Colonia:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-3 form-group">
        <label>Municipio o delegación:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-3 form-group">
        <label>Localidad:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-3 form-group">
        <label>Estado:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
      <div class="col-md-3 form-group">
        <label>C.P:</label>
        <input class="form-control" type="text" name="" value="">
      </div>
    </div>
  </div>
</div>
<!--//////////////////////-->
<div class="row">
  <div class="col-md-12">
    <hr class="subtitle">
    <h4>Beneficiarios</h4>
  </div>
</div>
<div class="row">
  <div class="col-md-4 form-group">
    <label>Nombre(s):</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido paterno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Apellido materno:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
    <input class="form-control" type="date" name="" value="">
  </div>
  <div class="col-md-4 form-group">
    <label>Género:</label>
    <div class="row">
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Masculino
          </label>
        </div>
      </div>
      <div class="col-md-6 form-group">
        <div class="form-check form-check-success">
          <label class="form-check-label">
            <input type="radio" class="form-check-input" name="hacienda" id="ExampleRadio2">
            Femenino
          </label>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <h4>Dirección</h4>
  </div>
</div>
<div class="row">
  <div class="col-md-2 form-group">
    <label>Tipo de vialidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Calle:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.ext:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>No.int:</label>
    <input class="form-control" type="number" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Colonia:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Municipio o delegación:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Localidad:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-3 form-group">
    <label>Estado:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
  <div class="col-md-2 form-group">
    <label>C.P:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div>
<div class="row">
  <div class="col-md-12" align="right">
    <button type="button" class="btn btn-dorado"><i class="fa fa-plus"></i> Agregar otro beneficiario</button>
  </div> 
</div>