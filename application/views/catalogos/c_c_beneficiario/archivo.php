<style type="text/css">
	.form-check .form-check-label input[type="checkbox"]:checked + .input-helper:before {
	    background: #323556;
	    border-width: 0;
	}
	.form-check .form-check-label input[type="checkbox"] + .input-helper:before {
	    content: "";
	    width: 18px;
	    height: 18px;
	    border-radius: 2px;
	    border: solid #323556;
	    border-width: 2px;
	}    
	.bg-success, .swal2-modal .swal2-buttonswrapper .swal2-styled.swal2-confirm, .settings-panel .color-tiles .tiles.success {
	    background-color: #b07f4a !important;
	}
	.progress.progress-md {
	    height: 19px;
	    border-radius: 10px;
	}
	.bg-danger, .settings-panel .color-tiles .tiles.danger {
	    background-color: #b07f4a !important;
	}
</style>
<span class="id_bene" hidden><?php echo $id_bene ?></span>
<span class="tipo_persona" hidden><?php echo $tipo_persona ?></span>
<span class="idbm" hidden><?php echo $idbm ?></span>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
		  <div class="card-body">
		  	<div class="row">
		      <div class="col-md-9">  
		        <h3>Recopilación de documentos del beneficiario de <?php echo $titulo_t ?></h3>
		      </div>
		      <div class="col-md-3" align="right">
              	<button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar</button>
               </div>  
		    </div>  
		    <hr class="subtitle"> 
		    <!--- -->
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
					      <?php echo $text ?><!---->
						</div>
					</div>	
					<br>               
		        </div>	
	        	<div class="col-md-6">
	        		<form id="form_docs_cliente" class="form">
	        			<?php if(isset($docs)) {?>
				            <input type="hidden" name="id" id="id" value="<?php echo $docs->id ?>">
				          <?php } else { ?>
				          	<input type="hidden" name="id" id="id" value="0">
				          <?php }?>
		        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
						    <div class="card-body">
						    <!-- -->
	                            <div class="row">
					              	<div class="col-md-12">
					                	<h5>Histórico de documentos capturados sobre el beneficiario</h5>
					              	</div>
					              	<div class="col-md-12" align="right">
					              		<a href="<?php echo base_url().'Clientes_c_beneficiario/exportarBitacoraDocs/'.$id_bene.'/'.$tipo_persona.'' ?>" class="btn gradient_nepal2"><i class="fa fa-file-excel-o"></i> Exportar Documentos</a>
					              	<?php if($tipo_persona==1){ ?>
						                <a href="<?php echo base_url().'Clientes_c_beneficiario/exportarBitacora/'.$id_bene.'/'.$tipo_persona.'/0' ?>" class="btn gradient_nepal2"><i class="fa fa-file-excel-o"></i> Exportar Lista</a>
						            <?php } 
						            else if($tipo_persona==2 || $tipo_persona==3) { ?>
						            	<input type="hidden" id="idbm_in" value="<?php echo $idbm; ?>">
						                <a href="<?php echo base_url().'Clientes_c_beneficiario/exportarBitacora/'.$id_bene.'/'.$tipo_persona.'/'.$idbm.'' ?>" class="btn gradient_nepal2"><i class="fa fa-file-excel-o"></i> Exportar Lista</a>
						            <?php } ?>
						            </div>
					            </div> 
					            <?php if($tipo_persona==1 || $tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont2">
					             	<div class="form-check form-check-purple">
					                	<h3 class="form-check-label">Formato "Conoce a tu cliente":
					                	</h3>
					              	</div>
					              	<div id="headingCollapse1"  class="card-header">
				                    	<a data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse1" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" aria-expanded="false">
					                <?php $i=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="formato_cc"){
					              			$i++;
					              			$img = base_url()."uploads_benes/formato_cc/".$b->nombre_doc;
						               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/formato_cc/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i."}";
						                	$ext = explode('.',$b->nombre_doc);
							                if($ext[1]!="pdf"){
							                    $imgp = "<img src='".base_url()."uploads_benes/formato_cc/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
							                    $typePDF = "false";  
							                }else{
							                    $imgp = "".base_url()."uploads_benes/formato_cc/".$b->nombre_doc." ";
							                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/formato_cc/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i."}";
							                    $typePDF = "true";
							                } 
						              	?>
						            	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgcc<?php echo $i;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcc'.$i.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
									    } 
							    } ?>
							    </div>
						    	<?php if($i==0){
					       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
						                	SIN DOCUMENTOS CARGADOS
					           		</div>';
				           		}
				           		?>
						        </div>
					            <?php } ?> <hr>
					            <?php if($tipo_persona==1){ ?> 
					            <div class="col-md-12 form-group" id="cont3">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Identificación oficial:
					                </h3>
					              </div>
					              <div id="headingCollapse2"  class="card-header">
				                    	<a data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse2" role="tabpanel" aria-labelledby="headingCollapse2" class="collapse" aria-expanded="false">
					                <?php $i2=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="ine"){
					              			$i2++;
					              			$img = base_url()."uploads_benes/ine/".$b->nombre_doc;
						               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/ine/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i2."}";
						                	$ext = explode('.',$b->nombre_doc);
							                if($ext[1]!="pdf"){
							                    $imgp = "<img src='".base_url()."uploads_benes/ine/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
							                    $typePDF = "false";  
							                }else{
							                    $imgp = "".base_url()."uploads_benes/ine/".$b->nombre_doc." ";
							                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/ine/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i2."}";
							                    $typePDF = "true";
							                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgio<?php echo $i2;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
						            <?php 
							       		echo '<script type="text/javascript">
							       		$("#imgio'.$i2.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
									    } 
							    } ?>
							    </div>
						    	<?php if($i2==0){
					       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
						                	SIN DOCUMENTOS CARGADOS
					           		</div>';
				           		}
				           		?>
						        </div>
					            <?php } ?> <hr>
					            
					            <?php if($tipo_persona==1){ ?> 
					            <div class="col-md-12 form-group" id="cont9">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">CURP:
					                </h3>
					              </div>
					              	<div id="headingCollapse3"  class="card-header">
				                    	<a data-toggle="collapse" href="#collapse3" aria-expanded="false" aria-controls="collapse3" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse3" role="tabpanel" aria-labelledby="headingCollapse3" class="collapse" aria-expanded="false">
					                <?php $i3=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="curp"){
					              			$i3++;
					              			$img = base_url()."uploads_benes/curp/".$b->nombre_doc;
						               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/curp/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i3."}";
						                	$ext = explode('.',$b->nombre_doc);
							                if($ext[1]!="pdf"){
							                    $imgp = "<img src='".base_url()."uploads_benes/curp/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
							                    $typePDF = "false";  
							                }else{
							                    $imgp = "".base_url()."uploads_benes/curp/".$b->nombre_doc." ";
							                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/curp/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i3."}";
							                    $typePDF = "true";
							                } 
						              	?>
							            <div class="row row_file">
								            <div class="col-md-6">
								            	<h3>Fecha: <?php echo $b->fecha; ?></h3>
								                <input width="50px" height="80px" class="img" id="imgcurp<?php echo $i3;?>" type="file">
								             </div>
								             <div class="col-md-10"></div>
								        </div>
					            		<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcurp'.$i3.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?>
							    	</div>
							    	<?php if($i3==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    ?>
					            </div>
					            <?php } ?> <hr>
					            <div class="col-md-12 form-group" id="cont4">
					              	<div class="form-check form-check-purple">
					                	<h3 class="form-check-label">Cédula de identificación fiscal:
					                	</h3>
					              	</div>
					              	<div id="headingCollapse4"  class="card-header">
				                    	<a data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapse4" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse4" role="tabpanel" aria-labelledby="headingCollapse4" class="collapse" aria-expanded="false">
					                <?php $i4=0; foreach ($bitacora as $b) {
				              		if($b->tipo_doc=="cif"){
				              			$i4++;
				              			$img = base_url()."uploads_benes/cif/".$b->nombre_doc;
					               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/cif/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i4."}";
					                	$ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads_benes/cif/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads_benes/cif/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/cif/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i4."}";
						                    $typePDF = "true";
						                } 
					              	?>
					              	<div class="row row_file">
						              <div class="col-md-6">
						              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
						                <input width="50px" height="80px" class="img" id="imgcif<?php echo $i4;?>" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           	</div>
					            	<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcif'.$i4.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?>
							    	</div>
							    	<?php if($i4==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    	?>
					            </div>
					            <hr>
					            <?php if($tipo_persona==1 || $tipo_persona==2){ ?> 
					            <div class="col-md-12 form-group" id="cont10">
					              	<div class="form-check form-check-purple">
					                	<h3 class="form-check-label">Comprobante de domicilio:
					                	</h3>
					              	</div>
					              	<div id="headingCollapse5" class="card-header">
				                    	<a data-toggle="collapse" href="#collapse5" aria-expanded="false" aria-controls="collapse5" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse5" role="tabpanel" aria-labelledby="headingCollapse5" class="collapse" aria-expanded="false">
					                <?php $i5=0; foreach ($bitacora as $b) {
				              		if($b->tipo_doc=="comprobante_dom"){
				              			$i5++;
				              			$img = base_url()."uploads_benes/compro_dom/".$b->nombre_doc;
					               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/compro_dom/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i5."}";
					                	$ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads_benes/compro_dom/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads_benes/compro_dom/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/compro_dom/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i5."}";
						                    $typePDF = "true";
						                } 
					              	?>
					              	<div class="row row_file">
						              <div class="col-md-6">
						              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
						                <input width="50px" height="80px" class="img" id="imgcomd<?php echo $i5;?>" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           	</div>
					           		<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcomd'.$i5.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?>
							    	</div>
							    	<?php if($i5==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    	?>
					            </div>
					            <?php } ?> <hr>
					            <?php if($tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              	<div class="form-check form-check-purple">
					                	<h3 class="form-check-label">Identificación oficial del apoderado legal:
					                	</h3>
					              	</div>
					              	<div id="headingCollapse6" class="card-header">
				                    	<a data-toggle="collapse" href="#collapse6" aria-expanded="false" aria-controls="collapse6" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse6" role="tabpanel" aria-labelledby="headingCollapse6" class="collapse" aria-expanded="false">
					                <?php $i6=0; foreach ($bitacora as $b) {
				              		if($b->tipo_doc=="imgineal"){
				              			$i6++;
				              			$img = base_url()."uploads_benes/ine_apoderado/".$b->nombre_doc;
					               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/ine_apoderado/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i6."}";
					                	$ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads_benes/ine_apoderado/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads_benes/ine_apoderado/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/ine_apoderado/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i6."}";
						                    $typePDF = "true";
						                } 
					              	?>
					              	<div class="row row_file">
						              <div class="col-md-6">
						              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
						                <input width="50px" height="80px" class="img" id="imgineapo<?php echo $i6;?>" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           	</div>
						            <?php 
							       		echo '<script type="text/javascript">
							       		$("#imgineapo'.$i6.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?>
							    	</div>
							    	<?php if($i6==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    	?>
					            </div>
					            <?php } ?> <hr>
	                            <?php if($tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont12">
					              	<div class="form-check form-check-purple">
					                <h3 class="form-check-label">Escritura constitutiva o instrumento público que acredite su existencia:
					                </h3>
					              	</div>
					              	<div id="headingCollapse7" class="card-header">
				                    	<a data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse7" role="tabpanel" aria-labelledby="headingCollapse7" class="collapse" aria-expanded="false">
						                <?php $i7=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="esc_ints"){
					              			$i7++;
					              			$img = base_url()."uploads_benes/escrituras/".$b->nombre_doc;
						               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/escrituras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i7."}";
						                	$ext = explode('.',$b->nombre_doc);
							                if($ext[1]!="pdf"){
							                    $imgp = "<img src='".base_url()."uploads_benes/escrituras/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
							                    $typePDF = "false";  
							                }else{
							                    $imgp = "".base_url()."uploads_benes/escrituras/".$b->nombre_doc." ";
							                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/escrituras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i7."}";
							                    $typePDF = "true";
							                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgesc<?php echo $i7;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
			
						            <?php 
							       		echo '<script type="text/javascript">
							       		$("#imgesc'.$i7.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?>
							    	</div>
							    	<?php if($i7==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    	?>
					            </div><hr>

					            <?php } ?>
					            <?php if($tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              	<div class="form-check form-check-purple">
					                	<h3 class="form-check-label">Poderes Notariales o documento para comprobar las facultades del(os) servidor(es) público(s):
					                	</h3>
					             	</div>
					             	<div id="headingCollapse8" class="card-header">
				                    	<a data-toggle="collapse" href="#collapse8" aria-expanded="false" aria-controls="collapse8" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse8" role="tabpanel" aria-labelledby="headingCollapse8" class="collapse" aria-expanded="false">
						                <?php $i8=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="poder_not_fac_serv"){
					              			$i8++;
					              			$img = base_url()."uploads_benes/poder_notarial/".$b->nombre_doc;
						               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/poder_notarial/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i8."}";
						                	$ext = explode('.',$b->nombre_doc);
							                if($ext[1]!="pdf"){
							                    $imgp = "<img src='".base_url()."uploads_benes/poder_notarial/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
							                    $typePDF = "false";  
							                }else{
							                    $imgp = "".base_url()."uploads_benes/poder_notarial/".$b->nombre_doc." ";
							                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/poder_notarial/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i8."}";
							                    $typePDF = "true";
							                } 
						              	?>
						              	<div class="row row_file">
							             	<div class="col-md-6">
							             		<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                	<input width="50px" height="80px" class="img" id="imgpod_not<?php echo $i8;?>" type="file">
							              	</div>
							              	<div class="col-md-10"></div>
							           	</div>

					            		<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgpod_not'.$i8.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?>
							    	</div>
							    	<?php if($i8==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    	?>
					            </div><hr>
					            <?php } ?>
					          	<div id="headingCollapse12" class="card-header">
			                      <a data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12" class="card-title lead collapsed" id="mas_docs_bene"><i class="fa fa-arrow-down"></i> Mas Documentos</a>
			                    </div>
			                    <div id="collapse12" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" aria-expanded="false">
			                    <div class="card-body">
			                    	<form id="form_docs_extra" class="form" method="post" role="form">
				                        <div class="card-block">
				                        	<?php if(isset($docs_ext)) {?>
									            <input type="hidden" id="id_extra" value="<?php echo $docs_ext->id ?>">
									        <?php } else { ?>
									          	<input type="hidden" id="id_extra" value="0">
									        <?php }?>

				                          	<div class="row">
				                                <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<h3 class="form-check-label">Documentos extras:</h3>
									              	</div>
									              	<div class="row">
									              		<?php $ex=0; foreach ($bitacora as $b) {
										              		if($b->tipo_doc=="doc extra"){
										              			$ex++;
										              		$img = base_url()."uploads_benes/extras/".$b->nombre_doc;
											                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads_benes/extras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$ex."}";
											                $ext = explode('.',$b->nombre_doc);
											                if($ext[1]!="pdf"){
											                    $imgp = "<img src='".base_url()."uploads_benes/extras/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
											                    $typePDF = "false";  
											                }else{
											                    $imgp = "".base_url()."uploads_benes/extras/".$b->nombre_doc." ";
											                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads_benes/extras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$ex."}";
											                    $typePDF = "true";
											                } 
											              	?>
											              	<div class="row row_file">
												             	<div class="col-md-12">
												             		<h3>Fecha: <?php echo $b->fecha; ?></h3>
												                	<input width="50px" height="80px" class="imgExt" id="extra<?php echo $ex;?>" type="file">
												              	</div>
												              	<div class="col-md-10"></div>
												           </div>
												           <?php 
												       		echo '<script type="text/javascript">
												       		$("#extra'.$ex.'").fileinput({
										                        language: "es",
															    overwriteInitial: false,
															    showUpload: false,
															    showClose: false,
															    showCaption: false,
															    showUploadedThumbs: false,
															    showBrowse: false,
															    showRemove: false,
															    showDelete: false,
															    fileActionSettings: {
															      showRemove: false,
															      showDelete: false,
															      showUpload: false,
															      showBrowse: false,
															      showZoom: true,
															      showDrag: false,
															    },
										                        removeTitle: "Cancel or reset changes",
										                        elErrorContainer: "#kv-avatar-errors-1",
										                        msgErrorClass: "alert alert-block alert-danger",
										                        defaultPreviewContent: "'.$img.'",
										                        layoutTemplates: {main2: "{preview} "},
										                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
										                        initialPreview: [
										                        "'.$imgp.'"
										                        ],
										                        initialPreviewAsData: '.$typePDF.',
										                        initialPreviewFileType: "image",
										                        initialPreviewConfig: [
										                            '.$imgdet.'
										                        ]
										                    });
														    </script>';
												       		}
											       		}
											    		if($ex==0){
											       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
												                	SIN DOCUMENTOS CARGADOS
											           		</div>';
										           		}
										           	?>
											       </div><br>
									            </div>
				                          	</div>
				                        </div>
			                    	</form>
			                      </div>
			                    </div>
						    </div>
						</div>
						<br>

					</form>    	
	        	</div>

	        </div>
            <!-- -->
	    </div>
	</div>
</div>