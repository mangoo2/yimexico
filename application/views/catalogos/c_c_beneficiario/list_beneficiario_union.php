<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #212b4c;
  }
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c;
  }
</style>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-9">
            <h3 class="">Listado de Dueños Beneficiarios de clientes unidos: <?php echo $nombre ?></h3>
          </div>
          <div class="col-md-3" align="right">
              <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar</button>
          </div>  
        </div> 
        <hr class="subtitle">
        <div class="col-md-12" align="right">
          <button class="btn gradient_nepal2 btn-rounded btn-fw" id="agregarBene"><i class="fa fa-user-plus"></i> Nuevo Dueño Beneficiario</button>
          <a class="btn gradient_nepal2" title="Exportar Beneficiarios de mancomunados" target="t_blanck" href="<?php echo base_url(); ?>Clientes_c_beneficiario/exportarBenesUnion/<?php echo $id_union; ?>"><i class="fa fa-download"></i> <i class="fa fa-file-excel-o"></i> Exportar</a>
        </div>
        <div class="col-md-12" align="right">
          <br>
        </div>
        <div class="table-responsive">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>">
          <table class="table" id="table_beneficiario">
            <thead>
              <tr>
                <!--<th>#</th>-->
                <th>Nombre</th>
                <th>Catálogos</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody id="info_benes">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_tipo_bene" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tipo de Beneficiario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>Elige un tipo de beneficiario para agregar</h5>
      </div>
      <div class=" col-md-12 form-group">
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Física  
            <input type="radio" class="form-check-input" name="tipoben" id="fisica" value="1">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Moral  
            <input type="radio" class="form-check-input" name="tipoben" id="moral" value="2">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Fideicomiso  
            <input type="radio" class="form-check-input" name="tipoben" id="fideicomiso" value="3">
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="aceptarAdd()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>