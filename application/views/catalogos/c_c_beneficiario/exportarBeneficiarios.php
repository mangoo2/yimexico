<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=beneficiarios".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <h2>Cliente: <?php echo $nombre; ?> </h2>
    <table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Beneficiario</th>
            <th>Tipo (unión)</th>
          </tr>
        </thead>
        <tbody>
        <?php $i=0; $html="";
    	$data = $this->ModeloCliente->get_benef_control_f('beneficiario_fisica',array('idtipo_cliente'=>$id_perf,'activo'=>1,"id_union"=>0));
    	foreach ($data as $key) { //para pintar las personas fisicas
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->nombre.' '.$key->apellido_paterno.' '.$key->apellido_materno.'
    				</td>
                    <td>Unico</td>
    			</tr>';
    	}

    	$datam = $this->ModeloCliente->get_benef_control_mf($id_perf,'beneficiario_moral_moral');
    	foreach ($datam as $key) { //para pintar las personas morales_fisicas
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->razon_social.'
    				</td>
                    <td>Unico</td>
    			</tr>';
    	}
    	$datam = $this->ModeloCliente->get_benef_control_mf($id_perf,'beneficiario_fideicomiso');
    	foreach ($datam as $key) { //para pintar fideicomiso
    		$i++;
    		$html .= '<tr class="rowt_'.$i.'">
    				<td>
    					'.$key->razon_social.'
    				</td>
                    <td>Único</td>   
    			</tr>';
    	}
        

    	$html.="</tbody>
            </table>";
    	echo $html;