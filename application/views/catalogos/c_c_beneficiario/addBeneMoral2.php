<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3><?php if(isset($b)) echo "Edita"; else echo "Nuevo" ?> Dueño Beneficiario</h3>
        <hr class="subtitle">
          <input type="hidden" id="idp" value="<?php echo $idp; ?>">
          <input type="hidden" id="idc" value="<?php echo $idc ?>">
          <input type="hidden" id="tipoc" value="<?php echo $tipoc ?>">
          <?php if(isset($b)){ ?>
            <input type="hidden" id="idbene" value="<?php echo $b->id ?>">
          <?php } ?>
          <!-- PERSONA MORAL PRIMER CHECK -->
          <span class="text_persona_moral">
            <hr class="subtitle"> 
            <div class="row">
              <div class="col-md-12">    
                <h3>IDENTIFICACIÓN DEL DUEÑO BENEFICIARIO</h3>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">A) Nombrar a la(s) persona(s) fisica(s) que directa o indirectamente, posea el 25% o más de la composición accionaria o dl capital social de la persona moral.   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_a" name="tipo_inciso" value="a"
                    <?php if(isset($b) && $b->identificacion_beneficiario=="a") echo "checked" ?>>
                  </label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">B) En caso de no tener accionistas que ostenten más del 25% de la composícion accionaria de la persona moral en forma individual o conjunta, favor de elistar a todos los accionistas personas física o morales, siempre y cuando matengan alguna relación juridica.   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_b" name="tipo_inciso" value="b"
                    <?php if(isset($b) && $b->identificacion_beneficiario=="b") echo "checked" ?>>
                  </label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">C) Nombre del presidente del consejo de administración o administrador unico (según aplique, proviniente del acta constitutiva poderes).   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_c" name="tipo_inciso" value="c"
                    <?php if(isset($b) && $b->identificacion_beneficiario=="c") echo "checked" ?>>
                  </label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">D) Nombre del director general(proviniente del documento de identificación).   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_d" name="tipo_inciso" value="d"
                    <?php if(isset($b) && $b->identificacion_beneficiario=="d") echo "checked" ?>>
                  </label>
                </div>
              </div>
            </div>
            <div id="contForm"> </div>

          </span>
          <br>

          <span class="btn_siguiente">
            <div class="row">
              <div class="col-md-12" align="right">
                <button type="button" id="btn_save" class="btn btn-yimexico"><i class="fa fa-save"></i> GUARDAR</button>
              </div>
            </div>
          </span>      

      </div>
    </div>
  </div>
</div>
