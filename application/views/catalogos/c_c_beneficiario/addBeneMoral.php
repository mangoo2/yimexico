<style type="text/css">
#resultado {
    color: red;
    font-weight: bold;
}
#resultado.ok {
    color: green; 
}
</style>
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-9">  
          <h3 class="barra_menu"><?php if(isset($b)) echo "Edita"; else echo "Nuevo" ?> Dueño Beneficiario</h3>
          </div>  
          <div class="col-md-3" align="right">
            <button type="button" class="btn gradient_nepal2 barra_menu" onclick="c_beneficiarios()"><i class="fa fa-arrow-left"></i> Regresar</button>
          </div>  
        </div>
        <div class="row firma_tipo_cliente" style="display: none">
          <div class="col-md-12">    
            <h3>Formato: Conoce al dueño beneficiario</h3>
            <h3>Fecha: <?php echo date("m-d-Y"); ?></h3>
            <h3>Persona moral</h3>
          </div>
        </div>
        <hr class="subtitle">
          <div class="row">
            <br><br>
            <div class="col-md-12 ">    
              <h3 class="barra_menu">Datos de la Persona moral</h3>
            </div>
          </div> 
          <form class="form" method="post" role="form" id="form_moral">
            <input type="hidden" id="idp" name="idp" value="<?php echo $idp; ?>">
            <input type="hidden" id="idopera" name="idopera" value="<?php echo $idopera; ?>">
            <input type="hidden" id="idc" name="idc" value="<?php echo $idc ?>">
            <input type="hidden" id="idcc" name="idcc" value="<?php echo $idcc ?>">
            <input type="hidden" id="tipoc" value="<?php echo $tipoc ?>">
            <input type="hidden" id="id_union" name="id_union" value="<?php echo $id_union ?>">
            <?php if(isset($b)){ ?>
              <input type="hidden" id="id" name="id" value="<?php echo $b->id ?>">
            <?php } ?>
            <div class="row">
              <div class="col-md-10 form-group">
                <label>Razón social:</label>
                <input class="form-control" type="text" name="razon_social" id="razon_social" value="<?php if(isset($b)) echo $b->razon_social ?>">
              </div>
            </div>
            <div class="row">    
              <div class="col-md-3 form-group">
                <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                <input class="form-control" max="<?php echo date('Y-m-d'); ?>" type="date" name="fecha_constitucion" value="<?php if(isset($b)) echo $b->fecha_constitucion ?>">
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 form-group">
                <label>R.F.C:</label>
                <input class="form-control rfc" type="text" name="rfc" value="<?php if(isset($b)) echo $b->rfc ?>" oninput="validarInput(this)">
                <!--<pre id="resultado"></pre>-->
              </div>
              <div class="col-md-4 form-group">
                <label>Pais de nacionalidad:</label><br>
                <select class="form-control pais_1" name="pais" onclick="getpais(1)">
                  <?php if(isset($b)){
                      foreach ($get_pais as $p){
                        $select="";
                        if(isset($b) && $b->pais==$p->clave){ $select="selected"; }
                          echo "<option value='$p->clave' $select>$p->pais</option>";
                      }
                    }else{
                      echo "<option value='MX'>MEXICO</option>";
                    }
                  ?>
                </select>
              </div>
            </div>
            <br>
            <div class="firma_tipo_cliente">
              <br>
              <br>
              <div class="firma_tipo_cliente" style="display: none;">
                <div class="row">
                  <div class="col-md-12" style="text-align: justify !important;">
                    
                    <hr>
                    <center><span id="nombre_cli_imp"><?php echo $nombre; ?></span></center><br>
                      Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
                  </div>
                  <!--
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_ejec_imp"></span></center><br>
                      Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados  
                  </div>
                  -->  
                </div>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-12" align="right">
                <?php if(isset($b) && $b->id>0){ ?>
                  <button type="button" class="btn gradient_nepal2 barra_menu" onclick="imprimir_formato1()"><i class="fa fa-print"></i> Imprimir formato conoce dueño beneficiario</button>
                <?php } ?>
                <button type="submit" id="btn_save" class="btn gradient_nepal2 barra_menu"><i class="fa fa-save"></i> GUARDAR</button>
                <button class="btn gradient_nepal2 barra_menu" type="button" onclick="c_beneficiarios()">Regresar</button>
              </div>
            </div>
          </form>   

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_fecha" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Fecha de formato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="tipo_con" readonly>
        <input type="hidden" id="id_cc" readonly>
        <input type="date" style="width: 100%" id="fecha_f" class="form-control">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>