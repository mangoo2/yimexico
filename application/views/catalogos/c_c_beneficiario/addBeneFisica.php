<style type="text/css">
#resultado {
    color: red;
    font-weight: bold;
}
#resultado.ok {
    color: green; 
}
.form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
}
.form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
}
.form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
}
.form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
}
</style>
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-9">  
            <h3 class="barra_menu"><?php if(isset($b)) echo "Edita"; else echo "Nuevo" ?> Dueño Beneficiario</h3>
          </div>  
          <div class="col-md-3" align="right">
            <button type="button" class="btn gradient_nepal2 barra_menu" onclick="c_beneficiarios()"><i class="fa fa-arrow-left"></i> Regresar</button>
          </div>  
        </div>
        <div class="row firma_tipo_cliente" style="display: none">
          <div class="col-md-12">    
            <h3>Formato: Conoce al dueño beneficiario</h3>
            <h3>Fecha: <?php echo date("m-d-Y"); ?></h3>
            <h3>Persona Física</h3>
          </div>
        </div> 
        <hr class="subtitle barra_menu"> 
          <form class="form" method="post" role="form" id="form_beneficiario_fisica"> 
            <?php if(isset($b)){ ?>
              <input type="hidden" id="id" name="id" value="<?php echo $b->id ?>">
              <input type="hidden" id="idopera" name="idopera" value="<?php echo $idopera; ?>">
              <input type="hidden" id="id_clientec" name="id_clientec" value="<?php echo $b->id_clientec ?>">
              <input type="hidden" id="idp" name="idp" value="<?php echo $idp ?>">
              <input type="hidden" id="idtipo_cliente" name="idtipo_cliente" value="<?php echo $b->idtipo_cliente ?>">
              <input type='hidden' id='tipoc' value="<?php echo $tipoc ?>">
              <input type="hidden" id="idcc" name="idcc" value="<?php echo $idcc ?>">
              <input type="hidden" id="id_union" name="id_union" value="<?php echo $id_union ?>">
            <?php }else{ 
              echo "<input type='hidden' id='id_clientec' name='id_clientec' value='".$idc."'>";
              echo "<input type='hidden' id='idtipo_cliente' name='idtipo_cliente' value='".$idp."'>";
              echo "<input type='hidden' id='tipoc' value='".$tipoc."'>";
              echo "<input type='hidden' id='idopera' name='idopera' value='".$idopera."'>";
              echo "<input type='hidden' id='idp' name='idp' value='".$idp."'>";
              echo "<input type='hidden' id='idcc' name='idcc' value='".$idcc."'>";
              echo "<input type='hidden' id='id_union' name='id_union' value='".$id_union."'>";
            }
            ?>
            <span class="text_persona_fisica">
              <div class="row">
                <div class="col-md-12">
                  <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que correspondan y nombre(s).</span>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Nombre(s):</label>
                  <input class="form-control" type="text" name="nombre" id="nombre" value="<?php if(isset($b)) echo $b->nombre ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno:</label>
                  <input class="form-control" type="text" name="apellido_paterno" id="apellido_paterno" value="<?php if(isset($b)) echo $b->apellido_paterno ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno:</label>
                  <input class="form-control" type="text" name="apellido_materno" id="apellido_materno" value="<?php if(isset($b)) echo $b->apellido_materno ?>">
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Nombre de identificación:</label>
                  <input class="form-control" type="text" name="nombre_identificacion" value="<?php if(isset($b)) echo $b->nombre_identificacion ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Autoridad que emite la identificación:</label>
                  <input class="form-control" type="text" name="autoridad_emite" value="<?php if(isset($b)) echo $b->autoridad_emite ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Número de identificación:</label>
                  <input class="form-control" type="text" name="numero_identificacion" value="<?php if(isset($b)) echo $b->numero_identificacion ?>">
                </div>
              </div>
              <div class="row">
                <div class="col-md-7 form-group">
                  <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
                  <select class="form-control" name="actividad_giro">
                    <?php foreach ($get_actividad as $item) { 
                      $select="";
                      if(isset($b) && $b->actividad_giro==$item->clave){ $select="selected"; }
                        echo "<option value='$item->clave' $select>$item->acitividad</option>";
                      } ?>
                  </select>
                </div>
              </div>    
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Género:</label>
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" checked name="genero" value="1" <?php if(isset($b) && $b->genero==1) echo "checked" ?>>
                          Masculino
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="genero" value="2" <?php if(isset($b) && $b->genero==2) echo "checked" ?>>
                          Femenino
                        </label>
                      </div>
                    </div>
                  </div>
                </div>  
              </div>
              <div class="row"> 
                <div class="col-md-4 form-group">
                  <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                  <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" value="<?php if(isset($b)) echo $b->fecha_nacimiento ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>País de nacimiento:</label><br>
                  <select class="form-control pais_1" name="pais_nacimiento" onclick="getpais(1)">
                    <?php if(isset($b)){
                        foreach ($get_pais as $p){
                          $select="";
                          if(isset($b) && $b->pais_nacimiento==$p->clave){ $select="selected"; }
                            echo "<option value='$p->clave' $select>$p->pais</option>";
                        }
                      }else{
                        echo "<option value='MX'>MEXICO</option>";
                      }
                    ?>
                    <!--<option value="MX">MEXICO</option>-->
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  <label>País de nacionalidad:</label><br>
                  <select class="form-control pais_2" name="pais_nacionalidad" onclick="getpais(2)">
                    <?php if(isset($b)){
                        foreach ($get_pais as $p){
                          $select="";
                          if(isset($b) && $b->pais_nacionalidad==$p->clave){ $select="selected"; }
                            echo "<option value='$p->clave' $select>$p->pais</option>";
                        }
                      }else{
                        echo "<option value='MX'>MEXICO</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>  
              <div class="row">
                <div class="col-md-12">
                  <h4>Dirección</h4>
                </div>
              </div>
              <div class="row">  
                <div class="col-md-2 form-group">
                  <label>Tipo de vialidad:</label>
                  <select class="form-control" name="tipo_vialidad">
                    <option disabled selected>Selecciona un tipo</option>
                    <?php foreach ($get_tipo_vialidad as $item){ 
                      $select="";
                      if(isset($b) && $b->tipo_vialidad==$item->id){ $select="selected"; }
                      echo "<option value='$item->id'$select>$item->nombre</option>";
                     } ?>
                  </select>
                </div>
                <div class="col-md-3 form-group">
                  <label>Calle:</label>
                  <input class="form-control" type="text" name="calle" value="<?php if(isset($b)) echo $b->calle ?>">
                </div>
                <div class="col-md-2 form-group">
                  <label>No.ext:</label>
                  <input class="form-control" type="text" name="no_ext" value="<?php if(isset($b)) echo $b->no_ext ?>">
                </div>
                <div class="col-md-2 form-group">
                  <label>No.int:</label>
                  <input class="form-control" type="text" name="no_int" value="<?php if(isset($b)) echo $b->no_int ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>C.P:</label>
                  <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="cp" value="<?php if(isset($b)) echo $b->cp ?>" id="cp">
                </div>
                <div class="col-md-3 form-group">
                  <label>Municipio o delegación:</label>
                  <input class="form-control" type="text" name="monicipio" value="<?php if(isset($b)) echo $b->monicipio ?>" id="municipio">
                </div>
                <div class="col-md-3 form-group">
                  <label>Localidad:</label>
                  <input class="form-control" type="text" name="localidad" value="<?php if(isset($b)) echo $b->localidad ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Estado:</label>
                  <input readonly="" type="hidden" name="estado" id="edo" class="estado_text_d_2" value="<?php if(isset($b)) echo $b->estado ?>">
                  <span class="span_div_3"></span>
                </div>
                <!--<div class="col-md-3 form-group">
                  <label>Colonia:</label>
                  <input class="form-control" type="text" name="colonia" value="<?php if(isset($b)) echo $b->colonia ?>">
                </div>-->
                <div class="col-md-3 form-group">
                  <label>Colonia:</label><br>
                  <select onclick="autoComplete(this.id)" class="form-control" name="colonia" id="colonia">
                  <?php if(isset($b) && $b->colonia!=""){
                    echo '<option value="'.$b->colonia.'" selected>'.$b->colonia.'</option>';      
                  }  ?>
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  <label>País:</label><br>
                  <select class="form-control pais_3" name="pais">
                    <?php if(isset($b)){
                        foreach ($get_pais as $p){
                          $select="";
                          if(isset($b) && $b->pais==$p->clave){ $select="selected"; }
                            echo "<option value='$p->clave' $select>$p->pais</option>";
                        }
                      }else{
                        echo "<option value='MX'>MEXICO</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">Tratándose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio.
                      <input type="checkbox" class="form-check-input" id="lugar_recidencia" name="trantadose_persona" onchange="lugar_recidencia_btn_paso3()" <?php if(isset($b) && $b->trantadose_persona!="off") echo "checked" ?>>
                    </label>
                  </div>
                </div>
              </div>
              <div class="residenciaEx" style="display: none">
                <div class="row">
                  <div class="col-md-12">
                    <span>Dirección en México.</span>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <label>Tipo de vialidad:</label>
                    <select class="form-control" name="tipo_vialidad_d">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ 
                        $select="";
                        if(isset($b) && $b->tipo_vialidad_d==$item->id){ $select="selected"; }
                          echo "<option value='$item->id' $select>$item->nombre</option>";

                        ?>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Calle:</label>
                    <input class="form-control" type="text" name="calle_d" value="<?php if(isset($b)) echo $b->calle_d ?>">
                  </div>
                  <div class="col-md-2 form-group">
                    <label>No.ext:</label>
                    <input class="form-control" type="text" name="no_ext_d" value="<?php if(isset($b)) echo $b->no_ext_d ?>">
                  </div>
                  <div class="col-md-2 form-group">
                    <label>No.int:</label>
                    <input class="form-control" type="text" name="no_int_d" value="<?php if(isset($b)) echo $b->no_int_d ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>C.P:</label>
                    <input class="form-control" onchange="cambiaCP2(this.id)" pattern="\d*" maxlength="5" type="text" name="cp_d" value="<?php if(isset($b)) echo $b->cp_d ?>" id="cp_d">
                  </div>
                  
                  <div class="col-md-3 form-group">
                    <label>Municipio o delegación:</label>
                    <input class="form-control" type="text" id="municipio_d" name="municipio_d" value="<?php if(isset($b)) echo $b->municipio_d ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Localidad:</label>
                    <input class="form-control" type="text" name="localidad_d" value="<?php if(isset($b)) echo $b->localidad_d ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Estado:</label><br>
                    <select class="form-control estado_fisica" name="estado_d" id="estado_d">
                      <?php foreach ($get_estado as $item) {
                        $select="";
                        if(isset($b) && $b->estado_d==$item->id){ $select="selected"; }
                          echo "<option value='$item->id' $select>$item->estado</option>";
                       } ?>
                    </select>
                  </div>
                  <!--<div class="col-md-3 form-group">
                    <label>Colonia:</label>
                    <input class="form-control" type="text" name="colonia_d" value="<?php if(isset($b)) echo $b->colonia_d ?>">
                  </div>-->
                  <div class="col-md-3 form-group">
                    <label>Colonia:</label><br>
                    <select onclick="autoComplete2(this.id)" class="form-control" name="colonia_d" id="colonia_d">
                      <option value=""></option>
                    <?php if(isset($b) && $b->colonia_d!=""){
                      echo '<option value="'.$b->colonia_d.'" selected>'.$b->colonia_d.'</option>';      
                      }  ?>
                     </select>
                  </div>
                </div>
                
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label>Correo electrónico:</label>
                  <input class="form-control" type="email" name="correo_d" value="<?php if(isset($b)) echo $b->correo_d ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>R.F.C:</label>
                  <input class="form-control rfc" type="text" name="r_f_c_d" value="<?php if(isset($b)) echo $b->r_f_c_d ?>">
                  <p>*Cuando cuente con R.F.C</p>
                </div>
                <div class="col-md-3 form-group">
                  <label>CURP:</label>
                  <input class="form-control" type="text" name="curp_d" value="<?php if(isset($b)) echo $b->curp_d ?>">
                  <p>*Cuando cuente con CURP</p>
                </div>
              </div>
            </span>
            <div class="firma_tipo_cliente">
              <br>
              <br>
              <div class="firma_tipo_cliente" style="display: none;">
                <div class="row">
                  <div class="col-md-12" style="text-align: justify !important;">
                    
                    <hr>
                    <center><span id="nombre_cli_imp"><?php echo $nombre; ?></span></center><br>
                      Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
                  </div>
                  <!--
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_ejec_imp"></span></center><br>
                      Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados  
                  </div>
                  -->  
                </div>
              </div>
            </div> 
            <hr class="subtitle">
            <div class="modal-footer barra_menu">
              <?php if(isset($b) && $b->id>0){ ?>
                <button type="button" class="btn gradient_nepal2" onclick="imprimir_formato1()"><i class="fa fa-print"></i> Imprimir formato conoce dueño beneficiario</button>
              <?php } ?>
              <button class="btn gradient_nepal2" id="btn_submit" type="submit"><i class="fa fa-save"></i> Guardar</button>
              <button class="btn gradient_nepal2" type="button" onclick="c_beneficiarios()">Regresar</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_fecha" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Fecha de formato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="tipo_con" readonly>
        <input type="hidden" id="id_cc" readonly>
        <input type="date" style="width: 100%" id="fecha_f" class="form-control">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>