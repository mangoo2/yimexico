<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=bitacora_benes".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>#</th>
          <th>Beneficiario</th>
          <th>Tipo de Documento</th>
          <th>Nombre de Documento</th>
          <th>Fecha de Registro</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
        $nombre="";
        foreach ($b as $key) {
          if($key->tipo_bene==1){
            $get_pf=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$key->id_beneficiario);
            foreach ($get_pf as $item) { 
              $bene = $item->nombre." ".$item->apellido_paterno." ".$item->apellido_materno;
            }
          }else if($key->tipo_bene==2){
            $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$idbm);
            foreach ($get_pm as $item) {
              if($item->identificacion_beneficiario=='m'){
                if($item->tipo_persona==2){
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id_bene_moral',$item->id);
                  foreach ($get_pmf as $item) {
                    $bene=$item->razon_social;
                  }
                }
              }
            }  
          }else if($key->tipo_bene==3){
            $get_pm=$this->ModeloCatalogos->getselectwhere('beneficiario_moral','id',$idbm);
            foreach ($get_pm as $item) {
              if($item->identificacion_beneficiario=='f'){
                if($item->tipo_persona==3){
                  $get_pmf=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id_bene_moral',$item->id);
                  foreach ($get_pmf as $item) {
                    $bene=$item->razon_social;
                  }
                }
              }
            }
          }

          if($key->tipo_doc=="formato_cc"){
            $nombre="Formato conoce a tu cliente";
          }
          if($key->tipo_doc=="ine"){
            $nombre="Identificación oficial";
          }
          if($key->tipo_doc=="cif"){
            $nombre="Cédula de identificación fiscal";
          }
          if($key->tipo_doc=="curp"){
            $nombre="CURP";
          }
          if($key->tipo_doc=="comprobante_dom"){
            $nombre="Comprobante de domicilio";
          }
          if($key->tipo_doc=="imgineal"){
            $nombre="Identificación oficial del apoderado legal";
          }
          if($key->tipo_doc=="esc_ints"){
            $nombre="Escritura constitutiva o instrumento público que acredite su existencia";
          }
          if($key->tipo_doc=="poder_not_fac_serv"){
            $nombre="Poderes Notariales o documento para comprobar las facultades del(os) servidor(es) público(s)";
          }
          if($key->tipo_doc=="doc extra"){
            $nombre="Documento extra";
          }
          
        	echo '
            <tr>
                <td >'.$key->id.'</td>
                <td >'.$bene.'</td>
                <td >'.$nombre.'</td>
                <td >'.$key->nombre_doc.'</td>
                <td >'.$key->fecha.'</td>';
          }
        	echo '</tr>';
        ?>
        
    </tbody>
</table>