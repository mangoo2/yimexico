<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=beneficiarios_de_mancomunados".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <h2>Cliente mancomunado: <?php echo $nombre; ?> </h2>
    <table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Beneficiario</th>
            <th>Tipo (unión)</th>
          </tr>
        </thead>
        <tbody>
        <?php
        $id_bene_aux=0;
        $tipo_bene_aux=0;
        $i=0;
        $html="";
        $get_bene_union=$this->ModeloCatalogos->getselectwherestatus2('*','beneficiario_union_cliente',array('id_union'=>$id_union,"activo"=>1),'tipo_bene');
        foreach ($get_bene_union as $k) {
            $id_union_aux=$k->id_union;
            $id_duenio_bene_aux=$k->id_duenio_bene;

            if($k->tipo_bene==1){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
                $tipo_bene_aux=$k->tipo_bene;
                $id_bene_aux=$k->id_duenio_bene;

                $data = $this->ModeloCliente->get_benef_control_f2('beneficiario_fisica',array("id_union"=>$id_union,"activo"=>1));

                foreach ($data as $key) { //para pintar las personas fisicas
                    $i++;
                    $html .= '<tr class="rowt_'.$i.'">
                    <td>
                      '.$key->nombre.' '.$key->apellido_paterno.' '.$key->apellido_materno.'
                    </td>
                    <td>Unión-Mancomunado</td>
                  </tr>';
                }
            }
            if($k->tipo_bene==2){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
                $tipo_bene_aux=$k->tipo_bene;
                $id_bene_aux=$k->id_duenio_bene;
                $datam = $this->ModeloCliente->get_benef_control_mfUnion($id_union,'beneficiario_moral_moral','beneficiario_moral_moral.id AS id_auni');
                foreach ($datam as $key) { //para pintar las personas morales_fisicas
                    $i++;
                    $html .= '<tr class="rowt_'.$i.'">
                    <td>
                      '.$key->razon_social.'
                    </td>
                    <td>Unión-Mancomunado</td>
                  </tr>';
                }
            }
            if($k->tipo_bene==3){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
                $tipo_bene_aux=$k->tipo_bene;
                $id_bene_aux=$k->id_duenio_bene;
                $datam = $this->ModeloCliente->get_benef_control_mfUnion($id_union,'beneficiario_fideicomiso','beneficiario_fideicomiso.id AS id_auni');

                foreach ($datam as $key) { //para pintar fideicomiso
                    $i++;
                    $html .= '<tr class="rowt_'.$i.'">
                    <td>
                      '.$key->razon_social.'
                    </td>
                    <td>Unión-Mancomunado</td>
                  </tr>';
                }
            }
            $tipo_bene_aux=$k->tipo_bene;
            $id_bene_aux=$k->id_duenio_bene;
        }
        $html.="</tbody>
        </table>";
    echo $html;