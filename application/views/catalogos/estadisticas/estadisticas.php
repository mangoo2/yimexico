<?php 
$perfilid=$this->session->userdata('perfilid'); 
$idcliente_usuario=$this->session->userdata('idcliente_usuario');
$menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,3);
//echo "idcliente_usuario: ".$idcliente_usuario;

?>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Consultas, bitácoras y estadisticas</h3>
          </div>
          <div class="col-md-4" align="right">
            <a href="<?php echo base_url() ?>Inicio_cliente"><button type="button" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</button></a>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>
        <hr class="subtitle">
        <div class="col-md-12" align="center">
          <?php 
            if($perfilid==3){
              echo '<button class="btn gradient_nepal tam_btn" onclick="href_bita_cliente()">
                      <div class="row texto_centro">
                        <div class="col-md-5" align="right">
                          <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-7" align="left">
                          <span>Bitácora Clientes</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;';
              echo '<button class="btn gradient_nepal tam_btn" onclick="href_consultas()"><!--  onclick="bitacora_transacciones()" -->
                <div class="row texto_centro">
                  <div class="col-md-3" align="right">
                    <i class="mdi mdi-currency-usd btn-icon-prepend icon_yi"></i>
                  </div>
                  <div class="col-md-9" align="left">
                    <span>Bitácora de Transacciones</span>
                  </div>
                </div>  
              </button>&nbsp;&nbsp;&nbsp;&nbsp;';
              echo '<button class="btn gradient_nepal tam_btn" onclick="href_avisos()">
                <div class="row texto_centro">
                  <div class="col-md-4" align="right">
                    <i class="fa fa-bullhorn btn-icon-prepend icon_yi"></i>
                  </div>
                  <div class="col-md-8" align="left">
                    <span>Avisos</span>
                  </div>
                </div>  
              </button>&nbsp;&nbsp;&nbsp;&nbsp';
              echo '<button class="btn gradient_nepal tam_btn" id="expedientes">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="mdi mdi-archive btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Expedientes</span>
                    </div>
                  </div>  
                </button>&nbsp;&nbsp;&nbsp;&nbsp';
              echo '<br><br><button class="btn gradient_nepal tam_btn">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="mdi mdi-chart-line btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Estadísticas</span>
                    </div>
                  </div>  
                </button>&nbsp;&nbsp;&nbsp;&nbsp';
              echo '<button class="btn gradient_nepal tam_btn" onclick="href_divisas()">
                        <div class="row texto_centro">
                          <div class="col-md-4">
                            <i class="fa fa-money icon_yi"></i>
                          </div>
                          <div class="col-md-8">
                            <span>Divisas</span>
                          </div>
                        </div>  
                      </button>&nbsp;&nbsp;&nbsp;&nbsp';

            
            }else{
              $cont=0;
              foreach ($menu as $x) { 
                $id_menusub = $x->id_menusub;
                $check_menu = $x->check_menu;
              }
              foreach ($menu as $x) { 
                $cont++;
                //echo "<br> x->id_menusub".$x->id_menusub;
                //echo "<br> x->check_menu".$x->check_menu;
                if($x->id_menusub==5){ // bita de clientes
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_bita_cliente()">
                            <div class="row texto_centro">
                              <div class="col-md-5" align="right">
                                <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-7" align="left">
                                <span>Bitácora Clientes</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                if($x->id_menusub==6){ // bita de transac
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_consultas()"><!--  onclick="bitacora_transacciones()" -->
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="mdi mdi-currency-usd btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Bitácora de Transacciones</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                /*if($x->id_menusub==7){ // Mis datos
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn" id="cata_inmueble">
                          <div class="row texto_centro">
                            <div class="col-md-3" align="right">
                              <i class="mdi mdi-city btn-icon-prepend icon_yi"></i>
                            </div>
                            <div class="col-md-9" align="left">
                              <span>Catálogo de Inmuebles</span>
                            </div>
                          </div>  
                        </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }*/
                if($id_menusub==14){ // Avisos
                  if($check_menu==1 && $cont==3){  
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_avisos()">
                      <div class="row texto_centro">
                        <div class="col-md-4" align="right">
                          <i class="fa fa-bullhorn btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-8" align="left">
                          <span>Avisos</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                if($x->id_menusub==7){ // Expedientes
                  if($x->check_menu==1){ 
                    echo '<button class="btn gradient_nepal tam_btn" id="expedientes">
                        <div class="row texto_centro">
                          <div class="col-md-4" align="right">
                            <i class="mdi mdi-archive btn-icon-prepend icon_yi"></i>
                          </div>
                          <div class="col-md-8" align="left">
                            <span>Expedientes</span>
                          </div>
                        </div>  
                      </button>&nbsp;&nbsp;&nbsp;&nbsp';
                  }
                }
                if($x->id_menusub==8){ // Mis datos
                  if($x->check_menu==1){
                    echo '<br><br><button class="btn gradient_nepal tam_btn">
                        <div class="row texto_centro">
                          <div class="col-md-4" align="right">
                            <i class="mdi mdi-chart-line btn-icon-prepend icon_yi"></i>
                          </div>
                          <div class="col-md-8" align="left">
                            <span>Estadísticas</span>
                          </div>
                        </div>  
                      </button>&nbsp;&nbsp;&nbsp;&nbsp';
                  }
                }
                
                if($x->id_menusub==13){ // Divisas
                  if($x->check_menu==1){ 
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_divisas()">
                          <div class="row texto_centro">
                            <div class="col-md-4">
                              <i class="fa fa-money icon_yi"></i>
                            </div>
                            <div class="col-md-8">
                              <span>Divisas</span>
                            </div>
                          </div>  
                        </button>&nbsp;&nbsp;&nbsp;&nbsp';
                  }
                }
                

              } //foreach menu 2
              
              
                /*if($x->id_menusub==10){ // Mis datos
                  if($x->check_menu==1){
                    echo '<button  style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="href_aviso_cero()">
                          <div class="row texto_centro">
                            <div class="col-md-3" align="right">
                              <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                            </div>
                            <div class="col-md-9" align="left">
                              <span>Aviso en ceros</span>
                            </div>
                          </div>  
                        </button>';
                  }
                }*/
              
            }
          ?>  
          
          
          <?php /*
          <?php if($perfilid==7){?><!-- 7-Responsable Cumplimiento -->
            <button class="btn gradient_nepal tam_btn" onclick="href_mis_usuarios()">
              <div class="row texto_centro">
                <div class="col-md-5" align="right">
                  <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-7" align="left">
                  <span>Bitácora Usuarios</span>
                </div>
              </div>  
            </button>&nbsp;&nbsp;&nbsp;&nbsp;
          <?php } ?> 
          
          <!-- 6 Director General --><!-- 7-Responsable Cumplimiento --><!-- 8 Supervisor --><!-- 9-Ejecutivo Comercial --><!-- 10-Ejecutivo de Operación --><!-- 10-Director General -->   
          <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==8 || $perfilid==9 || $perfilid==10){?>
            <button class="btn gradient_nepal tam_btn" onclick="href_bita_cliente()">
              <div class="row texto_centro">
                <div class="col-md-5" align="right">
                  <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-7" align="left">
                  <span>Bitácora Clientes</span>
                </div>
              </div>  
            </button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn gradient_nepal tam_btn" onclick="href_consultas()"><!--  onclick="bitacora_transacciones()" -->
              <div class="row texto_centro">
                <div class="col-md-3" align="right">
                  <i class="mdi mdi-currency-usd btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-9" align="left">
                  <span>Bitácora de Transacciones</span>
                </div>
              </div>  
            </button>&nbsp;&nbsp;&nbsp;&nbsp;
          <?php } ?> 
          <?php //if($perfilid==7){?><!-- 7-Responsable Cumplimiento -->
            <button class="btn gradient_nepal tam_btn" id="cata_inmueble">
              <div class="row texto_centro">
                <div class="col-md-3" align="right">
                  <i class="mdi mdi-city btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-9" align="left">
                  <span>Catálogo de Inmuebles</span>
                </div>
              </div>  
            </button>&nbsp;&nbsp;&nbsp;&nbsp;
          <?php //} ?> 
          <?php if($perfilid==7){ /*?><!-- 7-Responsable Cumplimiento -->
            <button class="btn btn-outline-dark btn-icon-text" onclick="">
              <i class="mdi mdi-note-multiple-outline btn-icon-prepend mdi-36px"></i><br>
              <span class="d-inline-block text-left">
                <h4 align="center">Bitácora XMLS / Acuses</h4>
              </span>
            </button>
            <hr>
          <?php */ /*} ?> 
          <!-- 6 Director General --><!-- 7-Responsable Cumplimiento --><!-- 8 Supervisor --><!-- 9-Ejecutivo Comercial -->  <!-- 10-Ejecutivo de Operación --> 
          <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==8 || $perfilid==9 || $perfilid==10){?>
            <!--
            <button class="btn btn-outline-dark btn-icon-text" onclick="">
              <i class="mdi mdi-note-multiple-outline btn-icon-prepend mdi-36px"></i><br>
              <span class="d-inline-block text-left">
                <h4 align="center">Bitácora Expedientes</h4>
              </span>
            </button>
            -->
            <button class="btn gradient_nepal tam_btn">
              <div class="row texto_centro">
                <div class="col-md-4" align="right">
                  <i class="mdi mdi-chart-line btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-8" align="left">
                  <span>Estadísticas</span>
                </div>
              </div>  
            </button>
          <?php } ?> 
         */?>
        </div><br> 
      </div>
    </div>
  </div>
</div>          