<?php $perfilid=$this->session->userdata('perfilid'); ?>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Consultas, bitácoras y estadisticas</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>
        <hr class="subtitle">
        <div class="col-md-12" align="center">

            <!--<button class="btn gradient_nepal tam_btn" onclick="href_aviso_cero()">
              <div class="row texto_centro">
                <div class="col-md-3" align="right">
                  <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-9" align="left">
                  <span>Aviso en cero</span>
                </div>
              </div>  
            </button>-->
            <button class="btn gradient_nepal tam_btn" onclick="bitacora_transacciones()">
              <div class="row texto_centro">
                <div class="col-md-3" align="right">
                  <i class="mdi mdi-currency-usd btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-9" align="left">
                  <span>Operaciones de cliente</span>
                </div>
              </div>  
            </button>

        </div><br> 
      </div>
    </div>
  </div>
</div>          