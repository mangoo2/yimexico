<?php 
  $perfilid=$this->session->userdata('perfilid'); 
  $idcliente_usuario=$this->session->userdata('idcliente_usuario');
  $menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,4);
?>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Avisos</h3>
          </div>
          <div class="col-md-4" align="right">
            <a href="<?php echo base_url() ?>Estadisticas"><button type="button" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</button></a>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>    
        <hr class="subtitle">
        <div class="col-md-12" align="center">
          <input id="idcliente" type="hidden" value="<?php echo $idcliente; ?>">
          <?php 
            if($perfilid==3){
              echo '<a href="'.base_url().'Estadisticas/aviso_cero" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Aviso en ceros y Avisos exentos</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
              echo '<a href="'.base_url().'Estadisticas/avisos_generados" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-check-square-o btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Avisos Generados</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
              echo '<a href="'.base_url().'Estadisticas/avisos_modificatorios" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-edit btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Avisos Modificatorios</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
              echo '<br><br><a href="'.base_url().'Estadisticas/avisos_24hrs" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-clock-o btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Avisos 24 horas</span>
                        </div>
                      </div>  
                    </button></a>';
              
            }else{  
              /*if($x->id_menusub==10){ // Mis datos
                if($x->check_menu==1){
                  echo '<a href="'.base_url().'Estadisticas/aviso_cero" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Aviso en ceros y Avisos exentos</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;&nbsp;&nbsp;';
                }
              }*/
              echo '<a href="'.base_url().'Estadisticas/aviso_cero" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Aviso en ceros y Avisos exentos</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
              echo '<a href="'.base_url().'Estadisticas/avisos_generados" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-check-square-o btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Avisos Generados</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
              echo '<a href="'.base_url().'Estadisticas/avisos_modificatorios" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-edit btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Avisos Modificatorios</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
              echo '<br><br><a href="'.base_url().'Estadisticas/avisos_24hrs" <button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-clock-o btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Avisos 24 horas</span>
                        </div>
                      </div>  
                    </button></a>';
            }
            
            ?>        

        </div><br> 
      </div>
    </div>
  </div>
</div>
