<span class="mes_actual" hidden=""><?php echo $mes_a ?></span>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3 class="">Bitácora de Avisos Modificatorios</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>  
        <hr class="subtitle">
        <div class="row">
          <input type="hidden" id="id_cliente" value="<?php echo $this->session->userdata('idcliente'); ?>">

          <div class="col-lg-2">
            <label>Año:</label>
            <select class="form-control" id="ano" onchange="load()">
            </select>
          </div>
          <div class="col-lg-2">
            <label>Mes reportado:</label>
            <select class="form-control" id="mes" onchange="load()">
              <option value="00">Todos</option>
              <option value="01">Enero</option>
              <option value="02">Febrero</option>
              <option value="03">Marzo</option>
              <option value="04">Abril</option>
              <option value="05">Mayo</option>
              <option value="06">Junio</option>
              <option value="07">Julio</option>
              <option value="08">Agosto</option>
              <option value="09">Septiembre</option>
              <option value="10">Octubre</option>
              <option value="11">Noviembre</option>
              <option value="12">Diciembre</option>
            </select>
          </div>
          <div class="col-lg-6">
            <label>Clientes:</label>
            <select class="form-control" id="cliente" onchange="load()">
              <option value="0-0-0-0">Todos</option>
              <?php foreach ($clientes_c as $item) { 
                  $html_cliente='';
                  if($item->idtipo_cliente==1){
                     $html_cliente=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
                  }else if($item->idtipo_cliente==2){
                     $html_cliente=$item->nombre2.' '.$item->apellido_paterno2.' '.$item->apellido_materno2;
                  }else if($item->idtipo_cliente==3){
                     $html_cliente=$item->razon_social;
                  }else if($item->idtipo_cliente==4){
                     $html_cliente=$item->nombre_persona;
                  }else if($item->idtipo_cliente==5){
                     $html_cliente=$item->denominacion2;
                  }else if($item->idtipo_cliente==6){
                     $html_cliente=$item->denominacion;
                  }
                  ?>
                <option value="<?php echo $item->idtipo_cliente.'-'.$item->idperfilamientop.'-'.$item->idtipo_cliente.'-0' ?>"><?php echo $html_cliente ?></option>
              <?php }
                foreach ($cli_union as $item) {  
                  if($item->cliente!=null){
                    echo '<option value="'.$item->id_cliente_tipo.'-'.$item->idperfilamientop.'-'.$item->idtipo_cliente.'-'.$item->id_union.'">'.$item->cliente.'</option>';
                    //echo '<option value="'.$item->id_cliente_tipo.'-'.$item->idperfilamientop.'-'.$item->idtipo_cliente.'-'.$item->id_union.'">'.$item->cliente.'</option>';
                  }
                }
              ?>
            </select>
          </div>
          <div class="col-lg-5" style="display: none">
            <label>Actividad:</label>
            <select class="form-control" id="actividad" onchange="load()">
              <?php foreach ($act as $av) {
                  echo "<option value='$av->idactividad'>$av->nombre</option>";
              } ?>
            </select>
          </div>
        </div>
        <br>
        <div class="table-responsive">
          <span class="tabla_transaccion"></span>
        </div>
      </div>
    </div>
  </div>
</div>