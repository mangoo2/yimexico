<span class="mes_actual" hidden=""><?php echo date("m"); ?></span>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3 class="">Bitácora de Avisos en 0 y avisos exentos</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>  
        <hr class="subtitle">
        <div class="row">
          <div class="col-lg-2">
            <label>Año:</label>
            <select class="form-control" id="anio" onchange="reload_table()">
            </select>
          </div>
          <div class="col-lg-2">
            <label>Actividad:</label>
            <select class="form-control" id="actividad" onchange="reload_table()">
              <?php foreach ($actividades as $av) {
                  echo "<option value='$av->idactividad'>$av->nombre</option>";
              } ?>
            </select>
          </div>
          <div class="col-lg-2">
            <label>Tipo</label>
            <select class="form-control" id="tipo_aviso" onchange="reload_table()">
              <option value="0">Exento</option>
              <option value="1">En 0</option>
            </select>
          </div>
        </div>
        <br>
        <div class="table-responsive">
          <table class="table" id="table_aviso">
            <thead>
              <tr>
                <th>No</th>
                <th>Mes reportado</th>
                <th>Tipo</th>
                <th>XML</th>
                <th>Acuse SAT</th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>