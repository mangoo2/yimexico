<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h3 >Nuevo usuario general</h3>
                  <hr class="subtitle">
                  <form class="form" method="post" role="form" id="form_empleado" autocomplete="off">
                    <input type="hidden" name="personalId" value="">
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <label>Nombre(S)</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Apellido paterno</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Apellido materno</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Función/Puesto</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                    </div>
                    <div class="row">  
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>Dirección</h4>
                      </div>
                      <div class="col-md-2 form-group">
                        <label>Tipo de vialidad</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Calle</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.ext</label>
                        <input class="form-control" type="number" name="" value="">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>No.int</label>
                        <input class="form-control" type="number" name="" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Colonia</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <label>Municipio o Delegación</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Localidad</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-2 form-group">
                        <label>Estado</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>País</label>
                        <select class="form-control idpais">
                          <option value="MX">MEXICO</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>Acceso a sistema</h4>
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Usuario</label>
                        <input class="form-control" type="text" name="" value="">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Contraseña</label>
                        <input class="form-control" type="password" name="" value="">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Vereficar Contraseña</label>
                        <input class="form-control" type="password" name="" value="">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Perfil</label>
                        <select class="form-control">
                          <option>Perfil Ejemplo 1</option>
                          <option>Perfil Ejemplo 2</option>
                          <option>Perfil Ejemplo 3</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Sucursal</label>
                        <select class="form-control">
                        </select>
                      </div>
                    </div>
                      <a class="btn btn-secondary" href="<?php echo base_url(); ?>Personal">Regresar</a>
                      <button type="button" class="btn btn-yimexico"><i class="fa  fa-floppy-o"></i> Guardar</button>
                      </form>
                    </div>
                    </div>
              </div>
            </div>
<div class="modal fade" id="modalpass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #ff5200; color: white;">
        <h4 class="modal-title" id="exampleModalLabel-2">Generar Contraseña</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-6">
          <div class="form-group" align="center">
            <br>
            <input type="text" id="pass" class="form-control">
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group" align="center">
            <br>
            <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="generarpass()">General</button>
          </div>
        </div>
      </div>
      <input type="hidden" id="personalId">
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-rounded btn-fw" data-dismiss="modal" onclick="addpass()">Aceptar</button>
        <button type="button" class="btn btn-secondary btn-rounded btn-fw" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalpuesto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #ff5200; color: white;">
        <h4 class="modal-title" id="exampleModalLabel-2">Nuevo puesto</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <div class="form-group" align="center">
            <br>
            <input type="text" id="puesto" class="form-control">
          </div>
        </div>
        <div class="col-md-1">
        </div>
      </div>
      <input type="hidden" id="personalId">
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-rounded btn-fw" data-dismiss="modal" onclick="nuevopuesto()">Aceptar</button>
        <button type="button" class="btn btn-secondary btn-rounded btn-fw" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
