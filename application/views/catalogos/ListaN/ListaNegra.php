<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #24284b;
  }
</style>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h4>Lista de bloqueados</h4>
          </div>
        </div>
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-12">
            <input type="hidden" id="ColoniPL" value="<?php echo $colonia; ?>">
            <form id="maleantes">
              <div id="general" class="row col-md-12">
                <div class="col-md-6 form-group">
                  <label>Razón</label>
                  <select class="form-control" id="razonlista" name="razonlista">
                    <option value="2" <?php if ($razonlista==2){echo "selected";} ?> >Delicuencial</option>
                    <option value="1" <?php if ($razonlista==1){echo "selected";} ?> >Persona politícamente expuesta</option>
                  </select>
                </div>
                <div class="col-md-6 form-group">
                  <label id="nomp">Nombre o Entidad</label>
                  <input type="hidden" id="id_lista" name="id_ln" value="<?php echo $id; ?>" >
                  <input class="form-control" type="text" name="nombre" value="<?php echo $nombre; ?>">
                </div>
              </div>
              <div id="pep" class="row col-md-12">
                <div class="col-md-6 form-group">
                  <label>Apellido Paterno</label>
                  <!--<input type="hidden" id="id_lista" name="id_ln" value="<?php echo $id;?>">-->
                  <input class="form-control" type="text" name="apellido_p" value="<?php echo $apellido_p; ?>">
                </div>
                <div class="col-md-6 form-group">
                  <label>Apellido Materno</label>
                  <!--<input type="hidden" id="id_lista" name="id_ln" value="<?php echo $id; ?>">-->
                  <input class="form-control" type="text" name="apellido_m" value="<?php echo $apellido_m; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Puesto</label>
                  <input class="form-control" type="text" name="puesto" value="<?php echo $puesto; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Dependencia</label>
                  <input class="form-control" type="text" name="dependencia" value="<?php echo $dependencia; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Año de servcio</label>
                  <input class="form-control" type="text" name="anioser" value="<?php echo $anioser; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>RFC</label>
                  <input class="form-control" id="rfc" type="text" name="rfc" value="<?php echo $rfc; ?>">
                </div>
                <div class="col-md-2 form-group">
                  <label>CP</label>
                  <input class="form-control" type="text" id="cpv" name="cp" value="<?php echo $cp; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Calle</label>
                  <input class="form-control" type="text" name="calle" value="<?php echo $calle; ?>">
                </div>
                <div class="col-md-2 form-group">
                  <label>No. ext</label>
                  <input class="form-control" type="text" name="next" value="<?php echo $next; ?>">
                </div>
                <div class="col-md-2 form-group">
                  <label>No. int</label>
                  <input class="form-control" type="text" name="nint" value="<?php echo $nint; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Colonia</label>
                  <select class="form-control" id="colonia" name="colonia">
                    
                  </select>
                </div>
                <div class="col-md-3 form-group">
                  <label>Municipio o delegación</label>
                  <input class="form-control" type="text" readonly id="mun" name="municipio" value="<?php echo $municipio; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Localidad</label>
                  <input class="form-control" type="text" readonly id="loc" name="localidad" value="<?php echo $localidad; ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Estado</label>
                  <select class="form-control" id="estado" readonly name="estado">
                    <?php foreach ($estados as $key): $sel=""; if($key->id==$estado){ $sel="selected"; } ?>
                      <option <?php echo $sel; ?> value="<?php echo $key->id; ?>"><?php echo $key->estado; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                <!--<div class="col-md-6 form-group">
                  <label>Dirección</label>
                  <input class="form-control" type="text" name="direccion" value="<?php echo $direccion; ?>">
                </div>--->
              </div>
              <div id="delicuencial" class="row col-md-12">
                <div class="col-md-6 form-group">
                  <label>Liga de idetificación</label>
                  <input class="form-control" type="text" name="ligaindentificacion" value="<?php echo $ligaindentificacion; ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Lista donde se encuentra</label>
                  <select class="form-control" name="listaecuentra">
                    <option value="GAFI"  <?php if ($listaecuentra=="GAFI"){echo "selected";} ?>>GAFI</option>
                    <option value="UIF"  <?php if ($listaecuentra=="UIF"){echo "selected";} ?>>UIF</option>
                    <option value="INTERPOL"  <?php if ($listaecuentra=="INTERPOL"){echo "selected";} ?>>INTERPOL</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <br>
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <?php $ch=""; if($entidad==1){ $ch="checked"; } ?>
                    <input type="checkbox" <?php echo $ch; ?> class="form-check-input" id="entidad" name="entidad">
                    Entidad</label>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6" id="alias">
            <form id="form-alias-0">
              <div class="row" id="aliases">
                <div class="col-md-10 form-group">
                  <input type="hidden" name="id" id="id" value="0">
                  <label>Alias</label>
                  <input class="form-control" type="text" name="alias" id="alias">
                </div>
                <div class="col-md-1">
                  <br>
                  <button type="button" class="btn gradient_nepal2" onclick="NuevoAlias(0,'')"><i class="fa fa-plus"></i></button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6" id="direc">
            <form id="directions-0">
              <div class="row" id="directions">
                <div class="col-md-10 form-group">
                  <label>Dirección</label>
                  <input type="hidden" name="id" value="0">
                  <textarea class="form-control" type="text" name="direccion"></textarea>
                </div>
                <div class="col-md-1">
                  <br>
                  <button type="button" class="btn gradient_nepal2" onclick="NuevaDireccion()"><i class="fa fa-plus"></i></button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6" id="fechanac">
            <form id="nacimiento-0">
              <div class="row">
                <div class="col-md-2 form-group">
                  <label>Día</label>
                  <input type="hidden" name="id" id="id" value="0">
                  <input class="form-control" type="number" name="dia" id="dia">
                </div>
                <div class="form-group col-md-3">
                  <label>Mes</label>
                  <select class="form-control" name="mes" id="mes">
                    <option value="Enero">Enero</option>
                    <option value="Febrero">Febrero</option>
                    <option value="Marzo">Marzo</option>
                    <option value="Abril">Abril</option>
                    <option value="Mayo">Mayo</option>
                    <option value="Junio">Junio</option>
                    <option value="Julio">Julio</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Septiembre">Septiembre</option>
                    <option value="Octubre">Octubre</option>
                    <option value="Noviembre">Noviembre</option>
                    <option value="Diciembre">Diciembre</option>
                  </select>
                </div>
                <div class="col-md-2 form-group">
                  <label>Año</label>
                  <input class="form-control" type="number" name="anio" id="anio">
                </div>
                <div class="col-md-2">
                  <br>
                    <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" value="1" id="exacta" name="exacta">
                     Exacta
                    </label>
                </div>
                <div class="col-md-2">
                  <br>
                  <button type="button" class="btn gradient_nepal2" onclick="NuevoNacimiento()"><i class="fa fa-plus"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
        <div class="modal-footer">
          <a class="btn gradient_nepal2" type="button" href="<?php echo base_url() ?>ListaNegra" >Regresar</a>
          <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="registrar()"><i class="fa fa-save"></i> Guardar</button>
        </div>
    </div>
  </div>
</div>