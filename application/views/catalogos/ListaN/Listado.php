<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-10">  
            <h4>Lista de bloqueados</h4>
          </div>  
          <div class="col-md-2" align="right">
            <a type="button" class="btn gradient_nepal2" href="<?php echo base_url(); ?>ListaNegra/Alta">Agregar</a>
          </div>
        </div>
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-2">
            <select id="Filtrar" class="form-control">
              <option value="1">Persona politicamente expuesta</option>
              <option value="2">Delicuencial</option>
            </select>
          </div>
        </div>
        <br>
        <div class="row">  
          <div class="col-md-12">
            <table class="table" id="table">
              <thead>
                <th>#</th>
                <th>Nombre</th>
                <th id="th1">Liga de idetificación</th>
                <th id="th2">Lista</th>
                <th>Razón</th>
                <th>.</th>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>