<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #313555;
  }
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<input type="hidden" id="aux_pga" value="<?php echo $aux_pga ?>">
<input type="hidden" id="id_ax5a" value="<?php echo $id ?>">
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <h4>Anexo 5A - Servicios de construcción o desarrollo inmobiliario</h4>
            <h3>Registro de transacción</h3>
          </div>
        </div>
      	<hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <form id="form_anexo5a" class="forms-sample">
          <input type="hidden" name="id" value="<?php echo $id ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <!-- -->  
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha de operación: Fecha de recepción de recursos</label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" value="<?php echo $referencia ?>" placeholder="Ingrese número de referencia">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group" style="display:none">
              <label>Monto de la factura sin IVA ni accesorios</label>
              <input type="text" id="monto_opera" class="form-control monto_1" onchange="tipo_modeda(1)" placeholder="Ingrese el monto total de la operación ($)" required="" value="<?php echo $monto_opera ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-4 form-group" style="display:none">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" value="<?php echo $num_factura ?>" placeholder="Ingrese número de factura">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!----------->
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <!-- -->
          <div class="row">
            <?php if($aviso==1){ ?>
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div>   
            </div>
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="idanexo5a" id="idanexo5a" class="form-control" value="<?php echo $idanexo5a ?>">
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div>   
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90"><?php echo $descrip_modifica ?></textarea>
            </div>
            <?php }?>
          </div>   
          <!----------->
          <hr class="subsubtitle">
          <div class="row">
            <div class="col-md-12">
              <h4>Tipo de operación: <span>Compra Venta de Inmuebles</span></h4>
            </div>
          </div>
          <h4>Datos de operación</h4>
          <div class="row">
            <div class="col-md-6 form-group">
              <label>Figura del Cliente reportado en el aviso en la operación</label>
              <select class="form-control" name="figura_cliente">
                <option value="1" <?php if($figura_cliente==1) echo 'selected' ?>>Vendedor</option>
                <option value="2" <?php if($figura_cliente==2) echo 'selected' ?>>Comprador</option>
              </select>
            </div>
            <div class="col-md-6 form-group">
              <label>Figura de la persona que realiza la Actividad Vulnerable</label>
              <select class="form-control" name="figura_so" id="figura_so" onchange="select_comptraparte()">
                <option value="1" <?php if($figura_so==1) echo 'selected' ?>>Vendedor</option>
                <option value="2" <?php if($figura_so==2) echo 'selected' ?>>Comprador</option>
                <option value="3" <?php if($figura_so==3) echo 'selected' ?>>Intermediario</option>
              </select>
            </div>
          </div>
          <hr class="subsubtitle">
          <div class="contraparte_txt" style="display: none">
            <h4>Datos de la contraparte que interviene en la operación cuando la persona que realiza la Actividad Vulnerable es Intermediario</h4>
            <div class="row">
              <div class="col-md-4 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="contraparte" id="contraparte1" value="1" onclick="select_contraparte()" <?php if($contraparte==1) echo 'checked'?>>
                    Persona Fisica
                  </label>
                </div>
              </div>
            </div>
            <div class="fisica_p" style="display: none">
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Nombre(s)</label>
                  <input type="text" name="nombref" class="form-control" value="<?php echo $nombref ?>" >
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno</label>
                  <input type="text" name="apellido_paternof" class="form-control" value="<?php echo $apellido_paternof ?>" >
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno</label>
                  <input type="text" name="apellido_maternof" class="form-control" value="<?php echo $apellido_maternof ?>" >
                </div>
                <div class="col-md-3 form-group">
                  <label>Fecha de nacimiento</label>
                  <input type="date" name="fecha_nacimientof" class="form-control" value="<?php echo $fecha_nacimientof ?>" >
                </div>
                <div class="col-md-3 form-group">
                  <label>País de nacionalidad</label><br>
                  <select class="form-control idpais_f" name="pais_nacionalidadf">
                    <option value="MX">MEXICO</option>
                  </select>
                </div>
                <div class="col-md-3 form-group">
                  <label>RFC</label>
                  <input type="text" name="rfcf" onblur="ValidRfc(this)" class="form-control" value="<?php echo $rfcf ?>" >
                </div>
                <div class="col-md-3 form-group">
                  <label>CURP</label>
                  <input type="text" name="curpf" onblur="ValidCurp(this)" class="form-control" value="<?php echo $curpf ?>" >
                </div>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-4 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="contraparte" id="contraparte2" onclick="select_contraparte()" <?php if($contraparte==2) echo 'checked'?>>
                    Persona Moral
                  </label>
                </div>
              </div>
            </div> 
            <div class="moral_p" style="display: none">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label>Denominación o Razón Social</label>
                  <input type="text" name="denominacion_razonm" class="form-control" value="<?php echo $denominacion_razonm ?>" >
                </div>
                <div class="col-md-3 form-group">
                  <label>Fecha de Constitución</label>
                  <input type="date" name="fecha_constitucionm" class="form-control" value="<?php echo $fecha_constitucionm ?>" >
                </div>
                <div class="col-md-3 form-group">
                  <label>Registro Federal de Contribuyentes (RFC)</label>
                  <input type="text" name="rfcm" onblur="ValidRfc(this)" class="form-control" value="<?php echo $rfcm ?>" >
                </div>
                <div class="col-md-3 form-group">
                  <label>Clave país de nacionalidad</label>
                  <select class="form-control idpais_m" name="pais_nacionalidadm">
                    <option value="MX">MEXICO</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="contraparte" id="contraparte3" onclick="select_contraparte()" <?php if($contraparte==3) echo 'checked'?>>
                    Fideicomiso
                  </label>
                </div>
              </div>
            </div> 
            <div class="fideicomiso_p" style="display: none;">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label>Denominación o Razón Social del Fiduciario</label>
                  <input type="text" name="denominacion_razonfi" class="form-control" value="<?php echo $denominacion_razonfi ?>" >
                </div>
                <div class="col-md-6 form-group">
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                  <input type="text" name="rfcfi" onblur="ValidRfc(this)" class="form-control" value="<?php echo $rfcfi ?>" >
                </div>
                <div class="col-md-6 form-group">
                  <label>Número, referencia o identificador del fideicomiso</label>
                  <input type="text" name="identificador_fideicomisofi" class="form-control" value="<?php echo $identificador_fideicomisofi ?>" >
                </div>
              </div>
            </div>
            <hr class="subsubtitle">
          </div>
          <div class="row"> 
            <div class="col-md-12">
              <h5>Características del inmueble objeto del aviso</h5>
            </div>
            <div class="col-md-6 form-group">
              <label>Tipo de bien inmueble</label>
              <select class="form-control" name="tipo_inmueble">
                <?php foreach ($tin->result() as $item){?>
                  <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmueble) echo 'selected' ?>><?php echo $item->nombre ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-6 form-group">
              <label>Valor pactado del inmueble en Moneda Nacional</label>
              <input type="text" name="valor_pactado" onblur="MonFor(this.value)" class="form-control" value="<?php echo $valor_pactado ?>">
            </div>
             <div class="col-md-3 form-group">
              <label>Codigo postal</label>
              <input type="text" name="codigo_postal" id="codigo_postal" onchange="cambiaCP(this.id)" pattern='\d*' maxlength="5" class="form-control" value="<?php echo $codigo_postal ?>" >
            </div>
            
            <div class="col-md-3 form-group">
              <label>Calle</label>
              <input type="text" name="calle" class="form-control" value="<?php echo $calle ?>" >
            </div>
            <div class="col-md-1 form-group">
              <label>No. ext</label>
              <input type="text" name="numero_exterior" class="form-control" value="<?php echo $numero_exterior ?>" >
            </div>
            <div class="col-md-1 form-group">
              <label>No. int</label>
              <input type="text" name="numero_interior" class="form-control" value="<?php echo $numero_interior ?>" >
            </div>
            <div class="col-md-4 form-group">
             <label>Colonia: </label>
              <select onclick="autoComplete(this.id)" class="form-control" name="colonia" id="colonia">
                <option value="<?php echo $colonia; ?>" selected><?php echo $colonia; ?></option>      
              </select>
            </div>
            
            <div class="col-md-4 form-group">
              <label>Dimensiones del inmueble en m2 del terreno</label>
              <input type="text" name="dimension_terreno" class="form-control" value="<?php echo $dimension_terreno ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Dimensiones del inmueble en m2 Construidos</label>
              <input type="text" name="dimension_construido" class="form-control" value="<?php echo $dimension_construido ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Folio real del inmueble o antecedentes registrales</label>
              <input type="text" name="folio_real" class="form-control" value="<?php echo $folio_real ?>" >
            </div>
            <div class="col-md-12">
              <hr class="subsubtitle">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h5>¿La operación se realizó por medio de un contrato privado o Instrumento Público?</h5>
            </div>
          </div>
          <div class="row">  
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" checked name="instrumento" id="instrumento1" value="1" onclick="select_instrumento()" <?php if($instrumento==1) echo 'checked' ?>>
                  Instrumento Público
                </label>
              </div>
            </div>
          </div>
          <div class="instrumento_publico" style="display: none">
            <div class="row">
              <div class="col-md-6 form-group">
                <label>Número del Instrumento Público</label>
                <input type="text" name="numero_instrumento_publico" class="form-control" value="<?php echo $numero_instrumento_publico ?>" >
              </div>
              <div class="col-md-6 form-group">
                <label>Fecha del Instrumento Público</label>
                <input type="date" name="fecha_instrumento_publico" class="form-control" value="<?php echo $fecha_instrumento_publico ?>" >
              </div>
              <div class="col-md-6 form-group">
                <label>Número del notario del Instrumento Público</label>
                <input type="text" name="notario_instrumento_publico" class="form-control" value="<?php echo $notario_instrumento_publico ?>" >
              </div>
              <div class="col-md-6 form-group">
                <label>Clave de la Entidad Federativa del Notario Público</label>
                <select name="notario_publico" class="form-control">
                  <?php foreach ($estado->result() as $item){ ?>
                    <option value="<?php echo $item->id ?>"><?php echo $item->estado ?></option>  
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-6 form-group">
                <label>Valor avalúo o valor catastral del inmueble</label>
                <input type="text" id="valor_avaluo_catastral" class="form-control monto_123" onchange="tipo_modeda(123)" value="<?php echo $valor_avaluo_catastral ?>" >
              </div>
            </div>  
          </div>    
          <div class="row">
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="instrumento" id="instrumento2" value="2" onclick="select_instrumento()" <?php if($instrumento==2) echo 'checked' ?>>  
                  Contrato privado
                </label>
              </div>
            </div>
          </div>
          <div class="instrumento_privado" style="display: none">
            <div class="row"> 
              <div class="col-md-6 form-group">
                <label>Fecha del contrato</label>
                <input type="date" name="fecha_contrato" class="form-control" value="<?php echo $fecha_contrato ?>" >
              </div>
            </div> 
          </div>
        </form> 
        <hr class="subsubtitle">
          <h5>Datos de liquidación de la operación o acto</h5>
          <div class="liquidacion_pago">
            <div class="liquidacion_pago_text">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total de la liquidación</label>
              <input readonly="" class="form-control" type="text" id="total_liquida" value="$0.00">
            </div>
            <div class="col-md-8 form-group">
              <br><br>
              <h5 class="validacion_cantidad" style="color: red"></h5>
            </div>
          </div>
          <div class="row"> 
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total en efectivo</label>
              <input readonly="" class="form-control" type="text" id="total_liquida_efect" value="$0.00">
              <div class="col-md-8" align="center">
              <br><br>
                <h5 class="limite_efect_msj" style="color: red"></h5>

              </div>
            </div>
          </div> 
      <div class="modal-footer">
        <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
        <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
      </div>
      <div id="settings-trigger" style="width: 204px; left: 70px;" class="settings-trigger2"> 
          <button type="button" id="agregar_liquida" class="btn gradient_nepal2" onclick="agregar_liqui()"><i class="fa fa-plus"></i> Agregar Liquidación</button>
        </div>
    	</div>
    </div>
	</div>
</div>

<!-- Operaciones -->
<!--------------Modal-------------->
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 carácteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- / -->

<!------ Modificatorio ------->
<!--- --->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- < / >--->