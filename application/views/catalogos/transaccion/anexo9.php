<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<input type="hidden" id="aux_pga" value="<?php echo $aux_pga ?>">
<input type="hidden" id="id_ax9" value="<?php echo $id ?>">
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <h4>Anexo 9 - Blindaje.</h4>  
            <h3>Registro de transacción</h3>    
          </div>
        </div>
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <input type="hidden" id="aux_pga" value="<?php echo $aux_pga ?>">
        <form id="form_anexo9" class="forms-sample">
          <input type="hidden" name="id" value="<?php echo $id ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <!-- -->  
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha de operación: Fecha de recepción de recursos</label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" value="<?php echo $referencia ?>" placeholder="Ingrese número de referencia">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group">
              <label>Monto de la factura sin IVA ni accesorios</label>
              <input type="text" id="monto_opera" class="form-control monto_1" onchange="tipo_modeda(1)" placeholder="Ingrese el monto total de la operación ($)" required="" value="<?php echo $monto_opera ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" value="<?php echo $num_factura ?>" placeholder="Ingrese número de factura">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-8 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-4 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
         
            <?php if($aviso==1){ ?>
          <hr class="subsubtitle">
          <div class="row">  
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>

            <div class="col-md-2 form-group">
            </div>

            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div>   
            </div>
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="idanexo9" id="idanexo9" class="form-control" value="<?php echo $idanexo9 ?>">
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div>   
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90"><?php echo $descrip_modifica ?></textarea>
            </div>
          </div>     
            <?php }?>
          
          <!----------->
          <hr class="subsubtitle">
          <div class="row">
            <div class="col-md-3">
              <h4>Tipo de operación:</h4>
              <input type="text" class="form-control" value="Servicios de blindaje" readonly=""><!-- 901 - Servicios de blindaje-->
            </div>
          </div>
          <hr>
          <h5>Tipo de Bien blindado</h5>
          <div class="row">
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="blindaje" id="blindaje1" value="1" onclick="tipo_blindaje()" <?php if($blindaje==1) echo 'checked' ?>>
                  Vehículo terrestre
                </label>
              </div>
            </div>
          </div>
          <div class="texto_terrestre" style="display: none">
            <div class="row"> 
              <div class="col-md-6 form-group">
                <label>Marca/fabricante</label>
                <input class="form-control" type="text" name="marca_fabricante" value="<?php echo $marca_fabricante ?>">
              </div>
              <div class="col-md-6 form-group">
                <label>Modelo</label>
                <input class="form-control" type="text" name="modelo" value="<?php echo $modelo ?>">
              </div>
              <div class="col-md-2 form-group">
                <label>Año</label>
                <select class="form-control" id="anio_vehiculo" name="anio_vehiculo" value="<?php echo $anio_vehiculo ?>">
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label>Número de Identificación Vehicular (VIN)</label>
                <input class="form-control" type="text" name="vin" id="num_vehit" maxlength="17" onchange="verificaTama()" value="<?php echo $vin ?>">
              </div>
              <div class="col-md-6 form-group">
                <label>No. de inscripción en el Registro Público Vehicular</label>
                <input class="form-control" type="text" name="repuve" id="num_vehit2" maxlength="8" onchange="verificaTama2()" value="<?php echo $repuve ?>">
              </div>
              <div class="col-md-4 form-group">
                <label>Placas</label>
                <input class="form-control" type="text" name="placas" value="<?php echo $placas ?>">
              </div>
              <div class="col-md-4 form-group">
                <label>Estado del Vehículo Terrestre que se blinda</label>
                <select class="form-control" name="estado_bien">
                  <option value="1" <?php if($estado_bien==1) echo 'selected'?>>Nuevo</option>
                  <option value="2" <?php if($estado_bien==2) echo 'selected'?>>Usado</option>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label>Nivel de blindaje</label>
                <select class="form-control" name="nivel_blindaje">
                  <option value="1" <?php if($nivel_blindaje==1) echo 'selected'?>>Nivel A</option>
                  <option value="2" <?php if($nivel_blindaje==2) echo 'selected'?>>Nivel B</option>
                  <option value="3" <?php if($nivel_blindaje==3) echo 'selected'?>>Nivel B Plus</option>
                  <option value="4" <?php if($nivel_blindaje==4) echo 'selected'?>>Nivel C</option>
                  <option value="5" <?php if($nivel_blindaje==5) echo 'selected'?>>Nivel C Plus</option>
                  <option value="6" <?php if($nivel_blindaje==6) echo 'selected'?>>Nivel D</option>
                  <option value="7" <?php if($nivel_blindaje==7) echo 'selected'?>>Nivel E</option>
                </select>
              </div>
            </div>
          </div>  
          <div class="row">  
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="blindaje" id="blindaje2" value="2" onclick="tipo_blindaje()"  <?php if($blindaje==2) echo 'checked' ?>>
                  Inmueble
                </label>
              </div>
            </div>
          </div>
          <div class="text_inmueble" style="display: none">  
            <div class="row">
              <div class="col-md-3 form-group">
                <label>Tipo de inmueble</label>
                <select class="form-control" name="tipo_inmueble">
                  <option value="1" <?php if($tipo_inmueble==1) echo 'selected'?>>Casa /Casa en condominio</option> 
                  <option value="2" <?php if($tipo_inmueble==2) echo 'selected'?>>Departamento</option> 
                  <option value="3" <?php if($tipo_inmueble==3) echo 'selected'?>>Edificio habitacional</option> 
                  <option value="4" <?php if($tipo_inmueble==4) echo 'selected'?>>Edificio comercial</option> 
                  <option value="5" <?php if($tipo_inmueble==5) echo 'selected'?>>Edificio oficinas</option> 
                  <option value="6" <?php if($tipo_inmueble==6) echo 'selected'?>>Local comercial independiente</option> 
                  <option value="7" <?php if($tipo_inmueble==7) echo 'selected'?>>Local en centro comercial</option> 
                  <option value="8" <?php if($tipo_inmueble==8) echo 'selected'?>>Oficina</option> 
                  <option value="9" <?php if($tipo_inmueble==9) echo 'selected'?>>Bodega comercial</option> 
                  <option value="10" <?php if($tipo_inmueble==10) echo 'selected'?>>Bodega industrial</option> 
                  <option value="11" <?php if($tipo_inmueble==11) echo 'selected'?>>Nave Industrial</option> 
                  <option value="17" <?php if($tipo_inmueble==17) echo 'selected'?>>Rancho/Hacienda/Quinta</option> 
                  <option value="99" <?php if($tipo_inmueble==99) echo 'selected'?>>Otro</option> 
                </select>
              </div>
              <div class="col-md-3 form-group">
                <label>Codigo postal</label>
                <input class="form-control" type="text" name="codigo_postal" value="<?php echo $codigo_postal ?>">
              </div>
            </div>
            <h5>Datos de parte blindada</h5>
            <div class="row">  
              <div class="col-md-3 form-group">
                <label>Parte de inmueble objeto del Servicio</label>
                <select class="form-control" name="parte_blindada">
                  <option value="1" <?php if($parte_blindada==1) echo 'selected'?>>Ventanas</option>
                  <option value="2" <?php if($parte_blindada==2) echo 'selected'?>>Puertas</option>
                  <option value="3" <?php if($parte_blindada==3) echo 'selected'?>>Cuarto Expuesto</option>
                  <option value="4" <?php if($parte_blindada==4) echo 'selected'?>>Cuarto Oculto</option>
                  <option value="5" <?php if($parte_blindada==5) echo 'selected'?>>Bodegas</option>
                  <option value="6" <?php if($parte_blindada==6) echo 'selected'?>>Bóveda</option>
                  <option value="7" <?php if($parte_blindada==7) echo 'selected'?>>Bardas  o paredes exteriores</option>
                  <option value="9" <?php if($parte_blindada==9) echo 'selected'?>>Otros</option>
                </select>
              </div>
              <div class="col-md-3 form-group">
                <label>Nivel de blindaje</label>
                <select class="form-control" name="nivel_blindajei">
                  <option value="1" <?php if($nivel_blindajei==1) echo 'selected'?>>Nivel A</option>
                  <option value="2" <?php if($nivel_blindajei==2) echo 'selected'?>>Nivel B</option>
                  <option value="3" <?php if($nivel_blindajei==3) echo 'selected'?>>Nivel B Plus</option>
                  <option value="4" <?php if($nivel_blindajei==4) echo 'selected'?>>Nivel C</option>
                  <option value="5" <?php if($nivel_blindajei==5) echo 'selected'?>>Nivel C Plus</option>
                  <option value="6" <?php if($nivel_blindajei==6) echo 'selected'?>>Nivel D</option>
                  <option value="7" <?php if($nivel_blindajei==7) echo 'selected'?>>Nivel E</option>
                </select>
              </div>
            </div>  


          </div>
          <hr class="subsubtitle">
          <h5>Datos de liquidación</h5>
          <div class="row_liquidacion">
            <div class="liquidacion">
              
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total de la liquidación</label>
              <input readonly="" class="form-control" type="text" id="total_liquida" value="$0.00">
            </div>
            <div class="col-md-8 form-group">
              <br><br>
              <h5 class="validacion_cantidad" style="color: red"></h5>
            </div>
          </div>
          <div class="row"> 
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total en efectivo</label>
              <input readonly="" class="form-control" type="text" id="total_liquida_efect" value="$0.00">
              <div class="col-md-8" align="center">
              <br><br>
                <h5 class="limite_efect_msj" style="color: red"></h5>

              </div>
            </div>
          </div>
        <div class="modal-footer">
          <button disabled class="btn gradient_nepal2" id="btn_submit" type="button" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
          <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
        </div>
        <div id="settings-trigger" style="width: 204px; left: 70px;" class="settings-trigger2"> 
          <button type="button" id="agregar_liquida" class="btn gradient_nepal2" onclick="agregar_liqui()"><i class="fa fa-plus"></i> Agregar Liquidación</button>
        </div>
    	</div>
    </div>
	</div>
</div>

<!-- Operaciones -->
<!--------------Modal-------------->
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 carácteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- / -->

<!------ Modificatorio ------->
<!--- --->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- < / >--->