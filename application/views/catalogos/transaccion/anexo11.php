<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<input type="hidden" id="aux_tipo_actividad" value="<?php echo $tipo_actividad ?>">
<input type="hidden" id="aux_cc4" value="<?php echo $aux_cc4 ?>">
<input type="hidden" id="aux_cc6" value="<?php echo $aux_cc6 ?>">
<input type="hidden" id="aux_cc7" value="<?php echo $aux_cc7 ?>">
<input type="hidden" id="aux_cc72" value="<?php echo $aux_cc72 ?>">
<input type="hidden" id="aux_cc9" value="<?php echo $aux_cc9 ?>">
<input type="hidden" id="aux_cc92" value="<?php echo $aux_cc92 ?>">
<div class="row">
      <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h4>Anexo 11 - Servicios Profesionales.</h4>  
            <h3>Registro de transacción</h3>
          </div>
        </div>
          <hr class="subtitle">
          <!--- --->
          <form id="form_anexo0">
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
            <!--- --->
            <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
            <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
            <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
            <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
            <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
            <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
            <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <!-- -->
            <!-- -->  
            <div class="row">
              <div class="col-md-4 form-group">
                <label>Fecha de operación: Fecha de recepción de recursos</label>
                <div class="d-flex mb-2">
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_fecha_opera()">
                    <i class="mdi mdi-help"></i>
                  </button>              
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label>Referencia</label>
                <div class="d-flex mb-2">
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" value="<?php echo $referencia ?>" placeholder="Ingrese número de referencia">
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button>              
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label>Monto de la factura sin IVA ni accesorios</label>
                <input type="text" id="monto_opera" class="form-control monto_1" onchange="tipo_modeda(1)" placeholder="Ingrese el monto total de la operación ($)" required="" value="<?php echo $monto_opera ?>">
              </div>
              <div class="col-md-3 form-group">
                <label>Año reportado:</label>
                <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
                <select class="form-control" id="anio" name="anio_acuse">
                </select>
              </div>
              <div class="col-md-3 form-group">
                <label>Mes reportado:</label>
                <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
                <select class="form-control" id="mes" name="mes_acuse">
                  <option value="01">Enero</option>
                  <option value="02">Febrero</option>
                  <option value="03">Marzo</option>
                  <option value="04">Abril</option>
                  <option value="05">Mayo</option>
                  <option value="06">Junio</option>
                  <option value="07">Julio</option>
                  <option value="08">Agosto</option>
                  <option value="09">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label>No. Factura:</label>
                <div class="d-flex mb-2">
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" value="<?php echo $num_factura ?>" placeholder="Ingrese número de factura">
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button>              
                </div>
              </div>
            </div>
            <!--- --->
            <div class="row"> 
              <div class="col-md-8 form-group">
                  <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                  <span style="font-size: 25px; color: #b57532;">
                  <?php if($tipoc==6){ 
                        echo mb_strtoupper($cc->denominacion);
                        }
                        if($tipoc==3){
                        echo mb_strtoupper($cc->razon_social);  
                        }
                        if($tipoc==4){
                        echo mb_strtoupper($cc->nombre_persona);
                        }
                        if($tipoc==5){
                        echo mb_strtoupper($cc->denominacion);
                        }
                        if($tipoc==1 || $tipoc==2){
                        echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                        }
                  ?> 
                  </span>
              </div>
              <div class="col-md-4 form-group">
                <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                  <?php echo $idcliente_cliente ?>
                </span>  
              </div>
              <div class="col-md-3 form-group">
                <label>Ocupación:</label>
                <select class="form-control" id="ocupacion" name="ocupacion">
                  <option value=""></option>
                  <option <?php if($ocupacion==1){ echo "selected"; } ?> value="1">Abogado</option>
                  <option <?php if($ocupacion==2){ echo "selected"; } ?> value="2">Contador</option>
                  <option <?php if($ocupacion==3){ echo "selected"; } ?> value="3">Administrador</option>
                  <option <?php if($ocupacion==4){ echo "selected"; } ?> value="4">Outsourcing / Servicios Especializados</option>
                  <option <?php if($ocupacion==5){ echo "selected"; } ?> value="5">Consultoría</option>
                  <option <?php if($ocupacion==99){ echo "selected"; } ?> value="99">Otro</option>
                </select>
              </div>
              <div class="col-md-6 form-group" id="cont_otra_ocupa" style="display:none;">
                <label>Descripción de otra ocupación</label>
                <input class="form-control" type="text" id="desc_otra_ocupa" name="desc_otra_ocupa" value="<?php echo $desc_otra_ocupa; ?>">
              </div>
            </div>
            <!-- -->
            <?php if($aviso==1){ ?>
              <hr class="subsubtitle">
              <div class="row">  
                <div class="col-md-12">
                </div>
                <div class="col-md-10 form-group">
                  <h5>Datos de la modificación:</h5>
                </div>
                <div class="col-md-2 form-group">
                </div>
                <div class="col-md-3 form-group">
                  <label>Referencia modificación:</label>
                  <div class="d-flex mb-2">  
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                      <i class="mdi mdi-help text-dark"></i>
                    </button>   
                  </div> 
                </div>
                <div class="col-md-3 form-group">
                  <label>Folio de aviso que se modificará:</label>
                  <input type="hidden" name="idanexo11" id="idanexo11" class="form-control" value="<?php echo $idanexo11 ?>">
                  <div class="d-flex mb-2">  
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                      <i class="mdi mdi-help text-dark"></i>
                    </button>   
                  </div> 
                </div>
                <div class="col-md-6 form-group">
                  <label>Descripción de la modificación:</label>
                  <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90"><?php echo $descrip_modifica ?></textarea>
                </div>
              </div>     
              <?php }?>
            <!-- -->
          </form>  
          <hr class="subsubtitle">
          <form id="frm1">
            <div class="row">
              <div class="col-md-6 form-group">
                  <label class="form-check-label">Elija una opción</label>
                  <select id="DataOpcion" name="tipo_actividad" class="form-control">
                    <option>Seleccione</option>
                    <option <?php if($tipo_actividad==1){ echo "selected"; } ?> value="1">Compraventa de Inmuebles</option>
                    <option <?php if($tipo_actividad==2){ echo "selected"; } ?> value="2">Cesión de Derechos sobre Inmuebles</option>
                    <option <?php if($tipo_actividad==3){ echo "selected"; } ?> value="3">Administración y manejo de recursos, valores, cuentas bancarias, ahorro o valores o cualquier otro activo</option>
                    <option <?php if($tipo_actividad==4){ echo "selected"; } ?> value="4"> Constitución de personas morales (incluidas las sociedades mercantiles)</option>
                    <option <?php if($tipo_actividad==5){ echo "selected"; } ?> value="5">Organización de aportaciones de capital o cualquier otro tipo de recursos para la constitución, operacion y administración de sociedades mercantiles</option>
                    <option <?php if($tipo_actividad==6){ echo "selected"; } ?> value="6">Fusión</option>
                    <option <?php if($tipo_actividad==7){ echo "selected"; } ?> value="7">Escisión</option>
                    <option <?php if($tipo_actividad==8){ echo "selected"; } ?> value="8">Operación y administración de personas morales y vehículos corporativos</option>
                    <option <?php if($tipo_actividad==9){ echo "selected"; } ?> value="9">Constitución de Fideicomiso</option>
                    <option <?php if($tipo_actividad==10){ echo "selected"; } ?> value="10">Compra o venta de entidades mercantiles</option>
                  </select>
              </div>
            </div>  
          </form>
          <!-- <form id="form_anexo1">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="1" id="tipo_actividad1" onclick="tipo_actividad_select(1)" <?php if($tipo_actividad==1) echo 'checked' ?>>
                      Compraventa de Inmuebles
                  </label>
                </div>
              </div>
            </div>  
          </form> tipo_actividad1 1--> 
          <div class="tipo_actividadtxt1" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad1">
                  <input type="hidden" name="actividad" value="<?php echo $actividad1 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <label>Tipo de Operación de compraventa</label>
                      <select class="form-control" name="tipo_operacion1">
                        <option value="1" <?php if($tipo_operacion1 == 1) echo 'selected' ?>>Compra de Inmueble</option>
                        <option value="2" <?php if($tipo_operacion1 == 2) echo 'selected' ?>>Venta de Inmueble</option>
                      </select>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Tipo de bien inmueble</label>
                      <select class="form-control" name="tipo_inmueble1">
                      <?php foreach ($tipo_inmueble_get as $i){ ?>
                        <option value="<?php echo $i->clave ?>" <?php if($i->clave == $tipo_inmueble1) echo 'selected' ?>><?php echo $i->nombre?></option> 
                      <?php } ?>  
                      </select>
                    </div>  
                    <div class="col-md-6 form-group">
                      <label>Valor pactado del inmueble en Moneda Nacional</label>
                      <input class="form-control" type="number" name="valor_pactado1" maxlength="17" value="<?php echo $valor_pactado1 ?>">
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la contraparte</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona1" id="tipo_persona_1_1" value="1" onclick="tipo_persona_opt(1)" <?php if($tipo_persona1==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona1" id="tipo_persona_1_2" value="2" onclick="tipo_persona_opt(1)" <?php if($tipo_persona1==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona1" id="tipo_persona_1_3" value="3" onclick="tipo_persona_opt(1)" <?php if($tipo_persona1==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="tipo_persona_txt_1_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombre1" value="<?php echo $nombre1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paterno1" value="<?php echo $apellido_paterno1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_materno1" value="<?php echo $apellido_materno1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimiento1" value="<?php echo $fecha_nacimiento1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc1" maxlength="13" value="<?php echo $rfc1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" onblur="ValidCurp(this)" type="text" name="curp1" maxlength="18" value="<?php echo $curp1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_1_1" name="pais_nacionalidad1">
                          <?php if($pais_nacionalidad1!=""){
                           echo '<option value="'.$pais_nacionalidad1.'">'.$pais_nacionalidad1t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                  </div>  
                  <div class="tipo_persona_txt_1_2" style="display: none">
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label> 
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonm1" value="<?php echo $denominacion_razonm1 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionm1" value="<?php echo $fecha_constitucionm1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcm1" maxlength="13" value="<?php echo $rfcm1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_1_1" name="pais_nacionalidadm1">
                          <?php if($pais_nacionalidadm1!=""){
                           echo '<option value="'.$pais_nacionalidadm1.'">'.$pais_nacionalidadm1t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <hr> 
                  </div>  
                  <div class="tipo_persona_txt_1_3" style="display: none">
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonf1" value="<?php echo $denominacion_razonf1 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf1" maxlength="13" value="<?php echo $rfcf1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="identificador_fideicomisof1" maxlength="40" value="<?php echo $identificador_fideicomisof1 ?>">
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                  <hr class="subsubtitle">
                  <h5>Características del Inmueble</h5>
                  <div class="row"> 
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" onchange="cambiaCP(this.id)" pattern='\d*' maxlength="5" type="text" name="codigo_postal1" maxlength="5" id="codigo_postal1" value="<?php echo $codigo_postal1 ?>">
                    </div>   
                    <div class="col-md-4 form-group">
                      <label>Calle, avenida o vía de la ubicación del inmueble</label>
                      <input class="form-control" type="text" name="calle1" maxlength="100" value="<?php echo $calle1 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterior1"  maxlength="56" value="<?php echo $numero_exterior1 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interior1" maxlength="40" value="<?php echo $numero_interior1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Colonia de la ubicación del inmueble</label>
                      <select onclick="autoComplete(this.id)" class="form-control" name="colonia1" id="colonia1">
                        <option value=""></option>
                        <?php if($colonia1!=""){
                          echo '<option value="'.$colonia1.'" selected>'.$colonia1.'</option>';      
                        } ?>  
                      </select>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Dimensiones del inmueble en m2 del terreno</label>
                      <input class="form-control" type="number" name="dimension_terreno1" minlength="4" maxlength="10" value="<?php echo $dimension_terreno1 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Dimensiones del inmueble en m2 Construidos</label>
                      <input class="form-control" type="text" name="dimension_construido1" minlength="4" maxlength="10" value="<?php echo $dimension_construido1 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Folio real del inmueble o antecedentes registrales</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="folio_real1" value="<?php echo $folio_real1 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_folio_ayuda()">
                          <i class="mdi mdi-help"></i>
                        </button>              
                      </div>
                      
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>La operación se realizó por medio de un contrato privado o Instrumento Público?</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_instrumento_publico1" id="datos_instrumento_publico1" value="1" onchange="datos_instrumento_publico_opt1()" <?php if($datos_instrumento_publico1==1) echo 'checked' ?>>
                          Instrumento Público
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_instrumento_publico1" id="datos_instrumento_publico2" value="2" onchange="datos_instrumento_publico_opt1()" <?php if($datos_instrumento_publico1==2) echo 'checked' ?>>
                          Contrato privado
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="datos_instrumento_publicotxt1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>No. del instrumento público</label>
                        <input class="form-control" type="text" name="numero_instrumento_publico1" maxlength="20" value="<?php echo $numero_instrumento_publico1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Fecha del Instrumento Público</label>
                        <input class="form-control" type="date" name="fecha_instrumento_publico1" value="<?php echo $fecha_instrumento_publico1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>No. De Notario del instrumento público</label>
                        <input class="form-control" type="text" name="notario_instrumento_publico1" maxlength="8" value="<?php echo $notario_instrumento_publico1 ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Clave de la Entidad Federativa del instrumento público</label>
                        <select class="form-control" name="entidad_instrumento_publico1"> 
                          <?php foreach ($estados_get as $e){ ?>
                            <option value="<?php echo $e->id ?>" <?php if($e->id==$entidad_instrumento_publico1) echo 'selected' ?>><?php echo $e->estado ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Valor de referencia del inmueble</label>
                        <input class="form-control" type="text" name="valor_referencia1" maxlength="8" value="<?php echo $valor_referencia1 ?>">
                      </div>
                    </div>
                  </div>
                  <div class="datos_instrumento_publicotxt2" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Fecha del contrato</label>
                        <input class="form-control" type="date" name="fecha_contrato1" value="<?php echo $fecha_contrato1 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Valor de referencia del inmueble</label>
                        <input class="form-control" type="text" name="valor_referencia1_1" value="<?php echo $valor_referencia1_1 ?>">
                      </div>
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la operación financiera</h5>
                  <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Fecha de la operación financiera</label>
                        <input class="form-control" type="date" name="fecha_pago1" value="<?php echo $fecha_pago1 ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Forma de pago</label>
                        <select class="form-control" name="forma_pago1"> 
                          <?php foreach ($forma_pago_get as $f){ ?>
                            <option value="<?php echo $f->clave ?>" <?php if($f->clave==$forma_pago1) echo 'selected' ?>><?php echo $f->descripcion ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Instrumento monetario con el que se realizó la operación </label>
                        <select class="form-control" name="instrumento_monetario1"> 
                          <?php foreach ($intrumento_monetario_get as $a){ ?>
                            <option value="<?php echo $a->clave ?>" <?php if($a->clave==$instrumento_monetario1) echo 'selected' ?>><?php echo $a->descripcion ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Tipo de moneda o divisa de la operación</label>
                        <select class="form-control" name="moneda1"> 
                          <?php /*foreach ($tipo_moneda_get as $t){
                            if($t->clave==1){   
                            ?>
                            <option value="<?php echo $t->clave ?>" <?php if($t->clave==$moneda1) echo 'selected' ?>><?php echo $t->moneda ?></option>
                          <?php }
                          }*/ ?>
                          <option value="0" <?php if($moneda1=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                          <option value="1" <?php if($moneda1=="1") echo "selected"; ?>>Dólar Americano</option>
                          <option value="2" <?php if($moneda1=="2") echo "selected"; ?>>Euro</option>
                          <option value="3" <?php if($moneda1=="3") echo "selected"; ?>>Libra Esterlina</option>
                          <option value="4" <?php if($moneda1=="4") echo "selected"; ?>>Dólar canadiense</option>
                          <option value="5" <?php if($moneda1=="5") echo "selected"; ?>>Yuan Chino</option>
                          <option value="6" <?php if($moneda1=="6") echo "selected"; ?>>Centenario</option>
                          <option value="7" <?php if($moneda1=="7") echo "selected"; ?>>Onza de Plata</option>
                          <option value="8" <?php if($moneda1=="8") echo "selected"; ?>>Onza de Oro</option>
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Monto de la liquidación</label>
                        <input class="form-control" type="number" name="monto_operacion1" minlength="4" maxlength="17" value="<?php echo $monto_operacion1 ?>">
                      </div>
                  </div>    
                  <!-- --> 
                </form>
              </div>
            </div>     
            <!--  -->
          </div>  
           <!--<form id="form_anexo2">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="2" id="tipo_actividad2" onclick="tipo_actividad_select(2)" <?php if($tipo_actividad==2) echo 'checked' ?>>
                      Cesión de Derechos sobre Inmuebles
                  </label>
                </div>
              </div>
            </div>  
          </form> --> 
          <div class="tipo_actividadtxt2" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad2">
                  <input type="hidden" name="actividad" value="<?php echo $actividad2 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <label>Figura del o los clientes objeto del aviso en la operación</label>
                      <select class="form-control" name="figura_cliente2">
                        <option value="1" <?php if($figura_cliente2==1) echo 'selected' ?>>Cedente</option>
                        <option value="2" <?php if($figura_cliente2==2) echo 'selected' ?>>Cesionario</option>
                      </select>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Figura del o los clientes objeto del aviso en la operación</label>
                      <select class="form-control" name="tipo_cesion2">
                        <option value="1" <?php if($tipo_cesion2==1) echo 'selected' ?>>Cesión de uso o goce</option>
                        <option value="2" <?php if($tipo_cesion2==2) echo 'selected' ?>>Cesión de usufructo</option>
                        <option value="3" <?php if($tipo_cesion2==2) echo 'selected' ?>>Cesión de dominio</option>
                      </select>
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la contraparte</h5> 
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona2" id="tipo_persona_2_1" value="1" onclick="tipo_persona_opt(2)" <?php if($tipo_persona2==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona2" id="tipo_persona_2_2" value="2" onclick="tipo_persona_opt(2)" <?php if($tipo_persona2==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona2" id="tipo_persona_2_3" value="3" onclick="tipo_persona_opt(2)" <?php if($tipo_persona2==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="tipo_persona_txt_2_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombre2" value="<?php echo $nombre2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paterno2" value="<?php echo $apellido_paterno2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_materno2" value="<?php echo $apellido_materno2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimiento2" value="<?php echo $fecha_nacimiento2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc2" maxlength="13" value="<?php echo $rfc2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp2" maxlength="18" value="<?php echo $curp2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_2_2" name="pais_nacionalidad2">
                          <?php if($pais_nacionalidad2!=""){
                           echo '<option value="'.$pais_nacionalidad2.'">'.$pais_nacionalidad2t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                  </div>  
                  <div class="tipo_persona_txt_2_2" style="display: none">
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonm2" value="<?php echo $denominacion_razonm2 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionm2" value="<?php echo $fecha_constitucionm2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcm2" maxlength="13" value="<?php echo $rfcm2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_2_2" name="pais_nacionalidadm2">
                          <?php if($pais_nacionalidadm2!=""){
                           echo '<option value="'.$pais_nacionalidadm2.'">'.$pais_nacionalidadm2t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <hr> 
                  </div>  
                  <div class="tipo_persona_txt_2_3" style="display: none">
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonf2" value="<?php echo $denominacion_razonf2 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-5 form-group">
                        <label>RFC del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf2" maxlength="13" value="<?php echo $rfcf2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="identificador_fideicomisof2" maxlength="40" value="<?php echo $identificador_fideicomisof2 ?>">
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                  <hr class="subsubtitle">
                  <h5>Características del Inmueble</h5>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Tipo de inmueble</label>
                      <select class="form-control" name="tipo_inmueble2">
                      <?php foreach ($tipo_inmueble_get as $item){ ?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmueble2) echo 'selected' ?>><?php echo $item->nombre ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Valor de referencia</label>
                      <input class="form-control" type="text" name="valor_referencia2" maxlength="17" value="<?php echo $valor_referencia2 ?>">
                    </div>
                  </div>
                  <div class="row">    
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" onchange="cambiaCP(this.id)" pattern='\d*' maxlength="5" type="text" name="codigo_postal2" maxlength="5" value="<?php echo $codigo_postal2 ?>" id="codigo_postal2">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Calle, avenida o vía de la ubicación del inmueble</label>
                      <input class="form-control" type="text" name="calle2" maxlength="100" value="<?php echo $calle2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterior2" maxlength="56" value="<?php echo $numero_exterior2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interior2" maxlength="40" value="<?php echo $numero_interior2 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Colonia de la ubicación del inmueble</label>
                      <select onclick="autoComplete(this.id)" class="form-control" name="colonia2" id="colonia2">
                        <option value=""></option>
                        <?php if($colonia2!=""){
                          echo '<option value="'.$colonia2.'" selected>'.$colonia2.'</option>';      
                        } ?>  
                      </select>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Dimensiones del inmueble en m2 del terreno</label>
                      <input class="form-control" type="number" name="dimension_terreno2" minlength="4" maxlength="10" value="<?php echo $dimension_terreno2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Dimensiones del inmueble en m2 Construidos</label>
                      <input class="form-control" type="text" name="dimension_construido2" minlength="4" maxlength="10" value="<?php echo $dimension_construido2 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Folio real del inmueble o antecedentes registrales</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="folio_real2" value="<?php echo $folio_real2 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_folio_ayuda()">
                          <i class="mdi mdi-help"></i>
                        </button>              
                      </div>
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la operación financiera</h5>
                  <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Fecha de la operación financiera</label>
                        <input class="form-control" type="date" name="fecha_pago2" value="<?php echo $fecha_pago2 ?>">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Instrumento monetario con el que se realizó la operación </label>
                        <select class="form-control" name="instrumento_monetario2"> 
                          <?php foreach ($actividad_econominica_fisica_get as $a){ ?>
                            <option value="<?php echo $a->clave ?>" <?php if($a->clave==$instrumento_monetario2) echo 'selected' ?>><?php echo $a->acitividad ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Tipo de moneda o divisa de la operación</label>
                        <select class="form-control" name="moneda2"> 
                          <?php /*foreach ($tipo_moneda_get as $t){
                            if($t->clave==1){   
                            ?>
                            <option value="<?php echo $t->clave ?>" <?php if($t->clave==$moneda2) echo 'selected' ?>><?php echo $t->moneda ?></option>
                          <?php /* }
                          } */ ?>
                          <option value="0" <?php if($moneda2=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                          <option value="1" <?php if($moneda2=="1") echo "selected"; ?>>Dólar Americano</option>
                          <option value="2" <?php if($moneda2=="2") echo "selected"; ?>>Euro</option>
                          <option value="3" <?php if($moneda2=="3") echo "selected"; ?>>Libra Esterlina</option>
                          <option value="4" <?php if($moneda2=="4") echo "selected"; ?>>Dólar canadiense</option>
                          <option value="5" <?php if($moneda2=="5") echo "selected"; ?>>Yuan Chino</option>
                          <option value="6" <?php if($moneda2=="6") echo "selected"; ?>>Centenario</option>
                          <option value="7" <?php if($moneda2=="7") echo "selected"; ?>>Onza de Plata</option>
                          <option value="8" <?php if($moneda2=="8") echo "selected"; ?>>Onza de Oro</option>
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Monto de la liquidación</label>
                        <input class="form-control" type="number" name="monto_operacion2" minlength="4" maxlength="17" value="<?php echo $monto_operacion2 ?>">
                      </div>
                  </div>  
                  <!-- -->
                </form>
              </div>
            </div>   
            <!--  -->
          </div> 
           <!--<form id="form_anexo3">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="3" id="tipo_actividad3" onclick="tipo_actividad_select(3)" <?php if($tipo_actividad==3) echo 'checked' ?>>
                      Administración y manejo de recursos, valores, cuentas bancarias, ahorro o valores o cualquier otro activo 
                  </label>
                </div>
              </div>
            </div>  
          </form> --> 
          <div class="tipo_actividadtxt3" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad3">
                  <input type="hidden" name="actividad" value="<?php echo $actividad3 ?>">
                  <!-- -->  

                  <h5>Tipo de Activos administrados</h5>
                  <div class="row">
                    <div class="col-md-3 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_activo3" id="tipo_activo3_1" value="1" onclick="tipo_activo_opt3()" <?php if($tipo_activo3==1) echo 'checked' ?>>
                          Cuentas bancarias, ahorro o valores
                        </label>
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_activo3" id="tipo_activo3_2" value="2" onclick="tipo_activo_opt3()" <?php if($tipo_activo3==2) echo 'checked' ?>>
                          Activos Inmobiliarios
                        </label>
                      </div>
                    </div>
                    <div class="col-md-3 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_activo3" id="tipo_activo3_4" value="4" onclick="tipo_activo_opt3()" <?php if($tipo_activo3==4) echo 'checked' ?>>
                          Outsourcing
                        </label>
                      </div>
                    </div>
                    <div class="col-md-3 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_activo3" id="tipo_activo3_3" value="3" onclick="tipo_activo_opt3()" <?php if($tipo_activo3==3) echo 'checked' ?>>
                          Otros Activos
                        </label>
                      </div>
                    </div>
                    
                  </div>
                  <div class="tipo_activo_txt3_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Estatus del manejo</label>
                        <select class="form-control" name="estatus_manejo3">
                          <option value="1" <?php if($estatus_manejo3 == 1) echo 'selected' ?>>Apertura de Cuenta</option>
                          <option value="2" <?php if($estatus_manejo3 == 2) echo 'selected' ?>>Operaciones</option>
                          <option value="3" <?php if($estatus_manejo3 == 3) echo 'selected' ?>>Cancelación de la cuenta</option>
                        </select>
                      </div>
                      <div class="col-md-8 form-group">
                        <label>Clave del tipo de Institución financiera donde se mantiene la cuenta</label>
                        <select class="form-control" name="clave_tipo_institucion3">
                          <option value="04" <?php if($clave_tipo_institucion3 == '04') echo 'selected' ?>>Adminsitradoras de Fondos para el Retiro</option>
                          <option value="13" <?php if($clave_tipo_institucion3 == '13') echo 'selected' ?>>Casas de Bolsa</option>
                          <option value="22" <?php if($clave_tipo_institucion3 == '22') echo 'selected' ?>>Instituciones de Seguros</option>
                          <option value="27" <?php if($clave_tipo_institucion3 == '27') echo 'selected' ?>>Sociedades Financieras Populares</option>
                          <option value="29" <?php if($clave_tipo_institucion3 == '29') echo 'selected' ?>>Sociedades Cooperativas de Ahorro y Préstamo</option>
                          <option value="37" <?php if($clave_tipo_institucion3 == '37') echo 'selected' ?>>Instituciones de Banca de Desarrollo</option>
                          <option value="40" <?php if($clave_tipo_institucion3 == '40') echo 'selected' ?>>Instituciones de Banca Múltiple</option>
                          <option value="58" <?php if($clave_tipo_institucion3 == '58') echo 'selected' ?>>Sociedades de Ahorro y Préstamo</option>
                          <option value="70" <?php if($clave_tipo_institucion3 == '70') echo 'selected' ?>>Sociedades Operadoras de Sociedades de Inversión</option>
                          <option value="71" <?php if($clave_tipo_institucion3 == '71') echo 'selected' ?>>Sociedades Distribuidoras de Sociedades de Inversión </option>
                          <option value="85" <?php if($clave_tipo_institucion3 == '85') echo 'selected' ?>>Uniones de Crédito </option>
                        </select>
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Nombre de la Institución financiera donde se mantiene la cuenta</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <textarea class="form-control" name="nombre_institucion3"><?php echo $nombre_institucion3 ?></textarea>
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_nom_inst_ayuda()">
                            <i class="mdi mdi-help"></i>
                          </button>              
                        </div>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número de cuenta, contrato o póliza</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input class="form-control" type="text" name="numero_cuenta3" maxlength="18" value="<?php echo $numero_cuenta3 ?>">
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_num_cta_ayuda()">
                            <i class="mdi mdi-help"></i>
                          </button>              
                        </div>
                      </div>
                      <div class="col-md-8 form-group">
                        <label>Saldo de la cuenta o instrumento financiero a la fecha de reporte o último estado de cuenta</label>
                        <input class="form-control" type="number" name="saldo3" maxlength="18" value="<?php echo $saldo3 ?>">
                      </div>
                      <div class="col-md-12 form-group">
                        <label>Tipo de moneda o divisa en la que esta expresado el saldo de la cuenta o instrumento financiero</label>
                        <select class="form-control" name="moneda3">
                          <?php /* foreach ($tipo_moneda_get as $t){
                            if($t->clave==1){   
                            ?>
                            <option value="<?php echo $t->clave ?>" <?php if($t->clave==$moneda3) echo 'selected' ?>><?php echo $t->moneda ?></option>
                          <?php }
                          } */ ?>
                          <option value="0" <?php if($moneda3=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                          <option value="1" <?php if($moneda3=="1") echo "selected"; ?>>Dólar Americano</option>
                          <option value="2" <?php if($moneda3=="2") echo "selected"; ?>>Euro</option>
                          <option value="3" <?php if($moneda3=="3") echo "selected"; ?>>Libra Esterlina</option>
                          <option value="4" <?php if($moneda3=="4") echo "selected"; ?>>Dólar canadiense</option>
                          <option value="5" <?php if($moneda3=="5") echo "selected"; ?>>Yuan Chino</option>
                          <option value="6" <?php if($moneda3=="6") echo "selected"; ?>>Centenario</option>
                          <option value="7" <?php if($moneda3=="7") echo "selected"; ?>>Onza de Plata</option>
                          <option value="8" <?php if($moneda3=="8") echo "selected"; ?>>Onza de Oro</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="tipo_activo_txt3_2" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Tipo de inmueble</label>
                        <select class="form-control" name="tipo_inmueble3">
                        <?php foreach ($tipo_inmueble_get as $item){ ?>
                          <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmueble3) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } ?>  
                        </select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Valor de referencia</label>
                        <input class="form-control" type="text" name="valor_referencia3" maxlength="8" value="<?php echo $valor_referencia3 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Código Postal</label>
                        <input class="form-control" onchange="cambiaCP(this.id)" pattern='\d*' type="text" name="codigo_postal3" maxlength="5" value="<?php echo $codigo_postal3 ?>" id="codigo_postal3">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Calle, avenida o vía de la ubicación del inmueble</label>
                        <input class="form-control" type="text" name="calle3" maxlength="100" value="<?php echo $calle3 ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Número exterior</label>
                        <input class="form-control" type="text" name="numero_exterior3" maxlength="56" value="<?php echo $numero_exterior3 ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Número interior</label>
                        <input class="form-control" type="text" name="numero_interior3" maxlength="40" value="<?php echo $numero_interior3 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Colonia de la ubicación del inmueble</label>
                        <select onclick="autoComplete(this.id)" class="form-control" name="colonia3" id="colonia3">
                          <option value=""></option>
                          <?php if($colonia3!=""){
                            echo '<option value="'.$colonia3.'" selected>'.$colonia3.'</option>';      
                          } ?>  
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Folio real del inmueble o antecedentes registrales</label>
                        <input class="form-control" type="text" name="folio_real3" maxlength="5" value="<?php echo $folio_real3 ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Clave del Área de Prestación del Servicio</label>
                        <select class="form-control" name="clave_presta" id="clave_presta">
                          <option value=""></option>
                          <?php foreach ($areas as $key){
                            if($clave_presta == $key->id){ $sel="Selected"; }else{ $sel=""; } ?>
                          <option <?php echo $sel; ?> value="<?php echo $key->id; ?>"><?php echo $key->descrip; ?></option>                
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Descripción de otra clave del área de prestacion del servicio</label>
                        <input class="form-control" type="text" name="descrip_otra_presta" value="<?php echo $descrip_otra_presta; ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Clave del Activo Administrado</label>
                        <select class="form-control" name="clave_activo" id="clave_activo">
                          <option value=""></option>
                          <?php foreach ($act_adm as $key){
                            if($clave_presta == $key->id){ $sel="Selected"; }else{ $sel=""; } ?>
                          <option <?php echo $sel; ?> value="<?php echo $key->id; ?>"><?php echo $key->descrip; ?></option>                
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Descripción de otra clave del Activo Administrado</label>
                        <input class="form-control" type="text" name="descrip_otro_activo" value="<?php echo $descrip_otro_activo; ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Cantidad de empleados utilizados en el servicio prestado</label>
                        <input class="form-control" type="number" name="num_emplea" value="<?php echo $num_emplea; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="tipo_activo_txt3_3" style="display: none">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Descripción del Activo Administrado</label>
                        <textarea class="form-control" name="descripcion3"><?php echo $descripcion3; ?></textarea>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Número de operaciones financieras que se estan reportando.</label>
                        <input class="form-control" type="number" name="num_opera" value="<?php echo $num_opera; ?>">
                      </div>

                    </div>  
                  </div>
                  <div class="tipo_activo_txt3_4" style="display: none">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Tipo de área de servicio</label>
                        <select class="form-control" name="tipo_area_servicio" id="tipo_area_servicio">
                          <option value="1" <?php if($tipo_area_servicio == 1) echo 'selected' ?>>Arrendamiento</option>
                          <option value="2" <?php if($tipo_area_servicio == 2) echo 'selected' ?>>Asesoría de negocio</option>
                          <option value="3" <?php if($tipo_area_servicio == 3) echo 'selected' ?>>Capacitación</option>
                          <option value="4" <?php if($tipo_area_servicio == 4) echo 'selected' ?>>Compras</option>
                          <option value="5" <?php if($tipo_area_servicio == 5) echo 'selected' ?>>Contabilidad</option>
                          <option value="6" <?php if($tipo_area_servicio == 6) echo 'selected' ?>>Investigación</option>
                          <option value="7" <?php if($tipo_area_servicio == 7) echo 'selected' ?>>Mantenimiento / Limpieza</option>
                          <option value="8" <?php if($tipo_area_servicio == 8) echo 'selected' ?>>Manufactura</option>
                          <option value="9" <?php if($tipo_area_servicio == 9) echo 'selected' ?>>Marketing / Publicidad / Promotoría</option>
                          <option value="10" <?php if($tipo_area_servicio == 10) echo 'selected' ?>>Recursos Financieros (diferente a Contabilidad)</option>
                          <option value="11" <?php if($tipo_area_servicio == 11) echo 'selected' ?>>Recursos Humanos</option>
                          <option value="12" <?php if($tipo_area_servicio == 12) echo 'selected' ?>>Recursos Materiales</option>
                          <option value="13" <?php if($tipo_area_servicio == 13) echo 'selected' ?>>Seguridad y Vigilancia</option>
                          <option value="14" <?php if($tipo_area_servicio == 14) echo 'selected' ?>>Servicios Legales</option>
                          <option value="15" <?php if($tipo_area_servicio == 15) echo 'selected' ?>>Tecnologías de la Información</option>
                          <option value="16" <?php if($tipo_area_servicio == 16) echo 'selected' ?>>Transporte / Distribución</option>
                          <option value="99" <?php if($tipo_area_servicio == 99) echo 'selected' ?>>Otro</option>
                        </select>
                      </div>
                      <div class="col-md-12 form-group" id="descripcion4_div" style="display: none">
                        <label>Descripción de otra área de servicio</label>
                        <textarea class="form-control" id="descripcion4" name="descripcion4"><?php echo $descripcion4; ?></textarea>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Tipo de activo administrado</label>
                        <select class="form-control" name="tipo_activo_admin" id="tipo_activo_admin">
                          <option value="1" <?php if($tipo_activo_admin == 1) echo 'selected' ?>>Administración de bases de datos</option>
                          <option value="2" <?php if($tipo_activo_admin == 2) echo 'selected' ?>>Administración de personal</option>
                          <option value="3" <?php if($tipo_activo_admin == 3) echo 'selected' ?>>Aduanas</option>
                          <option value="4" <?php if($tipo_activo_admin == 4) echo 'selected' ?>>Equipo de cómputo</option>
                          <option value="5" <?php if($tipo_activo_admin == 5) echo 'selected' ?>>Equipo médico</option>
                          <option value="6" <?php if($tipo_activo_admin == 6) echo 'selected' ?>>Estados Financieros</option>
                          <option value="7" <?php if($tipo_activo_admin == 7) echo 'selected' ?>>Fiscal / Tributario</option>
                          <option value="8" <?php if($tipo_activo_admin == 8) echo 'selected' ?>>Fusiones / Adquisiciones / Escisiones</option>
                          <option value="9" <?php if($tipo_activo_admin == 9) echo 'selected' ?>>Inmuebles</option>
                          <option value="10" <?php if($tipo_activo_admin == 10) echo 'selected' ?>>Instrumentos Financieros</option>
                          <option value="11" <?php if($tipo_activo_admin == 11) echo 'selected' ?>>Inversiones</option>
                          <option value="12" <?php if($tipo_activo_admin == 12) echo 'selected' ?>>Manejo de registros</option>
                          <option value="13" <?php if($tipo_activo_admin == 13) echo 'selected' ?>>Manejo de residuos</option>
                          <option value="14" <?php if($tipo_activo_admin == 14) echo 'selected' ?>>Maquinaria y Equipo Industrial</option>
                          <option value="15" <?php if($tipo_activo_admin == 15) echo 'selected' ?>>Mobiliario y equipo de oficina</option>
                          <option value="16" <?php if($tipo_activo_admin == 16) echo 'selected' ?>>Nómina</option>
                          <option value="17" <?php if($tipo_activo_admin == 17) echo 'selected' ?>>Reingeniería de procesos</option>
                          <option value="18" <?php if($tipo_activo_admin == 18) echo 'selected' ?>>Transporte / Distribución de mercancías</option>
                          <option value="19" <?php if($tipo_activo_admin == 19) echo 'selected' ?>>Traslado de personal y escolar</option>
                          <option value="20" <?php if($tipo_activo_admin == 20) echo 'selected' ?>>Valores / Obras de arte / Joyas / Relojes</option>
                          <option value="21" <?php if($tipo_activo_admin == 21) echo 'selected' ?>>Vehículos Aéreos</option>
                          <option value="22" <?php if($tipo_activo_admin == 22) echo 'selected' ?>>Vehículos Marítimos</option>
                          <option value="23" <?php if($tipo_activo_admin == 23) echo 'selected' ?>>Vehículos Terrestres</option>
                          <option value="99" <?php if($tipo_activo_admin == 99) echo 'selected' ?>>Otro</option>
                        </select>
                      </div>
                      <div class="col-md-12 form-group" id="descripcion4_2_div" style="display: none">
                        <label>Descripción de otro activo administrado</label>
                        <textarea class="form-control" id="desc_otro_activo_admin" name="desc_otro_activo_admin"><?php echo $desc_otro_activo_admin; ?></textarea>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Número de empleados</label>
                        <input class="form-control" type="number" name="num_empledos" value="<?php echo $num_empledos; ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Número de operaciones</label>
                        <input class="form-control" type="number" name="num_opera_out" value="<?php echo $num_opera_out; ?>">
                      </div>

                      <table class="table_" id="table_pays" width="100%">
                        <thead id="body_pays_out" class="table">
                          <tr id="tr_pays">
                            <td width="20%">
                              <div class="col-md-12 form-group">
                                <input type="hidden" id="id_pago" value="0">
                                <label>Fecha</label>
                                <input class="form-control" type="date" id="fecha" value="<?php echo date("Y-m-d"); ?>">
                              </div>
                            </td>
                            <td width="25%">
                              <div class="col-md-12 form-group">
                                <label>Instrumento monetario con el que se realizó la aportación</label>
                                <select class="form-control" id="instrumento_monetario3_4"> 
                                  <?php foreach ($intrumento_monetario_get as $a){ ?>
                                    <option value="<?php echo $a->clave; ?>"><?php echo $a->descripcion ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </td>
                            <td width="25%">
                              <div class="col-md-12 form-group">
                                <label>Tipo de moneda o divisa de la aportación</label>
                                <select class="form-control" id="moneda3_4"> 
                                  <option value="0">Pesos Mexicanos</option>
                                  <option value="1">Dólar Americano</option>
                                  <option value="2">Euro</option>
                                  <option value="3">Libra Esterlina</option>
                                  <option value="4">Dólar canadiense</option>
                                  <option value="5">Yuan Chino</option>
                                  <option value="6">Centenario</option>
                                  <option value="7">Onza de Plata</option>
                                  <option value="8">Onza de Oro</option>
                                </select>
                              </div>
                            </td>
                            <td width="25%">
                              <div class="col-md-12 form-group">
                                <label>Monto de la aportación</label>
                                <input class="form-control" type="number" id="monto_operacion3_4" value="">
                              </div> 
                            </td>
                            <td width="5%">
                              <div class="col-md-1 form-group">
                                <label style="color: transparent;">agregar</label>
                                <button type="button" onclick="addPago()" class="btn gradient_nepal2"><i class="fa fa-plus"></i></button>
                              </div> 
                            </td>
                          </tr>
                        </thead>
                        <tbody id="body_pays">
                        <?php if($id>0){
                          $getpays=$this->General_model->GetAllWhere('anexo11_actividad3_pagos',array("id_anexo11"=>$id,"estatus"=>1));
                          foreach ($getpays as $p){ ?>
                            <tr id="<?php echo 'tr_each_'.$p->id ?>">
                              <td width="20%">
                                <div class="col-md-12 form-group">
                                  <input type="hidden" id="id_pago" value="<?php echo $p->id; ?>">
                                  <label>Fecha</label>
                                  <input class="form-control" type="date" id="fecha" value="<?php echo $p->fecha; ?>">
                                </div>
                              </td>
                              <td>
                                <div class="col-md-12 form-group">
                                  <label>Instrumento monetario con el que se realizó la aportación</label>
                                  <select class="form-control" id="instrumento_monetario3_4"> 
                                    <?php foreach ($intrumento_monetario_get as $a){ ?>
                                      <option value="<?php echo $a->clave ?>" <?php if($a->clave==$p->instrumento_monetario3_4) echo 'selected' ?>><?php echo $a->descripcion ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </td>
                              <td>
                                <div class="col-md-12 form-group">
                                  <label>Tipo de moneda o divisa de la aportación</label>
                                  <select class="form-control" id="moneda3_4"> 
                                    <option value="0" <?php if($p->moneda3_4=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                                    <option value="1" <?php if($p->moneda3_4=="1") echo "selected"; ?>>Dólar Americano</option>
                                    <option value="2" <?php if($p->moneda3_4=="2") echo "selected"; ?>>Euro</option>
                                    <option value="3" <?php if($p->moneda3_4=="3") echo "selected"; ?>>Libra Esterlina</option>
                                    <option value="4" <?php if($p->moneda3_4=="4") echo "selected"; ?>>Dólar canadiense</option>
                                    <option value="5" <?php if($p->moneda3_4=="5") echo "selected"; ?>>Yuan Chino</option>
                                    <option value="6" <?php if($p->moneda3_4=="6") echo "selected"; ?>>Centenario</option>
                                    <option value="7" <?php if($p->moneda3_4=="7") echo "selected"; ?>>Onza de Plata</option>
                                    <option value="8" <?php if($p->moneda3_4=="8") echo "selected"; ?>>Onza de Oro</option>
                                  </select>
                                </div>
                              </td>
                              <td>
                                <div class="col-md-12 form-group">
                                  <label>Monto de la aportación</label>
                                  <input class="form-control" type="number" id="monto_operacion3_4" value="<?php echo $p->monto_operacion3_4 ?>">
                                </div> 
                              </td>
                              <td>
                                <div class="col-md-12 form-group">
                                  <label style="color: transparent;">delete</label>
                                  <button type="button" onclick="deletePago(1,<?php echo $p->id; ?>)" class="btn gradient_nepal2"><i class="fa fa-trash-o"></i></button>
                                </div> 
                              </td>
                            </tr>
                          <?php }
                         } ?>
                        </tbody>
                      </table>
                    </div>

                  </div>
                  <!-- -->  
                </form>
              </div>
            </div>     
            <!--  -->
          </div> 
           <!--<form id="form_anexo4">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="4" id="tipo_actividad4" onclick="tipo_actividad_select(4)" <?php if($tipo_actividad==4) echo 'checked' ?>>
                      Constitución de personas morales (incluidas las sociedades mercantiles)
                  </label>
                </div>
              </div>
            </div>  
          </form> --> 
          <div class="tipo_actividadtxt4" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad4">
                  <input type="hidden" name="actividad" value="<?php echo $actividad4 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Tipo de persona moral</label>
                      <select class="form-control" name="tipo_persona_moral4" id="tipo_persona_moral4" onchange="tipo_persona_moral_opt4()">
                        <option value="11" <?php if($tipo_persona_moral4 == 11) echo 'selected' ?>>Sociedad Mutualista de Seguros de Vida o de Daño</option>
                        <option value="12" <?php if($tipo_persona_moral4 == 12) echo 'selected' ?>>Sociedad Nacional de Crédito y/o Institución Bancaria de Desarrollo (S. N. C.)</option>
                        <option value="13" <?php if($tipo_persona_moral4 == 13) echo 'selected' ?>>Sociedad de Solidaridad Social (S. de S. S.)</option>
                        <option value="14" <?php if($tipo_persona_moral4 == 14) echo 'selected' ?>>Sociedad de Producción Rural de Responsabilidad Limitada</option>
                        <option value="15" <?php if($tipo_persona_moral4 == 15) echo 'selected' ?>>Sociedad de Producción Rural de Responsabilidad Ilimitada</option>
                        <option value="16" <?php if($tipo_persona_moral4 == 16) echo 'selected' ?>>Sociedad de Producción Rural de Responsabilidad Suplementada</option>
                        <option value="99" <?php if($tipo_persona_moral4 == 99) echo 'selected' ?>>Otra (especificar)</option>
                      </select>
                    </div>
                  </div>
                  <div class="tipo_persona_moraltxt4" style="display: none">
                    <div class="row" >  
                      <div class="col-md-12 form-group">
                        <label>Tipo de persona moral cuando es "Otra"</label>
                        <input class="form-control" type="text" name="tipo_persona_moral_otra4" maxlength="200" value="<?php echo $tipo_persona_moral_otra4 ?>">
                      </div>
                    </div>
                  </div>  
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Denominación o razón social de la persona moral que se constituye</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="denominacion_razon4" value="<?php echo $denominacion_razon4 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantil4">
                        <?php foreach ($actividad_econominica_morales_get as $a){ ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$giro_mercantil4) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o  antecedentes registrales</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="folio_mercantil4" value="<?php echo $folio_mercantil4 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Número total de acciones o partes sociales de la persona moral</label>
                      <input class="form-control" type="number" name="numero_total_acciones4" maxlength="17" value="<?php echo $numero_total_acciones4 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Clave de entidad federativa del domicilio social</label>
                      <select class="form-control" name="entidad_federativa4">
                      <?php foreach ($estados_get as $item){ ?>
                        <option value="<?php echo $item->id ?>" <?php if($item->id==$entidad_federativa4) echo 'selected' ?>><?php echo $item->estado ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div>  
                  <div class="row">
                    <div class="col-md-12">
                      <label>Se cuenta con un consejo de vigilancia</label>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="consejo_vigilancia4" value="SI" <?php if($consejo_vigilancia4=='SI') echo 'checked' ?>>
                          SI
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="consejo_vigilancia4" value="NO" <?php if($consejo_vigilancia4=='NO') echo 'checked' ?>>
                         NO
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Motivo de la constitución de la persona moral</label>
                      <select class="form-control" name="motivo_constitucion4">
                        <option value="1" <?php if($motivo_constitucion4==1) echo 'selected' ?>>Fusión</option>
                        <option value="2" <?php if($motivo_constitucion4==2) echo 'selected' ?>>Escisión</option>
                        <option value="3" <?php if($motivo_constitucion4==3) echo 'selected' ?>>Contrato privado</option>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Número de Instrumento Público de la fusión o escisión</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="instrumento_publico4" maxlength="20" value="<?php echo $instrumento_publico4 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NIPFE_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                </form>    
                  <hr class="subsubtitle">
                  <h5>Datos de los accionistas o socios</h5> 
                  <div class="row_accionistas_socios">
                    <div class="accionistas_socios">
                    </div>
                  </div>
                  <h5>Capital Social de constitución</h5>
                <form id="form_actividad41">    
                  <div class="row">
                    <div class="col-md-4 form-group">
                        <label>Monto de Capital Fijo</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="number" name="capital_fijo4" onblur="ValidCapital(this)" value="<?php echo $capital_fijo4 ?>">
                        </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_MCV_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-4 form-group">
                      <label>Monto de Capital Variable</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="number" name="capital_variable4" onblur="ValidCapital(this)" value="<?php echo $capital_variable4 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_MCV_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                </form> 
              </div>
            </div>   
            <!--  -->
          </div> 
           <!--<form id="form_anexo5">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="5" id="tipo_actividad5" onclick="tipo_actividad_select(5)" <?php if($tipo_actividad==5) echo 'checked' ?>>
                      Organización de aportaciones de capital o cualquier otro tipo de recursos para la constitución, operacion y administración de sociedades mercantiles
                  </label>
                </div>
              </div>
            </div>  
          </form> --> 
          <div class="tipo_actividadtxt5" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad5">
                  <input type="hidden" name="actividad" value="<?php echo $actividad5 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Motivo de la aportación</label>
                      <select class="form-control" name="motivo_aportacion5">
                        <option value="1" <?php if($motivo_aportacion5 == 1) echo 'selected' ?>>Constitución</option>
                        <option value="2" <?php if($motivo_aportacion5 == 2) echo 'selected' ?>>Operación</option>
                        <option value="3" <?php if($motivo_aportacion5 == 3) echo 'selected' ?>>Administración</option>
                      </select>
                    </div>
                  </div>  
                  <hr class="subsubtitle">
                  <h5>Datos de la contraparte</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona5" id="tipo_persona_5_1" value="1" onclick="tipo_persona_opt(5)" <?php if($tipo_persona5==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona5" id="tipo_persona_5_2" value="2" onclick="tipo_persona_opt(5)" <?php if($tipo_persona5==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona5" id="tipo_persona_5_3" value="3" onclick="tipo_persona_opt(5)" <?php if($tipo_persona5==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="tipo_persona_txt_5_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombre5" value="<?php echo $nombre5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paterno5" value="<?php echo $apellido_paterno5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_materno5" value="<?php echo $apellido_materno5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimiento5" value="<?php echo $fecha_nacimiento5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc5" maxlength="13" value="<?php echo $rfc5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp5" maxlength="18" value="<?php echo $curp5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_5_5" name="pais_nacionalidad5">
                          <?php if($pais_nacionalidad5!=""){
                           echo '<option value="'.$pais_nacionalidad5.'">'.$pais_nacionalidad5t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                      <div class="col-md-12 form-group">
                        <label>Actividad económica u ocupación</label>
                        <select class="form-control" name="actividad_economica5">
                          <?php foreach ($actividad_econominica_fisica_get as $a) { ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$actividad_economica5) echo 'selected' ?>><?php echo $a->acitividad ?></option>  
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                  </div>  
                  <div class="tipo_persona_txt_5_2" style="display: none">
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonm5" value="<?php echo $denominacion_razonm5 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionm5" value="<?php echo $fecha_constitucionm5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcm5" maxlength="13" value="<?php echo $rfcm5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_5_5" name="pais_nacionalidadm5">
                          <?php if($pais_nacionalidadm5!=""){
                           echo '<option value="'.$pais_nacionalidadm5.'">'.$pais_nacionalidadm5t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                      <div class="col-md-12 form-group">
                        <label>Actividad económica o giro mercantil u objeto social</label>
                        <select class="form-control" name="giro_mercantil5">
                          <?php foreach ($actividad_econominica_morales_get as $a) { ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$giro_mercantil5) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>  
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <hr> 
                  </div>  
                  <div class="tipo_persona_txt_5_3" style="display: none">
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonf5" value="<?php echo $denominacion_razonf5 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-5 form-group">
                        <label>RFC del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf5" maxlength="13" value="<?php echo $rfcf5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="identificador_fideicomisof5" maxlength="40" value="<?php echo $identificador_fideicomisof5 ?>">
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos del tipo de Aportación</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aportacion_monetaria5" id="aportacion_monetaria5_1" value="1" onclick="aportacion_monetaria_opt5()" <?php if($aportacion_monetaria5==1) echo 'checked' ?>>
                          Monetaria
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aportacion_monetaria5" id="aportacion_monetaria5_2" value="2" onclick="aportacion_monetaria_opt5()" <?php if($aportacion_monetaria5==2) echo 'checked' ?>>
                          Inmueble
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aportacion_monetaria5" id="aportacion_monetaria5_3" value="3" onclick="aportacion_monetaria_opt5()" <?php if($aportacion_monetaria5==3) echo 'checked' ?>>
                          Otro bien aportado
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="aportacion_monetariatxt5_1" style="display: none">
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Instrumento monetario con el que se realizó la aportación</label>
                        <select class="form-control" name="instrumento_monetario5"> 
                          <?php foreach ($intrumento_monetario_get as $a){ ?>
                            <option value="<?php echo $a->clave ?>" <?php if($a->clave==$instrumento_monetario5) echo 'selected' ?>><?php echo $a->descripcion ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Tipo de moneda o divisa de la aportación</label>
                        <select class="form-control" name="moneda5"> 
                          <option value="0" <?php if($moneda5=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                          <option value="1" <?php if($moneda5=="1") echo "selected"; ?>>Dólar Americano</option>
                          <option value="2" <?php if($moneda5=="2") echo "selected"; ?>>Euro</option>
                          <option value="3" <?php if($moneda5=="3") echo "selected"; ?>>Libra Esterlina</option>
                          <option value="4" <?php if($moneda5=="4") echo "selected"; ?>>Dólar canadiense</option>
                          <option value="5" <?php if($moneda5=="5") echo "selected"; ?>>Yuan Chino</option>
                          <option value="6" <?php if($moneda5=="6") echo "selected"; ?>>Centenario</option>
                          <option value="7" <?php if($moneda5=="7") echo "selected"; ?>>Onza de Plata</option>
                          <option value="8" <?php if($moneda5=="8") echo "selected"; ?>>Onza de Oro</option>
                        </select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Monto de la aportación</label>
                        <input class="form-control" type="number" name="monto_operacion5" maxlength="17" value="<?php echo $monto_operacion5 ?>">
                      </div>
                    </div>
                  </div>
                  <div class="aportacion_monetariatxt5_2" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Tipo de inmueble</label>
                        <select class="form-control" name="tipo_inmueble5">
                        <?php foreach ($tipo_inmueble_get as $item){ ?>
                          <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmueble5) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } ?>  
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Código Postal de la ubicación del inmueble</label>
                        <input class="form-control" pattern='\d*' maxlength="5" type="text" name="codigo_postal5" maxlength="5" value="<?php echo $codigo_postal5 ?>">
                      </div>
                      <div class="col-md-12 form-group">
                        <label>Folio real del inmueble o antecedentes registrales</label>
                        <input class="form-control" type="text" name="folio_real5" value="<?php echo $folio_real5 ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Valor de la aportación expresado en Moneda Nacional</label>
                        <input class="form-control" type="number" name="valor_aportacion5" maxlength="17" value="<?php echo $valor_aportacion5 ?>">
                      </div>
                    </div>
                  </div>
                  <div class="aportacion_monetariatxt5_3" style="display: none">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Descripción del bien aportado</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <textarea class="form-control" name="descripcion5"><?php echo $descripcion5 ?></textarea>
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DDBA_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Valor de la aportación expresado en Moneda Nacional</label>
                        <input class="form-control" type="number" name="valor_aportaciono5" maxlength="17" value="<?php echo $valor_aportaciono5 ?>">
                      </div>
                    </div>  
                  </div>
                </form>
              </div>
            </div>   
            <!--  -->
          </div> 
           <!--<form id="form_anexo6">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="6" id="tipo_actividad6" onclick="tipo_actividad_select(6)" <?php if($tipo_actividad==6) echo 'checked' ?>>
                      Fusión
                  </label>
                </div>
              </div>
            </div>  
          </form> --> 
          <div class="tipo_actividadtxt6" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad6">
                  <input type="hidden" name="actividad" value="<?php echo $actividad6 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <label>Tipo de fusión</label>
                      <select class="form-control" name="tipo_fusion6">
                        <option value="1" <?php if($tipo_fusion6 == 1) echo 'selected' ?>>Absorción</option>
                        <option value="2" <?php if($tipo_fusion6 == 2) echo 'selected' ?>>Integración</option>
                      </select>
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de las Personas Morales Fusionadas</h5>
                  <h5>Datos de la Persona Moral Fusionada</h5>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="denominacion_razonm6" value="<?php echo $denominacion_razonm6 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionm6" value="<?php echo $fecha_constitucionm6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcm6" maxlength="13" value="<?php echo $rfcm6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_6_6" name="pais_nacionalidadm6">
                        <?php if($pais_nacionalidadm6!=""){
                         echo '<option value="'.$pais_nacionalidadm6.'">'.$pais_nacionalidadm6t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantil6">
                        <?php foreach ($actividad_econominica_morales_get as $a){ ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$giro_mercantil6) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div> 
                  <div class="row">  
                    <div class="col-md-4 form-group">
                      <label>Capital social fijo</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="number" name="capital_social_fijo6" minlength="4" maxlength="17" value="<?php echo $capital_social_fijo6 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_CS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Capital social Variable</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="number" name="capital_social_variable6" minlength="4" maxlength="17" value="<?php echo $capital_social_variable6 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_CS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o  antecedentes registrales</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="folio_mercantil6" value="<?php echo $folio_mercantil6 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                  <h5>Datos de la Persona Moral Fusionante</h5>
                  <div class="row">
                    <div class="col-md-12">
                      <label>La persona moral fusionante ya se encuentra determinada</label>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="fusionante_determinadas6" value="SI" <?php if($fusionante_determinadas6=='SI') echo 'checked' ?>>
                          SI
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="fusionante_determinadas6" value="NO" <?php if($fusionante_determinadas6=='NO') echo 'checked' ?>>
                         NO
                        </label>
                      </div>
                    </div>
                  </div>
                  <h5>Fusionante</h5>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="denominacion_razonf6" value="<?php echo $denominacion_razonf6 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionf6" value="<?php echo $fecha_constitucionf6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf6" maxlength="13" value="<?php echo $rfcf6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_6_6" name="pais_nacionalidadf6">
                        <?php if($pais_nacionalidadf6!=""){
                         echo '<option value="'.$pais_nacionalidadf6.'">'.$pais_nacionalidadf6t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilf6">
                        <?php foreach ($actividad_econominica_morales_get as $a){ ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$giro_mercantilf6) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div> 
                  <div class="row">  
                    <div class="col-md-4 form-group">
                      <label>Capital social fijo</label>
                      <input class="form-control" type="number" name="capital_social_fijof6" minlength="4" maxlength="17" value="<?php echo $capital_social_fijof6 ?>">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Capital social Variable</label>
                      <input class="form-control" type="number" name="capital_social_variablef6" minlength="4" maxlength="17" value="<?php echo $capital_social_variablef6 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o  antecedentes registrales</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="folio_mercantilf6" value="<?php echo $folio_mercantilf6 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Número total de acciones o partes sociales de la persona moral</label>
                      <input class="form-control" type="number" name="numero_total_accionesf6" maxlength="17" value="<?php echo $numero_total_accionesf6 ?>">
                    </div>
                  </div>
                </form>
                  <hr class="subsubtitle">
                  <h5>Datos de los accionistas o socios de la persona moral Fusionante</h5>
                  <div class="row_accionistas_socios6">
                    <div class="accionistas_socios6">
                    </div>
                  </div>
              </div>
            </div>   
            <!--  -->
          </div>  
           <!--<form id="form_anexo7">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="7" id="tipo_actividad7" onclick="tipo_actividad_select(7)" <?php if($tipo_actividad==7) echo 'checked' ?>>
                      Escisión
                  </label>
                </div>
              </div>
            </div>  
          </form> --> 
          <div class="tipo_actividadtxt7" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad7">
                  <input type="hidden" name="actividad" value="<?php echo $actividad7 ?>">
                  <!-- -->  
                  <h5>Datos de la Persona Moral Escindente </h5>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="denominacion_razonm7" value="<?php echo $denominacion_razonm7 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionm7" value="<?php echo $fecha_constitucionm7 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcm7" maxlength="13" value="<?php echo $rfcm7 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_7_7" name="pais_nacionalidadm7">
                        <?php if($pais_nacionalidadm7!=""){
                         echo '<option value="'.$pais_nacionalidadm7.'">'.$pais_nacionalidadm7t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantil7">
                        <?php foreach ($actividad_econominica_morales_get as $a) { ?>
                        <option value="<?php echo $a->clave ?>" <?php if($a->clave==$giro_mercantil7) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>  
                        <?php } ?>
                      </select>
                    </div>
                  </div> 
                  <div class="row">  
                    <div class="col-md-4 form-group">
                      <label>Capital social fijo</label>
                      <input class="form-control" type="number" name="capital_social_fijo7" minlength="4" maxlength="17" value="<?php echo $capital_social_fijo7 ?>">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Capital social Variable</label>
                      <input class="form-control" type="number" name="capital_social_variable7" minlength="4" maxlength="17" value="<?php echo $capital_social_variable7 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o  antecedentes registrales</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="folio_mercantil7" value="<?php echo $folio_mercantil7 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>La persona moral escindente subsiste</label>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="escindente_subsiste7" value="SI" <?php if($escindente_subsiste7=='SI') echo 'checked' ?>>
                          SI
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="escindente_subsiste7" value="NO" <?php if($escindente_subsiste7=='NO') echo 'checked' ?>>
                         NO
                        </label>
                      </div>
                    </div>
                  </div>
                  <!-- -->
                </form>
                  <!-- -->
                  <hr class="subsubtitle">
                  <h5>Datos de los accionistas o socios de la persona moral escindente cuando esta subsiste</h5>
                  <div class="row_accionistas_socios7">
                    <div class="accionistas_socios7">
                    </div>
                  </div>
                <form id="form_actividad71">
                  <hr class="subsubtitle">
                  <h5>Datos de las Personas Morales Escindidas </h5>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Las personas morales escindidas ya se encuentran determinadas</label>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="escindidas_determinadas7" value="SI" <?php if($escindidas_determinadas7=='SI') echo 'checked' ?>>
                          SI
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="escindidas_determinadas7" value="NO" <?php if($escindidas_determinadas7=='NO') echo 'checked' ?>>
                         NO
                        </label>
                      </div>
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la Persona Moral Escindida</h5>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="denominacion_razonf7" value="<?php echo $denominacion_razonf7 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionf7" value="<?php echo $fecha_constitucionf7 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf7" maxlength="13" value="<?php echo $rfcf7 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_7_7" name="pais_nacionalidadf7">
                        <?php if($pais_nacionalidadf7!=""){
                         echo '<option value="'.$pais_nacionalidadf7.'">'.$pais_nacionalidadf7t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilf7">
                        <?php foreach ($actividad_econominica_morales_get as $a){ ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$giro_mercantilf7) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div> 
                  <div class="row">  
                    <div class="col-md-4 form-group">
                      <label>Capital social fijo</label>
                      <input class="form-control" type="number" name="capital_social_fijof7" minlength="4" maxlength="17" value="<?php echo $capital_social_fijof7 ?>">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Capital social Variable</label>
                      <input class="form-control" type="number" name="capital_social_variablef7" minlength="4" maxlength="17" value="<?php echo $capital_social_variablef7 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o  antecedentes registrales</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="folio_mercantilf7" value="<?php echo $folio_mercantilf7 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Número total de acciones o partes sociales de la persona moral</label>
                      <input class="form-control" type="number" name="numero_total_accionesf7" maxlength="17" value="<?php echo $numero_total_accionesf7 ?>">
                    </div>
                  </div>
                </form>  
                  <hr class="subsubtitle">
                  <h5>Datos de los accionistas o socios de la persona moral Escindida</h5> 
                  <div class="row_accionistas_socios72">
                    <div class="accionistas_socios72">
                    </div>
                  </div>
              </div>
            </div>   
            <!--  -->
          </div>  
          <!-- <form id="form_anexo8">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="8" id="tipo_actividad8" onclick="tipo_actividad_select(8)" <?php if($tipo_actividad==8) echo 'checked' ?>>
                      Operación y administración de personas morales y vehículos corporativos
                  </label>
                </div>
              </div>
            </div>  
          </form>--> 
          <div class="tipo_actividadtxt8" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad8">
                  <input type="hidden" name="actividad" value="<?php echo $actividad8 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Descripción del tipo de Administración</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <textarea name="tipo_administracion8" class="form-control"><?php echo $tipo_administracion8 ?></textarea>
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DDTAYO_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Descripción del tipo de Operación</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <textarea name="tipo_operacion8"class="form-control"><?php echo $tipo_operacion8 ?></textarea>
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DDTAYO_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>La persona moral o vehículo corporativo que se adminsitra es la persona opjeto del aviso</label>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="persona_moral_aviso8" value="SI" <?php if($persona_moral_aviso8=='SI') echo 'checked' ?>>
                          SI
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="persona_moral_aviso8" value="NO" <?php if($persona_moral_aviso8=='NO') echo 'checked' ?>>
                         NO
                        </label>
                      </div>
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la persona moral o vehículo corporativo que se adminsitra</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona8" id="tipo_persona_8_2" value="2" onclick="tipo_persona_opt(8)" <?php if($tipo_persona8==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona8" id="tipo_persona_8_3" value="3" onclick="tipo_persona_opt(8)" <?php if($tipo_persona8==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="tipo_persona_txt_8_2" style="display: none">
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonm8" value="<?php echo $denominacion_razonm8 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionm8" value="<?php echo $fecha_constitucionm8 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcm8" maxlength="13" value="<?php echo $rfcm8 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_8_8" name="pais_nacionalidadm8">
                          <?php if($pais_nacionalidadm8!=""){
                           echo '<option value="'.$pais_nacionalidadm8.'">'.$pais_nacionalidadm8t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <hr> 
                  </div>  
                  <div class="tipo_persona_txt_8_3" style="display: none">
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonf8" value="<?php echo $denominacion_razonf8 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf8" maxlength="13" value="<?php echo $rfcf8 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="identificador_fideicomisof8" maxlength="40" value="<?php echo $identificador_fideicomisof8 ?>">
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                </form>
              </div>
            </div>   
            <!--  -->
          </div>   
          <!-- <form id="form_anexo9">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="9" id="tipo_actividad9" onclick="tipo_actividad_select(9)" <?php if($tipo_actividad==9) echo 'checked' ?>>
                      Constitución de Fideicomiso
                  </label>
                </div>
              </div>
            </div>  
          </form> -->  
          <div class="tipo_actividadtxt9" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad9">
                  <input type="hidden" name="actividad" value="<?php echo $actividad9 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf9" maxlength="13" value="<?php echo $rfcf9 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="identificador_fideicomisof9" maxlength="40" value="<?php echo $identificador_fideicomisof9 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="denominacion_razonf9" value="<?php echo $denominacion_razonf9 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Objeto del Fideicomiso</label>
                      <select class="form-control" name="objeto_fideicomiso9">
                        <?php foreach ($actividad_econominica_morales_get as $a){ ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$objeto_fideicomiso9) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Monto total del patrimonio del fideicomiso en Moneda Nacional</label>
                      <input class="form-control" type="number" name="monto_total_patrimonio9" onblur="ValidCapital(this)" value="<?php echo $monto_total_patrimonio9 ?>">
                    </div>
                  </div> 
                </form>  
                  <hr class="subsubtitle">
                  <h5>Datos de los fideicomitentes</h5>
                  <div class="row_accionistas_socios9">
                    <div class="accionistas_socios9">
                    </div>
                  </div>
                <form id="form_actividad91">
                  <hr class="subsubtitle">
                  <h5>Datos del tipo de patrimonio</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aportacion_monetaria9" id="aportacion_monetaria9_1" value="1" onclick="aportacion_monetaria_opt9()" <?php if($aportacion_monetaria9==1) echo 'checked' ?>>
                          Monetaria
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aportacion_monetaria9" id="aportacion_monetaria9_2" value="2" onclick="aportacion_monetaria_opt9()" <?php if($aportacion_monetaria9==2) echo 'checked' ?>>
                          Inmueble
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aportacion_monetaria9" id="aportacion_monetaria9_3" value="3" onclick="aportacion_monetaria_opt9()" <?php if($aportacion_monetaria9==3) echo 'checked' ?>>
                          Otro bien aportado
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="aportacion_monetariatxt9_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Tipo de moneda o divisa de la aportación</label>
                        <select class="form-control" name="moneda9"> 
                          <?php /* foreach ($tipo_moneda_get as $t){
                            if($t->clave==1){   
                            ?>
                            <option value="<?php echo $t->clave ?>" <?php if($t->clave==$moneda9) echo 'selected' ?>><?php echo $t->moneda ?></option>
                          <?php }
                          } */ ?>
                          <option value="0" <?php if($moneda9=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                          <option value="1" <?php if($moneda9=="1") echo "selected"; ?>>Dólar Americano</option>
                          <option value="2" <?php if($moneda9=="2") echo "selected"; ?>>Euro</option>
                          <option value="3" <?php if($moneda9=="3") echo "selected"; ?>>Libra Esterlina</option>
                          <option value="4" <?php if($moneda9=="4") echo "selected"; ?>>Dólar canadiense</option>
                          <option value="5" <?php if($moneda9=="5") echo "selected"; ?>>Yuan Chino</option>
                          <option value="6" <?php if($moneda9=="6") echo "selected"; ?>>Centenario</option>
                          <option value="7" <?php if($moneda9=="7") echo "selected"; ?>>Onza de Plata</option>
                          <option value="8" <?php if($moneda9=="8") echo "selected"; ?>>Onza de Oro</option>
                        </select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Monto de la aportación</label>
                        <input class="form-control" type="number" name="monto_operacion9" maxlength="17" value="<?php echo $monto_operacion9 ?>">
                      </div>
                    </div>
                  </div>
                  <div class="aportacion_monetariatxt9_2" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Tipo de inmueble</label>
                        <select class="form-control" name="tipo_inmueble9">
                        <?php foreach ($tipo_inmueble_get as $item){ ?>
                          <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmueble9) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } ?>  
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Código Postal de la ubicación del inmueble</label>
                        <input class="form-control" pattern='\d*' maxlength="5" type="text" name="codigo_postal9" maxlength="5" value="<?php echo $codigo_postal9 ?>">
                      </div>
                      <div class="col-md-12 form-group">
                        <label>Folio real del inmueble o antecedentes registrales</label>
                        <input class="form-control" type="text" name="folio_real9" value="<?php echo $folio_real9 ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Importe que garantiza el inmueble que se aporta al Fideicomiso en Moneda Nacional</label>
                        <input class="form-control" type="number" name="importe_garantia9" maxlength="17" value="<?php echo $importe_garantia9 ?>">
                      </div>
                    </div>
                  </div>
                  <div class="aportacion_monetariatxt9_3" style="display: none">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Descripción del bien aportado</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <textarea class="form-control" name="descripcion9"><?php echo $descripcion9 ?></textarea>
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DDBA_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Valor del bien que se aporta al Fideicomiso en Moneda Nacional</label>
                        <input class="form-control" type="number" name="valor_aportaciono9" maxlength="17" value="<?php echo $valor_aportaciono9 ?>">
                      </div>
                    </div>  
                  </div>
                </form>  
                  <hr class="subsubtitle">
                  <h5>Datos de los fideicomisarios</h5>
                  <div class="row_accionistas_socios92">
                    <div class="accionistas_socios92">
                    </div>
                  </div>
                <form id="form_actividad92">
                  <hr class="subsubtitle">
                  <h5>Datos de los miembros del Comité Técnico</h5>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Se cuenta con un comité técnico</label>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="comite_tecnico9" value="SI" <?php if($comite_tecnico9=='SI') echo 'checked' ?>>
                          SI
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="comite_tecnico9" value="NO" <?php if($comite_tecnico9=='NO') echo 'checked' ?>>
                         NO
                        </label>
                      </div>
                    </div>
                  </div>
                </form>

              </div>
            </div>   
            <!--  -->
          </div> 
           <!--<form id="form_anexo10">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_actividad" value="10" id="tipo_actividad10" onclick="tipo_actividad_select(10)" <?php if($tipo_actividad==10) echo 'checked' ?>>
                      Compra o venta de entidades mercantiles
                  </label>
                </div>
              </div>
            </div>  
          </form>--> 
          <div class="tipo_actividadtxt10" style="display: none">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_actividad10">
                  <input type="hidden" name="actividad" value="<?php echo $actividad10 ?>">
                  <!-- -->  
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <label>Tipo de Operación de compraventa</label>
                      <select class="form-control" name="tipo_operacion10">
                        <option value="1" <?php if($tipo_operacion10 == 1) echo 'selected' ?>>Compra de la entidad mercantil</option>
                        <option value="2" <?php if($tipo_operacion10 == 2) echo 'selected' ?>>Venta de la entidad mercantil</option>
                      </select>
                    </div>
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la sociedad mercantil que se comercializa</h5>
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Denominación o razón social de la persona moral que se constituye</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="denominacion_razon10" value="<?php echo $denominacion_razon10 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantil10">
                        <?php foreach ($actividad_econominica_morales_get as $a){ ?>
                          <option value="<?php echo $a->clave ?>" <?php if($a->clave==$giro_mercantil10) echo 'selected' ?>><?php echo $a->giro_mercantil ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucion10" value="<?php echo $fecha_constitucion10 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc10" maxlength="13" value="<?php echo $rfc10 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_10_10" name="pais_nacionalidad10">
                          <?php if($pais_nacionalidad10!=""){
                           echo '<option value="'.$pais_nacionalidad10.'">'.$pais_nacionalidad10t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    <div class="col-md-12 form-group">
                      <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o  antecedentes registrales</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="folio_mercantil10" value="<?php echo $folio_mercantil10 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Número de acciones o partes sociales adquiridas</label>
                      <input class="form-control" type="number" name="acciones_adquiridas10" maxlength="17" value="<?php echo $acciones_adquiridas10 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Número total de acciones</label>
                      <input class="form-control" type="number" name="acciones_totales10" maxlength="17" value="<?php echo $acciones_totales10 ?>">
                    </div>
                  </div>  
                  <hr class="subsubtitle">
                  <h5>Datos de la Contraparte</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona10" id="tipo_persona_10_1" value="1" onclick="tipo_persona_opt(10)" <?php if($tipo_persona10==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona10" id="tipo_persona_10_2" value="2" onclick="tipo_persona_opt(10)" <?php if($tipo_persona10==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_persona10" id="tipo_persona_10_3" value="3" onclick="tipo_persona_opt(10)" <?php if($tipo_persona10==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="tipo_persona_txt_10_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombre10" value="<?php echo $nombre10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paterno10" value="<?php echo $apellido_paterno10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_materno10" value="<?php echo $apellido_materno10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimiento10" value="<?php echo $fecha_nacimiento10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfi10" maxlength="13" value="<?php echo $rfcfi10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp10" maxlength="18" value="<?php echo $curp10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_10_10" name="pais_nacionalidadfi10">
                          <?php if($pais_nacionalidadfi10!=""){
                           echo '<option value="'.$pais_nacionalidadfi10.'">'.$pais_nacionalidadfi10t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                  </div>  
                  <div class="tipo_persona_txt_10_2" style="display: none">
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonm10" value="<?php echo $denominacion_razonm10 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionm10" value="<?php echo $fecha_constitucionm10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcm10" maxlength="13" value="<?php echo $rfcm10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_10_10" name="pais_nacionalidadm10">
                          <?php if($pais_nacionalidadm10!=""){
                           echo '<option value="'.$pais_nacionalidadm10.'">'.$pais_nacionalidadm10t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <hr> 
                  </div>  
                  <div class="tipo_persona_txt_10_3" style="display: none">
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="denominacion_razonf10" value="<?php echo $denominacion_razonf10 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DRS_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      <div class="col-md-5 form-group">
                        <label>RFC del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf10" maxlength="13" value="<?php echo $rfcf10 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="identificador_fideicomisof10" maxlength="40" value="<?php echo $identificador_fideicomisof10 ?>">
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                  <hr class="subsubtitle">
                  <h5>Datos de la operación financiera</h5>
                  <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Fecha de la operación financiera</label>
                        <input class="form-control" type="date" name="fecha_pago10" value="<?php echo $fecha_pago10 ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Forma de pago</label>
                        <select class="form-control" name="forma_pago10"> 
                          <?php foreach ($forma_pago_get as $f){ ?>
                            <option value="<?php echo $f->clave ?>" <?php if($f->clave==$forma_pago10) echo 'selected' ?>><?php echo $f->descripcion ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Instrumento monetario con el que se realizó la operación </label>
                        <select class="form-control" name="instrumento_monetario10"> 
                          <?php foreach ($intrumento_monetario_get as $a){ ?>
                            <option value="<?php echo $a->clave ?>" <?php if($a->clave==$instrumento_monetario10) echo 'selected' ?>><?php echo $a->descripcion ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Tipo de moneda o divisa de la operación</label>
                        <select class="form-control" name="moneda10"> 
                          <?php /* foreach ($tipo_moneda_get as $t){
                            if($t->clave==1){   
                            ?>
                            <option value="<?php echo $t->clave ?>" <?php if($t->clave==$moneda10) echo 'selected' ?>><?php echo $t->moneda ?></option>
                          <?php }
                          } */ ?>
                          <option value="0" <?php if($moneda10=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                          <option value="1" <?php if($moneda10=="1") echo "selected"; ?>>Dólar Americano</option>
                          <option value="2" <?php if($moneda10=="2") echo "selected"; ?>>Euro</option>
                          <option value="3" <?php if($moneda10=="3") echo "selected"; ?>>Libra Esterlina</option>
                          <option value="4" <?php if($moneda10=="4") echo "selected"; ?>>Dólar canadiense</option>
                          <option value="5" <?php if($moneda10=="5") echo "selected"; ?>>Yuan Chino</option>
                          <option value="6" <?php if($moneda10=="6") echo "selected"; ?>>Centenario</option>
                          <option value="7" <?php if($moneda10=="7") echo "selected"; ?>>Onza de Plata</option>
                          <option value="8" <?php if($moneda10=="8") echo "selected"; ?>>Onza de Oro</option>
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Monto de la liquidación</label>
                        <input class="form-control" type="number" name="monto_operacion10" minlength="4" maxlength="17" value="<?php echo $monto_operacion10 ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Tipo de Activo Virtual</label>
                        <select class="form-control" name="activo_virtual" id="activo_virtual">
                          <option value=""></option>
                          <?php foreach ($activos as $key){
                            if($activo_virtual == $key->clave){ $sel="Selected"; }else{ $sel=""; } ?>
                          <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->nombre; ?></option>                
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Nombre del activo virtual si no está registrado en el catálogo</label>
                        <input class="form-control" type="text" name="nombre_otro_activo" value="<?php echo $nombre_otro_activo; ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Cantidad del activo virtual de la operación</label>
                        <input class="form-control" type="number" name="cant_activo" value="<?php echo $cant_activo; ?>">
                      </div>
                  </div>    
                </form>
              </div>
            </div>   
            <!--  -->
          </div>   
        <!-- -->
        <div class="modal-footer">
          <button class="btn gradient_nepal2" type="button" id="btn_submit" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
          <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
        </div>
        <!-- -->
    </div>
  </div>
</div>
<!-- Operaciones -->
<!--------------Modal-------------->
<div class="modal fade" id="modal_fecha_opera" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Fecha de Operación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Indicar la fecha de suscripción del contrato, instrumento o título de crédito correspondiente. Cuando aplique</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--------------Modal-------------->
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 carácteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_MCV_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Monto de capital fijo y variable</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Minimo 4 caracteres</li>
           <li>Máximo 17 caracteres</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_CS_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Capital social fijo y variable</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Minimo 4 caracteres</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_DRS_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        <h5>Denominación o razón social</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Minimo 1 caracter.</li>
           <li>Máximo 254 caracteres.</li>
           <li>Acepta letras, números, espacio, punto, coma, guion bajo, guión medio, apostrofe, gato y &.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_DTA_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Capital social fijo y variable</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Minimo 4 caracter</li>
           <li>Máximo 2000 caracter</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- / -->
<div class="modal fade" id="modal_folio_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio real del inmueble o antecedentes catastrales</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>No dejar espacios en blanco, separar caracteres con guión medio o bajo.</li>
           <li>Capturar el folio real físico o electrónico del inmueble, en caso de no contar con uno, se deben capturar los datos de los antecedentes registrales, si no se cuenta con lo anterior, capturar XXXX.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_NRIF_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número, referencia o identificador fideicomiso</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 carácter, máxima 40 caracteres.</li>
           <li>Acepta letras, números, ceros a la izquierda, coma, @, &, guión bajo, y guión medio.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_nom_insti_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Nombre de la institución financiera donde se mantiene la cuenta</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Longitud mínima 1 carácter, máxima 500 caracteres.</li>
           <li>Acepta letras, números, diagonal, apóstrofe, signo de pesos, guión medio.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_numcta__ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de cuenta, contrato o póliza</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Longitud mínima 1 carácter, máxima 18 caracteres.</li>
           <li>Acepta letras, números, ceros a la izquierda</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_NIPFE_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de Instrumento Público de la fusión o escisión</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 20 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda,guión bajo, y guión medio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliomer_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio mercantil o antecedentes registrales</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 200 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda,guión bajo, y guión medio.</li>
           <li>Capturar el folio mercantil o de inscripción en el registro que corresponda, en caso de no contar con uno por el marco juridico local, se deberán establece los datos de los antecedentes registrales según corresponda.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_DDBA_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Descripción del bien aportado</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 3000 caracteres.</li>
           <li>Acepta letras, numeros, espacio, coma, punto, dos puntos, diagonal, apostrofe, signo de pesos y guion medio.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_DDTAYO_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Ayuda</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Longitud mínima 1 caracter, máxima 2000 caracteres.</li>
           <li>Acepta letras, números, espacio, coma, punto, dos puntos y diagonal.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--modals modificatorio de ayuda-->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>