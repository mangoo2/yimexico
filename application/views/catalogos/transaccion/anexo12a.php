<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<!-- Tipo actividad 2-->
<input type="hidden" id="aux_cpm" value="<?php echo $aux_cpm ?>">
<!-- Tipo actividad 3-->
<input type="hidden" id="aux_mps" value="<?php echo $aux_mps ?>">
<!-- Tipo actividad 4-->
<input type="hidden" id="aux_fus" value="<?php echo $aux_fus ?>">
<!-- Tipo actividad 5-->
<input type="hidden" id="aux_esc" value="<?php echo $aux_esc ?>">
<input type="hidden" id="aux_esci" value="<?php echo $aux_esci ?>">
<input type="hidden" id="fecha_max" value="<?php echo date("Y-m-d"); ?>">
<!-- Tipo actividad 7-->
<input type="hidden" id="aux_fide" value="<?php echo $aux_fide ?>">
<input type="hidden" id="aux_fidei" value="<?php echo $aux_fidei ?>">
<!-- -->
<input type="hidden" id="aux_pga" value="<?php echo $aux_pga ?>">
<input type="hidden" id="aux_tipo_actividad" value="<?php echo $tipo_actividad ?>">
<input type="hidden" id="id_aux" value="<?php echo $id ?>">
<div class="row">
      <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h3>Anexo 12 A - Fedatarios Públicos</h3>  
            <h4>Registro de transacción</h4>
          </div>
        </div>
        <!--- --->
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <input type="hidden" id="aux_pga" value="<?php ?>">
        <form id="form_anexo" class="forms-sample">
          <input type="hidden" name="id" value="<?php echo $id ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Fecha de operación o acto </label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input type="text" name="referencia" id="referencia" class="form-control" placeholder="Ingrese número de referencia" required="" value="<?php echo $referencia ?>">
              </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group" style="display: none;">
              <label>Monto de factura</label>
              <input type="text" id="monto_opera" class="form-control monto_1" onchange="tipo_modeda(1)" placeholder="Ingrese el monto total de la operación ($)" value="<?php echo $monto_opera ?>">
            </div>
            <!--<div class="col-md-3 form-group">
            </div>-->
            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-3 form-group" style="display: none;">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input type="text" name="num_factura" id="num_factura" class="form-control" placeholder="Ingrese número de factura" value="<?php echo $num_factura ?>">
              </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-8 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-4 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
         
          <?php if($aviso==1){ ?>
          <hr class="subsubtitle">
          <div class="row">  
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>
            <div class="col-md-2 form-group">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div> 
            </div>
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="idanexo12a" id="idanexo12a" class="form-control" value="<?php echo $idanexo12a ?>">
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div> 
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90"><?php echo $descrip_modifica ?></textarea>
            </div>
          </div>     
          <?php }?>
          <!-- =========    Documento  ========== -->
          <hr class="subtitle">
          <div class="row">
              <div class="col-md-4 form-group">
                    <label>Número de Instrumento Público</label>
                    <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" maxlength="20" name="instrumento_publico" value="<?php echo $instrumento_publico ?>">
                    </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_intrumentoPublico_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button>              
                </div>
              </div>
          </div>
          <hr class="subsubtitle">
          <h5>Tipo de Actividad Vulnerable</h5>
          <div class="row">
            <div class="col-md-12 form-group">
                <label></label>
                <select class="form-control" id="tipo_actividad" name="tipo_actividad">
                  <option value="" disabled="">Seleccione una actividad</option>
                  <option <?php if($tipo_actividad==1){ echo"selected"; } ?> value="1" >Otorgamiento de poder irrevocable para actos de administración o para actos de dominio</option>
                  <option <?php if($tipo_actividad==2){ echo"selected"; } ?> value="2" >Constitución de personas Morales</option>
                  <option <?php if($tipo_actividad==3){ echo"selected"; } ?> value="3" >Modificacion patrimonial por aumento de capital o por disminucion de capital</option>
                  <option <?php if($tipo_actividad==4){ echo"selected"; } ?> value="4" >Fusión</option>
                  <option <?php if($tipo_actividad==5){ echo"selected"; } ?> value="5" >Escisión</option>
                  <option <?php if($tipo_actividad==6){ echo"selected"; } ?> value="6" >Compra o venta de acciones o partes sociales</option>
                  <option <?php if($tipo_actividad==7){ echo"selected"; } ?> value="7" >Constitución o Modificación de Fideicomiso traslativo de Dominio o  en garantía sobre inmuebles</option>
                  <option <?php if($tipo_actividad==8){ echo"selected"; } ?> value="8" >Cesión de derechos de fideicomitente</option>
                  <option <?php if($tipo_actividad==9){ echo"selected"; } ?> value="9" >Otorgamiento de contratos de mutuo o crédito con o sin garantía</option>
                  <option <?php if($tipo_actividad==10){ echo"selected"; } ?> value="10" >Realización de Avalúos</option>
                </select>
            </div>
          </div>
          <!-- tipo_actividad1 1-->

          <div class="otorgamiento_podertxt" style="display: none">
            <hr>
            <h4>Datos del poderante</h4>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <h5>Tipo de Persona</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personapo" id="tipo_persona1" value="1" onclick="tipo_persona_select()" checked <?php if($tipo_personapo==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personapo" id="tipo_persona2" value="2" onclick="tipo_persona_select()" <?php if($tipo_personapo==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personapo" id="tipo_persona3" value="3" onclick="tipo_persona_select()" <?php if($tipo_personapo==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="persona_fisicatxt" style="display: none">
                  <hr>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombrepo" value="<?php echo $nombrepo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternopo" value="<?php echo $apellido_paternopo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternopo" value="<?php echo $apellido_maternopo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientopo" value="<?php echo $fecha_nacimientopo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcpo" maxlength="13" value="<?php echo $rfcpo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curppo" maxlength="18" value="<?php echo $curppo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_f_1" name="pais_nacionalidadpo">
                        <?php if($pais_nacionalidadpo!=""){
                         echo '<option value="'.$clavepo.'">'.$pais_nacionalidadpog.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica u ocupación</label>
                      <select class="form-control" name="actividad_economicapo">
                      <?php foreach ($actividad_econominica_fisica_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$actividad_economicapo) echo 'selected' ?>><?php echo $item->acitividad ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>
                <div class="persona_moraltxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmpo" value="<?php echo $denominacion_razonmpo ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmpo" value="<?php echo $fecha_constitucionmpo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmpo" maxlength="13" value="<?php echo $rfcmpo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_m_1" name="pais_nacionalidadmpo">
                        <?php if($pais_nacionalidadmpo!=""){
                         echo '<option value="'.$clavempo.'">'.$pais_nacionalidadmpog.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilmpo">
                      <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilmpo) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <div class="fideicomisotxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfpo" value="<?php echo $denominacion_razonfpo ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)"  name="rfcfpo" maxlength="13" value="<?php echo $rfcfpo ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofpo" maxlength="40" value="<?php echo $identificador_fideicomisofpo ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>

                  </div> 
                  <hr> 
                </div>
                <h5>Datos de los Apoderados</h5>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Tipo de poder:</label>
                    <select class="form-control" name="tipo_poder">
                      <option value="1" <?php if($tipo_poder==1) echo 'selected' ?>>de administración</option>
                      <option value="2" <?php if($tipo_poder==2) echo 'selected' ?>>de dominio</option>
                      <option value="3" <?php if($tipo_poder==3) echo 'selected' ?>>de administración y de dominio</option>
                    </select>
                  </div>
                </div>
                <hr>
                <h5>Tipo de Persona</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personap" id="tipo_personap1" value="1" onclick="tipo_personap1_select()" checked <?php if($tipo_personap==1) echo 'checked' ?>>
                        Persona fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personap" id="tipo_personap2" value="2" onclick="tipo_personap1_select()" <?php if($tipo_personap==2) echo 'checked' ?>>
                        Persona moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personap" id="tipo_personap3" value="3" onclick="tipo_personap1_select()" <?php if($tipo_personap==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="persona_fisicaptxt" style="display: none">
                  <hr>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombrep" value="<?php echo $nombrep ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternop" value="<?php echo $apellido_paternop ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternop" value="<?php echo $apellido_maternop ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientop" value="<?php echo $fecha_nacimientop ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcp" value="<?php echo $rfcp ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpp" value="<?php echo $curpp ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_pf_1" name="pais_nacionalidadp">
                        <?php if($pais_nacionalidadp!=""){
                         echo '<option value="'.$claveg.'">'.$pais_nacionalidadpg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>
                <div class="persona_moralptxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmp" value="<?php echo $denominacion_razonmp ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmp" value="<?php echo $fecha_constitucionmp ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmp" maxlength="13" value="<?php echo $rfcmp ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_pm_1" name="pais_nacionalidadmp">
                        <?php if($pais_nacionalidadmp!=""){
                         echo '<option value="'.$clavempg.'">'.$pais_nacionalidadmpg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <div class="fideicomisoptxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfp" value="<?php echo $denominacion_razonfp ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcf" maxlength="13" value="<?php echo $rfcf ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofp" maxlength="40" value="<?php echo $identificador_fideicomisofp ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()">
                        <i class="mdi mdi-help"></i>
                      </button>
                    </div>
                  </div> 
                  <hr> 
                </div>
              </div>
            </div>  
            <hr class="subsubtitle">
          </div>
        </div>   
        </form>  
          <div class="constitucion_personas_moralestxt" style="display: none">
            <!------  ------>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
              <form id="form_anexo2" class="forms-sample">  
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Tipo de persona moral</label>
                    <select class="form-control" name="tipo_persona_moral" id="tipo_persona_moral" onchange="tipo_persona_moral_otros()">
                      <option value="1" <?php if($tipo_persona_moral==1) echo 'selected' ?>>Asociación Civil (A.C.)</option>
                      <option value="2" <?php if($tipo_persona_moral==2) echo 'selected' ?>>Sociedad Civil (S.C.)</option>
                      <option value="3" <?php if($tipo_persona_moral==3) echo 'selected' ?>>Sociedad en Nombre Colectivo</option>
                      <option value="4" <?php if($tipo_persona_moral==4) echo 'selected' ?>>Comandita Simple (S. en C.)</option>
                      <option value="5" <?php if($tipo_persona_moral==5) echo 'selected' ?>>Comandita por Acciones (S. en C. por A.)</option>
                      <option value="6" <?php if($tipo_persona_moral==6) echo 'selected' ?>>Sociedad Anónima (S.A.)</option>
                      <option value="7" <?php if($tipo_persona_moral==7) echo 'selected' ?>>Sociedad de Responsabilidad Limitada (S. de R. L.)</option>
                      <option value="8" <?php if($tipo_persona_moral==8) echo 'selected' ?>>Sociedad Cooperativa Limitada (S. C. L.)</option>
                      <option value="9" <?php if($tipo_persona_moral==9) echo 'selected' ?>>Sociedad Cooperativa Suplementada (S. C. S.)</option>
                      <option value="11" <?php if($tipo_persona_moral==11) echo 'selected' ?>>Sociedad Mutualista de Seguros de Vida o de Daño</option>
                      <option value="12" <?php if($tipo_persona_moral==12) echo 'selected' ?>>Sociedad Nacional de Crédito y/o Institución Bancaria de Desarrollo (S. N. C.)</option>
                      <option value="13" <?php if($tipo_persona_moral==13) echo 'selected' ?>>Sociedad de Solidaridad Social (S. de S. S.)</option>
                      <option value="14" <?php if($tipo_persona_moral==14) echo 'selected' ?>>Sociedad de Producción Rural de Responsabilidad Limitada</option>
                      <option value="15" <?php if($tipo_persona_moral==15) echo 'selected' ?>>Sociedad de Producción Rural de Responsabilidad Ilimitada</option>
                      <option value="16" <?php if($tipo_persona_moral==16) echo 'selected' ?>>Sociedad de Producción Rural de Responsabilidad Suplementada</option>
                      <option value="99" <?php if($tipo_persona_moral==99) echo 'selected' ?>>Otra (especificar)</option>
                    </select>
                  </div>
                </div>
                <div class="tipo_persona_moral_otratxt" style="display: none">  
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Tipo de persona moral cuando es "Otra"</label>
                      <input class="form-control" type="text" name="tipo_persona_moral_otra" maxlength="200" value="<?php echo $tipo_persona_moral_otra ?>">
                    </div>
                  </div>
                </div>  
                <div class="row">
                  <div class="col-md-12 form-group">
                    <label>Denominación o razón social de la persona moral que se constituye</label>
                    <input class="form-control" type="text" name="denominacion_razonco" value="<?php echo $denominacion_razonco ?>">
                  </div>
                  <div class="col-md-12 form-group">
                    <label>Actividad económica o giro mercantil u objeto social</label>
                    <select class="form-control" name="giro_mercantilco">
                    <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilco) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                    <?php } ?>  
                    </select>
                  </div>
                  <div class="col-md-11 form-group">
                    <label>Folio mercantil o antecedentes registrales</label>
                    <input class="form-control" type="text" name="folio_mercantilco" maxlength="200" value="<?php echo $folio_mercantilco ?>">
                  </div>
                  <div class="col-md-1">
                    <br>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button> 
                  </div>
                  <div class="col-md-8 form-group">
                    <label>Número total de acciones o partes sociales de la persona moral</label>
                    <input class="form-control" type="number" minlength="4" maxlength="17" name="numero_total_accionesco" value="<?php echo $numero_total_accionesco ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave de entidad federativa del domicilio social</label>
                    <select class="form-control" name="entidad_federativaco">
                    <?php foreach ($estados_get as $item){ ?>
                      <option value="<?php echo $item->id ?>" <?php if($item->id==$entidad_federativaco) echo 'selected' ?>><?php echo $item->estado ?></option>
                    <?php } ?>  
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Se cuenta con un consejo de vigilancia</label>
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="consejo_vigilanciaco" value="SI" checked <?php if($consejo_vigilanciaco=='SI') echo 'checked' ?>>
                            SI 
                          </label>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="consejo_vigilanciaco" value="NO" <?php if($consejo_vigilanciaco=='NO') echo 'checked' ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>  
                  <div class="col-md-4 form-group">
                    <label>Motivo de la constitución de la persona moral</label>
                    <select class="form-control" name="motivo_constitucionco">
                      <option value="1" <?php if($motivo_constitucionco==1) echo 'selected' ?>>Fusión</option>
                      <option value="2" <?php if($motivo_constitucionco==2) echo 'selected' ?>>Escisión</option>
                      <option value="3" <?php if($motivo_constitucionco==3) echo 'selected' ?>>Contrato privado</option>
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Número de Instrumento Público de la fusión o escisión</label>
                    <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="instrumento_publicoco" maxlength="20" value="<?php echo $instrumento_publicoco ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NIPFE_ayuda()">
                      <i class="mdi mdi-help"></i>
                    </button>              
                  </div>
                  </div>
                </div>
              </form>    
                <hr>
                <!------ ------>
                <h5>Datos de los accionistas o socios</h5><!-- 3.6.1.3.2.11 -->
                <div class="row_accionistas_socios">
                  <div class="accionistas_socios">
                  </div>
                </div>
                <!------ ------>
                <h5>Capital Social de constitución</h5>
              <form id="form_anexo3" class="forms-sample">    
                <div class="row">
                  <div class="col-md-4 form-group">
                      <label>Monto de Capital Fijo</label>
                      <input class="form-control" type="number" name="capital_fijoco" id="capital_fijoco" minlength="4" maxlength="17" value="<?php echo $capital_fijoco ?>">
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Monto de Capital Variable</label>
                    <input class="form-control" type="number" name="capital_variableco" id="capital_variableco" minlength="4" maxlength="17" value="<?php echo $capital_variableco ?>">
                  </div>
                </div>
              </form>  
              </div>
            </div>    
            <hr class="subsubtitle">
            <!------  ------>
          </div> 
          <!-- tipo_actividad 3-->
        <form id="form_anexo4" class="forms-sample">      
        </form>        
        <!--- ===================== --->
          <div class="modificacion_patrimonialtxt" style="display: none">
            <!------  ------>
            <hr>
            <h5>Datos de la persona moral que se modifica</h5>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11"> 
              <form id="form_anexo5" class="forms-sample">
                <div class="row">
                  <div class="col-md-12 form-group">
                    <label>Denominación o Razón Social</label>
                    <input class="form-control" type="text" name="denominacion_razonmo" value="<?php echo $denominacion_razonmo ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Fecha de Constitución</label>
                    <input class="form-control" type="date" name="fecha_constitucionmo" value="<?php echo $fecha_constitucionmo ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmo" maxlength="13" value="<?php echo $rfcmo ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave país nacionalidad</label>
                    <select class="form-control pais_mp_1" name="pais_nacionalidadmo">
                      <?php if($pais_nacionalidadmo!=""){
                         echo '<option value="'.$clavemog.'">'.$pais_nacionalidadmog.'</option>'; 
                      } ?>
                    </select>
                  </div>
                  <div class="col-md-12 form-group">
                    <label>Actividad económica o giro mercantil u objeto social</label>
                    <select class="form-control" name="giro_mercantilmo">
                    <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilmo) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                    <?php } ?>  
                    </select>
                  </div>
                  <div class="col-md-7 form-group">
                    <label>Número total de acciones o partes sociales de la persona moral</label>
                    <input class="form-control" type="number" minlength="4" maxlength="17" name="numero_total_accionesmo" value="<?php echo $numero_total_accionesmo ?>">
                  </div>
                  <div class="col-md-5 form-group">
                    <label>Motivo de la modificación de la persona moral</label>
                    <select class="form-control" name="motivo_modificacionmo">
                      <option value="1" <?php if($motivo_modificacionmo==1) echo 'selected' ?>>Fusión</option>
                      <option value="2" <?php if($motivo_modificacionmo==2) echo 'selected' ?>>Escisión</option>
                      <option value="3" <?php if($motivo_modificacionmo==3) echo 'selected' ?>>Contrato privado</option>
                    </select>
                  </div>
                  <div class="col-md-7 form-group">
                    <label>Número de Instrumento Público de la fusión o escisión</label>
                    <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" maxlength="20" name="instrumento_publicomo" value="<?php echo $instrumento_publicomo ?>"></div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NIPFE_ayuda()">
                      <i class="mdi mdi-help"></i>
                    </button>              
                  </div>
                  </div>
                </div>  
                <hr> 
                <!--- asta aqui me quede -->
                <h5>Datos de la modificación</h5>
                <div class="row">
                  <div class="col-md-6 form-group">
                    <label>Tipo de modificación patrimonial del capital fijo</label>
                    <select class="form-control" name="tipo_modificacion_capital_fijomo">
                      <option value="1" <?php if($tipo_modificacion_capital_fijomo==1) echo 'selected' ?>>Aumento</option>
                      <option value="2" <?php if($tipo_modificacion_capital_fijomo==2) echo 'selected' ?>>Disminución</option>
                      <option value="3" <?php if($tipo_modificacion_capital_fijomo==3) echo 'selected' ?>>Sin modificación</option>
                    </select>
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Monto anterior del capital fijo</label>
                    <input class="form-control" type="text" minlength="4" maxlength="17" name="inicial_capital_fijomo" value="<?php echo $inicial_capital_fijomo ?>">
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Monto final del capital fijo</label>
                    <input class="form-control" type="text" minlength="4" maxlength="17" name="final_capital_fijomo" value="<?php echo $final_capital_fijomo ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Tipo de modificación patrimonial del capital variable</label>
                    <select class="form-control" name="tipo_modificacion_capital_variablemo">
                      <option value="1" <?php if($tipo_modificacion_capital_variablemo==1) echo 'selected' ?>>Aumento</option>
                      <option value="2" <?php if($tipo_modificacion_capital_variablemo==2) echo 'selected' ?>>Disminución</option>
                      <option value="3" <?php if($tipo_modificacion_capital_variablemo==3) echo 'selected' ?>>Sin modificación</option>
                    </select>
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Monto anterior del capital variable</label>
                    <input class="form-control" type="text" minlength="4" maxlength="17" name="inicial_capital_variablemo" value="<?php echo $inicial_capital_variablemo ?>">
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Monto final del capital variable</label>
                    <input class="form-control" type="number" minlength="4" maxlength="17" name="final_capital_variablemo" id="final_capital_variablemo" value="<?php echo $final_capital_variablemo ?>">
                  </div>
                </div>
              </form>
                <hr class="subsubtitle">
                <h5>Datos de los accionistas vigentes</h5>
                <h5>Tipo de Persona</h5>
                <div class="row_accionistas_vigentes">
                  <div class="accionistas_vigentes">
                  </div>
                </div>
                <!-- --> 
              </div>
            </div>    
            <hr class="subsubtitle">
            <!------  ------>
          </div>
        <form id="form_anexo6" class="forms-sample">
        </form>  
          <div class="fusiontxt" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
              <form id="form_anexo7" class="forms-sample">
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Tipo de fusión</label>
                    <select class="form-control" name="tipo_fusion">
                      <option value="1" <?php if($tipo_fusion==1) echo 'selected' ?>>Absorción</option>             
                      <option value="2" <?php if($tipo_fusion==2) echo 'selected' ?>>Integración</option>
                    </select>
                  </div>
                </div>  
                <h4>Datos de las Personas Morales Fusionadas</h4> 
                <h5>Datos de la Persona Moral Fusionada</h5>
                <!-- -->
                <div class="row"> 
                  <div class="col-md-12 form-group">
                    <label>Denominación o Razón Social</label>
                    <input class="form-control" type="text" name="denominacion_razonfu" value="<?php echo $denominacion_razonfu ?>">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Fecha de Constitución</label>
                    <input class="form-control" type="date" name="fecha_constitucionfu" value="<?php echo $fecha_constitucionfu ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfu" maxlength="13" value="<?php echo $rfcfu ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave pais nacionalidad</label>
                    <select class="form-control pais_fu_1" name="pais_nacionalidadfu">
                      <?php if($pais_nacionalidadfu!=""){
                      echo '<option value="'.$pais_nacionalidadfu.'">'.$pais_nacionalidadfug.'</option>'; 
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-12 form-group">
                    <label>Actividad económica o giro mercantil u objeto social</label>
                    <select class="form-control" name="giro_mercantilfu">
                    <?php foreach ($actividad_econominica_morales_get as $item){?>
                      <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilfu) echo 'selected' ?>><?php echo $item->giro_mercantil?></option>   
                    <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Capital social fijo</label>
                    <input class="form-control" type="number" name="capital_social_fijofu" id="capital_social_fijofu" minlength="4" maxlength="17" value="<?php echo $capital_social_fijofu ?>">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Capital social Variable</label>
                    <input class="form-control" type="number" name="capital_social_variablefu" id="capital_social_variablefu" minlength="4" maxlength="17" value="<?php echo $capital_social_variablefu ?>">
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-11 form-group">
                    <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o antecedentes registrales</label>
                    <input class="form-control" type="text" name="folio_mercantilfu" value="<?php echo $folio_mercantilfu ?>">
                  </div>
                  <div class="col-md-1">
                    <br>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button> 
                </div>
                </div>
                <hr>
                <h5>Datos de la Persona Moral Fusionante</h5>
                <div class="row">
                  <div class="col-md-6 form-group">
                    <label>La persona moral fusionante ya se encuentra determinada</label>
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onclick="ValidFusion()" name="fusionante_determinadasfu" value="SI" checked <?php if($fusionante_determinadasfu=='SI') echo 'checked' ?>>
                            SI 
                          </label>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onclick="ValidFusion()" name="fusionante_determinadasfu" value="NO" <?php if($fusionante_determinadasfu=='NO') echo 'checked' ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
                <div id="DPMF">
                  <h5>Fusionante</h5>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonffu" value="<?php echo $denominacion_razonffu ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionffu" value="<?php echo $fecha_constitucionffu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcffu" maxlength="13" value="<?php echo $rfcffu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave pais nacionalidad</label>
                      <select class="form-control pais_ffu_1" name="pais_nacionalidadffu">
                        <?php if($pais_nacionalidadffu!=""){
                         echo '<option value="'.$pais_nacionalidadffu.'">'.$pais_nacionalidadffug.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilffu">
                      <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilffu) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-4 form-group">
                      <label>Capital social fijo</label>
                      <input class="form-control" type="number" name="capital_social_fijoffu" minlength="4" maxlength="17" value="<?php echo $capital_social_fijoffu ?>">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Capital social Variable</label>
                      <input class="form-control" type="number" name="capital_social_variableffu" minlength="4" maxlength="17" value="<?php echo $capital_social_variableffu ?>">
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-11 form-group">
                      <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o antecedentes registrales</label>
                      <input class="form-control" type="text" name="folio_mercantilffu" value="<?php echo $folio_mercantilffu ?>">
                    </div>
                    <div class="col-md-1">
                      <br>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()">
                      <i class="mdi mdi-help"></i>
                    </button> 
                  </div>
                    <div class="col-md-8 form-group">
                      <label>Número total de acciones o partes sociales de la persona moral</label>
                      <input class="form-control" type="number" minlength="4" maxlength="17" name="numero_total_accionesffu" value="<?php echo $numero_total_accionesffu ?>">
                    </div>
                  </div>
                </div>
              </form> 
              <div id="con_dato_accion_socio"> 
                <hr>
                <h5>Datos de los accionistas o socios de la persona moral Fusionante</h5>
                <h5>Tipo de Persona</h5>
                <!-- -->
                <div class="row_accionistas_socios_fucionante">
                  <div class="accionistas_socios_fucionante">
                  </div>
                </div>
              </div>
                <!-- -->
              </div>
            </div>    
          </div>  
        </form>
        <form id="form_anexo8" class="forms-sample">

        </form>  
          <div class="escisiontxt" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
              <form id="form_anexo9" class="forms-sample">
                <h5>Datos de la Persona Moral Escindente</h5>
                <div class="row">
                  <div class="col-md-12 form-group">
                    <label>Denominación o Razón Social</label>
                    <input class="form-control" type="text" name="denominacion_razone" value="<?php echo $denominacion_razone ?>">
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Fecha de Constitución</label>
                    <input class="form-control" type="date" name="fecha_constitucione" value="<?php echo $fecha_constitucione ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfce" maxlength="13" value="<?php echo $rfce ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave pais nacionalidad</label>
                    <select class="form-control pais_esci_1" name="pais_nacionalidade">
                    <?php if($pais_nacionalidade!=""){
                     echo '<option value="'.$pais_nacionalidade.'" >'.$pais_nacionalidadeg.'</option>'; 
                    } ?>
                    </select>
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-12 form-group">
                    <label>Actividad económica o giro mercantil u objeto social</label>
                    <select class="form-control" name="giro_mercantile">
                    <?php foreach ($actividad_econominica_morales_get as $item){?>
                      <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantile) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                    <?php } ?>  
                    </select>
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Capital social fijo</label>
                    <input class="form-control" type="number" name="capital_social_fijoe" minlength="4" maxlength="17" value="<?php echo $capital_social_fijoe ?>">
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Capital social Variable</label>
                    <input class="form-control" type="number" name="capital_social_variablee" minlength="4" maxlength="17" value="<?php echo $capital_social_variablee ?>">
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-11 form-group">
                    <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o antecedentes registrales</label>
                    <input class="form-control" type="text" name="folio_mercantile" value="<?php echo $folio_mercantile ?>">
                  </div>
                  <div class="col-md-1">
                    <br>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button> 
                </div>
                </div>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>¿La persona moral escindente subsiste?</label>
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" onclick="ValidEsicion()" class="form-check-input" id="escindente_subsistee1" name="escindente_subsistee" value="SI" checked <?php if($escindente_subsistee=='SI') echo 'checked' ?>>
                            SI 
                          </label>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" onclick="ValidEsicion()" class="form-check-input" id="escindente_subsistee1" name="escindente_subsistee" value="NO" <?php if($escindente_subsistee=='NO') echo 'checked' ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
                <hr>  
              </form>
              <div id="datos_accion_subs">
                <h5>Datos de los accionistas o socios de la persona moral escindente cuando esta subsiste</h5>
                <h5>Tipo de Persona</h5>
                <!-- -->
                <div class="row_accionistas_socios_escision">
                  <div class="accionistas_socios_escision">
                  </div>
                </div>
              </div>
                <!-- -->
              <div id="escision_subsiste">
                <form id="form_anexo10" class="forms-sample">  
                  <hr>
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <h5>Datos de las personas morales escindidas</h5>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>¿Las personas morales escindidas ya se encuentran determinadas?</label>
                      <div class="row">
                        <div class="col-md-3 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" onclick="ValidEsicion1()" name="escindidas_determinadase" value="SI" <?php if($escindidas_determinadase=='SI') echo 'checked' ?>>
                              SI 
                            </label>
                          </div>
                        </div>
                        <div class="col-md-3 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" onclick="ValidEsicion1()" name="escindidas_determinadase" value="NO" <?php if($escindidas_determinadase=='NO') echo 'checked' ?>>
                              NO
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>  
                  </div>
                  <div id="DPME">
                     <h5>Datos de la Persona Moral Escindida</h5>
                      <div class="row">
                        <div class="col-md-5 form-group">
                          <label>Denominación o Razón Social</label>
                          <input class="form-control" type="text" name="denominacion_razonme" value="<?php echo $denominacion_razonme ?>">
                        </div>
                        <div class="col-md-3 form-group">
                          <label>Fecha de Constitución</label>
                          <input class="form-control" type="date" name="fecha_constitucionme" value="<?php echo $fecha_constitucionme ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Registro Federal de Contribuyentes (RFC)</label>
                          <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcme" maxlength="13" value="<?php echo $rfcme ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Clave país nacionalidad</label>
                          <select class="form-control pais_esce_1" name="pais_nacionalidadme">
                            <?php if($pais_nacionalidadme!=""){
                             echo '<option value="'.$pais_nacionalidadme.'">'.$pais_nacionalidadmeg.'</option>'; 
                            } ?>
                          </select>
                        </div>
                        <div class="col-md-8 form-group">
                          <label>Actividad económica o giro mercantil u objeto social</label>
                          <select class="form-control" name="giro_mercantilme">
                          <?php foreach ($actividad_econominica_morales_get as $item){?>
                            <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilme) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                          <?php } ?>  
                          </select>
                        </div>
                      </div> 
                      <div class="row">  
                        <div class="col-md-4 form-group">
                          <label>Capital social fijo</label>
                          <input class="form-control" type="number" name="capital_social_fijome" id="capital_social_fijome" minlength="4" maxlength="17" value="<?php echo $capital_social_fijome ?>">
                        </div>
                      </div>
                      <div class="row">  
                        <div class="col-md-4 form-group">
                          <label>Capital social Variable</label>
                          <input class="form-control" type="number" name="capital_social_variableme" id="capital_social_variableme" minlength="4" maxlength="17" value="<?php echo $capital_social_variableme ?>">
                        </div>
                      </div>
                      <div class="row">  
                        <div class="col-md-11 form-group">
                          <label>Folio Mercantil o de inscripción en el registro que corresponda (Registro de Sociedades Civiles) o antecedentes registrales</label>
                          <input class="form-control" type="text" name="folio_mercantilme" value="<?php echo $folio_mercantilme ?>">
                        </div>
                        <div class="col-md-1">
                          <br>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliomer_ayuda()">
                          <i class="mdi mdi-help"></i>
                        </button> 
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-md-8 form-group">
                          <label>Número total de acciones o partes sociales de la persona moral</label>
                          <input class="form-control" type="number" minlength="4" maxlength="17" name="numero_total_accionesme" value="<?php echo $numero_total_accionesme ?>">
                        </div>
                      </div>
                      <hr>  
                  </div>
                </form>  
                <div id="cont_accion_moral_esc">
                  <h5>Datos de los accionistas o socios de la persona moral Escindida</h5>
                  <!-- -->
                  <div class="row_accionistas_socios_escindida">
                    <div class="accionistas_socios_escindida">
                    </div>
                  </div>
                </div>
                <!-- -->
              </div>
              </div>
            </div>
          </div>      
        <form id="form_anexo11" class="forms-sample">

        </form>  
          <div class="compra_venta_accionestxt" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <form id="form_anexo12" class="forms-sample">
                  <div class="row">
                    <div class="col-md-12">
                      <h4>Tipo de operación: <span>Compra de acciones o partes sociales</span></h4>
                    </div>
                  </div>
                  <h5>Datos de la persona moral objeto de la comercialización de acciones o partes sociales</h5>
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonven" value="<?php echo $denominacion_razonven ?>">
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-4 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionven" value="<?php echo $fecha_constitucionven ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcven" maxlength="13" value="<?php echo $rfcven ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave pais nacionalidad</label>
                      <select class="form-control pais_ven_1" name="pais_nacionalidadven">
                      <?php if($pais_nacionalidadven!=""){
                       echo '<option value="'.$pais_nacionalidadven.'" >'.$pais_nacionalidadveng.'</option>'; 
                      } ?>
                      </select>
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-8 form-group">
                      <label>Valor nominal de las acciones o partes sociales en Moneda Nacional</label>
                      <input class="form-control" type="number" name="valor_nominalven" value="<?php echo $valor_nominalven ?>">
                    </div>
                  </div> 
                  <div class="row">  
                    <div class="col-md-8 form-group">
                      <label>Número total de acciones o partes sociales comercializadas</label>
                      <input class="form-control" type="number" name="numero_accionesven" value="<?php echo $numero_accionesven ?>">
                    </div>
                  </div>
                  <hr>
                  <!------ ============================== ------>              
                  <h5>Datos de la persona que vende acciones</h5> 
                  <div class="row">  
                    <div class="col-md-8 form-group">
                      <label>Número de acciones o partes sociales vendidas</label>
                      <input class="form-control" type="number" name="numero_acciones_vendidasven" value="<?php echo $numero_acciones_vendidasven ?>">
                    </div>
                  </div>
                  <h5>Tipo de Persona</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personaven" id="tipo_persona61" value="1" onclick="tipo_persona6_select()" checked <?php if($tipo_personaven==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personaven" id="tipo_persona62" value="2" onclick="tipo_persona6_select()" <?php if($tipo_personaven==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personaven" id="tipo_persona63" value="3" onclick="tipo_persona6_select()" <?php if($tipo_personaven==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="persona_fisica6txt" style="display: none">
                    <hr>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombreven" value="<?php echo $nombreven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paternoven" value="<?php echo $apellido_paternoven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_maternoven" value="<?php echo $apellido_maternoven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimientoven" value="<?php echo $fecha_nacimientoven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcvend" maxlength="13" value="<?php echo $rfcvend ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpven" maxlength="18" value="<?php echo $curpven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_f_1" name="pais_nacionalidadvend">
                          <?php if($pais_nacionalidadvend!=""){
                           echo '<option value="'.$pais_nacionalidadvend.'">'.$pais_nacionalidadvendg.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                  </div>
                  <div class="persona_moral6txt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <input class="form-control" type="text" name="denominacion_razonmven" value="<?php echo $denominacion_razonmven ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionmven" value="<?php echo $fecha_constitucionmven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmven" maxlength="13" value="<?php echo $rfcmven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_m_1" name="pais_nacionalidadmven">
                          <?php if($pais_nacionalidadmven!=""){
                           echo '<option value="'.$pais_nacionalidadmven.'">'.$pais_nacionalidadmveng.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                  <div class="fideicomiso6txt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <input class="form-control" type="text" name="denominacion_razonfven" value="<?php echo $denominacion_razonfven ?>">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfven" maxlength="13" value="<?php echo $rfcfven ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="identificador_fideicomisofven" maxlength="40" value="<?php echo $identificador_fideicomisofven ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      
                    </div>
                  </div>
                    <hr>
                  </div>
                  <h5>Datos de la persona que compra acciones</h5>
                  <div class="row">  
                    <div class="col-md-8 form-group">
                      <label>Número de acciones o partes sociales compradas</label>
                      <input class="form-control" type="number" name="numero_acciones_compradascom" value="<?php echo $numero_acciones_compradascom ?>">
                    </div>
                  </div>
                  <h5>Tipo de Persona</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personacom" id="tipo_persona71" value="1" onclick="tipo_persona7_select()" checked <?php if($tipo_personacom==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personacom" id="tipo_persona72" value="2" onclick="tipo_persona7_select()" <?php if($tipo_personacom==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personacom" id="tipo_persona73" value="3" onclick="tipo_persona7_select()" <?php if($tipo_personacom==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="persona_fisica7txt" style="display: none">
                    <hr>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombrecom" value="<?php echo $nombrecom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paternocom" value="<?php echo $apellido_paternocom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_maternocom" value="<?php echo $apellido_maternocom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimientocom" value="<?php echo $fecha_nacimientocom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccom" maxlength="13" value="<?php echo $rfccom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpcom" maxlength="18" value="<?php echo $curpcom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_com_1" name="pais_nacionalidadcomc">
                          <?php if($pais_nacionalidadcomc!=""){
                           echo '<option value="'.$pais_nacionalidadcomc.'">'.$pais_nacionalidadcomcg.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="persona_moral7txt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <input class="form-control" type="text" name="denominacion_razonmcom" value="<?php echo $denominacion_razonmcom ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionmcom" value="<?php echo $fecha_constitucionmcom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmcom" maxlength="13" value="<?php echo $rfcmcom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_comm_1" name="pais_nacionalidadmcom">
                          <?php if($pais_nacionalidadmcom!=""){
                           echo '<option value="'.$pais_nacionalidadmcom.'">'.$pais_nacionalidadmcomg.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div> 
                  </div>
                  <div class="fideicomiso7txt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <input class="form-control" type="text" name="denominacion_razonfcom" value="<?php echo $denominacion_razonfcom ?>">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfcom" maxlength="13" value="<?php echo $rfcfcom ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="identificador_fideicomisofcom" maxlength="40" value="<?php echo $identificador_fideicomisofcom ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                  </div>
                </form>  
                <hr class="subsubtitle">
                <div class="row_liquidacion">
                  <div class="liquidacion">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <hr class="subtitle">
                    <label>Monto total de la liquidación</label>
                    <input readonly="" class="form-control" type="text" id="total_liquida" value="$0.00">
                  </div>
                  <div class="col-md-8 form-group">
                    <br><br>
                    <h5 class="validacion_cantidad" style="color: red"></h5>
                  </div>
                </div>
                <div class="row"> 
                  <div class="col-md-4 form-group">
                    <hr class="subtitle">
                    <label>Monto total en efectivo</label>
                    <input readonly="" class="form-control" type="text" id="total_liquida_efect" value="$0.00">
                  </div>
                </div>
                
              </div>
            </div>
          </div> 

        <form id="form_anexo13" class="forms-sample">

        </form>  
          <div class="constitucion_modificacion_fideicomisotxt" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11"> 
                <form id="form_anexo13" class="forms-sample">
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <label>Tipo de Movimiento</label>
                      <select class="form-control" name="tipo_movimientofi" id="tipo_movimientofi">
                        <option value="1" <?php if($tipo_movimientofi==1) echo 'selected' ?>>Constitucion</option>
                        <option value="2" <?php if($tipo_movimientofi==2) echo 'selected' ?>>Modificación - Aumento del patrimonio</option>
                        <option value="3" <?php if($tipo_movimientofi==3) echo 'selected' ?>>Modificación - Disminución del patrimonio</option>
                        <option value="6" <?php if($tipo_movimientofi==6) echo 'selected' ?>>Modificación de incorporación y/o destitución sin cambios al patrimonio</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-6 form-group">
                      <label>Tipo de Fideicomiso</label>
                      <select class="form-control" name="tipo_fideicomisofi" id="tipo_fideicomisofi" onchange="tipo_fideicomiso_fide()">
                        <option value="1" <?php if($tipo_fideicomisofi==1) echo 'selected' ?>>Traslativo de Dominio</option>
                        <option value="2" <?php if($tipo_fideicomisofi==2) echo 'selected' ?>>Garantía sobre Inmuebles</option>
                        <option value="3" <?php if($tipo_fideicomisofi==3) echo 'selected' ?>>Traslativo de domino y garantía sobre inmuebles</option>
                        <option value="4" <?php if($tipo_fideicomisofi==4) echo 'selected' ?>>Otro (Especificar)</option>
                      </select>
                    </div>
                  </div>
                  <div class="tipo_fideicomiso_cmftxt" style="display: none">
                    <div class="row">  
                      <div class="col-md-12 form-group">
                        <label>Descripción del tipo de fideicomiso cuando es otro</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="descripcion_fi" value="<?php echo $descripcion_fi ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DTFCO_ayuda()">
                          <i class="mdi mdi-help"></i>
                        </button>              
                      </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-6 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_fi" maxlength="13" value="<?php echo $rfc_fi ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomiso_fi" maxlength="40" value="<?php echo $identificador_fideicomiso_fi ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razon_fi" value="<?php echo $denominacion_razon_fi ?>">
                    </div>
                  </div>
                  <div class="row">  
                    <div class="col-md-8 form-group">
                      <label>Monto total o monto después de la modificación del patrimonio del fideicomiso en Moneda Nacional</label>
                      <input class="form-control" type="number" name="monto_patrimonio_fi" id="monto_patrimonio_fi" value="<?php echo $monto_patrimonio_fi ?>">
                    </div>
                  </div>    
                </form>
                <hr>
                <h5>Datos de los fideicomitentes</h5>
                <!-- -->
                <div class="row_datos_fideicomitente">
                  <div class="datos_fideicomitente">
                  </div>
                </div>
                <!-- -->
                <form id="form_anexo14" class="forms-sample">
                  <?php 
                  /*
                  <h5>Datos del tipo de patrimonio de constitución o modificación</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_tipo_patrimoniofi" value="1" id="datos_tipo_patrimoniofi1" onclick="datos_tipo_patrimoniofi_select()" checked <?php if($datos_tipo_patrimoniofi==1) echo 'checked' ?>>
                         Monetario
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_tipo_patrimoniofi" value="2" id="datos_tipo_patrimoniofi2" onclick="datos_tipo_patrimoniofi_select()" <?php if($datos_tipo_patrimoniofi==2) echo 'checked' ?>>
                         Inmueble
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_tipo_patrimoniofi" value="3" id="datos_tipo_patrimoniofi3" onclick="datos_tipo_patrimoniofi_select()" <?php if($datos_tipo_patrimoniofi==3) echo 'checked' ?>>
                         Otro bien
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="patrimonio_monetariotxt" style="display: none">
                    <div class="row">  
                      <div class="col-md-4 form-group">
                        <label>Tipo de moneda o divisa del patrimonio</label>
                        <select class="form-control" name="monedafi">
                        <?php foreach ($tipo_moneda_get as $item){ 
                          if($item->id==1){ ?>
                          <option value="<?php echo $item->id ?>" <?php if($item->clave==$monedafi) echo 'selected' ?>><?php echo $item->moneda ?></option>
                        <?php } } ?>  
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Monto del patrimonio</label>
                        <input class="form-control" type="number" name="monto_operacionfi" maxlength="17" value="<?php echo $monto_operacionfi ?>">
                      </div>
                    </div>
                  </div>
                  <div class="patrimonio_inmuebletxt" style="display: block">
                    <div class="row">  
                      <div class="col-md-4 form-group">
                        <label>Tipo de inmueble</label>
                        <select class="form-control" name="tipo_inmueblefi">
                        <?php foreach ($tipo_inmueble_get as $item){ ?>
                          <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmueblefi) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } ?>  
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Código Postal de la ubicación del inmueble</label>
                        <input class="form-control" type="text" name="codigo_postalfi" value="<?php echo $codigo_postalfi ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Folio real del inmueble o antecedentes registrales</label>
                        <input class="form-control" type="text" name="folio_realfi" value="<?php echo $folio_realfi ?>">
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Importe que garantiza el inmueble que se aporta al Fideicomiso en Moneda Nacional</label>
                        <input class="form-control" type="number" name="importe_garantiafi" minlength="4" maxlength="17" value="<?php echo $importe_garantiafi ?>">
                      </div>
                    </div>
                  </div>
                  <div class="patrimonio_otro_bientxt" style="display: none">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Descripción del bien</label>
                        <input class="form-control" type="text" name="descripcionfi" value="<?php echo $descripcionfi ?>">
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Valor del bien que se aporta al Fideicomiso en Moneda Nacional</label>
                        <input class="form-control" type="number" name="valor_bienfi" minlength="4" maxlength="17" value="<?php echo $valor_bienfi ?>">
                      </div>
                    </div>  
                  </div>
                  */ 
                  ?>
                </form> 
                <div id="cont_prin_ddlf">
                  <h5>Datos de los fideicomisarios</h5> 
                  <!-- -->
                  <div class="row_datos_fideicomisarios">
                    <div class="datos_fideicomisarios">
                    </div>
                  </div>
                  <!-- -->
                  <hr>
                </div>
                <div id="cont_ult_preg">
                  <form id="form_anexo15" class="forms-sample">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>¿Se cuenta con un comité técnico?</label>
                        <div class="row">
                          <div class="col-md-3 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="comite_tecnicofi" value="SI" checked <?php if($comite_tecnicofi=='SI') echo 'checked' ?>>
                                SI 
                              </label>
                            </div>
                          </div>
                          <div class="col-md-3 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="comite_tecnicofi" value="NO" <?php if($comite_tecnicofi=='NO') echo 'checked' ?>>
                                NO
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>  
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>¿Se modifica el comité técnico?</label>
                        <div class="row">
                          <div class="col-md-3 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="modificacion_comite_tecnicofi" value="SI" checked <?php if($modificacion_comite_tecnicofi=='SI') echo 'checked' ?>>
                                SI 
                              </label>
                            </div>
                          </div>
                          <div class="col-md-3 form-group">
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="modificacion_comite_tecnicofi" value="NO" <?php if($modificacion_comite_tecnicofi=='NO') echo 'checked' ?>>
                                NO
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>  
                    </div>
                  </form> 
                </div> 
              </div>  
            </div>  
          </div>
        <form id="form_anexo16" class="forms-sample">
          <div class="cesion_derechos_fideicomitente_fideicomisariotxt" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
              <!-- -->  
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Número, referencia o identificador del fideicomiso</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="identificador_fideicomisoces" maxlength="40" value="<?php echo $identificador_fideicomisoces ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                    </div>
                  </div>
                  <div class="col-md-5 form-group">
                    <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcces" maxlength="13" value="<?php echo $rfcces ?>">
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-12 form-group">
                    <label>Denominación o Razón Social del Fiduciario</label>
                    <input class="form-control" type="text" name="denominacion_razonces" value="<?php echo $denominacion_razonces ?>">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 form-group">
                    <label>Tipo de cesión</label>
                    <select class="form-control" name="tipo_cesionces">
                      <option value="3" <?php if($tipo_cesionces==3) echo 'selected' ?>>Cesión de derechos del fideicomitente a título oneroso</option>
                      <option value="4" <?php if($tipo_cesionces==4) echo 'selected' ?>>Cesión de derechos del fideicomitente a título gratuito</option>
                      <option value="5" <?php if($tipo_cesionces==5) echo 'selected' ?>>Cesión de derechos del fideicomisario a título oneroso</option>
                      <option value="6" <?php if($tipo_cesionces==6) echo 'selected' ?>>Cesión de derechos del fideicomisario a título gratuito</option>
                    </select>
                  </div>
                </div>
                <hr>
                <h5>Datos de cedente</h5>
                <h5>Tipo de Persona</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaces" id="tipo_personaces1" value="1" onclick="tipo_persona_selectces()" checked <?php if($tipo_personaces==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaces" id="tipo_personaces2" value="2" onclick="tipo_persona_selectces()" <?php if($tipo_personaces==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaces" id="tipo_personaces3" value="3" onclick="tipo_persona_selectces()" <?php if($tipo_personaces==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="persona_fisicacestxt" style="display: none">
                  <hr>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombreces" value="<?php echo $nombreces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternoces" value="<?php echo $apellido_paternoces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternoces" value="<?php echo $apellido_maternoces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientoces" value="<?php echo $fecha_nacimientoces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccesi" maxlength="13" value="<?php echo $rfccesi ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpces" maxlength="18" value="<?php echo $curpces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_ces_1" name="pais_nacionalidadces">
                        <?php if($pais_nacionalidadces!=""){
                         echo '<option value="'.$pais_nacionalidadces.'">'.$pais_nacionalidadcesg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica u ocupación</label>
                      <select class="form-control" name="actividad_economicaces">
                      <?php foreach ($actividad_econominica_fisica_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$actividad_economicaces) echo 'selected' ?>><?php echo $item->acitividad ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>
                <div class="persona_moralcestxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmces" value="<?php echo $denominacion_razonmces ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmces" value="<?php echo $fecha_constitucionmces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmces" maxlength="13" value="<?php echo $rfcmces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_cesm_1" name="pais_nacionalidadmces">
                        <?php if($pais_nacionalidadmces!=""){
                         echo '<option value="'.$pais_nacionalidadmces.'">'.$pais_nacionalidadmcesg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilmces">
                      <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilmces) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <div class="fideicomisocestxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfces" value="<?php echo $denominacion_razonfces ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfces" maxlength="13" value="<?php echo $rfcfces ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofces" maxlength="40" value="<?php echo $identificador_fideicomisofces ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <h5>Datos de cesionario</h5> 
                <h5>Tipo de Persona</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personace" id="tipo_personace1" value="1" onclick="tipo_persona_selectce()" checked <?php if($tipo_personace==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personace" id="tipo_personace2" value="2" onclick="tipo_persona_selectce()" <?php if($tipo_personace==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personace" id="tipo_personace3" value="3" onclick="tipo_persona_selectce()" <?php if($tipo_personace==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="persona_fisicacetxt" style="display: none">
                  <hr>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombrece" value="<?php echo $nombrece ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternoce" value="<?php echo $apellido_paternoce ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternoce" value="<?php echo $apellido_maternoce ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientoce" value="<?php echo $fecha_nacimientoce ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccei" maxlength="13" value="<?php echo $rfccei ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpce" maxlength="18" value="<?php echo $curpce ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_ce_1" name="pais_nacionalidadce">
                        <?php if($pais_nacionalidadce!=""){
                         echo '<option value="'.$pais_nacionalidadce.'">'.$pais_nacionalidadceg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica u ocupación</label>
                      <select class="form-control" name="actividad_economicace">
                      <?php foreach ($actividad_econominica_fisica_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$actividad_economicaces) echo 'selected' ?>><?php echo $item->acitividad ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>
                <div class="persona_moralcetxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmce" value="<?php echo $denominacion_razonmce ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmce" value="<?php echo $fecha_constitucionmce ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmce" maxlength="13" value="<?php echo $rfcmce ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_cem_1" name="pais_nacionalidadmce">
                        <?php if($pais_nacionalidadmce!=""){
                         echo '<option value="'.$pais_nacionalidadmce.'">'.$pais_nacionalidadmceg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilmce">
                      <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilmce) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <div class="fideicomisocetxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfce" value="<?php echo $denominacion_razonfce ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfcesi" maxlength="13" value="<?php echo $rfcfcesi ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofce" maxlength="40" value="<?php echo $identificador_fideicomisofce ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <h5>Datos de la cesión</h5>
                <div class="row">
                  <div class="col-md-5 form-group">
                    <label>Monto de la cesión en Moneda Nacional</label>
                    <input class="form-control" type="number" name="monto_cesionces" id="monto_cesionces" value="<?php echo $monto_cesionces ?>">
                  </div>
                </div>    
              <!-- -->  
              </div>
            </div>    
          </div>  
          <!-- =============================================== -->  
          <div class="row">

          </div>
          <div class="contrato_mutuo_creditotxt" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11"> 
                <div class="row">  
                  <div class="col-md-6 form-group">
                    <label>Tipo de Otorgamiento</label>
                    <select class="form-control" name="tipo_otorgamientoort" id="tipo_otorgamientoort">
                      <option value="1" <?php if($tipo_otorgamientoort==1) echo 'selected' ?>>Otorgamiento de Mutuo, Préstamo o Crédito sin Garantía</option>
                      <option value="2" <?php if($tipo_otorgamientoort==2) echo 'selected' ?>>Otorgamiento de Mutuo, Préstamo o Crédito con Garantía</option>
                    </select>
                  </div>
                </div>
                <hr>
                <h5>Datos de identificación del otorgante del mutuo o crédito o acreedor</h5>
                <h5>Tipo de persona</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaort" id="tipo_personaort1" value="1" onclick="tipo_persona_selectort()" <?php if($tipo_personaort==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaort" id="tipo_personaort2" value="2" onclick="tipo_persona_selectort()" <?php if($tipo_personaort==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaort" id="tipo_personaort3" value="3" onclick="tipo_persona_selectort()" <?php if($tipo_personaort==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="persona_fisicaorttxt" style="display: none">
                  <hr>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombreort" value="<?php echo $nombreort ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternoort" value="<?php echo $apellido_paternoort ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternoort" value="<?php echo $apellido_maternoort ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientoort" value="<?php echo $fecha_nacimientoort ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcorti" maxlength="13" value="<?php echo $rfcorti ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curport" maxlength="18" value="<?php echo $curport ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_ort_1" name="pais_nacionalidadort">
                        <?php if($pais_nacionalidadort!=""){
                         echo '<option value="'.$pais_nacionalidadort.'">'.$pais_nacionalidadortg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica u ocupación</label>
                      <select class="form-control" name="actividad_economicaort">
                      <?php foreach ($actividad_econominica_fisica_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$actividad_economicaort) echo 'selected' ?>><?php echo $item->acitividad ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>
                <div class="persona_moralorttxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmort" value="<?php echo $denominacion_razonmort ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmort" value="<?php echo $fecha_constitucionmort ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmort" maxlength="13" value="<?php echo $rfcmort ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_ortm_1" name="pais_nacionalidadmort">
                        <?php if($pais_nacionalidadmort!=""){
                         echo '<option value="'.$pais_nacionalidadmort.'">'.$pais_nacionalidadmortg.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilmort">
                      <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilmort) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <div class="fideicomisoorttxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfort" value="<?php echo $denominacion_razonfort ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfort" maxlength="13" value="<?php echo $rfcfort ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofort" maxlength="40" value="<?php echo $identificador_fideicomisofort ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <!-- -->
                <h5>Datos de identificación del solicitante del mutuo o crédito o deudor</h5>
                <h5>Tipo de persona</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personadeu" id="tipo_personadeu1" value="1" onclick="tipo_persona_selectdeu()" <?php if($tipo_personadeu==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personadeu" id="tipo_personadeu2" value="2" onclick="tipo_persona_selectdeu()" <?php if($tipo_personadeu==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personadeu" id="tipo_personadeu3" value="3" onclick="tipo_persona_selectdeu()" <?php if($tipo_personadeu==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="persona_fisicadeutxt" style="display: none">
                  <hr>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombredeu" value="<?php echo $nombredeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternodeu" value="<?php echo $apellido_paternodeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternodeu" value="<?php echo $apellido_maternodeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientodeu" value="<?php echo $fecha_nacimientodeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcdeui" maxlength="13" value="<?php echo $rfcdeui ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpdeu" maxlength="18" value="<?php echo $curpdeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_deu_1" name="pais_nacionalidaddeu">
                        <?php if($pais_nacionalidaddeu!=""){
                         echo '<option value="'.$pais_nacionalidaddeu.'">'.$pais_nacionalidaddeug.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica u ocupación</label>
                      <select class="form-control" name="actividad_economicadeu">
                      <?php foreach ($actividad_econominica_fisica_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$actividad_economicadeu) echo 'selected' ?>><?php echo $item->acitividad ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>
                <div class="persona_moraldeutxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmdeu" value="<?php echo $denominacion_razonmdeu ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmdeu" value="<?php echo $fecha_constitucionmdeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmdeu" maxlength="13" value="<?php echo $rfcmdeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_deum_1" name="pais_nacionalidadmdeu">
                        <?php if($pais_nacionalidadmdeu!=""){
                         echo '<option value="'.$pais_nacionalidadmdeu.'">'.$pais_nacionalidadmdeug.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Actividad económica o giro mercantil u objeto social</label>
                      <select class="form-control" name="giro_mercantilmdeu">
                      <?php foreach ($actividad_econominica_morales_get as $item){?>
                        <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilmdeu) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                      <?php } ?>  
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <div class="fideicomisodeutxt" style="display: none;">
                  <hr>
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfdeu" value="<?php echo $denominacion_razonfdeu ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfdeu" maxlength="13" value="<?php echo $rfcfdeu ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofdeu" maxlength="40" value="<?php echo $identificador_fideicomisofdeu ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                  <hr> 
                </div>
                <!-- -->
                <div id="cont_total_garantia">
                  <hr>
                  <h5>Datos de la garantía</h5>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Tipo de garantía</label>
                      <select class="form-control" name="tipo_garantiaort">
                        <option value="1" <?php if($tipo_garantiaort==1) echo 'selected' ?>>Sin garantía</option>
                        <option value="2" <?php if($tipo_garantiaort==2) echo 'selected' ?>>Inmueble</option>
                        <option value="3" <?php if($tipo_garantiaort==3) echo 'selected' ?>>Vehículo terrestre</option>
                        <option value="4" <?php if($tipo_garantiaort==4) echo 'selected' ?>>Vehículo aéreo</option>
                        <option value="5" <?php if($tipo_garantiaort==5) echo 'selected' ?>>Vehículo marítimo</option>
                        <option value="6" <?php if($tipo_garantiaort==6) echo 'selected' ?>>Priedras Preciosas</option>
                        <option value="7" <?php if($tipo_garantiaort==7) echo 'selected' ?>>Metales Preciosos</option>
                        <option value="8" <?php if($tipo_garantiaort==8) echo 'selected' ?>>Joyas o relojes</option>
                        <option value="9" <?php if($tipo_garantiaort==9) echo 'selected' ?>>Obras de arte o antigüedades</option>
                        <option value="10" <?php if($tipo_garantiaort==11) echo 'selected' ?>>Acciones o partes sociales</option>
                        <option value="11" <?php if($tipo_garantiaort==11) echo 'selected' ?>>Derechos fiduciarios</option>
                        <option value="12" <?php if($tipo_garantiaort==12) echo 'selected' ?>>Derechos de crédito</option>
                        <option value="15" <?php if($tipo_garantiaort==15) echo 'selected' ?>>Garantía Quirografaria</option>
                        <option value="99" <?php if($tipo_garantiaort==99) echo 'selected' ?>>Otro (Especificar)</option>
                      </select>
                    </div>
                  </div>
                  <h5>Datos del bien materia del mútuo, préstamo o crédito</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_bien_mutuoort" id="datos_bien_mutuoort1" value="1" onclick="datos_bien_mutuoort_select()" <?php if($datos_bien_mutuoort==1) echo 'checked' ?>>
                          Inmueble
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_bien_mutuoort" id="datos_bien_mutuoort2" value="2" onclick="datos_bien_mutuoort_select()" <?php if($datos_bien_mutuoort==2) echo 'checked' ?>>
                          Otro bien
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="datos_inmuebleorttxt" style="display: none;">
                    <div class="row">  
                      <div class="col-md-4 form-group">
                        <label>Tipo de inmueble</label>
                        <select class="form-control" name="tipo_inmuebleeort">
                        <?php foreach ($tipo_inmueble_get as $item){ ?>
                          <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmuebleeort) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } ?>  
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Valor de referencia del inmueble</label>
                        <input class="form-control" type="number" name="valor_referenciaort" maxlength="17" value="<?php echo $valor_referenciaort ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Código Postal de la ubicación del inmueble</label>
                        <input class="form-control" type="text" name="codigo_postalort" maxlength="5" value="<?php echo $codigo_postalort ?>">
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Folio real del inmueble o antecedentes registrales</label>
                        <input class="form-control" type="text" name="folio_realort" value="<?php echo $folio_realort ?>">
                      </div>
                    </div>  
                  </div>
                  <div class="datos_otroorttxt" style="display: none;">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Descripción cuando Tipo de Garantia es "Otro"</label>
                        <input class="form-control" type="text" name="descripcion_garantiaort" value="<?php echo $descripcion_garantiaort ?>">
                      </div>
                    </div>  
                  </div>  

                  <h5>Datos de tipo de persona del garante</h5>
                  <h5>Tipo de persona</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personagar" id="tipo_personagar1" value="1" onclick="tipo_persona_selectgar()" <?php if($tipo_personagar==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personagar" id="tipo_personagar2" value="2" onclick="tipo_persona_selectgar()" <?php if($tipo_personagar==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personagar" id="tipo_personagar3" value="3" onclick="tipo_persona_selectgar()" <?php if($tipo_personagar==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="persona_fisicagartxt" style="display: none">
                    <hr>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombregar" value="<?php echo $nombregar ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paternogar" value="<?php echo $apellido_paternogar ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_maternogar" value="<?php echo $apellido_maternogar ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimientogar" value="<?php echo $fecha_nacimientogar ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcgari" maxlength="13" value="<?php echo $rfcgari ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpgar" maxlength="18" value="<?php echo $curpgar ?>">
                      </div>
                    </div>
                    <hr>  
                  </div>
                  <div class="persona_moralgartxt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <input class="form-control" type="text" name="denominacion_razonmgar" value="<?php echo $denominacion_razonmgar ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionmgar" value="<?php echo $fecha_constitucionmgar ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmgar" maxlength="13" value="<?php echo $rfcmgar ?>">
                      </div>
                    </div> 
                    <hr> 
                  </div>
                  <div class="fideicomisogartxt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <input class="form-control" type="text" name="denominacion_razonfgar" value="<?php echo $denominacion_razonfgar ?>">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfgar" maxlength="13" value="<?php echo $rfcfgar ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="identificador_fideicomisofgar" maxlength="40" value="<?php echo $identificador_fideicomisofgar ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                      </div> 
                    <hr> 
                  </div>
                  <hr>
                </div>

                <h5>Datos de la forma de otorgamiento del crédito</h5> <!--actividad 6 -->
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Tipo de moneda o divisa de la operación o acto</label>
                    <select class="form-control" name="monedagar">
                    <?php /* foreach ($tipo_moneda_get as $item){ 
                      if($item->id==1){ ?>
                      <option value="<?php echo $item->id ?>" <?php if($item->clave==$monedagar) echo 'selected' ?>><?php echo $item->moneda ?></option>
                    <?php } } */ ?>
                    <option value="0" <?php if($monedagar=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                    <option value="1" <?php if($monedagar=="1") echo "selected"; ?>>Dólar Americano</option>
                    <option value="2" <?php if($monedagar=="2") echo "selected"; ?>>Euro</option>
                    <option value="3" <?php if($monedagar=="3") echo "selected"; ?>>Libra Esterlina</option>
                    <option value="4" <?php if($monedagar=="4") echo "selected"; ?>>Dólar canadiense</option>
                    <option value="5" <?php if($monedagar=="5") echo "selected"; ?>>Yuan Chino</option>
                    <option value="6" <?php if($monedagar=="6") echo "selected"; ?>>Centenario</option>
                    <option value="7" <?php if($monedagar=="7") echo "selected"; ?>>Onza de Plata</option>
                    <option value="8" <?php if($monedagar=="8") echo "selected"; ?>>Onza de Oro</option>  
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Monto de la operación o acto</label>
                    <input class="form-control" type="number" name="monto_operaciongar" maxlength="17" value="<?php echo $monto_operaciongar ?>">
                  </div>
                </div>
              </div>
            </div>        
          </div>  
          <div class="realizacion_avaluostxt" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Tipo de bien objeto de avalúo</label>
                    <select class="form-control" name="tipo_bienrea">
                      <option value="2" <?php if($tipo_bienrea==2) echo 'selected' ?>>Vehículo terrestre</option>
                      <option value="3" <?php if($tipo_bienrea==3) echo 'selected' ?>>Vehículo aéreo</option>
                      <option value="4" <?php if($tipo_bienrea==4) echo 'selected' ?>>Vehículo marítimo</option>
                      <option value="5" <?php if($tipo_bienrea==5) echo 'selected' ?>>Priedras Preciosas</option>
                      <option value="6" <?php if($tipo_bienrea==6) echo 'selected' ?>>Metales Preciosos</option>
                      <option value="7" <?php if($tipo_bienrea==7) echo 'selected' ?>>Joyas o relojes</option>
                      <option value="8" <?php if($tipo_bienrea==8) echo 'selected' ?>>Obras de arte o antigüedades</option>
                      <option value="99" <?php if($tipo_bienrea==99) echo 'selected' ?>>Otro (Especificar)</option>
                    </select>
                  </div>
                  <div class="col-md-12 form-group">
                    <label>Descripción del bien</label>
                    <input class="form-control" type="text" name="descripcionrea" value="<?php echo $descripcionrea ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Valor avalúo en Moneda Nacional</label>
                    <input class="form-control" type="number" name="valor_avaluorea" id="valor_avaluorea" minlength="4" maxlength="17" value="<?php echo $valor_avaluorea ?>">
                  </div>
                </div>
                <h5>Datos del propietario de los bienes objeto del avalúo</h5>
                <div class="row">
                  <div class="col-md-6 form-group">
                    <label>El propietario del bien es el que solicita la formalización del instrumento público </label>
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onclick="ValidForamlizacion()" name="propietario_solicita" value="SI" <?php if($propietario_solicita=='SI') echo 'checked' ?>>
                            SI 
                          </label>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onclick="ValidForamlizacion()" name="propietario_solicita" value="NO" <?php if($propietario_solicita=='NO') echo 'checked' ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
                <div id="DTPBOV">
                  <h5>Datos de tipo de persona del propietario de los bienes objeto del avalúo si no es quien solicita la formalización del instrumento público</h5>
                  <h5>Tipo de persona</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personarea" id="tipo_personarea1" value="1" onclick="tipo_persona_selectrea()" <?php if($tipo_personarea==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personarea" id="tipo_personarea2" value="2" onclick="tipo_persona_selectrea()" <?php if($tipo_personarea==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personarea" id="tipo_personarea3" value="3" onclick="tipo_persona_selectrea()" <?php if($tipo_personarea==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="persona_fisicareatxt" style="display: none">
                    <hr>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombrerea" value="<?php echo $nombrerea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paternorea" value="<?php echo $apellido_paternorea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_maternorea" value="<?php echo $apellido_maternorea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimientorea" value="<?php echo $fecha_nacimientorea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcreai" maxlength="13" value="<?php echo $rfcreai ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curprea" maxlength="18" value="<?php echo $curprea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_rea_1" name="pais_nacionalidadrea">
                          <?php if($pais_nacionalidadrea!=""){
                           echo '<option value="'.$pais_nacionalidadrea.'">'.$pais_nacionalidadreag.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                  </div>
                  <div class="persona_moralreatxt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <input class="form-control" type="text" name="denominacion_razonmrea" value="<?php echo $denominacion_razonmrea ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionmrea" value="<?php echo $fecha_constitucionmrea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmrea" maxlength="13" value="<?php echo $rfcmrea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_ream_1" name="pais_nacionalidadmrea">
                          <?php if($pais_nacionalidadmrea!=""){
                           echo '<option value="'.$pais_nacionalidadmrea.'">'.$pais_nacionalidadmreag.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                  <div class="fideicomisoreatxt" style="display: none;">
                    <hr>
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <input class="form-control" type="text" name="denominacion_razonfrea" value="<?php echo $denominacion_razonfrea ?>">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfrea" maxlength="13" value="<?php echo $rfcfrea ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="identificador_fideicomisofrea" maxlength="40" value="<?php echo $identificador_fideicomisofrea ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                    <hr> 
                  </div>
                </div>
                <!-- -->
              </div>
            </div>
          </div>      
          <!-- == -->  
        </form>  
        <?php /* 
        <form id="form_anexo1111" class="forms-sample">
          
        </form>  
        */ ?>    
        <!------  ------>
      <!-- -->

      <div class="modal-footer">
        <button class="btn gradient_nepal2" type="button" id="btn_submit" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
        <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
      </div>
        
    </div>
  </div>
</div>

<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 caracteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_descripcion_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Descripción del bien</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Longitud mínima 1 carácter y máxima 3000.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos y diagonal.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_intrumentoPublico_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de Instrumento Público</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 20 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda,guión bajo, y guión medio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliomer_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio mercantil o antecedentes registrales</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 200 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda,guión bajo, y guión medio.</li>
           <li>Capturar el folio mercantil o de inscripción en el registro que corresponda, en caso de no contar con uno por el marco juridico local, se deberán establece los datos de los antecedentes registrales según corresponda.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_NIPFE_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de Instrumento Público de la fusión o escisión</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 20 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda,guión bajo, y guión medio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_NRIF_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número, referencia o identificador fideicomiso</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 40 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda, coma, @, &, guión bajo, y guión medio.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_descripcion_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Descripción del bien</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Longitud mínima 1 carácter y máxima 3000.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos y diagonal.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_DTFCO_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Descripción del tipo de fideicomiso cuando es otro</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 200 caracteres.</li>
           <li>Acepta letras, numeros,, escpacio, coma, punto, dos puntos, diagonal, apostrofe, signo de pesos y guion medio.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--modals modificatorio de ayuda-->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>