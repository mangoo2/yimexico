<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<!-- Tipo actividad 3-->
<input type="hidden" id="aux_cpm" value="<?php echo $aux_cpm ?>">
<!-- Tipo actividad 4-->
<input type="hidden" id="aux_dcv" value="<?php echo $aux_dcv ?>">
<!-- -->
<input type="hidden" id="aux_tipo_actividad" value="<?php echo $tipo_actividad ?>">
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">   
            <h4>Anexo 12B - Servidores Públicos</h4> 
            <h3>Registro de transacción</h3>
          </div>
        </div>
        <!--- --->
        <hr class="subtitle">
        <!--- --->
        <form id="form_anexo">
          <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <!-- -->  
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Fecha de operación: Fecha de recepción de recursos</label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" value="<?php echo $referencia ?>" placeholder="Ingrese número de referencia">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group" style="display: none">
              <label>Monto de la factura sin IVA ni accesorios</label>
              <input type="text" id="monto_opera" class="form-control monto_1" onchange="tipo_modeda(1)" placeholder="Ingrese el monto total de la operación ($)" required="" value="<?php echo $monto_opera ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-4 form-group" style="display: none">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" value="<?php echo $num_factura ?>" placeholder="Ingrese número de factura">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!--- --->
          <div class="row"> 
            <div class="col-md-8 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-4 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <!-- -->
          <?php if($aviso==1){ ?>
            <hr class="subsubtitle">
            <div class="row">  
              <div class="col-md-12">
              </div>
              <div class="col-md-10 form-group">
                <h5>Datos de la modificación:</h5>
              </div>
              <div class="col-md-2 form-group">
              </div>
              <div class="col-md-3 form-group">
                <label>Referencia modificación:</label>
                <div class="d-flex mb-2">  
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                    <i class="mdi mdi-help text-dark"></i>
                  </button>   
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label>Folio de aviso que se modificará:</label>
                <input type="hidden" name="idanexo12b" id="idanexo12b" class="form-control" value="<?php echo $idanexo12b ?>">
                <div class="d-flex mb-2">  
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                    <i class="mdi mdi-help text-dark"></i>
                  </button>   
                </div>
              </div>
              <div class="col-md-6 form-group">
                <label>Descripción de la modificación:</label>
                <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90"><?php echo $descrip_modifica ?></textarea>
              </div>
            </div>     
            <?php }?>
          <!-- -->
          <hr class="subsubtitle">
          <h5>Tipo de Actividad Vulnerable</h5>
        </form>
        <form id="form_anexo2">
          <!-- tipo_actividad1 1-->
          <div class="row">
            <div class="col-md-4 form-group">
              <select class="form-control" name="tipo_actividad" id="tipo_actividad" onchange="otorgamiento_poder_select(this.value)">
                <option value="1" <?php if($tipo_actividad=="1") echo "selected"; ?>>Constitución o Transmisión de derechos reales sobre inmuebles </option>
                <option value="2" <?php if($tipo_actividad=="2") echo "selected"; ?>>Otorgamiento de poder irrevocable para actos de administración o para actos de dominio </option>
                <option value="3" <?php if($tipo_actividad=="3") echo "selected"; ?>>Constitución de personas Morales </option>
                <option value="4" <?php if($tipo_actividad=="4") echo "selected"; ?>>Modificacion patrimonial por aumento de capital o por disminucion de capital </option>
                <option value="5" <?php if($tipo_actividad=="5") echo "selected"; ?>>Otorgamiento de contratos de mutuo o crédito con o sin garantía </option>
                <option value="6" <?php if($tipo_actividad=="6") echo "selected"; ?>>Realización de Avalúos </option>
              </select>
            </div>

            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_actividad" value="1" id="tipo_actividad1" onclick="otorgamiento_poder_select(1)" <?php if($tipo_actividad==1) echo 'checked' ?>>
                    Constitución o Transmisión de derechos reales sobre inmuebles
                </label>
              </div>
            </div>-->
          </div>  
        </form>  

        <div class="tipo_actividadtxt1" style="display: none">
          <!--  -->
          <form id="form_actividad1">
            <input type="hidden" name="actividad" value="<?php echo $actividad1 ?>">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <!-- -->  
                <div class="row">
                  <div class="col-md-12 form-group">
                    <label>Órgano Jurisdiccional</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="organoact1" value="<?php echo $organoact1 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                    </div>
                  </div>
                  <div class="col-md-7 form-group">
                    <label>Tipo de juicio</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="tipo_juicioact1" maxlength="50" value="<?php echo $tipo_juicioact1 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_tipodeJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                    </div>
                  </div>
                  <div class="col-md-5 form-group">
                    <label>Materia</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="materiaact1" maxlength="20" value="<?php echo $materiaact1 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_materia_ayuda()"><i class="mdi mdi-help"></i></button>              
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Expediente</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="expedienteact1" maxlength="20"  value="<?php echo $expedienteact1 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_expediente_ayuda()"><i class="mdi mdi-help"></i></button>              
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Tipo de acto realizado</label>
                    <select class="form-control" name="tipo_actoact1" id="tipo_actoact1" onchange="tipo_acto_opt()">
                      <option value="1" <?php if($tipo_actoact1==1) echo 'selected' ?>>Prescripción Positiva</option>
                      <option value="2" <?php if($tipo_actoact1==2) echo 'selected' ?>>Dación en Pago</option>
                      <option value="3" <?php if($tipo_actoact1==3) echo 'selected' ?>>Adjudicación</option>
                      <option value="9" <?php if($tipo_actoact1==9) echo 'selected' ?>>Otro</option>
                    </select>
                  </div>
                </div>      
                <div class="tipo_actotxt" style="display: none">
                  <div class="row">  
                    <div class="col-md-6 form-group">
                      <label>Tipo de acto realizado cuando sea "Otro"</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="tipo_acto_otroact1" maxlength="50"  value="<?php echo $tipo_acto_otroact1 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_TARCSO_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="subsubtitle">
                <h5>Características del o los inmuebles objeto del aviso</h5>
                <div class="row">    
                    <div class="col-md-6 form-group">
                      <label>Tipo de bien inmueble</label>
                      <select class="form-control" name="tipo_inmuebleact1">
                        <?php foreach ($inmueble->result() as $item){ ?>
                          <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmuebleact1) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } ?>
                      </select><!--select-->
                    </div>
                </div>
                <div class="row">    
                  <div class="col-md-6 form-group">
                    <label>Valor catastral del inmueble en Moneda Nacional</label>
                    <input class="form-control" type="number" name="valor_catastralact1" minlength="4" maxlength="17" value="<?php echo $valor_catastralact1 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Código Postal de la ubicación del inmueble</label>
                    <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="codigo_postalact1" id="codigo_postalact1" value="<?php echo $codigo_postalact1 ?>">
                  </div>
                  
                  <div class="col-md-12 form-group">
                    <label>Calle, avenida o vía de la ubicación del inmueble</label>
                    <input class="form-control" type="text" name="calleact1" maxlength="100" value="<?php echo $calleact1 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Número exterior</label>
                    <input class="form-control" type="text" name="numero_exterioract1"  maxlength="56" value="<?php echo $numero_exterioract1 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Número interior</label>
                    <input class="form-control" type="text" name="numero_interioract1" maxlength="40" value="<?php echo $numero_interioract1 ?>">
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Colonia de la ubicación del inmueble</label>
                    <select onclick="autoComplete(this.id)" class="form-control" name="coloniaact1" id="coloniaact1">
                      <option value=""></option>
                      <?php if($coloniaact1!=""){
                        echo '<option value="'.$coloniaact1.'" selected>'.$coloniaact1.'</option>';      
                      } ?>  
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Dimensiones del inmueble en m2 del terreno</label>
                    <input class="form-control" type="number" name="dimension_terrenoact1" maxlength="10" value="<?php echo $dimension_terrenoact1 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Dimensiones del inmueble en m2 Construidos</label>
                    <input class="form-control" type="text" name="dimension_construidoact1" maxlength="10" value="<?php echo $dimension_construidoact1 ?>">
                  </div>
                  <div class="col-md-12 form-group">
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <label>Folio real del inmueble o antecedentes registrales</label>
                    <input class="form-control" type="text" name="folio_realact1" value="<?php echo $folio_realact1 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_folio_ayuda()"><i class="mdi mdi-help"></i></button>
                    </div>
                  </div>
                </div>
                <hr class="subsubtitle">
                <h5>Datos de las personas que intervienen en el acto</h5>
                <div class="row">    
                  <div class="col-md-6 form-group">
                    <label>Carácter de la persona que interviene en el acto</label>
                    <select class="form-control" name="caractervat1" id="caractervat1" onchange="caracter_opt()"> 
                      <option value="1" <?php if($caractervat1==1) echo 'selected' ?>>Poseedor</option>
                      <option value="2" <?php if($caractervat1==2) echo 'selected' ?>>Deudor</option>
                      <option value="3" <?php if($caractervat1==3) echo 'selected' ?>>Acreedor</option>
                      <option value="4" <?php if($caractervat1==4) echo 'selected' ?>>Transmitente</option>
                      <option value="5" <?php if($caractervat1==5) echo 'selected' ?>>Receptor</option>
                      <option value="9" <?php if($caractervat1==9) echo 'selected' ?>>Otro </option>
                    </select> <!--select-->
                  </div>
                </div>
                <div class="caractertxt" style="display: none">
                  <div class="row">  
                    <div class="col-md-6 form-group">
                      <label>Tipo de carácter de la persona que interviene en el acto cuando sea "Otro"</label>
                      <input class="form-control" type="text" name="caracter_otroact1" maxlength="50"  value="<?php echo $caracter_otroact1 ?>">
                    </div>
                  </div>
                </div>    
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact1" id="tipo_persona_1_1" value="1" onclick="tipo_persona_opt(1)" <?php if($tipo_personaact1==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact1" id="tipo_persona_1_2" value="2" onclick="tipo_persona_opt(1)" <?php if($tipo_personaact1==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact1" id="tipo_persona_1_3" value="3" onclick="tipo_persona_opt(1)" <?php if($tipo_personaact1==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_persona_txt_1_1" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombreact1" value="<?php echo $nombreact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternact1" value="<?php echo $apellido_paternact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternact1" value="<?php echo $apellido_maternact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientact1" value="<?php echo $fecha_nacimientact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccact1" maxlength="13" value="<?php echo $rfccact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpcact1" maxlength="18" value="<?php echo $curpcact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act1_1" name="pais_nacionalidadcact1">
                        <?php if($pais_nacionalidadcact1!=""){
                         echo '<option value="'.$pais_nacionalidadcact1.'">'.$pais_nacionalidadcact1t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>  
                <div class="tipo_persona_txt_1_2" style="display: none">
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmact1" value="<?php echo $denominacion_razonmact1 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmact1" value="<?php echo $fecha_constitucionmact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmact1" maxlength="13" value="<?php echo $rfcmact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act1_1" name="pais_nacionalidadmact1">
                        <?php if($pais_nacionalidadmact1!=""){
                         echo '<option value="'.$pais_nacionalidadmact1.'">'.$pais_nacionalidadmact1t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <h5>Representante o apoderado legal o persona que realice el acto u operación a nombre de la persona moral</h5>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombremact1" value="<?php echo $nombremact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternamact1" value="<?php echo $apellido_paternamact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternamact1" value="<?php echo $apellido_maternamact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientamact1" value="<?php echo $fecha_nacimientamact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccamact1" maxlength="13" value="<?php echo $rfccamact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpcamact1" maxlength="18" value="<?php echo $curpcamact1 ?>">
                    </div>
                  </div>
                  <hr> 
                </div>  
                <div class="tipo_persona_txt_1_3" style="display: none">
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfact1" value="<?php echo $denominacion_razonfact1 ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfact1" maxlength="13" value="<?php echo $rfcfact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofact1" maxlength="40" value="<?php echo $identificador_fideicomisofact1 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                  <h5>Apoderado legal o delegado fiduciario que realice el acto u operación a nombre del fideicomiso</h5>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombrefact1" value="<?php echo $nombrefact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternafact1" value="<?php echo $apellido_paternafact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternafact1" value="<?php echo $apellido_maternafact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientafact1" value="<?php echo $fecha_nacimientafact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccafact1" maxlength="13" value="<?php echo $rfccafact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpcafact1" maxlength="18" value="<?php echo $curpcafact1 ?>">
                    </div>
                  </div>
                  <hr> 
                </div> 
                <hr class="subsubtitle">
                <h5>Datos del domicilio de la persona objeto del aviso</h5> 
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_domicilioact1" id="domicilio_oficinaact11" value="1" onchange="domicilio_oficina_opt(1)" <?php if($tipo_domicilioact1==1) echo 'checked' ?>>
                        Domicilio Nacional
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_domicilioact1" id="domicilio_oficinaact12" value="2" onchange="domicilio_oficina_opt(1)" <?php if($tipo_domicilioact1==2) echo 'checked' ?>>
                       Domicilio Extranjero
                      </label>
                    </div>
                  </div>
                </div>
                <div class="domicilio_oficina1txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="codigo_postalnaact1" id="codigo_postalnaact1" value="<?php echo $codigo_postalnaact1 ?>">
                    </div>
                    
                    <div class="col-md-6 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="callenanaact1" maxlength="100" value="<?php echo $callenanaact1 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exteriornaact1" value="<?php echo $numero_exteriornaact1 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interiornaact1" value="<?php echo $numero_interiornaact1 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <select onclick="autoComplete(this.id)" class="form-control" name="colonianaact1" id="colonianaact1">
                      <option value=""></option>
                      <?php if($colonianaact1!=""){
                        echo '<option value="'.$colonianaact1.'" selected>'.$colonianaact1.'</option>';      
                      } ?>  
                    </select>
                    </div>
                  </div>  
                </div>
                <div class="domicilio_oficina1txt2" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Clave del país donde se encuentra el domicilio</label>
                      <select class="form-control pais_act1_1" name="paisexact1"> 
                        <?php if($paisexact1!=""){
                          echo '<option value="'.$paisexact1.'">'.$paisexact1t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Estado, provincia, departamento o demarcación política similar que corresponda</label>
                      <input class="form-control" type="text" name="estado_provinciaexact1" value="<?php echo $estado_provinciaexact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Ciudad o población</label>
                      <input class="form-control" type="text" name="ciudad_poblacionexact1" value="<?php echo $ciudad_poblacionexact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" pattern="\d*" maxlength="5" type="text" name="codigo_postalexact1" id="codigo_postalexact1" value="<?php echo $codigo_postalexact1 ?>">
                    </div>
                    
                    <div class="col-md-4 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleexact1" value="<?php echo $calleexact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exteriorexact1" value="<?php echo $numero_exteriorexact1 ?>">
                    </div>
                     <div class="col-md-4 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interiorexact1" value="<?php echo $numero_interiorexact1 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Colonia</label>
                      <input class="form-control" type="text" name="coloniaexact1" value="<?php echo $coloniaexact1 ?>">
                      <!--<select onclick="autoComplete(this.id)" class="form-control" name="coloniaexact1" id="coloniaexact1">
                        <option value=""></option>
                        <?php if($coloniaexact1!=""){
                          echo '<option value="'.$coloniaexact1.'" selected>'.$coloniaexact1.'</option>';      
                        } ?>  
                      </select>-->
                    </div>
                  </div>  
                </div>
                <h5>Datos del número telefónico</h5>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Clave del país</label>
                    <select class="form-control pais_act1_1" name="clave_paisact1">
                      <?php if($clave_paisact1!=""){
                        echo '<option value="'.$clave_paisact1.'">'.$clave_paisact1t.'</option>'; 
                      } ?>
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Número de teléfono</label>
                    <input class="form-control" type="text" name="numero_telefonoact1" value="<?php echo $numero_telefonoact1 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Correo electrónico</label>
                    <input class="form-control" type="text" name="correo_electronicoact1" minlength="10" maxlength="60" value="<?php echo $correo_electronicoact1 ?>">
                  </div>
                </div>
                <!-- -->
              </div>
            </div> 
          </form>      
          <!--  -->
        </div>  
        <form id="form_anexo3">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_actividad" value="2" id="tipo_actividad2" onclick="otorgamiento_poder_select(2)" <?php if($tipo_actividad==2) echo 'checked' ?>>
                    Otorgamiento de poder irrevocable para actos de administración o para actos de dominio
                </label>
              </div>
            </div>-->
          </div>
        </form>    
        <div class="tipo_actividadtxt2" style="display: none">
          <form id="form_actividad2">
            <input type="hidden" name="actividad" value="<?php echo $actividad2 ?>">
            <!--  -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <!-- -->  
                <h5>Tipo de autoridad</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact2" id="tipo_autoridadact21" value="1" onchange="tipo_autoridad_opt(2)" <?php if($tipo_autoridadact2==1) echo 'checked' ?>>
                        Administrativo
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact2" id="tipo_autoridadact22" value="2" onchange="tipo_autoridad_opt(2)" <?php if($tipo_autoridadact2==2) echo 'checked' ?>>
                       Jurisdiccional
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_autoridadact2txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Órgano administrativo</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organoact2" maxlength="100" value="<?php echo $organoact2 ?>">
                    </div><button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoA_ayuda()"><i class="mdi mdi-help"></i></button></div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Cargo de la autoridad ante quien se realiza el acto</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="cargoact2" maxlength="100" value="<?php echo $cargoact2 ?>">
                      </div><button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_CAAQSRA_ayuda()"><i class="mdi mdi-help"></i></button></div>
                    </div>
                    <div class="col-md-4 form-group">
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <label>Número de Instrumento Público</label>
                      <input class="form-control" type="text" name="instrumento_publicoact2" maxlength="20" value="<?php echo $instrumento_publicoact2 ?>">
                      </div><button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NIP_ayuda()"><i class="mdi mdi-help"></i></button></div>
                    </div>
                  </div>  
                </div>
                <div class="tipo_autoridadact2txt2" style="display: none">
                  <div class="row">  
                    <div class="col-md-12 form-group">
                      <label>Órgano Jurisdiccional</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organojact2" maxlength="100" value="<?php echo $organojact2 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoJ_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Tipo de juicio</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="tipo_juicioact2" maxlength="50" value="<?php echo $tipo_juicioact2 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_tipodeJ_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Materia</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="materiaact2" maxlength="20" value="<?php echo $materiaact2 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_materia_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Expediente</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="expedienteact2" maxlength="20" value="<?php echo $expedienteact2 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_expediente_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div>   
                </div>
                <hr class="subsubtitle">
                <h5>Datos del domicilio de la oficina donde se realiza el acto</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact2" id="domicilio_oficinaact21" value="1" onchange="domicilio_oficina_opt(2)" <?php if($domicilio_oficinaact2==1) echo 'checked' ?>>
                        Domicilio Nacional
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact2" id="domicilio_oficinaact22" value="2" onchange="domicilio_oficina_opt(2)" <?php if($domicilio_oficinaact2==2) echo 'checked' ?>>
                       Domicilio Extranjero
                      </label>
                    </div>
                  </div>
                </div>
                <div class="domicilio_oficina2txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleact2" maxlength="100" value="<?php echo $calleact2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="codigo_postalact2" id="codigo_postalact2" value="<?php echo $codigo_postalact2 ?>">
                    </div>
                    
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioract2" maxlength="56" value="<?php echo $numero_exterioract2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioract2" maxlength="40" value="<?php echo $numero_interioract2 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <select onclick="autoComplete(this.id)" class="form-control" name="coloniaact2" id="coloniaact2">
                        <option value=""></option>
                        <?php if($coloniaact2!=""){
                          echo '<option value="'.$coloniaact2.'" selected>'.$coloniaact2.'</option>';      
                        } ?>  
                      </select>
                    </div>
                  </div>  
                </div>
                <div class="domicilio_oficina2txt2" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Clave del país donde se encuentra el domicilio</label>
                      <select class="form-control pais_act2_2" name="paisact2">
                        <?php if($paisact2!=""){
                          echo '<option value="'.$paisact2.'">'.$paisact2t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Estado, provincia, departamento o demarcación política similar que corresponda</label>
                      <input class="form-control" type="text" name="estado_provinciaeact2" maxlength="100" value="<?php echo $estado_provinciaeact2 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Ciudad o población</label>
                      <input class="form-control" type="text" name="ciudad_poblacioneact2" maxlength="100" value="<?php echo $ciudad_poblacioneact2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" pattern="\d*" maxlength="5" type="text" name="codigo_postaleact2" id="codigo_postaleact2" value="<?php echo $codigo_postaleact2 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleeact2" maxlength="100" value="<?php echo $calleeact2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioreact2" maxlength="56" value="<?php echo $numero_exterioreact2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioreact2" maxlength="40" value="<?php echo $numero_interioreact2 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <input class="form-control" type="text" name="coloniaeact2" value="<?php echo $coloniaeact2 ?>">
                      <!--<select onclick="autoComplete(this.id)" class="form-control" name="coloniaeact2" id="coloniaeact2">
                        <option value=""></option>
                        <?php if($coloniaeact2!=""){
                          echo '<option value="'.$coloniaeact2.'" selected>'.$coloniaeact2.'</option>';      
                        } ?>  
                      </select>-->
                    </div>
                  </div>  
                </div>  
                <hr class="subsubtitle">
                <h5>Datos de la persona que solicita el acto u operación</h5>     
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Nombre(s)</label>
                    <input class="form-control" type="text" name="nombreact2" value="<?php echo $nombreact2 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Paterno</label>
                    <input class="form-control" type="text" name="apellido_paternoact2" value="<?php echo $apellido_paternoact2 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Materno</label>
                    <input class="form-control" type="text" name="apellido_maternoact2" value="<?php echo $apellido_maternoact2 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Fecha de Nacimiento</label>
                    <input class="form-control" type="date" name="fecha_nacimientoact2" value="<?php echo $fecha_nacimientoact2 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcact2" value="<?php echo $rfcact2 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave Única de Registro de Población</label>
                    <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpact2" value="<?php echo $curpact2 ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Clave pais nacionalidad</label>
                    <select class="form-control pais_act2_2" name="pais_nacionalidadact2">
                        <?php if($pais_nacionalidadact2!=""){
                          echo '<option value="'.$pais_nacionalidadact2.'">'.$pais_nacionalidadact2t.'</option>'; 
                        } ?>
                      </select>
                  </div>
                </div>
                <hr class="subsubtitle">
                <h5>Datos del Poderdante</h5>
                <h5>Tipo de Persona</h5> 
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personapact2" id="tipo_persona_21_1" value="1" onclick="tipo_persona_opt(21)" <?php if($tipo_personapact2==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personapact2" id="tipo_persona_21_2" value="2" onclick="tipo_persona_opt(21)" <?php if($tipo_personapact2==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personapact2" id="tipo_persona_21_3" value="3" onclick="tipo_persona_opt(21)" <?php if($tipo_personapact2==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_persona_txt_21_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombrepact2" value="<?php echo $nombrepact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paternapct2" value="<?php echo $apellido_paternapct2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_maternapct2" value="<?php echo $apellido_maternapct2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimientapct2" value="<?php echo $fecha_nacimientapct2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcpact2" maxlength="13" value="<?php echo $rfcpact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curppact2" maxlength="18" value="<?php echo $curppact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_act2_2" name="pais_nacionalidadpact2">
                          <?php if($pais_nacionalidadpact2!=""){
                           echo '<option value="'.$pais_nacionalidadpact2.'">'.$pais_nacionalidadpact2t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                </div>  
                <div class="tipo_persona_txt_21_2" style="display: none">
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmpact2" value="<?php echo $denominacion_razonmpact2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmpact2" value="<?php echo $fecha_constitucionmpact2 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmact2" maxlength="13" value="<?php echo $rfcmact2 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act2_2" name="pais_nacionalidadmpact2">
                        <?php if($pais_nacionalidadmpact2!=""){
                         echo '<option value="'.$pais_nacionalidadmpact2.'">'.$pais_nacionalidadmpact2t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>  
                <div class="tipo_persona_txt_21_3" style="display: none">
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfpact2" value="<?php echo $denominacion_razonfpact2 ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfpact2" maxlength="13" value="<?php echo $rfcfpact2 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofpact2" maxlength="40" value="<?php echo $identificador_fideicomisofpact2 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                  <hr> 
                </div> 
                <hr class="subsubtitle">
                <h5>Datos de los Apoderados</h5>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Tipo de poder</label>
                    <select class="form-control" name="tipo_poderact2">
                      <option value="1" <?php if($tipo_poderact2==1) echo 'selected' ?>>de administración</option>
                      <option value="2" <?php if($tipo_poderact2==2) echo 'selected' ?>>de dominio</option>
                      <option value="3" <?php if($tipo_poderact2==3) echo 'selected' ?>>de administración y de dominio</option>
                    </select>
                  </div>
                </div>
                <h5>Tipo de Persona</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaaact2" id="tipo_persona_22_1" value="1" onclick="tipo_persona_opt(22)" <?php if($tipo_personaaact2==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaaact2" id="tipo_persona_22_2" value="2" onclick="tipo_persona_opt(22)" <?php if($tipo_personaaact2==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaaact2" id="tipo_persona_22_3" value="3" onclick="tipo_persona_opt(22)" <?php if($tipo_personaaact2==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_persona_txt_22_1" style="display: none">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Nombre(s)</label>
                        <input class="form-control" type="text" name="nombreaact2" value="<?php echo $nombreaact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Paterno</label>
                        <input class="form-control" type="text" name="apellido_paternaact2" value="<?php echo $apellido_paternaact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Apellido Materno</label>
                        <input class="form-control" type="text" name="apellido_maternaact2" value="<?php echo $apellido_maternaact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                        <input class="form-control" type="date" name="fecha_nacimientaact2" value="<?php echo $fecha_nacimientaact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcaact2" maxlength="13" value="<?php echo $rfcaact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave Única de Registro de Población (CURP)</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpaact2" maxlength="18" value="<?php echo $curpaact2 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Clave país nacionalidad</label>
                        <select class="form-control pais_act1_1" name="pais_nacionalidadaact2">
                          <?php if($pais_nacionalidadaact2!=""){
                           echo '<option value="'.$pais_nacionalidadaact2.'">'.$pais_nacionalidadaact2t.'</option>'; 
                          } ?>
                        </select>
                      </div>
                    </div>
                    <hr>  
                </div>  
                <div class="tipo_persona_txt_22_2" style="display: none">
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmaact2" value="<?php echo $denominacion_razonmaact2 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmaact2" value="<?php echo $fecha_constitucionmaact2 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmaact2" maxlength="13" value="<?php echo $rfcmaact2 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act1_1" name="pais_nacionalidadmaact2">
                        <?php if($pais_nacionalidadmaact2!=""){
                         echo '<option value="'.$pais_nacionalidadmaact2.'">'.$pais_nacionalidadmaact2t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>  
                <div class="tipo_persona_txt_22_3" style="display: none">
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfaact2" value="<?php echo $denominacion_razonfaact2 ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfaact2" maxlength="13" value="<?php echo $rfcfaact2 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofaact2" maxlength="40" value="<?php echo $identificador_fideicomisofaact2 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                  <hr> 
                </div> 
                <!-- -->
              </div>
            </div>
            <!-- -->
          </form>        
        </div>
        <form id="form_anexo4">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_actividad" value="3" id="tipo_actividad3" onclick="otorgamiento_poder_select(3)" <?php if($tipo_actividad==3) echo 'checked' ?>>
                    Constitución de personas Morales
                </label>
              </div>
            </div>-->
          </div>  
        </form>  
        <div class="tipo_actividadtxt3" style="display: none">
          <!--  -->
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
              <form id="form_actividad3">
                <input type="hidden" name="actividad" value="<?php echo $actividad3 ?>">
                <!--  -->
                <h5>Tipo de autoridad</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact3" id="tipo_autoridadact31" value="1" onchange="tipo_autoridad_opt(3)" <?php if($tipo_autoridadact3==1) echo 'checked' ?>>
                        Administrativo
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact3" id="tipo_autoridadact32" value="2" onchange="tipo_autoridad_opt(3)" <?php if($tipo_autoridadact3==2) echo 'checked' ?>>
                       Jurisdiccional
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_autoridadact3txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Órgano administrativo</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organoact3" maxlength="100" value="<?php echo $organoact3 ?>">
                      </div><button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoA_ayuda()"><i class="mdi mdi-help"></i></button></div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Cargo de la autoridad ante quien se realiza el acto</label>
                      <input class="form-control" type="text" name="cargoact3" maxlength="100" value="<?php echo $cargoact3 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número de Instrumento Público</label>
                      <input class="form-control" type="text" name="instrumento_publicoact3" maxlength="20" value="<?php echo $instrumento_publicoact3 ?>">
                    </div>
                  </div>  
                </div>
                <div class="tipo_autoridadact3txt2" style="display: none">
                  <div class="row">  
                    <div class="col-md-12 form-group">
                      <label>Órgano Jurisdiccional</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organojact3" maxlength="100" value="<?php echo $organojact3 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                    </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Tipo de juicio</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="tipo_juicioact3" maxlength="50" value="<?php echo $tipo_juicioact3 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_tipodeJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Materia</label>
                      <input class="form-control" type="text" name="materiaact3" maxlength="20" value="<?php echo $materiaact3 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Expediente</label>
                      <input class="form-control" type="text" name="expedienteact3" maxlength="20" value="<?php echo $expedienteact3 ?>">
                    </div>
                  </div>   
                </div>
                <hr class="subsubtitle">
                <h5>Datos del domicilio de la oficina donde se realiza el acto</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact3" id="domicilio_oficinaact31" value="1" onchange="domicilio_oficina_opt(3)" <?php if($domicilio_oficinaact3==1) echo 'checked' ?>>
                        Domicilio Nacional
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact3" id="domicilio_oficinaact32" value="2" onchange="domicilio_oficina_opt(3)" <?php if($domicilio_oficinaact3==2) echo 'checked' ?>>
                       Domicilio Extranjero
                      </label>
                    </div>
                  </div>
                </div>
                <div class="domicilio_oficina3txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="codigo_postalact3" id="codigo_postalact3" value="<?php echo $codigo_postalact3 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleact3" maxlength="100" value="<?php echo $calleact3 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioract3" maxlength="56" value="<?php echo $numero_exterioract3 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioract3" maxlength="40" value="<?php echo $numero_interioract3 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <select onclick="autoComplete(this.id)" class="form-control" name="coloniaact3" id="coloniaact3">
                        <option value=""></option>
                        <?php if($coloniaact3!=""){
                          echo '<option value="'.$coloniaact3.'" selected>'.$coloniaact3.'</option>';      
                        } ?>  
                      </select>
                    </div>
                  </div>  
                </div>
                <div class="domicilio_oficina3txt2" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Clave del país donde se encuentra el domicilio</label>
                      <select class="form-control pais_act3_3" name="paisact3">
                        <?php if($paisact3!=""){
                          echo '<option value="'.$paisact3.'">'.$paisact3t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Estado, provincia, departamento o demarcación política similar que corresponda</label>
                      <input class="form-control" type="text" name="estado_provinciaeact3" maxlength="100" value="<?php echo $estado_provinciaeact3 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Ciudad o población</label>
                      <input class="form-control" type="text" name="ciudad_poblacioneact3" maxlength="100" value="<?php echo $ciudad_poblacioneact3 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control"  pattern="\d*" maxlength="5" type="text" name="codigo_postaleact3" id="codigo_postaleact3" value="<?php echo $codigo_postaleact3 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleeact3" maxlength="100" value="<?php echo $calleeact3 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <input class="form-control" type="text" name="coloniaeact3" value="<?php echo $coloniaeact3 ?>">
                      <!--<select onclick="autoComplete(this.id)" class="form-control" name="coloniaeact3" id="coloniaeact3">
                        <option value=""></option>
                        <?php if($coloniaeact3!=""){
                          echo '<option value="'.$coloniaeact3.'" selected>'.$coloniaeact3.'</option>';      
                        } ?>  
                      </select>-->
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioreact3" maxlength="56" value="<?php echo $numero_exterioreact3 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioreact3" maxlength="40" value="<?php echo $numero_interioreact3 ?>">
                    </div>
                    
                  </div>  
                </div>  
                <hr class="subsubtitle">
                <h5>Datos de la persona que solicita el acto u operación</h5>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Nombre(s)</label>
                    <input class="form-control" type="text" name="nombreact3" value="<?php echo $nombreact3 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Paterno</label>
                    <input class="form-control" type="text" name="apellido_paternoact3" value="<?php echo $apellido_paternoact3 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Materno</label>
                    <input class="form-control" type="text" name="apellido_maternoact3" value="<?php echo $apellido_maternoact3 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Fecha de Nacimiento</label>
                    <input class="form-control" type="date" name="fecha_nacimientoact3" value="<?php echo $fecha_nacimientoact3 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcact3" value="<?php echo $rfcact3 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave Única de Registro de Población</label>
                    <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpact3" value="<?php echo $curpact3 ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Clave pais nacionalidad</label>
                    <select class="form-control pais_act3_3" name="pais_nacionalidadact3">
                        <?php if($pais_nacionalidadact3!=""){
                          echo '<option value="'.$pais_nacionalidadact3.'">'.$pais_nacionalidadact3t.'</option>'; 
                        } ?>
                      </select>
                  </div>
                </div>
                <hr class="subsubtitle">
                <h5>Datos de la persona moral que se constituye</h5>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Tipo de persona moral</label>
                    <select class="form-control" name="tipo_persona_moralact3" id="tipo_persona_moralact3" onchange="tipo_persona_moralact_opt3()">
                      <option value="8" <?php if($tipo_persona_moralact3==8) echo 'selected' ?>>Sociedad Cooperativa Limitada (S. C. L.)</option>
                      <option value="9" <?php if($tipo_persona_moralact3==9) echo 'selected' ?>>Sociedad Cooperativa Suplementada (S. C. S.)</option>
                      <option value="17" <?php if($tipo_persona_moralact3==17) echo 'selected' ?>>Fondos de Inversión</option>
                      <option value="99" <?php if($tipo_persona_moralact3==99) echo 'selected' ?>>Otra (especificar)</option>
                    </select>
                  </div>
                </div> 
                <div class="tipo_persona_moralacttxt3" style="display: none">  
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Tipo de persona moral cuando es "Otra"</label>
                      <input class="form-control" type="text" name="tipo_persona_moral_otraact3" maxlength="200" value="<?php echo $tipo_persona_moral_otraact3 ?>">
                    </div>
                  </div>
                </div>  
                <div class="row">  
                  <div class="col-md-12 form-group">
                    <label>Denominación o razón social de la persona moral que se constituye</label>
                    <input class="form-control" type="text" name="denominacion_razonact3" value="<?php echo $denominacion_razonact3 ?>">
                  </div>
                  <div class="col-md-12 form-group">
                    <label>Actividad económica o giro mercantil u objeto social</label>
                    <select class="form-control" name="giro_mercantilact3">
                    <?php  foreach ($actividad_econominica_morales->result() as $act){ ?>
                      <option value="<?php echo $act->clave?>" <?php if($act->clave==$giro_mercantilact3) echo 'selected' ?>><?php echo $act->giro_mercantil ?></option>
                    <?php } ?>  
                    </select>
                  </div>
                  <div class="col-md-12 form-group">
                    <label>Folio mercantil o antecedentes registrales</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="folio_mercantilact3" maxlength="200" value="<?php echo $folio_mercantilact3 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick=" btn_foliomer_ayuda()"><i class="mdi mdi-help"></i></button>
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Número total de acciones o certificados de aportación de la persona moral</label>
                    <input class="form-control" type="number" name="numero_total_accionesact3" maxlength="17"  value="<?php echo $numero_total_accionesact3 ?>">
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Clave de entidad federativa del domicilio social</label>
                    <select class="form-control" name="entidad_federativaact3">
                    <?php  foreach ($estado->result() as $est){ ?>
                      <option value="<?php echo $est->id ?>" <?php if($est->id==$entidad_federativaact3) echo 'selected' ?>><?php echo $est->estado ?></option>
                    <?php } ?>  
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label>Se cuenta con un consejo de vigilancia</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="consejo_vigilanciaact3" value="SI" <?php if($consejo_vigilanciaact3=='SI') echo 'checked' ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="consejo_vigilanciaact3" value="NO" <?php if($consejo_vigilanciaact3=='NO') echo 'checked' ?>>
                       NO
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Motivo de la constitución de la persona moral</label>
                    <select class="form-control" name="motivo_constitucionact3">
                        <option value="1" <?php if($motivo_constitucionact3==1) echo 'selected' ?>>Fusión</option>
                        <option value="2" <?php if($motivo_constitucionact3==2) echo 'selected' ?>>Escisión</option>
                        <option value="3" <?php if($motivo_constitucionact3==3) echo 'selected' ?>>Contrato privado</option>
                    </select>
                  </div>
                  <div class="col-md-8 form-group">
                    <label>Número de Instrumento Público de la fusión o escisión</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="instrumento_publicocact3" maxlength="20" value="<?php echo $instrumento_publicocact3 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NIPFE_ayuda()"><i class="mdi mdi-help"></i></button>
                    </div>
                  </div>
                </div>
              </form> 
              <h5>Datos de los accionistas o socios</h5> 
              <div class="row_accionistas_socios">
                <div class="accionistas_socios">
                </div>
              </div>
              <h5>Capital Social de constitución</h5>
              <form id="form_actividad31" class="forms-sample">    
                <div class="row">
                  <div class="col-md-4 form-group">
                      <label>Monto de Capital Fijo</label>
                      <input class="form-control" type="number" name="capital_fijoact3" minlength="4" maxlength="17" value="<?php echo $capital_fijoact3 ?>">
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Monto de Capital Variable</label>
                    <input class="form-control" type="number" name="capital_variableact3" minlength="4" maxlength="17" value="<?php echo $capital_variableact3 ?>">
                  </div>
                </div>
              </form> 

              <!-- -->
            </div>
          </div>    
        </div>
        <form id="form_anexo5">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_actividad" value="4" id="tipo_actividad4" onclick="otorgamiento_poder_select(4)" <?php if($tipo_actividad==4) echo 'checked' ?>>
                    Modificacion patrimonial por aumento de capital o por disminucion de capital
                </label>
              </div>
            </div>-->
          </div>  
        </form>
        <div class="tipo_actividadtxt4" style="display: none">
          <!--  -->
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
              <!-- -->
              <form id="form_actividad4">
                <input type="hidden" name="actividad" value="<?php echo $actividad4 ?>">
                <!-- -->
                <h5>Tipo de autoridad</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact4" id="tipo_autoridadact41" value="1" onchange="tipo_autoridad_opt(4)" <?php if($tipo_autoridadact4==1) echo 'checked' ?>>
                        Administrativo
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact4" id="tipo_autoridadact42" value="2" onchange="tipo_autoridad_opt(4)" <?php if($tipo_autoridadact4==2) echo 'checked' ?>>
                       Jurisdiccional
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_autoridadact4txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Órgano administrativo</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organoact4" maxlength="100" value="<?php echo $organoact4 ?>">
                      </div><button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoA_ayuda()"><i class="mdi mdi-help"></i></button></div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Cargo de la autoridad ante quien se realiza el acto</label>
                      <input class="form-control" type="text" name="cargoact4" maxlength="100" value="<?php echo $cargoact4 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número de Instrumento Público  u oficio emitido</label>
                      <input class="form-control" type="text" name="instrumento_publicoact4" maxlength="20" value="<?php echo $instrumento_publicoact4 ?>">
                    </div>
                  </div>  
                </div>
                <div class="tipo_autoridadact4txt2" style="display: none">
                  <div class="row">  
                    <div class="col-md-12 form-group">
                      <label>Órgano Jurisdiccional</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organojact4" maxlength="100" value="<?php echo $organojact4 ?>">
                      </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                    </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Tipo de juicio</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="tipo_juicioact4" maxlength="50" value="<?php echo $tipo_juicioact4 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_tipodeJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Materia</label>
                      <input class="form-control" type="text" name="materiaact4" maxlength="20" value="<?php echo $materiaact4 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Expediente</label>
                      <input class="form-control" type="text" name="expedienteact4" maxlength="20" value="<?php echo $expedienteact4 ?>">
                    </div>
                  </div>   
                </div>  
                <hr class="subsubtitle">
                <h5>Datos del domicilio de la oficina donde se realiza el acto</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact4" id="domicilio_oficinaact41" value="1" onchange="domicilio_oficina_opt(4)" <?php if($domicilio_oficinaact4==1) echo 'checked' ?>>
                        Domicilio Nacional
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact4" id="domicilio_oficinaact42" value="2" onchange="domicilio_oficina_opt(4)" <?php if($domicilio_oficinaact4==2) echo 'checked' ?>>
                       Domicilio Extranjero
                      </label>
                    </div>
                  </div>
                </div>
                <div class="domicilio_oficina4txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleact4" maxlength="100" value="<?php echo $calleact4 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="codigo_postalact4" id="codigo_postalact4" value="<?php echo $codigo_postalact4 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioract4" maxlength="56" value="<?php echo $numero_exterioract4 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioract4" maxlength="40" value="<?php echo $numero_interioract4 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <select onclick="autoComplete(this.id)" class="form-control" name="coloniaact4" id="coloniaact4">
                        <option value=""></option>
                        <?php if($coloniaact4!=""){
                          echo '<option value="'.$coloniaact4.'" selected>'.$coloniaact4.'</option>';      
                        } ?>  
                      </select>
                    </div>
                  </div>  
                </div>
                <div class="domicilio_oficina4txt2" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Clave del país donde se encuentra el domicilio</label>
                      <select class="form-control pais_act4_4" name="paisact4">
                        <?php if($paisact4!=""){
                          echo '<option value="'.$paisact4.'">'.$paisact4t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Estado, provincia, departamento o demarcación política similar que corresponda</label>
                      <input class="form-control" type="text" name="estado_provinciaeact4" maxlength="100" value="<?php echo $estado_provinciaeact4 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Ciudad o población</label>
                      <input class="form-control" type="text" name="ciudad_poblacioneact4" maxlength="100" value="<?php echo $ciudad_poblacioneact4 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" pattern="\d*" maxlength="5" type="text" name="codigo_postaleact4" id="codigo_postaleact4" value="<?php echo $codigo_postaleact4 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleeact4" maxlength="100" value="<?php echo $calleeact4 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <input class="form-control" type="text" name="coloniaeact4" value="<?php echo $coloniaeact4 ?>">
                      <!--<select onclick="autoComplete(this.id)" class="form-control" name="coloniaeact4" id="coloniaeact4">
                        <option value=""></option>
                        <?php if($coloniaeact4!=""){
                          echo '<option value="'.$coloniaeact4.'" selected>'.$coloniaeact4.'</option>';      
                        } ?>  
                      </select>-->
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioreact4" maxlength="56" value="<?php echo $numero_exterioreact4 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioreact4" maxlength="40" value="<?php echo $numero_interioreact4 ?>">
                    </div>
                    
                  </div>  
                </div> 
                <hr class="subsubtitle">
                <h5>Datos de la persona moral que se modifica</h5>
                <div class="row">
                  <div class="col-md-5 form-group">
                    <label>Denominación o Razón Social</label>
                    <input class="form-control" type="text" name="denominacion_razonmact4" value="<?php echo $denominacion_razonmact4 ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Fecha de Constitución</label>
                    <input class="form-control" type="date" name="fecha_constitucionmact4" value="<?php echo $fecha_constitucionmact4 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmact4" maxlength="13" value="<?php echo $rfcmact4 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave país nacionalidad</label>
                    <select class="form-control pais_act1_1" name="pais_nacionalidadmact4">
                      <?php if($pais_nacionalidadmact4!=""){
                       echo '<option value="'.$pais_nacionalidadmact4.'">'.$pais_nacionalidadmact4t.'</option>'; 
                      } ?>
                    </select>
                  </div>
                  <div class="col-md-8 form-group">
                    <label>Actividad económica o giro mercantil u objeto social</label>
                    <select class="form-control" name="giro_mercantilact4">
                    <?php foreach ($actividad_econominica_morales->result() as $item){?>
                      <option value="<?php echo $item->clave ?>" <?php if($item->clave==$giro_mercantilact4) echo 'selected' ?>><?php echo $item->giro_mercantil ?></option>
                    <?php } ?>  
                    </select>
                  </div>
                  <div class="col-md-7 form-group">
                    <label>Número total de acciones o certificados de aportación de la persona moral</label>
                    <input class="form-control" type="number" name="numero_total_accionesact4" maxlength="17"  value="<?php echo $numero_total_accionesact4 ?>">
                  </div>
                  <div class="col-md-5 form-group">
                    <label>Motivo de la modificación de la persona moral</label>
                    <select class="form-control" name="motivo_modificacionact4">
                      <option value="1" <?php if($motivo_modificacionact4==1) echo 'selected' ?>>Fusión</option>
                      <option value="2" <?php if($motivo_modificacionact4==2) echo 'selected' ?>>Escisión</option>
                      <option value="3" <?php if($motivo_modificacionact4==3) echo 'selected' ?>>Contrato privado</option>
                    </select>
                  </div>
                  <div class="col-md-7 form-group">
                    <label>Número de Instrumento Público de la fusión o escisión</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="instrumento_publicocact4" maxlength="20" value="<?php echo $instrumento_publicocact4 ?>">
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NIPFE_ayuda()"><i class="mdi mdi-help"></i></button>
                    </div>
                  </div>
                </div> 
                <hr class="subsubtitle">
                <h5>Datos de la modificación</h5>
                <div class="row">
                  <div class="col-md-8 form-group">
                    <label>Tipo de modificación patrimonial del capital fijo</label>
                    <select class="form-control" name="tipo_modificacion_capital_fijoact4">
                      <option value="1" <?php if($tipo_modificacion_capital_fijoact4==1) echo 'selected' ?>>Aumento</option>
                      <option value="2" <?php if($tipo_modificacion_capital_fijoact4==2) echo 'selected' ?>>Disminución</option>
                      <option value="3" <?php if($tipo_modificacion_capital_fijoact4==3) echo 'selected' ?>>Sin modificación</option>
                    </select>
                  </div>
                  <div class="col-md-4 form-group"></div>
                  <div class="col-md-6 form-group">
                    <label>Monto anterior del capital fijo</label>
                    <input class="form-control" type="text" minlength="4" maxlength="17" name="inicial_capital_fijoact4" value="<?php echo $inicial_capital_fijoact4 ?>">
                  </div>
                  <div class="col-md-6 form-group"></div>
                  <div class="col-md-6 form-group">
                    <label>Monto final del capital fijo</label>
                    <input class="form-control" type="text" minlength="4" maxlength="17" name="final_capital_fijoact4" value="<?php echo $final_capital_fijoact4 ?>">
                  </div>
                  <div class="col-md-8 form-group">
                    <label>Tipo de modificación patrimonial del capital variable</label>
                    <select class="form-control" name="tipo_modificacion_capital_variableact4">
                      <option value="1" <?php if($tipo_modificacion_capital_variableact4==1) echo 'selected' ?>>Aumento</option>
                      <option value="2" <?php if($tipo_modificacion_capital_variableact4==2) echo 'selected' ?>>Disminución</option>
                      <option value="3" <?php if($tipo_modificacion_capital_variableact4==3) echo 'selected' ?>>Sin modificación</option>
                    </select>
                  </div>
                  <div class="col-md-6 form-group">
                    <label>Monto anterior del capital variable</label>
                    <input class="form-control" type="text" minlength="4" maxlength="17" name="inicial_capital_variableact4" value="<?php echo $inicial_capital_variableact4 ?>">
                  </div>
                  <div class="col-md-6 form-group"></div>
                  <div class="col-md-6 form-group">
                    <label>Monto final del capital variable</label>
                    <input class="form-control" type="number" minlength="4" maxlength="17" name="final_capital_variableact4" id="final_capital_variablemo" value="<?php echo $final_capital_variableact4 ?>">
                  </div>
                </div>
                <!-- -->
              </form>  
              <hr class="subsubtitle">
              <h5>Datos de los accionistas vigentes</h5>
              <div class="row_accionistas_vigentes">
                <div class="accionistas_vigentes">
                </div>
              </div>
              <!-- -->
            </div>
          </div>   
        </div>
        <form id="form_anexo6">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_actividad" value="5" id="tipo_actividad5" onclick="otorgamiento_poder_select(5)" <?php if($tipo_actividad==5) echo 'checked' ?>>
                    Otorgamiento de contratos de mutuo o crédito con o sin garantía
                </label>
              </div>
            </div>-->
          </div> 
        </form>
        <div class="tipo_actividadtxt5" style="display: none">
          <!--  -->
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
              <!-- -->
              <form id="form_actividad5">
                <input type="hidden" name="actividad" value="<?php echo $actividad5 ?>">
              <!-- -->   
                <h5>Tipo de autoridad</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact5" id="tipo_autoridadact51" value="1" onchange="tipo_autoridad_opt(5)" <?php if($tipo_autoridadact5==1) echo 'checked' ?>>
                        Administrativo
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_autoridadact5" id="tipo_autoridadact52" value="2" onchange="tipo_autoridad_opt(5)" <?php if($tipo_autoridadact5==2) echo 'checked' ?>>
                       Jurisdiccional
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_autoridadact5txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Órgano administrativo</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organoact5" maxlength="100" value="<?php echo $organoact5 ?>">
                      </div><button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoA_ayuda()"><i class="mdi mdi-help"></i></button></div>
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Cargo de la autoridad ante quien se realiza el acto</label>
                      <input class="form-control" type="text" name="cargoact5" maxlength="100" value="<?php echo $cargoact5 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número de Instrumento Público  u oficio emitido</label>
                      <input class="form-control" type="text" name="instrumento_publicoact5" maxlength="20" value="<?php echo $instrumento_publicoact5 ?>">
                    </div>
                  </div>  
                </div>
                <div class="tipo_autoridadact5txt2" style="display: none">
                  <div class="row">  
                    <div class="col-md-12 form-group">
                      <label>Órgano Jurisdiccional</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="organojact5" maxlength="100" value="<?php echo $organojact5 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                    </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Tipo de juicio</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="tipo_juicioact5" maxlength="50" value="<?php echo $tipo_juicioact5 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_tipodeJ_ayuda()"><i class="mdi mdi-help"></i></button>              
                      </div>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Materia</label>
                      <input class="form-control" type="text" name="materiaact5" maxlength="20" value="<?php echo $materiaact5 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Expediente</label>
                      <input class="form-control" type="text" name="expedienteact5" maxlength="20" value="<?php echo $expedienteact5 ?>">
                    </div>
                  </div>   
                </div>
                <!-- -->    
                <h5>Datos del domicilio de la oficina donde se realiza el acto</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact5" id="domicilio_oficinaact51" value="1" onchange="domicilio_oficina_opt(5)" <?php if($domicilio_oficinaact5==1) echo 'checked' ?>>
                        Domicilio Nacional
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="domicilio_oficinaact5" id="domicilio_oficinaact52" value="2" onchange="domicilio_oficina_opt(5)" <?php if($domicilio_oficinaact5==2) echo 'checked' ?>>
                       Domicilio Extranjero
                      </label>
                    </div>
                  </div>
                </div>
                <div class="domicilio_oficina5txt1" style="display: none">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleact5" maxlength="100" value="<?php echo $calleact5 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="codigo_postalact5" id="codigo_postalact5" value="<?php echo $codigo_postalact5 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <select onclick="autoComplete(this.id)" class="form-control" name="coloniaact5" id="coloniaact5">
                        <option value=""></option>
                        <?php if($coloniaact5!=""){
                          echo '<option value="'.$coloniaact5.'" selected>'.$coloniaact5.'</option>';      
                        } ?>  
                      </select>
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioract5" maxlength="56" value="<?php echo $numero_exterioract5 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioract5" maxlength="40" value="<?php echo $numero_interioract5 ?>">
                    </div>
                    
                  </div>  
                </div>
                <div class="domicilio_oficina5txt2" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Clave del país donde se encuentra el domicilio</label>
                      <select class="form-control pais_act5_5" name="paisact5">
                        <?php if($paisact5!=""){
                          echo '<option value="'.$paisact5.'">'.$paisact5t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-8 form-group">
                      <label>Estado, provincia, departamento o demarcación política similar que corresponda</label>
                      <input class="form-control" type="text" name="estado_provinciaeact5" maxlength="100" value="<?php echo $estado_provinciaeact5 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Ciudad o población</label>
                      <input class="form-control" type="text" name="ciudad_poblacioneact5" maxlength="100" value="<?php echo $ciudad_poblacioneact5 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Código Postal</label>
                      <input class="form-control" pattern="\d*" maxlength="5" type="text" name="codigo_postaleact5" id="codigo_postaleact5" value="<?php echo $codigo_postaleact5 ?>">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Calle, avenida o vía</label>
                      <input class="form-control" type="text" name="calleeact5" maxlength="100" value="<?php echo $calleeact5 ?>">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Colonia</label>
                      <input class="form-control" type="text" name="coloniaeact5" value="<?php echo $coloniaeact5 ?>">
                      <!--<select onclick="autoComplete(this.id)" class="form-control" name="coloniaeact5" id="coloniaeact5">
                        <option value=""></option>
                        <?php if($coloniaeact5!=""){
                          echo '<option value="'.$coloniaeact5.'" selected>'.$coloniaeact5.'</option>';      
                        } ?>  
                      </select>-->
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número exterior</label>
                      <input class="form-control" type="text" name="numero_exterioreact5" maxlength="56" value="<?php echo $numero_exterioreact5 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Número interior</label>
                      <input class="form-control" type="text" name="numero_interioreact5" maxlength="40" value="<?php echo $numero_interioreact5 ?>">
                    </div>
                    
                  </div>  
                </div>  
                <div class="row">
                  <div class="col-md-6 form-group">
                    <label>Tipo de Otorgamiento</label>
                    <select class="form-control" name="tipo_otorgamientoact5">
                      <option value="1" <?php if($tipo_otorgamientoact5==1) echo 'selected' ?>>Otorgamiento de Mutuo, Préstamo o Crédito sin Garantía</option>
                      <option value="2" <?php if($tipo_otorgamientoact5==2) echo 'selected' ?>>Otorgamiento de Mutuo, Préstamo o Crédito con Garantía</option>
                    </select>
                  </div>
                </div>  
                <hr class="subsubtitle">
                <h5>Datos de la persona que solicita el acto u operación</h5>     
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Nombre(s)</label>
                    <input class="form-control" type="text" name="nombreact5" value="<?php echo $nombreact5 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Paterno</label>
                    <input class="form-control" type="text" name="apellido_paternoact5" value="<?php echo $apellido_paternoact5 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Materno</label>
                    <input class="form-control" type="text" name="apellido_maternoact5" value="<?php echo $apellido_maternoact5 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Fecha de Nacimiento</label>
                    <input class="form-control" type="date" name="fecha_nacimientoact5" value="<?php echo $fecha_nacimientoact5 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcact5" value="<?php echo $rfcact5 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave Única de Registro de Población</label>
                    <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpact5" value="<?php echo $curpact5 ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Clave pais nacionalidad</label>
                    <select class="form-control pais_act2_2" name="pais_nacionalidadact5">
                        <?php if($pais_nacionalidadact5!=""){
                          echo '<option value="'.$pais_nacionalidadact5.'">'.$pais_nacionalidadact5t.'</option>'; 
                        } ?>
                      </select>
                  </div>
                </div>
                <hr class="subsubtitle">
                <h5>Datos de identificación del otorgante del mutuo o crédito o acreedor</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact5" id="tipo_persona_5_1" value="1" onclick="tipo_persona_opt(5)" <?php if($tipo_personaact5==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact5" id="tipo_persona_5_2" value="2" onclick="tipo_persona_opt(5)" <?php if($tipo_personaact5==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact5" id="tipo_persona_5_3" value="3" onclick="tipo_persona_opt(5)" <?php if($tipo_personaact5==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_persona_txt_5_1" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombreact25" value="<?php echo $nombreact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternact25" value="<?php echo $apellido_paternact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternact25" value="<?php echo $apellido_maternact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientact25" value="<?php echo $fecha_nacimientact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccact25" maxlength="13" value="<?php echo $rfccact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpcact25" maxlength="18" value="<?php echo $curpcact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act5_5" name="pais_nacionalidadact25">
                        <?php if($pais_nacionalidadact25!=""){
                         echo '<option value="'.$pais_nacionalidadact25.'">'.$pais_nacionalidadact25t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>  
                <div class="tipo_persona_txt_5_2" style="display: none">
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmact5" value="<?php echo $denominacion_razonmact5 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmact5" value="<?php echo $fecha_constitucionmact5 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmact5" maxlength="13" value="<?php echo $rfcmact5 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act1_1" name="pais_nacionalidadmact5">
                        <?php if($pais_nacionalidadmact5!=""){
                         echo '<option value="'.$pais_nacionalidadmact5.'">'.$pais_nacionalidadmact5t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>  
                <div class="tipo_persona_txt_5_3" style="display: none">
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfact5" value="<?php echo $denominacion_razonfact5 ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfact5" maxlength="13" value="<?php echo $rfcfact5 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofact5" maxlength="40" value="<?php echo $identificador_fideicomisofact5 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                </div> 
                <hr class="subsubtitle">
                <h5>Datos de identificación del solicitante del mutuo o crédito o deudor</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact25" id="tipo_persona_52_1" value="1" onclick="tipo_persona_opt(52)" <?php if($tipo_personaact25==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact25" id="tipo_persona_52_2" value="2" onclick="tipo_persona_opt(52)" <?php if($tipo_personaact25==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact25" id="tipo_persona_52_3" value="3" onclick="tipo_persona_opt(52)" <?php if($tipo_personaact25==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_persona_txt_52_1" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombreact35" value="<?php echo $nombreact35 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternact35" value="<?php echo $apellido_paternact35 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternact35" value="<?php echo $apellido_maternact35 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientact35" value="<?php echo $fecha_nacimientact35 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccact35" maxlength="13" value="<?php echo $rfccact35 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpcact35" maxlength="18" value="<?php echo $curpcact35 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act5_5" name="pais_nacionalidadcact35">
                        <?php if($pais_nacionalidadcact35!=""){
                         echo '<option value="'.$pais_nacionalidadcact35.'">'.$pais_nacionalidadcact35t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div>
                  <hr>  
                </div>  
                <div class="tipo_persona_txt_52_2" style="display: none">
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmact25" value="<?php echo $denominacion_razonmact25 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmact25" value="<?php echo $fecha_constitucionmact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmact35" maxlength="13" value="<?php echo $rfcmact35 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act1_1" name="pais_nacionalidadmact35">
                        <?php if($pais_nacionalidadmact35!=""){
                         echo '<option value="'.$pais_nacionalidadmact35.'">'.$pais_nacionalidadmact35t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <hr> 
                </div>  
                <div class="tipo_persona_txt_52_3" style="display: none">
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfact25" value="<?php echo $denominacion_razonfact25 ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfact25" maxlength="13" value="<?php echo $rfcfact25 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input class="form-control" type="text" name="identificador_fideicomisofact25" maxlength="40" value="<?php echo $identificador_fideicomisofact25 ?>">
                      </div>
                      <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                      </div>
                    </div>
                  </div> 
                </div> 
                <hr class="subsubtitle">
                <h5>Datos de la garantía</h5>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Tipo de garantía</label>
                    <select class="form-control" name="tipo_garantiaact5" id="tipo_garantiaact5">
                      <option value="1" <?php if($tipo_garantiaact5==1) echo 'selected' ?>>Sin garantía</option>
                      <option value="2" <?php if($tipo_garantiaact5==2) echo 'selected' ?>>Inmueble</option>
                      <option value="3" <?php if($tipo_garantiaact5==3) echo 'selected' ?>>Vehículo terrestre</option>
                      <option value="4" <?php if($tipo_garantiaact5==4) echo 'selected' ?>>Vehículo aéreo</option>
                      <option value="5" <?php if($tipo_garantiaact5==5) echo 'selected' ?>>Vehículo marítimo</option>
                      <option value="6" <?php if($tipo_garantiaact5==6) echo 'selected' ?>>Priedras Preciosas</option>
                      <option value="7" <?php if($tipo_garantiaact5==7) echo 'selected' ?>>Metales Preciosos</option>
                      <option value="8" <?php if($tipo_garantiaact5==8) echo 'selected' ?>>Joyas o relojes</option>
                      <option value="9" <?php if($tipo_garantiaact5==9) echo 'selected' ?>>Obras de arte o antigüedades</option>
                      <option value="10" <?php if($tipo_garantiaact5==11) echo 'selected' ?>>Acciones o partes sociales</option>
                      <option value="11" <?php if($tipo_garantiaact5==11) echo 'selected' ?>>Derechos fiduciarios</option>
                      <option value="12" <?php if($tipo_garantiaact5==12) echo 'selected' ?>>Derechos de crédito</option>
                      <option value="15" <?php if($tipo_garantiaact5==15) echo 'selected' ?>>Garantía Quirografaria</option>
                      <option value="99" <?php if($tipo_garantiaact5==99) echo 'selected' ?>>Otro (Especificar)</option>
                    </select>
                  </div>
                </div>
                
                <div id="cont_datos_garantia">
                  <hr class="subsubtitle">
                  <h5>Datos del bien objeto de la garantía</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_bien_mutuoact5" id="datos_bien_mutuo15" value="1" onclick="datos_bien_mutuo_select(5)" <?php if($datos_bien_mutuoact5==1) echo 'checked' ?>>
                          Inmueble
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="datos_bien_mutuoact5" id="datos_bien_mutuo25" value="2" onclick="datos_bien_mutuo_select(5)" <?php if($datos_bien_mutuoact5==2) echo 'checked' ?>>
                          Otro bien
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="datos_inmuebletxt5" style="display: none;">
                    <div class="row">  
                      <div class="col-md-4 form-group">
                        <label>Tipo de inmueble</label>
                        <select class="form-control" name="tipo_inmuebleact5">
                        <?php foreach ($inmueble->result() as $item){ ?>
                          <option value="<?php echo $item->clave ?>" <?php if($item->clave==$tipo_inmuebleact5) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } ?>  
                        </select>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Valor de referencia del inmueble</label>
                        <input class="form-control" type="number" name="valor_referenciaact5" maxlength="17" value="<?php echo $valor_referenciaact5 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Código Postal de la ubicación del inmueble</label>
                        <input class="form-control" type="text" name="codigo_postalact25" maxlength="5" value="<?php echo $codigo_postalact25 ?>">
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Folio real del inmueble o antecedentes registrales</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="folio_realact5" value="<?php echo $folio_realact5 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_folio_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div>  
                  </div>
                  <div class="datos_otrotxt5" style="display: none;">
                    <div class="row">
                      <div class="col-md-12 form-group">
                        <label>Descripción cuando Tipo de Garantia es "Otro"</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="descripcion_garantiaact5" value="<?php echo $descripcion_garantiaact5 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_DCTGO_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div>  
                  </div>  
                  <hr class="subsubtitle">
                  <h5>Datos de tipo de persona del garante</h5>
                  <div class="row">
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personapact53" id="tipo_persona_53_1" value="1" onclick="tipo_persona_opt(53)" <?php if($tipo_personapact53==1) echo 'checked' ?>>
                          Persona Fisica
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personapact53" id="tipo_persona_53_2" value="2" onclick="tipo_persona_opt(53)" <?php if($tipo_personapact53==2) echo 'checked' ?>>
                          Persona Moral
                        </label>
                      </div>
                    </div>
                    <div class="col-md-2 form-group"> 
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="tipo_personapact53" id="tipo_persona_53_3" value="3" onclick="tipo_persona_opt(53)" <?php if($tipo_personapact53==3) echo 'checked' ?>>
                          Fideicomiso
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="tipo_persona_txt_53_1" style="display: none">
                      <div class="row">
                        <div class="col-md-4 form-group">
                          <label>Nombre(s)</label>
                          <input class="form-control" type="text" name="nombrepact53" value="<?php echo $nombrepact53 ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido Paterno</label>
                          <input class="form-control" type="text" name="apellido_paternapact53" value="<?php echo $apellido_paternapact53 ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido Materno</label>
                          <input class="form-control" type="text" name="apellido_maternapact53" value="<?php echo $apellido_maternapact53 ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                          <input class="form-control" type="date" name="fecha_nacimientapact53" value="<?php echo $fecha_nacimientapact53 ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Registro Federal de Contribuyentes (RFC)</label>
                          <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcpact53" maxlength="13" value="<?php echo $rfcpact53 ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Clave Única de Registro de Población (CURP)</label>
                          <input class="form-control" type="text" onblur="ValidCurp(this)" name="curppact53" maxlength="18" value="<?php echo $curppact53 ?>">
                        </div>
                      </div>
                      <hr>  
                  </div>  
                  <div class="tipo_persona_txt_53_2" style="display: none">
                    <div class="row">
                      <div class="col-md-5 form-group">
                        <label>Denominación o Razón Social</label>
                        <input class="form-control" type="text" name="denominacion_razonmpact53" value="<?php echo $denominacion_razonmpact53 ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha de Constitución</label>
                        <input class="form-control" type="date" name="fecha_constitucionmpact53" value="<?php echo $fecha_constitucionmpact53 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Registro Federal de Contribuyentes (RFC)</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmact53" maxlength="13" value="<?php echo $rfcmact53 ?>">
                      </div>
                    </div> 
                    <hr> 
                  </div>  
                  <div class="tipo_persona_txt_53_3" style="display: none">
                    <div class="row">
                      <div class="col-md-7 form-group">
                        <label>Denominación o Razón Social del Fiduciario</label>
                        <input class="form-control" type="text" name="denominacion_razonfpact53" value="<?php echo $denominacion_razonfpact53 ?>">
                      </div>
                      <div class="col-md-5 form-group">
                        <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfpact53" maxlength="13" value="<?php echo $rfcfpact53 ?>">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Número, referencia o identificador del fideicomiso</label>
                        <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input class="form-control" type="text" name="identificador_fideicomisofpact53" maxlength="40" value="<?php echo $identificador_fideicomisofpact53 ?>">
                        </div>
                        <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_NRIF_ayuda()"><i class="mdi mdi-help"></i></button>
                        </div>
                      </div>
                    </div> 
                    <hr> 
                  </div> 
                </div>

                <hr class="subsubtitle">
                <h5>Datos de la forma de otorgamiento del crédito</h5>
                 <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Tipo de moneda o divisa de la operación o acto</label>
                    <select class="form-control" name="monedaact5">
                    <?php /* foreach ($tipo_moneda_get as $item){ 
                      if($item->id==1){ ?>
                      <option value="<?php echo $item->id ?>"><?php echo $item->moneda ?></option>
                    <?php } } */ ?>  
                    <option value="0" <?php if($monedaact5=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                    <option value="1" <?php if($monedaact5=="1") echo "selected"; ?>>Dólar Americano</option>
                    <option value="2" <?php if($monedaact5=="2") echo "selected"; ?>>Euro</option>
                    <option value="3" <?php if($monedaact5=="3") echo "selected"; ?>>Libra Esterlina</option>
                    <option value="4" <?php if($monedaact5=="4") echo "selected"; ?>>Dólar canadiense</option>
                    <option value="5" <?php if($monedaact5=="5") echo "selected"; ?>>Yuan Chino</option>
                    <option value="6" <?php if($monedaact5=="6") echo "selected"; ?>>Centenario</option>
                    <option value="7" <?php if($monedaact5=="7") echo "selected"; ?>>Onza de Plata</option>
                    <option value="8" <?php if($monedaact5=="8") echo "selected"; ?>>Onza de Oro</option>
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Monto de la operación o acto</label>
                    <input class="form-control" type="number" name="monto_operacionact5" maxlength="17" value="<?php echo $monto_operacionact5 ?>">
                  </div>
                </div>
              <!-- -->
              </form>
            </div>
          </div>   
        </div>
        <form id="form_anexo7">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_actividad" value="6" id="tipo_actividad6" onclick="otorgamiento_poder_select(6)" <?php if($tipo_actividad==6) echo 'checked' ?>>
                    Realización de Avalúos
                </label>
              </div>
            </div>-->
          </div> 
        </form>
        <div class="tipo_actividadtxt6" style="display: none">
          <!--  -->
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
              <!-- -->
              <form id="form_actividad6">
                <input type="hidden" name="actividad" value="<?php echo $actividad6 ?>">
                <div class="row">
                  <div class="col-md-12 form-group">
                    <label>Órgano administrativo</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="organoact6" maxlength="100" value="<?php echo $organoact6 ?>">
                    </div><button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_organoA_ayuda()"><i class="mdi mdi-help"></i></button></div>
                  </div>
                  <div class="col-md-12 form-group">
                    <label>Cargo de la autoridad ante quien se realiza el acto</label>
                    <input class="form-control" type="text" name="cargoact6" maxlength="100" value="<?php echo $cargoact6 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Número de Expediente u Oficio</label>
                    <input class="form-control" type="text" name="expediente_oficioact6" maxlength="20" value="<?php echo $expediente_oficioact6 ?>">
                  </div>
                </div>  
                <hr class="subsubtitle">
                <h5>Datos de la persona que solicita el acto u operación</h5>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <label>Nombre(s)</label>
                    <input class="form-control" type="text" name="nombreact6" value="<?php echo $nombreact6 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Paterno</label>
                    <input class="form-control" type="text" name="apellido_paternoact6" value="<?php echo $apellido_paternoact6 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Apellido Materno</label>
                    <input class="form-control" type="text" name="apellido_maternoact6" value="<?php echo $apellido_maternoact6 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Fecha de Nacimiento</label>
                    <input class="form-control" type="date" name="fecha_nacimientoact6" value="<?php echo $fecha_nacimientoact6 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Registro Federal de Contribuyentes (RFC)</label>
                    <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcact6" value="<?php echo $rfcact6 ?>">
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Clave Única de Registro de Población</label>
                    <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpact6" value="<?php echo $curpact6 ?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Clave pais nacionalidad</label>
                    <select class="form-control pais_act6_6" name="pais_nacionalidadact6">
                        <?php if($pais_nacionalidadact6!=""){
                          echo '<option value="'.$pais_nacionalidadact6.'">'.$pais_nacionalidadact6t.'</option>'; 
                        } ?>
                      </select>
                  </div>
                </div>
                <div class="row">  
                  <div class="col-md-4 form-group">
                    <label>Tipo de bien objeto de avalúo</label>
                    <select class="form-control" name="tipo_bienact6">
                      <option value="1" <?php if($tipo_bienact6==1) echo 'selected' ?>>Inmueble</option>
                      <option value="2" <?php if($tipo_bienact6==2) echo 'selected' ?>>Vehículo terrestre</option>
                      <option value="3" <?php if($tipo_bienact6==3) echo 'selected' ?>>Vehículo aéreo</option>
                      <option value="4" <?php if($tipo_bienact6==4) echo 'selected' ?>>Vehículo marítimo</option>
                      <option value="5" <?php if($tipo_bienact6==5) echo 'selected' ?>>Priedras Preciosas</option>
                      <option value="6" <?php if($tipo_bienact6==6) echo 'selected' ?>>Metales Preciosos</option>
                      <option value="7" <?php if($tipo_bienact6==7) echo 'selected' ?>>Joyas o relojes</option>
                      <option value="8" <?php if($tipo_bienact6==8) echo 'selected' ?>>Obras de arte o antigüedades</option>
                      <option value="99" <?php if($tipo_bienact6==99) echo 'selected' ?>>Otro (Especificar)</option>
                    </select>
                  </div>
                  <div class="col-md-12 form-group">
                    <label>Descripción del bien</label>
                    <textarea class="form-control" type="text" name="descripcionact6" ><?php echo $descripcionact6 ?></textarea>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Valor avalúo en Moneda Nacional</label>
                    <input class="form-control" type="number" name="valor_avaluoact6" id="valor_avaluorea" minlength="4" maxlength="17" value="<?php echo $valor_avaluoact6 ?>">
                  </div>
                </div> 
                <hr class="subsubtitle">
                <h5>Datos del propietario de los bienes objeto del avalúo</h5>
                <div class="row">
                  <div class="col-md-6 form-group">
                    <label>El propietario del bien es el que solicita la formalización del instrumento público </label>
                    <div class="row">
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="propietario_solicitaact6" value="SI" <?php if($propietario_solicitaact6=='SI') echo 'checked' ?>>
                            SI 
                          </label>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="propietario_solicitaact6" value="NO" <?php if($propietario_solicitaact6=='NO') echo 'checked' ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
                <h5>Datos del o los propietarios de los bienes objeto del avalúo</h5>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact6" id="tipo_persona_6_1" value="1" onclick="tipo_persona_opt(6)" <?php if($tipo_personaact6==1) echo 'checked' ?>>
                        Persona Fisica
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact6" id="tipo_persona_6_2" value="2" onclick="tipo_persona_opt(6)" <?php if($tipo_personaact6==2) echo 'checked' ?>>
                        Persona Moral
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2 form-group"> 
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="tipo_personaact6" id="tipo_persona_6_3" value="3" onclick="tipo_persona_opt(6)" <?php if($tipo_personaact6==3) echo 'checked' ?>>
                        Fideicomiso
                      </label>
                    </div>
                  </div>
                </div>
                <div class="tipo_persona_txt_6_1" style="display: none">
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <label>Nombre(s)</label>
                      <input class="form-control" type="text" name="nombrepact6" value="<?php echo $nombrepact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Paterno</label>
                      <input class="form-control" type="text" name="apellido_paternpact6" value="<?php echo $apellido_paternpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Apellido Materno</label>
                      <input class="form-control" type="text" name="apellido_maternpact6" value="<?php echo $apellido_maternpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>
                      <input class="form-control" type="date" name="fecha_nacimientpact6" value="<?php echo $fecha_nacimientpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfccpact6" maxlength="13" value="<?php echo $rfccpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave Única de Registro de Población (CURP)</label>
                      <input class="form-control" type="text" onblur="ValidCurp(this)" name="curpcpact6" maxlength="18" value="<?php echo $curpcpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act6_6" name="pais_nacionalidadpact6">
                        <?php if($pais_nacionalidadpact6!=""){
                         echo '<option value="'.$pais_nacionalidadpact6.'">'.$pais_nacionalidadpact6t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div>
                </div>  
                <div class="tipo_persona_txt_6_2" style="display: none">
                  <div class="row">
                    <div class="col-md-5 form-group">
                      <label>Denominación o Razón Social</label>
                      <input class="form-control" type="text" name="denominacion_razonmpact6" value="<?php echo $denominacion_razonmpact6 ?>">
                    </div>
                    <div class="col-md-3 form-group">
                      <label>Fecha de Constitución</label>
                      <input class="form-control" type="date" name="fecha_constitucionmpact6" value="<?php echo $fecha_constitucionmpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Registro Federal de Contribuyentes (RFC)</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcmpact6" maxlength="13" value="<?php echo $rfcmpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Clave país nacionalidad</label>
                      <select class="form-control pais_act6_6" name="pais_nacionalidadmpact6">
                        <?php if($pais_nacionalidadmpact6!=""){
                         echo '<option value="'.$pais_nacionalidadmpact6.'">'.$pais_nacionalidadmpact6t.'</option>'; 
                        } ?>
                      </select>
                    </div>
                  </div> 
                </div>  
                <div class="tipo_persona_txt_6_3" style="display: none">
                  <div class="row">
                    <div class="col-md-7 form-group">
                      <label>Denominación o Razón Social del Fiduciario</label>
                      <input class="form-control" type="text" name="denominacion_razonfpact6" value="<?php echo $denominacion_razonfpact6 ?>">
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>
                      <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfcfpact6" maxlength="13" value="<?php echo $rfcfpact6 ?>">
                    </div>
                    <div class="col-md-4 form-group">
                      <label>Número, referencia o identificador del fideicomiso</label>
                      <div class="d-flex mb-2">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input class="form-control" type="text" name="identificador_fideicomisofpact6" maxlength="40" value="<?php echo $identificador_fideicomisofpact6 ?>">
                        </div>
                      </div> 
                    </div> 
                  </div>
                </div>
              </form>  
              <!-- -->
            </div>
          </div>   
        </div>
        <!-- -->

        <div class="modal-footer">
          <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
          <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
        </div>
        <!-- -->
    	</div>
    </div>
	</div>
</div>
<!-- Operaciones -->
<!--------------Modal-------------->
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 carácteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- / -->
<!------ Modificatorio ------->
<!--- --->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_organoJ_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Organo jurisdiccional</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 100 caracteres.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos, diagonal, guión medio y guión bajo.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_tipodeJ_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Tipo de juicio</h5>
       <span style="text-align : justify; font-size: 12px">
          <li>Campo alfanumérico.</li>
          <li>Longitud mínima 1 caracter, máxima 50 caracteres.</li>
          <li>Acepta letras, números, espacios, coma, punto, dos puntos, diagonal, guión medio y guión bajo.</li>
          <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_materia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Materia</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 20 caracteres.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos, diagonal, guión medio y guión bajo.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_expediente_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Expediente</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 20 caracteres.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos, diagonal, guión medio y guión bajo.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_TARCSO_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Tipo de acto cuando es otro</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 50 caracteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_organoA_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Organo Administrativo</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 100 caracteres.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos, diagonal, guión medio y guión bajo.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_CAAQSRA_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Cargo de la autoridad ante quien se realiza el acto</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 100 caracteres.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos, diagonal, guión medio y guión bajo.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_NIP_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de instrumento Público</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 20 caracteres.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos, diagonal, guión medio y guión bajo.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliomer_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio mercantil o antecedentes registrales</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 200 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda,guión bajo, y guión medio.</li>
           <li>Capturar el folio mercantil o de inscripción en el registro que corresponda, en caso de no contar con uno por el marco juridico local, se deberán establece los datos de los antecedentes registrales según corresponda.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_NRIF_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número, referencia o identificador fideicomiso</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 40 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda, coma, @, &, guión bajo, y guión medio.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_folio_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio real del inmueble o antecedentes catastrales</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>No dejar espacios en blanco, separar caracteres con guión medio o bajo.</li>
           <li>Capturar el folio real físico o electrónico del inmueble, en caso de no contar con uno, se deben capturar los datos de los antecedentes registrales, si no se cuenta con lo anterior, capturar XXXX.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_DCTGO_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Descripción cuando tipo de garantia es otro</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 200 caracteres.</li>
           <li>Acepta letras, numeros,, escpacio, coma, punto, dos puntos, diagonal, apostrofe, signo de pesos y guion medio.</li>
           <li>No acepta parentesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_NIPFE_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de Instrumento Público de la fusión o escisión</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo alfanumérico.</li>
           <li>Longitud mínima 1 caracter, máxima 20 caracteres.</li>
           <li>Acepta letras, numeros, ceros a la izquierda,guión bajo, y guión medio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- < / >--->

<!--modals modificatorio de ayuda-->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>