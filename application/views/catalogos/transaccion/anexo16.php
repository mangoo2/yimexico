<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h4>Anexo 16 - Activos virtuales.</h4>  
            <h3>Registro de transacción</h3>
          </div>
        </div>
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <input type="hidden" id="aux_pga" value="<?php echo $aux_pga; ?>">
        <input type="hidden" id="id_ax1" value="<?php echo $id; ?>">
        <form id="form_anexo16" class="forms-sample">
          <input type="hidden" name="id" value="<?php echo $id; ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Fecha de operación o acto </label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
              <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input type="text" name="referencia" id="referencia" class="form-control" placeholder="Ingrese número de referencia" required="" value="<?php echo $referencia ?>">
              </div>
              <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()"><i class="mdi mdi-help"></i></button>
              </div>
            </div>
            <div class="col-md-3 form-group">
              <label>Monto de factura</label>
              <input type="text" id="monto_opera" class="form-control Nformat" placeholder="Ingrese el monto total de la operación ($)" name="monto_opera" required="" value="<?php echo $monto_opera ?>">
            </div>
            <div class="col-md-3 form-group">
            </div>
            <div class="col-md-3 form-group">
              <label>Año Reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes Reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
              <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input type="text" name="num_factura" id="num_factura" class="form-control" placeholder="Ingrese número de factura" required="" value="<?php echo $num_factura ?>">
              </div>
              <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()"><i class="mdi mdi-help"></i></button>
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <div class="row">
            <?php if($aviso==1){ ?>
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>

            <div class="col-md-2 form-group">
            </div>

            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div>
            </div>
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="idanexo16" id="idanexo16" class="form-control" value="<?php echo $idanexo16 ?>">
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div>
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="70"><?php echo $descrip_modifica ?></textarea>
            </div>
            <?php }?>
          </div>
      </form>
      <div class="col-md-3 form_group">
        <label>Tipo Operación</label>
        <select class="form-control" id="tipoOperacion" name="tipoOperacion">
          <option value="0">Seleccione</option>
          <option <?php if ($tipoOperacion==1){ echo "selected"; } ?> value="1">Compra de activos virtuales</option>
          <option <?php if ($tipoOperacion==2){ echo "selected"; } ?> value="2">Venta de activos virtuales</option>
          <option <?php if ($tipoOperacion==3){ echo "selected"; } ?> value="3">Intercambio de activos virtuales</option>
          <option <?php if ($tipoOperacion==4){ echo "selected"; } ?> value="4">Transferencia de activos virtuales(Enviados)</option>
          <option <?php if ($tipoOperacion==5){ echo "selected"; } ?> value="5">Transferencia de activos virtuales(recibidos)</option>
          <option <?php if ($tipoOperacion==6){ echo "selected"; } ?> value="6">Retiro de fondos de plataforma virtual</option>
          <option <?php if ($tipoOperacion==7){ echo "selected"; } ?> value="7">Deposito de fondos en plataforma virtual</option>
        </select>
      </div>
      <div class="col-md-12">
        <br>
      </div>
      <input type="hidden" id="idLi" value="<?php echo $idliq; ?>">
      <div id="EL1" style="display: none;">
        <form id="T1">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha y hora en la que se concluyo la operación reportada</label>
              <input class="form-control dateTime" data-inputmask="'alias': 'datetime'" data-inputmask-inputformat="yyyy-mm-dd HH:MM:ss" name="fecha_hora_operacion" value="<?php echo $fecha_hora_operacion; ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Tipo de moneda de la operacion o acto</label>
              <select class="form-control" type="" name="moneda_operacion">
                <?php /* foreach ($MonedasM as $key): ?>
                  <?php if ($moneda_operacion == $key->clave){ $sel="Selected"; }else{ $sel=""; } ?> 
              <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->moneda; ?></option>    
                <?php endforeach */ ?>
                <option value="0" <?php if($moneda_operacion=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                <option value="1" <?php if($moneda_operacion=="1") echo "selected"; ?>>Dólar Americano</option>
                <option value="2" <?php if($moneda_operacion=="2") echo "selected"; ?>>Euro</option>
                <option value="3" <?php if($moneda_operacion=="3") echo "selected"; ?>>Libra Esterlina</option>
                <option value="4" <?php if($moneda_operacion=="4") echo "selected"; ?>>Dólar canadiense</option>
                <option value="5" <?php if($moneda_operacion=="5") echo "selected"; ?>>Yuan Chino</option>
                <option value="6" <?php if($moneda_operacion=="6") echo "selected"; ?>>Centenario</option>
                <option value="7" <?php if($moneda_operacion=="7") echo "selected"; ?>>Onza de Plata</option>
                <option value="8" <?php if($moneda_operacion=="8") echo "selected"; ?>>Onza de Oro</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Monto de la operación o acto</label>
              <input class="form-control Nformat" type="text" name="monto_operacion" value="<?php echo $monto_operacion; ?>">
            </div>
            <div class="col-md-12">
              <hr class="subsubtitle">
              Datos del activo virtual
            </div>
            <div class="col-md-4 form-group">
              <label>Activo virtual operado</label>
              <select class="form-control" id="SAV1" name="activo_virtual_operado">
                <?php foreach ($ActivosV as $key): ?>
                  <?php if ($activo_virtual_operado == $key->clave){ $sel="Selected"; }else{ $sel=""; } ?>
              <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->nombre; ?></option>                  
                <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-4 form-group" id="NAC1" style="display: none;">
              <label>Nombre del activo virtual</label>
              <input class="form-control" type="text" name="descripcion_activo_virtual" value="<?php $descripcion_activo_virtual; ?>">
            </div>
            <div class="col-md-5 form-group">
              <label>Tipo de cambio a Pesos Mexicanos en el momento que se concluyo la operación</label>
              <input class="form-control Nformat" type="" name="tipo_cambio_mn" value="<?php echo $tipo_cambio_mn; ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Cantidad del activo Virtual de la operación</label>
              <input class="form-control" type="" name="cantidad_activo_virtual" value="<?php echo $cantidad_activo_virtual; ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Hash de la operación</label>
              <div class="d-flex mb-2">
              <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input class="form-control" type="" name="hash_operacion" value="<?php echo $hash_operacion; ?>">
              </div>
              <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_hashOperacion_ayuda()"><i class="mdi mdi-help"></i></button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div id="EL2" style="display: none;">
        <form id="T2">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha y hora en la que se concluyo la operación reportada</label>
              <input class="form-control dateTime" data-inputmask="'alias': 'datetime'" data-inputmask-inputformat="yyyy-mm-dd HH:MM:ss" name="fecha_hora_operacion" value="<?php echo $fecha_hora_operacion; ?>" >
            </div>
            <div class="col-md-12">
              <hr class="subsubtitle">
              Datos del activo virtual
            </div>
            <div class="col-md-4 form-group">
              <label>Activo virtual operado</label>
              <select class="form-control" id="SAV2" name="activo_virtual_operado">
                <?php foreach ($ActivosV as $key): ?>
                <?php if ($activo_virtual_operado == $key->clave){ $sel="Selected"; }else{ $sel=""; } ?>
              <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->nombre; ?></option>                  
                <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-4 form-group" id="NAC2" style="display: none;">
              <label>Nombre del activo virtual</label>
              <input class="form-control" type="" name="descripcion_activo_virtual" value="<?php echo $descripcion_activo_virtual; ?>" >
            </div>
            <div class="col-md-5 form-group">
              <label>Tipo de cambio a Pesos Mexicanos en el momento que se concluyo la operación</label>
              <input class="form-control Nformat" type="" name="tipo_cambio_mn" value="<?php echo $tipo_cambio_mn; ?>" >
            </div>
            <div class="col-md-3 form-group">
              <label>Cantidad del activo Virtual de la operación</label>
              <input class="form-control" type="" name="cantidad_activo_virtual" value="<?php echo $cantidad_activo_virtual; ?>" >
            </div>
            <div class="col-md-4 form-group">
              <label>Monto Total en Pesos Mexicanos de la operación</label>
              <input class="form-control Nformat" type="text" name="monto_operacion_mn" value="<?php echo $monto_operacion_mn; ?>" >
            </div>
            <div class="col-md-12">
              <hr class="subsubtitle">
            </div>
            <div class="col-md-4 form-group">
              <label>Activo virtual operado</label>
              <select class="form-control" id="SAV3" name="activo_virtual_operado_r">
                <?php foreach ($ActivosV as $key): ?>
               <?php if ($activo_virtual_operado == $key->clave){ $sel="Selected"; }else{ $sel=""; } ?>
              <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->nombre; ?></option>                 
                <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-4 form-group" id="NAC3" style="display: none;">
              <label>Nombre del activo virtual</label>
              <input class="form-control" type="" name="descripcion_activo_virtual_r" value="<?php echo $descripcion_activo_virtual_r; ?>" >
            </div>
            <div class="col-md-5 form-group">
              <label>Tipo de cambio a Pesos Mexicanos en el momento que se concluyo la operación</label>
              <input class="form-control Nformat" type="" name="tipo_cambio_mn_r" value="<?php echo $tipo_cambio_mn_r; ?>" >
            </div>
            <div class="col-md-3 form-group">
              <label>Cantidad del activo Virtual de la operación</label>
              <input class="form-control" type="" name="cantidad_activo_virtual_r" value="<?php echo $cantidad_activo_virtual_r; ?>" >
            </div>
            <div class="col-md-4 form-group">
              <label>Monto Total en Pesos Mexicanos de la operación</label>
              <input class="form-control Nformat" type="text" name="monto_operacion_mn_r" value="<?php echo $monto_operacion_mn_r; ?>" >
            </div>
            <div class="col-md-4 form-group">
              <label>Hash de la operación</label>
              <div class="d-flex mb-2">
              <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input class="form-control" type="text" name="hash_operacion_r" value="<?php echo $hash_operacion_r; ?>" >
              </div>
              <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_hashOperacion_ayuda()"><i class="mdi mdi-help"></i></button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div id="EL3" style="display: none;">
        <form id="T3">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha y hora en la que se concluyo la operación reportada</label>
              <input class="form-control dateTime" data-inputmask="'alias': 'datetime'" data-inputmask-inputformat="yyyy-mm-dd HH:MM:ss" name="fecha_hora_operacion" value="<?php echo $fecha_hora_operacion; ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Monto Total en Pesos Mexicanos de la operación</label>
              <input class="form-control Nformat" type="text" name="monto_operacion_mn" value="<?php echo $monto_operacion_mn; ?>">
            </div>
            <div class="col-md-12">
              <hr class="subsubtitle">
              Datos del activo virtual
            </div>
            <div class="col-md-4 form-group">
              <label>Activo virtual operado</label>
              <select class="form-control" id="SAV4" name="activo_virtual_operado">
                <?php foreach ($ActivosV as $key): ?>
                <?php if ($activo_virtual_operado == $key->clave){ $sel="Selected"; }else{ $sel=""; } ?>
              <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->nombre; ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-4 form-group" id="NAC4" style="display: none;">
              <label>Nombre del activo virtual</label>
              <input class="form-control" type="" name="descripcion_activo_virtual" value="<?php echo $descripcion_activo_virtual; ?>">
            </div>
            <div class="col-md-5 form-group">
              <label>Tipo de cambio a Pesos Mexicanos en el momento que se concluyo la operación</label>
              <input class="form-control Nformat" type="" name="tipo_cambio_mn" value="<?php echo $tipo_cambio_mn; ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Cantidad del activo Virtual de la operación</label>
              <input class="form-control" type="" name="cantidad_activo_virtual" value="<?php echo $cantidad_activo_virtual; ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Hash de la operación</label>
              <div class="d-flex mb-2">
              <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input class="form-control" type="" name="hash_operacion" value="<?php echo $hash_operacion; ?>">
              </div>
              <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_hashOperacion_ayuda()"><i class="mdi mdi-help"></i></button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div id="EL4" style="display: none;">
        <form id="T4">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha y hora en la que se concluyo la operación reportada</label>
              <input class="form-control dateTime" data-inputmask="'alias': 'datetime'" data-inputmask-inputformat="yyyy-mm-dd HH:MM:ss" name="fecha_hora_operacion" value="<?php echo $fecha_hora_operacion; ?>">
            </div>
            <div class="col-md-6 form-group">
              <label>Instrumento monetario con el que se realizó la operación o acto:</label>
              <select class="form-control" disabled name="instrumento_monetario">
                <option value="1">Efectivo</option>
                <option value="2">Tarjeta de Crédito</option>
                <option value="3">Tarjeta de Debito</option>
                <option value="4">Tarjeta de Prepago</option>
                <option value="5">Cheque Nominativo</option>
                <option value="6">Cheque de Caja</option>
                <option value="7">Cheques de Viajero</option>
                <option value="8">Transferencia Interbancaria</option>
                <option value="9">Transferencia Misma Institución</option>
                <option value="10">Transferencia Internacional</option>
                <option value="11">Orden de Pago</option>
                <option value="12">Giro</option>
                <option value="13">Oro o Platino Amonedados</option>
                <option value="14">Plata Amonedada</option>
                <option value="15">Metales Preciosos</option>
                <option value="16">Activos Virtuales</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Tipo de moneda de la operacion o acto</label>
              <select class="form-control" type="" name="moneda_operacion">
                <?php /*foreach ($MonedasM as $key): ?>
                <?php if ($moneda_operacion == $key->clave){ $sel="Selected"; }else{ $sel=""; } ?> 
                <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->moneda; ?></option>
                <?php endforeach */ ?>
                <option value="0" <?php if($moneda_operacion=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                <option value="1" <?php if($moneda_operacion=="1") echo "selected"; ?>>Dólar Americano</option>
                <option value="2" <?php if($moneda_operacion=="2") echo "selected"; ?>>Euro</option>
                <option value="3" <?php if($moneda_operacion=="3") echo "selected"; ?>>Libra Esterlina</option>
                <option value="4" <?php if($moneda_operacion=="4") echo "selected"; ?>>Dólar canadiense</option>
                <option value="5" <?php if($moneda_operacion=="5") echo "selected"; ?>>Yuan Chino</option>
                <option value="6" <?php if($moneda_operacion=="6") echo "selected"; ?>>Centenario</option>
                <option value="7" <?php if($moneda_operacion=="7") echo "selected"; ?>>Onza de Plata</option>
                <option value="8" <?php if($moneda_operacion=="8") echo "selected"; ?>>Onza de Oro</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Monto de la operación o acto</label>
              <input class="form-control Nformat" type="text" name="monto_operacion" value="<?php echo $monto_operacion; ?>">
            </div>
            <div class="col-md-12">
              <hr class="subsubtitle">
              <label>Datos del beneficiario</label>
            </div>
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" value="1" name="Persona" id="Fis">
                  Persona fisica
                </label>
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" value="2" name="Persona" id="Mor">
                  Persona Moral
                </label>
              </div>
            </div>
            <div id="Fisica" class="col-md-12 row">
              <div class="col-md-4 form-group">
                <label>Nombre</label>
                <input class="form-control" type="text" name="nombre" value="<?php echo $nombre; ?>">
              </div>
              <div class="col-md-4 form-group">
                <label>Apellido Paterno</label>
                <input class="form-control" type="text" name="apellido_paterno" value="<?php echo $apellido_paterno; ?>">
              </div>
              <div class="col-md-4 form-group">
                <label>Apellido Materno</label>
                <input class="form-control" type="text" name="apellido_materno" value="<?php echo $apellido_materno; ?>">
              </div>
            </div> 
            <div id="Moral" style="display: none;" class="col-md-12 row">
              <div class="col-md-4 form-group">
                <label>Denominacion o razón social</label>
                <input class="form-control Nformat" type="" name="denominacion_razon" value="<?php echo $denominacion_razon; ?>">
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" value="1" id="Nac" name="Cuenta">
                  Cuenta Nacional
                </label>
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" value="2" id="Ext" name="Cuenta">
                  Cuenta Extranjera
                </label>
              </div>
            </div>
            <div id="Nacional" class="col-md-12 row">
              <div class="col-md-4 form-group">
                <label>Clabe interbancaria</label>
                <input class="form-control Nformat" type="" name="clabe_destino" value="<?php echo $clabe_destino; ?>">
              </div>
              <div class="col-md-4 form-group">
                <label>Institución financiera</label>
                <input class="form-control" type="" name="clave_institucion_financiera" value="<?php echo $clave_institucion_financiera; ?>">
              </div>
            </div>
            <div id="Extranjera" style="display: none;" class="col-md-12 row">
              <div class="col-md-4 form-group">
                <label>Numero de Cuenta</label>
                <input class="form-control Nformat" type="" name="numero_cuenta" value="<?php echo $numero_cuenta; ?>">
              </div>
              <div class="col-md-4 form-group">
                <label>Nombre de la institución</label>
                <input class="form-control Nformat" type="" name="nombre_banco" value="<?php echo $nombre_banco; ?>">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
          <button class="btn gradient_nepal2" type="button" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
          <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
        </div>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 carácteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_hashOperacion_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Hash de Operación</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Mínimo 1 caracter.</li>
           <li>Máximo 2000 caracteres.</li>
           <li>Acepta letras y números.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--modals modificatorio de ayuda-->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>