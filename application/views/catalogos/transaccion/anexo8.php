<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #323556;
  }
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #24284b;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4>Anexo 8 - Vehículos</h4>  
      	<h3>Registro de transacción</h3>
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        
        <form id="form_anexo8" class="forms-sample">
          <input type="hidden" id="idtransbene" name="idtransbene" class="form-control" value="<?php echo $idtransbene ?>">
          <?php if(isset($a->id)){ ?>
            <input type="hidden" name="id" id="id" value="<?php echo $a->id ?>">
          <?php } ?>
          <input type="hidden" name="idperfilamiento_cliente" id="idperfilamiento_cliente" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="idcliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>">
          <input type="hidden" id="id_anexo8" value="<?php echo $id_anexo8; ?>">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha de operación: Fecha de recepción de recursos</label>
              <input type="date" max="<?php echo date("Y-m-d")?>" onkeydown="return false" name="fecha_opera" id="fecha_opera" class="form-control" <?php if(isset($a->fecha_opera)) echo "value='$a->fecha_opera'"; ?> placeholder="" required>
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" <?php if(isset($a->referencia)) echo "value='$a->referencia'"; ?> placeholder="Ingrese número de referencia">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group">
              <label>Monto de factura</label>
              <input type="text" name="monto_opera" id="monto_opera" class="form-control" onchange="tipo_modeda()" <?php if(isset($a->monto_opera)) echo "value='$a->monto_opera'"; ?> placeholder="Ingrese el monto total de la operación ($)" required>
            </div>
            <div class="row col-12 form-group">
              <div class="col-md-3 form-group">
                <label>Año acuse:</label>
                <select class="form-control" id="anio" name="anio_acuse">
                </select>
              </div>
              <div class="col-md-3 form-group">
                <label>Mes acuse:</label>
                <select class="form-control" id="mes" name="mes_acuse">
                  <option value="01">Enero</option>
                  <option value="02">Febrero</option>
                  <option value="03">Marzo</option>
                  <option value="04">Abril</option>
                  <option value="05">Mayo</option>
                  <option value="06">Junio</option>
                  <option value="07">Julio</option>
                  <option value="08">Agosto</option>
                  <option value="09">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label>No. Factura:</label>
                <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" <?php if(isset($a->num_factura)) echo "value='$a->num_factura'"; ?> placeholder="Ingrese número de factura">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <!-- --> 
          <hr class="subsubtitle">
          <div class="row">
            <?php if($aviso==1){ ?>
              <div class="col-md-12">
              </div>
              <div class="col-md-10 form-group">
                <h5>Datos de la modificación:</h5>
              </div>

              <div class="col-md-2 form-group">
              </div>

              <div class="col-md-3 form-group">
                <label>Referencia modificación:</label>
                <div class="d-flex mb-2">  
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica" <?php if(isset($a->referencia_modifica)) { echo "value='$a->referencia_modifica'";  } ?> placeholder="Referencia de modificación">
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button>   
                </div>   
              </div>
              <div class="col-md-3 form-group">
                <label>Folio de aviso que se modificará:</label>
                <input type="hidden" name="id_anexo8" id="id_anexo8" class="form-control" <?php if(isset($a->id)) echo "value='$a->id'"; ?>>
                <div class="d-flex mb-2">  
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio" <?php if(isset($a->folio_modificatorio)) echo "value='$a->folio_modificatorio'"; ?>>
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button>   
                </div>   
              </div>
              <div class="col-md-3 form-group">
                <label>Descripción de la modificación:</label>
                <textarea name="descrip_modifica" id="descrip_modifica" name="textarea" rows="4" cols="80" <?php if(isset($a->descrip_modifica)) echo "value='$a->descrip_modifica'";?> > <?php if(isset($a->descrip_modifica)) echo $a->descrip_modifica; ?> </textarea>
              </div>
              <div class="col-md-12">
                <hr class="subsubtitle">
              </div>
            <?php }?>
          </div>


          <div class="row">
            <div class="col-md-12 form-group">
              <label>Tipo de operación</label>
              <select class="form-control" name="tipo_opera" id="tipo_opera" required>
                <option value="801" <?php if(isset($a) && $a->tipo_opera=="801"){ echo "selected"; } ?>>Venta de vehículo nuevo</option>
                <option value="802" <?php if(isset($a) && $a->tipo_opera=="802"){ echo "selected"; } ?>>Venta de vehículo usado</option>
                <option value="805" <?php if(isset($a) && $a->tipo_opera=="805"){ echo "selected"; } ?>>Intercambio</option>
                <!--<option value="renta" <?php if(isset($a) && $a->tipo_opera=="renta"){ echo "selected"; } ?>>Renta de vehículo</option>
                <option value="prestamo" <?php if(isset($a) && $a->tipo_opera=="prestamo"){ echo "selected"; } ?>>Préstamo de vehículo</option>
                <option value="servicio" <?php if(isset($a) && $a->tipo_opera=="servicio"){ echo "selected"; } ?>>Servicio</option>-->
              </select>
            </div>
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" <?php if(isset($a->tipo_vehiculo) && $a->tipo_vehiculo==1) echo "value='$a->tipo_vehiculo' checked"; ?> class="form-check-input" name="tipo_vehiculo" id="vehiculo" value="1">
                  Vehículo Terrestre
                </label>
              </div>
             </div>
              <div class="row" id="cont_terre" style="display: none">
                <div class="col-md-12 form-group">
                  <label>Marca/fabricante</label>
                  <input class="form-control" type="text" name="marcat" <?php if(isset($a->marcat)) echo "value='$a->marcat'"; ?>>
                </div>
                <div class="col-md-6 form-group">
                  <label>Modelo</label>
                  <input class="form-control" type="text" name="modelot" <?php if(isset($a->modelot)) echo "value='$a->modelot'"; ?>>
                </div>
                <div class="col-md-6 form-group">
                  <label>Año</label>
                  <input class="form-control" type="text" name="aniot" <?php if(isset($a->aniot)) echo "value='$a->aniot'"; ?>>
                </div>
                <div class="col-md-6 form-group">
                  <label>Número de Identificación vehicular</label>
                  <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input type="text" class="form-control form-control-sm" name="num_vehit" id="num_vehit" onchange="verificaTama();" <?php if(isset($a->num_vehit)) echo "value='$a->num_vehit'"; ?>>
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_numero_ayuda()">
                      <i class="mdi mdi-help"></i>
                    </button>              
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <label>Número de inscripción en el Registro Público Vehicular</label>
                  <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input type="text" class="form-control form-control-sm" name="num_reg_pubt" <?php if(isset($a->num_reg_pubt)) echo "value='$a->num_reg_pubt'"; ?>>
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_numero_ayuda2()">
                      <i class="mdi mdi-help"></i>
                    </button>              
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <label>Matrícula</label>
                  <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <input type="text" class="form-control form-control-sm" name="matriculat" <?php if(isset($a->matriculat)) echo "value='$a->matriculat'"; ?>>
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_matricula_ayuda()">
                      <i class="mdi mdi-help"></i>
                    </button>              
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <label>Nivel de blindaje</label>
                  <select class="form-control" name="blindajet">
                    <option value="">Elige un nivel</option>
                    <option value="1" <?php if(isset($a) && $a->blindajet=="1"){ echo "selected"; } ?>>Nivel A</option>
                    <option value="2" <?php if(isset($a) && $a->blindajet=="2"){ echo "selected"; } ?>>Nivel B</option>
                    <option value="3" <?php if(isset($a) && $a->blindajet=="3"){ echo "selected"; } ?>>Nivel B Plus</option>
                    <option value="4" <?php if(isset($a) && $a->blindajet=="4"){ echo "selected"; } ?>>Nivel C</option>
                    <option value="5" <?php if(isset($a) && $a->blindajet=="5"){ echo "selected"; } ?>>Nivel C Plus</option>
                    <option value="6" <?php if(isset($a) && $a->blindajet=="6"){ echo "selected"; } ?>>Nivel D</option>
                    <option value="7" <?php if(isset($a) && $a->blindajet=="7"){ echo "selected"; } ?>>Nivel E</option>
                    <option value="9" <?php if(isset($a) && $a->blindajet=="9"){ echo "selected"; } ?>>No Aplica</option>
                  </select>
                </div>
              </div>
             <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_vehiculo" id="vehiculo_terr" <?php if(isset($a->tipo_vehiculo) && $a->tipo_vehiculo==2) echo "value='$a->tipo_vehiculo' checked"; ?> value="2">
                  Vehículo Marítimo
                </label>
              </div>
             </div>
              <div class="row" id="cont_mari" style="display: none">
                <div class="col-md-12 form-group">
                  <label>Marca/fabricante</label>
                  <input class="form-control" type="text" name="marcam" <?php if(isset($a->marcam)) echo "value='$a->marcam'"; ?>>
                </div>
                <div class="col-md-6 form-group">
                  <label>Modelo</label>
                  <input class="form-control" type="text" name="modelom" <?php if(isset($a->modelom)) echo "value='$a->modelom'"; ?>>
                </div>
                <div class="col-md-6 form-group">
                  <label>Año</label>
                  <input class="form-control" type="text" name="aniom" <?php if(isset($a->aniom)) echo "value='$a->aniom'"; ?>>
                </div>
                <div class="col-md-12 form-group">
                  <label>Número de serie</label>
                  <input class="form-control" type="text" name="num_seriem" <?php if(isset($a->num_seriem)) echo "value='$a->num_seriem'"; ?>>
                </div>
                <div class="col-md-12 form-group">
                  <label>Bandera</label>
                  <select class="form-control" name="banderam">
                    <?php foreach ($ban->result() as $b) {
                      $select="";
                      if(isset($a) && $a->banderam==$b->id){ $select="selected"; }
                      echo "<option value='$b->id'$select>$b->descrip</option>";
                    } ?>
                  </select>
                </div>
                <div class="col-md-12 form-group">
                  <label>Matrícula</label>
                  <input class="form-control" type="text" name="matriculam" <?php if(isset($a->matriculam)) echo "value='$a->matriculam'"; ?>>
                </div>
                <div class="col-md-12 form-group">
                  <label>Nivel de blindaje</label>
                  <select class="form-control" name="nivel_blindajem">
                    <option value="">Elige un nivel</option>
                    <option value="1" <?php if(isset($a) && $a->blindajet=="1"){ echo "selected"; } ?>>Nivel A</option>
                    <option value="2" <?php if(isset($a) && $a->blindajet=="2"){ echo "selected"; } ?>>Nivel B</option>
                    <option value="3" <?php if(isset($a) && $a->blindajet=="3"){ echo "selected"; } ?>>Nivel B Plus</option>
                    <option value="4" <?php if(isset($a) && $a->blindajet=="4"){ echo "selected"; } ?>>Nivel C</option>
                    <option value="5" <?php if(isset($a) && $a->blindajet=="5"){ echo "selected"; } ?>>Nivel C Plus</option>
                    <option value="6" <?php if(isset($a) && $a->blindajet=="6"){ echo "selected"; } ?>>Nivel D</option>
                    <option value="7" <?php if(isset($a) && $a->blindajet=="7"){ echo "selected"; } ?>>Nivel E</option>
                    <option value="9" <?php if(isset($a) && $a->blindajet=="9"){ echo "selected"; } ?>>No Aplica</option>
                  </select>
                </div>
              </div>
             <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_vehiculo" id="vehiculo_aereo" <?php if(isset($a->tipo_vehiculo) && $a->tipo_vehiculo==3) echo "value='$a->tipo_vehiculo' checked"; ?>  value="3">
                  Vehículo Aéreo
                </label>
              </div>
             </div>
              <div class="row" id="cont_aereo" style="display: none">
                <div class="col-md-12 form-group">
                  <label>Marca/fabricante</label>
                  <input class="form-control" type="text" name="marcaa" <?php if(isset($a->marcaa)) echo "value='$a->marcaa'"; ?>>
                </div>
                <div class="col-md-6 form-group">
                  <label>Modelo</label>
                  <input class="form-control" type="text" name="modeloa" <?php if(isset($a->modeloa)) echo "value='$a->modeloa'"; ?>>
                </div>
                <div class="col-md-6 form-group">
                  <label>Año</label>
                  <input class="form-control" type="text" name="anioa" <?php if(isset($a->anioa)) echo "value='$a->anioa'"; ?>>
                </div>
                <div class="col-md-12 form-group">
                  <label>Número de serie</label>
                  <input class="form-control" type="text" name="num_seriea" <?php if(isset($a->num_seriea)) echo "value='$a->num_seriea'"; ?>>
                </div>
                <div class="col-md-12 form-group">
                  <label>Bandera</label>
                  <select class="form-control" name="bandera_aerea">
                    <?php foreach ($ban->result() as $b) {
                      $select="";
                      if(isset($a) && $a->bandera_aerea==$b->id){ $select="selected"; }
                      echo "<option value='$b->id'$select>$b->descrip</option>";
                    } ?>
                  </select>
                </div>
                <div class="col-md-12 form-group">
                  <label>Matrícula</label>
                  <input class="form-control" type="text" name="matricula_aerea" <?php if(isset($a->matricula_aerea)) echo "value='$a->matricula_aerea'"; ?>>
                </div>
                <div class="col-md-12 form-group">
                  <label>Nivel de blindaje</label>
                  <select class="form-control" name="nivel_blindaje_a">
                    <option value="">Elige un nivel</option>
                    <option value="1" <?php if(isset($a) && $a->blindajet=="1"){ echo "selected"; } ?>>Nivel A</option>
                    <option value="2" <?php if(isset($a) && $a->blindajet=="2"){ echo "selected"; } ?>>Nivel B</option>
                    <option value="3" <?php if(isset($a) && $a->blindajet=="3"){ echo "selected"; } ?>>Nivel B Plus</option>
                    <option value="4" <?php if(isset($a) && $a->blindajet=="4"){ echo "selected"; } ?>>Nivel C</option>
                    <option value="5" <?php if(isset($a) && $a->blindajet=="5"){ echo "selected"; } ?>>Nivel C Plus</option>
                    <option value="6" <?php if(isset($a) && $a->blindajet=="6"){ echo "selected"; } ?>>Nivel D</option>
                    <option value="7" <?php if(isset($a) && $a->blindajet=="7"){ echo "selected"; } ?>>Nivel E</option>
                    <option value="9" <?php if(isset($a) && $a->blindajet=="9"){ echo "selected"; } ?>>No Aplica</option>
                  </select>
                </div>
              </div>
            </form><!-- form abexo8 -->
            <div class="col-md-12">
              <hr class="subsubtitle">
            </div>
          </div>
          <div class="row" id="cont_gral">
            <?php 
            $totalp = 0;
            $totalefect = 0;
            $cont = 0;
              if($pagos_transP==1){
                foreach ($pagos_trans as $p){ 
                  $totalp = $totalp + $p->monto;
                  if(isset($p) && $p->instrum_notario=="1"){
                    $totalefect = $totalefect +$p->monto;
                  }
                  $cont++;
                  ?>
                  <div class="cont_padre">
                    <form id="form_pagos" method="POST" class="form_pagos">
                      <div class="cont_hijoExist row">
                        <input type="hidden" id="acusado" <?php if(isset($p->acusado)) echo "value='$p->acusado'"; ?>>
                        <input type="hidden" id="id_pago" <?php if(isset($p->id)) echo "value='$p->id'"; ?>>
                        <input type="hidden" id="xml" <?php if(isset($p->xml)) echo "value='$p->xml'"; ?>>
                        <div class="col-md-4 form-group">
                          <label>Fecha de pago</label>
                          <input class="form-control fecha_pago" onkeydown="return false" onchange="validar_fecha(this.value,0)" max="<?php echo date("Y-m-d")?>" type="date" id="fecha_pago" <?php if(isset($p->fecha_pago)) echo "value='$p->fecha_pago'"; ?>>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Forma de pago</label>
                          <select id="forma_pago" class="form-control">
                            <option value="1" <?php if(isset($p) && $p->forma_pago=="1"){ echo "selected"; } ?>>Contado</option>
                            <option value="2" <?php if(isset($p) && $p->forma_pago=="2"){ echo "selected"; } ?>>Diferido</option>
                            <option value="3" <?php if(isset($p) && $p->forma_pago=="3"){ echo "selected"; } ?>>Dación en pago</option>
                            <option value="4" <?php if(isset($p) && $p->forma_pago=="4"){ echo "selected"; } ?>>Préstamo o crédito</option>
                            <option value="5" <?php if(isset($p) && $p->forma_pago=="5"){ echo "selected"; } ?>>Permuta</option>
                          </select>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Instrumento monetario con el que se realizó la operación o acto</label>
                          <select id="instrum_notario" class="form-control" onchange="confirmarPagosMonto()">
                            <option value="1" <?php if(isset($p) && $p->instrum_notario=="1"){ echo "selected"; } ?>>Efectivo</option>
                            <option value="2" <?php if(isset($p) && $p->instrum_notario=="2"){ echo "selected"; } ?>>Tarjeta de Crédito</option>
                            <option value="3" <?php if(isset($p) && $p->instrum_notario=="3"){ echo "selected"; } ?>>Tarjeta de Debito</option>
                            <option value="4" <?php if(isset($p) && $p->instrum_notario=="4"){ echo "selected"; } ?>>Tarjeta de Prepago</option>
                            <option value="5" <?php if(isset($p) && $p->instrum_notario=="5"){ echo "selected"; } ?>>Cheque Nominativo</option>
                            <option value="6" <?php if(isset($p) && $p->instrum_notario=="6"){ echo "selected"; } ?>>Cheque de Caja</option>
                            <option value="7" <?php if(isset($p) && $p->instrum_notario=="7"){ echo "selected"; } ?>>Cheques de Viajero</option>
                            <option value="8" <?php if(isset($p) && $p->instrum_notario=="8"){ echo "selected"; } ?>>Transferencia Interbancaria</option>
                            <option value="9" <?php if(isset($p) && $p->instrum_notario=="9"){ echo "selected"; } ?>>Transferencia Misma Institución</option>
                            <option value="10" <?php if(isset($p) && $p->instrum_notario=="10"){ echo "selected"; } ?>>Transferencia Internacional</option>
                            <option value="11" <?php if(isset($p) && $p->instrum_notario=="11"){ echo "selected"; } ?>>Orden de Pago</option>
                            <option value="12" <?php if(isset($p) && $p->instrum_notario=="12"){ echo "selected"; } ?>>Giro</option>
                            <option value="13" <?php if(isset($p) && $p->instrum_notario=="13"){ echo "selected"; } ?>>Oro o Platino Amonedados</option>
                            <option value="14" <?php if(isset($p) && $p->instrum_notario=="14"){ echo "selected"; } ?>>Plata Amonedada</option>
                            <option value="15" <?php if(isset($p) && $p->instrum_notario=="15"){ echo "selected"; } ?>>Metales Preciosos</option>
                            <option value="16" <?php if(isset($p) && $p->instrum_notario=="16"){ echo "selected"; } ?>>Activos Virtuales</option>
                            <option value="17" <?php if(isset($p) && $p->instrum_notario=="17"){ echo "selected"; } ?>>Otros</option>
                          </select>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Tipo de moneda o divisa de la operación</label>
                          <select id="tipo_moneda" class="form-control" onchange="confirmarPagosMonto()">
                            <?php
                            /*$pesomxc=1;   
                            foreach ($tm->result() as $t) {
                            $select="";
                              if($t->id==$pesomxc){
                                  if(isset($p) && $p->tipo_moneda==$t->id){ $select="selected"; }
                                echo "<option value='$t->id'$select>$t->moneda</option>";
                              } 
                            } */ ?>
                            <option value="0" <?php if($p->tipo_moneda=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                            <option value="1" <?php if($p->tipo_moneda=="1") echo "selected"; ?>>Dólar Americano</option>
                            <option value="2" <?php if($p->tipo_moneda=="2") echo "selected"; ?>>Euro</option>
                            <option value="3" <?php if($p->tipo_moneda=="3") echo "selected"; ?>>Libra Esterlina</option>
                            <option value="4" <?php if($p->tipo_moneda=="4") echo "selected"; ?>>Dólar canadiense</option>
                            <option value="5" <?php if($p->tipo_moneda=="5") echo "selected"; ?>>Yuan Chino</option>
                            <option value="6" <?php if($p->tipo_moneda=="6") echo "selected"; ?>>Centenario</option>
                            <option value="7" <?php if($p->tipo_moneda=="7") echo "selected"; ?>>Onza de Plata</option>
                            <option value="8" <?php if($p->tipo_moneda=="8") echo "selected"; ?>>Onza de Oro</option>
                          </select>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Monto de la operación o acto sin IVA ni accesorios</label>
                          <input onchange="confirmarPagosMonto()" class="form-control monto<?php echo $cont; ?>" type="text" min="0" step="0.01" name="monto" id="monto<?php echo $cont; ?>" <?php if(isset($p->monto)) { $monto = number_format($p->monto,2,'.',','); echo "value='$ $monto'";} else { echo "value='$0.00'"; } ?> >
                        </div>
                        <div class="row">
                          <div class="col-md-10 form-group">
                            <div>
                              <label style="color: transparent;">agregar liquidacion</label>
                              <button type="button" onclick="remove_liquidaE($(this),<?php echo $p->id; ?>)" class="btn gradient_nepal2"><i class="fa fa-trash-o"></i> Liquidación</button>
                            </div>         
                          </div>
                        </div>
                        <div class="col-md-12">
                          <hr class="subsubtitle">
                        </div>
                      </div><!-- cont_hijo -->
                    </form>
                  </div><!-- cont_padre -->
              <?php
                }
              } ?>
          <div class="col-md-12">
              <h4>Datos de liquidación</h4>
            </div><br><br>
            <div class="cont_padre">
              <form id="form_pagos" method="POST" class="form_pagos_padre">
                <div class="cont_hijo row">
                  <input type="hidden" id="id_pago" value='0'>
                  <div class="col-md-4 form-group">
                    <label>Fecha de pago</label>
                    <input class="form-control fecha_pago" onkeydown="return false" onchange="validar_fecha(this.value,0)" max="<?php echo date("Y-m-d")?>" type="date" id="fecha_pago" >
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Forma de pago</label>
                    <select id="forma_pago" class="form-control">
                      <option value="1">Contado</option>
                      <option value="2">Diferido</option>
                      <option value="3">Dación en pago</option>
                      <option value="4">Préstamo o crédito</option>
                      <option value="5">Permuta</option>
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Instrumento monetario con el que se realizó la operación o acto</label>
                    <select id="instrum_notario" class="form-control" onchange="confirmarPagosMonto()">
                      <option value="1">Efectivo</option>
                      <option value="2">Tarjeta de Crédito</option>
                      <option value="3">Tarjeta de Debito</option>
                      <option value="4">Tarjeta de Prepago</option>
                      <option value="5">Cheque Nominativo</option>
                      <option value="6">Cheque de Caja</option>
                      <option value="7">Cheques de Viajero</option>
                      <option value="8">Transferencia Interbancaria</option>
                      <option value="9">Transferencia Misma Institución</option>
                      <option value="10">Transferencia Internacional</option>
                      <option value="11">Orden de Pago</option>
                      <option value="12">Giro</option>
                      <option value="13">Oro o Platino Amonedados</option>
                      <option value="14">Plata Amonedada</option>
                      <option value="15">Metales Preciosos</option>
                      <option value="16">Activos Virtuales</option>
                      <option value="17">Otros</option>
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Tipo de moneda o divisa de la operación</label>
                    <select id="tipo_moneda" class="form-control" onchange="confirmarPagosMonto()">
                      <?php
                      /*$moneda_mxc=1;
                      foreach ($tm->result() as $t) {
                        if($t->id==$moneda_mxc){
                          echo "<option value='$t->id'>$t->moneda</option>";
                        }
                      } */ ?>
                      <option value="0">Pesos Mexicanos</option>
                      <option value="1">Dólar Americano</option>
                      <option value="2">Euro</option>
                      <option value="3">Libra Esterlina</option>
                      <option value="4">Dólar canadiense</option>
                      <option value="5">Yuan Chino</option>
                      <option value="6">Centenario</option>
                      <option value="7">Onza de Plata</option>
                      <option value="8">Onza de Oro</option>
                    </select>
                  </div>
                  <div class="col-md-4 form-group">
                    <label>Monto de la operación o acto sin IVA ni accesorios</label>
                    <input class="form-control monto" type="text" name="monto" id="monto" onchange="confirmarPagosMonto()" value="$0.00">
                  </div>
                  <div class="col-md-2 row">
                    <div class="col-md-10 form-group">
                      <div>
                        <label style="color: transparent;">agregar liquidación</label>
                        <button type="button" onclick="add_liquida()" class="btn gradient_nepal2"><i class="fa fa-plus"></i> Otra Liquidación</button>
                      </div>         
                    </div>
                  </div>
                  <div class="col-md-2">
                    <br>
                    <h3 class="monto_divisa" style="color: red;"></h3>
                  </div>
                  <div class="col-md-12">
                    <hr class="subsubtitle">
                  </div>
                </div><!-- cont_hijo -->
              </form>
            </div><!-- cont_padre -->
          <div class="cont_padre">
            <form id="form_pagos" method="POST" class="form_pagos_clone">
              
            </form>
          </div>
          
          <div class="row" id="cont_benef">
          </div>
        </div>
        <div class="row">
           <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total de la liquidación</label>
              <input readonly class="form-control" type="text" id="total_liquida" value="$0.00">
            </div>
            <div class="col-md-8" align="center">
              <br><br>
              <h5 class="validacion_cantidad" style="color: red"></h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total en efectivo</label>
              <input readonly class="form-control" type="text" id="total_liquida_efect" value="$0.00">
              <div class="col-md-8" align="center">
              <br><br>
                <h5 class="limite_efect_msj" style="color: red"></h5>

              </div>
            </div>
        </div>
        <div class="modal-footer">
      		<button class="btn gradient_nepal2" id="btn_submit" type="submit"><i class="fa fa-save"></i> Registrar Transacción</button>
      		<a class="btn gradient_nepal2 regresar" href="#" id="regresa" type=""><i class="fa fa-arrow-left"></i> Regresar</a>
        </div>
        <div id="settings-trigger" style="width: 204px; left: 70px;" class="settings-trigger2"> 
          <button type="button" id="agregar_liquida" class="btn gradient_nepal2" onclick="add_liquida()"><i class="fa fa-plus"></i> Agregar Liquidación</button>
        </div>
    	</div>
    </div>
	</div>
</div>

<!------ Modificatorio ------->
<div class="modal fade" id="modal_numero_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de Identificación vehicular</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe de capturar al menos alguno de los tres campos: número de identificación vehícular o número de inscripción vehícular o matricula. 
Si se cuenta con los tres datos, se pueden capturar.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<!------ Modificatorio ------->
<div class="modal fade" id="modal_numero_ayuda2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de inscripción en el Registro Público Vehicular</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe de capturar al menos alguno de los tres campos: número de identificación vehícular o número de inscripción vehícular o matricula. 
Si se cuenta con los tres datos, se pueden capturar..</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_matricula_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Matrícula</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe de capturar al menos alguno de los tres campos: número de identificación vehícular o número de inscripción vehícular o matricula. 
Si se cuenta con los tres datos, se pueden capturar.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Operaciones -->
<!--------------Modal-------------->
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 caracteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- / -->
<!------ Modificatorio ------->
<!--- --->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- < / >--->