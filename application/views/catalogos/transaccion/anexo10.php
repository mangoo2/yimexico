<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h4>Anexo 10 - Traslado y custodia de valores.</h4>  
            <h3>Registro de transacción</h3>
          </div>
        </div>
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <input type="hidden" id="aux_pga" value="<?php echo $aux_pga; ?>">
        <input type="hidden" id="id_ax1" value="<?php echo $id; ?>">
        <input type="hidden" id="valor_uma">
        <form id="form_anexo10" class="forms-sample">
          <input type="hidden" name="id" value="<?php echo $id; ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso" id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Fecha de operación o acto </label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input type="text" name="referencia" id="referencia" class="form-control" placeholder="Ingrese número de referencia" required="" value="<?php echo $referencia ?>">
              </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group">
              <label>Monto de factura</label>
              <input type="text" id="monto_opera" class="form-control Nformat" placeholder="Ingrese el monto total de la operación ($)" name="monto_opera" required="" value="<?php echo $monto_opera ?>">
            </div>
            <div class="col-md-3 form-group">
            </div>
            <div class="col-md-3 form-group">
              <label>Año acuse:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes acuse:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <input type="text" name="num_factura" id="num_factura" class="form-control" placeholder="Ingrese número de factura" required="" value="<?php echo $num_factura ?>">
              </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <div class="row">
            <?php if($aviso==1){ ?>
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>

            <div class="col-md-2 form-group">
            </div>

            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div> 
            </div>
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="idanexo10" id="idanexo10" class="form-control" value="<?php echo $idanexo10 ?>">
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div> 
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="70"><?php echo $descrip_modifica ?></textarea>
            </div>
            <?php }?>
          </div>
      </form>
        <hr class="subsubtitle">
      	<div class="row" id="det_client" style="display: none;">
      		<div class="col-md-6">
      			<div class="row">
      				<div class="col-md-12">
      					<h3>Cliente</h3>
      				</div>
      				<div class="col-md-3">
      					<label>Nombre</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>Apellido Paterno</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>Apellido Materno</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-5">
      					<label>Nombre de identificación</label>
      				</div>
      				<div class="col-md-7">
      					<label></label>
      				</div>
      				<div class="col-md-5">
      					<label>Autoridad que emita la identificación</label>
      				</div>
      				<div class="col-md-7">
      					<label></label>
      				</div>
      				<div class="col-md-5">
      					<label>Número de identificación</label>
      				</div>
      				<div class="col-md-7">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>RFC</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>CURP</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      			</div>
      		</div>
      	</div>
        <input type="hidden" id="TiBi" value="<?php echo $TipoBien; ?>" name="">
      	<div class="row">
          <form id="complete">
            <input type="hidden" id="idLi" name="idliq" value="<?php echo $idliq; ?>">
        		<div class="row">
      				<div class="col-md-6 form-group">
      					<label>Tipo de operación</label>
      					<select class="form-control" name="tipo_operacion">
                  <option value="1001">1 Traslado</option>
      						<option value="1002">2 Custodia</option>
                  <option value="1003">3 Traslado y Custodia</option>
      					</select>
      				</div>
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" <?php if($TipoBien==1){ echo "checked"; } ?> class="form-check-input" name="TipoBien" id="efectivo_inst" value="1">
                    Efectivo o instrumentos financieros
                  </label>
                </div>
               </div>
               <div class="col-md-12">
                 <div class="row" id="traslado_efect" style="display: none">
                <div class="col-md-6 form-group">
                  <label>Instrumento Monetario de Monto Traslado y/o Custodia</label>
                  <select class="form-control" name="instrumento_monetario">
                    <option <?php if($instrumento_monetario==1){  echo "selected";  } ?> value="1">Efectivo</option>
                    <option <?php if($instrumento_monetario==2){  echo "selected";  } ?> value="2">Transacción/Deposito</option>
                    <option <?php if($instrumento_monetario==3){  echo "selected";  } ?> value="3">Cheque</option>
                  </select>
                </div>
                <div class="col-md-6 form-group">
                  <label>Tipo de moneda o divisa del monto trasladado y/o Custodia</label>
                  <select class="form-control" name="moneda">
                    <?php /* foreach ($MonedasM as $key){ 
                      $sel="";
                      if($moneda==$key->clave){ $sel="selected"; } ?>
                      <option <?php echo $sel; ?> value="<?php echo $key->clave; ?>"><?php echo $key->moneda; ?></option>
                    <?php  } */ ?>
                    <option value="0" <?php if($moneda=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                    <option value="1" <?php if($moneda=="1") echo "selected"; ?>>Dólar Americano</option>
                    <option value="2" <?php if($moneda=="2") echo "selected"; ?>>Euro</option>
                    <option value="3" <?php if($moneda=="3") echo "selected"; ?>>Libra Esterlina</option>
                    <option value="4" <?php if($moneda=="4") echo "selected"; ?>>Dólar canadiense</option>
                    <option value="5" <?php if($moneda=="5") echo "selected"; ?>>Yuan Chino</option>
                    <option value="6" <?php if($moneda=="6") echo "selected"; ?>>Centenario</option>
                    <option value="7" <?php if($moneda=="7") echo "selected"; ?>>Onza de Plata</option>
                    <option value="8" <?php if($moneda=="8") echo "selected"; ?>>Onza de Oro</option>
                  </select>
                </div>
                <div class="col-md-6 form-group">
                 <label>Monto Trasladado y/o custodia</label>
                  <input type="text" placeholder="$" name="monto_operacion" value="<?php echo $monto_operacion; ?>" class="form-control Nformat"> 
                </div>
               </div>
               </div>
               <div class="col-md-12 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" <?php if($TipoBien==2){ echo "checked"; } ?> class="form-check-input" name="TipoBien" id="otros" value="2">
                      Otros valores
                    </label>
                  </div>
               </div>
               <div class="col-md-12">
                 <div class="row" id="traslado_otros" style="display: none">
                <div class="col-md-6 form-group">
                  <label>Tipo de Valor traslado y/o custodiado</label>
                  <select class="form-control" name="tipo_valor">
                    <option <?php if ($tipo_valor==1){ echo "selected"; } ?> value="1">Metales Preciosos</option>
                    <option <?php if ($tipo_valor==2){ echo "selected"; } ?> value="2">Piedras preciosas</option>
                    <option <?php if ($tipo_valor==3){ echo "selected"; } ?> value="3">Joyas y/o relojes</option>
                    <option <?php if ($tipo_valor==4){ echo "selected"; } ?> value="4">Obras de arte</option>
                    <option <?php if ($tipo_valor==5){ echo "selected"; } ?> value="5">Vales o cupones</option>
                    <option <?php if ($tipo_valor==6){ echo "selected"; } ?> value="6">Boletos, fichas o cualquier otro tipo de comprobante similar para la práctica de juegos, concursos o sorteos</option>
                    <option <?php if ($tipo_valor==8){ echo "selected"; } ?> value="8">Titulos de crédito</option>
                    <option <?php if ($tipo_valor==99){ echo "selected"; } ?> value="99">Otros</option>
                  </select>
                </div>
                <div class="col-md-6 form-group">
                  <label>Valor estimado de Traslado y/o custodia en moneda nacional</label>
                  <input type="text" name="valor_objeto" placeholder="$" value="<?php echo $valor_objeto; ?>" class="form-control Nformat"> 
                </div>
                <div class="col-md-12 form-group">
                  <label>Descripción del Valor Trasladado y/o custodia</label>
                    <div class="d-flex mb-2">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <textarea class="form-control" name="descripcion" rows="3"><?php echo $descripcion; ?></textarea>
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_descripcion_ayuda()">
                      <i class="mdi mdi-help"></i>
                    </button>              
                  </div>
                </div>
              </div>
               </div>
            </div>
      	</div>
        <hr class="subsubtitle">
        <h3>Datos de Recepción de dinero o valores</h3>
      	<div class="row" id="contenedor-1">
      		<div class="col-md-6">
      				<div class="row">
      					<div class="col-md-12 form-group">
                  <label>Tipo de servicio</label>
                  <select class="form-control" name="tipo_servicio" id="tipo_serv">
                    <option <?php if($tipo_servicio==1){ echo "selected"; }  ?> value="1">Aereo</option>
                    <option <?php if($tipo_servicio==2){ echo "selected"; }  ?> value="2">Terrestre</option>
                    <option <?php if($tipo_servicio==3){ echo "selected"; }  ?> value="3">Marítimo</option>
                    <option <?php if($tipo_servicio==4){ echo "selected"; }  ?> value="4">Mixto</option>
                  </select>
                </div>
      					<div class="col-md-6 form-group">
                  <label>Fecha de recepción</label>
                  <input class="form-control" type="date" name="fecha_recepcion" id="fecha_recep" value="<?php echo $fecha_recepcion; ?>">
                </div>
      					<div class="col-md-12 form-group">
      						<label>CP. de lugar de recepción</label>
      						<input class="form-control" pattern='\d*' maxlength="5" type="text" name="codigo_postal_1" value="<?php echo $codigo_postal_1; ?>">
      					</div>
      				</div>
      		</div>
      	</div>
      <hr class="subsubtitle">
      <h3>Datos de Custodia</h3>
      <div class="row" id="contenedor-1">
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-6 form-group">
                <label>Fecha inicio</label>
                <input class="form-control" type="date" name="fecha_inicio" id="fecha_ini" value="<?php echo $fecha_inicio; ?>">
              </div>
              <div class="col-md-6 form-group">
                <label>Fecha fin</label>
                <input class="form-control" type="date" name="fecha_fin" id="fecha_fin" value="<?php echo $fecha_fin; ?>">
              </div>
            </div>
        </div>
      </div>
      <hr class="subsubtitle">
      <h3>Tipo de Custodia</h3>
      <div class="row" id="contenedor-1">
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" <?php if($bien_tc==1){ echo "checked"; } ?> name="bien_tc" value="1" id="otros">
                    Se realizó custodia en sucursal
                  </label>
                </div>
              </div>
              <div class="col-md-12 form-group">
                <label>Sucursales</label>
                <select class="form-control" name="id_suc">
                  <option></option>
                  <?php foreach ($sucur as $key){ 
                    $dir=$key->vialidad." ".$key->calle." ".$key->no_ext." ".$key->no_int." ".$key->colonia." ".$key->municipio." ".$key->localidad." ".$key->estado." ".$key->cp;
                    ?>
                    <option value="<?php echo $key->iddireccion; ?>"><?php echo $dir; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" <?php if($bien_tc==2){ echo "checked"; } ?> name="bien_tc" value="2" id="otros">
                    Se realizó custodia en otra ubicación
                  </label>
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label>CP</label>
                <input class="form-control" onchange="cambiaCP(this.id)" pattern='\d*' maxlength="5" type="text" name="codigo_postal_2" value="<?php echo $codigo_postal_2; ?>" id="cp">
              </div>
              <div class="col-md-4 form-group">
                <label>Calle, avenida o vía</label>
                <input class="form-control" type="text" name="calle" value="<?php echo $calle; ?>" id="calle">
              </div>
              <div class="col-md-1 form-group">
                <label>No. ext.</label>
                <input class="form-control" type="text" name="numero_exterior" value="<?php echo $numero_exterior; ?>" id="no_ext">
              </div>
              <div class="col-md-1 form-group">
                <label>No. int.</label>
                <input class="form-control" type="text" name="numero_interior" value="<?php echo $numero_interior; ?>" id="no_int">
              </div>
              <div class="col-md-4 form-group">
                <label>Colonia: </label>
                <select onclick="autoComplete(this.id)" class="form-control" name="colonia" id="colonia">
                  <option value=""></option>
                  <?php if($colonia!=""){
                    echo '<option value="'.$colonia.'" selected>'.$colonia.'</option>';      
                  } ?>  
                </select>
              </div>
              
            </div>
        </div>
      </div>
      <hr class="subsubtitle">
      <h3>Datos de entrega del dinero o valores</h3>
      <div class="row" id="contenedor-1">
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-4 form-group">
                <label>Fecha de entrega</label>
                <input class="form-control" type="date" name="fecha_entrega" id="fecha_entre" value="<?php echo $fecha_entrega; ?>">
              </div>
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" <?php if($tipo_entrega==1){ echo "checked"; } ?> class="form-check-input" name="tipo_entrega" value="1" id="ent_nac">
                    Entrega Nacional
                  </label>
                </div>
              </div>
              <div class="row" id="cont_cust" style="display: none">
                <div class="col-md-12 form-group">
                  <label>Código Postal</label>
                  <input class="form-control" type="text" name="codigo_postal_3" value="<?php echo $codigo_postal_3; ?>" id="cp">
                </div>
              </div>
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" <?php if($tipo_entrega==2){ echo "checked"; } ?> class="form-check-input" name="tipo_entrega" value="2" id="ent_inter">
                    Entrega Internacional
                  </label>
                </div>
              </div>
              <div class="row" id="cont_cust_int" style="display: none">
                <div class="col-md-4 form-group">
                  <label>País:</label>
                  <select name="pais" class="form-control">
                    <option value="">Elegir opción</option>
                    <?php foreach ($Pais as $key) { 
                      $sel="";
                      if($pais==$key->clave){ $sel="selected"; } ?>
                      <option value="<?php echo $key->clave ?>" id=""><?php echo $key->pais ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  <label>Estado, providencia, departamento o demarcación</label>
                  <input class="form-control" type="text" name="estado_provincia" value="<?php echo $estado_provincia; ?>" id="calle">
                </div>
                <div class="col-md-3 form-group">
                  <label>Ciudad</label>
                  <input class="form-control" type="text" name="ciudad_poblacion" value="<?php echo $ciudad_poblacion; ?>" id="no_ext">
                </div>
                <div class="col-md-2 form-group">
                  <label>Código Postal</label>
                  <input class="form-control" pattern='\d*' maxlength="5" type="text" name="codigo_postal_4" value="<?php echo $codigo_postal_4; ?>" id="cp">
                </div>
              </div>
            </div>
        </div>
      </div>
      <hr class="subsubtitle">
      <h3>Destino</h3>
      <div class="row" id="contenedor-1">
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-12 form-group">
                <label class="form-check-label">El destinatario es la persona objeto del aviso </label>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" <?php if($destinatario=="SI"){ echo "checked"; } ?> onclick="showPerson()" class="form-check-input" name="destinatario" value="SI" id="sioda">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-12 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" <?php if($destinatario=="NO"){ echo "checked"; } ?> onclick="showPerson()" class="form-check-input" name="destinatario" value="NO" id="nooda">
                    No
                  </label>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div id="Conten-Person">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-4 form-group">
                <label class="form-check-label">Tipo de persona </label>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" onclick="ShowOPC()" name="EDPOA" value="PF" id="POAPF">
                    Persona Fisica
                  </label>
                </div>
              </div>
              <div class="col-md-4 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" onclick="ShowOPC()" name="EDPOA" value="PM" id="POAPM">
                    Persona Moral
                  </label>
                </div>
              </div>
              <div class="col-md-4 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" onclick="ShowOPC()" name="EDPOA" value="F" id="POAF">
                    Fideicomiso
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row" id="DPOVFisica">
          <div class="col-md-12 form-group">
            <label>Nombre(s)</label>
            <input class="form-control" type="text" name="nombre" value="<?php echo $nombre; ?>">
          </div>
          <div class="col-md-6 form-group">
            <label>Apellido Paterno</label>
            <input class="form-control" type="text" name="apellidop" value="<?php echo $apellidop; ?>">
          </div>
          <div class="col-md-6 form-group">
            <label>Apellido Materno</label>
            <input class="form-control" type="text" name="apellidom" value="<?php echo $apellidom; ?>">
          </div>
          <div class="col-md-4 form-group">
            <label>Fecha de nacimiento</label>
            <input class="form-control" type="date" name="fechapf" value="<?php echo $fecha; ?>">
          </div>
          <div class="col-md-4 form-group">
            <label>RFC</label>
            <input class="form-control" type="text" name="rfcpf" value="<?php echo $rfc; ?>">
          </div>
          <div class="col-md-4 form-group">
            <label>CURP</label>
            <input class="form-control" type="text" name="curp" value="<?php echo $curp; ?>">
          </div>
        </div>
        <div class="row" id="DPOVMoral">
          <div class="col-md-12 form-group">
            <label>Denominacion o Razón Social</label>
            <input class="form-control" type="text" name="razons" value="<?php echo $razons; ?>">
          </div>
          <div class="col-md-6 form-group">
            <label>Fecha de Constitución</label>
            <input class="form-control" type="date" name="fechapm" value="<?php echo $fecha; ?>">
          </div>
          <div class="col-md-6 form-group">
            <label>RFC</label>
            <input class="form-control" type="text" name="rfcpm" value="<?php echo $rfc; ?>">
          </div>
        </div>
        <div class="row" id="DPOVFideicomiso">
          <div class="col-md-12 form-group">
            <label>Denominacion o Razón Social del Fiduciario</label>
            <input class="form-control" type="text" name="fideicomiso" value="<?php echo $fideicomiso; ?>">
          </div>
          <div class="col-md-6 form-group">
            <label>identificador fideicomiso</label>
            <input class="form-control" type="text" name="identificador" value="<?php echo $identificador; ?>">
          </div>
          <div class="col-md-6 form-group">
            <label>RFC del fideicomiso</label>
            <input class="form-control" type="text" name="rfcf" value="<?php echo $rfc; ?>">
          </div>
        </div>
      </div>
      </form>
      <div class="modal-footer">
        <button class="btn gradient_nepal2" type="button" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
        <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 caracteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_descripcion_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Descripción del bien</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Longitud mínima 1 carácter y máxima 3000.</li>
           <li>Acepta números, letras, punto, coma, espacio, dos puntos, diagonal,apóstrofe, signo de pesos, guión medio.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--modals modificatorio de ayuda-->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>