<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #323556;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<input type="hidden" id="id_ax4" value="<?php echo $id ?>">   
<input type="hidden" id="checked_tipo_persona" value="<?php echo $tipopersona ?>">
<div class="row">
      <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h4>Anexo 4 - Servicios de mutuo, préstamos o créditos.</h4>  
            <h3>Registro de transacción</h3>
          </div>
        </div>
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <input type="hidden" id="aux_pga" value="<?php echo $aux_pga ?>">
        <form id="form_anexo4" class="forms-sample">
          <input type="hidden" name="id" value="<?php echo $id ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <!-- --> 
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha de operación: Fecha de recepción de recursos</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_fecha_opera()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
              
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" value="<?php echo $referencia ?>" placeholder="Ingrese número de referencia">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group" style="display:none">
              <label>Monto de la factura sin IVA ni accesorios</label>
              <input type="text" id="monto_opera" class="form-control monto_opera_1" onchange="tipo_modeda(1)" placeholder="Ingrese el monto total de la operación ($)" value="<?php echo $monto_opera ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-4 form-group" style="display:none">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" value="<?php echo $num_factura ?>" placeholder="Ingrese número de factura">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <!-- -->
          <div class="row">
            <?php if($aviso==1){ ?>
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>

            <div class="col-md-2 form-group">
            </div>

            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div>   
            </div>
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="idanexo4" id="idanexo4" class="form-control" value="<?php echo $idanexo4 ?>">
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div>   
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90"><?php echo $descrip_modifica ?></textarea>
            </div>
            <?php }?>
          </div>   
          <!----------->
          <hr class="subsubtitle">
          <div class="row">
            <div class="col-md-6 form-group">
              <label>Tipo de operación:</label>
              <select name="tipo_operacion" id="tipo_operacion" class="form-control" onchange="selecionar_ganrantica()">
              <option value="401" <?php if($tipo_operacion==401) echo 'selected' ?>>Otorgamiento de Mutuo, Préstamo o Crédito sin Garantía</option>
              <option value="402" <?php if($tipo_operacion==402) echo 'selected' ?>>Otorgamiento de Mutuo, Préstamo o Crédito con Garantía</option>
            </select>
            </div>
          </div>
          <div class="datos_garante" style="display: none;">
            <hr class="subsubtitle">
            <h5>Datos de la garantia</h5>
            <div class="row">
              <div class="col-md-6 form-group">
                <label>Tipos de bien objeto de la garantia:</label>
                <input type="hidden" id="tipo_bien_aux" value="<?php echo $tipo_bien ?>">
                <select class="form-control" name="tipo_bien" id="tipo_bien" onchange="tipo_bine_otros()">
                  <option value="2">Inmueble</option>
                  <option value="3">Vehículo terrestre</option>
                  <option value="4">Vehículo aéreo</option>
                  <option value="5">Vehículo marítimo</option>
                  <option value="6">Piedras Preciosas</option>
                  <option value="7">Metales Preciosos</option>
                  <option value="8">Joyas o relojes</option>
                  <option value="9">Obras de arte o antigüedades</option>
                  <option value="10">Acciones o partes sociales</option> 
                  <option value="11">Derechos fiduciarios</option> 
                  <option value="12">Derechos de crédito</option> 
                  <option value="15">Garantía Quirografaria</option> 
                  <option value="99">Otro (Especificar)</option> 
                </select>
              </div>
            </div>
            <div class="tipo_bien_text">
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Tipo de inmueble:</label>
                  <select name="tipo_inmueble" class="form-control">
                    <?php foreach ($inmueble->result() as $item){ ?>
                      <option value="<?php echo $item->idinmueble?>" <?php if($tipo_inmueble==$item->idinmueble) echo 'selected' ?>><?php echo $item->nombre?></option>   
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  <label>Valor de referencia:</label>
                  <input class="form-control" type="text" name="valor_referencia" id="valor_referencia" onchange="numero_decimal()" value="<?php echo $valor_referencia ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Código postal del inmuble:</label>
                  <input class="form-control" type="text" name="codigo_postal_inmuble" value="<?php echo $codigo_postal_inmuble ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Folio real del inmuble o antecedentes registrales:</label>
                  <input class="form-control" type="text" name="folio_inmpuble" value="<?php echo $folio_inmpuble ?>">
                </div>
              </div>
            </div>
            <div class="tipo_bien_text_otros" style="display: none">
              <div class="row">
                 <div class="col-md-12 form-group">
                  <label>Descripción de garantia:</label>
                  <input class="form-control" type="text" name="descripcion_garatia" value="<?php echo $descripcion_garatia ?>">
                </div>
              </div>  
            </div> 
          </div> 
          
          <div class="datos_garante2">
            <hr class="subsubtitle">
            <h5>Datos del garante</h5>
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipopersona" id="tipo1" value="1" onclick="tipo_persona(1)" <?php if($tipopersona==1) echo 'checked'?>>
                    Persona fisica:
                  </label>
                </div>
              </div>
            </div>

            <div class="text_tipo1" style="display: none">
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Nombre(s):</label>
                  <input class="form-control" type="text" name="nombre_f" value="<?php echo $nombre_f ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno:</label>
                  <input class="form-control" type="text" name="app_paternof" value="<?php echo $app_paternof ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno:</label>
                  <input class="form-control" type="text" name="app_maternof" value="<?php echo $app_maternof ?>">
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Fecha de nacimiento:</label>
                  <input class="form-control" type="date" name="fecha_nacimientof" value="<?php echo $fecha_nacimientof ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>R.F.C:</label>
                  <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_f" value="<?php echo $rfc_f ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>CURP:</label>
                  <input class="form-control" onblur="ValidCurp(this)" type="text" name="curpf" value="<?php echo $curpf ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 form-group">    
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipopersona" id="tipo2" value="2" onclick="tipo_persona(2)" <?php if($tipopersona==2) echo 'checked'?>>
                    Persona moral:
                  </label>
                </div>
              </div>
            </div>
            <div class="text_tipo2" style="display: none">
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Denomincación o Razón social:</label>
                  <input class="form-control" type="text" name="razon_socialm" value="<?php echo $razon_socialm ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Fecha de constitución:</label>
                  <input class="form-control" type="date" name="fecha_constitucionm" value="<?php echo $fecha_constitucionm ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>R.F.C:</label>
                  <input class="form-control" type="text" name="rfc_m" value="<?php echo $rfc_m ?>">
                </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-6 form-group">    
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipopersona" id="tipo3" value="3" onclick="tipo_persona(3)" <?php if($tipopersona==3) echo 'checked'?>>
                    Persona Fideicomiso:
                  </label>
                </div>
              </div>
            </div>
            <div class="text_tipo3" style="display: none">
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Denomincación o Razón social del fiduciario:</label>
                  <input class="form-control" type="text" name="razon_socialfi" value="<?php echo $razon_socialfi ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>R.F.C:</label>
                  <input class="form-control" type="text" name="rfc_fi" value="<?php echo $rfc_fi ?>">
                </div>
                <div class="col-md-4 form-group">
                  <label>Número, referencia o identificador del fideicomiso:</label>
                  <input class="form-control" type="text" name="referencia_fi" value="<?php echo $referencia_fi ?>">
                </div>
              </div>
            </div>
          </div>
        </form>
          <hr class="subsubtitle">
          <h5>Datos de la forma de otorgamiento del crédito</h5>
          <input type="hidden" id="fecha_max" value="<?php echo date("Y-m-d")?>">
          <div class="liquidacion_pago">
            <div class="liquidacion_pago_text">
            </div>
            <h3 class="monto_divisa" style="color: red"></h3>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total de crédito</label>
              <input readonly="" class="form-control" type="text" id="total_liquida" value="$0.00">
            </div>
            <div class="col-md-8 form-group">
              <br><br>
              <h5 class="validacion_cantidad" style="color: red"></h5>
            </div>
          </div>
          <div class="row"> 
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total en efectivo</label>
              <input readonly="" class="form-control" type="text" id="total_liquida_efect" value="$0.00">
              <div class="col-md-8" align="center">
              <br><br>
                <h5 class="limite_efect_msj" style="color: red"></h5>

              </div>
            </div>
          </div>
      <div class="modal-footer">
        <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
        <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
      </div>
      <div id="settings-trigger" style="width: 204px; left: 70px;" class="settings-trigger2"> 
          <button type="button" id="agregar_liquida" class="btn gradient_nepal2" onclick="agregar_liqui()"><i class="fa fa-plus"></i> Agregar Disposición</button>
        </div>

    </div>
  </div>
</div>

<!-- Operaciones -->
<!--------------Modal-------------->
<div class="modal fade" id="modal_fecha_opera" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Fecha de Operación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Indicar la fecha de suscripción del contrato, instrumento o título de crédito correspondiente.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--------------Modal-------------->
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 carácteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- / -->
<!------ Modificatorio ------->
<!--- --->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- < / >--->