<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h3>Perfilamiento del cliente</h3>
      	<form id="infoGen">
      		<div class="row">
      			<div class="col-md-5 form-group">
      				<label>Fecha de transacción</label>
      				<input class="form-control" type="date" name="" value="">
      			</div>
      			<div  class="col-md-7 form-group">
      				<label>Alerta</label>
      				<select class="form-control">
      					<option>Sin alerta</option>
      					<option>Con alerta</option>
      				</select>
      			</div>
      			<div class="col-md-12 form-group">
      				<label>Detalle alerta</label>
      				<textarea class="form-control" rows="5"></textarea>
      			</div>
      		</div>
      	</form>
      	<div class="row">
      		<div class="col-md-6">
      			<div class="row">
      				<div class="col-md-12">
      					<h3>Cliente</h3>
      				</div>
      				<div class="col-md-3">
      					<label>Nombre</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>Apellido Paterno</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>Apellido Materno</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-5">
      					<label>Nombre de identificación</label>
      				</div>
      				<div class="col-md-7">
      					<label></label>
      				</div>
      				<div class="col-md-5">
      					<label>Autoridad que emita la identificación</label>
      				</div>
      				<div class="col-md-7">
      					<label></label>
      				</div>
      				<div class="col-md-5">
      					<label>Número de identificación</label>
      				</div>
      				<div class="col-md-7">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>RFC</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      				<div class="col-md-3">
      					<label>CURP</label>
      				</div>
      				<div class="col-md-9">
      					<label></label>
      				</div>
      			</div>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-md-6">
      			<div class="row">
      				<div class="col-md-12 form-group">
      					<label>Tipo de operación</label>
      					<select class="form-control">
      						<option value="0"></option>
      						<option value="1">Venta de boletos/fichas/recibos u otros instrumentos de juego similares</option>
      						<option value="2">Consumo en Tarjetas de crédito o servicio</option>
      						<option value="3">Comercializacion de Tarjetas Prepagadas(Carga o recarga)</option>
      						<option value="4">Abono de recursos por devoluciones</option>
      						<option value="5">Venta de Cheques de viajero</option>
      						<option value="6">Otorgamiento de Mutuo, Préstamo o Crédito con Garantia</option>
      						<option value="7">Compra Venta de Inmuebles</option>
      					</select>
      				</div>
      			</div>
      		</div>
      	</div>
      	<div class="row" id="contenedor-1">
      		<div class="col-md-6">
      			<form id="form-1">
      				<div class="row">
      					<div class="col-md-12 form-group">
      						<label>Linea de negocio</label>
      						<select class="form-control">
      							<option>Hipódromo</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Medio empleado</label>
      						<select class="form-control">
      							<option>Presencial</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Instrumento monetario con el que se realizo la operación</label>
      						<select class="form-control">
      							<option>Efectivo</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de moneda o divisa</label>
      						<select class="form-control">
      							<option>Peso Mexicano</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Monto de la transacción</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de moneda o divisa</label>
      						<select class="form-control">
      							<option>Peso Mexicano</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Valor del Bien</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de bien</label>
      						<select class="form-control">
      							<option>inmueble</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de inmueble</label>
      						<select class="form-control">
      							<option>Casa/ Casa en condominio</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Código Postal del Inmueble</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Folio del inmueble</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      				</div>
      			</form>
      		</div>
      	</div>
      	<div class="row" id="contenedor-2">
      		<div class="col-md-6">
      			<form id="form-2">
      				<div class="row">
      					<div class="col-md-12 form-group">
      						<label>Numero de Tarjeta, Cuenta, Contrato o Identificador</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Monto Total del gasto en el periodo</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      				</div>
      			</form>
      		</div>
      	</div>
      	<div class="row" id="contenedor-3">
      		<div class="col-md-6">
      			<form id="form-3">
      				<div class="row">
      					<div class="col-md-12 form-group">
      						<label>Cantidad de Tarjetas o cupones comercializados</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Instrumento monetario con el que se realizo la operación</label>
      						<select class="form-control">
      							<option>Efectivo</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de moneda o divisa</label>
      						<select class="form-control">
      							<option>Peso Mexicano</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Monto de la transacción</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      				</div>
      			</form>
      		</div>
      	</div>
      	<div class="row" id="contenedor-4">
      		<div class="col-md-6">
      			<form id="form-4">
      				<div class="row">
      					<div class="col-md-12 form-group">
      						<label>Cantidad de Tarjetas</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de moneda o divisa</label>
      						<select class="form-control">
      							<option>Peso Mexicano</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Monto de la transacción</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      				</div>
      			</form>
      		</div>
      	</div>
      	<div class="row" id="contenedor-5">
      		<div class="col-md-6">
      			<form id="form-5">
      				<div class="row">
      					<div class="col-md-12 form-group">
      						<label>Numero de cheques de viajero</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Instrumento monetario con el que se realizo la operación</label>
      						<select class="form-control">
      							<option>Efectivo</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de moneda o divisa</label>
      						<select class="form-control">
      							<option>Peso Mexicano</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Monto de la transacción</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      				</div>
      			</form>
      		</div>
      	</div>
      	<div class="row" id="contenedor-6">
      		<div class="col-md-12">
      			<form id="form-6">
      				<div class="row col-md-6">
      					<div class="col-md-12 form-group">
      						<label>Tipo de bien objeto de la garantia</label>
      						<select class="form-control">
      							<option>Inmueble</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de inmueble</label>
      						<select class="form-control">
      							<option>Casa/Casa en condominio</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Valor de referencia</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Código postal del inmueble</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Folio del inmueble</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-12">
      						<hr class="subsubtitle">
      						<h5>Datos de la garantia</h5>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Descripcion de la garantia</label>
      						<textarea class="form-control" rows="5"></textarea>
      					</div>
      				</div>
      				<div class="row">
      					<div class="col-md-12">
      						<hr class="subsubtitle">
      						<h5>Datos del garante</h5>
      					</div>
      					<div class="col-md-6">
      						<div class="row">
			             <div class="col-md-12 form-group">
			              <div class="form-check form-check-success">
			                <label class="form-check-label">
			                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio1">
			                  Persona Fisica
			                </label>
			              </div>
			             </div>
			             <div class="col-md-12 form-group">
			              <div class="form-check form-check-success">
			                <label class="form-check-label">
			                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
			                  Persona Moral
			                </label>
			              </div>
			             </div>
			             <div class="col-md-12 form-group">
			              <div class="form-check form-check-success">
			                <label class="form-check-label">
			                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio3">
			                  Fideicomiso
			                </label>
			              </div>
			             </div>
			            </div>
      					</div>
      					<div class="row" id="PFisicaF6">
      						<div class="col-md-12 form-group">
      							<label>Nombre</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>Apellido Paterno</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>Apellido Materno</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-4 form-group">
      							<label>Fecha de nacimiento</label>
      							<input class="form-control" type="date" name="" value="">
      						</div>
      						<div class="col-md-4 form-group">
      							<label>RFC</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-4 form-group">
      							<label>CURP</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      					</div>
      					<div id="PMoralF6" class="row">
      						<div class="col-md-12 form-group">
      							<label>Razon social</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>Fecha de constitución</label>
      							<input class="form-control" type="date" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>RFC</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      					</div>
      					<div id="FidecoF6" class="row">
      						<div class="col-md-12 form-group">
      							<label>Razon social del fiduciario</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>RFC del fideicomiso</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      					</div>
      				</div>
      				<div class="row">
      					<div class="col-md-6">
      						<hr class="subsubtitle">
      						<h5>Datos del otorgamiento del credito</h5>
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Fecha de disposición</label>
      						<input class="form-control" type="date" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Instrumento monetario con el que se realizo la operación</label>
      						<select class="form-control">
      							<option>Efectivo</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de moneda o divisa</label>
      						<select class="form-control">
      							<option>Peso Mexicano</option>
      						</select>
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Monto de la transacción</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      				</div>
      			</form>
      		</div>
      	</div>
      	<div class="row" id="contenedor-7">
      		<div class="col-md-12">
      			<form id="form-7">
      				<div class="row">
      					<div class="col-md-6 form-group">
      						<label>Figura del cliente reportado</label>
      						<select class="form-control">
      							<option>Vendedor</option>
      						</select>
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Tipo de inmueble</label>
      						<select class="form-control">
      							<option>Casa/Casa en condominio</option>
      						</select>
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Valor de referencia</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Codigo postal del inmueble</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Folio del inmueble</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      					<div class="col-md-6">
      						<hr class="subsubtitle">
      						<h5>Datos de la garantia</h5>
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Descripcion de la garantia</label>
      						<textarea class="form-control" rows="5" name=""></textarea>
      					</div>
      				</div>
      				<div class="row">
      					<div class="col-md-12">
      						<hr class="subsubtitle">
      						<h5>Datos del garante</h5>
      					</div>
      					<div class="col-md-6">
      						<div class="row">
			             <div class="col-md-12 form-group">
			              <div class="form-check form-check-success">
			                <label class="form-check-label">
			                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio1">
			                  Persona Fisica
			                </label>
			              </div>
			             </div>
			             <div class="col-md-12 form-group">
			              <div class="form-check form-check-success">
			                <label class="form-check-label">
			                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio2">
			                  Persona Moral
			                </label>
			              </div>
			             </div>
			             <div class="col-md-12 form-group">
			              <div class="form-check form-check-success">
			                <label class="form-check-label">
			                  <input type="radio" class="form-check-input" name="BenReal" id="ExampleRadio3">
			                  Fideicomiso
			                </label>
			              </div>
			             </div>
			            </div>
      					</div>
      					<div class="row" id="PFisicaF7">
      						<div class="col-md-12 form-group">
      							<label>Nombre</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>Apellido Paterno</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>Apellido Materno</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-4 form-group">
      							<label>Fecha de nacimiento</label>
      							<input class="form-control" type="date" name="" value="">
      						</div>
      						<div class="col-md-4 form-group">
      							<label>RFC</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-4 form-group">
      							<label>CURP</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      					</div>
      					<div id="PMoralF7" class="row">
      						<div class="col-md-12 form-group">
      							<label>Razon social</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>Fecha de constitución</label>
      							<input class="form-control" type="date" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>RFC</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      					</div>
      					<div id="FidecoF7">
      						<div class="col-md-12 form-group">
      							<label>Razon social del fiduciario</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      						<div class="col-md-6 form-group">
      							<label>RFC del fideicomiso</label>
      							<input class="form-control" type="text" name="" value="">
      						</div>
      					</div>
      				</div>
      				<div class="row">
      					<div class="col-md-6">
      						<hr class="subsubtitle">
      						<h5>Datos del otorgamiento del credito</h5>
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Fecha de disposición</label>
      						<input class="form-control" type="date" name="" value="">
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Instrumento monetario con el que se realizo la operación</label>
      						<select class="form-control">
      							<option>Efectivo</option>
      						</select>
      					</div>
      					<div class="col-md-12 form-group">
      						<label>Tipo de moneda o divisa</label>
      						<select class="form-control">
      							<option>Peso Mexicano</option>
      						</select>
      					</div>
      					<div class="col-md-6 form-group">
      						<label>Monto de la transacción</label>
      						<input class="form-control" type="text" name="" value="">
      					</div>
      				</div>
      			</form>
      		</div>
      	</div>
      </div>
    </div>
  </div>
</div>