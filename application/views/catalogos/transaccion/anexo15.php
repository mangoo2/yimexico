<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="anio_actual" hidden=""><?php echo date('Y'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<?php if(isset($a->id)){
  echo '<input type="hidden" id="id_ax15" value="'.$a->id.'">';   
}else{
  echo '<input type="hidden" id="id_ax15" value="0">';
} ?>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h4>Anexo 15 - Arrendamiento de inmueble</h4>  
        <h3>Registro de transacción</h3>
        <hr class="subtitle">
        <input type="hidden" id="aux_pga" value="<?php echo $aux_pga ?>">
        <form id="form_anexo15" class="forms-sample">
          <?php if(isset($a->id)){ ?>
            <input type="hidden" name="id" id="id" value="<?php echo $a->id ?>">
          <?php } ?>
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <div class="row">
            <div class="col-md-4 form-group">
              <label> Fecha de operación: Fecha de recepción de recursos</label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" <?php if(isset($a->fecha_operacion)) echo "value='$a->fecha_operacion'"; ?>>
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" <?php if(isset($a->referencia)) echo "value='$a->referencia'"; ?> placeholder="Ingrese número de referencia">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group">
              <label><!--Monto de factura-->Monto de la factura sin IVA ni accesorios</label>
              <input type="text" id="monto_opera" class="form-control" onchange="tipo_modeda()" <?php if(isset($a->monto_opera)) echo "value='$a->monto_opera'"; ?> placeholder="Ingrese el monto total de la operación ($)" required="">
            </div>
          </div>
          <div class="row">  
            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_aux" value="<?php if(isset($a->anio_acuse)) echo $a->anio_acuse; ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" <?php if(isset($a->num_factura)) echo "value='$a->num_factura'"; ?> placeholder="Ingrese número de factura">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <!-- -->
          <hr class="subsubtitle">
          <div class="row"> 
            <?php if($aviso==1){ ?>
              <div class="col-md-12">
              </div>
              <div class="col-md-10 form-group">
                <h5>Datos de la modificación:</h5>
              </div>

              <div class="col-md-2 form-group">
              </div>

              <div class="col-md-3 form-group">
                <label>Referencia modificación:</label>
                <div class="d-flex mb-2">  
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica" <?php if(isset($a->referencia_modifica)) { echo "value='$a->referencia_modifica'";  } ?> placeholder="Referencia de modificación">
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button>   
                </div>   
              </div>
              <div class="col-md-3 form-group">
                <label>Folio de aviso que se modificará:</label>
                <input type="hidden" name="id_anexo15" id="id_anexo15" class="form-control" <?php if(isset($a->id)) echo "value='$a->id'"; ?> >
                <div class="d-flex mb-2">  
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio" <?php if(isset($a->folio_modificatorio)) echo "value='$a->folio_modificatorio'"; ?>>
                  </div>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                    <i class="mdi mdi-help"></i>
                  </button>   
                </div>   
              </div>
              <div class="col-md-6 form-group">
                <label>Descripción de la modificación:</label>
                <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90" <?php if(isset($a->descrip_modifica)) echo "value='$a->descrip_modifica'";?> > <?php if(isset($a->descrip_modifica)) echo $a->descrip_modifica; ?> </textarea>
              </div>
              <div class="col-md-12">
                <hr class="subsubtitle">
              </div>
            <?php }?>

          </div>
          <div class="row">
            <div class="col-md-6 form-group">
              <label>Fecha inicio del arrendamiento liquidado</label>
              <input class="form-control" type="date" name="fecha_arrenda" max="<?php echo date("Y-m-d")?>" <?php if(isset($a->fecha_arrenda)) echo "value='$a->fecha_arrenda'"; ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Fecha de término del arrendamiento liquidado</label>
              <input class="form-control" type="date" name="fecha_arrenda_fin" <?php if(isset($a->fecha_arrenda_fin)) echo "value='$a->fecha_arrenda_fin'"; ?>>
            </div>
          </div>
          <h5>Datos del inmueble objeto del arrendamiento</h5>
          <div class="row">  
            <div class="col-md-10 form-group">
              <label>Inmuebles del cliente:</label>
              <select class="form-control" id="inmuebles" name="id_inmueble_cli">
                <option disabled selected>Selecciona un imueble</option>
                  <?php foreach ($inmue_cli as $item){ ?>
                    <option value="<?php echo $item->id ?>" <?php if(isset($a) && $item->id==$a->id_inmueble_cli) echo 'selected' ?>><?php echo $item->calle_avenida." ".$item->num_ext." No. Int. ".$item->num_int." Colonia ".$item->colonia." C.P. ".$item->cp ?></option> 
                  <?php } ?>
              </select>
            </div>

            <div class="col-md-6 form-group">
              <label>Tipo de inmueble</label>
              <select class="form-control" name="tipo_inmueble" id="tipo_inmueble">
                <option value="1" <?php if(isset($a) && $a->tipo_inmueble=="1"){ echo "selected"; }?>>Casa/casa en condominio</option>
                <option value="2" <?php if(isset($a) && $a->tipo_inmueble=="2"){ echo "selected"; }?>>Departamento</option>
                <option value="3" <?php if(isset($a) && $a->tipo_inmueble=="3"){ echo "selected"; }?>>Edificio habitacional</option>
                <option value="4" <?php if(isset($a) && $a->tipo_inmueble=="4"){ echo "selected"; }?>>Edificio comercial</option>
                <option value="5" <?php if(isset($a) && $a->tipo_inmueble=="5"){ echo "selected"; }?>>Edificio oficinas</option>
                <option value="6" <?php if(isset($a) && $a->tipo_inmueble=="6"){ echo "selected"; }?>>Local comercial independiente</option>
                <option value="7" <?php if(isset($a) && $a->tipo_inmueble=="7"){ echo "selected"; }?>>Local en centro comercial</option>
                <option value="8" <?php if(isset($a) && $a->tipo_inmueble=="8"){ echo "selected"; }?>>Oficina</option>
                <option value="9" <?php if(isset($a) && $a->tipo_inmueble=="9"){ echo "selected"; }?>>Bodega comercial</option>
                <option value="10" <?php if(isset($a) && $a->tipo_inmueble=="10"){ echo "selected"; }?>>Bodega industrial</option>
                <option value="11" <?php if(isset($a) && $a->tipo_inmueble=="11"){ echo "selected"; }?>>Nave Industrial</option>
                <option value="12" <?php if(isset($a) && $a->tipo_inmueble=="12"){ echo "selected"; }?>>Terreno urbano habitacional</option>
                <option value="13" <?php if(isset($a) && $a->tipo_inmueble=="13"){ echo "selected"; }?>>Terreno no urbano habitacional</option>
                <option value="14" <?php if(isset($a) && $a->tipo_inmueble=="14"){ echo "selected"; }?>>Terreno urbano comercial o industrial</option>
                <option value="15" <?php if(isset($a) && $a->tipo_inmueble=="15"){ echo "selected"; }?>>Terreno no urbano comercial o industrial</option>
                <option value="16" <?php if(isset($a) && $a->tipo_inmueble=="16"){ echo "selected"; }?>>Terreno ejidal</option>
                <option value="17" <?php if(isset($a) && $a->tipo_inmueble=="17"){ echo "selected"; }?>>Rancho/Hacienda/Quinta</option>
                <option value="18" <?php if(isset($a) && $a->tipo_inmueble=="18"){ echo "selected"; }?>>Huerta</option>
                <option value="99" <?php if(isset($a) && $a->tipo_inmueble=="99"){ echo "selected"; }?>>Otro</option>

              </select>
            </div>
            <div class="col-md-6 form-group">
              <label>Indicar: valor avalúo, valor comercial o valor catastral</label>
              <input class="form-control" type="text" name="valor_inmueble" id="valor_inmueble" <?php if(isset($a->valor_inmueble)) echo "value='$a->valor_inmueble'"; ?>>
            </div>
            <div class="col-md-2 form-group">
              <label>Código postal</label>
              <input class="form-control" pattern='\d*' maxlength="5" type="text" name="cp" id="cp" <?php if(isset($a->cp)) echo "value='$a->cp'"; ?>>
            </div>
            <div class="col-md-4 form-group">
              <label>Calle, avenida o vía de la ubicación del inmueble</label>
              <input class="form-control" type="text" name="calle" id="calle" <?php if(isset($a->calle)) echo "value='$a->calle'"; ?>>
            </div>
            <div class="col-md-1 form-group">
              <label>No. Ext</label>
              <input class="form-control" type="text" name="no_ext" id="no_ext" <?php if(isset($a->no_ext)) echo "value='$a->no_ext'"; ?>>
            </div>
            <div class="col-md-1 form-group">
              <label>No. Int</label>
              <input class="form-control" type="text" name="no_int" id="no_int" <?php if(isset($a->no_int)) echo "value='$a->no_int'"; ?>>
            </div>
            <div class="col-md-4 form-group">
              <label>Colonia de la ubicación del inmueble</label>
              <!--<input class="form-control" type="text" name="colonia" id="colonia" <?php if(isset($a->colonia)) echo "value='$a->colonia'"; ?>>-->
              <select onclick="autoComplete()" class="form-control" name="colonia" id="colonia">
                <option value=""></option>
                <?php if(isset($a->colonia) && $a->colonia!=""){
                  echo '<option value="'.$a->colonia.'" selected>'.$a->colonia.'</option>';      
                } ?>  
              </select>
            </div>
            
            <div class="col-md-6 form-group">
              <label>Folio real del inmueble o antecedentes catastrales</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio" id="folio" <?php if(isset($a->folio)) echo "value='$a->folio'"; ?>>
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_folio_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <?php /*
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Fecha de pago</label>
              <input class="form-control" type="date" name="fecha_pago" <?php if(isset($a->fecha_pago)) echo "value='$a->fecha_pago'"; ?>>
            </div>
            <div class="col-md-4 form-group">
              <label>Forma de pago</label>
              <select name="forma_pago" class="form-control">
                <option value="1" <?php if(isset($a) && $a->forma_pago=="1"){ echo "selected"; } ?>>Contado</option>
                <option value="2" <?php if(isset($a) && $a->forma_pago=="2"){ echo "selected"; } ?>>Diferido</option>
                <option value="3" <?php if(isset($a) && $a->forma_pago=="3"){ echo "selected"; } ?>>Dación en pago</option>
                <option value="4" <?php if(isset($a) && $a->forma_pago=="4"){ echo "selected"; } ?>>Préstamo o crédito</option>
                <option value="5" <?php if(isset($a) && $a->forma_pago=="5"){ echo "selected"; } ?>>Permuta</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Instrumento monetario con el que se realizo la operación o acto</label>
              <select name="instrum_notario" class="form-control">
                <option value="1" <?php if(isset($a) && $a->instrum_notario=="1"){ echo "selected"; } ?>>Efectivo</option>
                <option value="2" <?php if(isset($a) && $a->instrum_notario=="2"){ echo "selected"; } ?>>Tarjeta de Crédito</option>
                <option value="3" <?php if(isset($a) && $a->instrum_notario=="3"){ echo "selected"; } ?>>Tarjeta de Debito</option>
                <option value="4" <?php if(isset($a) && $a->instrum_notario=="4"){ echo "selected"; } ?>>Tarjeta de Prepago</option>
                <option value="5" <?php if(isset($a) && $a->instrum_notario=="5"){ echo "selected"; } ?>>Cheque Nominativo</option>
                <option value="6" <?php if(isset($a) && $a->instrum_notario=="6"){ echo "selected"; } ?>>Cheque de Caja</option>
                <option value="7" <?php if(isset($a) && $a->instrum_notario=="7"){ echo "selected"; } ?>>Cheques de Viajero</option>
                <option value="8" <?php if(isset($a) && $a->instrum_notario=="8"){ echo "selected"; } ?>>Transferencia Interbancaria</option>
                <option value="9" <?php if(isset($a) && $a->instrum_notario=="9"){ echo "selected"; } ?>>Transferencia Misma Institución</option>
                <option value="10" <?php if(isset($a) && $a->instrum_notario=="10"){ echo "selected"; } ?>>Transferencia Internacional</option>
                <option value="11" <?php if(isset($a) && $a->instrum_notario=="11"){ echo "selected"; } ?>>Orden de Pago</option>
                <option value="12" <?php if(isset($a) && $a->instrum_notario=="12"){ echo "selected"; } ?>>Giro</option>
                <option value="13" <?php if(isset($a) && $a->instrum_notario=="13"){ echo "selected"; } ?>>Oro o Platino Amonedados</option>
                <option value="14" <?php if(isset($a) && $a->instrum_notario=="14"){ echo "selected"; } ?>>Plata Amonedada</option>
                <option value="15" <?php if(isset($a) && $a->instrum_notario=="15"){ echo "selected"; } ?>>Metales Preciosos</option>
                <option value="16" <?php if(isset($a) && $a->instrum_notario=="16"){ echo "selected"; } ?>>Activos Virtuales</option>
                <option value="17" <?php if(isset($a) && $a->instrum_notario=="17"){ echo "selected"; } ?>>Otros</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Tipo de moneda o divisa de la operacion</label>
              <select id="tipo_moneda" class="form-control" name="tipo_moneda">
                <?php foreach ($tm->result() as $t) {
                $select="";
                if(isset($a) && $a->tipo_moneda==$t->id){ $select="selected"; }
                  echo "<option value='$t->id'$select>$t->moneda</option>";
                } ?>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Monto de la operación o acto</label>
              <input class="form-control monto" type="text" name="monto_opera" id="monto" <?php if(isset($a->monto_opera)) echo "value='$a->monto_opera'"; ?>>
            </div>
          </div>
          */ ?>
          <div class="row">
            <div class="col-md-12">
              <hr class="subsubtitle">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h4>Datos de liquidación</h4>
            </div>
          </div>
          <input type="hidden" id="fecha_max" value="<?php echo date("Y-m-d")?>">
          <div class="liquidacion_pago">
            <div class="liquidacion_pago_text">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total de la liquidación</label>
              <input readonly="" class="form-control" type="text" id="total_liquida" value="$0.00">
            </div>
            <div class="col-md-8 form-group">
              <br><br>
              <h5 class="validacion_cantidad" style="color: red"></h5>
            </div>
          </div>
          <div class="row"> 
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total en efectivo</label>
              <input readonly="" class="form-control" type="text" id="total_liquida_efect" value="$0.00">
            </div>
          </div>
        </form>
        <div class="modal-footer">
            <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="guardar_anexo15()"><i class="fa fa-save"></i> Registrar Transacción</button>
            <a class="btn gradient_nepal2 regresar" href="#" id="regresa" type=""><i class="fa fa-arrow-left"></i> Regresar</a>
          </div>
          <div id="settings-trigger" style="width: 204px; left: 70px;" class="settings-trigger2"> 
          <button type="button" id="agregar_liquida" class="btn gradient_nepal2" onclick="agregar_liqui()"><i class="fa fa-plus"></i> Agregar Liquidación</button>
        </div>
    	</div>
    </div>
	</div>
</div>
<!--------------Modal-------------->
<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 caracteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_folio_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio real del inmueble o antecedentes catastrales</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>No dejar espacios en blanco, separar caracteres con guión medio o bajo.</li>
           <li>Capturar el folio real físico o electrónico del inmueble, en caso de no contar con uno, se deben capturar los datos de los antecedentes registrales, si no se cuenta con lo anterior, capturar XXXX.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!------ Modificatorio ------->
<!--- --->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- < / >--->