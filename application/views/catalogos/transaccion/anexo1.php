<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #24284b;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<div class="row">
      <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h4>Anexo 1 - Práctica de juegos con apuesta, concursos y sorteos.</h4>  
            <h3>Registro de transacción</h3>
          </div>
        </div>
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <input type="hidden" id="aux_pga" value="<?php echo $aux_pga; ?>">
        <input type="hidden" id="id_ax1" value="<?php echo $id; ?>">
        <form id="form_anexo1" class="forms-sample">
          <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Fecha de operación o acto </label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" value="<?php echo $fecha_operacion ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" name="referencia" id="referencia" class="form-control" placeholder="Ingrese número de referencia" required="" value="<?php echo $referencia ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>
              </div>
            </div>
            <div class="col-md-3 form-group">
              <label>Monto de factura</label>
              <input type="text" id="monto_opera" class="form-control" onblur="SetNformat(this)" placeholder="Ingrese el monto total de la operación ($)" name="monto_opera" required="" value="<?php echo $monto_opera ?>">
            </div>
            <div class="col-md-3 form-group">
            </div>
            <div class="col-md-3 form-group">
              <label>Año acuse:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes acuse:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" name="num_factura" id="num_factura" class="form-control" placeholder="Ingrese número de factura" value="<?php echo $num_factura ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <div class="row">
            <?php if($aviso==1){ ?>
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>

            <div class="col-md-2 form-group">
            </div>

            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica"  value="<?php echo $referencia_modifica ?>" placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div> 
            </div>
            
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="idanexo1" id="idanexo1" class="form-control" value="<?php echo $idanexo1 ?>">
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio"  value="<?php echo $folio_modificatorio ?>">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help text-dark"></i>
                </button>   
              </div> 
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="70"><?php echo $descrip_modifica ?></textarea>
            </div>
            <?php }?>
          </div>
        <!-- -->
        <hr class="subsubtitle">
        <div class="row">
          <div class="col-md-6 form-group">
            <label>Tipo de operación o acto:</label>
            <select class="form-control" name="tipo_operacion">
           <option value="101">Venta de boletos /fichas /recibos u otros instrumentos de juego similares</option>
            <option value="102">Pago de boletos /fichas /recibos u otros instrumentos de juego similares</option>
             <option value="103">Pago de premios</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 form-group">
            <label>Linea de negocio en la que se llevó a cabo la operación o acto:</label>
            <select class="form-control" name="linea_negocio">
              <option value="1">Hipódromo</option>
              <option value="2">Galgódromo</option>
              <option value="3">Frontones</option>
              <option value="4">Centros de Apuesta Remotas (libros foráneos)</option>
              <option value="5">Salas de Sorteos de números</option>
              <option value="6">Cruce de apuestas en ferias</option>
              <option value="7">Carreras de caballos en escenarios temporales</option>
              <option value="8">Peleas de gallos en escenarios temporales</option>
              <option value="9">Sorteos</option>
              <option value="10">Concursos</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 form-group">
            <label>Medio empleado para realizar la operación o acto:</label>
            <select class="form-control" name="medio_operacion">
              <option value="1">Presencial</option>
              <option value="2">Internet</option>
              <option value="3">Telefónica-voz</option>
              <option value="4">Telefónica-mensajedetexto</option>
              <option value="5">Otro</option>
            </select>
          </div>
        </div>
      </form>
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-4 form-group">
            <label>Datos de liquidación de la operación o acto:</label>
          </div>
        </div>
        <div id="form-principal">
        <form id="form-1" class="formita">
          <div class="row">
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" value="1" onclick="EnabledSelctN(this)" name="tipo_liquidacion" id="tln-1" checked>
                  Liquidación numeraria
                </label>
              </div>
            </div>
          </div>
          <div class="row" id="numeraria">
            <div class="col-md-3 form-group">
              <label>Fecha de pago</label>
              <input class="form-control fechaPago" onchange="validarF(this)" type="date" name="fecha_pago" value="">
            </div>
            <div class="col-md-6 form-group">
              <label>Instrumento monetario con el que se realizó la operación o acto:</label>
              <select class="form-control" name="instrumento_monetario" onchange="confirmarPagosMonto()">
                <option value="1">Efectivo</option>
                <option value="2">Tarjeta de Crédito</option>
                <option value="3">Tarjeta de Debito</option>
                <option value="4">Tarjeta de Prepago</option>
                <option value="5">Cheque Nominativo</option>
                <option value="6">Cheque de Caja</option>
                <option value="7">Cheques de Viajero</option>
                <option value="8">Transferencia Interbancaria</option>
                <option value="9">Transferencia Misma Institución</option>
                <option value="10">Transferencia Internacional</option>
                <option value="11">Orden de Pago</option>
                <option value="12">Giro</option>
                <option value="13">Oro o Platino Amonedados</option>
                <option value="14">Plata Amonedada</option>
                <option value="15">Metales Preciosos</option>
                <option value="16">Activos Virtuales</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Tipo de moneda o divisa de la operación o acto</label>
              <select class="form-control" name="moneda" onchange="confirmarPagosMonto()">
                <?php /* foreach ($MonedasM as $key): ?>
                <option value="<?php echo $key->clave; ?>"><?php echo $key->moneda; ?></option>                  
                <?php endforeach */ ?>
                <option value="0">Pesos Mexicanos</option>
                <option value="1">Dólar Americano</option>
                <option value="2">Euro</option>
                <option value="3">Libra Esterlina</option>
                <option value="4">Dólar canadiense</option>
                <option value="5">Yuan Chino</option>
                <option value="6">Centenario</option>
                <option value="7">Onza de Plata</option>
                <option value="8">Onza de Oro</option>
              </select>
            </div>
            
            <div class="col-md-4 form-group">
              <labe>Monto de la operación o acto</label>
              <input class="form-control sumont" type="text" onchange="confirmarPagosMonto()" name="monto_operacion" onblur="SetNformat(this)" value="">
            </div>
            <div class="col-md-3">
              <br>
               <h3 class="monto_divisa" style="color: red"></h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" value="2" onclick="EnabledSelctE(this)" name="tipo_liquidacion" id="tle">
                  Liquidación en especie
                </label>
              </div>
            </div>
            <div class="col-md-4 form-group">
              <labe>Valor del bien de liquidación:</label>
              <input onblur="SetNformat(this)" class="form-control sumont" onchange="confirmarPagosMonto()" type="text" readonly name="valor_bien" value="">
            </div>
            <div class="col-md-4 form-group">
              <label>Tipo de moneda o divisa en la que esta expresado el valor del bien de liquidación:</label>
              <select class="form-control" disabled name="moneda" id="monedale">
                <?php /* foreach ($MonedasM as $key): ?>
                <option value="<?php echo $key->clave; ?>"><?php echo $key->moneda; ?></option>                  
                <?php endforeach */ ?>
                <option value="0">Pesos Mexicanos</option>
                <option value="1">Dólar Americano</option>
                <option value="2">Euro</option>
                <option value="3">Libra Esterlina</option>
                <option value="4">Dólar canadiense</option>
                <option value="5">Yuan Chino</option>
                <option value="6">Centenario</option>
                <option value="7">Onza de Plata</option>
                <option value="8">Onza de Oro</option>
              </select>
            </div>
            
            <div class="col-md-4 form-group">
              <label>Tipo de bien de la liquidación</label>
              <select class="form-control" disabled name="bien_liquidacion" onblur="TipoBien(this)">
                <option value="1">Inmueble</option>
                <option value="2">Vehículo terrestre</option>
                <option value="3">Vehículo aéreo</option>
                <option value="4">Vehículo marítimo</option>
                <option value="5">Piedras Preciosas</option>
                <option value="6">Metales Preciosos</option>
                <option value="7">Joyas o relojes</option>
                <option value="8">Obras de arete o antigüedades</option>
                <option value="99">Otro(Especificar)</option>
              </select>
            </div>
          </div>
          <div id="InfoInmueble-1" style="display: none">
            <div class="row">
              <div class="col-md-12"> 
                <label>Datos del inmueble</label>
              </div>
              <div class="col-md-4 form-group">
                <label>Tipo de inmueble</label>
                <select class="form-control" name="tipo_inmueble">
                 <option value="1">Casa /Casa en condominio</option>
                 <option value="2">Departamento</option>
                 <option value="3">Edificio habitacional</option>
                 <option value="4">Edificio comercial</option>
                 <option value="5">Edificio oficinas</option>
                 <option value="6">Local comercial independiente</option>
                 <option value="7">Local en centro comercial</option>
                 <option value="8">Oficina</option>
                 <option value="9">Bodega comercial</option>
                 <option value="10">Bodega industrial</option>
                 <option value="11">Nave Industrial</option>
                 <option value="12">Terreno urbano habitacional</option>
                 <option value="13">Terreno no urbano habitacional</option>
                 <option value="14">Terreno urbano comercial o industrial</option>
                 <option value="15">Terreno no urbano comercial o industrial</option>
                 <option value="16">Terreno ejidal</option>
                 <option value="17">Rancho/Hacienda/Quinta</option>
                 <option value="18">Huerta</option>
                 <option value="99">Otro</option>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label>Código postal de la ubicación del inmueble:</label>
                <input class="form-control" type="text" name="codigo_postal" value="">
              </div>
              <div class="col-md-4 form-group">
                <label>Folio real del inmueble o antecedentes registrales:</label>
                <input class="form-control" type="text" name="folio_real" value="">
              </div>
            </div>
          </div>
          <div id="TipoOtro-1" style="display: none">
            <div class="row">
              <div class="col-md-12 form-group">
                <label>Descripción</label>
                <textarea class="form-control" name="descripcion_bien_liquidacion"></textarea>
              </div>
            </div>
          </div>
          <div class="row">
          <div class="col-md-10"></div>
          <div class="col-md-2">
          <button type="button" id="But-1" class="btn gradient_nepal2" style="color:white" onclick="removerliquidacion(1)"><i class="fa fa-trash-o"></i> Quitar liquidación</button></div>
          <input type="hidden" value="0" name="idliq">
        </div>
          <!--<button type="button" class="btn gradient_nepal2" onclick="Clonar(0)"><i class="fa fa-plus"></i> Otra Liquidación</button>-->
        </form>
        </div>
        <div class="col-md-12" id="cloadas">
          <hr class="subsubtitle">
        </div>
        <div class="row">
           <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total de la liquidación</label>
              <input readonly class="form-control" type="text" id="total_liquida" value="">
            </div>
            <div class="col-md-8" align="center">
              <br><br>
              <h5 class="validacion_cantidad" style="color: red"></h5>

            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total en efectivo</label>
              <input readonly class="form-control" type="text" id="total_liquida_efect" value="">
              <div class="col-md-8" align="center">
              <br><br>
                <h5 class="limite_efect_msj" style="color: red"></h5>

              </div>
              
            </div>
        </div>
        <hr class="subtitle">
        <div class="modal-footer">
          <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="registrar()"><i class="fa fa-save"></i> Registrar Transacción</button>
          <button type="button" class="btn gradient_nepal2 regresar" onclick="regresar()"><i class="fa fa-arrow-left"></i> Regresar</button>
        </div>
        <div id="settings-trigger" style="width: 204px; left: 70px;" class="settings-trigger2"> 
          <button type="button" id="agregar_liquida" class="btn gradient_nepal2" onclick="Clonar(0)"><i class="fa fa-plus"></i> Agregar Liquidación</button>
        </div>
    </div>
  </div>
</div>
<!--modals de ayuda-->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
