<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #323556;
  }
</style>
<span class="status" hidden><?php echo $status ?></span>
<span class="mes_actual" hidden=""><?php echo date('m'); ?></span>
<span class="fecha_actual" hidden=""><?php echo date('Y-m-d'); ?></span>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <h4>Anexo 5B - Desarrollos Inmobiliarios</h4>
      	    <h3>Registro de transacción</h3> 
          </div>
        </div>  
        <hr class="subtitle">
        <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
        <form id="form_anexo5b" class="forms-sample">
          <input type="hidden" name="id" id="id_anexo" value="<?php echo $id ?>">
          <!--- --->
          <input type="hidden" id="idtransbene" name="id_bene_transac" class="form-control" value="<?php echo $idtransbene ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idperfilamiento_cliente ?>">
          <input type="hidden" name="id_cliente_cliente" class="form-control" value="<?php echo $idcliente_cliente ?>">
          <input type="hidden" name="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" class="form-control" value="<?php echo $idactividad ?>">
          <input type="hidden" name="aviso" id="aviso" value="<?php echo $aviso ?>">
          <input type="hidden" name="id_aviso"id="id_aviso" value="<?php echo $id_aviso ?>">
          <input type="hidden" id="id_union" value="<?php echo $id_union; ?>"> <!--agregado para sumatoria -->
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Fecha de operación o acto </label>
              <input type="date" max="<?php echo date("Y-m-d")?>" name="fecha_operacion" id="fecha_operacion" class="form-control" <?php if(isset($b->fecha_operacion)) echo "value='$b->fecha_operacion'"; ?> onkeydown="return false">
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" <?php if(isset($b->referencia)) echo "value='$b->referencia'"; ?> placeholder="Ingrese número de referencia">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
            <div class="col-md-3 form-group" style="display:none">
              <label>Monto de factura</label>
              <input type="text" name="monto_opera" id="monto_opera" class="form-control monto" placeholder="Ingrese el monto total de la operación ($)" <?php if(isset($b->monto_opera)) { $monto_opera = number_format($b->monto_opera,2,'.',','); echo "value='$monto_opera'"; } ?>>
            </div>

            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-4 form-group" style="display:none">
              <label>No. Factura:</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="num_factura" id="num_factura" <?php if(isset($b->num_factura)) echo "value='$b->num_factura'"; ?> placeholder="Ingrese número de factura">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_factura_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
          <!----------->
          <!-- -->
          <div class="row"> 
            <div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                      }
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $idcliente_cliente ?>
              </span>  
            </div>
          </div>
          <!-- PARA EL AVISO MODIFICATORIO -->
          <div class="row">
            <?php if($aviso==1){ ?>
            <div class="col-md-12">
            </div>
            <div class="col-md-10 form-group">
              <h5>Datos de la modificación:</h5>
            </div>
            <div class="col-md-3 form-group">
              <label>Referencia modificación:</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="referencia_modifica" id="referencia_modifica" <?php if(isset($b->referencia_modifica)) { echo "value='$b->referencia_modifica'"; } ?> placeholder="Referencia de modificación">
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_referenciam_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div> 
            </div>
            <div class="col-md-3 form-group">
              <label>Folio de aviso que se modificará:</label>
              <input type="hidden" name="id_anexo5b" id="id_anexo5b" class="form-control" <?php if(isset($a->id)) echo "value='$b->id'"; ?>>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control form-control-sm" name="folio_modificatorio" id="folio_modificatorio" <?php if(isset($b->folio_modificatorio)) echo "value='$b->folio_modificatorio'"; ?>>
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_foliom_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div> 
            </div>
            <div class="col-md-6 form-group">
              <label>Descripción de la modificación:</label>
              <textarea name="descrip_modifica" id="descrip_modifica" class="form-control" rows="4" cols="90"><?php if(isset($b->descrip_modifica)) echo "value='$b->descrip_modifica'";?> <?php if(isset($b->descrip_modifica)) echo $b->descrip_modifica; ?></textarea>
            </div>
            <?php }?>
          </div>   
          <!-- PARA EL AVISO MODIFICATORIO -->
          <hr class="subsubtitle">
          <div class="row">
            <div class="col-md-12">
              <h4>Tipo de operación: <span>Aportación a desarrollos(s) inmobiliarios(s)</span></h4>
            </div>
          </div>
          <h4>Datos de operación</h4>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <h5>Datos del desarrollo inmobiliario</h5>
            </div>
          </div>  
          <div class="row">
            <div class="col-md-12 form-group">
              <label>¿El desarrollo inmobiliario fue objeto de un aviso anterior?</label>
              <div class="row">
                <div class="col-md-2 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="objeto_aviso_anterior" value="SI" <?php if(isset($b->objeto_aviso_anterior) && $b->objeto_aviso_anterior=="SI") echo "checked"; ?>>
                      SI 
                    </label>
                  </div>
                </div>
                <div class="col-md-2 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="objeto_aviso_anterior" value="NO" <?php if(isset($b->objeto_aviso_anterior) && $b->objeto_aviso_anterior=="NO") echo "checked"; ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>  
          <div class="row">  
            <div class="col-md-12 form-group">
              <label>¿Las características del desarrollo inmobiliario objeto de un aviso anterior sufrió alguna modificación?</label>
              <div class="row">
                <div class="col-md-2 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="modificacion" value="SI" <?php if(isset($b->modificacion) && $b->modificacion=="SI") echo "checked"; ?>>
                      SI 
                    </label>
                  </div>
                </div>
                <div class="col-md-2 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="modificacion" value="NO" <?php if(isset($b->modificacion) && $b->modificacion=="NO") echo "checked"; ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>   
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Entidad Federativa  que autorizó, registró o emitió la licencia del desarrollo inmobiliario</label>
              <select class="form-control" name="entidad_federativa">
                <?php foreach ($estados->result() as $item){ ?>
                  <option value="<?php echo $item->id ?>" <?php if(isset($b) && $item->id==$b->entidad_federativa) echo 'selected' ?>><?php echo $item->estado ?></option>                   
                <?php } ?>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Identificador único de autorización, registro o licencia de obra del desarrollo inmobiliario</label>
              <div class="d-flex mb-2">  
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input class="form-control" type="text" name="registro_licencia" <?php if(isset($b->registro_licencia)) echo "value='$b->registro_licencia'"; ?>>
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_registro_licencia_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>   
              </div> 
            </div>
          </div>
          <hr class="subsubtitle">
          <h5>Características del desarrollo inmobiliario</h5>  
          <div class="row">
            <div class="col-md-6 form-group">
              <label>Inmuebles del cliente:</label>
              <select class="form-control" id="inmuebles" name="id_inmueble_cli">
                <option disabled selected>Selecciona un imueble</option>
                  <?php foreach ($inmue_cli as $i){ ?>
                    <option value="<?php echo $i->id ?>" <?php if(isset($b) && $i->id==$b->id_inmueble_cli) echo 'selected' ?>><?php echo $i->calle.", Colonia ".$i->colonia." C.P. ".$i->codigo_postal ?></option> 
                  <?php } ?>
              </select>
            </div>
            <div class="col-md-6 form-group"></div>
            <div class="col-md-3 form-group">
              <label>Código Postal de la ubicación del desarrollo inmobiliario</label>
              <input class="form-control" onchange="cambiaCP(this.id)" pattern="\d*" maxlength="5" type="text" name="codigo_postal" id="cp" <?php if(isset($b->codigo_postal)) echo "value='$b->codigo_postal'"; ?>>
            </div>
            <div class="col-md-4 form-group">
              <label>Colonia de la ubicación del desarrollo inmobiliario</label>
              <!--<input class="form-control" type="text" name="colonia" id="colonia">-->
              <select onclick="autoComplete(this.id)" class="form-control" name="colonia" id="colonia">
                <option value=""></option>
                <?php if(isset($b->colonia) && $b->colonia!=""){
                  echo '<option value="'.$b->colonia.'" selected>'.$b->colonia.'</option>';      
                } ?>  
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Calle, avenida o vía de la ubicación del desarrollo inmobiliario</label>
              <input class="form-control" type="text" id="calle" name="calle" <?php if(isset($b->calle)) echo "value='$b->calle'"; ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Tipo de desarrollo inmobiliario</label>
              <select class="form-control" id="tipo_desarrollo" name="tipo_desarrollo">
                <option value="1" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="1") echo "selected"; ?>>Habitacional</option>
                <option value="2" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="2") echo "selected"; ?>>Comercial</option>
                <option value="3" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="3") echo "selected"; ?>>Oficinas</option>
                <option value="4" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="4") echo "selected"; ?>>Industrial</option>
                <option value="5" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="5") echo "selected"; ?>>Mixto (Habitacional y Comercial)</option>
                <option value="99" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="99") echo "selected"; ?>>Otro</option>
              </select>
            </div>
            <div class="col-md-6 form-group" id="cont_inm_otro">
              <label>Descripción del tipo de desarrollo inmobiliario, en caso de ser otro</label>
              <input class="form-control" type="text" id="descripcion_desarrollo" name="descripcion_desarrollo" <?php if(isset($b->descripcion_desarrollo)) echo "value='$b->descripcion_desarrollo'"; ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Monto estimado total de costo de desarrollo del proyecto inmobiliario</label>
              <input class="form-control monto" type="text" id="monto_desarrollo" name="monto_desarrollo" <?php if(isset($b->monto_desarrollo)){ $monto_desarrollo = number_format($b->monto_desarrollo,2,'.',','); echo "value='$monto_desarrollo'"; } ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Número de unidades individuales a comercializar</label>
              <input class="form-control" type="text" id="unidades_comercializadas" name="unidades_comercializadas" <?php if(isset($b->unidades_comercializadas)) echo "value='$b->unidades_comercializadas'"; ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Costo comercial promedio estimado por unidad</label>
              <input class="form-control monto" type="text" id="costo_unidad" name="costo_unidad" <?php if(isset($b->costo_unidad)) { $costo_unidad = number_format($b->costo_unidad,2,'.',','); echo "value='$costo_unidad'"; } ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>¿Existe(n) otra(s) empresa(s) participando en el desarrollo de este proyecto inmobiliario?</label>
              <div class="row">
               <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" id="otras_empresass" name="otras_empresas" value="SI" <?php if(isset($b->otras_empresas) && $b->otras_empresas=="SI") echo "checked"; ?>>
                    SI 
                  </label>
                </div>
               </div>
               <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" id="otras_empresasn" name="otras_empresas" value="NO" <?php if(isset($b->otras_empresas) && $b->otras_empresas=="NO") echo "checked"; ?>>
                   NO
                  </label>
                </div>
               </div>
              </div>
            </div>
          </div>  
        </form>
         <!-------------------------------------------------------------------------------------> 
        <hr class="subsubtitle">
        <h4>Datos de las aportaciones</h4> 
        <div class="row">  
          <div class="col-md-4 form-group">
            <label>Fecha de la aportación</label>
            <input max="<?php echo date("Y-m-d")?>" class="form-control" type="date" id="fecha_aporta" name="fecha_aporta" <?php if(isset($a->fecha_aporta)) echo "value='$a->fecha_aporta'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?> >
          </div>
          <div class="col-md-8 form-group">
          </div>
          <div class="col-md-4 form-group">
            <label>Tipos de aportaciones:</label>
            <select class="form-control" name="tipo_aportacion" id="tipo_aportacion" onchange="select_aportacion()">
              <option value="1" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="1") echo "selected"; ?>>Datos cuando la aportación es a través de recursos propios </option>
              <option value="2" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="2") echo "selected"; ?>>Datos cuando la aportación es a través de socios </option>
              <option value="3" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="3") echo "selected"; ?>>Terceros </option>
              <option value="4" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="4") echo "selected"; ?>>Crédito o préstamo financiero </option>
              <option value="5" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="5") echo "selected"; ?>>Crédito o préstamo no financiero </option>
              <option value="6" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="6") echo "selected"; ?>>Aportación a través de financiamiento bursátil (emisión de deuda capital) </option>
            </select>
          </div>
        </div>
        <!--<div class="row">  
          <div class="col-md-12">
            <h5>Datos del tipo de aportaciones</h5>
          </div>
        </div>-->
        <!--------------------->
        <form id="form_aporta1" class="forms-sample">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_aportacion" value="1" id="tipo_aportacion1" onclick="select_aportacion()" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="1") echo "checked"; ?>>
                  Datos cuando la aportación es a través de recursos propios
                </label>
              </div>
            </div>-->
          </div>
          <!----->
          <div class="recursos_propios" style="display: none;">
            <hr>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="aportacion_numerario_especie" id="aportacion_numerario_especie1" onclick="select_numerario_especie()" <?php if(isset($b->aportacion_numerario_especie) && $b->aportacion_numerario_especie=="1") echo "checked"; ?> value="1">
                    Aportación numeraria
                  </label>
                </div>
               </div>
            </div>
            <div class="aportacion_numerario" style="display: none;">
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-5 form-group">
                  <label>Instrumento monetario con el que se realizó la operación</label>
                  <select class="form-control" name="instrumento_monetario" onchange="confirmarPagosMonto()">
                  <?php foreach ($instrumento_monetario->result() as $item){ ?>
                    <option value="<?php echo $item->clave ?>" <?php if(isset($b->instrumento_monetario) && $b->instrumento_monetario==$item->clave) echo "selected"; ?> ><?php echo $item->descripcion ?></option>
                  <?php }  ?>
                  </select>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5 form-group">
                  <label>Tipo de moneda o divisa de la aportación</label>
                  <select class="form-control" name="tipo_moneda" id="tipo_moneda_1" onchange="confirmarPagosMonto()">
                    <option value=""></option>
                    <?php 
                    /*$moneda_n=1;
                    foreach ($moneda->result() as $item){
                      if($item->id==$moneda_n){ 
                      ?>
                      <option value="<?php echo $item->id ?>" <?php if(isset($b->tipo_moneda) && $b->tipo_moneda==$item->id && $b->tipo_aportacion=="1") echo "selected"; ?>><?php echo $item->moneda ?></option>
                    <?php }
                    }*/ ?>
                    <option value="0" <?php if(isset($b) && $b->tipo_moneda=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                    <option value="1" <?php if(isset($b) && $b->tipo_moneda=="1") echo "selected"; ?>>Dólar Americano</option>
                    <option value="2" <?php if(isset($b) && $b->tipo_moneda=="2") echo "selected"; ?>>Euro</option>
                    <option value="3" <?php if(isset($b) && $b->tipo_moneda=="3") echo "selected"; ?>>Libra Esterlina</option>
                    <option value="4" <?php if(isset($b) && $b->tipo_moneda=="4") echo "selected"; ?>>Dólar canadiense</option>
                    <option value="5" <?php if(isset($b) && $b->tipo_moneda=="5") echo "selected"; ?>>Yuan Chino</option>
                    <option value="6" <?php if(isset($b) && $b->tipo_moneda=="6") echo "selected"; ?>>Centenario</option>
                    <option value="7" <?php if(isset($b) && $b->tipo_moneda=="7") echo "selected"; ?>>Onza de Plata</option>
                    <option value="8" <?php if(isset($b) && $b->tipo_moneda=="8") echo "selected"; ?>>Onza de Oro</option>
                  </select>
                </div>
              </div>
              <div class="row"> 
                <div class="col-md-1"></div>
                <div class="col-md-4 form-group">
                  <label>Monto de la aportación</label>
                  <input class="form-control monto" type="text" onchange="confirmarPagosMonto()" name="monto_aportacion" id="monto_aportacion" placeholder="$" <?php if(isset($b->monto_aportacion)) { $monto_aportacion = number_format($b->monto_aportacion,2,'.',','); echo "value='$monto_aportacion'"; } ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>La aportación fue realizada por medio de un Fideicomiso</label>
                  <div class="row">
                    <div class="col-md-4 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="checkbox" class="form-check-input" id="aportacion_fideicomiso" name="aportacion_fideicomiso" value="SI" <?php if(isset($b->aportacion_fideicomiso) && $b->aportacion_fideicomiso=="SI") echo "checked"; ?>>

                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 form-group" id="cont_nom_insti">
                  <label>Nombre de la institución fiduciaria del fideicomiso</label>
                  <input class="form-control" type="text" name="nombre_insti" <?php if(isset($b->nombre_insti) && $b->tipo_aportacion=="1") echo "value='$b->nombre_insti'"; ?>>
                </div>
              </div>
            </div>
            <!-- -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="aportacion_numerario_especie" id="aportacion_numerario_especie2" onclick="select_numerario_especie()" value="2" <?php if(isset($b->aportacion_numerario_especie) && $b->aportacion_numerario_especie=="2") echo "checked"; ?>>
                    Aportación en especie
                  </label>
                </div>
               </div>
            </div>
            <div class="aportacion_especie" style="display: none;">
              <div class="row"> 
                <div class="col-md-1"></div>
                <div class="col-md-5 form-group">
                  <label>Descripción del bien</label>
                  <input class="form-control" type="text" name="descripcion_bien" <?php if(isset($b->descripcion_bien)) echo "value='$b->descripcion_bien'"; ?>> 
                </div>
                <div class="col-md-1">
                  <br>
                  <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_descripcion_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4 form-group">
                  <label>Monto estimado del valor de la aportación en especie en moneda nacional</label>
                  <input onchange="confirmarPagosMonto()" class="form-control monto" type="text" id="monto_estimado" name="monto_estimado" <?php if(isset($b->monto_estimado)) echo "value='$b->monto_estimado'"; ?>><!-- -->
                </div>
              </div>  
            </div>  
            <!-- -->
            <hr>
          </div>  
        </form>
        <!-- termina aportacion 1 -->

        <!-- empieza aportacion 2 -->
        <form id="form_aporta2" class="forms-sample">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_aportacion" value="2" id="tipo_aportacion2" onclick="select_aportacion()" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="2") echo "checked"; ?>>
                  Datos cuando la aportación es a través de socios
                </label>
              </div>
             </div>-->
          </div>
          <div class="socios_text" style="display: none">  
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-3 form-group">
                <label>Número de socios de la aportación:</label>
                <input class="form-control" type="number" name="num_socios" id="num_socios" <?php if(isset($b) && $b->num_socios>0) { echo "value='$b->num_socios'"; } ?>>
              </div>
              <div class="col-md-2">
                <br>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_nsocios_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button> 
              </div>
              <div class="col-md-8 socios_text">
                <hr>
                <h5>Datos de los socios</h5>
                <hr>
                <!-- -->
              </div>

              <div class="col-md-5 form-group">
                <div class="col-md-1"></div>
                <label>¿El socio que realizo la aportación fue reportado en un aviso anterior?</label>
                  <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aviso_ant" id="aviso_ant" value="si" <?php if(isset($b->aviso_ant) && $b->aviso_ant=="si") echo "checked"; ?> >
                          Si
                        </label>
                      </div>
                    </div>
                    <div class="col-md-5 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="aviso_ant" id="aviso_ant2" value="no" <?php if(isset($b->aviso_ant) && $b->aviso_ant=="no") echo "checked"; ?>>
                          No
                        </label>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-4 form-group">
                <label>Registro Federal de Contribuyente(RFC) del socio:</label>
                <input class="form-control" type="text" onblur="ValidRfc(this)" id="rfc_socio" name="rfc_socio" <?php if(isset($b->rfc_socio)) echo "value='$b->rfc_socio'"; ?>>
              </div>
            </div>

            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-5 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_persona" id="fisica" value="1" <?php if(isset($b->tipo_persona) && $b->tipo_persona=="1" && $b->tipo_aportacion=="2") { echo "checked"; } ?>>
                    Persona física:
                  </label>
                </div>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_persona" id="moral" value="2" <?php if(isset($b->tipo_persona) && $b->tipo_persona=="2" && $b->tipo_aportacion=="2") echo "checked"; ?>>
                    Persona moral:
                  </label>
                </div>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_persona" id="fideicomiso" value="3" <?php if(isset($b->tipo_persona) && $b->tipo_persona=="3" && $b->tipo_aportacion=="2") echo "checked"; ?>>
                    Fideicomiso:
                  </label>
                </div>
              </div>
            </div> 
            <!-- persona fisica -->
            <div id="cont_persfi"> 
              <div class="row"> 
                <div class="col-md-1"></div>
                <div class="col-md-11 form-group">
                  <label>Nombre - los apellidos completos que corresponda y nombre(s).</label>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-3 form-group">
                  <label>Nombre(s):</label>
                  <input class="form-control" type="text" name="nombre" <?php if(isset($b->nombre) && $b->tipo_aportacion=="2") echo "value='$b->nombre'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno:</label>
                  <input class="form-control" type="text" name="app" <?php if(isset($b->app) && $b->tipo_aportacion=="2") echo "value='$b->app'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno:</label>
                  <input class="form-control" type="text" name="apm" <?php if(isset($b->apm) && $b->tipo_aportacion=="2") echo "value='$b->apm'"; ?>>
                </div>
              </div>
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-3 form-group">
                  <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                  <input max="<?php echo date("Y-m-d")?>" class="form-control" type="date" name="fecha_nac" <?php if(isset($a->fecha_nac) && $b->tipo_aportacion=="2") echo "value='$a->fecha_nac'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>País de nacionalidad:</label>
                  <select class="form-control" id="pais_nacionalidad" name="pais_nacionalidad">
                    <?php 
                      foreach ($get_pais as $p){
                        $select="";
                        if(isset($b) && $b->pais_nacionalidad==$p->clave && $b->tipo_aportacion=="2"){ 
                          $select="selected"; 
                          echo "<option value='$p->clave' $select>$p->pais</option>";
                        }else if(!isset($b) && $p->clave=="MX"){
                          $select="selected";
                        } 
                        echo "<option value='$p->clave' $select>$p->pais</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  <label>R.F.C:</label>
                  <input class="form-control" onblur="ValidRfc(this)" type="text" name="rfc" <?php if(isset($b->rfc) && $b->tipo_aportacion=="2") echo "value='$b->rfc'"; ?>>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3 form-group">
                  <label>CURP:</label>
                  <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp" <?php if(isset($b->curp) && $b->tipo_aportacion=="2") echo "value='$b->curp'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Actividad económica u ocupación</label>
                  <select class="form-control" name="actividad_ocupa">
                    <?php foreach ($get_actividad as $item) { 
                      $select="";
                      if(isset($b) && $b->actividad_ocupa==$item->clave && $b->tipo_aportacion=="2"){ $select="selected"; }
                        echo "<option value='$item->clave' $select>$item->acitividad</option>";
                      } ?>
                    </select>
                </div>
              </div>
            </div>

            <!-- persona moral -->
            <div class="row" id="cont_persmor" style="display: none">
              <div class="col-md-1"></div>
              <div class="col-md-3 form-group">
                <label>Denominación o Razón Social:</label>
                <input class="form-control" type="text" name="razon_social" <?php if(isset($b->razon_social) && $b->tipo_aportacion=="2") echo "value='$b->razon_social'"; ?>>
              </div>
              <div class="col-md-4 form-group">
                <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                <input max="<?php echo date("Y-m-d")?>" class="form-control" type="date" name="fecha_consti" <?php if(isset($b->fecha_consti) && $b->tipo_aportacion=="2") echo "value='$b->fecha_consti'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?>>
              </div>
              <div class="col-md-4 form-group">
                <label>País de nacionalidad:</label><br>
                <select class="form-control" name="pais_nacion_moral">
                  <?php 
                    foreach ($get_pais as $p){
                      $select="";
                      if(isset($b) && $b->pais_nacion_moral==$p->clave && $b->tipo_aportacion=="2"){ 
                        $select="selected"; 
                        echo "<option value='$p->clave' $select>$p->pais</option>";
                      }else if(!isset($b) && $p->clave=="MX" /* && $b->tipo_aportacion=="2"*/){
                        $select="selected";
                      } 
                      echo "<option value='$p->clave' $select>$p->pais</option>";
                    }
                  ?>
                </select>
              </div> 
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-11 form-group">
                  <label>Representante o apoderado legal o persona que realice la aportación o nombre de la persona moral.</label>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3 form-group">
                  <label>Nombre(s):</label>
                  <input class="form-control" type="text" name="nombre_moral" <?php if(isset($b->nombre_moral) && $b->tipo_aportacion=="2") echo "value='$b->nombre_moral'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno:</label>
                  <input class="form-control" type="text" name="app_moral" <?php if(isset($b->app_moral) && $b->tipo_aportacion=="2") echo "value='$b->app_moral'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno:</label>
                  <input class="form-control" type="text" name="apm_moral" <?php if(isset($b->apm_moral) && $b->tipo_aportacion=="2") echo "value='$b->apm_moral'"; ?>>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4 form-group">
                  <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                  <input  max="<?php echo date("Y-m-d")?>" class="form-control" type="date" name="fecha_nac_moral" <?php if(isset($a->fecha_nac_moral) && $b->tipo_aportacion=="2") echo "value='$a->fecha_nac_moral'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Registro Federal del Contribuyentes (R.F.C):</label>
                  <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_moral" <?php if(isset($b->rfc_moral) && $b->tipo_aportacion=="2") echo "value='$b->rfc_moral'"; ?>>
                </div>
                <div class="col-md-3 form-group">
                  <label>CURP:</label>
                  <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp_moral" <?php if(isset($b->curp_moral) && $b->tipo_aportacion=="2") echo "value='$b->curp_moral'"; ?>>
                </div>
              </div>
            </div>
            <!-- persona moral -->

            <!-- fideicomiso -->
            <div class="row" id="cont_fide" style="display: none">
              <div class="col-md-1"></div>
              <div class="col-md-4 form-group">
                <label>Denominación o Razón Social:</label>
                <input class="form-control" type="text" name="razon_social_fide" <?php if(isset($b->razon_social_fide) && $b->tipo_aportacion=="2") echo "value='$b->razon_social_fide'"; ?>>
              </div>
              <div class="col-md-3 form-group">
                <label>Registro Federal del Contribuyentes (R.F.C) del Fideicomiso:</label>
                <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_fide" <?php if(isset($b->rfc_fide) && $b->tipo_aportacion=="2") echo "value='$b->rfc_fide'"; ?>>
              </div>
              <div class="col-md-3 form-group">
                <label>Número de referencia o identificadordel fideicomiso:</label>
                <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp_fide" <?php if(isset($b->curp_fide) && $b->tipo_aportacion=="2") echo "value='$b->curp_fide'"; ?>>
              </div>
            </div>
            <!-- fideicomiso -->
            <hr class="subtitle">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-5 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="domicilio" id="nacional" value="1" <?php if(isset($b->domicilio) && $b->domicilio=="1") echo "checked"; ?> checked="">
                    Domicilio nacional:
                  </label>
                </div>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="domicilio" id="extrangero" value="2" <?php if(isset($b->domicilio) && $b->domicilio=="2") echo "checked"; ?>>
                    Domicilio extranjero:
                  </label>
                </div>
              </div>
            </div> 
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-4 form-group">
                <h3>Dirección:</h3>
              </div>
            </div> 
            <div class="row" id="cont_dir_ext" style="display: none">
              <div class="col-md-1"></div>
              <div class="col-md-3 form-group">
                <label>Clave del País donde se encuentra el domicilio:</label>
                <select class="form-control" name="pais_dir_ext">
                  <?php 
                    foreach ($get_pais as $p){
                      $select="";
                      if(isset($b) && $b->pais_dir_ext==$p->clave){ 
                        $select="selected"; 
                      }
                      echo "<option value='$p->clave' $select>$p->pais</option>";
                    }
                  ?>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label>Estado, provincia, departamento o demarcación política similar que corresponda:</label>
                <input class="form-control" type="text" name="edo_provincia" <?php if(isset($b->edo_provincia)) echo "value='$b->edo_provincia'"; ?>>
              </div>
              <div class="col-md-4 form-group">
                <label>Ciudad o Población:</label>
                <input class="form-control" type="text" name="ciudad_pobla" <?php if(isset($b->ciudad_pobla)) echo "value='$b->ciudad_pobla'"; ?>>
              </div>
            </div>

            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-2 form-group">
                <label>C.P:</label>
                <input class="form-control" onchange="cambiaCP(this.id)" pattern='\d*' maxlength="5" type="text" name="cp_nacional" id="cp_nacional" <?php if(isset($b->cp_nacional)) echo "value='$b->cp_nacional'"; ?>>
              </div>
              <div class="col-md-4 form-group">
                <label>Calle, avenida o vía:</label>
                <input class="form-control" type="text" name="calle_soc" <?php if(isset($b->calle_soc)) echo "value='$b->calle_soc'"; ?>>
              </div>
              <div class="col-md-1 form-group">
                <label>No. ext</label>
                <input class="form-control" type="text" name="num_ext_soc" <?php if(isset($b->num_ext_soc)) echo "value='$b->num_ext_soc'"; ?>>
              </div>
              <div class="col-md-1 form-group">
                <label>No. int:</label>
                <input class="form-control" type="text" name="num_int_soc" <?php if(isset($b->num_int_soc)) echo "value='$b->num_int_soc'"; ?>>
              </div>           
              <?php 
              //if(isset($b) && $b->pais_dir=="MX" && $b->pais_dir_ext!="MX") { ?>
              <div class="col-md-3 form-group" id="cont_col_sel">
                <div class="col-md-1"></div>
                <label>Colonia: </label>
                <select onclick="autoComplete(this.id)" class="form-control" name="colonia_dir" id="colonia_dir">
                  <option value=""></option>
                  <?php if(isset($b->colonia_dir) && $b->colonia_dir!=""){
                    echo '<option value="'.$b->colonia_dir.'" selected>'.$b->colonia_dir.'</option>';      
                  } ?>  
                </select>
              </div>
              <?php //} 
              //else if(isset($b) && $b->pais_dir!="MX") { ?>
              <div class="col-md-3 form-group" id="cont_col_sel_ext">
                <div class="col-md-1"></div>
                <div class="col-md-12 form-group">
                  <label>Colonia:</label>
                  <input class="form-control" type="text" id="colonia_dir2" name="colonia_dir" <?php if(isset($b->colonia_dir)) echo "value='$b->colonia_dir'"; ?>>
                </div>
              </div>
              <?php // }

              //else if(isset($b) && $b->pais_dir_ext!="MX") { ?>
                <!--<div class="col-md-4 form-group" id="cont_col_sel">
                  <label>Colonia:</label>
                  <input class="form-control" type="text" name="colonia_dir" <?php if(isset($b->colonia_dir)) echo "value='$b->colonia_dir'"; ?>>
                </div>-->
              <?php // } ?>
              
            </div>
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-11 form-group">
                <label>Teléfono y correo electrónico</label>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-4 form-group">
                <label>País:</label>
                <select class="form-control" name="pais_dir">
                  <?php 
                    foreach ($get_pais as $p){
                      $select="";
                      if(isset($b) && $b->pais_dir==$p->clave){ 
                        $select="selected"; 
                      }
                      echo "<option value='$p->clave' $select>$p->pais</option>";
                    }
                  ?>
                </select>
              </div>
              <div class="col-md-3 form-group">
                <label>Número de teléfono:</label>
                <input class="form-control" type="text" name="telefono" <?php if(isset($b->telefono)) echo "value='$b->telefono'"; ?>>
              </div>
              <div class="col-md-4 form-group">
                <label>Correo electrónico:</label>
                <input class="form-control" type="text" name="correo" <?php if(isset($b->correo)) echo "value='$b->correo'"; ?>>
              </div>
            </div>
            <hr class="subtitle">
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-11 form-group">
                <h3>Aportación(es) de socio</h3>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-5 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="aportacion" id="aport_num" value="1" <?php if(isset($b->aportacion) && $b->aportacion=="1") echo "checked"; ?> checked="">
                    Aportación numeraria:
                  </label>
                </div>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="aportacion" id="aport_espe" <?php if(isset($b->aportacion) && $b->aportacion=="2") echo "checked"; ?> value="2">
                    Aportación en especie:
                  </label>
                </div>
              </div>
            </div> 
            <div class="row" id="cont_apor_num" style="display: none">
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-5 form-group">
                  <label>Instrumento monetario con el que se realizó la operación</label>
                  <select id="instrum_notario" name="instrum_notario" class="form-control" onchange="confirmarPagosMonto()">
                    <option value="1" <?php if(isset($b) && $b->instrum_notario=="1"){ echo "selected"; } ?>>Efectivo</option>
                    <option value="2" <?php if(isset($b) && $b->instrum_notario=="2"){ echo "selected"; } ?>>Tarjeta de Crédito</option>
                    <option value="3" <?php if(isset($b) && $b->instrum_notario=="3"){ echo "selected"; } ?>>Tarjeta de Débito</option>
                    <option value="4" <?php if(isset($b) && $b->instrum_notario=="4"){ echo "selected"; } ?>>Tarjeta de Prepago</option>
                    <option value="5" <?php if(isset($b) && $b->instrum_notario=="5"){ echo "selected"; } ?>>Cheque Nominativo</option>
                    <option value="6" <?php if(isset($b) && $b->instrum_notario=="6"){ echo "selected"; } ?>>Cheque de Caja</option>
                    <option value="7" <?php if(isset($b) && $b->instrum_notario=="7"){ echo "selected"; } ?>>Cheques de Viajero</option>
                    <option value="8" <?php if(isset($b) && $b->instrum_notario=="8"){ echo "selected"; } ?>>Transferencia Interbancaria</option>
                    <option value="9" <?php if(isset($b) && $b->instrum_notario=="9"){ echo "selected"; } ?>>Transferencia Misma Institución</option>
                    <option value="10" <?php if(isset($b) && $b->instrum_notario=="10"){ echo "selected"; } ?>>Transferencia Internacional</option>
                    <option value="11" <?php if(isset($b) && $b->instrum_notario=="11"){ echo "selected"; } ?>>Orden de Pago</option>
                    <option value="12" <?php if(isset($b) && $b->instrum_notario=="12"){ echo "selected"; } ?>>Giro</option>
                    <option value="13" <?php if(isset($b) && $b->instrum_notario=="13"){ echo "selected"; } ?>>Oro o Platino Amonedados</option>
                    <option value="14" <?php if(isset($b) && $b->instrum_notario=="14"){ echo "selected"; } ?>>Plata Amonedada</option>
                    <option value="15" <?php if(isset($b) && $b->instrum_notario=="15"){ echo "selected"; } ?>>Metales Preciosos</option>
                  </select>
                </div>
                <div class="col-md-6 form-group">
                  <label>Tipo de moneda o divisa de la aportación</label>
                  <select id="tipo_moneda" name="tipo_moneda" class="tipo_moneda_2 form-control" onchange="confirmarPagosMonto()">
                    <option value=""></option>
                    <?php
                    /*$pesomxc=1;   
                    foreach ($tm->result() as $t) {
                    $select="";
                      if($t->id==$pesomxc){
                          if(isset($b) && $b->tipo_moneda==$t->id && $b->tipo_aportacion=="2"){ $select="selected"; }
                        echo "<option value='$t->id'$select>$t->moneda</option>";
                      } 
                    } */ ?>
                    <option value="0" <?php if(isset($b) && $b->tipo_moneda=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                    <option value="1" <?php if(isset($b) && $b->tipo_moneda=="1") echo "selected"; ?>>Dólar Americano</option>
                    <option value="2" <?php if(isset($b) && $b->tipo_moneda=="2") echo "selected"; ?>>Euro</option>
                    <option value="3" <?php if(isset($b) && $b->tipo_moneda=="3") echo "selected"; ?>>Libra Esterlina</option>
                    <option value="4" <?php if(isset($b) && $b->tipo_moneda=="4") echo "selected"; ?>>Dólar canadiense</option>
                    <option value="5" <?php if(isset($b) && $b->tipo_moneda=="5") echo "selected"; ?>>Yuan Chino</option>
                    <option value="6" <?php if(isset($b) && $b->tipo_moneda=="6") echo "selected"; ?>>Centenario</option>
                    <option value="7" <?php if(isset($b) && $b->tipo_moneda=="7") echo "selected"; ?>>Onza de Plata</option>
                    <option value="8" <?php if(isset($b) && $b->tipo_moneda=="8") echo "selected"; ?>>Onza de Oro</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-5">                 
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" id="aport_fide" name="aport_fide" <?php if(isset($b->aport_fide) && $b->aport_fide=="on" && $b->tipo_aportacion=="2") echo "checked"; ?>>
                      La aportación fue realizada por medio de un fideicomiso.
                    <i class="input-helper"></i></label>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <label>Nombre de la institución fiduciario o del fideicomiso</label>
                  <input class="form-control" type="text" name="nombre_insti" <?php if(isset($b->nombre_insti) && $b->tipo_aportacion=="2") echo "value='$b->nombre_insti'"; ?>>
                </div>
              </div>
            </div>

            <div class="row" id="cont_apor_esp" style="display: none"> 
              <div class="col-md-1"></div>
              <div class="col-md-11 form-group" >
                <label>Descripción del bien</label>
                <div class="d-flex mb-2">
                  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input class="form-control" type="text" name="descrip_bien" <?php if(isset($b->descrip_bien) && $b->tipo_aportacion=="2") echo "value='$b->descrip_bien'"; ?>>
                    </div>
                    <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_descripcion_ayuda()"><i class="mdi mdi-help"></i></button>
                </div>
              </div>
            </div>
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-4 form-group">
                <label>Monto de la aportación</label>
                <input class="form-control monto" type="text" id="monto_aporta" name="monto_aporta" placeholder="$" <?php if(isset($b->monto_aporta) && $b->tipo_aportacion=="2"){ $monto_aporta = number_format($b->monto_aporta,2,'.',',');  echo "value='$monto_aporta'"; } ?> onchange="confirmarPagosMonto()">
              </div>
            </div>
          </div>
        </form>
        <!-- termina aporta 2 -->

        <!-- empieza aportacion 3 -->
        <form id="form_aporta3" class="forms-sample">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_aportacion" value="3" id="tipo_aportacion3" onclick="select_aportacion()" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="3") echo "checked"; ?>>
                  Terceros 
                </label>
              </div>
             </div>-->
          </div>
        </form>
          <!--- -->
          <div class="terceros_text" style="display: none">
            <table class="table table-sm" id="tabla_tercs">
              <thead>
                <tr id="solo_bton" style="display:none">
                  <td>
                    <div class="row col-md-12"> 
                      <div class="col-md-12 form-group" align="end" id="cont_add_tro">
                        <label style="color: transparent;">agregar tercero</label>
                        <button type="button" onclick="agregarTercero($(this))" class="btn gradient_nepal2 agregar_tercero"><i class="fa fa-plus"></i>Agregar Tercero</button>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
              <tbody id="table_servicios" >
                
                <tr id="datos_tercero_principal">
                  <td>
                    <div class="col-md-12" id="cont_tit_dt" style="display:none">
                      <br><br><hr>
                      <h4 id="tit_dt">Datos de Tercero:</h4>
                    </div>
                    <div class="row"> 
                      <input class="form-control" type="hidden" name="id" id="id_terceros_prin" value="0">
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Número de terceros de la aportación:</label>
                        <input class="form-control" type="text" name="num_aporta_tercero" value="">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Tipo de tercero:</label>
                        <select class="form-control" name="tipo_tercero" id="tipo_tercero">
                          <option value=""></option>
                          <option value="1">Proveedores</option>
                          <option value="2">Cliente(s) en Preventa</option>
                          <option value="99">Otro</option>
                        </select>
                      </div>
                    </div>
                    <div class="row" id="cont_desc_otro" style="display: none"> 
                      <div class="col-md-1"></div>
                      <div class="col-md-8 form-group">
                        <label>Descripción de tercero</label>
                        <input class="form-control" type="text" name="descrip_tercero" value="">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col-md-1"></div>
                      <div class="col-md-5 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="tipo_persona" id="fisca_ter" value="1">
                            Persona física:
                          </label>
                        </div>
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="tipo_persona" id="moral_ter" value="2">
                            Persona moral:
                          </label>
                        </div>
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="tipo_persona" id="fide_ter" value="3">
                            Fideicomiso:
                          </label>
                        </div>
                      </div>
                    </div> 
                    <!-- persona fisica -->
                    <div id="cont_persfi_ter"> 
                      <div class="row"> 
                        <div class="col-md-1"></div>
                        <div class="col-md-11 form-group">
                          <label>Nombre - los apellidos completos que corresponda y nombre(s).</label>
                        </div>
                      </div> 
                      <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label>Nombre(s):</label>
                          <input class="form-control" type="text" name="nombre" value="">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido paterno:</label>
                          <input class="form-control" type="text" name="app" value="">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido materno:</label>
                          <input class="form-control" type="text" name="apm" value="">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                          <input max="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" name="fecha_nac" value="">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>País de nacionalidad:</label>
                          <select class="form-control" id="pais_nacionalidad" name="pais_nacionalidad">
                            <?php 
                              foreach ($get_pais as $p){
                                $select="";
                                if($p->clave=="MX"){ 
                                  $select="selected"; 
                                  echo "<option value='$p->clave' $select>$p->pais</option>";
                                }
                                echo "<option value='$p->clave' $select>$p->pais</option>";
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>R.F.C:</label>
                          <input class="form-control" onblur="ValidRfc(this)" type="text" name="rfc" value="">
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4 form-group">
                          <label>CURP:</label>
                          <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp" value="">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Actividad económica u ocupación</label>
                          <select class="form-control" name="actividad_ocupa">
                            <?php foreach ($get_actividad as $item) { 
                              echo "<option value='$item->clave' $select>$item->acitividad</option>";
                            } ?>
                            </select>
                        </div>
                      </div>
                    </div>
                    <!-- persona fisica -->
                    <!-- persona moral -->
                    <div class="row" id="cont_persmor_ter" style="display: none">
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Denominación o Razón Social:</label>
                        <input class="form-control" type="text" name="razon_social" value="">
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                        <input max="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" name="fecha_consti" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>País de nacionalidad:</label><br>
                        <select class="form-control" name="pais_nacion_moral">
                          <?php 
                            foreach ($get_pais as $p){
                              $select="";
                              if($p->clave=="MX"){
                                $select="selected";
                              } 
                              echo "<option value='$p->clave' $select>$p->pais</option>";
                            }
                          ?>
                        </select>
                      </div> 
                      <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-11 form-group">
                          <label>Representante o apoderado legal o persona que realice la aportación o nombre de la persona moral.</label>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label>Nombre(s):</label>
                          <input class="form-control" type="text" name="nombre_moral" value="">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido paterno:</label>
                          <input class="form-control" type="text" name="app_moral" value="">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido materno:</label>
                          <input class="form-control" type="text" name="apm_moral" value="">
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                          <input max="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" name="fecha_nac_moral" value="<?php echo date("Y-m-d"); ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Registro Federal del Contribuyentes (R.F.C):</label>
                          <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_moral" value="">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>CURP:</label>
                          <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp_moral" value="">
                        </div>
                      </div>
                    </div>
                    <!-- persona moral -->

                    <!-- fideicomiso -->
                    <div class="row" id="cont_fide_ter" style="display: none">
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Denominación o Razón Social:</label>
                        <input class="form-control" type="text" name="razon_social_fide" value="">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Registro Federal del Contribuyentes (R.F.C) del Fideicomiso:</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_fide" value="">
                      </div>
                      <div class="col-md-4 form-group">
                        <label>CURP:</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp_fide" value="">
                      </div>
                    </div>
                    <!-- fideicomiso -->
                    <hr class="subtitle">
                    <div class="row">
                      <div class="col-md-1"></div>
                      <div class="col-md-5 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="aportacion" id="aport_num_ter" value="1" >
                            Aportación numeraria:
                          </label>
                        </div>
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="aportacion" id="aport_esp_ter" value="2">
                            Aportación en especie:
                          </label>
                        </div>
                      </div>
                    </div> 
                    <div class="row" id="cont_apor_num_ter">
                      <div class="col-md-1"></div>
                      <div class="col-md-3 form-group">
                        <label>Instrumento monetario con el que se realizó la operación</label>
                        <select id="instrum_notario" name="instrum_notario" class="form-control" onchange="confirmarPagosMonto()">
                          <option value="1">Efectivo</option>
                          <option value="2">Tarjeta de Crédito</option>
                          <option value="3">Tarjeta de Débito</option>
                          <option value="4">Tarjeta de Prepago</option>
                          <option value="5">Cheque Nominativo</option>
                          <option value="6">Cheque de Caja</option>
                          <option value="7">Cheques de Viajero</option>
                          <option value="8">Transferencia Interbancaria</option>
                          <option value="9">Transferencia Misma Institución</option>
                          <option value="10">Transferencia Internacional</option>
                          <option value="11">Orden de Pago</option>
                          <option value="12">Giro</option>
                          <option value="13">Oro o Platino Amonedados</option>
                          <option value="14">Plata Amonedada</option>
                          <option value="15">Metales Preciosos</option>
                        </select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Tipo de moneda o divisa de la aportación</label>
                        <select id="tipo_moneda" name="tipo_moneda" class="form-control"onchange="confirmarPagosMonto()">
                          <option value="0">Pesos Mexicanos</option>
                          <option value="1">Dólar Americano</option>
                          <option value="2">Euro</option>
                          <option value="3">Libra Esterlina</option>
                          <option value="4">Dólar canadiense</option>
                          <option value="5">Yuan Chino</option>
                          <option value="6">Centenario</option>
                          <option value="7">Onza de Plata</option>
                          <option value="8">Onza de Oro</option>
                        </select>
                      </div>
                      <div class="col-md-5">
                        <div class="form-check form-check-flat form-check-primary">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" id="aport_fide" name="aport_fide" value="">
                            La aportación fue realizada por medio de un fideicomiso.
                          <i class="input-helper"></i></label>
                        </div>
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Nombre de la institución fiduciario o del fideicomiso</label>
                        <input class="form-control" type="text" name="nombre_insti" value="">
                      </div>
                      <div class="col-md-5 form-group" id="cont_img_nvo1">
                        <h4>Comprobante 1:</h4>
                        <input name="name_comprobante" id="name_comprobante" type="hidden">
                        <div class="col-md-10">
                          <input data-num_aux="1" width="50px" height="80px" data-idaux="0" class="img compro_nvo compro_nvo1" id="comprobante" type="file">
                        </div>
                      </div>
                      <div class="col-md-5 form-group" id="cont_img_nvo2">
                        <h4>Comprobante 2:</h4>
                        <input name="name_comprobante2" id="name_comprobante2" type="hidden">
                        <div class="col-md-10">
                          <input data-num_aux="2" width="50px" height="80px" data-idaux="0" class="img compro_nvo compro_nvo2" id="comprobante2" type="file">
                        </div>
                      </div>
                      <div class="col-md-5 form-group" id="cont_img_nvo3">
                        <h4>Comprobante 3:</h4>
                        <input name="name_comprobante3" id="name_comprobante3" type="hidden">
                        <div class="col-md-10">
                          <input data-num_aux="3" width="50px" height="80px" data-idaux="0" class="img compro_nvo compro_nvo3" id="comprobante3" type="file">
                        </div>
                      </div>
                    </div>
                    <div class="row" id="cont_apor_esp_ter" style="display: none"> 
                      <div class="col-md-1"></div>
                      <div class="col-md-9 form-group">
                        <label>Descripción del bien</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input class="form-control" type="text" name="descrip_bien" value="">
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_descripcion_ayuda()">
                            <i class="mdi mdi-help"></i>
                          </button>              
                        </div>
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Monto de la aportación</label>
                        <input class="form-control monto" type="text" id="monto_aporta2" name="monto_aporta" placeholder="$" value="" onchange="confirmarPagosMonto()">
                      </div>
                      
                    </div>
                    <div class="row" id="cont_apor_num_ter2"> 
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Valor total pactado del inmueble en preventa en moneda nacional</label>
                        <input class="form-control monto" type="text" id="valor_pactado" placeholder="$" name="valor_pactado" value="" onchange="confirmarPagosMonto()">
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="col-md-2 form-group" align="end" id="cont_add_tro">
                        <label style="color: transparent;">agregar tercero</label>
                        <button type="button" onclick="agregarTercero($(this))" class="btn gradient_nepal2 agregar_tercero"><i class="fa fa-plus"></i>Agregar Tercero</button>
                      </div>
                      <div class="col-md-2 form-group" align="end">
                        <label style="color: transparent;">elimina tercero</label>
                        <button type="button" onclick="remove_tercero($(this),0)" class="btn gradient_nepal2 elimina_tercero"><i class="fa fa-trash"></i> Eliminar Tercero</button>
                      </div>
                    </div>
                  </td>
                </tr>
              <?php if(isset($dato_ter) && $cont_ter>0){ $cont_ter=0;
                foreach($dato_ter as $bt){ $cont_ter++; ?>  
                <tr id="datos_tercero_<?php echo $bt->id; ?>" <?php if($cont_ter==1){ echo 'class="datos_tercero_clone1"';} ?>>
                  <td>
                    <br><br><hr>
                    <h4>Datos de Tercero (<?php echo $cont_ter; ?>):</h4>
                    <div class="row"> 
                      <input class="form-control" type="hidden" name="id" id="id_terceros" value="<?php echo $bt->id; ?>">
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Número de terceros de la aportación:</label>
                        <input class="form-control" type="text" name="num_aporta_tercero" <?php echo "value='$bt->num_aporta_tercero'"; ?>>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>Tipo de tercero:</label>
                        <select class="form-control" name="tipo_tercero" id="tipo_tercero" onchange="cambiaTipoTercero(this.value,<?php echo $bt->id; ?>,0)">
                          <option value=""></option>
                          <option value="1" <?php if($bt->tipo_tercero=="1"){ echo "selected"; } ?>>Proveedores</option>
                          <option value="2" <?php if($bt->tipo_tercero=="2"){ echo "selected"; } ?>>Cliente(s) en Preventa</option>
                          <option value="99" <?php if($bt->tipo_tercero=="99"){ echo "selected"; } ?>>Otro</option>
                        </select>
                      </div>
                    </div>
                    <div class="row" id="cont_desc_otro_<?php echo $bt->id; ?>" style="display: none"> 
                      <div class="col-md-1"></div>
                      <div class="col-md-8 form-group">
                        <label>Descripción de tercero</label>
                        <input class="form-control" type="text" name="descrip_tercero" <?php echo "value='$bt->descrip_tercero'"; ?>>
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col-md-1"></div>
                      <div class="col-md-5 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onchange="cambiaPersonaTercero(1,<?php echo $bt->id;?>,0)" name="tipo_persona_<?php echo $bt->id;?>" id="fisca_ter" <?php if($bt->tipo_persona=="1") echo "checked"; ?> value="1">
                            Persona física:
                          </label>
                        </div>
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onchange="cambiaPersonaTercero(2,<?php echo $bt->id;?>,0)" name="tipo_persona_<?php echo $bt->id;?>" id="moral_ter" <?php if($bt->tipo_persona=="2") echo "checked"; ?> value="2">
                            Persona moral:
                          </label>
                        </div>
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onchange="cambiaPersonaTercero(3,<?php echo $bt->id;?>,0)" name="tipo_persona_<?php echo $bt->id;?>" id="fide_ter" <?php if($bt->tipo_persona=="3") echo "checked"; ?> value="3">
                            Fideicomiso:
                          </label>
                        </div>
                      </div>
                    </div> 
                    <div id="cont_persfi_ter_<?php echo $bt->id; ?>" <?php if($bt->tipo_persona!="1") echo 'style="display: none"'; ?>> 
                      <div class="row"> 
                        <div class="col-md-1"></div>
                        <div class="col-md-11 form-group">
                          <label>Nombre - los apellidos completos que corresponda y nombre(s).</label>
                        </div>
                      </div> 
                      <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label>Nombre(s):</label>
                          <input class="form-control" type="text" name="nombre" value="<?php echo $bt->nombre; ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido paterno:</label>
                          <input class="form-control" type="text" name="app" value="<?php echo $bt->app; ?>">
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido materno:</label>
                          <input class="form-control" type="text" name="apm" value="<?php echo $bt->apm; ?>">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                          <input max="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" name="fecha_nac" value="<?php echo $bt->descrip_tercero; ?>" >
                        </div>
                        <div class="col-md-4 form-group">
                          <label>País de nacionalidad:</label>
                          <select class="form-control" id="pais_nacionalidad" name="pais_nacionalidad">
                            <?php 
                              foreach ($get_pais as $p){
                                $select="";
                                if($bt->pais_nacionalidad==$p->clave){ 
                                  $select="selected"; 
                                  echo "<option value='$p->clave' $select>$p->pais</option>";
                                }else if($p->clave=="MX"){
                                  $select="selected";
                                } 
                                echo "<option value='$p->clave' $select>$p->pais</option>";
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>R.F.C:</label>
                          <input class="form-control" onblur="ValidRfc(this)" type="text" name="rfc" <?php echo "value='$bt->rfc'"; ?>>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4 form-group">
                          <label>CURP:</label>
                          <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp" <?php echo "value='$bt->curp'"; ?>>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Actividad económica u ocupación</label>
                          <select class="form-control" name="actividad_ocupa">
                            <?php foreach ($get_actividad as $item) { 
                              $select="";
                              if($bt->actividad_ocupa==$item->clave){ $select="selected"; }
                                echo "<option value='$item->clave' $select>$item->acitividad</option>";
                              } ?>
                            </select>
                        </div>
                      </div>
                    </div>

                    <!-- persona moral -->
                    <div class="row" id="cont_persmor_ter_<?php echo $bt->id; ?>" <?php if($bt->tipo_persona!="2") echo 'style="display: none"'; ?>>
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Denominación o Razón Social:</label>
                        <input class="form-control" type="text" name="razon_social" <?php echo "value='$bt->razon_social'"; ?>>
                      </div>
                      <div class="col-md-4 form-group">
                        <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                        <input max="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" name="fecha_consti" <?php echo "value='$bt->fecha_consti'"; ?>>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>País de nacionalidad:</label><br>
                        <select class="form-control" name="pais_nacion_moral">
                          <?php 
                            foreach ($get_pais as $p){
                              $select="";
                              if($bt->pais_nacion_moral==$p->clave){ 
                                $select="selected"; 
                                echo "<option value='$p->clave' $select>$p->pais</option>";
                              }else{
                                $select="selected";
                              } 
                              echo "<option value='$p->clave' $select>$p->pais</option>";
                            }
                          ?>
                        </select>
                      </div> 
                      <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-11 form-group">
                          <label>Representante o apoderado legal o persona que realice la aportación o nombre de la persona moral.</label>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label>Nombre(s):</label>
                          <input class="form-control" type="text" name="nombre_moral" <?php echo "value='$bt->nombre_moral'"; ?>>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido paterno:</label>
                          <input class="form-control" type="text" name="app_moral" <?php echo "value='$bt->app_moral'"; ?>>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Apellido materno:</label>
                          <input class="form-control" type="text" name="apm_moral" <?php echo "value='$bt->apm_moral'"; ?>>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3 form-group">
                          <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                          <input max="<?php echo date('Y-m-d'); ?>" class="form-control" type="date" name="fecha_nac_moral" value="<?php echo date("Y-m-d"); ?>" <?php echo "value='$bt->fecha_nac_moral'"; ?>>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>Registro Federal del Contribuyentes (R.F.C):</label>
                          <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_moral" <?php echo "value='$bt->rfc_moral'"; ?>>
                        </div>
                        <div class="col-md-4 form-group">
                          <label>CURP:</label>
                          <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp_moral" <?php echo "value='$bt->curp_moral'"; ?>>
                        </div>
                      </div>
                    </div>
                    <!-- persona moral -->

                    <!-- fideicomiso -->
                    <div class="row" id="cont_fide_ter_<?php echo $bt->id; ?>" <?php if($bt->tipo_persona!="3") echo 'style="display: none"'; ?>>
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Denominación o Razón Social:</label>
                        <input class="form-control" type="text" name="razon_social_fide" <?php echo "value='$bt->razon_social_fide'"; ?>>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Registro Federal del Contribuyentes (R.F.C) del Fideicomiso:</label>
                        <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_fide" <?php echo "value='$bt->rfc_fide'"; ?>>
                      </div>
                      <div class="col-md-4 form-group">
                        <label>CURP:</label>
                        <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp_fide" <?php echo "value='$bt->curp_fide'"; ?>>
                      </div>
                    </div>
                    <!-- fideicomiso -->
                    <hr class="subtitle">
                    <div class="row">
                      <div class="col-md-1"></div>
                      <div class="col-md-5 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onclick="cambiaPagoTercero(1,<?php echo $bt->id; ?>,0)" name="aportacion_<?php echo $bt->id; ?>" id="aport_num_ter" value="1" <?php if($bt->aportacion=="1") echo "checked"; ?>>
                            Aportación numeraria:
                          </label>
                        </div>
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" onclick="cambiaPagoTercero(2,<?php echo $bt->id; ?>,0)" name="aportacion_<?php echo $bt->id; ?>" id="aport_esp_ter" <?php if($bt->aportacion=="2") echo "checked"; ?> value="2">
                            Aportación en especie:
                          </label>
                        </div>
                      </div>
                    </div> 
                    <div class="row" id="cont_apor_num_ter_<?php echo $bt->id; ?>" <?php if($bt->aportacion!="1") echo 'style="display: none"'; ?>>
                      <div class="col-md-1"></div>
                      <div class="col-md-3 form-group">
                        <label>Instrumento monetario con el que se realizó la operación</label>
                        <select id="instrum_notario" name="instrum_notario" class="form-control" onchange="confirmarPagosMonto()">
                          <option value="1" <?php if($bt->instrum_notario=="1"){ echo "selected"; } ?>>Efectivo</option>
                          <option value="2" <?php if($bt->instrum_notario=="2"){ echo "selected"; } ?>>Tarjeta de Crédito</option>
                          <option value="3" <?php if($bt->instrum_notario=="3"){ echo "selected"; } ?>>Tarjeta de Débito</option>
                          <option value="4" <?php if($bt->instrum_notario=="4"){ echo "selected"; } ?>>Tarjeta de Prepago</option>
                          <option value="5" <?php if($bt->instrum_notario=="5"){ echo "selected"; } ?>>Cheque Nominativo</option>
                          <option value="6" <?php if($bt->instrum_notario=="6"){ echo "selected"; } ?>>Cheque de Caja</option>
                          <option value="7" <?php if($bt->instrum_notario=="7"){ echo "selected"; } ?>>Cheques de Viajero</option>
                          <option value="8" <?php if($bt->instrum_notario=="8"){ echo "selected"; } ?>>Transferencia Interbancaria</option>
                          <option value="9" <?php if($bt->instrum_notario=="9"){ echo "selected"; } ?>>Transferencia Misma Institución</option>
                          <option value="10" <?php if($bt->instrum_notario=="10"){ echo "selected"; } ?>>Transferencia Internacional</option>
                          <option value="11" <?php if($bt->instrum_notario=="11"){ echo "selected"; } ?>>Orden de Pago</option>
                          <option value="12" <?php if($bt->instrum_notario=="12"){ echo "selected"; } ?>>Giro</option>
                          <option value="13" <?php if($bt->instrum_notario=="13"){ echo "selected"; } ?>>Oro o Platino Amonedados</option>
                          <option value="14" <?php if($bt->instrum_notario=="14"){ echo "selected"; } ?>>Plata Amonedada</option>
                          <option value="15" <?php if($bt->instrum_notario=="15"){ echo "selected"; } ?>>Metales Preciosos</option>
                        </select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Tipo de moneda o divisa de la aportación</label>
                        <select id="tipo_moneda" name="tipo_moneda" class="form-control"onchange="confirmarPagosMonto()">
                          <option value="0" <?php if($bt->tipo_moneda=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                          <option value="1" <?php if($bt->tipo_moneda=="1") echo "selected"; ?>>Dólar Americano</option>
                          <option value="2" <?php if($bt->tipo_moneda=="2") echo "selected"; ?>>Euro</option>
                          <option value="3" <?php if($bt->tipo_moneda=="3") echo "selected"; ?>>Libra Esterlina</option>
                          <option value="4" <?php if($bt->tipo_moneda=="4") echo "selected"; ?>>Dólar canadiense</option>
                          <option value="5" <?php if($bt->tipo_moneda=="5") echo "selected"; ?>>Yuan Chino</option>
                          <option value="6" <?php if($bt->tipo_moneda=="6") echo "selected"; ?>>Centenario</option>
                          <option value="7" <?php if($bt->tipo_moneda=="7") echo "selected"; ?>>Onza de Plata</option>
                          <option value="8" <?php if($bt->tipo_moneda=="8") echo "selected"; ?>>Onza de Oro</option>
                        </select>
                      </div>
                      <div class="col-md-5">
                        <div class="form-check form-check-flat form-check-primary">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" id="aport_fide" name="aport_fide" <?php if($bt->aport_fide=="on") echo "checked"; ?>>
                            La aportación fue realizada por medio de un fideicomiso.
                          <i class="input-helper"></i></label>
                        </div>
                      </div>
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Nombre de la institución fiduciario o del fideicomiso</label>
                        <input class="form-control" type="text" name="nombre_insti" <?php echo "value='$bt->nombre_insti'"; ?>>
                      </div>
                      <div class="col-md-5 form-group">
                        <h4>Comprobante Cargado 1:</h4>
                        <input name="name_comprobante" id="name_comprobante_<?php echo $bt->id; ?>" type="hidden" value="<?php echo $bt->comprobante; ?>">
                        <div class="col-md-10">
                          <input width="50px" height="80px" data-num_aux="1" data-idaux="<?php echo $bt->id;?>" class="img" id="comprobante_<?php echo $bt->id; ?>" type="file">
                        </div>
                      </div>
                      <div class="col-md-5 form-group">
                        <h4>Comprobante Cargado 2:</h4>
                        <input name="name_comprobante2" id="name_comprobante2_<?php echo $bt->id; ?>" type="hidden" value="<?php echo $bt->comprobante2; ?>">
                        <div class="col-md-10">
                          <input width="50px" height="80px" data-num_aux="2" data-idaux="<?php echo $bt->id;?>" class="img" id="comprobante2_<?php echo $bt->id; ?>" type="file">
                        </div>
                      </div>
                      <div class="col-md-5 form-group">
                        <h4>Comprobante Cargado 3:</h4>
                        <input name="name_comprobante3" id="name_comprobante3_<?php echo $bt->id; ?>" type="hidden" value="<?php echo $bt->comprobante3; ?>">
                        <div class="col-md-10">
                          <input width="50px" height="80px" data-num_aux="3" data-idaux="<?php echo $bt->id;?>" class="img" id="comprobante3_<?php echo $bt->id; ?>" type="file">
                        </div>
                      </div>
                    </div>
                    <div class="row" id="cont_apor_esp_ter_<?php echo $bt->id; ?>" <?php if($bt->aportacion!="2") echo 'style="display: none"'; ?>> 
                      <div class="col-md-1"></div>
                      <div class="col-md-9 form-group">
                        <label>Descripción del bien</label>
                        <div class="d-flex mb-2">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input class="form-control" type="text" name="descrip_bien" <?php echo "value='$bt->descrip_bien'"; ?>>
                          </div>
                          <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_descripcion_ayuda()">
                            <i class="mdi mdi-help"></i>
                          </button>              
                        </div>
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Monto de la aportación</label>
                        <input class="form-control monto" type="text" id="monto_aporta2" name="monto_aporta" placeholder="$" <?php $monto_aporta = number_format($bt->monto_aporta,2,'.',','); echo "value='$monto_aporta'"; ?> onchange="confirmarPagosMonto()">
                      </div>
                      
                    </div>
                    <div class="row" id="cont_apor_num_ter2_<?php echo $bt->id; ?>" <?php if($bt->aportacion!="1") echo 'style="display: none"'; ?>> 
                      <div class="col-md-1"></div>
                      <div class="col-md-4 form-group">
                        <label>Valor total pactado del inmueble en preventa en moneda nacional</label>
                        <input class="form-control monto" type="text" id="valor_pactado" placeholder="$" name="valor_pactado" <?php $valor_pactado = number_format($bt->valor_pactado,2,'.',','); echo "value='$valor_pactado'"; ?> onchange="confirmarPagosMonto()">
                      </div>
                    </div>
                    <div class="col-md-2 form-group" align="end">
                      <label style="color: transparent;">agregar tercero</label>
                      <button type="button" onclick="eliminarTercero(<?php echo $bt->id; ?>)" class="btn gradient_nepal2"><i class="fa fa-trash"></i> Eliminar Tercero</button>
                    </div>
                  </td>
                </tr>
              <?php } 
              } ?>
              </tbody>
            </table>  
          </div>
        
        <!-- termina aportacion 3 -->

        <!-- empieza aportacion 4 -->
        <form id="form_aporta4" class="forms-sample">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_aportacion" id="tipo_aportacion4" value="4" onclick="select_aportacion()" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="4") echo "checked"; ?>>
                  Crédito o préstamo financiero
                </label>
              </div>
            </div>-->
          </div>
          <!-- -->
          <div class="credito_finan_text" style="display: none">
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-5 form-group">
                <label>Tipo de institución financiera:</label>
                <select class="form-control" id="tipo_inst" name="tipo_inst">
                  <option value="" selected=""></option>
                  <?php foreach ($ti->result() as $k){?>
                    <option value="<?php echo $k->clave ?>" <?php if(isset($b) && $k->clave==$b->tipo_inst) echo 'selected' ?>><?php echo $k->tipo ?></option>
                  <?php } ?> 
                </select>
              </div>
              <div class="col-md-6 form-group">
                <label>Institución que otorga el financiamiento:</label>
                <input class="form-control" type="text" name="insti_financiamiento" <?php if(isset($b) && $b->tipo_aportacion=="4") echo "value='$b->insti_financiamiento'"; ?>>
              </div>
            </div>
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-5 form-group">
                <label>Tipo de crédito:</label>
                <select class="form-control" name="tipo_credito">
                  <option value="" selected=""></option>
                  <option value="1" <?php if(isset($b) && $b->tipo_credito=="1") echo 'selected' ?>>Crédito Corporativo</option>
                  <option value="2" <?php if(isset($b) && $b->tipo_credito=="2") echo 'selected' ?>>Crédito Puente</option>
                  <option value="3" <?php if(isset($b) && $b->tipo_credito=="3") echo 'selected' ?>>Crédito Hipotecario</option>
                </select>
              </div>
              <div class="col-md-6 form-group">
                <label>Monto del préstamo:</label>
                <input class="form-control monto" type="text" placeholder="$" name="monto_prestamo" id="monto_prestamo" <?php if(isset($b) && $b->tipo_aportacion=="4") { $monto_prestamo = number_format($b->monto_prestamo,2,'.',','); echo "value='$monto_prestamo'"; } ?>>
              </div>
            </div>
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-4 form-group">
                <label>Plazo en meses del préstamo:</label>
                <input class="form-control" type="text" name="plazo" <?php if(isset($b) && $b->tipo_aportacion=="4") echo "value='$b->plazo'"; ?>>
              </div>
              <div class="col-md-1">
                <br>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_plazo_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>
              </div>
              <div class="col-md-6 form-group">
                <label>Tipo de moneda o divisa del préstamo:</label>
                <select class="form-control" name="tipo_moneda" id="tipo_moneda_4" onchange="confirmarPagosMonto()">
                  <?php
                  /*$pesomxc=1;   
                  foreach ($tm->result() as $t) {
                  $select="";
                    if($t->id==$pesomxc){
                        if(isset($b) && $b->tipo_moneda==$t->id && $b->tipo_aportacion=="4"){ $select="selected"; }
                      echo "<option value='$t->id'$select>$t->moneda</option>";
                    } 
                  } */ ?>
                  <option value="0" <?php if(isset($b) && $b->tipo_moneda=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                  <option value="1" <?php if(isset($b) && $b->tipo_moneda=="1") echo "selected"; ?>>Dólar Americano</option>
                  <option value="2" <?php if(isset($b) && $b->tipo_moneda=="2") echo "selected"; ?>>Euro</option>
                  <option value="3" <?php if(isset($b) && $b->tipo_moneda=="3") echo "selected"; ?>>Libra Esterlina</option>
                  <option value="4" <?php if(isset($b) && $b->tipo_moneda=="4") echo "selected"; ?>>Dólar canadiense</option>
                  <option value="5" <?php if(isset($b) && $b->tipo_moneda=="5") echo "selected"; ?>>Yuan Chino</option>
                  <option value="6" <?php if(isset($b) && $b->tipo_moneda=="6") echo "selected"; ?>>Centenario</option>
                  <option value="7" <?php if(isset($b) && $b->tipo_moneda=="7") echo "selected"; ?>>Onza de Plata</option>
                  <option value="8" <?php if(isset($b) && $b->tipo_moneda=="8") echo "selected"; ?>>Onza de Oro</option>
                </select>
              </div>
            </div>
          </div>
          <!-- termina aportacion 4 --> 
        </form>

        <!-- empieza aportacion 5 -->
        <form id="form_aporta5" class="forms-sample">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_aportacion" id="tipo_aportacion5" value="5" onclick="select_aportacion()" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="5") echo "checked"; ?>>
                  Crédito o préstamo no financiero
                </label>
              </div>
            </div>-->
          </div>
          <div class="credito_nofinan_text" style="display: none">
            <div class="row"> 
              <div class="col-md-1"></div>
              <div class="col-md-3 form-group">
                <label>Monto del préstamo:</label>
                <input class="form-control monto" type="text" placeholder="$" id="monto_prestamo2" name="monto_prestamo" <?php if(isset($b) && $b->tipo_aportacion=="5") { $monto_prestamo = number_format($b->monto_prestamo,2,'.',','); echo "value='$monto_prestamo'"; } ?> onchange="confirmarPagosMonto()">
              </div>
              <div class="col-md-3 form-group">
                <label>Plazo en meses del préstamo:</label>
                <input class="form-control" type="text" name="plazo" <?php if(isset($b) && $b->tipo_aportacion=="5") echo "value='$b->plazo'"; ?>>
              </div>
              <div class="col-md-5 form-group">
                <label>Tipo de moneda o divisa del préstamo:</label>
                <select class="form-control tipo_moneda_5" name="tipo_moneda" onchange="confirmarPagosMonto()">
                  <?php
                  /*$pesomxc=1;   
                  foreach ($tm->result() as $t) {
                  $select="";
                    if($t->id==$pesomxc){
                        if(isset($b) && $b->tipo_moneda==$t->id && $b->tipo_aportacion=="5"){ $select="selected"; }
                      echo "<option value='$t->id'$select>$t->moneda</option>";
                    } 
                  } */ ?>
                  <option value="0" <?php if(isset($b) && $b->tipo_moneda=="0") echo "selected"; ?>>Pesos Mexicanos</option>
                  <option value="1" <?php if(isset($b) && $b->tipo_moneda=="1") echo "selected"; ?>>Dólar Americano</option>
                  <option value="2" <?php if(isset($b) && $b->tipo_moneda=="2") echo "selected"; ?>>Euro</option>
                  <option value="3" <?php if(isset($b) && $b->tipo_moneda=="3") echo "selected"; ?>>Libra Esterlina</option>
                  <option value="4" <?php if(isset($b) && $b->tipo_moneda=="4") echo "selected"; ?>>Dólar canadiense</option>
                  <option value="5" <?php if(isset($b) && $b->tipo_moneda=="5") echo "selected"; ?>>Yuan Chino</option>
                  <option value="6" <?php if(isset($b) && $b->tipo_moneda=="6") echo "selected"; ?>>Centenario</option>
                  <option value="7" <?php if(isset($b) && $b->tipo_moneda=="7") echo "selected"; ?>>Onza de Plata</option>
                  <option value="8" <?php if(isset($b) && $b->tipo_moneda=="8") echo "selected"; ?>>Onza de Oro</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-5">
                 <h4>Acreedores</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-5 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_persona" id="fisica_nof" value="1" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="5" && $b->tipo_persona=="1") echo "checked"; ?>>
                    Persona física:
                  </label>
                </div>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_persona" id="moral_nof" value="2" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="5" && $b->tipo_persona=="2") echo "checked"; ?>>
                    Persona moral:
                  </label>
                </div>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tipo_persona" id="fide_nof" value="3" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="5" && $b->tipo_persona=="3") echo "checked"; ?>>
                    Fideicomiso:
                  </label>
                </div>
              </div>
            </div> 
            <div id="cont_persfi_nof"> 
              <div class="row"> 
                <div class="col-md-1"></div>
                <div class="col-md-11 form-group">
                  <label>Nombre - los apellidos completos que corresponda y nombre(s).</label>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-3 form-group">
                  <label>Nombre(s):</label>
                  <input class="form-control" type="text" name="nombre" <?php if(isset($b) && $b->tipo_aportacion=="5") echo "value='$b->nombre'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno:</label>
                  <input class="form-control" type="text" name="app" <?php if(isset($b) && $b->tipo_aportacion=="5") echo "value='$b->app'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno:</label>
                  <input class="form-control" type="text" name="apm" <?php if(isset($b) && $b->tipo_aportacion=="5") echo "value='$b->apm'"; ?>>
                </div>
              </div>
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-4 form-group">
                  <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                  <input max="<?php echo date("Y-m-d"); ?>" class="form-control" type="date" name="fecha_nac" <?php if(isset($a->fecha_nac) && $b->tipo_aportacion=="5") echo "value='$a->fecha_nac'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?>>
                </div>
                <div class="col-md-3 form-group">
                  <label>País de nacionalidad:</label>
                  <select class="form-control" id="pais_nacionalidad" name="pais_nacionalidad">
                    <?php 
                      foreach ($get_pais as $p){
                        $select="";
                        if(isset($b) && $b->pais_nacionalidad==$p->clave && $b->tipo_aportacion=="5"){ 
                          $select="selected"; 
                          echo "<option value='$p->clave' $select>$p->pais</option>";
                        }else if(!isset($b) && $p->clave=="MX"){
                          $select="selected";
                        } 
                        echo "<option value='$p->clave' $select>$p->pais</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  <label>R.F.C:</label> 
                  <input class="form-control" onblur="ValidRfc(this)" type="text" name="rfc" <?php if(isset($b->rfc) && $b->tipo_aportacion=="5") echo "value='$b->rfc'"; ?>>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4 form-group">
                  <label>CURP:</label>
                  <input class="form-control" onblur="ValidCurp(this)" type="text" name="curp" <?php if(isset($b->curp) && $b->tipo_aportacion=="5") echo "value='$b->curp'"; ?>>
                </div>
                <div class="col-md-3 form-group">
                  <label>Actividad económica u ocupación</label>
                  <select class="form-control" name="actividad_ocupa">
                    <?php foreach ($get_actividad as $item) { 
                      $select="";
                      if(isset($b) && $b->actividad_ocupa==$item->clave && $b->tipo_aportacion=="5"){ $select="selected"; }
                        echo "<option value='$item->clave' $select>$item->acitividad</option>";
                      } ?>
                    </select>
                </div>
              </div>
            </div>

            <!-- persona moral -->
            <div class="row" id="cont_persmor_nof" style="display: none">
              <div class="col-md-1"></div>
              <div class="col-md-3 form-group">
                <label>Denominación o Razón Social:</label>
                <input class="form-control" type="text" name="razon_social" <?php if(isset($b->razon_social) && $b->tipo_aportacion=="5") echo "value='$b->razon_social'"; ?>>
              </div>
              <div class="col-md-4 form-group">
                <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                <input max="<?php echo date("Y-m-d"); ?>" class="form-control" type="date" name="fecha_consti" <?php if(isset($a->fecha_consti) && $b->tipo_aportacion=="5") echo "value='$a->fecha_consti'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?>>
              </div>
              <div class="col-md-4 form-group">
                <label>País de nacionalidad:</label><br>
                <select class="form-control" name="pais_nacion_moral">
                  <?php 
                    foreach ($get_pais as $p){
                      $select="";
                      if(isset($b) && $b->pais_nacion_moral==$p->clave && $b->tipo_aportacion=="5"){ 
                        $select="selected"; 
                        echo "<option value='$p->clave' $select>$p->pais</option>";
                      }else if(!isset($b) && $p->clave=="MX"){
                        $select="selected";
                      } 
                      echo "<option value='$p->clave' $select>$p->pais</option>";
                    }
                  ?>
                </select>
              </div> 
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-11 form-group">
                  <label>Representante o apoderado legal o persona que realice la aportación o nombre de la persona moral.</label>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3 form-group">
                  <label>Nombre(s):</label>
                  <input class="form-control" type="text" name="nombre_moral" <?php if(isset($b->nombre_moral) && $b->tipo_aportacion=="5") echo "value='$b->nombre_moral'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno:</label>
                  <input class="form-control" type="text" name="app_moral" <?php if(isset($b->app_moral) && $b->tipo_aportacion=="5") echo "value='$b->app_moral'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno:</label>
                  <input class="form-control" type="text" name="apm_moral" <?php if(isset($b->apm_moral) && $b->tipo_aportacion=="5") echo "value='$b->apm_moral'"; ?>>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3 form-group">
                  <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                  <input class="form-control" type="date" name="fecha_nac_moral" <?php if(isset($a->fecha_nac_moral) && $b->tipo_aportacion=="5") echo "value='$a->fecha_nac_moral'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>Registro Federal del Contribuyentes (R.F.C):</label>
                  <input class="form-control" type="text" onblur="ValidRfc(this)" name="rfc_moral" <?php if(isset($b->rfc_moral) && $b->tipo_aportacion=="5") echo "value='$b->rfc_moral'"; ?>>
                </div>
                <div class="col-md-4 form-group">
                  <label>CURP:</label>
                  <input class="form-control" type="text" onblur="ValidCurp(this)" name="curp_moral" <?php if(isset($b->curp_moral) && $b->tipo_aportacion=="5") echo "value='$b->curp_moral'"; ?>>
                </div>
              </div>
            </div>
            <!-- persona moral -->

            <!-- fideicomiso -->
            <div class="row" id="cont_fide_nof" style="display: none">
              <div class="col-md-1"></div>
              <div class="col-md-4 form-group">
                <label>Denominación o Razón Social:</label>
                <input class="form-control" type="text" name="razon_social_fide" <?php if(isset($b->razon_social_fide) && $b->tipo_aportacion=="5") echo "value='$b->razon_social_fide'"; ?>>
              </div>
              <div class="col-md-3 form-group">
                <label>Registro Federal del Contribuyentes (R.F.C) del Fideicomiso:</label>
                <input class="form-control" onblur="ValidRfc(this)" type="text" name="rfc_fide" <?php if(isset($b->rfc_fide) && $b->tipo_aportacion=="5") echo "value='$b->rfc_fide'"; ?>>
              </div>
              <div class="col-md-4 form-group">
                <label>Número de referencia o identificadordel fideicomiso:</label>
                <input class="form-control" type="text" name="curp_fide" <?php if(isset($b->curp_fide) && $b->tipo_aportacion=="5") echo "value='$b->curp_fide'"; ?>>
              </div>
            </div>
            <!-- fideicomiso -->
            <hr class="subtitle">


          </div>
        </form>
        <!-- termina aportacion 5 -->

        <!-- empieza aportacion 6 -->
        <form id="form_aporta6" class="forms-sample">
          <div class="row">
            <!--<div class="col-md-12 form-group">
              <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_aportacion" id="tipo_aportacion6" value="6" onclick="select_aportacion()" <?php if(isset($b->tipo_aportacion) && $b->tipo_aportacion=="6") echo "checked"; ?>>
                  Aportación a través de financiamiento bursátil (emisión de deuda capital)
                </label>
              </div>
            </div>-->
          </div>
          <div class="finan_burs" style="display: none">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-3 form-group">
                <label><i class="fa fa-calendar"></i> Fecha de emisión:</label>
                <input class="form-control" type="date" name="fecha_emision" <?php if(isset($a->fecha_emision)) echo "value='$a->fecha_emision'"; else { ?> value="<?php echo date('Y-m-d') ?>" <?php } ?>>
              </div>
              <div class="col-md-4 form-group">
                <label>Monto solicitado en la emisión en moneda nacional:</label>
                <input class="form-control monto" type="text" id="monto_estimado6" name="monto_estimado" <?php if(isset($b->monto_estimado) && $b->tipo_aportacion=="6") { $monto_estimado = number_format($b->monto_estimado,2,'.',','); echo "value='$monto_estimado'"; } ?> placeholder="$" onchange="confirmarPagosMonto()">
              </div>
              <div class="col-md-4 form-group">
                <label>Monto recibido en moneda nacional:</label>
                <input class="form-control monto" type="text" id="monto_recibido" name="monto_recibido" <?php if(isset($b->monto_recibido) && $b->tipo_aportacion=="6") { $monto_recibido = number_format($b->monto_recibido,2,'.',','); echo "value='$monto_recibido'"; } ?> placeholder="$" onchange="confirmarPagosMonto()">
              </div>
            </div>
          </div>
          <div class="row"> 
            <div class="col-md-4 form-group">
              <hr class="subtitle">
              <label>Monto total en efectivo</label>
              <input readonly="" class="form-control" type="text" id="total_liquida_efect" value="$0.00">
              <div class="col-md-8" align="center">
              <br><br>
                <h5 class="limite_efect_msj" style="color: red"></h5>

              </div>
            </div>
            <div class="col-md-8" align="center">
              <br><br>
              <h5 class="validacion_cantidad" style="color: red"></h5>
            </div>
          </div>
          <div class="row"> 
            <div class="col-md-4 form-group">
              <label>Monto total de liquidaciones</label>
              <input readonly="" class="form-control" type="text" id="total_liquida" value="$0.00">
            </div>
          </div>
        </form>
        <!-- termina aporatacion 6 -->

        <div class="modal-footer">
          <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="guardar()"><i class="fa fa-save"></i> Registrar Transacción</button>
          <a class="btn gradient_nepal2 regresar" href="#" onclick="regresar()" id="regresa" type=""><i class="fa fa-arrow-left"></i> Regresar</a>
        </div>
        

    	</div>
    </div>
	</div>
</div>

<div class="modal fade" id="modal_referencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Referencia</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo es obligatorio, acepta números y letras, longitud mínima 1 carácter y máxima 14 caracteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_no_factura_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>No. Factura</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>El campo no es obligatorio.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_descripcion_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Descripción del bien</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Longitud mínima 1 carácter y máxima 3000.</li>
           <li>Acepta letras, números, espacios, coma, punto, dos puntos y diagonal.</li>
           <li>No acepta paréntesis.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_licencia_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Identificador único de autorización</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Acepta números y letras, guión bajo y guión medio, ceros a la izquierda. Longitud de 1 a 200 carácteres.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_nsocios_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Número de socios de la operación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo numérico mínimo 1 digito, máximo 8 dígitos.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_plazo_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Plazo en meses del préstamo</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Campo numérico mínimo 1 digito, máximo 8 dígitos.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--modals modificatorio de ayuda-->
<div class="modal fade" id="modal_referenciam_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Referencia modificación</h5>
       <span style="text-align : justify; font-size: 14px">
           <li>Se debe capturar la misma referencia del aviso original que se modificará</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_foliom_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio de aviso que se modificará</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Es el "folio aviso" del acuse que generó el SAT (accesar en el portal del SAT a Avisos e informes, seleccionar ver acuses, buscar el aviso enviado, seleccionar ver detalle y el dato a obtener es "folio aviso")</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>