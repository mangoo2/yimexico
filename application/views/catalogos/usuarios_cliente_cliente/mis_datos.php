<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-8">
                      <h3>Mis datos</h3>
                    </div>
                    <div class="col-md-4" align="right">
                      <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Inicio_cliente"> <i class="fa fa-arrow-left"></i> Regresar</a>
                      <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
                    </div> 
                  </div>
                  
                  
                  <hr class="subtitle">
                    <div class="row">
                      <div class="col-md-12">
                        <h5 style="color: black">Nombre: <?php echo $get_u_c->nombre.' '.$get_u_c->apellido_paterno.' '.$get_u_c->apellido_materno ?></h5>
                      </div>  
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h5 style="color: black">Función / Puesto: <?php echo $get_u_c->funcion_puesto ?></h5>
                      </div>  
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <?php 
                          $aux_t='';
                          if($get_t_v2==1){
                            $aux_t=$get_t_v->nombre;
                          }else{
                            $aux_t='';
                          } 
                        ?>
                        <h5 style="color: black">Dirección: <?php echo $aux_t.' '.$get_u_c->calle.' No. Ext '.$get_u_c->no_ext.' No. Int '.$get_u_c->no_int.' col.'.$get_u_c->colonia.', '.$get_u_c->municipio_delegacion.', '.$get_u_c->localidad.', '.$estado.', '.$get_p->pais.'.' ?></h5>
                      </div>  
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <hr class="subtitle">
                        <h4>Acceso a sistema</h4>
                      </div>
                    </div>  
                  <form class="form" method="post" role="form" id="form_usuario"> 
                    <input class="form-control" type="hidden" name="UsuarioID" value="<?php echo $get_u_u->UsuarioID ?>">  
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Usuario</label>
                        <input class="form-control" type="text" name="Usuario" readonly="" value="<?php echo $get_u_u->Usuario ?>">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Contraseña</label>
                        <input class="form-control" type="password" name="contrasena" id="contrasena" oninput="verificarpass()">
                        <label style="color: red">Estimado usuario, favor de ingresar al menos 6 caracteres.</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <label>Verificar Contraseña</label>
                        <input class="form-control" type="password" id="contrasenav" oninput="verificarpass()">
                        <label class="text_validar_pass" style="color: red;"></label>
                      </div>
                    </div>
                  </form>  
                      <button type="button" class="btn gradient_nepal2 guardar_registro"><i class="fa  fa-floppy-o"></i> Guardar</button>
                      </form>
                    </div>
                    </div>
              </div>
            </div>