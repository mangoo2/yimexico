<style type="text/css">
	.form-check .form-check-label input[type="checkbox"]:checked + .input-helper:before {
	    background: #323556;
	    border-width: 0;
	}
	.form-check .form-check-label input[type="checkbox"] + .input-helper:before {
	    content: "";
	    width: 18px;
	    height: 18px;
	    border-radius: 2px;
	    border: solid #323556;
	    border-width: 2px;
	}    
	.bg-success, .swal2-modal .swal2-buttonswrapper .swal2-styled.swal2-confirm, .settings-panel .color-tiles .tiles.success {
	    background-color: #b07f4a !important;
	}
	.progress.progress-md {
	    height: 19px;
	    border-radius: 10px;
	}
	.bg-danger, .settings-panel .color-tiles .tiles.danger {
	    background-color: #b07f4a !important;
	}
</style>
<span class="id_bene" hidden><?php echo $id_bene ?></span>
<span class="id_benef_m" hidden><?php echo $id_bene ?></span>
<span class="tipo_persona" hidden><?php echo $tipo_persona ?></span>
<span class="idperfilamiento_cliente" hidden><?php echo $idperfilamiento_cliente ?></span>
<span class="idcientec" hidden><?php echo $idclientec ?></span>
<span class="idopera" hidden><?php echo $idopera ?></span>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
		  <div class="card-body">
		  	<div class="row">
		      <div class="col-md-9">  
		        <h3>Recopilación de documentos del Dueño Beneficiario de <?php echo $titulo_t ?></h3>
		      </div>
		      <div class="col-md-3" align="right">
              	<button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar</button>
               </div>  
		    </div>  
		    <hr class="subtitle"> 
		    <!--- -->
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
					      <?php echo $text ?><!---->
						</div>
					</div>	
					<br>  
					<!---->
					<div class="col-md-12 grid-margin stretch-card">
		              	<div class="card">
			                <div class="card-body">
			                  <strong>Progreso de captura de documentos</strong>
			                  <div class="template-demo">
			                    <div class="progress progress-md" id="cont_barra">
			                     	
			                    </div>
			                  </div>
			                </div>
		              	</div>
		            </div>
					<!-- -->              
		        </div>	
	        	<div class="col-md-6">
	        		<form id="form_docs_cliente" class="form">
	        			<?php if(isset($docs)) {?>
				            <input type="hidden" name="id" id="id" value="<?php echo $docs->id ?>">
				          <?php } else { ?>
				          	<input type="hidden" name="id" id="id" value="0">
				          <?php }?>
		        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
						    <div class="card-body">
						    <!-- -->
	                           <div class="row">
					              <div class="col-md-12">
					                <div class="d-flex mb-2">
						                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
						                	<h5>Documentos a recolectar (El nombre de los documentos debe ir sin punto)</h5>
						                </div>
						                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="tamano_ayuda()">
						                  <i class="mdi mdi-help"></i>
						                </button>
              						</div>
					              </div>
					            </div> 
					            <div class="col-md-12">
					            	<span>*Por favor, recolecte una copia digital y coteje contra original los siguientes documentos.</span>
					            </div>
					            <div class="col-md-12 form-group" id="cont2">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Consulta de listas de personas bloqueadas":
					                	<input type="checkbox" class="form-check-input" id="Chimg0" value="" checked="">
					                </label>
					                <span>Resultado:</span> <button type="button" class="btn gradient_nepal2" onclick="formatoPersonasBloquedas()"><i class="fa fa-eye"></i> Ver detalle</button>
					              </div>
					            </div>
					            <!--
					            <div class="col-md-12 form-group" id="cont1">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Consulta en listado de personas bloqueadas":
					                  <input type="checkbox" class="form-check-input" id="archt_1">
					                </label>
					              </div>


					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_1" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            -->
					            <?php if($tipo_persona==1 || $tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont2">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Conoce a tu cliente":
					                  <input type="checkbox" class="form-check-input" id="Chimg2" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgcc" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_persona==1){ ?> 
					            <div class="col-md-12 form-group" id="cont3">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Identificación oficial:
					                  <input type="checkbox" class="form-check-input" id="archt_3">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_3" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            
					            <?php if($tipo_persona==1){ ?> 
					            <div class="col-md-12 form-group" id="cont9">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">CURP:
					                  <input type="checkbox" class="form-check-input" id="archt_5">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_5" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <div class="col-md-12 form-group" id="cont4">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Cédula de identificación fiscal:
					                  <input type="checkbox" class="form-check-input" id="archt_4">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_4" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php if($tipo_persona==1 || $tipo_persona==2){ ?> 
					            <div class="col-md-12 form-group" id="cont10">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Comprobante de domicilio:
					                  <input type="checkbox" class="form-check-input" id="archt_6">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_6" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Identificación oficial del apoderado legal y/o servidor público:
					                  <input type="checkbox" class="form-check-input" id="Chimg15" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgineal" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
	                            <?php } ?>
	                            <?php if($tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont12">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Escritura constitutiva o instrumento público que acredite su existencia:
					                  <input type="checkbox" class="form-check-input" id="Chimg12" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="esc_ints" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_persona==2 || $tipo_persona==3){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					              	<?php if($tipo_persona==2){ ?> 
					                	<label class="form-check-label">Poderes Notariales del apoderado legal o documento para comprobar las facultades del servidor público:
					               	<?php } ?>
					               	<?php if($tipo_persona==3){ //fideicomiso ?> 
					                	<label class="form-check-label">Poder notarial para comprobar las facultades del apoderado legal:
					               	<?php } ?>
					                  <input type="checkbox" class="form-check-input" id="Chimg13" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="poder_not_fac_serv" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } 
					            $idcliente_usuario=$this->session->userdata('idcliente_usuario');
					            $tipo=$this->session->userdata('tipo');
					            $menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,2);
					            $perfilid=$this->session->userdata('perfilid');
					            if($perfilid==3){
					            	echo '<div class="row row_file valid" style:"display:none">
		                        		<div class="form-check form-check-purple">
						                	<label class="form-check-label"><h4>Documentos Validados</h4>
						                  	<input type="checkbox" id="validado" class="form-check-input" name="validado" value="">
						                	</label>
						              	</div>
						          	</div>';
					            }else{
					            	foreach ($menu as $x) { 
						                if($x->id_menusub==16){ // Valdiar docs
						                  	if($x->check_menu==1){ ?>
						                    	<div class="row row_file valid">
					                        		<div class="form-check form-check-purple">
									                	<label class="form-check-label"><h4>Documentos Validados</h4>
									                  	<input type="checkbox" id="validado" class="form-check-input" name="validado" value="">
									                	</label>
									              	</div>
									          	</div>
						                 	<?php }
						                }
						            } 
					            }
	                        	?>
					            
					          	<div id="headingCollapse12" class="card-header">
			                      <a data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12" class="card-title lead collapsed" id="mas_docs_bene"><i class="fa fa-arrow-down"></i> Mas Documentos</a>
			                    </div>
			                    <div id="collapse12" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" aria-expanded="false">
			                    <div class="card-body">
			                    	<form id="form_docs_extra" class="form" method="post" role="form">
				                        <div class="card-block">
				                        	<?php if(isset($docs_ext)) {?>
									            <input type="hidden" id="id_extra" value="<?php echo $docs_ext->id ?>">
									        <?php } else { ?>
									          	<input type="hidden" id="id_extra" value="0">
									        <?php }?>

				                          	<div class="row">
				                                <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 1:
									                  	<input type="checkbox" class="form-check-input" id="Chext1" value="">
									                	</label>
									              	</div>
									              	<div class="row">
										              	<div class="row row_file">
											             	<div class="col-md-10">
											                	<input width="50px" height="80px" class="imgExt" id="extra" type="file">
											              	</div>
											              	<div class="col-md-10"></div>
											           </div>
											       </div><br>
										            <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip" id="descrip" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato" value="">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 2:
									                  	<input type="checkbox" class="form-check-input" id="Chext2" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra2" type="file">
										              	</div>
										              	<div class="col-md-10"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip2" id="descrip2" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 3:
									                  	<input type="checkbox" class="form-check-input" id="Chext3" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra3" type="file">
										              	</div>
										              	<div class="col-md-6"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip3" id="descrip3" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 4:
									                  	<input type="checkbox" class="form-check-input" id="Chext4" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra4" type="file">
										              	</div>
										              	<div class="col-md-10"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip4" id="descrip4" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 5:
									                  	<input type="checkbox" class="form-check-input" id="Chext5" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra5" type="file">
										              	</div>
										              	<div class="col-md-10"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip5" id="descrip5" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div> 

				                          	</div>
				                        </div>
			                    	</form>
			                      </div>
			                    </div>
						    </div>
						</div>
						<br>
			            <div class="row"> 
				            <div class="col-md-12" align="right">
				              <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="guardar_documentos()"><i class="fa fa-save"></i> Guardar </button>
				            </div>
			          	</div>
					</form>    	
	        	</div>

	        </div>
            <!-- -->
	    </div>
	</div>
</div>

<div class="modal fade" id="modal_carga_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Tamaño y caracteristicas en carga de archivos</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>El tamaño maximo es de 2.5mb por archivo, los nombres de los documentos no deben presentar puntos medios, los 
           formaros permitidos son: "jpg", "png", "pdf", "jpeg"</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>