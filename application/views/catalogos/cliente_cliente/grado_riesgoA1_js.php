<script src="<?php echo base_url(); ?>public/js/catalogos/clientes_cliente/grado_riesgoA1.js?v=<?php echo date('Ymdgis');?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		setTimeout(function(){ 
			calcularpaicesestadosriesgos();
		}, 1000);

		
	});
	function calcularpaicesestadosriesgos(){
		<?php 
			if($tipoc==1) {
				//$tabla = 'tipo_cliente_p_f_m';
				//$nacionalidad= $info->pais_nacionalidad;
				$pais= $info->pais_d;
				$estadoinfo=$info->estado_d;
			}
		    if($tipoc==2) {
		    	//$tabla = 'tipo_cliente_p_f_e';
		    	//$nacionalidad= $info->pais_nacionalidad;
		    	$pais= $info->pais_d;
				//$estadoinfo=$info->estado_d;
				if (is_numeric($info->estado_d)) {
				   $estadoinfo=$info->estado_d;
				}else{
					$estadoinfo=$info->estado_t;
				}
		    }
		    if($tipoc==3) {
		    	//$tabla = 'tipo_cliente_p_m_m_e';
		    	//$nacionalidad= $info->nacionalidad;
		    	$pais= $info->pais_d;
				$estadoinfo=$info->estado_d;
		    }
		    if($tipoc==4) {
		    	//$tabla = 'tipo_cliente_p_m_m_d';
		    	//$nacionalidad= $info->nacionalidad;
		    	$pais= "MX";
				$estadoinfo=$info->estado_d;
		    }
		    if($tipoc==5) {
		    	//$tabla = 'tipo_cliente_e_c_o_i';
		    	//$nacionalidad= $info->nacionalidad;
		    	$pais= $info->pais_d;
				$estadoinfo=$info->estado_d; 
		    }
		    if($tipoc==6) {
		    	//$tabla = 'tipo_cliente_f';
		    	//$nacionalidad= 0;
		    	$pais=0;
				$estadoinfo=0;
				$idestado=0;
				$id_estado=0;
		    }
		    $gradoriesgopaises_round=1;
		    $gradoriesgopaises=1;
		    $array_grados= array();
		    if($pais=='MX' && $tipoc!=6){
		    	$gradoresgostotal=0;
		    	$gradoresgostotal_count=0;
		    	$idestado=$estadoinfo;
		    	//echo 'Estado: '.$estadoinfo.'<br>';
		    	?>
		    	//$('#nacionalidad').val(1);
		    	<?php
		    	$estadoIdg=$this->General_model->get_tableRow("estado",array('id'=>$idestado));
		    	$id_estado=$estadoIdg->id;
		    	if($id_estado==0){
		    		$array_grados = array_merge($array_grados,array(1));
		    	}
		    	if($id_estado>0){
		    		if(isset($estadoIdg->riesgo) && $estadoIdg->riesgo!=""){
		    			$array_grados = array_merge($array_grados,array($estadoIdg->riesgo));
		    		}else{
		    			$array_grados = array_merge($array_grados,array(1));
		    		}
		    		$gradoresgostotal=$gradoresgostotal+$estadoIdg->riesgo;
		    		$gradoresgostotal_count++;	
		    	}
		    	if($id_estado>0){
		    		$datosestados_lf=$this->General_model->get_tablell("paises_lugares_frontera",array('id_estado'=>$id_estado));
		    		if($datosestados_lf->num_rows()>0){
		    			$datosestados_lf=$datosestados_lf->row();
			    		$array_grados = array_merge($array_grados,array($datosestados_lf->riesgo));
			    		
		    			$gradoresgostotal=$gradoresgostotal+$datosestados_lf->riesgo;
		    			$gradoresgostotal_count++;	
		    		}else{
		    			$array_grados = array_merge($array_grados,array(1));
	    			}
		    	}
		    	if($id_estado>0){
		    		$datosestados_ip=$this->General_model->get_tablell("paises_indice_paz",array('id_estado'=>$id_estado));
		    		if($datosestados_ip->num_rows()>0){
		    			$datosestados_ip=$datosestados_ip->row();

			    		$array_grados = array_merge($array_grados,array($datosestados_ip->riesgo));
		    			
		    			$gradoresgostotal=$gradoresgostotal+$datosestados_ip->riesgo;
		    			$gradoresgostotal_count++;	
		    		}else{
		    			$array_grados = array_merge($array_grados,array(1));
	    			}
		    	}
		    	if($id_estado>0){
		    		$datosestados_id=$this->General_model->get_tablell("paises_incidencia_delictiva",array('id_estado'=>$id_estado));
		    		if($datosestados_id->num_rows()>0){
		    			$datosestados_id=$datosestados_id->row();
		    			$array_grados = array_merge($array_grados,array($datosestados_id->riesgo));
		    			$gradoresgostotal=$gradoresgostotal+$datosestados_id->riesgo;
		    			$gradoresgostotal_count++;	
		    		}else{
		    			$array_grados = array_merge($array_grados,array(1));
	    			}
		    	}
		    	if($id_estado>0){
		    		$datosestados_cezu=$this->General_model->get_tablell("paises_percepcion_corrupcion_estatal_zonas_urbanas",array('id_estado'=>$id_estado));
		    		if($datosestados_cezu->num_rows()>0){
		    			$datosestados_cezu=$datosestados_cezu->row();
		    			$array_grados = array_merge($array_grados,array($datosestados_cezu->riesgo));
		    			$gradoresgostotal=$gradoresgostotal+$datosestados_cezu->riesgo;
		    			$gradoresgostotal_count++;	
		    		}else{
		    			log_message('error', 'datosestados_cezu es vacio: ');
		    			$array_grados = array_merge($array_grados,array(1));
	    			}
		    	}
		    	if($gradoresgostotal_count>0){
			    	echo 'var gradoresgostotal='.$gradoresgostotal.';
			    	';
			    	echo 'var gradoresgostotal_count='.$gradoresgostotal_count.';
			    	';
			    	$gradoriesgoestados = $gradoresgostotal/$gradoresgostotal_count;
			    	$gradoriesgoestados_round=round($gradoriesgoestados);
			    	echo 'var gradoriesgoestados='.$gradoriesgoestados_round.'; // sin redondedo '.$gradoriesgoestados.'
			    	';
			    }else{
			    	echo 'var gradoresgostotal=1;
			    	';
			    	echo 'var gradoresgostotal_count=0;
			    	';
			    	$gradoriesgoestados_round=1;
			    	echo 'var gradoriesgoestados='.$gradoriesgoestados_round.'; // sin redondedo '.$gradoriesgoestados.'
			    	';
			    }
		    	?>
		    		//$('#paises').val(<?php echo $gradoriesgoestados_round;?>);
		    		$('#paises').val(<?php echo max($array_grados);?>);
		    	<?php
		    	
		    }else if($pais!='MX' && $tipoc!=6){
		    	?>
		    	//$('#nacionalidad').val(2);
		    	<?php
		    	$id_estado=0;
		    	//echo "estadoinfo: ".$estadoinfo;
		    	$estadoIdg=$this->General_model->get_tableRow("estado",array('id'=>$estadoinfo));
		    	$id_estado=$estadoIdg->id;
		    	$datospais=$this->General_model->get_tablell("pais",array('clave'=>$pais));
		    	if($datospais->num_rows()>0){
		    		$datospais=$datospais->row();
		    		$id_pais = $datospais->id;
		    		$gradoresgospaistotal=0;
		    		$gradoresgospaistotal_count=0;
		    		if($id_estado==0){
			    		$array_grados = array_merge($array_grados,array(1));
			    	}
		    		if($id_pais>0){
		    			$datospais_ig=$this->General_model->get_tablell("paises_indice_gafi",array('id_pais'=>$id_pais));
			    		if($datospais_ig->num_rows()>0){
			    			$datospais_ig=$datospais_ig->row();
			    			$array_grados = array_merge($array_grados,array($datospais_ig->riesgo));
			    			$gradoresgospaistotal=$gradoresgospaistotal+$datospais_ig->riesgo;
			    			$gradoresgospaistotal_count++;	
			    		}else{
			    			$array_grados = array_merge($array_grados,array(1));
			    		}
		    		}
		    		if($id_pais>0){
		    			$datospais_ic=$this->General_model->get_tablell("paises_indice_corrupcion",array('id_pais'=>$id_pais));
			    		if($datospais_ic->num_rows()>0){
			    			$datospais_ic=$datospais_ic->row();
			    			$array_grados = array_merge($array_grados,array($datospais_ic->riesgo));
			    			$gradoresgospaistotal=$gradoresgospaistotal+$datospais_ic->riesgo;
			    			$gradoresgospaistotal_count++;	
			    		}else{
			    			$array_grados = array_merge($array_grados,array(1));
			    		}
		    		}
		    		if($id_pais>0){
		    			$datospais_icg=$this->General_model->get_tablell("paises_indice_corrupcion_global",array('id_pais'=>$id_pais));
			    		if($datospais_icg->num_rows()>0){
			    			$datospais_icg=$datospais_icg->row();
			    			$array_grados = array_merge($array_grados,array($datospais_icg->riesgo));
			    			$gradoresgospaistotal=$gradoresgospaistotal+$datospais_icg->riesgo;
			    			$gradoresgospaistotal_count++;	
			    		}else{
			    			$array_grados = array_merge($array_grados,array(1));
			    		}
		    		}
		    		if($id_pais>0){
		    			$datospais_gt=$this->General_model->get_tablell("paises_guia_terrorismo",array('id_pais'=>$id_pais));
			    		if($datospais_gt->num_rows()>0){
			    			$datospais_gt=$datospais_gt->row();
			    			$array_grados = array_merge($array_grados,array($datospais_gt->riesgo));
			    			$gradoresgospaistotal=$gradoresgospaistotal+$datospais_gt->riesgo;
			    			$gradoresgospaistotal_count++;	
			    		}else{
			    			$array_grados = array_merge($array_grados,array(1));
			    		}
		    		}
		    		if($gradoresgospaistotal_count>0){
			    		echo 'var gradoresgospaistotal='.$gradoresgospaistotal.';
				    	';
				    	echo 'var gradoresgospaistotal_count='.$gradoresgospaistotal_count.';
				    	';
				    	$gradoriesgopaises = $gradoresgospaistotal/$gradoresgospaistotal_count;
				    	$gradoriesgopaises_round=round($gradoriesgopaises);
				    	echo 'var gradoriesgopaises='.$gradoriesgopaises_round.'; // sin redondedo '.$gradoriesgopaises.'
				    	';
				    }else{
				    	echo 'var gradoresgospaistotal=1;
				    	';
				    	echo 'var gradoresgospaistotal_count=0;
				    	';
				    	$gradoriesgopaises_round=1;
				    	echo 'var gradoriesgopaises='.$gradoriesgopaises_round.'; // sin redondedo '.$gradoriesgopaises.'
				    	';
				    }
			    	?>
		    			//$('#paises').val(<?php echo $gradoriesgopaises_round;?>);
		    			$('#paises').val(<?php echo max($array_grados);?>);
		    		<?php
		    	}
		    }


		?>
		calculaGrado();
	}
</script>