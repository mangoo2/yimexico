<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css"><style type="text/css">
  .check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c;-webkit-print-color-adjust: exact;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
      background: #212b4c;-webkit-print-color-adjust: exact;
  }
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
      border-color: #212b4c;-webkit-print-color-adjust: exact;
  }
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
      background: #25294c;-webkit-print-color-adjust: exact;
  }
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
      border-color: #25294c;-webkit-print-color-adjust: exact;
  }
</style>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 >Cuestionario Ampliado para clientes PEP de alto riesgo - <?php echo $nombre; ?></h3>
        <hr class="subtitle">
        <!---------------->
        <form class="form" method="post" role="form" id="form_cuestionario_pep">
          <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
          <input type="hidden" name="id_operacion" id="idopera" value="<?php echo $idopera; ?>">
          <input type="hidden" name="id_actividad" id="id_actividad" value="<?php echo $id_actividad; ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idp; ?>">
          <div class="row">
            <div class="col-md-6 form-group">
              <label>Actividad conforme a su Alta Fiscal:</label>
              <select class="form-control actividad_fisica_2" name="actividad_conforme_alta_fiscal">
                <option disabled selected>Selecciona una opción</option>
                <?php foreach ($get_actividad as $item) {
                  $selected="";
                  if($actividad_conforme_alta_fiscal==$item->clave){
                    $selected="selected";
                  }
                  if($tcc==1 || $tcc==2)
                    echo '<option value="'.$item->clave.'" '.$selected.'>'.$item->acitividad.'</option>';
                  //if($tcc==2)
                  else
                    echo '<option value="'.$item->clave.'" '.$selected.'>'.$item->giro_mercantil.'</option>';  
                } ?>
              </select>
            </div>
            <!--<div class="col-md-12 form-group">
              <label>Actividad conforme a su Alta Fiscal:</label>
              <input class="form-control" type="text" name="actividad_conforme_alta_fiscal" value="<?php echo $actividad_conforme_alta_fiscal; ?>">
            </div>-->
          </div>
          <div class="row">
            <div class="col-md-12 form-group">
              <label>Indicar el último año en el cargo de la figura pública o el año en que finalizará su cargo como figura pública:</label>
              <input class="form-control" type="text" name="indicar_ultimo_anio_pago_figura_publica" value="<?php echo $indicar_ultimo_anio_pago_figura_publica; ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 form-group">
              <label>¿En qué dependencia /entidad labora?</label>
              <input class="form-control" type="text" name="dependencia_entidad_laboral" value="<?php echo $dependencia_entidad_laboral; ?>">
            </div>
          </div>
          <?php
            $ingreso_proviene_fuente_gobiernotx1='';
            $ingreso_proviene_fuente_gobiernotx2='';
            if($ingreso_proviene_fuente_gobierno==1){
              $ingreso_proviene_fuente_gobiernotx1='checked';
              $ingreso_proviene_fuente_gobiernotx2='';
            }else{
              $ingreso_proviene_fuente_gobiernotx1='';
              $ingreso_proviene_fuente_gobiernotx2='checked';
            }
          ?>
          <div class="row">
            <div class="col-md-5">
              <label>¿La fuente de ingresos proviene de alguna fuente del gobierno?                              
              </label>
              <div class="row">
                <div class="col-md-4 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="ingreso_proviene_fuente_gobierno" onclick="ingreso_proviene()" value="1" <?php echo $ingreso_proviene_fuente_gobiernotx1 ?>>
                      Si
                    </label>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="ingreso_proviene_fuente_gobierno" onclick="ingreso_proviene()" value="2" <?php echo $ingreso_proviene_fuente_gobiernotx2 ?>>
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <!--  En caso de que la respuesta sea afirmativa indicar  -->
            <div class="col-md-7 form-group">
              <div class="texto_fuente_ingreso" style="display: none">
                <label>¿Cuál es la fuente de ingresos?</label>
                <input class="form-control" type="text" name="fuente_ingreso_gobierno" value="<?php echo $fuente_ingreso_gobierno; ?>">
              </div>
            </div>
          </div>
          <?php
            $fuente_ingresotx1='';
            $fuente_ingresotx2='';
            if($fuente_ingreso==1){
              $fuente_ingresotx1='checked';
              $fuente_ingresotx2='';
            }else{
              $fuente_ingresotx1='';
              $fuente_ingresotx2='checked';
            }
          ?>
          <div class="row">
            <div class="col-md-5">
              <label>¿Tiene alguna otra fuente de ingresos?</label>
              <div class="row">
                <div class="col-md-4 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="fuente_ingreso"  onclick="otra_fuente_ingreso()" value="1" <?php echo $fuente_ingresotx1 ?>>
                      Si
                    </label>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="fuente_ingreso"  onclick="otra_fuente_ingreso()" value="2" <?php echo $fuente_ingresotx2 ?>>
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <!--  En caso de que la respuesta sea afirmativa indicar  -->
            <div class="col-md-7 form-group">
              <div class="text_fuente_ingreso" style="display: none">
                <label>¿Cuál es la otra fuente de ingresos?</label>
                <input class="form-control" type="text" name="cual_fuente_ingreso" value="<?php echo $cual_fuente_ingreso; ?>">
              </div>
            </div>
          </div>
          <?php
            $ingreso_extranjerotx1='';
            $ingreso_extranjerotx2='';
            if($ingreso_extranjero==1){
              $ingreso_extranjerotx1='checked';
              $ingreso_extranjerotx2='';
            }else{
              $ingreso_extranjerotx1='';
              $ingreso_extranjerotx2='checked';
            }
          ?>
          <div class="row">
            <div class="col-md-5">
              <label>¿Recibe ingresos del extranjero? </label>
              <div class="row">
                <div class="col-md-4 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="ingreso_extranjero" onclick="recibe_ingreso_extranjero()" value="1" <?php echo $ingreso_extranjerotx1 ?>>
                      Si
                    </label>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="ingreso_extranjero" onclick="recibe_ingreso_extranjero()" value="2" <?php echo $ingreso_extranjerotx2 ?>>
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <!--  En caso de que la respuesta sea afirmativa indicar  -->
            <div class="col-md-7 form-group">
              <div class="text_de_que_pais" style="display: none">
                <label>¿De qué país(es)?</label>
                <input class="form-control" type="text" name="que_pais" value="<?php echo $que_pais; ?>">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 form-group">
              <label>Ingresos brutos mensuales totales (considerar todas las fuentes de ingresos):</label>
              <input class="form-control" type="text" name="ingresos_brutos_mensuales_totales" value="<?php echo $ingresos_brutos_mensuales_totales; ?>">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 form-group">
              <label>¿Tiene la capacidad o el acceso para mover recursos del gobierno?</label>
              <input class="form-control" type="text" name="tiene_capacidad_acceso_mover_recursos_gorbierno" value="<?php echo $tiene_capacidad_acceso_mover_recursos_gorbierno; ?>">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 form-group">
              <label>Actividad económica / ocupación de esposa(o):</label>
              <input class="form-control" type="text" name="actividad_econominca_ocupacion_esposo" value="<?php echo $actividad_econominca_ocupacion_esposo; ?>">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 form-group">
              <label>Actividad económica / ocupación de  hijos(as) en caso de ser mayores de edad:</label>
              <input class="form-control" type="text" name="actividad_economica_ocupacion_hijos" value="<?php echo $actividad_economica_ocupacion_hijos; ?>">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 form-group">
              <label>Explicar la razón por la cual se realiza la operación/transacción con la Entidad (naturaleza y propósito de la relación comercial):</label>
              <input class="form-control" type="text" name="explicar_razon_cual_operacion_trasanccion" value="<?php echo $explicar_razon_cual_operacion_trasanccion; ?>">
            </div>
          </div>
          
        </form>
        <div class="texto_firma" style="display: none">
          <div class="row">
            <div class="col-md-6" align="center">
              <h3 style="color: black;"><u> Firma Cliente </u></h3>
              
            </div>
            <div class="col-md-6" align="center">
              <h3 style="color: black;"><u>Firma Ejecutivo Comercial / Vendedor</u></h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6" align="center">
              <h6 style="color: black;">Doy fé que los datos  proporcionados son veridicos y están vigentes</h6>
              
            </div>
            <div class="col-md-6" align="center">
              <h6 style="color: black;">Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial </h6>
            </div>
          </div>
          <br>
        </div> 
        <div class="btn_guardar_text">
          <div class="row">
            <div class="col-md-12" align="right">
              <button type="button" class="btn gradient_nepal2" onclick="imprimir_for()"><i class="fa fa-print"></i> Imprimir formato</button>

              <button type="button" class="btn gradient_nepal2" onclick="guardar_cuestionario_pep()"><i class="fa  fa-floppy-o" ></i> Guardar</button>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </div>
</div>          

<div class="modal fade" id="modalimprimir" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" align="center">
            <label>¿Deseas imprimir el formulario?                
            </label>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4 form-group">
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="1">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="2" checked>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_registro" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
