<span class="perfil_v" hidden><?php echo $perfilid ?></span>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
            <div class="col-md-8">
              <h3 class="">Listado de mis Clientes</h3>
            </div>
            <div class="col-md-4" align="right">
              <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
              <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
            </div>  
          </div>   
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-2 form-group">
            <label>Tipo de listado</label>
            <select class="form-control" id="tipo_lista">
              <option value="1">Clientes</option>
              <option value="2">Clientes mancomunados</option>
            </select>
          </div>
          <?php if($perfilid!=10){ ?>   
          <div class="col-md-10" align="right">
            <a title="Exportar Clientes" class="btn gradient_nepal2" target="t_blanck" href="<?php echo base_url(); ?>Clientes_cliente/exportarClientes"><i class="fa fa-download"></i> <i class="fa fa-file-excel-o"></i> Clientes</a>
            <a title="Exportar Clientes+Beneficiarios" class="btn gradient_nepal2" target="t_blanck" href="<?php echo base_url(); ?>Clientes_cliente/exportarClientesBenes"><i class="fa fa-download"></i> <i class="fa fa-file-excel-o"></i> Clientes+Beneficiarios</a>
            <button type="button" data-toggle="modal" data-target="#modal_masivas" title="Carga masiva de clientes" class="btn gradient_nepal2"><i class="fa fa-download"></i> <i class="fa fa-upload"></i> <i class="fa fa-file-excel-o"></i> Cargas Masivas</button>
            <!--<a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Perfilamiento"><i class="fa fa-user-plus"></i> Nuevo cliente</a>
            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Clientes_cliente/union"><i class="fa fa-user-plus"></i> Nuevo mancomunado</a>-->
          </div>
          <?php } ?>
        </div>
        <div class="table-responsive">
          <table class="table" id="table_clientec">
            <thead>
              <tr>
                <th></th>
                <th>Nombre</th>
                <th>Fecha de última transacción</th>
                <!--<th>Operaciones</th>-->
                <th>Catálogos</th>
                <th>Acciones</th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Deseas eliminar este cliente?</h5>
      </div>
      <input type="hidden" id="idcliente">
      <input type="hidden" id="idtipocliente">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="detalles" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title ddaa">Histórico de Transacciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
      </div>
      <div class="modal-body" style="max-width: 93%">
        <span>
          <strong class="modal-header" id="nameCliente"></strong>
        </span> 
        <br>
        <table id='detalles_trans' class='table' width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Transacción</th>
              <th>Fecha</th>
              <th>Mes acuse</th>
              <th>Detalles</th>
            </tr>
          </thead>
          <tbody>

          </tbody>

        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bd-example-modal-lg" id="transacción_actividades" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title ddaa">Transacciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
      </div>
      <input type="hidden" id="idclientec">
      <input type="hidden" id="idperfila">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <span class="transaccion_acti"></span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="aceptarAct()" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_xml" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Historial de XML</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <table id='bita_xml' class='table' width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Actividad</th>
              <th>Fecha</th>
              <th>Descarga</th>
            </tr>
          </thead>
          <tbody>

          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>

<!--- Modal de cargas masivas -->
<div class="modal fade" id="modal_masivas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Carga masiva de clientes</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <div class="row">
          <div class="col-md-6 form-group">
            <label>Identifique el tipo de cliente:</label>
            <select style="font-weight: bold;" class="form-control" name="idtipo_cliente" id="idtipo_cliente">
              <option selected="" disabled="" value="0">Seleccione el tipo de cliente</option>
              <?php foreach ($get_tipo_cliente as $i) { ?>
                  <option value="<?php echo $i->idtipo_cliente ?>"><?php echo $i->tipo ?></option>
               <?php } ?>
            </select>
          </div>
          <div class="col-md-6">
            <form name="frmSubirCSV" id="frmSubirCSV" method="POST" enctype="multipart/form-data">
              <div class="col-md-12">
               <div class="form-group">
                   <label>Seleccione un cvs para cargar malla</label>
                   <input type="file" name="inputFile" id="inputFile" class="form-control cargar_docs_det" accept=".csv"> 
                </div>
               </div>
              <div class="col-md-10">
                <div class="form-group">
                  <button disabled type="button" title="click para cargar archivo seleccionado" id="cargar_docs_det" class="btn btn-success" data-dismiss="modal"><i class="fa fa-upload"></i> Carga Masiva</button>
                  <button type="button" name="cancelar" id="cancelar" class="btn btn-dark">Cancelar</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6 form-group">
            <button disabled type="button" title="Click para descargar malla ejemplo" class="btn btn-success" id="btn_down"> <i class="fa fa-download"></i> Descargar Malla </button>
          </div>
        </div>         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
