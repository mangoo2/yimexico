<!--<div class="row">
  <div class="col-md-12 form-group">
    <div class="form-check form-check-primary">
      <label class="form-check-label">C) Nombre del presidente del consejo de administración o administrador unico (según aplique, proviniente del acta constitutiva poderes).   
        <input type="radio" class="form-check-input" id="accionaria_socio_c" name="tipo_inciso" value="c">
      </label>
    </div>
  </div>
</div>-->
<div class="accion_socio_c" style="display: none"> 
		<div class="row">
			<div class="col-md-2 form-group">
			  <div class="form-check form-check-success">
			    <label class="form-check-label">
			      <input type="radio" checked class="form-check-input" name="tipo_persona_a" id="persona_fisica_c" value="1">
			      Persona fisica
			    </label>
			  </div>
			</div>  
		</div>	
		<form class="form" method="post" role="form" id="beneficiario_moral_f">
		<div class="row" id="pf_c">
		  <div class="col-md-4 form-group">
		    <label>Nombre(s):</label>
		    <input class="form-control" type="text" name="nombre" value="<?php if(isset($b) && $b->identificacion_beneficiario=="c") echo $b->nombre ?>">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Apellido paterno:</label>
		    <input class="form-control" type="text" name="app" value="<?php if(isset($b) && $b->identificacion_beneficiario=="c") echo $b->app ?>">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Apellido materno:</label>
		    <input class="form-control" type="text" name="apm" value="<?php if(isset($b) && $b->identificacion_beneficiario=="c") echo $b->apm ?>">
		  </div>
		</div>
        </form>
        <div class="row">
			<div class="col-md-12 form-group">
			  <div class="form-check form-check-success">
			    <label class="form-check-label">
			      <input type="radio" class="form-check-input" name="tipo_persona_a" id="persona_m_f_c" value="2">
			      Si el administrador es una persona moral o fideicomiso, indicar el nombre de la persona fisica nombrada por el administrador de la persona moral o fideicomiso. 
			    </label>
			  </div>
			</div>
		</div>
        <form class="form" method="post" role="form" id="beneficiario_moral_m">	
		<div class="row" id="pmfd_c" style="display: none">
		  <div class="col-md-4 form-group">
		    <label>Nombre(s):</label>
		    <input class="form-control" type="text" name="nombre" value="<?php if(isset($b) && $b->identificacion_beneficiario=="d") echo $b->nombre ?>">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Apellido paterno:</label>
		    <input class="form-control" type="text" name="app" value="<?php if(isset($b) && $b->identificacion_beneficiario=="d") echo $b->app ?>">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Apellido materno:</label>
		    <input class="form-control" type="text" name="apm" value="<?php if(isset($b) && $b->identificacion_beneficiario=="d") echo $b->apm ?>">
		  </div>
		</div>
    </form>
</div>
<!--<div class="row">
  <div class="col-md-12 form-group">
    <div class="form-check form-check-primary">
      <label class="form-check-label">D) Nombre del director general(proviniente del documento de identificación).   
        <input type="radio" class="form-check-input" id="accionaria_socio_d" name="tipo_inciso" value="d">
      </label>
    </div>
  </div>
</div>-->
<div class="accion_socio_d" style="display: none"> 
    <form class="form" method="post" role="form" id="beneficiario_moral_d">	
		<div class="row">
		  <div class="col-md-4 form-group">
		    <label>Nombre(s):</label>
		    <input class="form-control" type="text" name="nombre" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Apellido paterno:</label>
		    <input class="form-control" type="text" name="app" value="">
		  </div>
		  <div class="col-md-4 form-group">
		    <label>Apellido materno:</label>
		    <input class="form-control" type="text" name="apm" value="">
		  </div>
		</div>
	</form>	
</div>
<script src="<?php echo base_url(); ?>/public/js/catalogos/clientes_cliente/paso3.js?v=20201404"></script>