<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=clientes".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>#</th>
          <th>Cliente</th>
          <th>Ultima Operación</th>
          <th>Tipo de cliente</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
        $id="";
        $nombre="";
        $tipo="";
        foreach ($c as $k) {
          if($k->idtipo_cliente==1){
            $id=$k->idtipo_cliente_p_f_m;
            $nombre=$k->nombre.' '.$k->apellido_paterno.' '.$k->apellido_materno;
            $tipo="Persona física mexicana o extranjera con condición de estancia temporal o permanente";
          }else if($k->idtipo_cliente==2){
            $id=$k->idtipo_cliente_p_f_e;
            $nombre=$k->nombre2.' '.$k->apellido_paterno2.' '.$k->apellido_materno2;
            $tipo="Persona física extranjera con condición de estancia de visitante";
          }else if($k->idtipo_cliente==3){
            $id=$k->idtipo_cliente_p_m_m_e;
            $nombre=$k->razon_social;
            $tipo="Persona moral mexicana y extranjera, persona moral de derecho público";
          }else if($k->idtipo_cliente==4){
            $id=$k->idtipo_cliente_p_m_m_d;
            $nombre=$k->nombre_persona;
            $tipo="Persona moral mexicana de derecho público detallados en el anexo 7 bis A";
          }else if($k->idtipo_cliente==5){
            $id=$k->idtipo_cliente_e_c_o_i;
            $nombre=$k->denominacion2;
            $tipo="Embajadas, consulados u organismos internacionales";
          }else if($k->idtipo_cliente==6){
            $id=$k->idtipo_cliente_f;
            $nombre=$k->denominacion;
            $tipo="Fideicomisos";
          }

        	echo '
            <tr>
                <td >'.$id.'</td>
                <td >'.$nombre.'</td>
                <td >'.$k->ultimo_reg.'</td>
                <td >'.$tipo.'</td>';
        }
        echo '</tr>';

        foreach ($cu as $k2) {
          echo '
            <tr>
                <td >'.$k2->id_union.'</td>
                <td >'.$k2->cliente.'</td>
                <td >'.$k2->ultimo_reg.'</td>
                <td >Mancomunado</td>';
        }
        echo '</tr>';
        ?>
        
    </tbody>
</table>