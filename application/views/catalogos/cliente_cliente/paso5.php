<style type="text/css">
    .form-check .form-check-label input[type="checkbox"]:checked + .input-helper:before {
	    background: #25294c;
	    border-width: 0;
    }
    .form-check .form-check-label input[type="checkbox"] + .input-helper:before {
        width: 18px;
	    height: 18px;
	    border-radius: 2px;
	    border: solid #25294c;
	    border-width: 2px;
	}
	.bg-success, .swal2-modal .swal2-buttonswrapper .swal2-styled.swal2-confirm, .settings-panel .color-tiles .tiles.success {
	    background-color: #b07f4a !important;
	}
	.progress.progress-md {
	    height: 19px;
	    border-radius: 10px;
	}
	.bg-danger, .settings-panel .color-tiles .tiles.danger {
	    background-color: #b07f4a !important;
	}
</style>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
		  <div class="card-body">
		  	<div class="row">
			  	  <input type="hidden" name="id_clientec" id="id_clientec" value="<?php echo $id_clientec ?>">
			  	  <input type="hidden" name="idperfilamiento" id="idperfilamiento" value="<?php echo $info->idperfilamiento ?>">
			  	  <input type="hidden" name="tipo_cliente" id="tipo_cliente" value="<?php echo $tipo_cliente ?>">
			  	  <input type="hidden" id="idopera" value="<?php echo $idopera ?>">
			  	  <input type="hidden" id="idtipo_cliente" value="<?php echo $idtipo_cliente ?>">
			  	  <input type="hidden" id="clasificacion" value="<?php echo $clasificacion ?>">
			  	  <input type="hidden" id="es_pep" value="<?php echo $es_pep ?>">
			  	  <input type="hidden" id="grado" value="<?php echo $grado ?>">
			      <div class="col-md-9">  
	                <?php if($tipo_cliente==1){ ?> 
			      	<h4>Persona fisica mexicana o extranjera con condición de estancia temporal permanente.</h4>
			        <?php }else if($tipo_cliente==2){ ?>
	                <h4>Persona fisica extranjera con condición de estancia de visitante.</h4>
			        <?php }else if($tipo_cliente==3){ ?>
			        <h4>Persona moral de nacionalidad mexicana, extranjera y persona moral mexicana del derecho público.</h4>
			        <?php }else if($tipo_cliente==4){ ?>
			        <h4>Persona moral mexicana del derecho público (Anexo 7 Bis A).</h4>
			        <?php }else if($tipo_cliente==5){ ?>
			        <h4>Embajadas, consulados u organismo internacionales.</h4>
			        <?php }else if($tipo_cliente==6){ ?>
	                <h4>Fideicomisos.</h4>
			        <?php } ?>	
			        <h3>Paso 5. Recopilación de documentos</h3>
			      </div>
			      <div class="col-md-3" align="right">
			      	<?php if($idopera>0) { ?>
			      		<a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $idopera; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</button></a>
			      	<?php } else { ?>	
	              		<button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar</button>
	              	<?php } ?>	
	               </div>  
		    </div>  
		    
		    <hr class="subtitle"> 
		    <!--- -->
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
					    	<?php if($tipo_cliente==6){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Documentación o razón social del fideciario:</label>
				                <?php echo "<strong>" .$info->denominacion."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>	
					    	<?php if($tipo_cliente==3){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Razón social:</label>
				                <?php echo "<strong>" .$info->razon_social."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>	
					    	<?php if($tipo_cliente==4){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Razón social:</label>
				                <?php echo "<strong>" .$info->nombre_persona."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>
					    	<?php if($tipo_cliente==5){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Denominación:</label>
				                <?php echo "<strong>" .$info->denominacion."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>	
					    	<?php if($tipo_cliente==1 || $tipo_cliente==2){ ?>
						  	<div class="row">
					            <div class="col-md-12">
					                <label>Nombre:</label>
					                <?php echo "<strong>" .$info->nombre."</strong>"; ?>
					            </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Apellido paterno:</label>
				                <?php echo "<strong>" .$info->apellido_paterno."</strong>"; ?>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Apellido materno:</label>
				                <?php echo "<strong>" .$info->apellido_materno."</strong>"; ?>
				              </div>
				            </div><br>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Nombre de identificación:</label>
				                <?php echo "<strong>" .$info->nombre_identificacion."</strong>"; ?>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Autoridad que emite la identificación:</label>
				                <?php echo "<strong>" .$info->autoridad_emite."</strong>"; ?>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Número de Identificación:</label>
				                <?php echo "<strong>" .$info->numero_identificacion."</strong>"; ?>
				              </div>
				            </div>
				            <br>
				            <?php } ?>
				            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3){ ?>
				            <div class="row">
				              <div class="col-md-12">
				                <label>R.F.C:</label>
				                <?php if($tipo_cliente==3) echo "<strong>" .$info->r_f_c_g."</strong>"; 
				                      if($tipo_cliente==1 || $tipo_cliente==2) echo "<strong>" .$info->r_f_c_t."</strong>";
				                ?>
				              </div>
				            </div>
				            <?php } ?>
				            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
				            <div class="row">
				              <div class="col-md-12">
				                <label>CURP:</label>
				                <?php echo "<strong>" .$info->curp_t."</strong>"; ?>
				              </div>
				            </div>
                            <?php } ?>	
				            <br>
				            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==5){ ?> 
				            <div class="row">
				              <div class="col-md-12">
				                <h6>Domicilio reportado:</h6>
				              </div>
				              <div class="col-md-6">
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Tipo de vialidad:</label>
				                    <?php 
				                        $get_tipo_vial=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$info->tipo_vialidad_d);
									    $tipo_v = '';
									    foreach ($get_tipo_vial as $item_e) {
									      $tipo_v = $item_e->nombre; 
									    }
				                    echo "<strong>" .$tipo_v."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Calle:</label>
				                    <?php echo "<strong>" .$info->calle_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>No. ext:</label>
				                    <?php echo "<strong>" .$info->no_ext_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>No. int:</label>
				                    <?php echo "<strong>" .$info->no_int_d."</strong>"; ?>
				                  </div>
				                </div>
				              </div>
				              <div class="col-md-6">
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Colonia:</label>
				                    <?php echo "<strong>" .$info->colonia_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Municipio o delegación:</label>
				                    <?php echo "<strong>" .$info->municipio_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Estado</label>
				                    <?php echo "<strong>" .$info->estado_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>C.P</label>
				                    <?php echo "<strong>" .$info->cp_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>País</label>
				                    <?php echo "<strong>" .$info->pais_d."</strong>"; ?>
				                  </div>
				                </div>
				              </div>
				            </div>
                            <?php } ?>	
				            <br>
				            <?php $dis=""; if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
				            <div class="row">
				            	<?php $checked=""; if($tipo_cliente==1 && $info->presente_legal!="" && $info->presente_legal!="off" || $tipo_cliente==3 && $info->nombre_g!="" || $tipo_cliente==5 && $info->nombre_g!=""
				            	 || $tipo_cliente==6 && $info->nombre_g!=""){ 
				            		$checked="checked"; 
				            		//echo "....".$checked;
				            	}
				            	if($checked=="") $dis="style='display:none;'";
				            	?>
				               <div class="col-md-12 form-group" <?php echo $dis; ?>>
				               	  <div class="form-check form-check-purple">
					                <label class="form-check-label">Presentó apoderado legal
					                	<?php
					                		if($tipo_cliente==3 && $info->nombre_g!="" || $tipo_cliente==5 && $info->nombre_g!="" ||
					                		$tipo_cliente==6 && $info->nombre_g!="") $checked="checked";
					                	?>
					                  <input disabled type="checkbox" class="form-check-input" id="Ch1" value="" <?php echo $checked; ?>>
					                </label>
					              </div>
				               </div>	
				            </div>
                            <?php } ?>	
                            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || /*$tipo_cliente==4 ||*/ $tipo_cliente==5 || $tipo_cliente==6){ ?> 
				            <hr class="subtitle">
				            <div class="row" <?php echo $dis; ?>>
				            	<div class="col-md-12">
				            		<h5>Apoderado legal <?php if($tipo_cliente==5 || $tipo_cliente==6){?> / Servicio público<?php } ?>	</h5>
				            	</div>
				            </div>
                            <?php } ?>	
                            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || /*$tipo_cliente==4 ||*/ $tipo_cliente==5  || $tipo_cliente==6){ ?> 
				            <div class="row" <?php echo $dis; ?>>
					            <div class="col-md-12">
					                <label>Nombre:</label>
					                <?php if($tipo_cliente==1) echo "<strong>" .$info->nombre_p."</strong>"; 
					                	if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->nombre_g."</strong>";
					                ?>
					            </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Apellido paterno:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->apellido_paterno_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->apellido_paterno_g."</strong>";
					            ?>
				              </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Apellido materno:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->apellido_materno_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->apellido_materno_g."</strong>";
					            ?>
				              </div>
				            </div><br>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Nombre de identificación:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->nombre_identificacion_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->nombre_identificacion_g."</strong>";
					            ?>
				              </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Autoridad que emite la identificación:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->autoridad_emite_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->autoridad_emite_g."</strong>";
					            ?>
				              </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Número de Identificación:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->numero_identificacion_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->numero_identificacion_g."</strong>";
					            ?>
				              </div>
				            </div><br>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?>
					            <div class="row" <?php echo $dis; ?>>
					              <div class="col-md-12">
					                <h6>Domicilio reportado:</h6>
					              </div>
					              <div class="col-md-6">
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Tipo de calle:</label>
					                    <?php 
					                    $get_tipo_vial=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$info->tipo_vialidad_t);
									    $tipo_v = '';
									    foreach ($get_tipo_vial as $item_e) {
									      $tipo_v = $item_e->nombre; 
									    }
					                    echo "<strong>" .$tipo_v."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Calle:</label>
					                    <?php echo "<strong>" .$info->calle_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>No. ext:</label>
					                    <?php echo "<strong>" .$info->no_ext_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>No. int:</label>
					                    <?php echo "<strong>" .$info->no_int_t."</strong>"; ?>
					                  </div>
					                </div>
					              </div>
					              <div class="col-md-6">
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Colonia:</label>
					                    <?php echo "<strong>" .$info->colonia_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Municipio o delegación:</label>
					                    <?php echo "<strong>" .$info->municipio_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Estado</label>
					                    <?php echo "<strong>" .$info->estado_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>C.P</label>
					                    <?php echo "<strong>" .$info->cp_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>País</label>
					                    <?php echo "<strong>" .$info->pais_d."</strong>"; ?>
					                  </div>
					                </div>
					              </div>
					            </div>
	                            <?php } ?>
                            <?php } ?>	
						</div>
					</div>	 
					<br>
						<div class="col-md-12 grid-margin stretch-card">
			              	<div class="card">
				                <div class="card-body">
				                  <!--<h4 class="card-title">Barra de Proceso</h4>-->
				                  <p class="page-description"><strong>Progreso de captura de documentos</strong></p>
				                  <div class="template-demo">
				                    <div class="progress progress-md" id="cont_barra">
				                     	
				                    </div>
				                  </div>
				                </div>
			              	</div>
			            </div>   
		        </div>	

	        	<div class="col-md-6">
	        		<form id="form_docs_cliente" class="form">
	        			<?php if(isset($docs)) {?>
				            <input type="hidden" name="id" id="id" value="<?php echo $docs->id ?>">
				          <?php } else { ?>
				          	<input type="hidden" name="id" id="id" value="0">
				          <?php }?>
		        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
						    <div class="card-body">
						    <!-- -->
	                           <div class="row">
					              <div class="col-md-12">
					                
					                <div class="d-flex mb-2">
						                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
						                	<h5>Documentos a recolectar (El nombre de los documentos debe ir sin punto)</h5>
						                </div>
						                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="tamano_ayuda()">
						                  <i class="mdi mdi-help"></i>
						                </button>
              						</div>
					              </div>
					            </div> 
					            <div class="col-md-12">
					            	<span>*Por favor, recolecte una copia digital y coteje contra original los siguientes documentos.</span>
					            </div>
					            <div class="col-md-12 form-group" id="cont2">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Consulta de listas de personas bloqueadas":
					                	<input type="checkbox" class="form-check-input" id="Chimg0" value="" checked="">
					                </label>
					                <span>Resultado: </span> <button type="button" class="btn gradient_nepal2" onclick="formatoPersonasBloquedas()"><i class="fa fa-eye"></i> Ver detalle</button>
					              </div>
					            </div>
					            <!--
					            <div class="col-md-12 form-group" id="cont1">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Consulta en listado de personas bloqueadas":
					                  <input type="checkbox" class="form-check-input" id="Chimg1" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="img" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
                                -->
					            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont2">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Conoce a tu cliente":
					                  <input type="checkbox" class="form-check-input" id="Chimg2" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgcc" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
	                            <?php if($tipo_cliente==1){ ?> 
					            <div class="col-md-12">
					            	<span>*Por favor, recolecte una copia digital y coteje contra original los siguientes documentos.</span>
					            </div>
					            <?php } ?>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
					            <div class="col-md-12 form-group" id="cont3">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Identificación oficial:
					                  <input type="checkbox" class="form-check-input" id="Chimg3" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgio" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
	                            <?php } ?>
					            <?php if($tipo_cliente==1 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont4">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Cédula de identificación fiscal:
					                  <input type="checkbox" class="form-check-input" id="Chimg4" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgcif" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
	                            <?php if($tipo_cliente==1){ ?> 
					            <div class="col-md-12 form-group" id="cont9">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">CURP:
					                  <input type="checkbox" class="form-check-input" id="Chimg5" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgcurp" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5){ ?> 
					            <div class="col-md-12 form-group" id="cont10">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Comprobante de domicilio:
					                  <input type="checkbox" class="form-check-input" id="Chimg6" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgcd" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
	                            <?php } ?>
	                            <?php if(/*$tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3  || $tipo_cliente==5*/ $tipo_cliente==8){ ?> 
					            <div class="col-md-12 form-group" id="cont11" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato consultas de dueño beneficiario:
					                  <input type="checkbox" class="form-check-input" id="Chimg7" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgcdb" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
	                            <?php } ?>
	                            <?php if($tipo_cliente==2){ ?> 
	                            <hr class="subtitle">
	                            <h5>Documento del Instituto Nacional de Migración</h5>
	                            <div class="col-md-12 form-group" id="cont6">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Constancia de estancia legal en el país:
					                  <input type="checkbox" class="form-check-input" id="Chimg8" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="cons_elp" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <hr class="subtitle">
	                            <?php } ?>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
					            <div class="col-md-12 form-group" id="cont6" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Poder:
					                  <input type="checkbox" class="form-check-input" id="Chimg9" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="carta_poder" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_cliente==4){ ?> 
	                            <hr class="subtitle">
	                            <h5>Comprobación de facultades del servidor público</h5>
	                            <div class="col-md-12 form-group" id="cont6">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Constancia de comprobación de facultades del servidor público:
					                  <input type="checkbox" class="form-check-input" id="Chimg10" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="cons_elp2" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <hr class="subtitle">
	                            <?php } ?>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
					            <div class="col-md-12 form-group" id="cont8" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Comprobante de domicilio de apoderado legal:
					                  <input type="checkbox" class="form-check-input" id="Chimg11" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgcdal" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont12">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Escritura constitutiva o instrumento público que acredite su existencia:
					                  <input type="checkbox" class="form-check-input" id="Chimg12" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="esc_ints" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_cliente==3){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Poderes Notariales del apoderado legal o documento para comprobar las facultades del servidor público:
					                  <input type="checkbox" class="form-check-input" id="Chimg13" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="poder_not_fac_serv" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Poder Notarial para comprobar las facultades del apoderado legal:
					                  <input type="checkbox" class="form-check-input" id="Chimg14" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="poder_not" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont7" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					              	<?php if($tipo_cliente!=6){ ?> 
					                	<label class="form-check-label">Identificación oficial del apoderado legal y/o servidor público:
					                <?php } else { ?>
					                	<label class="form-check-label">Identificación del apoderado legal:
					                <?php } ?>

					                  <input type="checkbox" class="form-check-input" id="Chimg15" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgineal" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
	                            <?php } ?>
	                            <?php if($tipo_cliente==5){ ?> 
					            <div class="col-md-12 form-group" id="cont6">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Documento que acredite su legal existencia: 
					                  <input type="checkbox" class="form-check-input" id="Chimg16" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="imgacre_legal" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Poderes Notariales del apoderado legal o documento para comprobar las facultades del servidor público:
					                  <input type="checkbox" class="form-check-input" id="Chimg17" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="poder_not2" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <!-- SI ES CLIENTE PEP DESDE LA CALCULADORA PEDIR DOCUMENTOS, O PEP DE PERFILAMIENTO-->
					            <input type="hidden" id="emb_cons_org_inter" value="<?php echo $emb_cons_org_inter; ?>">
					            <input type="hidden" id="id_actividad" value="<?php echo $id_actividad; ?>">
					            <?php if($emb_cons_org_inter==1 || $idtipo_cliente==5 || $idtipo_cliente==1 && $clasificacion>0 && $clasificacion<=7 || $idtipo_cliente==1 && $clasificacion>=9 && $clasificacion<=15 && $id_actividad!=15 && $id_actividad!=16 
					            	|| $idtipo_cliente==1 && $clasificacion>9 && $clasificacion<=15 && $id_actividad==15 || 
					            		$idtipo_cliente==1 && $clasificacion>9 && $clasificacion<=15 && $id_actividad==16
					            	|| $es_pep>0){ ?> 
					            <div class="col-md-12 form-group" id="cont1PEP">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Cuestionario Persona Políticamente Expuesta: 
					                  <input type="checkbox" class="form-check-input" id="Chimg18" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="cuestionario_pep" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <?php if(/*$emb_cons_org_inter==1 || $idtipo_cliente==5 || $idtipo_cliente==1 && $clasificacion>0 && $clasificacion<=7 || $idtipo_cliente==1 && $clasificacion>=9 && $clasificacion<=15 || */ $grado>2.0){ ?> 
					            <div class="col-md-12 form-group" id="cont2PEP">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Cuestionario ampliado:
					                  <input type="checkbox" class="form-check-input" id="Chimg19" value="">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="cuestionario_amp" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php } ?>
					            <!-- -->
					            <?php 
					            $idcliente_usuario=$this->session->userdata('idcliente_usuario');
					            //log_message('error', 'idcliente_usuario: '.$idcliente_usuario);
					            $tipo=$this->session->userdata('tipo');
					            $menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,2);
					            $perfilid=$this->session->userdata('perfilid');
								//echo "perfilid: ".$perfilid;
								//log_message('error', 'perfilid: '.$perfilid);
					            if($perfilid==3){
					            	echo '<div class="row row_file valid">
				                        		<div class="form-check form-check-purple">
								                	<label class="form-check-label"><h4>Documentos Validados</h4>
								                  	<input  type="checkbox" id="validado" class="form-check-input" name="validado" value="">
								                	</label>
								              	</div>
								          	</div>';
					            }else{
					            	foreach ($menu as $x) { 
					            		//echo "id_menusub: ".$x->id_menusub;
						                if($x->id_menusub==16){ // Valdiar docs
						                  	if($x->check_menu==1){ ?>
						                    	<div class="row row_file valid">
					                        		<div class="form-check form-check-purple">
									                	<label class="form-check-label"><h4>Documentos Validados</h4>
									                  	<input  type="checkbox" id="validado" class="form-check-input" name="validado" value="">
									                	</label>
									              	</div>
									          	</div>
						                 	<?php }
						                }
						            } 
					            }
	                        	?>

					          	<div id="headingCollapse12" class="card-header">
			                      <a data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12" class="card-title lead collapsed" id="mas_docs_cli"><i class="fa fa-arrow-down"></i> Mas Documentos</a>
			                    </div>
			                    <div id="collapse12" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" aria-expanded="false">
			                    <div class="card-body">
			                    	<form id="form_docs_extra" class="form" method="post" role="form">
				                        <div class="card-block">
				                        	<?php if(isset($docs_ext)) {?>
									            <input type="hidden" id="id_extra" value="<?php echo $docs_ext->id ?>">
									        <?php } else { ?>
									          	<input type="hidden" id="id_extra" value="0">
									        <?php }?>

				                          	<div class="row">
				                                <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 1:
									                  	<input type="checkbox" class="form-check-input" id="Chext1" value="">
									                	</label>
									              	</div>
									              	<div class="row">
										              	<div class="row row_file">
											             	<div class="col-md-12">
											                	<input width="50px" height="80px" class="imgExt" id="extra" type="file">
											              	</div>
											              	<div class="col-md-10"></div>
											           </div>
											       </div><br>
										            <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip" id="descrip" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato" value="">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 2:
									                  	<input type="checkbox" class="form-check-input" id="Chext2" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra2" type="file">
										              	</div>
										              	<div class="col-md-10"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip2" id="descrip2" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 3:
									                  	<input type="checkbox" class="form-check-input" id="Chext3" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra3" type="file">
										              	</div>
										              	<div class="col-md-6"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip3" id="descrip3" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 4:
									                  	<input type="checkbox" class="form-check-input" id="Chext4" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra4" type="file">
										              	</div>
										              	<div class="col-md-10"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip4" id="descrip4" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div>
									            <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<label class="form-check-label">Extra 5:
									                  	<input type="checkbox" class="form-check-input" id="Chext5" value="">
									                	</label>
									              	</div>
									              	<div class="row row_file">
										             	<div class="col-md-6">
										                	<input width="50px" height="80px" class="imgExt" id="extra5" type="file">
										              	</div>
										              	<div class="col-md-10"></div>
										           </div><br>
										           <div class="col-md-12 form-group">
										                <label>Nombre o tipo de documento:</label>
										                <input class="form-control" type="text" name="descrip5" id="descrip5" placeholder="Ejemplo: INE, Pasaporte, Comprobante de domicilio, formato">
										            </div>
									            </div> 

				                          	</div>
				                        </div>
			                    	</form>
			                      </div>
			                    </div>

						    </div>
						</div>
						
			            <div class="row"> 
				            <div class="col-md-12" align="right">
				            	<br>
				              <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="btn_alert_guardado()">Guardar</button>
				            </div>
			          	</div>
					</form>    	
	        	</div>

	        </div>
            <!-- -->
	    </div>
	</div>
</div>

<div class="modal fade" id="modal_carga_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Tamaño y caracteristicas en carga de archivos</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>El tamaño maximo es de 2.5mb por archivo, los nombres de los documentos no deben presentar puntos medios, los 
           formaros permitidos son: "jpg", "png", "pdf", "jpeg"</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>