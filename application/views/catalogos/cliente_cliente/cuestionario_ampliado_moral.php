<style type="text/css">
.check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c;
}
.form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c;
}
.form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #212b4c;
}
.form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
}
.form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
}
</style>
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <!---------------->
          <br>
          <div align="center">
             <h3>CUESTIONARIO AMPLIADO (PARA PERSONAS MORALES MEXICANAS Y EXTRANJERAS)</h3>
          </di>
          <hr class="subtitle">
          <h4 class="barra_menu" align="left">Entidad a cuestionar:</h4>
          <br>
          <div class="row">
            <input id="idopera" type="hidden" value="<?php echo $idopera; ?>">
            <div align="left" class="col-md-12 barra_menu">
              <?php if($tipo==1){ $giro = $info->activida_o_giro; $edo = $info->estado_d; ?> 
              <h4>Tipo de persona: Persona fisica mexicana o extranjera con condición de estancia temporal permanente.</h4>
              <?php }else if($tipo==2){ $giro = $info->actividad_o_giro; $edo = $info->estado_d; ?>
                  <h4>Tipo de persona: Persona fisica extranjera con condición de estancia de visitante.</h4>
              <?php }else if($tipo==3){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
              <h4>Tipo de persona: Persona moral de nacionalidad mexicana, extranjera y persona moral mexicana del derecho público.</h4>
              <?php }else if($tipo==4){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
              <h4>Tipo de persona: Persona moral mexicana del derecho público (Anexo 7 Bis A).</h4>
              <?php }else if($tipo==5){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
              <h4>Tipo de persona: Embajadas, consulados u organismo internacionales.</h4>
              <?php }else if($tipo==6){ $giro = ""; $edo=""; ?>
                  <h4>Tipo de persona: Fideicomisos.</h4>
              <?php } ?>  
            </div>  
          </div>
          <?php if($tipo==1 || $tipo==2){ ?>
            <div class="row barra_menu">
              <div class="col-md-6" align="left">
                  <h4>Nombre:
                  <?php echo $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno."</h4>"; ?>
              </div>
              <div align="left" class="col-md-6">
                <h4>Fecha de nacimiento:
                <?php echo $fechan=$info->fecha_nacimiento."</h4>"; ?>
              </div>
            </div>
            
            <br><br>
          <?php } ?>
          <?php if($tipo==3){ ?> 
            <div class="row barra_menu">
              <div align="left" class="col-md-6">
                <br><h4>Razón social:
                <?php echo $info->razon_social."</h4>"; ?>
              </div>
              <div align="left" class="col-md-6">
                <br><h4>Fecha de constitución:
                <?php echo $fechan=$info->fecha_constitucion_d."</h4>";  ?>
              </div>
            </div><br>
          <?php } ?>
          <?php if($tipo==4){ ?> 
          <div class="row barra_menu">
            <div align="left" class="col-md-6">
              <h4>Razón social:
              <?php echo $info->nombre_persona."</h4>"; ?>
            </div>
            <div align="left" class="col-md-6">
              <br><h4>Fecha de constitución:
              <?php echo $fechan=$info->fecha_constitucion_d."</h4>";  ?>
            </div>
          </div><br>
          <?php } ?>
          <?php if($tipo==5 || $tipo==6){ ?> 
            <div class="row barra_menu">
              <div align="left" class="col-md-6">
                <h4>Denominación:
                <?php echo $info->denominacion."</h4>"; ?>
              </div>
              <?php if($tipo==5){ ?> 
                  <div align="left" class="col-md-6">
                    <br><h4>Fecha de constitución:
                    <?php echo $fechan=$info->fecha_establecimiento."</h4>"; ?>
                  </div>
              <?php } ?>
              <?php if($tipo==6){ ?> 
                  <div align="left" class="col-md-6">
                    <br><h4>Fecha de constitución:
                    <?php //echo $info->fecha_nacimiento_g."</h4>"; ?>
                  </div>
              <?php } ?>
            </div>
          <?php } ?>

          <form class="form" method="post" role="form" id="form_cuestionario" required> 
            <input type="hidden" id="id" name="id" value="<?php if(isset($ca->id)) echo $ca->id; else echo "0"; ?>">
            <input type="hidden" id="tipoc" value="<?php echo $tipo; ?>">
            <input type="hidden" name="id_perfilamiento" id="idperfilamiento" value="<?php echo $idperfilamiento; ?>">
            <input type="hidden" id="idclientec" value="<?php echo $idclientec; ?>"> 
            <input id="idopera" name="id_operacion" type="hidden" value="<?php echo $idopera; ?>">
            <input id="idcgr" type="hidden" name="id_grado_riesgo" value="<?php echo $idcgr; ?>">
            <div align="left" class="row">
              <div class="col-md-12 form-group">
                <label>Actividad conforme a su Alta Fiscal: </label>
                <input class="form-control" type="text" id="actividad" name="actividad" <?php if(isset($ca)) echo "value='$ca->actividad'"; ?>>
              </div>

              <div class="col-md-6 form-group">
                <label>¿Cuál es la fuente principal de ingresos?</label>
                <input class="form-control" type="text" id="fuente_ingreso" name="fuente_ingreso" <?php if(isset($ca)) echo "value='$ca->fuente_ingreso'"; ?>>
              </div>
              <div class="col-md-6 form-group">
                <label>Idica el % que representa esta fuente del total de los ingresos</label>
                <input class="form-control" type="text" id="porcentaje" name="porcentaje" <?php if(isset($ca)) echo "value='$ca->porcentaje'"; ?> placeholder="%">
              </div>

              <div class="col-md-6 form-group">
                <label>Si la fuente  principal de ingresos represente menos del 50% del total, describe cuales son  las otras fuentes de ingreso</label>
                <input class="form-control" type="text" id="otras_fuentes" name="otras_fuentes" <?php if(isset($ca)) echo "value='$ca->otras_fuentes'"; ?>>
              </div>
              <div class="col-md-6 form-group">
                <label>¿Cuál es el monto mensual de ventas (considerando todas las fuentes de ingresos)?</label>
                <input class="form-control" type="text" id="monto_ventas_mes" name="monto_ventas_mes" <?php if(isset($ca)) echo "value='$ca->monto_ventas_mes'"; ?>>
              </div>

              <div class="col-md-2 form-group">
                <label>¿Recibe ingresos del extranjero?</label>
                <div class="row">
                  <div class="col-md-6 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input ingreso_extran" name="ingreso_extran" value="1" <?php if(isset($ca) && $ca->ingreso_extran==1) echo "checked"; if(!isset($ca)) echo "checked"; ?>>
                        Si
                      </label>
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input ingreso_extran" name="ingreso_extran" value="2" <?php if(isset($ca) && $ca->ingreso_extran==2) echo "checked"; ?>>
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="col-md-4">
                <div class="col-md-12 form-group" id="div_pais_ext">
                  <label>¿De qué país(es)?</label>
                  <input class="form-control" type="text" id="pais_ingreso" name="pais_ingreso" <?php if(isset($ca)) echo "value='$ca->pais_ingreso'"; ?>>
                </div>
              </div>

              <div class="col-md-4 form-group">
              </div>
              <div class="col-md-5 form-group">
                <label>¿Mantiene relaciones comerciales con alguna dependencia o entidad gubernamental, nacional o extranjera?</label>
                <div class="row">
                  <div class="col-md-6 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input relacion_extran" name="relacion_extran" value="1" <?php if(isset($ca) && $ca->relacion_extran==1) echo "checked"; if(!isset($ca)) echo "checked"; ?>>
                        Si
                      </label>
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input relacion_extran" name="relacion_extran" value="2" <?php if(isset($ca) && $ca->relacion_extran==2) echo "checked"; ?>>
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div> 
              <div id="col-md-4">
                <div class="col-md-12 form-group" id="div_relacion">
                  <label>¿Cuál(es)?</label>
                  <input class="form-control" type="text" id="cual_relacion" name="cual_relacion" <?php if(isset($ca)) echo "value='$ca->cual_relacion'"; ?>>
                </div>
              </div>

              <div class="col-md-12 form-group">
                <label>Del total de pagos que recibe de sus clientes, favor de desglosar en porcentaje, ¿qué forma de pago utilizan?</label>
              </div>
              <div class="col-md-2 form-group">
                <label>Efectivo</label>
                <input class="form-control" type="number" id="efectivo" name="efectivo" <?php if(isset($ca)) echo "value='$ca->efectivo'"; ?> placeholder="%" max="100">
              </div>
              <div class="col-md-2 form-group">
                <label>Transferencia de otros bancos </label>
                <input class="form-control" type="number" id="transfer_otro" name="transfer_otro" <?php if(isset($ca)) echo "value='$ca->transfer_otro'"; ?> placeholder="%" max="100">
              </div>
              <div class="col-md-2 form-group">
                <label>Transferencias internacionales </label>
                <input class="form-control" type="number" id="transfer_inter" name="transfer_inter" <?php if(isset($ca)) echo "value='$ca->transfer_inter'"; ?> placeholder="%" max="100">
              </div>
              <div class="col-md-2 form-group">
                <label>Pago con tarjeta de crédito/debito </label>
                <input class="form-control" type="number" id="tarjeta" name="tarjeta" <?php if(isset($ca)) echo "value='$ca->tarjeta'"; ?> placeholder="%" max="100">
              </div>
              <div class="col-md-2 form-group">
                <label>Cheques</label>
                <input class="form-control" type="number" id="cheques" name="cheques" <?php if(isset($ca)) echo "value='$ca->cheques'"; ?> placeholder="%" max="100">
              </div>
              <div class="col-md-2 form-group">
                <label>Otras (describir)</label>
                <input class="form-control" type="number" id="otros" name="otros" <?php if(isset($ca)) echo "value='$ca->otros'"; ?> max="100">
              </div>

              <div class="col-md-4 form-group">
                <label>¿La entidad tiene sucursales en el país?</label>
                <div class="row">
                  <div class="col-md-3 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input sucursales" id="sucursales" name="sucursales" value="1" <?php if(isset($ca) && $ca->sucursales==1) echo "checked"; if(!isset($ca)) echo "checked"; ?>>
                        Si
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input sucursales" id="sucursales" name="sucursales" value="2" <?php if(isset($ca) && $ca->sucursales==2) echo "checked"; ?>>
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 form-group">
                <div class="col-md-12 form-group div_sucurs">
                  <label>¿Cuántas?</label>
                  <input class="form-control" type="number" id="cuantas" name="cuantas" <?php if(isset($ca)) echo "value='$ca->cuantas'"; ?> max="100">
                </div>
              </div>
              <div class="col-md-4 form-group">
                <div class="col-md-12 form-group div_sucurs">
                  <label>¿En qué ciudades y estados?</label>
                  <input class="form-control" type="text" id="pais_estados" name="pais_estados" <?php if(isset($ca)) echo "value='$ca->pais_estados'"; ?>>
                </div>
              </div>

              <div class="col-md-4 form-group">
                <label>¿La entidad tiene sucursales en otros países?</label>
                <div class="row">
                  <div class="col-md-3 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input sucursal_otro_pais" id="sucursal_otro_pais" name="sucursal_otro_pais" value="1" <?php if(isset($ca) && $ca->sucursal_otro_pais==1) echo "checked"; if(!isset($ca)) echo "checked"; ?>>
                        Si
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input sucursal_otro_pais" id="sucursal_otro_pais" name="sucursal_otro_pais" value="2" <?php if(isset($ca) && $ca->sucursal_otro_pais==2) echo "checked"; ?>>
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 form-group">
                <div class="col-md-12 form-group div_sucurs_otro">
                  <label>¿Cuántas?</label>
                  <input class="form-control" type="number" id="cuantas_otro" name="cuantas_otro" <?php if(isset($ca)) echo "value='$ca->cuantas_otro'"; ?> max="100">
                </div>
              </div>
              <div class="col-md-4 form-group">
                <div class="col-md-12 form-group div_sucurs_otro">
                  <label>¿En qué países?</label>
                  <input class="form-control" type="text" id="paises_otro" name="paises_otro" <?php if(isset($ca)) echo "value='$ca->paises_otro'"; ?>>
                </div>
              </div>

              <div class="col-md-12 form-group">
                <label>¿La(s) ubicación(es) de la entidad, son propias o rentadas?</label>
                <br><br>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <label>Propia(s):</label>
                    <input class="form-control" type="text" id="propias" name="propias" <?php if(isset($ca)) echo "value='$ca->propias'"; ?> max="100">
                  </div>
                  <div class="col-md-2 form-group">
                    <label>Rentada(s):</label>
                    <input class="form-control" type="text" id="rentadas" name="rentadas" <?php if(isset($ca)) echo "value='$ca->rentadas'"; ?>>
                  </div>
                </div>
              </div>
              <div class="row" style="display: none; page-break-before: always" id="break_emple">
                <div class="col-md-12 form-group">
                  <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label>Número de empleados </label>
                <input class="form-control" type="number" id="num_emplea" name="num_emplea" <?php if(isset($ca)) echo "value='$ca->num_emplea'"; ?>>
              </div>
              <div class="col-md-3 form-group">
                <label>Número de clientes</label>
                <input class="form-control" type="number" id="num_cli" name="num_cli" <?php if(isset($ca)) echo "value='$ca->num_cli'"; ?>>
              </div>
              <div class="col-md-3 form-group">
                <label>Número de proveedores</label>
                <input class="form-control" type="number" id="num_prov" name="num_prov" <?php if(isset($ca)) echo "value='$ca->num_prov'"; ?>>
              </div>


              <div class="col-md-4 form-group">
                <label>¿La empresa pertenece a algún grupo?</label>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input empresa_grupo" id="empresa_grupo" name="empresa_grupo" value="1" <?php if(isset($ca) && $ca->empresa_grupo==1) echo "checked"; if(!isset($ca)) echo "checked"; ?>>
                        Si
                      </label>
                    </div>
                  </div>
                  <div class="col-md-4 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input empresa_grupo" id="empresa_grupo" name="empresa_grupo" value="2" <?php if(isset($ca) && $ca->empresa_grupo==2) echo "checked"; ?>>
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 form-group">
                <div class="col-md-12 form-group div_cta_emp">
                  <label>¿Cuántas empresas conforman el grupo empresarial?</label>
                  <input class="form-control" type="number" id="cuantas_empre" name="cuantas_empre" <?php if(isset($ca)) echo "value='$ca->cuantas_empre'"; ?>>
                </div>
              </div>


              <div class="col-md-6 form-group">
                <label>¿La empresa tiene Consejo de Administración o Administrador Único?</label>
                <div class="row">
                  <div class="col-md-4 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input consejo_admin" id="consejo_admin" name="consejo_admin" value="1" <?php if(isset($ca) && $ca->consejo_admin==1) echo "checked"; if(!isset($ca)) echo "checked"; ?>>
                        Consejo de Administración
                      </label>
                    </div>
                  </div>
                  <div class="col-md-4 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input consejo_admin" id="consejo_admin" name="consejo_admin" value="2" <?php if(isset($ca) && $ca->consejo_admin==2) echo "checked"; ?>>
                        Administrador Único
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 form-group">
                <label>Motivo por el cual la entidad solicita la transacción / operación:</label>
                <textarea name="motivo_transac" id="motivo_transac" class="form-control"><?php if(isset($ca)) echo $ca->motivo_transac; ?></textarea>
              </div>
            </div>

            <div class="firma">
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="firma" style="display: none;">
                <div class="row">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_cli_imp">Firma de Cliente</span></center><br>
                      Doy fé que los datos proporcionados son verídicos y están vigentes
                  </div>
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_ejec_imp">Firma Ejecutivo Comercial / Vendedor</span></center><br>
                      Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial.
                  </div>  
                  <div class="col-md-12">
                    <br><br><br><br><br><br><br>
                  </div>
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_cli_imp">Firma de aprobación del Director Comercial</span></center><br>
                      Apruebo continuar con la transacción comercial
                  </div>
                </div>
              </div>
            </div> 

            <div class="row">
              <div class="col-md-12" align="right">
                <button type="button" class="btn gradient_nepal2 barra_menu" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Atrás</button>
                <?php if(isset($ca->id) && $ca->id>0){ ?>
                  <button type="button" class="btn gradient_nepal2 barra_menu" onclick="imprimir_for()"><i class="fa fa-print"></i> Imprimir cuestionario</button>
                <?php } ?>
                <button type="button" class="btn gradient_nepal2 barra_menu" id="btn_submit"><i class="fa fa-arrow-right"></i> Guardar</button>
              </div>
            </div>
          </form>
          
    

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalimprimir" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" align="center">
            <label>¿Deseas imprimir el formulario?                
            </label>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4 form-group">
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="1">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="2" checked>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_registro" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

