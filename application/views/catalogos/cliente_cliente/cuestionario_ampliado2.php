<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<style type="text/css">
.check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c; -webkit-print-color-adjust: exact;
}
.form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c; -webkit-print-color-adjust: exact;
}
.form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #212b4c; -webkit-print-color-adjust: exact;
}
.form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c; -webkit-print-color-adjust: exact;
}
.form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c; -webkit-print-color-adjust: exact;
}
</style>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
          <h3>Cuestionario ampliado - <?php echo $nombre; ?></h3>
          <hr class="subtitle">
          <form class="form" method="post" role="form" id="form_cuestionario" required> 
            <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
            <input type="hidden" id="tipoc" value="<?php echo $tipo; ?>">
            <input type="hidden" name="id_perfilamiento" id="idperfilamiento" value="<?php echo $idperfilamiento; ?>">
            <input type="hidden" id="idclientec" value="<?php echo $idclientec; ?>"> 
            <input type="hidden" name="id_operacion" id="id_operacion" value="<?php echo $idopera; ?>">
            <input id="idcgr" type="hidden" name="id_grado_riesgo" value="<?php echo $idcgr; ?>">
            <?php if($tipo==1){ $giro = $info->activida_o_giro; $edo = $info->estado_d; ?> 
            <h4>Tipo de persona: Persona fisica mexicana o extranjera con condición de estancia temporal permanente.</h4>
            <?php }else if($tipo==2){ $giro = $info->actividad_o_giro; $edo = $info->estado_d; ?>
                <h4>Tipo de persona: Persona fisica extranjera con condición de estancia de visitante.</h4>
            <?php }else if($tipo==3){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
            <h4>Tipo de persona: Persona moral de nacionalidad mexicana, extranjera y persona moral mexicana del derecho público.</h4>
            <?php }else if($tipo==4){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
            <h4>Tipo de persona: Persona moral mexicana del derecho público (Anexo 7 Bis A).</h4>
            <?php }else if($tipo==5){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
            <h4>Tipo de persona: Embajadas, consulados u organismo internacionales.</h4>
            <?php }else if($tipo==6){ $giro = ""; $edo=""; ?>
                <h4>Tipo de persona: Fideicomisos.</h4>
            <?php } ?>  
            <div align="left" class="row">
              <div class="col-md-12 form-group" style="display: none;">
                <label>Actividad conforme a su Alta Fiscal:</label>
                <select class="form-control" id="giro" name="giro">
                  <option value="" disabled>Selecciona una opción</option>
                  <?php foreach ($get_actividad as $item){
                    $selected="";
                    if($item->clave==$giro){                      
                      $selected = "selected";
                    }
                    if($tcc==1)
                      echo "<option value='".$item->clave."' ".$selected.">".$item->acitividad."</option>";  
                    else 
                      echo "<option value='".$item->clave."' ".$selected.">".$item->giro_mercantil."</option>";  
                  } ?>
                </select>
              </div>
              <div class="col-md-12 form-group">
                <label>Proporcionar una descripción del tipo de negocios que realiza (ocupación):</label>
                <input class="form-control" type="text" id="descrip_nego" name="descrip_nego" value="<?php echo $descrip_nego ?>">
              </div>
            </div>
            <?php
              $propietario_empresatx1='';
              $propietario_empresatx2='';
              if($propietario_empresa==1){
                $propietario_empresatx1='checked';
                $propietario_empresatx2='';
              }else if($propietario_empresa==2){
                $propietario_empresatx1='';
                $propietario_empresatx2='checked';
              }
            ?>
            <div class="row">
              <div class="col-md-3"><label>¿Es propietario de empresa?</label></div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input propietario_empresa" name="propietario_empresa" value="1" <?php echo $propietario_empresatx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input propietario_empresa" name="propietario_empresa" value="2" <?php echo $propietario_empresatx2 ?>>
                    No
                  </label>
                </div>
              </div>
            </div>
            <?php
              $trabajador_autonomotx1='';
              $trabajador_autonomotx2='';
              if($trabajador_autonomo==1){
                $trabajador_autonomotx1='checked';
                $trabajador_autonomotx2='';
              }else if($trabajador_autonomo==2){
                $trabajador_autonomotx1='';
                $trabajador_autonomotx2='checked';
              }
            ?>
            <div class="row" id="princ2">
              <div class="col-md-3">
                <label>¿Es trabajador autónomo?</label>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input trabajador_autonomo" name="trabajador_autonomo" value="1" <?php echo $trabajador_autonomotx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input trabajador_autonomo" name="trabajador_autonomo" value="2" <?php echo $trabajador_autonomotx2 ?>>
                    No
                  </label>
                </div>
              </div>
            </div>
            <?php
              $es_empleadotx1='';
              $es_empleadotx2='';
              if($es_empleado==1){
                $es_empleadotx1='checked';
                $es_empleadotx2='';
              }else if($es_empleado==2){
                $es_empleadotx1='';
                $es_empleadotx2='checked';
              }
            ?>
            <div class="row" id="princ3">
              <div class="col-md-3">
                <label>¿Es empleado?</label>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input es_empleado" name="es_empleado" value="1" <?php echo $es_empleadotx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input es_empleado" name="es_empleado" value="2" <?php echo $es_empleadotx2 ?>>
                    No
                  </label>
                </div>
              </div>
            </div>
            <?php
              $otras_actividades_generadoras_recursox1='';$otras_actividades_generadoras_recursox2='';
              $otras_actividades_generadoras_recursox3='';$otras_actividades_generadoras_recursox4='';
              if($es_empleado==1){
                $otras_actividades_generadoras_recursox1='checked';$otras_actividades_generadoras_recursox2='';
                $otras_actividades_generadoras_recursox3='';$otras_actividades_generadoras_recursox4='';
              }else if($es_empleado==1){
                $otras_actividades_generadoras_recursox1='';$otras_actividades_generadoras_recursox2='checked';
                $otras_actividades_generadoras_recursox3='';$otras_actividades_generadoras_recursox4='';
              }else if($es_empleado==3){
                $otras_actividades_generadoras_recursox1='';$otras_actividades_generadoras_recursox2='';
                $otras_actividades_generadoras_recursox3='checked';$otras_actividades_generadoras_recursox4='';
              }else if($es_empleado==4){
                $otras_actividades_generadoras_recursox1='';$otras_actividades_generadoras_recursox2='';
                $otras_actividades_generadoras_recursox3='';$otras_actividades_generadoras_recursox4='checked';
              }
            ?>
            <div class="row" id="princ4">
              <div class="col-md-4">
                <label>Otras actividades no generadoras de recurso:</label>
              </div>
              <div class="col-md-2 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input riquezas_otras" name="otras_actividades_generadoras_recurso" value="1" <?php echo $otras_actividades_generadoras_recursox1 ?>>
                    Ama de casa
                  </label>
                </div>
              </div>
              <div class="col-md-2 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input riquezas_otras" name="otras_actividades_generadoras_recurso" value="2" <?php echo $otras_actividades_generadoras_recursox2 ?>>
                    Estudiante
                  </label>
                </div>
              </div>
              <div class="col-md-2 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input riquezas_otras" name="otras_actividades_generadoras_recurso" value="3" <?php echo $otras_actividades_generadoras_recursox3 ?>>
                    Menor de Edad
                  </label>
                </div>
              </div>
              <div class="col-md-2 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input riquezas_otras" name="otras_actividades_generadoras_recurso" value="4" <?php echo $otras_actividades_generadoras_recursox4 ?>>
                    Otra actividad
                  </label>
                </div>
              </div>
            </div>
            <!-- preguntas dependientes de pregunta anterior -->
            <div id="div_cont_riqueza" style="display:none;">
              <h3>Origen de la riqueza para actividades no generadoras de ingresos</h3>
              <div class="subtitle" style="height: 3px !important;background-color: #2b254e;"></div>
              <br>
              <div class="row">
                <div class="col-md-3 form-group">
                  <label>Apoyo conyugal, parental o de pareja </label>
                  <input class="form-control" type="text" name="apoyo_conyugal_parental_pareja" value="<?php echo $apoyo_conyugal_parental_pareja ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Apoyo gubernamental</label>
                  <input class="form-control" type="text" name="apoyo_gubernamental" value="<?php echo $apoyo_gubernamental ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Herencia o donación</label>
                  <input class="form-control" type="text" name="herencia_donacion" value="<?php echo $herencia_donacion ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Fondo fiduciario</label>
                  <input class="form-control" type="text" name="fondo_fiduciario" value="<?php echo $fondo_fiduciario ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Préstamo o subvención</label>
                  <input class="form-control" type="text" name="prestamo_subvencion" value="<?php echo $prestamo_subvencion ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Inversiones</label>
                  <input class="form-control" type="text" name="inversiones" value="<?php echo $inversiones ?>">
                </div>
              </div>
              <div class="subtitle" style="height: 3px !important;background-color: #2b254e;"></div>
              <br><br>
            </div>

            <?php
              $cuenta_alguna_otra_fuente_ingresostx1='';
              $cuenta_alguna_otra_fuente_ingresostx2='';
              if($cuenta_alguna_otra_fuente_ingresos==1){
                $cuenta_alguna_otra_fuente_ingresostx1='checked';
                $cuenta_alguna_otra_fuente_ingresostx2='';
              }else if($cuenta_alguna_otra_fuente_ingresos==2){
                $cuenta_alguna_otra_fuente_ingresostx1='';
                $cuenta_alguna_otra_fuente_ingresostx2='checked';
              }
            ?>
            <div class="row">
              <div class="col-md-4">
                <label>¿Cuenta con alguna otra fuente de ingresos?</label>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="cuenta_alguna_otra_fuente_ingresos" value="1"  onclick="btn_cuenta_alguna_otra_fuente_ingresos()" <?php echo $cuenta_alguna_otra_fuente_ingresostx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="cuenta_alguna_otra_fuente_ingresos" value="2"  onclick="btn_cuenta_alguna_otra_fuente_ingresos()" <?php echo $cuenta_alguna_otra_fuente_ingresostx2 ?>>
                    No
                  </label>
                </div>
              </div>  
              <div class="col-md-6 form-group">
                <div class="text_cuenta_alguna_otra_fuente_ingresos" style="display: none">
                  <label>¿Cuál?</label>
                  <input class="form-control" type="text" name="fuente_ingresos_cual" value="<?php echo $fuente_ingresos_cual ?>">
                </div> 
              </div>
            </div>  
            <div class="row">
              <div class="col-md-12 form-group">
                <label>¿Cuál es el monto mensual que recibe de ingresos (considerando todas las fuentes de ingresos)</label>
                <input class="form-control" type="text" name="cual_monto_mensual_recibe_ingresos" value="<?php echo $cual_monto_mensual_recibe_ingresos ?>">
              </div>
            </div>  
            <?php
              $recibe_ingresos_extranjerotx1='';
              $recibe_ingresos_extranjerotx2='';
              if($recibe_ingresos_extranjero==1){
                $recibe_ingresos_extranjerotx1='checked';
                $recibe_ingresos_extranjerotx2='';
              }else if($recibe_ingresos_extranjero==2){
                $recibe_ingresos_extranjerotx1='';
                $recibe_ingresos_extranjerotx2='checked';
              }
            ?>
            <div class="row">
              <div class="col-md-4">
                <label>¿Recibe ingresos del extranjero?</label>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="recibe_ingresos_extranjero" value="1" onclick="btn_recibe_ingresos_extranjero()" <?php echo $recibe_ingresos_extranjerotx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="recibe_ingresos_extranjero" value="2" onclick="btn_recibe_ingresos_extranjero()" <?php echo $recibe_ingresos_extranjerotx2 ?>>
                    No
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="texto_recibe_ingresos_extranjero" style="display: none">
                  <label>¿De qué país(es)?</label>
                  <input class="form-control" type="text" name="ingresos_extranjero_de_que_pais" value="<?php echo $ingresos_extranjero_de_que_pais ?>">
                </div>  
              </div>
            </div>
            <?php
              $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1='';
              $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx2='';
              if($mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta==1){
                $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1='checked';
                $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx2='';
              }else if($mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta==2){
                $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1='';
                $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx2='checked';
              }
            ?>
            <div class="row">
              <div class="col-md-8">
                <label>¿Mantiene relaciones comerciales con alguna dependencia o entidad gubernamental, nacional o extranjera?</label>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta" value="1" onclick="btn_mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjera()" <?php echo $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta" value="2" onclick="btn_mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjera()" <?php echo $mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjeratx2 ?>>
                    No
                  </label>
                </div>
              </div>
              <div class="col-md-12 form-group">
                <div class="text_mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjera" style="display: none">
                  <label>¿Cuál(es)?</label>
                  <input class="form-control" type="text" name="nacional_extranjera_cual" value="<?php echo $nacional_extranjera_cual ?>">
                </div>  
              </div>
            </div>

            <div class="row" id="cont_porcs">  
              <div class="col-md-12 form-group">
                <label>Del total de pagos que recibe de sus clientes, favor de desglosar en porcentaje, ¿qué forma de pago utilizan?</label>
              </div>
              <div class="col-md-2 form-group">
                <label>Efectivo</label>
                <input class="form-control" type="number" name="efectivo" placeholder="%" max="100" value="<?php echo $efectivo ?>">
              </div>
              <div class="col-md-3 form-group">
                <label>Transferencia de otros bancos </label>
                <input class="form-control" type="number" name="transfer_otro" placeholder="%" max="100" value="<?php echo $transfer_otro ?>">
              </div>
              <div class="col-md-2 form-group">
                <label>Transferencias internacionales </label>
                <input class="form-control" type="number" name="transfer_inter" placeholder="%" max="100" value="<?php echo $transfer_inter ?>">
              </div>
              <div class="col-md-3 form-group">
                <label>Pago con tarjeta de crédito/debito </label>
                <input class="form-control" type="number" name="tarjeta" placeholder="%" max="100" value="<?php echo $tarjeta ?>">
              </div>
              <div class="col-md-2 form-group">
                <label>Cheques</label>
                <input class="form-control" type="number" name="cheques" placeholder="%" max="100" value="<?php echo $cheques ?>">
              </div>
            </div>
            <h3>Estado Civil y Dependientes Económicos</h3>
            <div class="subtitle" style="height: 1px !important;background-color: #2b254e;"></div>
            <br>
            <?php
              $usted_casado_vive_concubinatotx1='';
              $usted_casado_vive_concubinatotx2='';
              if($usted_casado_vive_concubinato==1){
                $usted_casado_vive_concubinatotx1='checked';
                $usted_casado_vive_concubinatotx2='';
              }else if($usted_casado_vive_concubinato==2){
                $usted_casado_vive_concubinatotx1='';
                $usted_casado_vive_concubinatotx2='checked';
              }
            ?>
            <div class="row">
              <div class="col-md-4">
                <label>¿Es usted casado o vive en concubinato?</label>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="usted_casado_vive_concubinato" value="1" onclick="btn_usted_casado_vive_concubinato()" <?php echo $usted_casado_vive_concubinatotx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="usted_casado_vive_concubinato" value="2" onclick="btn_usted_casado_vive_concubinato()" <?php echo $usted_casado_vive_concubinatotx2 ?>>
                    No
                  </label>
                </div>
              </div>
            </div>
            <div class="text_usted_casado_vive_concubinato" style="display: none;">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label>¿Cuál es el nombre de su esposa(o)?</label>
                  <input class="form-control" type="text" name="cual_nombre_esposa" value="<?php echo $cual_nombre_esposa ?>">
                </div>
                <div class="col-md-6 form-group">
                  <label>¿Cuál es la ocupación de su espos(a)?</label>
                  <input class="form-control" type="text" name="cual_ocupacion_espos" value="<?php echo $cual_ocupacion_espos ?>">
                </div>
              </div>
            </div>
            <div class="text_usted_casado_vive_concubinato" style="display: none;">
              <div class="row">
                <div class="col-md-3 form-group" >
                  <label>Edad:</label>
                  <input class="form-control" type="text" name="edad" value="<?php echo $edad ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Nacionalidad:</label>
                  <!--<input class="form-control" type="text" name="nacionalidad" id="pais_2" value="<?php echo $nacionalidad ?>">-->
                  <select class="form-control" name="nacionalidad" id="pais_4">
                    <?php if($nacionalidad!=''){
                      echo '<option value="'.$clave4.'" >'.$pais4.'<option>';
                    } ?>
                  </select>  
                </div>
              </div>
            </div>
            <?php
              $tiene_dependientes_economicostx1='';
              $tiene_dependientes_economicostx2='';
              if($tiene_dependientes_economicos==1){
                $tiene_dependientes_economicostx1='checked';
                $tiene_dependientes_economicostx2='';
              }else if($tiene_dependientes_economicos==2){
                $tiene_dependientes_economicostx1='';
                $tiene_dependientes_economicostx2='checked';
              }
            ?>
            <div class="row">
              <div class="col-md-4">
                <label>¿Tiene dependientes económicos?</label>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tiene_dependientes_economicos" value="1" onclick="btn_tiene_dependientes_economicos()" <?php echo $tiene_dependientes_economicostx1 ?>>
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="tiene_dependientes_economicos" value="2" onclick="btn_tiene_dependientes_economicos()" <?php echo $tiene_dependientes_economicostx2 ?>>
                    No
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="txto_tiene_dependientes_economicos" style="display: none">
                  <label>¿Cuántos?:</label>
                  <input class="form-control" type="number" min="1" name="cuantos" id="cuantos" value="<?php echo $cuantos; ?>" />
                </div>  
              </div>
            </div>
            <!-- -->
            <div class="txto_tiene_dependientes_economicos" style="display: none">
              <h3>Sobre los dependientes económicos</h3>
              <div class="subtitle" style="height: 3px !important;background-color: #2b254e;"></div>
              <br>
              <!--<div class="row">
                <div class="col-md-3 form-group">
                  <label>Parentesco</label>
                  <input class="form-control" type="text" name="parentesco" value="<?php echo $parentesco ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Nombre Completo</label>
                  <input class="form-control" type="text" name="nombre_ompleto" value="<?php echo $nombre_ompleto ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Género</label>
                  <select class="form-control" name="genero">
                    <option value="1" <?php if($genero==1) echo 'selected' ?>>Masculino</option>
                    <option value="2" <?php if($genero==2) echo 'selected' ?>>Femenino</option>
                  </select>
                </div>
                <div class="col-md-3 form-group">
                  <label>Fecha Nacimiento</label>
                  <input class="form-control" type="date" name="fecha_nacimiento" value="<?php echo $fecha_nacimiento ?>">
                </div>
                <div class="col-md-3 form-group">
                  <label>Ocupación</label>
                  <input class="form-control" type="text" name="ocupacion" value="<?php echo $ocupacion ?>">
                </div>
              </div>-->
              <!--<table width="100%" id="tabla_otros">
                <thead>
                  
                </thead>
                <tbody id="otros_dependientes">
                  <div id="div_otros_dependientes">
                  </div>
                  
                </tbody>
              </table>-->
            </form>
            <div class="row_otros_dependientes">
              <div class="datos_otros">
                
                <?php if(isset($get_ode)){ ?>
                  <!--<form class="form" method="post" role="form" id="form_cuestionario_otros" required> -->
                  <?php foreach($get_ode as $k){ ?>
                    <div class="row" id="row_<?php echo $k->id; ?>">
                      <input class="form-control" type="hidden" id="id" value="<?php echo $k->id; ?>">
                      <div class="col-md-3 form-group">
                        <label>Parentesco</label>
                        <input class="form-control" type="text" id="parentesco" value="<?php echo $k->parentesco; ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Nombre Completo</label>
                        <input class="form-control" type="text" id="nombre" value="<?php echo $k->nombre; ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Género</label>
                        <select class="form-control" id="genero" >
                          <option value="1" <?php if($k->genero==1) echo 'selected' ?>>Masculino</option>
                          <option value="2" <?php if($k->genero==2) echo 'selected' ?>>Femenino</option>
                        </select>
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Fecha Nacimiento</label>
                        <input class="form-control" type="date" id="fecha_nacimiento" value="<?php echo $k->fecha_nacimiento; ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label>Ocupación</label>
                        <input class="form-control" type="text" id="ocupacion" value="<?php echo $k->ocupacion; ?>">
                      </div>
                      <div class="col-md-3 form-group">
                        <label style="color:transparent">eliminar</label>
                        <button type="button" class="btn btn_borrar" onclick="eliminaDependienteId(<?php echo $k->id; ?>)"><i class="fa fa-minus"></i> </button>
                      </div>
                    </div>
          <?php  } ?>
                <!--</form>-->
              <?php } ?>
              
              </div>
            </div>
          </div>
            
          
          <div class="texto_firma" style="display: none">
            <br><br><br><br><br>
            <div class="row">
              <div class="col-md-6" align="center">
                <h3 style="color: black;"><u> Firma Cliente </u></h3>
                
              </div>
              <div class="col-md-6" align="center">
                <h3 style="color: black;"><u>Firma Ejecutivo Comercial / Vendedor</u></h3>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6" align="center">
                <h6 style="color: black;">Doy fé que los datos  proporcionados son veridicos y están vigentes</h6>
                
              </div>
              <div class="col-md-6" align="center">
                <h6 style="color: black;">Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial  </h6>
              </div>
            </div>
            <br>
          </div> 
          <div class="btn_guardar_text">
            <div class="row">
              <div class="col-md-12" align="right">
                <?php if($id>0){ ?>
                  <button type="button" class="btn gradient_nepal2" onclick="imprimir_for()"><i class="fa fa-print"></i> Imprimir formato</button>
                <?php } ?>
                <button type="button" class="btn gradient_nepal2" onclick="guardar_form_cuestionario()"><i class="fa  fa-floppy-o" ></i> Guardar</button>
              </div>
            </div> 
          </div>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalimprimir" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" align="center">
            <label>¿Deseas imprimir el formulario?                
            </label>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4 form-group">
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="1">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="2" checked>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_registro" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

