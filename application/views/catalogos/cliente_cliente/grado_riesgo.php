
<style type="text/css">
  textarea {
    color: #b57532 !important;
    border-radius: 60px !important;
    border-color: #b57532 !important;
    outline: 0px solid #e9e8ef00 !important;
    border: 1px solid #b57532 !important;
  }
  .select2-selection{
    color: #b57532 !important;
    border-radius: 60px !important;
    border-color: #b57532 !important;
    outline: 0px solid #e9e8ef00 !important; 
    border: 1px solid #b57532 !important;
    font-size: 14px;
  }
  .select2-selection__rendered{
    color: #b57532 !important;
  }
  .select2-container--open{
    color: #b57532 !important;
  }
  

</style>
<input type="hidden" id="desde_cal" value="1">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $idopera; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-rocket"></i> Regresar a Proceso</button></a>
        </div>
        <!---------------->
          <br>
          <div align="center">
             <h3>Calificar grado de riesgo del cliente <?php echo $anexo_name; ?></h3>
          </di>
          <hr class="subtitle">
          <h4 align="left" id="txt_title">Entidad a calificar</h4>
          <br>
          <div class="row">
            <input id="idopera" type="hidden" value="<?php echo $idopera; ?>">
            <div align="left" class="col-md-12">
              <?php if($tipo==1){ $giro = $info->activida_o_giro; $edo = $info->estado_d; ?> 
              <h4>Tipo de persona: Persona fisica mexicana o extranjera con condición de estancia temporal permanente.</h4>
              <?php }else if($tipo==2){ $giro = $info->actividad_o_giro; $edo = $info->estado_d; ?>
                  <h4>Tipo de persona: Persona fisica extranjera con condición de estancia de visitante.</h4>
              <?php }else if($tipo==3){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
              <h4>Tipo de persona: Persona moral de nacionalidad mexicana, extranjera y persona moral mexicana del derecho público.</h4>
              <?php }else if($tipo==4){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
              <h4>Tipo de persona: Persona moral mexicana del derecho público (Anexo 7 Bis A).</h4>
              <?php }else if($tipo==5){ $giro = $info->giro_mercantil; $edo = $info->estado_d; ?>
              <h4>Tipo de persona: Embajadas, consulados u organismo internacionales.</h4>
              <?php }else if($tipo==6){ $giro = ""; $edo=""; ?>
                  <h4>Tipo de persona: Fideicomisos.</h4>
              <?php } ?>  
            </div>  
          </div>
          <?php if($tipo==1 || $tipo==2){ ?>
            <div class="row">
              <div class="col-md-6" align="left">
                  <h4>Nombre:
                  <?php echo $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno."</h4>"; ?>
              </div>
              <div align="left" class="col-md-6">
                <h4>Fecha de nacimiento:
                <?php echo $fechan=$info->fecha_nacimiento."</h4>"; ?>
              </div>
            </div>
            
            <br><br>
          <?php } ?>
          <?php if($tipo==3){ ?> 
            <div class="row">
              <div align="left" class="col-md-6">
                <br><h4>Razón social:
                <?php echo $info->razon_social."</h4>"; ?>
              </div>
              <div align="left" class="col-md-6">
                <br><h4>Fecha de constitución:
                <?php echo $fechan=$info->fecha_constitucion_d."</h4>";  ?>
              </div>
            </div><br>
          <?php } ?>
          <?php if($tipo==4){ ?> 
            <div class="row">
              <div align="left" class="col-md-6">
                <h4>Razón social:
                <?php echo $info->nombre_persona."</h4>"; ?>
              </div>
              <div align="left" class="col-md-6">
                <br><h4>Fecha de constitución:
                <?php echo $fechan=$info->fecha_constitucion_d."</h4>";  ?>
              </div>
            </div><br>
          <?php } ?>
          <?php if($tipo==5 || $tipo==6){ ?> 
            <div class="row">
              <div align="left" class="col-md-6">
                <h4>Denominación:
                <?php echo $info->denominacion."</h4>"; ?>
              </div>
              <?php if($tipo==5){ ?> 
                  <div align="left" class="col-md-6">
                    <br><h4>Fecha de constitución:
                    <?php echo $fechan=$info->fecha_establecimiento."</h4>"; ?>
                  </div>
              <?php } ?>
              <?php if($tipo==6){ ?> 
                  <div align="left" class="col-md-6">
                    <br><h4>Fecha de constitución:
                    <?php //echo $info->fecha_nacimiento_g."</h4>"; ?>
                  </div>
              <?php } ?>
            </div>
          <?php } ?>

          <form class="form" method="post" role="form" id="form_grado" required> 
            <input type="hidden" id="id" name="id" value="<?php if(isset($gr->id)) echo $gr->id; else echo "0"; ?>">
            <input type="hidden" id="tipoc" value="<?php echo $tipo; ?>">
            <input type="hidden" name="id_perfilamiento" id="idperfilamiento" value="<?php echo $idperfilamiento; ?>">
            <input type="hidden" name="id_clientec" id="idclientec" value="<?php echo $idclientec; ?>"> 
            <input id="grado_edo" type="hidden" value="<?php if (isset($edos->riesgo)) echo $edos->riesgo; else echo '0'?>">
            <input id="idopera" name="id_operacion" type="hidden" value="<?php echo $idopera; ?>">
            <input id="idact" type="hidden" name="id_actividad" value="<?php echo $idact; ?>">
            <input type="hidden" id="id_union" value="<?php echo $id_union; ?>">
            <div align="left" class="row">
         
              <?php if($idact==11) { //vehiculos ?>
                <div class="col-md-12 form-group">
                  <label>Productos y servicios:</label>
                  <select class="form-control" id="actividad_monto" name="actividad_monto">
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="1"){ echo "selected"; } ?> value="1">El vehículo tiene un valor superior a $575,000 pesos</option>
                    <option data-nivelriesgo="2" <?php if(isset($gr) && $gr->actividad_monto=="2"){ echo "selected"; } ?> value="2">El vehículo tiene un valor superior a $287,500 pesos e inferior a $575,000 pesos</option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->actividad_monto=="3"){ echo "selected"; } ?> value="3">El vehículo tiene un valor inferior a $287,500 pesos</option>
                  </select>
                </div>
              <?php } ?>
               <?php if($idact==12) { //blindaje ?>
                <div class="col-md-12 form-group">
                  <label>Productos y servicios:</label>
                  <select class="form-control" id="actividad_monto" name="actividad_monto">
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="1"){ echo "selected"; } ?> value="1">El blindaje es de valor superior a $430,000 pesos </option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->actividad_monto=="2"){ echo "selected"; } ?> value="2">El blindaje es de valor inferior a $215,000 pesos</option>
                    <option data-nivelriesgo="2" <?php if(isset($gr) && $gr->actividad_monto=="3"){ echo "selected"; } ?> value="3">El blindaje es de valor superior a  a $215,000 pesos pero inferior a $430,000 pesos</option>
                  </select>
                </div>
              <?php } ?>
              <?php if($idact==15 || $idact==16) { //notarios anexo 12a y 12b ?>
                <div class="col-md-12 form-group">
                  <label>Productos y servicios:</label>
                  <select class="mi_select2 form-control" id="actividad_monto" name="actividad_monto">
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="1"){ echo "selected"; } ?> value="1">Transmisión o constitución de derechos reales sobre bienes inmuebles (no considera la constitución de garantías a favor del sistema financiero o de algún organismo público de vivienda)</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="2"){ echo "selected"; } ?> value="2">El otorgamiento de poderes irrevocables para actos de administración y dominio </option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="3"){ echo "selected"; } ?> value="3">Constitución de personas morales, cuando la operación se realicen por un monto igual o superior a 8025 UMAS (en pesos $697,212 al valor de la UMA del año 2020)</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="4"){ echo "selected"; } ?> value="4">Constitución de personas morales, cuando al menos el 50% de los accionistas sean personas de entre 18 y 25 años</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="5"){ echo "selected"; } ?> value="5">Modificación patrimonial de personas morales (aumento o disminución del capital social), cuando las operaciones se realicen por un monto igual o superior a 8025 UMAS (en pesos $697,212 al valor de la UMA del año 2020) </option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="6"){ echo "selected"; } ?> value="6">Modificación patrimonial de personas morales (aumento o disminución del capital social), cuando  al menos el 50% de los accionistas sean personas de entre 18 y 25 años</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="7"){ echo "selected"; } ?> value="7">Fusión o escisión de personas morales, cuando las operaciones se realicen por un monto igual o superior a 8025 UMAS (en pesos $697,212 al valor de la UMA del año 2020)</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="8"){ echo "selected"; } ?> value="8">Compra venta de acciones y partes sociales de personas morales </option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="9"){ echo "selected"; } ?> value="9">Constitución o modificación de fideicomisos traslativos de dominio</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="10"){ echo "selected"; } ?> value="10">Constitución o modificación de fideicomisos de garantía sobre inmuebles (no considera la constitución de garantías a favor del sistema financiero o de algún organismo público de vivienda)</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="11"){ echo "selected"; } ?> value="11">Otorgamiento de contratos de mutuo o de crédito, con o sin garantía (no considera casos donde el acreedor forma parte del sistema financiero o de algún organismo público de vivienda)</option>
                    <option data-nivelriesgo="3" <?php if(isset($gr) && $gr->actividad_monto=="12"){ echo "selected"; } ?> value="11">Realización de Avalúos con valor igual o superior a $650 mil pesos </option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->actividad_monto=="13"){ echo "selected"; } ?> value="12">Otros servicios</option>
                  </select>
                </div>
              <?php } ?>
              <!--<div class="col-md-12 form-group">
                <label>Tipo de cliente:</label>
                <select style="font-weight: bold;" class="form-control" id="idtipo_cliente" disabled="">
                  <?php foreach ($get_tipo_cliente as $i) { ?>
                      <option <?php if($tipo==$i->idtipo_cliente) echo "selected"; ?> value="<?php echo $i->idtipo_cliente ?>"><?php echo $i->tipo ?></option>
                   <?php } ?>
                </select>
              </div>-->
              
              <div class="col-md-12 form-group">
                <label>Tipo de cliente:</label>
                <select style="font-weight: bold;" class="form-control" id="idtipo_cliente" name="idtipo_cliente">
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="1"){ echo "selected"; } ?> value="1">Persona Física Nacionalidad Mexicana y Extranjera</option>
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="2"){ echo "selected"; } ?> value="2">Persona Moral Nacionalidad Mexicana y Extranjera</option>
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="3"){ echo "selected"; } ?> value="3">Entidades Gubernamentales Federales, Estatales y Municipales</option>
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="4"){ echo "selected"; } ?> value="4">Entidades Financieras Bancarias y no Bancarias</option>
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="5"){ echo "selected"; } ?> value="5">Personas Políticamente Expuestas nacionales e internacionales</option>
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="6"){ echo "selected"; } ?> value="6">Fideicomisos</option>
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="7"){ echo "selected"; } ?> value="7">Asociaciones Sin Fines de Lucro</option>
                  <option <?php if(isset($gr) && $gr->idtipo_cliente=="8"){ echo "selected"; } ?> value="8">Embajadas, Consulados u Organismos Internacionales</option>
                </select>
              </div>
            
              <div class="col-md-12 form-group" id="cont_det_tipo">
                <label>Detalle tipo de cliente:</label>
                <select class="form-control" id="detalle_tipo_cli" name="detalle_tipo_cli">
                  <option class="nda" data-nivelriesgo="0" value="">Elige una opción:</option>
                  <option class="ent_gub" data-nivelriesgo="2.3" <?php if(isset($gr) && $gr->detalle_tipo_cli=="1"){ echo "selected"; } ?> value="1">Municipios</option>
                  <!--<option class="ent_gub" <?php if($idact==11 || $idact==12) echo 'data-nivelriesgo="2.3"'; if($idact==15 || $idact==16) echo 'data-nivelriesgo="2.3"'; ?> <?php if(isset($gr) && $gr->detalle_tipo_cli=="2"){ echo "selected"; } ?> value="2">Organismos Internacionales</option>-->
                  <option class="ent_gub" data-nivelriesgo="2.1" <?php if(isset($gr) && $gr->detalle_tipo_cli=="3"){ echo "selected"; } ?> value="3">Dependencias Estatales</option>
                  <option class="ent_gub" data-nivelriesgo="2.0" <?php if(isset($gr) && $gr->detalle_tipo_cli=="4"){ echo "selected"; } ?> value="4">Dependencias Federales</option>

                  <option class="ent_finan" data-nivelriesgo="2.0" <?php if(isset($gr) && $gr->detalle_tipo_cli=="5"){ echo "selected"; } ?> value="5">Instituciones Financieras Nacionales</option>
                  <option class="ent_finan" data-nivelriesgo="2.1" <?php if(isset($gr) && $gr->detalle_tipo_cli=="6"){ echo "selected"; } ?> value="6">Instituciones Financieras Extranjeras</option>
                  <option class="fides_tc" data-nivelriesgo="2" <?php if(isset($gr) && $gr->detalle_tipo_cli=="7"){ echo "selected"; } ?> value="7">Fideicomisos</option><!-- actualizados para 3era version de ajuste 18-04-22 -->
                  <option class="asocia_sin" data-nivelriesgo="2.4" <?php if(isset($gr) && $gr->detalle_tipo_cli=="8"){ echo "selected"; } ?> value="8">Sindicatos</option>
                  <option class="asocia_sin" data-nivelriesgo="2.4" <?php if(isset($gr) && $gr->detalle_tipo_cli=="9"){ echo "selected"; } ?> value="9">Empresas de beneficencia</option>
                  <option class="asocia_sin" data-nivelriesgo="2.4" <?php if(isset($gr) && $gr->detalle_tipo_cli=="10"){ echo "selected"; } ?> value="10">Organizaciones no gubernamentales</option>
                  <option class="asocia_sin" data-nivelriesgo="2.4" <?php if(isset($gr) && $gr->detalle_tipo_cli=="11"){ echo "selected"; } ?> value="11">Otro tipo de asociación o sociedad sin fines de lucro</option>
                  <option class="emb" data-nivelriesgo="1.5" <?php if(isset($gr) && $gr->detalle_tipo_cli=="12"){ echo "selected"; } ?> value="12">Embajadas, Consulados u Organismos Internacionales</option> <!-- ajuste de calc ultima version -->
                </select>
              </div>
        
              <div class="col-md-12 form-group clasificacionform">
                <label>Clasificación:</label>
                <select class="form-control" id="clasificacion" name="clasificacion">
                  <option data-nivelriesgo="0" value="">Elige una opción:</option>
                  <?php //if($idact==15 || $idact==16) { //notarios anexo 12a y 12b ?>
                    <option class="clasificacion_1" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="1"){ echo "selected"; } ?> value="1">El Cliente o Usuario es un(a) precandidato(a), candidato(a) o dirigente partidista y/o servidor(a) público(a)</option>
                    <option class="clasificacion_1" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="2"){ echo "selected"; } ?> value="2">El Cliente o Usuario contrata un servicio a favor de un(a) precandidato(a), candidato(a)o dirigente partidista y/o servidor(a) público(a)</option>

                    <?php if($idact==11 || $idact==15 || $idact==16) { ?>
                      <option class="clasificacion_1" data-nivelriesgo="3.5"
                        <?php if(isset($gr) && $gr->clasificacion=="3"){ echo "selected"; } ?> value="3">Un(a) precandidato(a), candidato(a)o dirigente partidista y/o servidor (a) público (a) aparece como beneficiario o tercero relacionado del contrato que se está celebrando con el cliente o usuario</option>
                    <?php } ?>

                     <?php if($idact==11 || $idact==15 || $idact==16) { ?>
                      <option class="clasificacion_1" data-nivelriesgo="3.5"
                        <?php if(isset($gr) && $gr->clasificacion=="4"){ echo "selected"; } ?> value="4">El Cliente o Usuario es beneficiario o tercero relacionado de algún contrato de un(a) precandidato(a), candidato(a)o dirigente partidista y/o servidor(a) público(a)</option>
                      <?php } ?>

                    <option class="clasificacion_1" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="5"){ echo "selected"; } ?> value="5">El cliente o usuario es familiar en primer grado de un(a) precandidato(a), candidato(a), dirigente partidista y/o servidor(a) público(a) </option>
                    <option class="clasificacion_1" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="6"){ echo "selected"; } ?> value="6">El cliente o usuario es familiar en segundo grado de un(a) precandidato(a), candidato(a), dirigente partidista y/o servidor(a) público(a) </option>
                    <option class="clasificacion_1" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="7"){ echo "selected"; } ?> value="7">El cliente o usuario es familiar político de un(a) precandidato(a), candidato(a), dirigente partidista y/o servidor(a) público(a)</option>

                    <?php if($idact==11 || $idact==12) { ?>
                      <option class="clasificacion_1" data-nivelriesgo="1"
                       <?php if(isset($gr) && $gr->clasificacion=="8"){ echo "selected"; } ?> value="8">El Clientes es Persona Física</option>
                    <?php } ?>
                    <?php if($idact==15 || $idact==16) { ?>
                      <option class="clasificacion_1" data-nivelriesgo="1"
                       <?php if(isset($gr) && $gr->clasificacion=="9"){ echo "selected"; } ?> value="9">Ninguno de las anteriores</option>
                    <?php } ?>

                  <?php //} ?>

                  <?php //if($idact==11) { //vehiculos ?>
                    <!-- VEHICULOS, BLINDAJE -->
                    <option class="clasificacion_1_de_11" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="9" && $idact==11 || isset($gr) && $gr->clasificacion=="9" && $idact==12){ echo "selected"; } ?> value="9">El Cliente o Usuario es un(a) precandidato(a), candidato(a) o dirigente partidista y/o servidor (a) público (a). </option>
                    <option class="clasificacion_1_de_11" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="10"){ echo "selected"; } ?> value="10">El Cliente o Usuario adquiere el vehículo a favor de un(a) precandidato(a), candidato(a)o dirigente partidista y/o servidor (a) público (a) a pesar de que su actividad económica esté o no relacionada. </option>
                    <option class="clasificacion_1_de_11" data-nivelriesgo="3.5"
                        <?php if(isset($gr) && $gr->clasificacion=="11"){ echo "selected"; } ?> value="11">Un(a) precandidato(a), candidato(a)o dirigente partidista y/o servidor (a) público (a) aparece como beneficiario o tercero relacionado del contrato que se está celebrando con el cliente o usuario</option>
                    <option class="clasificacion_1_de_11" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="12"){ echo "selected"; } ?> value="12">El Cliente o Usuario es beneficiario o tercero relacionado de algún contrato de un(a) precandidato(a), candidato(a)o dirigente partidista y/o servidor (a) público (a).  </option>
                    <option class="clasificacion_1_de_11" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="13"){ echo "selected"; } ?> value="13">El cliente o usuario es familiar en primer grado de un(a) precandidato(a), candidato(a), dirigente partidista y/o servidor(a) público(a) - (esposo(a), padres o tutores, hijos(as)) </option>
                    <option class="clasificacion_1_de_11" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="14"){ echo "selected"; } ?> value="14">El cliente o usuario es familiar en segundo grado de un(a) precandidato(a), candidato(a), dirigente partidista y/o servidor(a) público(a) - tío(a), abuelo(a), sobrino(a)</option>
                    <option class="clasificacion_1_de_11" data-nivelriesgo="3.5"
                      <?php if(isset($gr) && $gr->clasificacion=="15"){ echo "selected"; } ?> value="15">El cliente o usuario es familiar en político de un(a) precandidato(a), candidato(a), dirigente partidista y/o servidor(a) público(a) : cuñado(a), concuño(a), suegro(a)</option>
                    <option class="clasificacion_1_de_11" data-nivelriesgo="1.0"
                      <?php if(isset($gr) && $gr->clasificacion=="16"){ echo "selected"; } ?> value="16">El Clientes es Persona Física</option>
                    <!-- TERMINA VEHICULOS  BLINDAJE-->
                  <?php //} ?>

                  <option class="clasificacion_5" data-nivelriesgo="2.8"
                    <?php if(isset($gr) && $gr->clasificacion=="17"){ echo "selected"; } ?> value="17">Funcionario Federal</option>
                  <option class="clasificacion_5" data-nivelriesgo="2.9"
                    <?php if(isset($gr) && $gr->clasificacion=="18"){ echo "selected"; } ?> value="18">Funcionario Estatal</option>
                  <option class="clasificacion_5" data-nivelriesgo="4.0"
                    <?php if(isset($gr) && $gr->clasificacion=="19"){ echo "selected"; } ?> value="19">Funcionario Municipal</option>
                  <option class="clasificacion_5" data-nivelriesgo="2.8"
                    <?php if(isset($gr) && $gr->clasificacion=="20"){ echo "selected"; } ?> value="20">Otro</option>
                </select>
              </div>
              <div class="col-md-12 form-group" id="cont_constitucion">
                <label id="name_edad">Edad / Constitución</label>
                <select class="form-control" id="edad" disabled="" name="edad">
                  <option data-nivelriesgo="0" value=""></option>
                  <?php /*if($info->edad<300) //- 25 años 
                        if($info->edad>=312 && $info->edad<=420) //26 a 35 años
                        if($info->edad>=432) //Mayor de 36 años
                        if($tipo==6)
                        if($info->edad>37 && $info->edad<299) //Mayor de 37 meses
                        if($info->edad>13 && $info->edad<36) //Mayor a 13 meses, menor a 36 meses
                        if($info->edad<=12)*/ //Menor o igual a 12 meses
                  ?>
                  <?php if($tipoc==1 || $tipo==2){ ?>
                    <option data-nivelriesgo="3"
                            <?php if($info->edad<300 && $info->edad>12) echo "selected"; ?> 
                            value="1">Menos de 25 años</option>
                    <option data-nivelriesgo="2.0"
                            <?php if($info->edad>=312 && $info->edad<=431) echo "selected" ;?> 
                            value="2">26 a 35 años</option>
                    <option data-nivelriesgo="3"
                            <?php if($info->edad>=840) echo "selected" ;?> 
                            value="3">Mayor de 70 años</option>
                    <option data-nivelriesgo="1"
                            <?php if($info->edad>=432 && $info->edad<840) echo "selected" ;?> 
                            value="4">Mayor de 36 años</option>
                    
                  <?php } ?>
                  <?php if($tipoc==3 || $tipoc==4){ //es moral en calculadora y tipo 3 en perfilamiento ?>
                    <option data-nivelriesgo="1"
                            <?php if($info->edad>=37 ) echo "selected" ;?> 
                            value="5">Mayor a 37 meses</option>
                    <option data-nivelriesgo="2"
                            <?php if($info->edad>=13 && $info->edad<36 ) echo "selected" ;?> 
                            value="6">Mayor a 13 meses, menor a 36 meses</option>
                    <option data-nivelriesgo="3"
                            <?php if($info->edad<=12 ) echo "selected" ;?> 
                            value="7">Menor o igual a 12 meses</option>
                  <?php } ?>
                  <?php /*echo "edad ".$info->edad; echo "tipoc ".$tipoc;*/ if($tipoc==5){ ?>
                    <option data-nivelriesgo="3.5"
                            <?php if($info->edad<300 && $info->edad>12 && $tipoc==5) echo "selected"; ?> 
                            value="8">Menos de 25 años</option>
                    <option data-nivelriesgo="3.0"
                            <?php if($info->edad>=312 && $info->edad<=431 && $tipoc==5) echo "selected" ;?> 
                            value="9">26 a 35 años</option>
                    <option data-nivelriesgo="3.5"
                            <?php if($info->edad>=840) echo "selected" ;?> 
                            value="10">Mayor de 70 años</option>
                    <option data-nivelriesgo="2.5"
                            <?php if($info->edad>=432 && $info->edad<840 && $tipoc==5) echo "selected" ;?> 
                            value="11">Mayor de 36 años</option>
                    
                  <?php } ?>
                  <?php if($tipoc==5 || $tipoc==6){ ?>
                    <option data-nivelriesgo="0" selected="" value="12">N/A</option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-12 form-group" id="cont_nacional">
                <label>Nacionalidad:</label>
                <select class="form-control" id="nacionalidad" name="nacionalidad" disabled>
                  <option value="0">Elige una opción:</option>
                  <option <?php if($pais_nac=="MX"){ echo "selected"; } ?> value="1">Mexicana</option>
                  <option <?php if($pais_nac!="MX"){ echo "selected"; } ?> value="2">Extranjera</option>
                </select>
              </div>
              <?php if($idact==11 || $idact==12) { //anexo 8 vehiculos y anexo 9 blindaje ?>
                <div class="col-md-12 form-group" id="cont_profe">
                  <label>Profesión / Actividad:</label>
                  <select class="form-control" id="profesion" name="profesion" disabled>
                    <option data-nivelriesgo="3" class="prof_pfme" <?php if(isset($gr) && $gr->profesion=="1"){ echo "selected"; } if($grado_riesgo_giro==2) echo "selected"; ?> value="1"><?php echo $nameact."/"; ?> <br>Profesión de alto riesgo</option>
                    <option data-nivelriesgo="1" class="prof_pfme" <?php if(isset($gr) && $gr->profesion=="2"){ echo "selected"; } if($grado_riesgo_giro==1) echo "selected"; ?> value="2"><?php echo $nameact."/"; ?> <br>Profesión de bajo riesgo</option>
                    <option data-nivelriesgo="1" class="prof_pmm" <?php if(isset($gr) && $gr->profesion=="3"){ echo "selected"; } if($grado_riesgo_giro==1) echo "selected"; ?> value="3"><?php echo $nameact."/"; ?> <br>Actividad comercial de riesgo bajo</option>
                    <option data-nivelriesgo="3" class="prof_pmm" <?php if(isset($gr) && $gr->profesion=="4"){ echo "selected"; } if($grado_riesgo_giro==2) echo "selected"; ?> value="4"><?php echo $nameact."/"; ?> <br>Actividad comercial de riesgo alto</option>
                  </select>
                </div>
              <?php } ?>
              <?php if($idact==15 || $idact==16) { //notarios anexo 12a y 12b ?>
                <div class="col-md-12 form-group" id="cont_profe">
                  <label>Profesión / Actividad:</label>
                  <select class="form-control" id="profesion" name="profesion" disabled>
                    <option data-nivelriesgo="3" class="prof_pfme" <?php if(isset($gr) && $gr->profesion=="1"){ echo "selected"; } else if($grado_riesgo_giro==2) echo "selected"; ?> value="1"><?php echo $nameact."/"; ?><br> Profesión de alto riesgo</option>
                    <option data-nivelriesgo="1" class="prof_pfme" <?php if(isset($gr) && $gr->profesion=="2"){ echo "selected"; } else if($grado_riesgo_giro==1) echo "selected"; ?> value="2"><?php echo $nameact."/"; ?><br> Profesión de bajo riesgo</option>
                    <option data-nivelriesgo="0" class="prof_pfme" <?php if(isset($gr) && $gr->profesion=="3"){ echo "selected"; } else if($grado_riesgo_giro==1) echo "selected"; ?> value="3">N/A</option>
                    <option data-nivelriesgo="1" class="prof_pmm" <?php if(isset($gr) && $gr->profesion=="4"){ echo "selected"; } else if($grado_riesgo_giro==1) echo "selected"; ?> value="4"><?php echo $nameact."/"; ?><br> Actividad comercial de riesgo bajo</option>
                    <option data-nivelriesgo="3" class="prof_pmm" <?php if(isset($gr) && $gr->profesion=="5"){ echo "selected"; } else if($grado_riesgo_giro==2) echo "selected"; ?> value="5"><?php echo $nameact."/"; ?><br> Actividad comercial de riesgo alto</option>
                  </select>
                </div>
              <?php } ?>

              <?php if($idact==11) { //anexo 8 vehiculos ?>
                <div class="col-md-12 form-group" id="cont_historico">
                  <label>Histórico de compras:</label>
                  <select class="form-control" id="historico" name="historico">
                    <option <?php if(isset($gr) && $gr->historico=="1"){ echo "selected"; } ?> value="1">Primer automóvil que el cliente adquiere</option>
                    <option <?php if(isset($gr) && $gr->historico=="2"){ echo "selected"; } ?> value="2">En los últimos 12 meses, el cliente ha adquirido más de un vehículo con valor superior a $287,500 pesos</option>
                    <option <?php if(isset($gr) && $gr->historico=="3"){ echo "selected"; } ?> value="3">En los últimos 12 meses, el cliente ha adquirido más de un vehículo con valor superior a $575,000 pesos</option>
                  </select>
                </div>
              <?php } ?>
              <?php if($idact==12) { //anexo 9 blindaje ?>
                <div class="col-md-12 form-group" id="cont_historico">
                  <label>Histórico de compras:</label>
                  <select class="form-control" id="historico" name="historico">
                    <option <?php if(isset($gr) && $gr->historico=="1"){ echo "selected"; } ?> value="1">Es el primer servicio que el cliente adquiere</option>
                    <option <?php if(isset($gr) && $gr->historico=="2"){ echo "selected"; } ?> value="2">En los últimos 12 meses, el cliente ha blindado más de un activo, por más de $430,000 pesos (vehículos terrestres nuevos o usados y/o bienes inmuebles)</option>
                    <option <?php if(isset($gr) && $gr->historico=="3"){ echo "selected"; } ?> value="3">En los últimos 12 meses, el cliente ha blindado más de un activo, por más de $215,000 pesos (vehículos terrestres nuevos o usados y/o bienes inmuebles)</option>
                  </select>
                </div>
              <?php } ?>
              <?php if($idact==15 || $idact==16) { //notarios anexo 12a y 12b ?>
                <div class="col-md-12 form-group">
                  <label>Histórico de compras:</label>
                  <select class="form-control" id="historico" name="historico">
                    <option <?php if(isset($gr) && $gr->historico=="1"){ echo "selected"; } ?> value="1">Es el primer servicio que el cliente solicita</option>
                    <option <?php if(isset($gr) && $gr->historico=="2"){ echo "selected"; } ?> value="2">En los últimos 12 meses, el cliente ha adquirido más de un servicio de los listados en el combo "productos/servicios"</option>
                    <option <?php if(isset($gr) && $gr->historico=="3"){ echo "selected"; } ?> value="3">En los últimos 12 meses, el cliente ha solicitado más de un servicio diferente a los listados en el combo "productos/servicios"</option>
                  </select>
                </div>
              <?php } ?>
              <div class="col-md-12 form-group" id="cont_est_acc">
                <label>Estructura accionaria:</label>
                <select class="form-control" id="estructura" name="estructura">
                  <option value=""></option>
                  <option <?php if(isset($gr) && $gr->estructura=="1"){ echo "selected"; } ?> value="1">La estructura accionaria está compuesta por otra(s) empresa(s)</option>
                  <option <?php if(isset($gr) && $gr->estructura=="2"){ echo "selected"; } ?> value="2">La estructura accionaria está compuesta por más de 5 accionistas personas físicas</option>
                  <option <?php if(isset($gr) && $gr->estructura=="3"){ echo "selected"; } ?> value="3">La estructura accionaria está compuesta por 4 personas o menos</option>
                </select>
              </div>
              <div class="col-md-12 form-group">
                <label>Área geográfica de riesgo bajo, medio o alto:</label>
                <select class="form-control" disabled="" id="paises" name="paises">
                  <option <?php if(isset($gr) && $gr->paises=="1" || $estadoinfo==1){ echo "selected"; } ?> value="1">Área geográfica de riesgo bajo</option>
                  <option <?php if(isset($gr) && $gr->paises=="2" || $estadoinfo==2){ echo "selected"; } ?> value="2">Área geográfica de riesgo medio</option>
                  <option <?php if(isset($gr) && $gr->paises=="3" || $estadoinfo==3){ echo "selected"; } ?> value="3">Área geográfica de riesgo alto</option>
                </select>
              </div>

              <div class="col-md-12 form-group" id="padre_trans_pep">
                
                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg22">
                  <label>Forma de Pago:</label>
                  <select class="form-control trans_pep preg_pep_most" id="preg1" name="preg1">
                    <option data-nivelriesgo="0" value=""></option>
                    <option data-nivelriesgo="4.5" <?php if(isset($gr) && $gr->preg1=="1"){ echo "selected"; } ?> value="1">El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se liquidará total o parcialmente en efectivo</option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->preg1=="2"){ echo "selected"; } ?> value="2">El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se liquidará con transferencia bancaria nacional</option>
                    <option data-nivelriesgo="4" <?php if(isset($gr) && $gr->preg1=="3"){ echo "selected"; } ?> value="3">El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se liquidará con transferencia bancaria internacional</option>
                  </select>
                </div>
                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg23" style="display: none">
                  <label>¿De qué país provienen los fondos?</label>
                  <select class="form-control trans_pep" name="preg2" id="preg2">
                    <?php
                      foreach ($get_pais as $p){
                        $select="";
                        if(isset($gr) && $gr->preg2==$p->clave){ $select="selected"; }
                          echo "<option data-nivelriesgo='0' value='$p->clave' $select>$p->pais</option>";
                      }
                    ?>
                  </select>
                </div>
                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg24" style="display: none">
                  <label>¿Por qué los fondos provienen de ese país? </label>
                  <textarea id="preg3" name="preg3" data-nivelriesgo="0" class="form-control"><?php if(isset($gr)){ echo $gr->preg3; } ?></textarea>
                </div>
                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg25">
                  <label></label>
                  <label>¿El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se liquidará en uno o varios pagos?</label>
                  <select class="form-control trans_pep" id="preg4" name="preg4">
                    <option data-nivelriesgo="0" value=""></option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->preg4=="1"){ echo "selected"; } ?> value="1">Un único pago</option>
                    <option data-nivelriesgo="2.5" <?php if(isset($gr) && $gr->preg4=="2"){ echo "selected"; } ?> value="2">Mas de un pago</option>
                  </select>
                </div>
                <!-- agregado recientemente -->
                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg26">
                  <label>¿El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> lo pagará el cliente directamente?</label>
                  <select class="form-control trans_pep preg_pep_most" id="preg5" name="preg5">
                    <option data-nivelriesgo="0" value=""></option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->preg5=="1"){ echo "selected"; } ?> value="1">Si</option>
                    <option data-nivelriesgo="4" <?php if(isset($gr) && $gr->preg5=="2"){ echo "selected"; } ?> value="2">No</option>
                  </select>
                </div>
                <!-- pregunta para respuesta si en la pregunta anterior -->
                <!--<div class="col-md-12 form-group <?php if($idact==15 || $idact==16) echo "transaccionalidacliente_pep"; else echo "transaccionalidacliente_pep_no"; ?>" id="preg26_1" style="display: none">
                  <label>Indique lo siguiente:</label>
                  <select class="form-control trans_pep preg_pep_most" id="preg10" name="preg10">
                    <option data-nivelriesgo="0" value=""></option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->preg10=="1"){ echo "selected"; } ?> value="1">El servicio se paga con recursos provenientes de una sola cuenta o institución bancaria</option>
                    <option data-nivelriesgo="2" <?php if(isset($gr) && $gr->preg10=="2"){ echo "selected"; } ?> value="2">El servicio se paga con recursos provenientes de más de una cuenta o institución bancaria</option>
                  </select>
                </div>-->

                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg27" style="display: none">
                  <label>¿Quién está liquidando el <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?>?</label>
                  <textarea id="preg6" name="preg6" data-nivelriesgo="0" class="form-control"><?php if(isset($gr)){ echo $gr->preg6; } ?></textarea>
                </div>
                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg28" style="display: none">
                  <label>¿Qué relación tiene la persona que liquidará el <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> con el cliente?</label>
                  <textarea id="preg7" name="preg7" data-nivelriesgo="0" class="form-control"><?php if(isset($gr)){ echo $gr->preg7; } ?></textarea>
                </div>
                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg29" style="display: none">
                  <label>¿El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> lo liquidará alguna empresa en donde el PEP o algún familiar sea accionista?</label>
                  <select class="form-control trans_pep preg_pep_most" id="preg8" name="preg8">
                    <option data-nivelriesgo="0" value=""></option>
                    <option data-nivelriesgo="0" <?php if(isset($gr) && $gr->preg8=="1"){ echo "selected"; } ?> value="1">Si</option>
                    <option data-nivelriesgo="0" <?php if(isset($gr) && $gr->preg8=="2"){ echo "selected"; } ?> value="2">No</option>
                  </select>
                </div>
                <!-- agregado recientemente -->


                <div class="col-md-12 form-group transaccionalidacliente_pep" id="preg30">
                  <label>Tipo de Moneda:</label>
                  <select class="form-control trans_pep" id="preg9" name="preg9">
                    <option data-nivelriesgo="0" value=""></option>
                    <option data-nivelriesgo="1" <?php if(isset($gr) && $gr->preg9=="1"){ echo "selected"; } ?> value="1">El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se liquida en moneda nacional</option>
                    <option data-nivelriesgo="4" <?php if(isset($gr) && $gr->preg9=="2"){ echo "selected"; } ?> value="2">El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se liquidará con dólares de EE.UU o cualquier otra divisa</option>
                  </select>
                </div>
              </div>

              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-12 form-group" id="trans_no_pep">
                <label>Tipo de Moneda:</label>
                <select class="form-control" id="transaccionalidad" name="transaccionalidad">
                  <option data-nivelriesgo="1"
                    <?php if(isset($gr) && $gr->transaccionalidad=="1"){ echo "selected"; } ?> value="1">
                    El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se pagará en moneda nacional</option>
                  <option data-nivelriesgo="2"
                    <?php if(isset($gr) && $gr->transaccionalidad=="2"){ echo "selected"; } ?> value="2">
                    El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se pagará en dólares de EE.UU. O bien otra divisa</option>
                </select>
              </div>
              <div class="col-md-12 form-group" id="trans_no_pep2">
                <label>Forma de Pago:</label>
                <select class="form-control" id="transaccionalidad_forma" name="transaccionalidad_forma">
                  <option data-nivelriesgo="3"
                    <?php if(isset($gr) && $gr->transaccionalidad_forma=="1"){ echo "selected"; } ?> value="1">
                    El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se pagará total o parcialmente con transferencia(s) internacional(es) y/o cheques o remesas provenientes del extranjero</option>
                  <option data-nivelriesgo="1"
                    <?php if(isset($gr) && $gr->transaccionalidad_forma=="2"){ echo "selected"; } ?> value="2">
                    El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se pagará con recursos que provienen directamente de la cuenta del cliente que solicita el trámite</option>
                  <option data-nivelriesgo="2"
                    <?php if(isset($gr) && $gr->transaccionalidad_forma=="3"){ echo "selected"; } ?> value="3">
                    El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se pagará con recursos provenientes de más de una cuenta o institución bancaria</option>
                  <option data-nivelriesgo="4" 
                    <?php if(isset($gr) && $gr->transaccionalidad_forma=="4"){ echo "selected"; } ?> value="4">El <?php if($idact==11) echo "vehículo"; else echo "servicio"; ?> se pagará total o parcialmente con recursos provenientes de un proveedor de recursos (persona diferente al cliente o solicitante)</option> <!-- se ajustó a la ultima version de calc. 21-04-22 -->
                  <?php if($idact==11) { ?>
                    <option data-nivelriesgo="4" 
                    <?php if(isset($gr) && $gr->transaccionalidad_forma=="5"){ echo "selected"; } ?> value="5">El vehículo se pagará en efectivo parcial o totalmente o  el efectivo se deposita en la cuenta de la agencia de autos </option>
                  <?php } 
                  else if($idact==12){ ?>
                    <option data-nivelriesgo="4" 
                    <?php if(isset($gr) && $gr->transaccionalidad_forma=="5"){ echo "selected"; } ?> value="5">El servicio se pagará en efectivo parcial o totalmente o  el efectivo se deposita en la cuenta de la empresa o persona que realiza el servicio de blindaje</option>
                  <?php } 
                  else if($idact==15 || $idact==16){ ?>
                    <option data-nivelriesgo="4" <?php if(isset($gr) && $gr->transaccionalidad_forma=="6"){ echo "selected"; } ?> value="6">El servicio se pagará en efectivo parcial o totalmente o  el efectivo se deposita en la cuenta de la Notaría</option>
                  <?php } ?>
                </select>
              </div>
              <?php if($idact==11) { //anexo8 vehiculos ?>
                <!--<div class="col-md-12 form-group">
                  <label>Canal de distribución sobre la agencia :</label>
                  <select class="form-control" id="c_d_agencia" name="c_d_notaria">
                    <option <?php if(isset($gr) && $gr->c_d_notaria=="1"){ echo "selected"; } ?> value="1" data-nivelriesgo="1">El cliente fue atendido en en su trámite de compra directamente por personal de la Agencia  </option>
                    <option <?php if(isset($gr) && $gr->c_d_notaria=="2"){ echo "selected"; } ?> value="2" data-nivelriesgo="3">El cliente fue atendido en su trámite de compra por algún tercero o personal que no labora directamente en la Agencia</option>
                  </select>
                </div>-->
                <div class="col-md-12 form-group">
                  <label>Canal de distribución Sobre el requerimiento :</label>
                  <select class="form-control" id="c_d_cliente_usuario" name="c_d_cliente_usuario">
                    <option <?php if(isset($gr) && $gr->c_d_cliente_usuario=="1"){ echo "selected"; } ?> value="1" data-nivelriesgocu="1">El trámite de compra es realizado directamente por el cliente y/o representante legal</option>
                    <option <?php if(isset($gr) && $gr->c_d_cliente_usuario=="2"){ echo "selected"; } ?> value="2" data-nivelriesgocu="4">El trámite de compra es realizado por un tercero diferente al cliente y/o representante legal</option> <!-- se ajustó a la ultima version de calc. 21-04-22 -->
                  </select>
                </div>
              <?php } ?>
              <?php if($idact==12) { //anexo9 blindaje ?>
                <!--<div class="col-md-12 form-group">
                  <label>Canal de distribución sobre la agencia :</label>
                  <select class="form-control" id="c_d_agencia" name="c_d_notaria">
                    <option <?php if(isset($gr) && $gr->c_d_notaria=="1"){ echo "selected"; } ?> value="1" data-nivelriesgo="1">Los clientes son atendidos en sus trámites por personal de la Entidad  </option>
                    <option <?php if(isset($gr) && $gr->c_d_notaria=="2"){ echo "selected"; } ?> value="2" data-nivelriesgo="3">La Entidad delega a algún tercero o terceriza la atención a los clientes</option>
                  </select>
                </div>-->
                <div class="col-md-12 form-group">
                  <label>Canal de distribución Sobre el requerimiento :</label>
                  <select class="form-control" id="c_d_cliente_usuario" name="c_d_cliente_usuario">
                    <option <?php if(isset($gr) && $gr->c_d_cliente_usuario=="1"){ echo "selected"; } ?> value="1" data-nivelriesgocu="1">El servicio es solicitado directamente por el cliente y/o representante legal</option>
                    <option <?php if(isset($gr) && $gr->c_d_cliente_usuario=="2"){ echo "selected"; } ?> value="2" data-nivelriesgocu="4">El servicio es solicitado por un tercero diferente al cliente y/o representante legal</option>
                  </select>
                </div>
              <?php } ?>
              <?php if($idact==15 || $idact==16) { //notarios anexo 12a y 12b ?>
                <!--<div class="col-md-12 form-group">
                  <label>Canal de distribución sobre la Notaria :</label>
                  <select class="form-control" id="c_d_notaria" name="c_d_notaria">
                    <option <?php if(isset($gr) && $gr->c_d_notaria=="1"){ echo "selected"; } ?> value="1" data-nivelriesgo="1">Los clientes son atendidos en sus trámites por personal de la Notaría</option>
                    <option <?php if(isset($gr) && $gr->c_d_notaria=="2"){ echo "selected"; } ?> value="2" data-nivelriesgo="3">La Notaría delega a algún tercero o terceriza la atención a los clientes</option>
                  </select>
                </div>-->
                <div class="col-md-12 form-group">
                  <label>Canal de distribución sobre el servicio:</label>
                  <select class="form-control" id="c_d_cliente_usuario" name="c_d_cliente_usuario">
                    <option <?php if(isset($gr) && $gr->c_d_cliente_usuario=="1"){ echo "selected"; } ?> value="1" data-nivelriesgocu="1">El servicio es solicitado directamente por el cliente y/o representante legal</option>
                    <option <?php if(isset($gr) && $gr->c_d_cliente_usuario=="2"){ echo "selected"; } ?> value="2" data-nivelriesgocu="4">El servicio es solicitado por un tercero diferente al cliente y/o representante legal</option>
                  </select>
                </div>
              <?php } ?>
            </div>
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <p class="page-description"><h3><strong>Grado riesgo de cliente:</strong></h3></p>
                    <div class="template-demo" style="height:55px;">
                      <div class="progress progress-md" id="cont_gdo1" style="height:35px;">
                       
                      </div>
                      <div class="col-md-6">
                        <span id="txt_grado"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!--<div class="col-md-2 form-group">
                <h4>Grado riesgo de cliente:</h4>
              </div>
              <div class="col-md-8 form-group" id="cont_gdo1">
                
              </div>-->
            </div>
            <button type="button" align="left" class="btn gradient_nepal2" data-toggle="modal" data-target="#modal_parametros" title="Ver Parámetros de Calulo"><i class="fa fa-eye"></i> Parámetros</button>

            <div class="row">
              <div class="col-md-12" align="right">
                <button type="button" class="btn gradient_nepal2" id="btn_submit"><i class="fa fa-arrow-right"></i> Siguiente</button>
              </div>
            </div>
          </form>
          
    

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_parametros" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Parámetros de Cálculo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <div class="row">
          <div class="col-md-12">
            <div class="grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="page-description"><strong>BAJO: <= 1.79</strong></p>
                  <div class="template-demo">
                    <div class="progress progress-md">
                      <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="page-description"><strong>MEDIO: >= 1.8 <= 2.0</strong></p>
                  <div class="template-demo">
                    <div class="progress progress-md">
                      <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
            <div class="grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="page-description"><strong>ALTO: >= 2.01</strong></p>
                  <div class="template-demo">
                    <div class="progress progress-md">
                      <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>