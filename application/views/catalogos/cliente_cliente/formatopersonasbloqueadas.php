<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<style type="text/css">
	.kv-file-upload, .kv-file-remove, .fileinput-cancel,  .btn-file{
		display: none;
	}
</style>

<div class="card">
    <div class="card-body">	
		<div class="row">
			<div class="col-md-12">    
				<input type="hidden" id="idopera" value="<?php echo $idopera ?>">
				<input type="hidden" id="idc" value="<?php echo $idc ?>">
				<input type="hidden" id="idp" value="<?php echo $idp ?>">
				<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">

				<input type="hidden" id="nombreC" value="0" readonly>
				<input type="hidden" id="fechaC" value="0" readonly>
				<input type="hidden" id="folioC" value="0" readonly>

			    <h3>Formato: Consulta del cliente en listas de personas bloqueadas.</h3>
			    <h3 id="fecha_busqueda">Fecha consulta: </h3>
			    <h3 id="folio_busqueda">Folio de búsqueda:</h3>
			 </div>
		</div> 
		<div class="row">
			<div class="col-md-12">    
			    <h5>Tipo de persona:
                <?php if($tipo_cliente==1){ ?> 
		      	Persona fisica mexicana o extranjera con condición de estancia temporal permanente.
		        <?php }else if($tipo_cliente==2){ ?>
                Persona fisica extranjera con condición de estancia de visitante.
		        <?php }else if($tipo_cliente==3){ ?>
		        Persona moral de nacionalidad mexicana, extranjera y persona moral mexicana del derecho público.
		        <?php }else if($tipo_cliente==4){ ?>
		        Persona moral mexicana del derecho público (Anexo 7 Bis A).
		        <?php }else if($tipo_cliente==5){ ?>
		        Embajadas, consulados u organismo internacionales.
		        <?php }else if($tipo_cliente==6){ ?>
                Fideicomisos.
		        <?php } ?>	
                </h5>
			    <!--Nombre o entidad buscada-->
                <?php if($tipo_cliente==6){ ?> 
	            <h5>Razón social del fiduciario: <?php echo $info->denominacion ?></h5>
		    	<?php } ?>	
		    	<?php if($tipo_cliente==3){ ?> 
	            <h5>Razón social: <?php echo $info->razon_social ?></h5>
		    	<?php } ?>	
		    	<?php if($tipo_cliente==4){ ?> 
		    	<h5>Razón social: <?php echo $info->nombre_persona ?></h5>
	            </div><br>
		    	<?php } ?>
		    	<?php if($tipo_cliente==5){ ?> 
		    	<h5>Denominación: <?php echo $info->denominacion ?></h5>
		    	<?php } ?>	
		    	<?php if($tipo_cliente==1 || $tipo_cliente==2){ ?>
			  	<h5>Nombre: <?php echo $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno ?></h5>
			  	<h5>Nombre de identificación: <?php echo $info->nombre_identificacion ?></h5>
	            <h5>Número de identificación: <?php echo $info->numero_identificacion ?></h5>
	            <?php } ?>   
	            
			 </div>
		</div> 
		<hr>
		<div class="table-responsive">
          <table class="table" id="table_opera" width="100%">
            <thead>
	            <tr>
	                <th><strong>Lista de búsqueda</strong></th>
	                <th><strong>Resultado</strong></th>
	            </tr>
            </thead>
            <tbody>
            	<tr>
            	  <td width="35%" id="lista_result"><a href="javascript:void(0)"> Ver información de busqueda </a></td>	
            	  <td width="65%" id="result_pep"></td>
            	</tr>
            </tbody>
          </table>
        </div>
				<!--
        <div class="row">
        	<div class="col-md-12" align="right">
        		<button type="button" class="btn gradient_nepal2 btnf_guardar" onclick="imprimir()"><i class="fa fa-print"></i> Imprimir consulta de persona en listas bloqueadas</button>
        	</div>
        </div>
				-->
	</div>	 
</div>	

<div class="modal fade" id="modal_detalle" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Información de busqueda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
      	<div class="table-responsive">
	        <div class="row">
	          <div class="col-md-12">
							<img id="img_detalle" width="720px">
	          </div>
						<div class="col-md-12">
							<input class="form-control" type="file" id="file_doc">
						</div>
	        </div>
	      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url(); ?>template/vendors/js/vendor.bundle.base.js"></script>
<script src="<?php echo base_url(); ?>template/fileinput/fileinput.js" type="text/javascript"></script>
<script type="text/javascript">
	base_url = $("#base_url").val();
	var band_bus=0; 

	$(document).ready(function(){

		verificaResultados();

		$("#lista_result").on("click",function(){
	    $("#modal_detalle").modal();

			if(band_bus==0){
				//$('#img_detalle').attr("src", base_url+"public/img/resultados/result_inexistente.jpg");
				$.ajax({
					type: "POST",
					url: base_url+'Clientes_cliente/generarPDF_busqueda',
					data:{
						nombreC: $('#nombreC').val(),
						fechaC: $('#fechaC').val(),
						folioC: $('#folioC').val(),
						tipoC: '0'
					},
					success: function (data){
						//console.log('PDF');
						file_doc = ''+base_url+'public/pdf/formatoBusquedaNE.pdf';
						$('#file_doc').fileinput({
							showCaption: false,
							showUpload: false,
							allowedFileExtensions: ["pdf"],
							//maxFilePreviewSize: 5000,
							initialPreview: [
									''+file_doc+'',
							],
							initialPreviewAsData: true,
							initialPreviewConfig: [
									{type: 'pdf', caption: 'No se encontraron resultados', url: file_doc},
							],
						});

					}
				});
			
			}else{
				//$('#img_detalle').attr("src", base_url+"public/img/resultados/listas_pagadas.png");

				$.ajax({
					type: "POST",
					url: base_url+'Clientes_cliente/generarPDF_busqueda',
					data:{
						nombreC: $('#nombreC').val(),
						fechaC: $('#fechaC').val(),
						folioC: $('#folioC').val(),
						tipoC: '1'
					},
					success: function (data){
						file_doc = ''+base_url+'public/pdf/formatoBusquedaSE.pdf';

						$('#file_doc').fileinput({
							showCaption: false,
							showUpload: false,
							allowedFileExtensions: ["pdf"],
							//maxFilePreviewSize: 5000,
							initialPreview: [
									''+file_doc+'',
							],
							initialPreviewAsData: true,
							initialPreviewConfig: [
									{type: 'pdf', caption: 'Listas pagadas', url: file_doc},
							],
						});
					}
				});

			}
	  });

	});

	function verificaResultados(){
		$.ajax({
	    type: "POST",
	    url: base_url+"Clientes_cliente/verificaListado",
	    data:{
	      id_operacion: $('#idopera').val(), id_perfilamiento:$("#idp").val(), id_clientec: $("#idc").val()
	    },
	    success: function (data){
	    	//console.log(data);
	    	var resultado="";
	    	if(data!="0"){
	      	var array = $.parseJSON(data);   
	      	//console.log(array);
					$('#nombreC').val(array[0].nombre +' '+ array[0].apellidos);
					$('#fechaC').val(array[0].fecha_consulta);
					$('#folioC').val(array[0].id);

	     		$('#folio_busqueda').html("Folio de búsqueda: "+array[0].id);
	      	$('#fecha_busqueda').html("Fecha consulta: "+array[0].fecha_consulta);
		    	//$.each(array[0].resultado, function(index, item) {
		    	//var resultadox='['+array[0].resultado+']';
		    	//console.log("resultadox: "+resultadox);
		    	var resultadox=array[0].resultado;
		    	if(resultadox!=""){
			    	resultadox = $.parseJSON(resultadox);
			    	resultadox = JSON.stringify(resultadox);
			    	jo = $.parseJSON(resultadox);
			    
			    	//console.log("resultadox: "+resultadox);
			    	//console.log("finalizacionCargo: "+resultadox.finalizacionCargo);
			    	jo = $.parseJSON(resultadox);
			    	//console.log("jo: "+jo.denominacion);

			    	$('#result_pep').html('');
			    	//resultadox.forEach(function(item) { //se comenta porque no accede e elementos, se agrega linea jo = $.parseJSON(resultadox); para acceder a ellos
			    	//$.each(array, function(index, item) {
			    		//console.log(item.resultado); 
			    		//var resultadox2 = item.resultado;
			    		//console.log('****************');
			    		//console.log(JSON.stringify(resultadox2));
			    		/*
			    		item.resultado.forEach(function(element) {
			    			console.log(item);
			    		});
			    		*/
			   			//console.log("finalizacionCargo: "+jo.finalizacionCargo);
			        if(jo.resultado!="" || jo.estado=="ACTIVO" || jo.estado=="INACTIVO" || jo.Estado=="ACTIVO" || jo.Estado=="INACTIVO"){
		            //resultado = $.parseJSON(item.resultado);
		            resultado = resultadox;
		            band_bus=1;
		          }
		          
		         	if (band_bus == 0){
	              $('#result_pep').html("<p><strong>Cliente no encontrado en listas de búsqueda</strong></p>");
	            }
		          
		          //console.log("Estado: "+item.estado);
		          if(jo.Message!=undefined){
		            $('#result_pep').append('<p>'+jo.Message+'</p>');
		            //console.log("mensaje: "+resultado.Message);
		          }
		          else if(jo.Status=="OK"){
		            $('#result_pep').append('<p>'+jo.Message+'</p>');
		          }
		          else if(jo.Status=="ERROR"){
		            $('#result_pep').append('<p>'+jo.Message+'</p>');
		          }
		        	else if(jo.estado=="ACTIVO" || jo.estado=="INACTIVO" || jo.Estado=="ACTIVO" || jo.Estado=="INACTIVO"){
		        		//console.log("Estado: "+item.Estado);
		        		fina_cargo="";
		        		if(jo.finalizacionCargo!=undefined){
		        			fina_cargo=jo.finalizacionCargo;
		        		}
		        		//console.log("finalizacionCargo: "+item.finalizacionCargo);
		        		if(jo.Status!=undefined){
		        			$('#result_pep').append("<p><strong>Cliente encontrado en listas de busqueda </strong></p>\
		           				<p> <strong>Denominacion: </strong>"+jo.Denominacion+"</p>\
		           				<p> <strong>Identificacion: </strong>"+jo.Identificacion+" </p>\
									    <p> <strong>Id Tributaria: </strong>"+jo.Id_Tributaria+" </p>\
									    <p> <strong>Otra_Identificacion: </strong>"+jo.Otra_Identificacion+" </p>\
									    <p> <strong>Cargo: </strong>"+jo.Cargo+" </p>\
									    <p> <strong>Lugar Trabajo: </strong>"+jo.Lugar_Trabajo+" </p>\
									    <p> <strong>Direccion: </strong>"+jo.Direccion+" </p>\
									    <p> <strong>Enlace: </strong>"+jo.Enlace+" </p>\
									    <p> <strong>Tipo: </strong>"+jo.Tipo+" </p>\
									    <p> <strong>Estado: </strong>"+jo.Estado+" </p>\
									    <p> <strong>Lista: </strong>"+jo.Lista+" </p>\
									    <p> <strong>Pais Lista: </strong>"+jo.Pais_Lista+" </p>\
									    <p> <strong>Cod Individuo: </strong>"+jo.Cod_Individuo+" </p>\
									    <p> <strong>Exactitud_Denominacion: </strong>"+jo.Exactitud_Denominacion+" </p>\
									    <p> <strong>Exactitud Identificacion: </strong>"+jo.Exactitud_Identificacion+" </p><hr>\
									");
		        		}
		        		if(jo.denominacion!=undefined){
		        			$('#result_pep').append("<p><strong>Cliente encontrado en listas de busqueda </strong></p>\
		           				<p> <strong>Denominacion: </strong>"+jo.denominacion+"</p>\
		           				<p> <strong>Identificacion: </strong>"+jo.identificacion+" </p>\
									    <p> <strong>Id Tributaria: </strong>"+jo.idTributaria+" </p>\
									    <p> <strong>Otra_Identificacion: </strong>"+jo.otraIdentificacion+" </p>\
									    <p> <strong>Cargo: </strong>"+jo.cargo+" </p>\
									    <p> <strong>Lugar Trabajo: </strong>"+jo.lugarTrabajo+" </p>\
									    <p> <strong>Direccion: </strong>"+jo.direccion+" </p>\
									    <p> <strong>Enlace: </strong>"+jo.enlace+" </p>\
									    <p> <strong>Tipo: </strong>"+jo.tipo+" </p>\
									    <p> <strong>Subtipo: </strong>"+jo.subTipo+" </p>\
									    <p> <strong>Estado: </strong>"+jo.estado+" </p>\
									    <p> <strong>Lista: </strong>"+jo.lista+" </p>\
									    <p> <strong>Pais Lista: </strong>"+jo.paisLista+" </p>\
									    <p> <strong>Cod Individuo: </strong>"+jo.codigoIndividuo+" </p>\
									    <p> <strong>Exactitud_Denominacion: </strong>"+jo.exactitudDenominacion+" </p>\
									    <p> <strong>finalización Cargo: </strong>"+jo.finalizacionCargo+" </p>\
									    <p> <strong>Exactitud Identificacion: </strong>"+jo.exactitudIdentificacion+" </p><hr>\
									");
		        		}
		        		//console.log("denominacion: "+item.denominacion);
		          		
		        	}	     	
		      	//}); 
		      }else{
		      	band_bus=0;
	    			$('#result_pep').html("<p><strong>Cliente no encontrado en listas de busqueda </strong></p>");	
		      }
				}else{
	    		band_bus=0;
	    		$('#result_pep').html("<p><strong>Cliente no encontrado en listas de busqueda </strong></p>");	      		
	    	}
	    }
	 });
	}

	function imprimir(){
		$('.btnf_guardar').css('display','none');
		window.print();
		setTimeout(function () {  $('.btnf_guardar').css('display','block'); }, 1000);
	}
</script>
