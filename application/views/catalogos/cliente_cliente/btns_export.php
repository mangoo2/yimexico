<?php 
$perfilid=$this->session->userdata('perfilid'); 
$idcliente_usuario=$this->session->userdata('idcliente_usuario');
$menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,3);
?>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <input type="hidden" id="id_cliente" value="<?php echo $this->session->userdata('idcliente'); ?>">
        <div class="row">
          <div class="col-md-8">
            <h3>Bitácoras de Expedientes</h3>
          </div>
          <div class="col-md-4" align="right">
            <a href="<?php echo base_url() ?>Estadisticas"><button type="button" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</button></a>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>
        <hr class="subtitle">
        <div class="col-md-12" align="center">
          <?php 
            if($perfilid==3){
              echo '<a title="Descargar lista de clientes" href="'.base_url().'Clientes_cliente/exportarBitacora/0"><button class="btn gradient_nepal tam_btn">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Lista de Clientes</span>
                    </div>
                  </div>  
                </button></a>';
              echo '<a title="Descargar expedientes de clientes" href="javascript:void(0)"><button class="btn gradient_nepal tam_btn down_expcli">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="fa fa-files-o btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Docs. de clientes</span>
                    </div>
                  </div>  
                </button></a>';
              echo '<a title="Descargar lista de Beneficiarios" href="'.base_url().'Clientes_c_beneficiario/exportarBitacora/0/0/0"><button class="btn gradient_nepal tam_btn" id="expedientes">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="mdi mdi-account-settings btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Lista Dueños Beneficiarios</span>
                    </div>
                  </div>  
                </button>';
                echo '<a title="Descargar expedientes de beneficiarios" href="javascript:void(0)"><button class="btn gradient_nepal tam_btn down_expdb" id="expedientes">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="fa fa-files-o btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Docs. Dueños Beneficiarios</span>
                    </div>
                  </div>  
                </button>';
            }else{
                 echo '<a title="Descargar lista de clientes" href="'.base_url().'Clientes_cliente/exportarBitacora/0"><button class="btn gradient_nepal tam_btn">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Lista de Clientes</span>
                    </div>
                  </div>  
                </button></a>';
                echo '<a title="Descargar expedientes de clientes" href="javascript:void(0)"><button class="btn gradient_nepal tam_btn down_expcli">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="fa fa-files-o btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Docs. de clientes</span>
                    </div>
                  </div>  
                </button></a>';
                echo '<a title="Descargar lista de beneficiarios" href="'.base_url().'Clientes_c_beneficiario/exportarBitacora/0/0/0"><button class="btn gradient_nepal tam_btn" id="expedientes">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="mdi mdi-account-settings btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Lista Dueños Beneficiarios</span>
                    </div>
                  </div>  
                </button>';
                echo '<a title="Descargar expedientes de beneficiarios" href="javascript:void(0)"><button class="btn gradient_nepal tam_btn down_expdb" id="expedientes">
                  <div class="row texto_centro">
                    <div class="col-md-4" align="right">
                      <i class="fa fa-files-o btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-8" align="left">
                      <span>Docs. Dueños Beneficiarios</span>
                    </div>
                  </div>  
                </button>';
            }
          ?>  

        </div><br> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_btdc" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Exportar bitácora de documentos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="">
          <span class="select_clientes"></span>

          <span class="select_beneficiario"></span>

          <div class="col-md-12 form-group">
            <label>Año de operación:</label>
            <select style="font-weight: bold;" class="form-control" id="anio_opera">
              <option value="0">Todos</option>
              <?php for($i=date("Y"); $i>=date("Y")-5; $i--) { echo '<option value="'.$i.'">'.$i.'</option>'; } ?>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_export" data-dismiss="modal" id="exportBDCli">Exportar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
