<style type="text/css">
    .form-check .form-check-label input[type="checkbox"]:checked + .input-helper:before {
	    background: #25294c;
	    border-width: 0;
    }
    .form-check .form-check-label input[type="checkbox"] + .input-helper:before {
        width: 18px;
	    height: 18px;
	    border-radius: 2px;
	    border: solid #25294c;
	    border-width: 2px;
	}
	.bg-success, .swal2-modal .swal2-buttonswrapper .swal2-styled.swal2-confirm, .settings-panel .color-tiles .tiles.success {
	    background-color: #b07f4a !important;
	}
	.progress.progress-md {
	    height: 19px;
	    border-radius: 10px;
	}
	.bg-danger, .settings-panel .color-tiles .tiles.danger {
	    background-color: #b07f4a !important;
	}
</style>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
		  <div class="card-body">
		  	<div class="row">
			  	  <input type="hidden" name="id_clientec" id="id_clientec" value="<?php echo $id_clientec ?>">
			  	  <input type="hidden" name="idperfilamiento" id="idperfilamiento" value="<?php echo $info->idperfilamiento ?>">
			  	  <input type="hidden" name="tipo_cliente" id="tipo_cliente" value="<?php echo $tipo_cliente ?>">
			      <div class="col-md-9">  
	                <?php if($tipo_cliente==1){ ?> 
			      	<h4>Persona fisica mexicana o extranjera con condición de estancia temporal permanente.</h4>
			        <?php }else if($tipo_cliente==2){ ?>
	                <h4>Persona fisica extranjera con condición de estancia de visitante.</h4>
			        <?php }else if($tipo_cliente==3){ ?>
			        <h4>Persona moral de nacionalidad mexicana, extranjera y persona moral mexicana del derecho público.</h4>
			        <?php }else if($tipo_cliente==4){ ?>
			        <h4>Persona moral mexicana del derecho público (Anexo 7 Bis A).</h4>
			        <?php }else if($tipo_cliente==5){ ?>
			        <h4>Embajadas, consulados u organismo internacionales.</h4>
			        <?php }else if($tipo_cliente==6){ ?>
	                <h4>Fideicomisos.</h4>
			        <?php } ?>	
			        <h3>Bitácora de documentos</h3>
			      </div>
			      <div class="col-md-3" align="right">
	              <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar</button>
	               </div>  
		    </div>  
		    
		    <hr class="subtitle"> 
		    <!--- -->
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
					    	<?php if($tipo_cliente==6){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Documentación o razón social del fideciario:</label>
				                <?php echo "<strong>" .$info->denominacion."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>	
					    	<?php if($tipo_cliente==3){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Razón social:</label>
				                <?php echo "<strong>" .$info->razon_social."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>	
					    	<?php if($tipo_cliente==4){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Razón social:</label>
				                <?php echo "<strong>" .$info->nombre_persona."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>
					    	<?php if($tipo_cliente==5){ ?> 
					    	<div class="row">
				              <div class="col-md-12">
				                <label>Denominación:</label>
				                <?php echo "<strong>" .$info->denominacion."</strong>"; ?>
				              </div>
				            </div><br>
					    	<?php } ?>	
					    	<?php if($tipo_cliente==1 || $tipo_cliente==2){ ?>
						  	<div class="row">
					            <div class="col-md-12">
					                <label>Nombre:</label>
					                <?php echo "<strong>" .$info->nombre."</strong>"; ?>
					            </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Apellido paterno:</label>
				                <?php echo "<strong>" .$info->apellido_paterno."</strong>"; ?>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Apellido materno:</label>
				                <?php echo "<strong>" .$info->apellido_materno."</strong>"; ?>
				              </div>
				            </div><br>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Nombre de identificación:</label>
				                <?php echo "<strong>" .$info->nombre_identificacion."</strong>"; ?>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Autoridad que emite la identificación:</label>
				                <?php echo "<strong>" .$info->autoridad_emite."</strong>"; ?>
				              </div>
				            </div>
				            <div class="row">
				              <div class="col-md-12">
				                <label>Número de Identificación:</label>
				                <?php echo "<strong>" .$info->numero_identificacion."</strong>"; ?>
				              </div>
				            </div>
				            <br>
				            <?php } ?>
				            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3){ ?>
				            <div class="row">
				              <div class="col-md-12">
				                <label>R.F.C:</label>
				                <?php if($tipo_cliente==3) echo "<strong>" .$info->r_f_c_g."</strong>"; 
				                      if($tipo_cliente==1 || $tipo_cliente==2) echo "<strong>" .$info->r_f_c_t."</strong>";
				                ?>
				              </div>
				            </div>
				            <?php } ?>
				            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
				            <div class="row">
				              <div class="col-md-12">
				                <label>CURP:</label>
				                <?php echo "<strong>" .$info->curp_t."</strong>"; ?>
				              </div>
				            </div>
                            <?php } ?>	
				            <br>
				            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==5){ ?> 
				            <div class="row">
				              <div class="col-md-12">
				                <h6>Domicilio reportado:</h6>
				              </div>
				              <div class="col-md-6">
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Tipo de vialidad:</label>
				                    <?php 
				                        $get_tipo_vial=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$info->tipo_vialidad_d);
									    $tipo_v = '';
									    foreach ($get_tipo_vial as $item_e) {
									      $tipo_v = $item_e->nombre; 
									    }
				                    echo "<strong>" .$tipo_v."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Calle:</label>
				                    <?php echo "<strong>" .$info->calle_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>No. ext:</label>
				                    <?php echo "<strong>" .$info->no_ext_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>No. int:</label>
				                    <?php echo "<strong>" .$info->no_int_d."</strong>"; ?>
				                  </div>
				                </div>
				              </div>
				              <div class="col-md-6">
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Colonia:</label>
				                    <?php echo "<strong>" .$info->colonia_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Municipio o delegación:</label>
				                    <?php echo "<strong>" .$info->municipio_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>Estado</label>
				                    <?php echo "<strong>" .$info->estado_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>C.P</label>
				                    <?php echo "<strong>" .$info->cp_d."</strong>"; ?>
				                  </div>
				                </div>
				                <div class="row">
				                  <div class="col-md-12">
				                    <label>País</label>
				                    <?php echo "<strong>" .$info->pais_d."</strong>"; ?>
				                  </div>
				                </div>
				              </div>
				            </div>
                            <?php } ?>	
				            <br>
				            <?php $dis=""; if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
				            <div class="row">
				            	<?php $checked=""; if($tipo_cliente==1 && $info->presente_legal!="" || $tipo_cliente==3 && $info->nombre_g!="" || $tipo_cliente==5 && $info->nombre_g!=""
				            	 || $tipo_cliente==6 && $info->nombre_g!=""){ 
				            		$checked="checked"; 
				            		//echo "....".$checked;
				            	}
				            	if($checked=="") $dis="style='display:none;'";
				            	?>
				               <div class="col-md-12 form-group" <?php echo $dis; ?>>
				               	  <div class="form-check form-check-purple">
					                <label class="form-check-label">Presentó apoderado legal
					                	<?php
					                		if($tipo_cliente==3 && $info->nombre_g!="" || $tipo_cliente==5 && $info->nombre_g!="" ||
					                		$tipo_cliente==6 && $info->nombre_g!="") $checked="checked";
					                	?>
					                  <input disabled type="checkbox" class="form-check-input" id="Ch1" value="" <?php echo $checked; ?>>
					                </label>
					              </div>
				               </div>	
				            </div>
                            <?php } ?>	
                            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || /*$tipo_cliente==4 ||*/ $tipo_cliente==5 || $tipo_cliente==6){ ?> 
				            <hr class="subtitle">
				            <div class="row" <?php echo $dis; ?>>
				            	<div class="col-md-12">
				            		<h5>Apoderado legal <?php if($tipo_cliente==5 || $tipo_cliente==6){?> / Servicio público<?php } ?>	</h5>
				            	</div>
				            </div>
                            <?php } ?>	
                            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || /*$tipo_cliente==4 ||*/ $tipo_cliente==5  || $tipo_cliente==6){ ?> 
				            <div class="row" <?php echo $dis; ?>>
					            <div class="col-md-12">
					                <label>Nombre:</label>
					                <?php if($tipo_cliente==1) echo "<strong>" .$info->nombre_p."</strong>"; 
					                	if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->nombre_g."</strong>";
					                ?>
					            </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Apellido paterno:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->apellido_paterno_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->apellido_paterno_g."</strong>";
					            ?>
				              </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Apellido materno:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->apellido_materno_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->apellido_materno_g."</strong>";
					            ?>
				              </div>
				            </div><br>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Nombre de identificación:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->nombre_identificacion_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->nombre_identificacion_g."</strong>";
					            ?>
				              </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Autoridad que emite la identificación:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->autoridad_emite_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->autoridad_emite_g."</strong>";
					            ?>
				              </div>
				            </div>
				            <div class="row" <?php echo $dis; ?>>
				              <div class="col-md-12">
				                <label>Número de Identificación:</label>
				                <?php if($tipo_cliente==1) echo "<strong>" .$info->numero_identificacion_p."</strong>"; 
					                if($tipo_cliente==3 || $tipo_cliente==5 || $tipo_cliente==6) echo "<strong>" .$info->numero_identificacion_g."</strong>";
					            ?>
				              </div>
				            </div><br>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?>
					            <div class="row" <?php echo $dis; ?>>
					              <div class="col-md-12">
					                <h6>Domicilio reportado:</h6>
					              </div>
					              <div class="col-md-6">
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Tipo de calle:</label>
					                    <?php 
					                    $get_tipo_vial=$this->ModeloCatalogos->getselectwhere('tipo_vialidad','id',$info->tipo_vialidad_t);
									    $tipo_v = '';
									    foreach ($get_tipo_vial as $item_e) {
									      $tipo_v = $item_e->nombre; 
									    }
					                    echo "<strong>" .$tipo_v."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Calle:</label>
					                    <?php echo "<strong>" .$info->calle_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>No. ext:</label>
					                    <?php echo "<strong>" .$info->no_ext_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>No. int:</label>
					                    <?php echo "<strong>" .$info->no_int_t."</strong>"; ?>
					                  </div>
					                </div>
					              </div>
					              <div class="col-md-6">
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Colonia:</label>
					                    <?php echo "<strong>" .$info->colonia_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Municipio o delegación:</label>
					                    <?php echo "<strong>" .$info->municipio_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>Estado</label>
					                    <?php echo "<strong>" .$info->estado_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>C.P</label>
					                    <?php echo "<strong>" .$info->cp_t."</strong>"; ?>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-md-12">
					                    <label>País</label>
					                    <?php echo "<strong>" .$info->pais_d."</strong>"; ?>
					                  </div>
					                </div>
					              </div>
					            </div>
	                            <?php } ?>
                            <?php } ?>	
						</div>
					</div>	 
					<br>
		        </div>	

	        	<div class="col-md-6">
	        		<form id="form_docs_cliente" class="form">
	        			<?php if(isset($docs)) {?>
				            <input type="hidden" name="id" id="id" value="<?php echo $docs->id ?>">
				          <?php } else { ?>
				          	<input type="hidden" name="id" id="id" value="0">
				          <?php }?>
		        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
						    <div class="card-body">
						    <!-- -->
	                           <div class="row">
					              <div class="col-md-12">
					                <h5>Histórico de documentos capturados sobre el cliente</h5>
					                <div class="col-md-12" align="right">
					                	<a href="<?php echo base_url().'Clientes_cliente/exportarBitacoraDocs/'.$info->idperfilamiento.'' ?>" class="btn gradient_nepal2"><i class="fa fa-file-excel-o"></i> Exportar Documentos</a>
						                <a href="<?php echo base_url().'Clientes_cliente/exportarBitacora/'.$info->idperfilamiento.'' ?>" class="btn gradient_nepal2"><i class="fa fa-file-excel-o"></i> Exportar Lista</a>
						            </div>
					              </div>
					            </div> 
					            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont2">
					            	<div class="form-check form-check-purple">
					                	<h3 class="form-check-label">Formato "Conoce a tu cliente"</h3>
					             	</div>
					             	<div id="headingCollapse1"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse1" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse" aria-expanded="false">
					                <?php $i=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="formato_cc"){
					              			$i++;
					              			$img = base_url()."uploads/formato_cc/".$b->nombre_doc;
						               		$imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/formato_cc/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i."}";
						                	$ext = explode('.',$b->nombre_doc);
							                if($ext[1]!="pdf"){
							                    $imgp = "<img src='".base_url()."uploads/formato_cc/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
							                    $typePDF = "false";  
							                }else{
							                    $imgp = "".base_url()."uploads/formato_cc/".$b->nombre_doc." ";
							                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/formato_cc/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i."}";
							                    $typePDF = "true";
							                } 
						              	?>

						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgcc<?php echo $i;?>" type="file">

							              </div>
							              <div class="col-md-10"></div>
							           	</div>

							       		<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcc'.$i.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							    	} 
							    } ?>
							    </div>
						    	<?php if($i==0){
					       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
						                	SIN DOCUMENTOS CARGADOS
					           		</div>';
				           		}
				           		?>
						        </div>
					            <?php } ?> <hr>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
					            <div class="col-md-12 form-group" id="cont3">
					                <div class="form-check form-check-purple">
					                	<h3 class="form-check-label">Identificación oficial:</h3>
					             	</div>
					             	<div id="headingCollapse2"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse2" role="tabpanel" aria-labelledby="headingCollapse2" class="collapse" aria-expanded="false">
					             	<?php $i2=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="ine"){
					              			$i2++;
					              		$img = base_url()."uploads/ine/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/ine/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i2."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/ine/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/ine/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/ine/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i2."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgio<?php echo $i2;?>" type="file">

							              </div>
							              <div class="col-md-10"></div>
							           	</div>

							       		<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgio'.$i2.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?> 
							   		</div>
							    	<?php if($i2==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}
					           		?>
					            </div>
	                            <?php } ?> <hr>
					            <?php if($tipo_cliente==1 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont4">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Cédula de identificación fiscal:</h3>
					              </div>
					              <div id="headingCollapse3"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse3" aria-expanded="false" aria-controls="collapse3" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse3" role="tabpanel" aria-labelledby="headingCollapse3" class="collapse" aria-expanded="false">
					              <?php $i3=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="cif"){
					              			$i3++;
					              		$img = base_url()."uploads/cif/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/cif/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i3."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/cif/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/cif/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/cif/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i3."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							             	<div class="col-md-6">
							             		<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                	<input width="50px" height="80px" class="img" id="imgcif<?php echo $i3;?>" type="file">
							              	</div>
							              	<div class="col-md-10"></div>
							           	</div>
							           <?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcif'.$i3.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
							    	} ?>
							    	</div>
							    	<?php if($i3==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    ?>
					            </div>
					            <?php } ?> <hr>
	                            <?php if($tipo_cliente==1){ ?> 
					            <div class="col-md-12 form-group" id="cont9">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">CURP:</h3>
					              </div>
					              <div id="headingCollapse4"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapse4" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse4" role="tabpanel" aria-labelledby="headingCollapse4" class="collapse" aria-expanded="false">
					              <?php $i4=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="curp"){
					              			$i4++;
					              		$img = base_url()."uploads/curp/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/curp/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i4."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/curp/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/curp/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/curp/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i4."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              <div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgcurp<?php echo $i4;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           </div>
							           <?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcurp'.$i4.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i4==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
							    ?>
					            </div>
					            <?php } ?> <hr>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5){ ?> 
					            <div class="col-md-12 form-group" id="cont10">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Comprobante de domicilio:</h3>
					              </div>
					              <div id="headingCollapse5"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse5" aria-expanded="false" aria-controls="collapse5" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse5" role="tabpanel" aria-labelledby="headingCollapse5" class="collapse" aria-expanded="false">
					              <?php $i5=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="comprobante_dom"){
					              			$i5++;
					              		$img = base_url()."uploads/compro_dom/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/compro_dom/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i5."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/compro_dom/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/compro_dom/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/compro_dom/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i5."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgcd<?php echo $i5;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcd'.$i5.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i5==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
					           	?>
					            </div>
	                            <?php } ?> <hr>
	                            <?php if(/*$tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3  || $tipo_cliente==5*/ $tipo_cliente==8){ ?> 
					            <div class="col-md-12 form-group" id="cont11" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Formato consultas de dueño beneficiario:</h3>
					              </div>
					              <div id="headingCollapse6"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse6" aria-expanded="false" aria-controls="collapse6" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse6" role="tabpanel" aria-labelledby="headingCollapse6" class="collapse" aria-expanded="false">
					             	<?php $i6=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="formato_duen_bene"){
					              			$i6++;
					              		$img = base_url()."uploads/compro_due_ben/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/compro_due_ben/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i6."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/compro_due_ben/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/compro_due_ben/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/compro_due_ben/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i6."}";
						                    $typePDF = "true";
						                } 
						              	?>
							            <div class="row row_file">
								            <div class="col-md-6">
								            	<h3>Fecha: <?php echo $b->fecha; ?></h3>
								                <input width="50px" height="80px" class="img" id="imgcdb<?php echo $i6;?>" type="file">
								            </div>
								            <div class="col-md-10"></div>
								        </div>
								        <?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcdb'.$i6.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i6==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
					           	?>
					            </div>
	                            <?php } ?>
	                            <?php if($tipo_cliente==2){ ?> 
	                            <hr class="subtitle">
	                            <h5>Documento del INM</h5>
	                            <div class="col-md-12 form-group" id="cont6">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Constancia de estancia legal en el país:</h3>
					              </div>
					              <div id="headingCollapse7"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse7" role="tabpanel" aria-labelledby="headingCollapse7" class="collapse" aria-expanded="false">
					              <?php $i7=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="const_est_legal"){
					              			$i7++;
					              		$img = base_url()."uploads/constancia_legal/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/constancia_legal/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i7."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/constancia_legal/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/constancia_legal/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/constancia_legal/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i7."}";
						                    $typePDF = "true";
						                } 
						              	?>
							            <div class="row row_file">
								            <div class="col-md-6">
								            	<h3>Fecha: <?php echo $b->fecha; ?></h3>
								                <input width="50px" height="80px" class="img" id="cons_elp<?php echo $i7;?>" type="file">
								            </div>
								            <div class="col-md-10"></div>
								        </div>
								        <?php 
							       		echo '<script type="text/javascript">
							       		$("#cons_elp'.$i7.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i7==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
					           	?>
					            </div>
					            <hr class="subtitle">
	                            <?php } ?>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
					            <div class="col-md-12 form-group" id="cont6" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Carta poder:</h3>
					              </div>
					              <div id="headingCollapse8"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse8" aria-expanded="false" aria-controls="collapse8" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse8" role="tabpanel" aria-labelledby="headingCollapse8" class="collapse" aria-expanded="false">
					              <?php $i8=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="carta_poder"){
					              			$i8++;
					              		$img = base_url()."uploads/carta_poder/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/carta_poder/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i8."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/carta_poder/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/carta_poder/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/carta_poder/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i8."}";
						                    $typePDF = "true";
						                } 
						              	?>
						             	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="carta_poder<?php echo $i8;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#carta_poder'.$i8.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i8==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
					           	?>
					            </div>
					            <?php } ?>
					            <?php if($tipo_cliente==4){ ?> 
	                            <hr class="subtitle">
	                            <h5>Comprobación de facultades del servicio público</h5>
	                            <div class="col-md-12 form-group" id="cont6">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Constancia de comprobación de facultades del servidor publico:</h3>
					              </div>
					              <div id="headingCollapse9" class="card-header">
				                      <a data-toggle="collapse" href="#collapse9" aria-expanded="false" aria-controls="collapse9" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse9" role="tabpanel" aria-labelledby="headingCollapse9" class="collapse" aria-expanded="false">
					                <?php $i9=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="const_est_legal2"){
					              			$i9++;
					              		$img = base_url()."uploads/constancia_legal/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/constancia_legal/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i9."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/constancia_legal/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/constancia_legal/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/constancia_legal/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i9."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="cons_elp2<?php echo $i9;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							            </div>
							            <?php 
							       		echo '<script type="text/javascript">
							       		$("#cons_elp2'.$i9.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i9==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
					           	?>
					            </div>
					            <hr class="subtitle">
	                            <?php } ?>
	                            <?php if($tipo_cliente==1 || $tipo_cliente==2){ ?> 
					            <div class="col-md-12 form-group" id="cont8" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Comprobante de domicilio de apoderado legal:</h3>
					              </div>
					              <div id="headingCollapse10"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse10" aria-expanded="false" aria-controls="collapse10" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse10" role="tabpanel" aria-labelledby="headingCollapse10" class="collapse" aria-expanded="false">
					              <?php $i10=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="comprobante_dom_apod"){
					              			$i10++;
					              		$img = base_url()."uploads/compro_dom/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/compro_dom/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i10."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/compro_dom/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/compro_dom/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/compro_dom/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i10."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgcdal<?php echo $i10;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgcdal'.$i10.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i10==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
					           	?>
					            </div>
					            <?php } ?> <hr>
					            <?php if($tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont12">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Escritura constitutiva o instrumento público que acredite su existencia:
					                </h3>
					              </div>
					              <div id="headingCollapse11"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse11" aria-expanded="false" aria-controls="collapse11" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse11" role="tabpanel" aria-labelledby="headingCollapse11" class="collapse" aria-expanded="false">
					              	<?php $i11=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="esc_ints"){
					              			$i11++;
					              		$img = base_url()."uploads/escrituras/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/escrituras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i11."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/escrituras/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/escrituras/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/escrituras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i11."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="esc_ints<?php echo $i11;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
						           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#esc_ints'.$i11.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i11==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}	
					           	?>
					            </div>
					            <?php } ?> <hr>
					            <?php if($tipo_cliente==3){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Poderes Notariales o documento para comprobar las facultades del(os) servidor(es) público(s):
					                </h3>
					              </div>
					              <div id="headingCollapse12"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse12" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" aria-expanded="false">
					              	<?php $i12=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="poder_not_fac_serv"){
					              			$i12++;
					              		$img = base_url()."uploads/poder_not/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/poder_not/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i12."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/poder_not/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/poder_not/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/poder_not/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i12."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="poder_not_fac_serv<?php echo $i12;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#poder_not_fac_serv'.$i12.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i12==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}
					           	?>
					            </div>
					            <?php } ?> <hr>
					            <?php if($tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Poderes Notariales: </h3>
					              </div>
					              <div id="headingCollapse13"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse13" aria-expanded="false" aria-controls="collapse13" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse13" role="tabpanel" aria-labelledby="headingCollapse13" class="collapse" aria-expanded="false">
					              	<?php $i13=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="poder_not"){
					              			$i13++;
					              		$img = base_url()."uploads/poder_not/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/poder_not/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i13."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/poder_not/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/poder_not/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/poder_not/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i13."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="poder_not<?php echo $i13;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#poder_not'.$i13.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i13==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}
					           	?>
					            </div>
					            <?php } ?> <hr>
					            <?php if($tipo_cliente==1 || $tipo_cliente==2 || $tipo_cliente==3 || $tipo_cliente==4 || $tipo_cliente==5 || $tipo_cliente==6){ ?> 
					            <div class="col-md-12 form-group" id="cont7" <?php echo $dis; ?>>
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Identificación oficial de apoderado legal:</h3>
					              </div>
					              <div id="headingCollapse14"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse14" aria-expanded="false" aria-controls="collapse14" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse14" role="tabpanel" aria-labelledby="headingCollapse14" class="collapse" aria-expanded="false">
					              	<?php $i14=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="ine_apo"){
					              			$i14++;
					              		$img = base_url()."uploads/ine/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/ine/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i14."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/ine/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/ine/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/ine/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i14."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgineal<?php echo $i14;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgineal'.$i14.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i14==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}
					           	?>
					            </div>
	                            <?php } ?> <hr>
	                            <?php if($tipo_cliente==5){ ?> 
					            <div class="col-md-12 form-group" id="cont6">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Documento que acredite su legal existencia: 
					                  <input type="checkbox" class="form-check-input" id="Chimg16" value="">
					                </label>
					              </div>
					              <div id="headingCollapse15" class="card-header">
				                      <a data-toggle="collapse" href="#collapse15" aria-expanded="false" aria-controls="collapse15" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse15" role="tabpanel" aria-labelledby="headingCollapse15" class="collapse" aria-expanded="false">
					              	<?php $i15=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="const_est_legal3"){
					              			$i15++;
					              		$img = base_url()."uploads/constancia_legal/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/constancia_legal/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i15."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/constancia_legal/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/constancia_legal/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/constancia_legal/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i15."}";
						                    $typePDF = "true";
						                } 
						              	?>
						              	<div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="imgacre_legal<?php echo $i15;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							           	</div>
							           	<?php 
							       		echo '<script type="text/javascript">
							       		$("#imgacre_legal'.$i15.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						    		</div>
						    		<?php if($i15==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}
					           	?>
					            </div> <hr>
					            <div class="col-md-12 form-group" id="cont7">
					              <div class="form-check form-check-purple">
					                <h3 class="form-check-label">Poderes notariales:</h3>
					              </div>
					              <div id="headingCollapse16"  class="card-header">
				                      <a data-toggle="collapse" href="#collapse16" aria-expanded="false" aria-controls="collapse16" class="card-title lead collapsed"><i class="fa fa-arrow-down"></i> Ver Documentos</a>
				                    </div>
				                    <div id="collapse16" role="tabpanel" aria-labelledby="headingCollapse16" class="collapse" aria-expanded="false">
					              	<?php $i16=0; foreach ($bitacora as $b) {
					              		if($b->tipo_doc=="poder_not2"){
					              			$i16++;
					              		$img = base_url()."uploads/poder_not/".$b->nombre_doc;
						                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/poder_not/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i16."}";
						                $ext = explode('.',$b->nombre_doc);
						                if($ext[1]!="pdf"){
						                    $imgp = "<img src='".base_url()."uploads/poder_not/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
						                    $typePDF = "false";  
						                }else{
						                    $imgp = "".base_url()."uploads/poder_not/".$b->nombre_doc." ";
						                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/poder_not/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$i16."}";
						                    $typePDF = "true";
						                } 
						              	?>
						                <div class="row row_file">
							              <div class="col-md-6">
							              	<h3>Fecha: <?php echo $b->fecha; ?></h3>
							                <input width="50px" height="80px" class="img" id="poder_not2<?php echo $i16;?>" type="file">
							              </div>
							              <div class="col-md-10"></div>
							            </div>
							            <?php 
							       		echo '<script type="text/javascript">
							       		$("#poder_not2'.$i16.'").fileinput({
					                        language: "es",
										    overwriteInitial: false,
										    showUpload: false,
										    showClose: false,
										    showCaption: false,
										    showUploadedThumbs: false,
										    showBrowse: false,
										    showRemove: false,
										    showDelete: false,
										    fileActionSettings: {
										      showRemove: false,
										      showDelete: false,
										      showUpload: false,
										      showBrowse: false,
										      showZoom: true,
										      showDrag: false,
										    },
					                        removeTitle: "Cancel or reset changes",
					                        elErrorContainer: "#kv-avatar-errors-1",
					                        msgErrorClass: "alert alert-block alert-danger",
					                        defaultPreviewContent: "'.$img.'",
					                        layoutTemplates: {main2: "{preview} "},
					                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
					                        initialPreview: [
					                        "'.$imgp.'"
					                        ],
					                        initialPreviewAsData: '.$typePDF.',
					                        initialPreviewFileType: "image",
					                        initialPreviewConfig: [
					                            '.$imgdet.'
					                        ]
					                    });
									    </script>';
							       		}
						       		} ?>
						       		</div>
						    		<?php if($i16==0){
						       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
							                	SIN DOCUMENTOS CARGADOS
						           		</div>';
					           		}
					           	?>
					            </div>
					            <?php } ?>
	                        <!-- -->
	                        	<hr>
					          	<div id="headingCollapse12" class="card-header">
			                      <a data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12" class="card-title lead collapsed" id="mas_docs_cli"><i class="fa fa-arrow-down"></i> Mas Documentos</a>
			                    </div>
			                    <div id="collapse12" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" aria-expanded="false">
			                    <div class="card-body">
			                    	<form id="form_docs_extra" class="form" method="post" role="form">
				                        <div class="card-block">
				                        	<?php if(isset($docs_ext)) {?>
									            <input type="hidden" id="id_extra" value="<?php echo $docs_ext->id ?>">
									        <?php } else { ?>
									          	<input type="hidden" id="id_extra" value="0">
									        <?php }?>

				                          	<div class="row">
				                                <div class="col-md-12 form-group">
									              	<div class="form-check form-check-purple">
									                	<h3 class="form-check-label">Documentos extras:</h3>
									              	</div>
									              	<div class="row">
									              		<?php $ex=0; foreach ($bitacora as $b) {
										              		if($b->tipo_doc=="doc extra"){
										              			$ex++;
										              		$img = base_url()."uploads/extras/".$b->nombre_doc;
											                $imgdet = "{type: 'image', downloadUrl: '".base_url()."uploads/extras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$ex."}";
											                $ext = explode('.',$b->nombre_doc);
											                if($ext[1]!="pdf"){
											                    $imgp = "<img src='".base_url()."uploads/extras/".$b->nombre_doc."' class='kv-preview-data file-preview-image' width='80px'>"; 
											                    $typePDF = "false";  
											                }else{
											                    $imgp = "".base_url()."uploads/extras/".$b->nombre_doc." ";
											                    $imgdet = "{type: 'pdf', downloadUrl: '".base_url()."uploads/extras/".$b->nombre_doc."', caption: '".$b->nombre_doc."', key:".$ex."}";
											                    $typePDF = "true";
											                } 
											              	?>
											              	<div class="row row_file">
												             	<div class="col-md-12">
												             		<h3>Fecha: <?php echo $b->fecha; ?></h3>
												                	<input width="50px" height="80px" class="imgExt" id="extra<?php echo $ex;?>" type="file">
												              	</div>
												              	<div class="col-md-10"></div>
												           </div>
												           <?php 
												       		echo '<script type="text/javascript">
												       		$("#extra'.$ex.'").fileinput({
										                        language: "es",
															    overwriteInitial: false,
															    showUpload: false,
															    showClose: false,
															    showCaption: false,
															    showUploadedThumbs: false,
															    showBrowse: false,
															    showRemove: false,
															    showDelete: false,
															    fileActionSettings: {
															      showRemove: false,
															      showDelete: false,
															      showUpload: false,
															      showBrowse: false,
															      showZoom: true,
															      showDrag: false,
															    },
										                        removeTitle: "Cancel or reset changes",
										                        elErrorContainer: "#kv-avatar-errors-1",
										                        msgErrorClass: "alert alert-block alert-danger",
										                        defaultPreviewContent: "'.$img.'",
										                        layoutTemplates: {main2: "{preview} "},
										                        allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "pdf"],
										                        initialPreview: [
										                        "'.$imgp.'"
										                        ],
										                        initialPreviewAsData: '.$typePDF.',
										                        initialPreviewFileType: "image",
										                        initialPreviewConfig: [
										                            '.$imgdet.'
										                        ]
										                    });
														    </script>';
												       		}
											       		}
											    		if($ex==0){
											       			echo '<div class="row row_file" style="padding: 3px 10px; border: PowderBlue 5px solid; border-radius: 20px; height: 60px;">
												                	SIN DOCUMENTOS CARGADOS
											           		</div>';
										           		}
										           	?>
											       </div><br>
									            </div>
				                          	</div>
				                        </div>
			                    	</form>
			                      </div>
			                    </div>

						    </div>
						</div>
					</form>    	
	        	</div>

	        </div>
            <!-- -->
	    </div>
	</div>
</div>