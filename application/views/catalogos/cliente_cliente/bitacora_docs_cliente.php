<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=bitacora".date('Y-m-d').".xls");	
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla_reporte_dir" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>#</th>
          <th>Cliente</th>
          <th>Tipo de Documento</th>
          <th>Nombre de Documento</th>
          <th>Fecha de Registro</th>
        </tr>
    </thead>
    <tbody>
    	<?php
        $nombre="";
        foreach ($b as $key) {
          $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$key->id_perfilamiento));
          foreach ($get_pp as $g) {
            //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
            $tipoccon = $g->idtipo_cliente;
            //echo "<br>tipoccon: ".$tipoccon;
            if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
            if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
            if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
            if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
            if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
            if($tipoccon==6) $tabla = "tipo_cliente_f";
            $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
            foreach ($get_per as $g2) {
              if($tipoccon==1 || $tipoccon==2) $nombre_cli = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
              if($tipoccon==3) $nombre_cli = $g2->razon_social;
              if($tipoccon==4) $nombre_cli = $g2->nombre_persona;
              if($tipoccon==5 || $tipoccon==6) $nombre_cli = $g2->denominacion;
            }
          }

          if($key->tipo_doc=="formato_cc"){
            $nombre="Formato conoce a tu cliente";
          }
          if($key->tipo_doc=="ine"){
            $nombre="Identificación oficial";
          }
          if($key->tipo_doc=="cif"){
            $nombre="Cédula de identificación fiscal";
          }
          if($key->tipo_doc=="curp"){
            $nombre="CURP";
          }
          if($key->tipo_doc=="comprobante_dom"){
            $nombre="Comprobante de domicilio";
          }
          if($key->tipo_doc=="formato_duen_bene"){
            $nombre="Formato consultas de dueño beneficiario";
          }
          if($key->tipo_doc=="const_est_legal"){
            $nombre="Constancia de estancia legal en el país";
          }
          if($key->tipo_doc=="carta_poder"){
            $nombre="Carta poder";
          }
          if($key->tipo_doc=="const_est_legal2"){
            $nombre="Constancia de comprobación de facultades del servidor publico";
          }
          if($key->tipo_doc=="comprobante_dom_apod"){
            $nombre="Comprobante de domicilio de apoderado legal";
          }
          if($key->tipo_doc=="esc_ints"){
            $nombre="Escritura constitutiva o instrumento público que acredite su existencia";
          }
          if($key->tipo_doc=="poder_not_fac_serv"){
            $nombre="Poderes Notariales o documento para comprobar las facultades del(os) servidor(es) público(s)";
          }
          if($key->tipo_doc=="poder_not"){
            $nombre="Poderes Notariales";
          }
          if($key->tipo_doc=="ine_apo"){
            $nombre="Identificación oficial de apoderado legal";
          }
          if($key->tipo_doc=="const_est_legal3"){
            $nombre="Documento que acredite su legal existencia";
          }
          if($key->tipo_doc=="poder_not2"){
            $nombre="Poderes notariales";
          }
          if($key->tipo_doc=="doc extra"){
            $nombre="Documento extra";
          }
        	echo '
            <tr>
                <td >'.$key->id.'</td>
                <td >'.$nombre_cli.'</td>
                <td >'.$nombre.'</td>
                <td >'.$key->nombre_doc.'</td>
                <td >'.$key->fecha.'</td>';
          }
        	echo '</tr>';
        ?>
        
    </tbody>
</table>