<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=clientes_benes".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>#</th>
          <th>Cliente</th>
          <th>Ultima Operación</th>
          <th>Tipo de cliente</th>
          <th>Beneficario(s)</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
        $id="";
        $nombre="";
        $tipo="";
        foreach ($c as $k) {
          if($k->idtipo_cliente==1){
            $id=$k->idtipo_cliente_p_f_m;
            $nombre=$k->nombre.' '.$k->apellido_paterno.' '.$k->apellido_materno;
            $tipo="Persona física mexicana o extranjera con condición de estancia temporal o permanente";
          }else if($k->idtipo_cliente==2){
            $id=$k->idtipo_cliente_p_f_e;
            $nombre=$k->nombre2.' '.$k->apellido_paterno2.' '.$k->apellido_materno2;
            $tipo="Persona física extranjera con condición de estancia de visitante";
          }else if($k->idtipo_cliente==3){
            $id=$k->idtipo_cliente_p_m_m_e;
            $nombre=$k->razon_social;
            $tipo="Persona moral mexicana y extranjera, persona moral de derecho público";
          }else if($k->idtipo_cliente==4){
            $id=$k->idtipo_cliente_p_m_m_d;
            $nombre=$k->nombre_persona;
            $tipo="Persona moral mexicana de derecho público detallados en el anexo 7 bis A";
          }else if($k->idtipo_cliente==5){
            $id=$k->idtipo_cliente_e_c_o_i;
            $nombre=$k->denominacion2;
            $tipo="Embajadas, consulados u organismos internacionales";
          }else if($k->idtipo_cliente==6){
            $id=$k->idtipo_cliente_f;
            $nombre=$k->denominacion;
            $tipo="Fideicomisos";
          }
           echo '
            <tr>
                <td >'.$id.'</td>
                <td >'.$nombre.'</td>
                <td >'.$k->ultimo_reg.'</td>
                <td >'.$tipo.'</td>';
            /* BENEFICIARIOS */
            $i=0; $html=""; $prehtml="";
            $data = $this->ModeloCliente->get_benef_control_f('beneficiario_fisica',array('idtipo_cliente'=>$k->idperfilamientop,'activo'=>1,"id_union"=>0));
            foreach ($data as $key) { //para pintar las personas fisicas
                $i++;
                echo '<td>
                         '.$key->nombre.' '.$key->apellido_paterno.' '.$key->apellido_materno.'
                     </td>';
            }

            $datam = $this->ModeloCliente->get_benef_control_mf($k->idperfilamientop,'beneficiario_moral_moral');
            foreach ($datam as $key) { //para pintar las personas morales_fisicas
                $i++;
                echo '<td>
                            '.$key->razon_social.'
                        </td>
                    ';
            }
            $datam = $this->ModeloCliente->get_benef_control_mf($k->idperfilamientop,'beneficiario_fideicomiso');
            foreach ($datam as $key) { //para pintar fideicomiso
                $i++;
                echo '
                    <td>
                        '.$key->razon_social.'
                    </td>';
            }
            if($i==0){
                echo "<td></td>";
            }
            /* ******************************** */
        
        }
        echo '</tr>';
        /* *****UNIONES BENES********** */

        $i2=0; $html2=""; $pre="";
        
        foreach ($cu as $k2) {
            //echo "id_union: ".$k2->id_union;
            $html_bf=""; $html_bm=""; $html_bfd="";
            $get_bene_union=$this->ModeloCatalogos->getselectwherestatus2('*','beneficiario_union_cliente',array('id_union'=>$k2->id_union,"activo"=>1),'tipo_bene');
            foreach ($get_bene_union as $k) {
                $id_union_aux=$k->id_union;
                $id_duenio_bene_aux=$k->id_duenio_bene;

                if($k->tipo_bene==1){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
                    $tipo_bene_aux=$k->tipo_bene;
                    $id_bene_aux=$k->id_duenio_bene;
                    $data = $this->ModeloCliente->get_benef_control_f2('beneficiario_fisica',array("id_union"=>$k->id_union,"activo"=>1));
                    foreach ($data as $key) { //para pintar las personas fisicas
                        $i2++;
                        $html_bf .= '
                        <td>
                          '.$key->nombre.' '.$key->apellido_paterno.' '.$key->apellido_materno.'
                        </td>
                      ';
                    }
                }
                if($k->tipo_bene==2){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
                    $tipo_bene_aux=$k->tipo_bene;
                    $id_bene_aux=$k->id_duenio_bene;
                    $datam = $this->ModeloCliente->get_benef_control_mfUnion($k->id_union,'beneficiario_moral_moral','beneficiario_moral_moral.id AS id_auni');
                    foreach ($datam as $key) { //para pintar las personas morales_fisicas
                        $i2++;
                        $html_bm .= '
                        <td>
                          '.$key->razon_social.'
                        </td>';
                    }
                }
                if($k->tipo_bene==3){// && $id_bene_aux!=$id_duenio_bene_aux && $tipo_bene_aux!=$k->tipo_bene){
                    $tipo_bene_aux=$k->tipo_bene;
                    $id_bene_aux=$k->id_duenio_bene;
                    $datam = $this->ModeloCliente->get_benef_control_mfUnion($k->id_union,'beneficiario_fideicomiso','beneficiario_fideicomiso.id AS id_auni');
                    foreach ($datam as $key) { //para pintar fideicomiso
                        $i2++;
                        $html_bfd .= '
                        <td>
                          '.$key->razon_social.'
                        </td>';
                    }
                }
                $tipo_bene_aux=$k->tipo_bene;
                $id_bene_aux=$k->id_duenio_bene;
            }

            echo '
            <tr>
                <td>'.$k2->id_union.'</td>
                <td>'.$k2->cliente.'</td>
                <td>'.$k2->ultimo_reg.'</td>
                <td>Mancomunado</td>
                '.$html_bf.''.$html_bm.''.''.$html_bfd.'';
            if($i2==0 || $html_bf=="" && $html_bm=="" && $html_bfd==""){
                echo "<td></td>";
            }
        }
        echo '</tr>';
        ?>
        
    </tbody>
</table>