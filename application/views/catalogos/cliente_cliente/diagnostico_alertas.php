<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css"><style type="text/css">
  .check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c; -webkit-print-color-adjust: exact;
  }
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
      background: #212b4c; -webkit-print-color-adjust: exact;
  }
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
      border-color: #212b4c; -webkit-print-color-adjust: exact;
  }
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
      background: #25294c; -webkit-print-color-adjust: exact;
  }
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
      border-color: #25294c; -webkit-print-color-adjust: exact;
  }
</style>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 >Diagnóstico de alertas - <?php echo $nombre; ?></h3>
        <hr class="subtitle">
        <!---------------->
        <form class="form" method="post" role="form" id="form_formulario_pep">
          <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
          <input type="hidden" name="id_operacion" id="idopera" value="<?php echo $idopera ?>">
          <input type="hidden" id="id_actividad" value="<?php echo $id_actividad ?>">
          <input type="hidden" name="id_perfilamiento" id="id_perfilamiento" value="<?php echo $idp ?>">
          <input type="hidden" id="ver_diag_pep" value="<?php echo $ver_diag_pep; ?>">
          <input type="hidden" id="pep_comp" value="<?php echo $pep_comp; ?>">

          <div id="formulario_pep">
            <?php //if($id_actividad==15 || $id_actividad==16){ 

              if($ver_diag_pep==1 || $pep_comp==1) { ?>
              <?php
                $pregunta_pep_1x1='';
                $pregunta_pep_1x2='';
                if($pregunta_pep_1==1){
                  $pregunta_pep_1x1='checked';
                  $pregunta_pep_1x2='';
                }else if($pregunta_pep_1==2){
                  $pregunta_pep_1x1='';
                  $pregunta_pep_1x2='checked';
                }
              ?>
              <h3 >Diagnóstico de alertas  (para ser contestado por el personal de la Notaria)</h3><br>

              <h3 >Nota: Esta guía permite identificar varios de los factores de riesgo de corrupción y cohecho, su análisis y la decisión de continuar con la relación o transacción comercial, es responsabilidad del personal de la Notaría. </h3>                    
              <h3>Las preguntas de la presente guía NO deben ser consultadas con el cliente, esta guía debe ser contestada por el empleado de atención al cliente o el empleado que está realizando el trámite con el cliente.</h3> <br>              


              <div class="row ppform" id="preg_prinform1"><!-- pregunta pep 1 -->
                <div class="col-md-10">
                  <label>¿El PEP, esposa(o) o hijos u otro familiar, están solicitando la constitución de alguna persona moral o fideicomiso? </label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class="form-check-input pregunta_pep_1" name="pregunta_pep_1" value="1" onclick="btn_pregunta_pep_1()" <?php echo $pregunta_pep_1x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_1" name="pregunta_pep_1" value="2" onclick="btn_pregunta_pep_1()" <?php echo $pregunta_pep_1x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
              <div class="texto_pregunta_pep_1" style="display: none">
                <table width="100%">
                  <tr>
                    <td width="2%"></td>
                    <td width="98%">
                    <?php
                      $pregunta_pep_2x1='';
                      $pregunta_pep_2x2='';
                      if($pregunta_pep_2==1){
                        $pregunta_pep_2x1='checked';
                        $pregunta_pep_2x2='';
                      }else if($pregunta_pep_2==2){
                        $pregunta_pep_2x1='';
                        $pregunta_pep_2x2='checked';
                      }
                      else if($pregunta_pep_2=="0" || $pregunta_pep_2==""){
                        $pregunta_pep_2x1='';
                        $pregunta_pep_2x2='checked';
                      }
                    ?>
                    <div class="row ppform"><!-- pregunta pep 2.1 -->
                      <div class="col-md-10">
                        <label>¿La persona moral tiene accionistas de nacionalidad extranjera? / ¿En el fideicomiso participan personas de nacionalidad extranjera como fideicomitentes o fideicomisarios?</label>
                      </div>  
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="3" type="radio" class="form-check-input pregunta_pep_2" name="pregunta_pep_2" value="1" <?php echo $pregunta_pep_2x1 ?>>
                            SI
                          </label>
                        </div>
                      </div>
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="1" type="radio" class="form-check-input pregunta_pep_2" name="pregunta_pep_2" value="2" <?php echo $pregunta_pep_2x2 ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>

                    <?php
                      $pregunta_pep_3x1='';
                      $pregunta_pep_3x2='';
                      if($pregunta_pep_3==1){
                        $pregunta_pep_3x1='checked';
                        $pregunta_pep_3x2='';
                      }else if($pregunta_pep_3==2){
                        $pregunta_pep_3x1='';
                        $pregunta_pep_3x2='checked';
                      }else if($pregunta_pep_3=="0" || $pregunta_pep_3==""){
                        $pregunta_pep_3x1='';
                        $pregunta_pep_3x2='checked';
                      }
                    ?>
                    <div class="row ppform"><!-- pregunta pep 3 -->
                      <div class="col-md-10">
                        <label>¿El PEP, esposa(o), hijos u otro familiar son accionistas de la persona moral o fideicomiso que se está constituyendo?</label>
                      </div>  
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="3" type="radio" class="form-check-input pregunta_pep_3" name="pregunta_pep_3" value="1" <?php echo $pregunta_pep_3x1 ?>>
                            SI
                          </label>
                        </div>
                      </div>
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="1" type="radio" class="form-check-input pregunta_pep_3" name="pregunta_pep_3" value="2" <?php echo $pregunta_pep_3x2 ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                 
                    <?php
                      $pregunta_pep_4x1='';
                      $pregunta_pep_4x2='';
                      if($pregunta_pep_4==1){
                        $pregunta_pep_4x1='checked';
                        $pregunta_pep_4x2='';
                      }else if($pregunta_pep_4==2){
                        $pregunta_pep_4x1='';
                        $pregunta_pep_4x2='checked';
                      }else if($pregunta_pep_4=="0" || $pregunta_pep_4==""){
                        $pregunta_pep_4x1='';
                        $pregunta_pep_4x2='checked';
                      }
                    ?>
                    <div class="row ppform"><!-- pregunta pep 4 -->
                      <div class="col-md-10">
                        <label>La persona moral que se está constituyendo, ¿Se dedicará a alguna(s) de la(s) siguiente(s) actividade(s)?:  Extracción de recursos naturales (petróleo y madera) /Contratación pública / Salud (dispositivos médicos y farmacéuticos)   / Contratación de infraestructura / Privatización de servicios gubernamentales / Obas públicas y de construcción / Empresa de servicio público / Bienes raíces, propiedades, servicios legales y de negocios / Industria del petróleo y gas  o   Minería o   Generación y transmisión de energía / Industria farmacéutica y sanitaria</label>
                      </div>  
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="3" type="radio" class="form-check-input pregunta_pep_4" name="pregunta_pep_4" value="1" <?php echo $pregunta_pep_4x1 ?>>
                            SI
                          </label>
                        </div>
                      </div>
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="1" type="radio" class="form-check-input pregunta_pep_4" name="pregunta_pep_4" value="2" <?php echo $pregunta_pep_4x2 ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                    <br>
                    <?php
                      $pregunta_pep_5x1='';
                      $pregunta_pep_5x2='';
                      if($pregunta_pep_5==1){
                        $pregunta_pep_5x1='checked';
                        $pregunta_pep_5x2='';
                      }else if($pregunta_pep_5==2){
                        $pregunta_pep_5x1='';
                        $pregunta_pep_5x2='checked';
                      }else if($pregunta_pep_5=="0" || $pregunta_pep_5==""){
                        $pregunta_pep_5x1='';
                        $pregunta_pep_5x2='checked';
                      }
                    ?>
                    <div class="row ppform"><!-- pregunta pep 5 -->
                      <div class="col-md-10">
                        <label>Los accionistas de la persona moral, ¿tienen el mismo domicilio?</label>
                      </div>  
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="3" type="radio" class="form-check-input pregunta_pep_5" name="pregunta_pep_5" value="1" <?php echo $pregunta_pep_5x1 ?>>
                            SI
                          </label>
                        </div>
                      </div>
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="1" type="radio" class="form-check-input pregunta_pep_5" name="pregunta_pep_5" value="2" <?php echo $pregunta_pep_5x2 ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                    <?php
                      $pregunta_pep_6x1='';
                      $pregunta_pep_6x2='';
                      if($pregunta_pep_6==1){
                        $pregunta_pep_6x1='checked';
                        $pregunta_pep_6x2='';
                      }else if($pregunta_pep_6==2){
                        $pregunta_pep_6x1='';
                        $pregunta_pep_6x2='checked';
                      }else if($pregunta_pep_6=="0" || $pregunta_pep_6==""){
                        $pregunta_pep_6x1='';
                        $pregunta_pep_6x2='checked';
                      }
                    ?>
                    <div class="row ppform"><!-- pregunta pep 6 -->
                      <div class="col-md-10">
                        <label>Los directivos de la persona moral, ¿tienen el mismo domicilio?</label>
                      </div>  
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="3" type="radio" class="form-check-input pregunta_pep_6" name="pregunta_pep_6" value="1" <?php echo $pregunta_pep_6x1 ?>>
                            SI
                          </label>
                        </div>
                      </div>
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="1" type="radio" class="form-check-input pregunta_pep_6" name="pregunta_pep_6" value="2" <?php echo $pregunta_pep_6x2 ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                    <?php
                      $pregunta_pep_7x1='';
                      $pregunta_pep_7x2='';
                      if($pregunta_pep_7==1){
                        $pregunta_pep_7x1='checked';
                        $pregunta_pep_7x2='';
                      }else if($pregunta_pep_7==2){
                        $pregunta_pep_7x1='';
                        $pregunta_pep_7x2='checked';
                      }
                      else if($pregunta_pep_7=="0" || $pregunta_pep_7==""){
                        $pregunta_pep_7x1='';
                        $pregunta_pep_7x2='checked';
                      }
                    ?>
                    <div class="row ppform"><!-- pregunta pep 7.1 -->
                      <div class="col-md-10">
                        <label>Los accionistas o directivos de la persona moral ¿son empleados del PEP, esposa(o) o hijos? O ¿En el fideicomiso participa algún empleado del PEP, esposa o hijos? <label>
                      </div>  
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="3" type="radio" class="form-check-input pregunta_pep_7" name="pregunta_pep_7" value="1" <?php echo $pregunta_pep_7x1 ?>>
                            SI
                          </label>
                        </div>
                      </div>
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="1" type="radio" class="form-check-input pregunta_pep_7" name="pregunta_pep_7" value="2" <?php echo $pregunta_pep_7x2 ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                    <?php
                      $pregunta_pep_8x1='';
                      $pregunta_pep_8x2='';
                      if($pregunta_pep_8==1){
                        $pregunta_pep_8x1='checked';
                        $pregunta_pep_8x2='';
                      }else if($pregunta_pep_8==2){
                        $pregunta_pep_8x1='';
                        $pregunta_pep_8x2='checked';
                      }else if($pregunta_pep_8=="0" || $pregunta_pep_8==""){
                        $pregunta_pep_8x1='';
                        $pregunta_pep_8x2='checked';
                      }
                    ?>
                    <div class="row ppform"><!-- pregunta pep 8 -->
                      <div class="col-md-10">
                        <label>¿El PEP ha constituido otras empresas en la Notaría, en las cuales participen los mismos accionistas y/o directivos?
                        </label>
                      </div>  
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="3" type="radio" class="form-check-input pregunta_pep_8" name="pregunta_pep_8" value="1" <?php echo $pregunta_pep_8x1 ?>>
                            SI
                          </label>
                        </div>
                      </div>
                      <div class="col-md-1 form-group">
                        <div class="form-check form-check-success">
                          <label class="form-check-label">
                            <input data-grado="1" type="radio" class="form-check-input pregunta_pep_8" name="pregunta_pep_8" value="2" <?php echo $pregunta_pep_8x2 ?>>
                            NO
                          </label>
                        </div>
                      </div>
                    </div>
                    </td>
                  </tr>
                </table>
              </div>
              <?php
                $pregunta_pep_9x1='';
                $pregunta_pep_9x2='';
                if($pregunta_pep_9==1){
                  $pregunta_pep_9x1='checked';
                  $pregunta_pep_9x2='';
                }else if($pregunta_pep_9==2){
                  $pregunta_pep_9x1='';
                  $pregunta_pep_9x2='checked';
                }
              ?>
              <div class="row ppform" id="princ2"><!-- pregunta pep 9 -->
                <div class="col-md-10">
                  <label>¿El PEP, esposa(o), hijo(s) o algún familiar directo del PEP está(n) adquiriendo algún inmueble, terreno o desarrollo inmobiliario?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class="form-check-input pregunta_pep_9" name="pregunta_pep_9" value="1" <?php echo $pregunta_pep_9x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_9" name="pregunta_pep_9" value="2" <?php echo $pregunta_pep_9x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
              <?php
                $pregunta_pep_10x1='';
                $pregunta_pep_10x2='';
                if($pregunta_pep_10==1){
                  $pregunta_pep_10x1='checked';
                  $pregunta_pep_10x2='';
                }else if($pregunta_pep_10==2){
                  $pregunta_pep_10x1='';
                  $pregunta_pep_10x2='checked';
                }
              ?>
              <div class="row ppform" id="princ3"><!-- pregunta pep 10 -->
                <div class="col-md-10">
                  <label>¿El PEP esposa(o), hijo(s) o algún familiar directo del PEP está(n) vendiendo algún inmueble, terreno o desarrollo inmobiliario? </label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class="form-check-input pregunta_pep_10" name="pregunta_pep_10" value="1" onclick="btn_pregunta_pep_10()" <?php echo $pregunta_pep_10x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_10" name="pregunta_pep_10" value="2" onclick="btn_pregunta_pep_10()" <?php echo $pregunta_pep_10x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>  
              
              <!--Si la respuesta 10 es afirrmativa -->
              <div class="texto_pregunta_pep_10" style="display: none">
                <div class="row"><!-- pregunta pep 11 -->
                  <div class="col-md-12 form-group">
                    <label>¿Cuándo compró el inmueble, terreno o el desarrollo inmobiliario que ahora está vendiendo?</label>
                    <!--<input class="form-control" type="text" name="pregunta_pep_11" id="pregunta_pep_11" value="<?php echo $pregunta_pep_11 ?>">-->
                    <select class="form-control" id="pregunta_pep_11" name="pregunta_pep_11">
                      <option data-nivelriesgo="0" value=""></option>
                      <option data-nivelriesgo="3" <?php if($pregunta_pep_11=="1"){ echo "selected"; } ?> value="1">Menos de un año</option>
                      <option data-nivelriesgo="2" <?php if($pregunta_pep_11=="2"){ echo "selected"; } ?> value="2"> Más de un año y menos de dos años</option>
                      <option data-nivelriesgo="2.5" <?php if($pregunta_pep_11=="3"){ echo "selected"; } ?> value="3">Más de dos años y menos de tres años</option>
                      <option data-nivelriesgo="1" <?php if($pregunta_pep_11=="4"){ echo "selected"; } ?> value="4">Más de 3 años</option>
                    </select>
                  </div>  
                </div>  
              </div>
              <?php
                $pregunta_pep_12x1='';
                $pregunta_pep_12x2='';
                if($pregunta_pep_12==1){
                  $pregunta_pep_12x1='checked';
                  $pregunta_pep_12x2='';
                }else if($pregunta_pep_12==2){
                  $pregunta_pep_12x1='';
                  $pregunta_pep_12x2='checked';
                }
              ?>
              <div class="row ppform" id="princ4"><!-- pregunta pep  12 -->
                <div class="col-md-10">
                  <label>¿El servicio a contratar es la cesión de acciones de una persona moral a favor del PEP, esposa(o), hijo(s), socio(s) o empleado(s) del PEP?  </label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class= "form-check-input pregunta_pep_12" name="pregunta_pep_12" value="1" onclick="btn_pregunta_pep_12()" <?php echo $pregunta_pep_12x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_12" name="pregunta_pep_12" value="2" onclick="btn_pregunta_pep_12()" <?php echo $pregunta_pep_12x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>     
              <!--Si la respuesta 13 es afirrmativa -->
              <div class="texto_pregunta_pep_12" style="display: none">
                <div class="row"><!-- pregunta pep  -->
                  <div class="col-md-12 form-group">
                    <label>¿Cuál es la razón de la cesión de las acciones? </label>
                    <input data-grado="0" class="form-control" type="text" name="pregunta_pep_13" value="<?php echo $pregunta_pep_13 ?>">
                 </div>  
                </div>  
              </div>                
              <?php
                $pregunta_pep_14x1='';
                $pregunta_pep_14x2='';
                if($pregunta_pep_14==1){
                  $pregunta_pep_14x1='checked';
                  $pregunta_pep_14x2='';
                }else if($pregunta_pep_14==2){
                  $pregunta_pep_14x1='';
                  $pregunta_pep_14x2='checked';
                }
              ?>
              <div class="row ppform" id="princ5"><!-- pregunta pep 14 -->
                <div class="col-md-10">
                  <label>¿El servicio a contratar es la compra de acciones de una persona moral a favor del PEP, esposa(o), hijo(s), socio(s) o empleado(s) del PEP?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class="form-check-input pregunta_pep_14" name="pregunta_pep_14" value="1" onclick="btn_pregunta_pep_14()" <?php echo $pregunta_pep_14x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_14" name="pregunta_pep_14" value="2" onclick="btn_pregunta_pep_14()" <?php echo $pregunta_pep_14x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>     
              <!--Si la respuesta 10 es afirrmativa -->
              <div class="texto_pregunta_pep_14" style="display: none">
                <div class="row"><!-- pregunta pep 15 -->
                  <div class="col-md-10 form-group">
                    <label>¿Cuál es la razón de la compra de las acciones?</label>
                    <input data-grado="0" class="form-control" type="text" name="pregunta_pep_15" value="<?php echo $pregunta_pep_15 ?>">
                  </div>  
                </div>  
              </div>    
              <?php
                $pregunta_pep_16x1='';
                $pregunta_pep_16x2='';
                if($pregunta_pep_16==1){
                  $pregunta_pep_16x1='checked';
                  $pregunta_pep_16x2='';
                }else if($pregunta_pep_16==2){
                  $pregunta_pep_16x1='';
                  $pregunta_pep_16x2='checked';
                }
              ?>
              <div class="row ppform"><!-- pregunta pep  12 -->
                <div class="col-md-10">
                  <label>La contratación del servicio se está realizando para : (a) Alguna Persona moral donde el PEP, esposa(o), hijo(s) es accionista o socio (b) Fideicomiso en el que participe el PEP o algún familiar, socio o amigo (c) Mandato sin representación (d) Organización sin fines de lucro (e) Empresas de responsabilidad limitada (f) Empresas u otros fideicomisos constituidos en otros países</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class=  "form-check-input pregunta_pep_16" name="pregunta_pep_16" value="1" <?php echo $pregunta_pep_16x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_16" name="pregunta_pep_16" value="2" <?php echo $pregunta_pep_16x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>  
              <br>
              <?php
                $pregunta_pep_17x1='';
                $pregunta_pep_17x2='';
                if($pregunta_pep_17==1){
                  $pregunta_pep_17x1='checked';
                  $pregunta_pep_17x2='';
                }else if($pregunta_pep_17==2){
                  $pregunta_pep_17x1='';
                  $pregunta_pep_17x2='checked';
                }
              ?>
              <div class="row ppform"><!-- pregunta pep 17 -->
                <div class="col-md-10">
                  <label>¿En el servicio que se está contratando con la Notaría, participa la familia: esposa(o), hijos, amigos, empleados o socios del PEP?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class="form-check-input pregunta_pep_17" name="pregunta_pep_17" value="1" <?php echo $pregunta_pep_17x1 ?>> 
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_17" name="pregunta_pep_17" value="2" <?php echo $pregunta_pep_17x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div> 
              <?php
                $pregunta_pep_18x1='';
                $pregunta_pep_18x2='';
                if($pregunta_pep_18==1){
                  $pregunta_pep_18x1='checked';
                  $pregunta_pep_18x2='';
                }else if($pregunta_pep_18==2){
                  $pregunta_pep_18x1='';
                  $pregunta_pep_18x2='checked';
                }
              ?>
              <div class="row ppform"><!-- pregunta pep 18 -->
                <div class="col-md-10">
                  <label>¿El servicio lo está contratando o solicitando directamente el PEP?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_18" name="pregunta_pep_18" value="1" <?php echo $pregunta_pep_18x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class="form-check-input pregunta_pep_18" name="pregunta_pep_18" value="2" <?php echo $pregunta_pep_18x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div> 
              <?php
                $pregunta_pep_19x1='';
                $pregunta_pep_19x2='';
                if($pregunta_pep_19==1){
                  $pregunta_pep_19x1='checked';
                  $pregunta_pep_19x2='';
                }else if($pregunta_pep_19==2){
                  $pregunta_pep_19x1='';
                  $pregunta_pep_19x2='checked';
                }
              ?>
              <div class="row ppform" id="princ19"><!-- pregunta pep 19 -->
                <div class="col-md-10">
                  <label>¿El servicio lo está contratando o solicitando directamente la familia o socios o empleados del PEP?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" onclick="btn_pregunta_pep_19()" class="form-check-input pregunta_pep_19" name="pregunta_pep_19" value="1" <?php echo $pregunta_pep_19x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" onclick="btn_pregunta_pep_19()" class="form-check-input pregunta_pep_19" name="pregunta_pep_19" value="2" <?php echo $pregunta_pep_19x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>

              <?php
                $pregunta_pep_20x1='';
                $pregunta_pep_20x2='';
                if($pregunta_pep_20==1){
                  $pregunta_pep_20x1='checked';
                  $pregunta_pep_20x2='';
                }else if($pregunta_pep_20==2){
                  $pregunta_pep_20x1='';
                  $pregunta_pep_20x2='checked';
                }
              ?>
              <div class="row ppform" id="princ20"><!-- pregunta pep 20 -->
                <div class="col-md-10">
                  <label>¿El servicio se está contratando a través de terceros intermediarios (por ejemplo: abogados, contadores, otros)?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="2" type="radio" class="form-check-input pregunta_pep_20" name="pregunta_pep_20" value="1" <?php echo $pregunta_pep_20x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_20" name="pregunta_pep_20" value="2" <?php echo $pregunta_pep_20x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
              <div class="row" id="princ21">
                <div class="col-md-10 form-group">
                  <label>¿Quién está contratando el servicio?</label>
                  <input data-grado="0" class="form-control pregunta_pep_21" type="text" name="pregunta_pep_21" value="<?php echo $pregunta_pep_21; ?>">
                </div>
              </div>

              <?php
                $pregunta_pep_22x1='';
                $pregunta_pep_22x2='';
                if($pregunta_pep_22==1){
                  $pregunta_pep_22x1='checked';
                  $pregunta_pep_22x2='';
                }else if($pregunta_pep_22==2){
                  $pregunta_pep_22x1='';
                  $pregunta_pep_22x2='checked';
                }
              ?>
              <div class="row ppform"><!-- pregunta pep 22 -->
                <div class="col-md-10">
                  <label>¿La transacción que está realizando el PEP o familiar, es congruente con su nivel de ingresos?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_22" name="pregunta_pep_22" value="1" <?php echo $pregunta_pep_22x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="3" type="radio" class="form-check-input pregunta_pep_22" name="pregunta_pep_22" value="2" <?php echo $pregunta_pep_22x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
              <?php
                $pregunta_pep_23x1='';
                $pregunta_pep_23x2='';
                if($pregunta_pep_23==1){
                  $pregunta_pep_23x1='checked';
                  $pregunta_pep_23x2='';
                }else if($pregunta_pep_23==2){
                  $pregunta_pep_23x1='';
                  $pregunta_pep_23x2='checked';
                }
              ?>
              <div class="row ppform"><!-- pregunta pep 23 -->
                <div class="col-md-10">
                  <label>¿Se puede fácilmente identificar la fuente de donde provendrá el recurso para la transacción solicitada, ejemplo para la compra del inmueble, o la constitución de la sociedad o la compra de acciones, entre otros?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_23" name="pregunta_pep_23" value="1" <?php echo $pregunta_pep_23x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="3" type="radio" class="form-check-input pregunta_pep_23" name="pregunta_pep_23" value="2" <?php echo $pregunta_pep_23x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
              <?php
                $pregunta_pep_24x1='';
                $pregunta_pep_24x2='';
                if($pregunta_pep_24==1){
                  $pregunta_pep_24x1='checked';
                  $pregunta_pep_24x2='';
                }else if($pregunta_pep_24==2){
                  $pregunta_pep_24x1='';
                  $pregunta_pep_24x2='checked';
                }
              ?>
              <div class="row ppform"><!-- pregunta pep 24 -->
                <div class="col-md-10">
                  <label>¿Se ha detectado el PEP o su familiar, han proporcionado información incongruente o inexacta?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="3" type="radio" class="form-check-input pregunta_pep_24" name="pregunta_pep_24" value="1" <?php echo $pregunta_pep_24x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_24" name="pregunta_pep_24" value="2" <?php echo $pregunta_pep_24x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>
              <?php
                $pregunta_pep_25x1='';
                $pregunta_pep_25x2='';
                if($pregunta_pep_25==1){
                  $pregunta_pep_25x1='checked';
                  $pregunta_pep_25x2='';
                }else if($pregunta_pep_25==2){
                  $pregunta_pep_25x1='';
                  $pregunta_pep_25x2='checked';
                }
              ?>
              <div class="row ppform"><!-- pregunta pep 25 -->
                <div class="col-md-10">
                  <label>¿El PEP o su familiar se niegan a presentar la información o datos solicitados?</label>
                </div>  
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="3" type="radio" class="form-check-input pregunta_pep_25" name="pregunta_pep_25" value="1" <?php echo $pregunta_pep_25x1 ?>>
                      SI
                    </label>
                  </div>
                </div>
                <div class="col-md-1 form-group">
                  <div class="form-check form-check-success">
                    <label class="form-check-label">
                      <input data-grado="1" type="radio" class="form-check-input pregunta_pep_25" name="pregunta_pep_25" value="2" <?php echo $pregunta_pep_25x2 ?>>
                      NO
                    </label>
                  </div>
                </div>
              </div>

              <div class="row d-flex flex-row justify-content-center alig-items-center">
                <div class="col-md-6 grid-margin stretch-card ">
                  <div class="card">
                    <div class="card-body">
                      <h2 align="center" id="cal_txt_not"></h2>
                      <div class="template-demo">
                        <div class="progress progress-sm" id="cont_barra_not" style="height: 20px;">
                          <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 1%" aria-valuenow="1" aria-valuemin="1" aria-valuemax="1"></div><p style="font-size:17px"><b>0.00</b></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="texto_firma" style="display: none">
                <div class="row">
                  <div class="col-md-12">
                    <h6 style="color: black;">Revisado por: Firma de ____________________________________________________________________</h6>
                    
                  </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="row texto_firma">
                  <div class="col-md-12">
                    <h6 style="color: black;">Autorizado por: Firma de ___________________________________________________________________</h6>
                  </div>
                </div>

                <br>
              </div> 
              <div class="row" id="espa_uno" style=" page-break-before: always">
                <div class="col-md-12"><br><br><br>
                </div>
              </div>
              <!--<hr class="subtitle">-->
            <?php } //if de anexo 12a y 12b ?>
          </div>
          <?php if($ver_diag_pep==0 && $pep_comp==0) { ?>
            <?php
              $pregunta_1x1='';
              $pregunta_1x2='';
              if($pregunta_1==1){
                $pregunta_1x1='checked';
                $pregunta_1x2='';
              }else if($pregunta_1==2){
                $pregunta_1x1='';
                $pregunta_1x2='checked';
              }
            ?>
           
            <h3 >Diagnóstico de alertas (este cuestionario debe ser contestado por el vendedor o el personal de atención al público y es una guía de conductas sospechosas, por lo cual no se debe de realizar directamente ninguna de las preguntas al cliente/usuario.)</h3><hr>
            <div class="row"><!-- pregunta 1 -->
              <div class="col-md-10">
                <label>El Cliente o Usuario se niega a identificarse</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_1" name="pregunta_1" value="1" <?php echo $pregunta_1x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_1" name="pregunta_1" value="2" <?php echo $pregunta_1x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_2x1='';
              $pregunta_2x2='';
              if($pregunta_2==1){
                $pregunta_2x1='checked';
                $pregunta_2x2='';
              }else if($pregunta_2==2){
                $pregunta_2x1='';
                $pregunta_2x2='checked';
              }
            ?>
            <!-- pregunta 2 -->
            <!--<div class="row">
              <div class="col-md-10">
                <label>El Cliente o Usuario se niega a proporcionar la información solicitada</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_2" name="pregunta_2" value="1" <?php echo $pregunta_2x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_2" name="pregunta_2" value="2" <?php echo $pregunta_2x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> -->
            <?php
              $pregunta_3x1='';
              $pregunta_3x2='';
              if($pregunta_3==1){
                $pregunta_3x1='checked';
                $pregunta_3x2='';
              }else if($pregunta_3==2){
                $pregunta_3x1='';
                $pregunta_3x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 3 -->
              <div class="col-md-10">
                <label>El Cliente o Usuario proporciona la información de un tercero para el llenado del aviso</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_3" name="pregunta_3" value="1" <?php echo $pregunta_3x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_3" name="pregunta_3" value="2" <?php echo $pregunta_3x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_4x1='';
              $pregunta_4x2='';
              if($pregunta_4==1){
                $pregunta_4x1='checked';
                $pregunta_4x2='';
              }else if($pregunta_4==2){
                $pregunta_4x1='';
                $pregunta_4x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 4 -->
              <div class="col-md-10">
                <label>El Cliente o Usuario solicita que la transacción o compra se realizada de forma fraccionada con la finalidad de no alcanzar a los umbrales de alerta</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_4" name="pregunta_4" value="1" <?php echo $pregunta_4x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_4" name="pregunta_4" value="2" <?php echo $pregunta_4x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_5x1='';
              $pregunta_5x2='';
              if($pregunta_5==1){
                $pregunta_5x1='checked';
                $pregunta_5x2='';
              }else if($pregunta_5==2){
                $pregunta_5x1='';
                $pregunta_5x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 5 -->
              <div class="col-md-10">
                <label>El Cliente o Usuario tiene una actividad económica conocida, sin embargo, proporciona información diferente (ejemplo: algún connotado político que se identifique como vendedor</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_5" name="pregunta_5" value="1" <?php echo $pregunta_5x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_5" name="pregunta_5" value="2" <?php echo $pregunta_5x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_6x1='';
              $pregunta_6x2='';
              if($pregunta_6==1){
                $pregunta_6x1='checked';
                $pregunta_6x2='';
              }else if($pregunta_6==2){
                $pregunta_6x1='';
                $pregunta_6x2='checked';
              }
            ?>
            <!-- pregunta 6 -->
            <!--<div class="row">
              <div class="col-md-10">
                <label>En caso de que el cliente sea persona moral, ¿su constitución es inferior a 12 meses?</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_6" name="pregunta_6" value="1" <?php echo $pregunta_6x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_6" name="pregunta_6" value="2" <?php echo $pregunta_6x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> -->
            <?php
              $pregunta_7x1='';
              $pregunta_7x2='';
              if($pregunta_7==1){
                $pregunta_7x1='checked';
                $pregunta_7x2='';
              }else if($pregunta_7==2){
                $pregunta_7x1='';
                $pregunta_7x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 7 -->
              <div class="col-md-10">
                <label>El Cliente o Usuario solicita realizar el pago en efectivo sin que se realice el llenado del Aviso</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_7" name="pregunta_7" value="1" <?php echo $pregunta_7x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_7" name="pregunta_7" value="2" <?php echo $pregunta_7x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_8x1='';
              $pregunta_8x2='';
              if($pregunta_8==1){
                $pregunta_8x1='checked';
                $pregunta_8x2='';
              }else if($pregunta_8==2){
                $pregunta_8x1='';
                $pregunta_8x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 8 -->
              <div class="col-md-10">
                <label>Se desconoce el Beneficiario final y se sospecha de ser un prestanombre o robo de identidad</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_8" name="pregunta_8" value="1" <?php echo $pregunta_8x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_8" name="pregunta_8" value="2" <?php echo $pregunta_8x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_9x1='';
              $pregunta_9x2='';
              if($pregunta_9==1){
                $pregunta_9x1='checked';
                $pregunta_9x2='';
              }else if($pregunta_9==2){
                $pregunta_9x1='';
                $pregunta_9x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 9 -->
              <div class="col-md-10">
                <label>El Cliente o Usuario está declarando que  los recursos con los que se liquida el servicio provienen de cuentas bancarias de Entidades del Estado y/o personas jurídicas que administran recursos públicos, así como a aquellas operaciones relacionadas con los conceptos de campaña y actos de campaña</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_9" name="pregunta_9" value="1" <?php echo $pregunta_9x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_9" name="pregunta_9" value="2" <?php echo $pregunta_9x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_10x1='';
              $pregunta_10x2='';
              if($pregunta_10==1){
                $pregunta_10x1='checked';
                $pregunta_10x2='';
              }else if($pregunta_10==2){
                $pregunta_10x1='';
                $pregunta_10x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 10 -->
              <div class="col-md-10">
                <label>Existe alguna Inconsistencia y/o ausencia en la información proporcionada tal como los números telefónicos y direcciones de los accionistas o empleados en caso de clientes personas morales se repiten o no son verificables</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_10" name="pregunta_10" value="1" <?php echo $pregunta_10x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_10" name="pregunta_10" value="2" <?php echo $pregunta_10x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_11x1='';
              $pregunta_11x2='';
              if($pregunta_11==1){
                $pregunta_11x1='checked';
                $pregunta_11x2='';
              }else if($pregunta_11==2){
                $pregunta_11x1='';
                $pregunta_11x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 11 -->
              <div class="col-md-10">
                <label>En periodos electorales, el trámite lo realiza algún despacho de abogados o contadores en nombre del cliente o titular del servicio</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_11" name="pregunta_11" value="1" <?php echo $pregunta_11x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_11" name="pregunta_11" value="2" <?php echo $pregunta_11x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_12x1='';
              $pregunta_12x2='';
              if($pregunta_12==1){
                $pregunta_12x1='checked';
                $pregunta_12x2='';
              }else if($pregunta_12==2){
                $pregunta_12x1='';
                $pregunta_12x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 12 -->
              <div class="col-md-10">
                <label>Se identifica incongruencia entre el perfil económico de los Clientes o Usuarios y las transacciones efectuadas por ellos, ya sea en su nombre o en nombre de sus empresas</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_12" name="pregunta_12" value="1" <?php echo $pregunta_12x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_12" name="pregunta_12" value="2" <?php echo $pregunta_12x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_13x1='';
              $pregunta_13x2='';
              if($pregunta_13==1){
                $pregunta_13x1='checked';
                $pregunta_13x2='';
              }else if($pregunta_13==2){
                $pregunta_13x1='';
                $pregunta_13x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 13 -->
              <div class="col-md-10">
                <label>La Persona moral o física, presenta incongruencias en los datos o los cambia, al momento del contrato o documento de que se trate, o al proporcionar los datos fiscales o cuando proporciona la información para el llenado de los cuestionarios y formularios</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_13" name="pregunta_13" value="1" <?php echo $pregunta_13x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_13" name="pregunta_13" value="2" <?php echo $pregunta_13x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_14x1='';
              $pregunta_14x2='';
              if($pregunta_14==1){
                $pregunta_14x1='checked';
                $pregunta_14x2='';
              }else if($pregunta_14==2){
                $pregunta_14x1='';
                $pregunta_14x2='checked';
              }
            ?> <!-- pregunta 14 -->
            
            <div class="row">
              <div class="col-md-10">
                <label>Se declara una ocupación o actividad distinta a la que aparece en su cédula de identificación fiscal</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_14" name="pregunta_14" value="1" <?php echo $pregunta_14x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_14" name="pregunta_14" value="2" <?php echo $pregunta_14x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_15x1='';
              $pregunta_15x2='';
              if($pregunta_15==1){
                $pregunta_15x1='checked';
                $pregunta_15x2='';
              }else if($pregunta_15==2){
                $pregunta_15x1='';
                $pregunta_15x2='checked';
              }
            ?>
            <!-- pregunta 15 -->
            <!--<div class="row">
              <div class="col-md-10">
                <label>Los representantes legales de la persona moral son personas físicas jóvenes de menos de 25 años</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_15" name="pregunta_15" value="1" <?php echo $pregunta_15x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_15" name="pregunta_15" value="2" <?php echo $pregunta_15x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> -->
            <?php
              $pregunta_16x1='';
              $pregunta_16x2='';
              if($pregunta_16==1){
                $pregunta_16x1='checked';
                $pregunta_16x2='';
              }else if($pregunta_16==2){
                $pregunta_16x1='';
                $pregunta_16x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 16 -->
              <div class="col-md-10">
                <label>El Cliente no solicita la expedición de sus Comprobantes Fiscales Digitales</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_16" name="pregunta_16" value="1" <?php echo $pregunta_16x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_16" name="pregunta_16" value="2" <?php echo $pregunta_16x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_17x1='';
              $pregunta_17x2='';
              if($pregunta_17==1){
                $pregunta_17x1='checked';
                $pregunta_17x2='';
              }else if($pregunta_17==2){
                $pregunta_17x1='';
                $pregunta_17x2='checked';
              }
            ?>
            <!-- pregunta 17 -->
            <!--
            <div class="row">
              <div class="col-md-10">
                <label>El cliente paga con recursos en efectivo, por lo que se desconoce su procedencia</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_17" name="pregunta_17" value="1" <?php echo $pregunta_17x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_17" name="pregunta_17" value="2" <?php echo $pregunta_17x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> -->
            <?php
              $pregunta_18x1='';
              $pregunta_18x2='';
              if($pregunta_18==1){
                $pregunta_18x1='checked';
                $pregunta_18x2='';
              }else if($pregunta_18==2){
                $pregunta_18x1='';
                $pregunta_18x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 18 -->
              <div class="col-md-10">
                <label>El cliente para liquidar la transacción recibe recursos de múltiples zonas geográficas, sin que se encuentre relación entre los sujetos receptores y/u ordenantes</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_18" name="pregunta_18" value="1" <?php echo $pregunta_18x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_18" name="pregunta_18" value="2" <?php echo $pregunta_18x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_19x1='';
              $pregunta_19x2='';
              if($pregunta_19==1){
                $pregunta_19x1='checked';
                $pregunta_19x2='';
              }else if($pregunta_19==2){
                $pregunta_19x1='';
                $pregunta_19x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 19 -->
              <div class="col-md-10">
                <label>La transacción y operación que está realizando el cliente , no es acorde o congruente con su nivel de ingresos</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_19" name="pregunta_19" value="1" <?php echo $pregunta_19x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_19" name="pregunta_19" value="2" <?php echo $pregunta_19x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_20x1='';
              $pregunta_20x2='';
              if($pregunta_20==1){
                $pregunta_20x1='checked';
                $pregunta_20x2='';
              }else if($pregunta_20==2){
                $pregunta_20x1='';
                $pregunta_20x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 20 -->
              <div class="col-md-10">
                <label>El cliente hace uso de un intermediario para realizar operaciones no acordes con su actividad económica o giro mercantil </label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_20" name="pregunta_20" value="1" <?php echo $pregunta_20x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_20" name="pregunta_20" value="2" <?php echo $pregunta_20x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_21x1='';
              $pregunta_21x2='';
              if($pregunta_21==1){
                $pregunta_21x1='checked';
                $pregunta_21x2='';
              }else if($pregunta_21==2){
                $pregunta_21x1='';
                $pregunta_21x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 21 -->
              <div class="col-md-10">
                <label>El cliente está liquidando la operación con divisas en efectivo sin justificación alguna</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_21" name="pregunta_21" value="1" <?php echo $pregunta_21x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_21" name="pregunta_21" value="2" <?php echo $pregunta_21x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_22x1='';
              $pregunta_22x2='';
              if($pregunta_22==1){
                $pregunta_22x1='checked';
                $pregunta_22x2='';
              }else if($pregunta_22==2){
                $pregunta_22x1='';
                $pregunta_22x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 22 -->
              <div class="col-md-10">
                <label>El cliente o accionista está siendo extrañamente acompañado o vigilado por otros individuos o bien,  un tercero mantiene en todo momento en su poder la documentación del cliente</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_22" name="pregunta_22" value="1" <?php echo $pregunta_22x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_22" name="pregunta_22" value="2" <?php echo $pregunta_22x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_23x1='';
              $pregunta_23x2='';
              if($pregunta_23==1){
                $pregunta_23x1='checked';
                $pregunta_23x2='';
              }else if($pregunta_23==2){
                $pregunta_23x1='';
                $pregunta_23x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 23 -->
              <div class="col-md-10">
                <label>El cliente o accionista  está siendo extrañamente acompañado o vigilado y el cliente o accionista parece desconocer el servicio solicitado</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_23" name="pregunta_23" value="1" <?php echo $pregunta_23x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_23" name="pregunta_23" value="2" <?php echo $pregunta_23x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_24x1='';
              $pregunta_24x2='';
              if($pregunta_24==1){
                $pregunta_24x1='checked';
                $pregunta_24x2='';
              }else if($pregunta_24==2){
                $pregunta_24x1='';
                $pregunta_24x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 24 -->
              <div class="col-md-10">
                <label>La información y documentación presentada es inconsistente o de difícil verificación</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_24" name="pregunta_24" value="1" <?php echo $pregunta_24x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input pregunta_24" name="pregunta_24" value="2" <?php echo $pregunta_24x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div>

            <!-- TERMINA PREGUNTAS NUEVAS DEL ULTIMO DOC DE MARA  -->
              <!--<div class="col-md-12 form-group">
                <label>Conclusiones:</label><br>
                <textarea class="form-control"></textarea>
              </div>-->
            
            <?php
              $pregunta_25x1='';
              $pregunta_25x2='';
              if($pregunta_25==1){
                $pregunta_25x1='checked';
                $pregunta_25x2='';
              }else if($pregunta_25==2){
                $pregunta_25x1='';
                $pregunta_25x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 25 -->
              <div class="col-md-10">
                <label>El cliente intenta sobornar, extorsionar o amenazar con  el fin de realizar la operación fuera de los parámetros establecidos, o con la finalidad de evitar el envío del aviso</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_25" name="pregunta_25" value="1" <?php echo $pregunta_25x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_25" name="pregunta_25" value="2" <?php echo $pregunta_25x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_26x1='';
              $pregunta_26x2='';
              if($pregunta_26==1){
                $pregunta_26x1='checked';
                $pregunta_26x2='';
              }else if($pregunta_26==2){
                $pregunta_26x1='';
                $pregunta_26x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 26 -->
              <div class="col-md-10">
                <label>En caso de realizar la operación con organizaciones sin fines de lucro,  las características de la transacción no coinciden  con los objetivos de la entidad</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_26" name="pregunta_26" value="1" <?php echo $pregunta_26x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_26" name="pregunta_26" value="2" <?php echo $pregunta_26x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <?php
              $pregunta_27x1='';
              $pregunta_27x2='';
              if($pregunta_27==1){
                $pregunta_27x1='checked';
                $pregunta_27x2='';
              }else if($pregunta_27==2){
                $pregunta_27x1='';
                $pregunta_27x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 27 -->
              <div class="col-md-10">
                <label>El cliente insiste en liquidar la operación en efectivo rebasando el umbral permitido o bien, por montos elevados</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_27" name="pregunta_27" value="1" <?php echo $pregunta_27x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_27" name="pregunta_27" value="2" <?php echo $pregunta_27x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div> 

            <?php
              $pregunta_29x1='';
              $pregunta_29x2='';
              if($pregunta_29==1){
                $pregunta_29x1='checked';
                $pregunta_29x2='';
              }else if($pregunta_29==2){
                $pregunta_29x1='';
                $pregunta_29x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 29 -->
              <div class="col-md-10">
                <label>De acuerdo con medios informativos u otras fuentes de información pública, se tiene conocimiento o sospecha de que el cliente, un familiar o persona relacionada, está vinculado con actividades ilícitas o se encuentra bajo proceso de investigación</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_29" name="pregunta_29" value="1" <?php echo $pregunta_29x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_29" name="pregunta_29" value="2" <?php echo $pregunta_29x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div>
            <?php
              $pregunta_30x1='';
              $pregunta_30x2='';
              if($pregunta_30==1){
                $pregunta_30x1='checked';
                $pregunta_30x2='';
              }else if($pregunta_30==2){
                $pregunta_30x1='';
                $pregunta_30x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 30 -->
              <div class="col-md-10">
                <label>El pago es realizado por un tercero sin relación aparente con el cliente</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_30" name="pregunta_30" value="1" <?php echo $pregunta_30x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_30" name="pregunta_30" value="2" <?php echo $pregunta_30x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div>
            <?php
              $pregunta_31x1='';
              $pregunta_31x2='';
              if($pregunta_31==1){
                $pregunta_31x1='checked';
                $pregunta_31x2='';
              }else if($pregunta_31==2){
                $pregunta_31x1='';
                $pregunta_31x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 31 -->
              <div class="col-md-10">
                <label>El cliente proporciona documentos falsos o apócrifos </label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_31" name="pregunta_31" value="1" <?php echo $pregunta_31x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_31" name="pregunta_31" value="2" <?php echo $pregunta_31x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div>
            <?php
              $pregunta_32x1='';
              $pregunta_32x2='';
              if($pregunta_32==1){
                $pregunta_32x1='checked';
                $pregunta_32x2='';
              }else if($pregunta_32==2){
                $pregunta_32x1='';
                $pregunta_32x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 32 -->
              <div class="col-md-10">
                <label>El cliente ha realizado múltiples operaciones en un periodo muy corto de tiempo sin razón aparente</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_32" name="pregunta_32" value="1" <?php echo $pregunta_32x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_32" name="pregunta_32" value="2" <?php echo $pregunta_32x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div>
            <?php
              $pregunta_33x1='';
              $pregunta_33x2='';
              if($pregunta_33==1){
                $pregunta_33x1='checked';
                $pregunta_33x2='';
              }else if($pregunta_33==2){
                $pregunta_33x1='';
                $pregunta_33x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 33 -->
              <div class="col-md-10">
                <label>El cliente pretende liquidar la operación con monedas virtuales</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_33" name="pregunta_33" value="1" <?php echo $pregunta_33x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_33" name="pregunta_33" value="2" <?php echo $pregunta_33x2 ?>>
                    NO
                  </label>
                </div>
              </div>
            </div>
            <?php
              $pregunta_34x1='';
              $pregunta_34x2='';
              if($pregunta_34==1){
                $pregunta_34x1='checked';
                $pregunta_34x2='';
              }else if($pregunta_34==2){
                $pregunta_34x1='';
                $pregunta_34x2='checked';
              }
            ?>
            <div class="row"><!-- pregunta 34 -->
              <div class="col-md-10">
                <label>Los datos proporcionados por el cliente no coinciden con los datos de los documentos de identificación</label>
              </div>  
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="3" class="form-check-input pregunta_34" name="pregunta_34" value="1" <?php echo $pregunta_34x1 ?>>
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" data-valord="1" class="form-check-input pregunta_34" name="pregunta_34" value="2" <?php echo $pregunta_34x2 ?>>
                    NO
                  </label>
                </div>
              </div>

              <!--<div class="col-md-12 form-group">
                <label>Conclusiones:</label><br>
                <textarea class="form-control"></textarea>
              </div>-->
            </div>
            <div class="col-md-12"><hr class="subtitle"></div>
            <div class="" id="cont_preg_ext"><!-- pregunta 34 -->
              <div class="" id="cont_preg_ext_sub">
              <!-- preguntas extras por actividad -->
              <?php if($id_actividad==2){ ?>
              <!-- actividad 2A -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>El cliente realiza el pago del saldo de tarjeta en efectivo por cantidades elevadas por medio de canales no bancarios</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
        
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cleinte realiza una compra por un valor significativamente elevado, pagando en la fecha de corte dicha operación en efectivo</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>De acuerdo con los ingresos declarados, las operaciones realizadas con  la tarjeta parecen estar fuera del alcance del cliente</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label>Se piden  varias tarjetas con características similares sin que exista justificación para ello</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 2A -->

              <?php if($id_actividad==3){ ?>
              <!-- actividad 2B -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>El cliente realiza operaciones de carga en diferentes tarjetas de forma periódica rebasando el umbral de aviso</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
        
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cliente realiza actividades de carga o recarga por montos elevados en efectivo o en un periodo de corto tiempo</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>Las operaciones de carga o recarga de realizan en varias localidades, estados o jurisdicciones</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 2B -->

              <?php if($id_actividad==6){ ?>
              <!-- actividad 4 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>La operación de mutuo o crédito se lleva a cabo por medio de una garantía poco usual o que no corresponde con la actividad o ingresos del cliente</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2.5" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
        
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>Hay indicios, o certeza, de que los bienes empeñados son robados o provienen de una actividad ilícita</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>El cliente realiza operaciones de manera periódica en las que se liquida el total del monto del préstamo otorgado en efectivo al poco tiempo de haberlo adquirido</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label> El cliente registra domicilios distintos cada vez que solicita un préstamo o crédito</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 4 -->

              <?php if($id_actividad==7 || $id_actividad==8){ ?>
              <!-- actividad 5 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label> El cliente no quiere ser relacionado con el desarrollo o con la compra del inmueble</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>No hay interés en las características de la propiedad objeto de la operación, o en el precio y condiciones de la transacción</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>Se  sospecha que algunos intermediarios dentro del sistema financiero están coludidos con el cliente</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2.5" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label> Al cliente no le importa pagar precios superiores a los del mercado con la finalidad de que la operación se realice fuera de los parámetros establecidos </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_39x1='';
                  $pregunta_39x2='';
                  if($pregunta_39==1){
                    $pregunta_39x1='checked';
                    $pregunta_39x2='';
                  }else if($pregunta_39==2){
                    $pregunta_39x1='';
                    $pregunta_39x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 39 -->
                  <div class="col-md-10">
                    <label> El cliente solicita que se realice la operación por medio de un contrato privado, donde no hay intención de registrarlo ante notario  </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_39" name="pregunta_39" id="39" value="1" <?php echo $pregunta_39x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_39" name="pregunta_39" value="2" <?php echo $pregunta_39x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_40x1='';
                  $pregunta_40x2='';
                  if($pregunta_40==1){
                    $pregunta_40x1='checked';
                    $pregunta_40x2='';
                  }else if($pregunta_40==2){
                    $pregunta_40x1='';
                    $pregunta_40x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 40 -->
                  <div class="col-md-10">
                    <label>El cliente opera en grupos sin que exista algún parentesco o relación entre ellos y la operación se lleva acabo a un valor de venta o compra significativamente diferente (mucho mayor o menor) a partir del valor real de la propiedad de los valores de mercado </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_40" name="pregunta_40" id="40" value="1" <?php echo $pregunta_40x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_40" name="pregunta_40" value="2" <?php echo $pregunta_40x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_41x1='';
                  $pregunta_41x2='';
                  if($pregunta_41==1){
                    $pregunta_41x1='checked';
                    $pregunta_41x2='';
                  }else if($pregunta_41==2){
                    $pregunta_41x1='';
                    $pregunta_41x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 41 -->
                  <div class="col-md-10">
                    <label>El cliente es un fideicomiso y se niega a proporcionar información</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_41" name="pregunta_41" id="41" value="1" <?php echo $pregunta_41x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_41" name="pregunta_41" value="2" <?php echo $pregunta_41x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_42x1='';
                  $pregunta_42x2='';
                  if($pregunta_42==1){
                    $pregunta_42x1='checked';
                    $pregunta_42x2='';
                  }else if($pregunta_42==2){
                    $pregunta_42x1='';
                    $pregunta_42x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 42 -->
                  <div class="col-md-10">
                    <label>El cliente realiza transacciones sucesivas de compra y venta de la misma propiedad en un periodo corto de tiempo, con cambios injustificados del valor de la misma </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_42" name="pregunta_42" id="42" value="1" <?php echo $pregunta_42x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_42" name="pregunta_42" value="2" <?php echo $pregunta_42x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 5 -->

              <?php if($id_actividad==9){ ?>
              <!-- actividad 6 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>El cliente realiza la compra o venta de grandes cantidades de piedras preciosas, metales preciosos, joyas o relojes sin justificar su procedencia </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
        
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cliente realiza compras indiscriminadas de mercancía sin importar su color, tamaño y precio</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>Lo que se conoce del cliente se sabe que recolecta metales preciosos y después lo funde.</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2.5" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label> El cliente exporta e importa piedras preciosas, metales preciosos, joyas o relojes sin justificación aparente</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_39x1='';
                  $pregunta_39x2='';
                  if($pregunta_39==1){
                    $pregunta_39x1='checked';
                    $pregunta_39x2='';
                  }else if($pregunta_39==2){
                    $pregunta_39x1='';
                    $pregunta_39x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 39 -->
                  <div class="col-md-10">
                    <label>El cliente vende padecería de metales preciosos </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_39" name="pregunta_39" id="39" value="1" <?php echo $pregunta_39x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_39" name="pregunta_39" value="2" <?php echo $pregunta_39x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              
              <?php } ?>
              <!-- termina actividad 6 -->

              <?php if($id_actividad==10){ ?>
              <!-- actividad 7 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>La operación se lleva a cabo a un valor de compra o venta significativamente diferente, a partir del precio estimado de la obra </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                
              <?php } ?>
              <!-- termina actividad 7 -->

              <?php if($id_actividad==11){ ?>
              <!-- actividad 8 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>El cliente solicita el reembolso del pago del vehículo poco tiempo después de ser adquirido </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
        
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cliente compra  múltiples vehículos en un periodo muy corto de tiempo, sin tener la preocupación sobre el costo, condiciones o tipo</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>El cliente vende su vehículo a precios muy por debajo del precio de mercado</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2.5" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label> El cliente registra el mismo domicilio que otros clientes sin que exista relación aparente entre ellos</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_39x1='';
                  $pregunta_39x2='';
                  if($pregunta_39==1){
                    $pregunta_39x1='checked';
                    $pregunta_39x2='';
                  }else if($pregunta_39==2){
                    $pregunta_39x1='';
                    $pregunta_39x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 39 -->
                  <div class="col-md-10">
                    <label> El cliente es menor de edad y no cuenta con la capacidad de decisión ni la documentación necesaria para realizar la operación </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_39" name="pregunta_39" id="39" value="1" <?php echo $pregunta_39x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_39" name="pregunta_39" value="2" <?php echo $pregunta_39x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_40x1='';
                  $pregunta_40x2='';
                  if($pregunta_40==1){
                    $pregunta_40x1='checked';
                    $pregunta_40x2='';
                  }else if($pregunta_40==2){
                    $pregunta_40x1='';
                    $pregunta_40x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 40 -->
                  <div class="col-md-10">
                    <label> Hay indicios o certeza de que los vehículos adquiridos son para exportación</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_40" name="pregunta_40" id="40" value="1" <?php echo $pregunta_40x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_40" name="pregunta_40" value="2" <?php echo $pregunta_40x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 8 -->

              <?php if($id_actividad==12){ ?>
              <!-- actividad 9 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>El cliente contrata el servicio vía remota y se dan justificaciones para evitar tener contacto con el personal de la empresa de blindaje </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2.5" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
        
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cliente solicita el blindaje de bienes muebles o partes de inmuebles poco comunes o de poco valor</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>          
              <?php } ?>
              <!-- termina actividad 9 -->

              <?php if($id_actividad==13){ ?>
              <!-- actividad 10 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>El cliente se rehúsa a revelar cuáles son los bienes transportados o custodiados </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>No se cuenta con seguro para proteger los bienes custodiados o trasladados</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>          
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>Custodia o traslado de valores poco comunes que no coinciden con la actividad económica del cliente</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>          
              <?php } ?>
              <!-- termina actividad 10 -->

              <?php if($id_actividad==14){ ?>
              <!-- actividad 11 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>El cliente solicita que los honorarios del servicio sean pagados por un tercero que no participó en la operación </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cliente es extranjero y aparentemente no cuenta con transacciones/negocios significativos en el país y solicita el apoyo de servicios profesionales</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>El cliente es el firmante de las cuentas de la persona moral sin que exista causa que justifique el servicio</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label> Hay indicios de que posterior a la constitución de la empresa, hubo un periodo largo de inactividad seguido de un aumento repentino e inexplicable de las actividades financieras  </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_39x1='';
                  $pregunta_39x2='';
                  if($pregunta_39==1){
                    $pregunta_39x1='checked';
                    $pregunta_39x2='';
                  }else if($pregunta_39==2){
                    $pregunta_39x1='';
                    $pregunta_39x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 39 -->
                  <div class="col-md-10">
                    <label> El cliente es una persona moral que se describe como un negocio comercial, sin embargo no es posible corroborar esta información en fuentes abiertas</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_39" name="pregunta_39" id="39" value="1" <?php echo $pregunta_39x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_39" name="pregunta_39" value="2" <?php echo $pregunta_39x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_40x1='';
                  $pregunta_40x2='';
                  if($pregunta_40==1){
                    $pregunta_40x1='checked';
                    $pregunta_40x2='';
                  }else if($pregunta_40==2){
                    $pregunta_40x1='';
                    $pregunta_40x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 40 -->
                  <div class="col-md-10">
                    <label>Se tiene una cantidad inusualmente grande de beneficiarios y participantes mayoritarios </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="2.5" class="form-check-input pregunta_40" name="pregunta_40" id="40" value="1" <?php echo $pregunta_40x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_40" name="pregunta_40" value="2" <?php echo $pregunta_40x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_41x1='';
                  $pregunta_41x2='';
                  if($pregunta_41==1){
                    $pregunta_41x1='checked';
                    $pregunta_41x2='';
                  }else if($pregunta_41==2){
                    $pregunta_41x1='';
                    $pregunta_41x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 41 -->
                  <div class="col-md-10">
                    <label>La administración de recursos del cliente permite identificar que usa múltiples cuentas bancarias sin justificación aparente </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_41" name="pregunta_41" id="41" value="1" <?php echo $pregunta_41x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_41" name="pregunta_41" value="2" <?php echo $pregunta_41x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_42x1='';
                  $pregunta_42x2='';
                  if($pregunta_42==1){
                    $pregunta_42x1='checked';
                    $pregunta_42x2='';
                  }else if($pregunta_42==2){
                    $pregunta_42x1='';
                    $pregunta_42x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 42 -->
                  <div class="col-md-10">
                    <label>El cliente no  muestra interés en la estructura de la empresa que se está constituyendo </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_42" name="pregunta_42" id="42" value="1" <?php echo $pregunta_42x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_42" name="pregunta_42" value="2" <?php echo $pregunta_42x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_43x1='';
                  $pregunta_43x2='';
                  if($pregunta_43==1){
                    $pregunta_43x1='checked';
                    $pregunta_43x2='';
                  }else if($pregunta_43==2){
                    $pregunta_43x1='';
                    $pregunta_43x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 43 -->
                  <div class="col-md-10">
                    <label>Se tienen indicios de que el cliente involucra a múltiples profesionales para facilitar aspectos de una transacción sin una razón que lo justifique </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_43" name="pregunta_43" id="43" value="1" <?php echo $pregunta_43x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_43" name="pregunta_43" value="2" <?php echo $pregunta_43x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_44x1='';
                  $pregunta_44x2='';
                  if($pregunta_44==1){
                    $pregunta_44x1='checked';
                    $pregunta_44x2='';
                  }else if($pregunta_44==2){
                    $pregunta_44x1='';
                    $pregunta_44x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 44 -->
                  <div class="col-md-10">
                    <label>La operación involucra a dos personas morales con directores, accionistas o beneficiarios finales similares</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_44" name="pregunta_44" id="44" value="1" <?php echo $pregunta_44x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_44" name="pregunta_44" value="2" <?php echo $pregunta_44x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_45x1='';
                  $pregunta_45x2='';
                  if($pregunta_45==1){
                    $pregunta_45x1='checked';
                    $pregunta_45x2='';
                  }else if($pregunta_45==2){
                    $pregunta_45x1='';
                    $pregunta_45x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 45 -->
                  <div class="col-md-10">
                    <label>El cliente (persona moral) está registrado con un nombre que indica actividades o servicios distintos a los que realiza</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_45" name="pregunta_45" id="45" value="1" <?php echo $pregunta_45x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_45" name="pregunta_45" value="2" <?php echo $pregunta_45x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 11 -->

              <?php if($id_actividad==17){ ?>
              <!-- actividad 13 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label> El cliente condiciona o pretende condicionar el otorgamiento del donativo a la imposición de ciertas condiciones poco usuales para el uso del mismo </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cliente otorga uno o varios donativos por un monto elevado sin mostrar interés en el objeto de la organización</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>El cliente intenta o se realiza un donativo en especie de bienes poco usuales o sin relación con el objeto de la organización</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label>Hay indicios, o certeza, de que los bienes donados provienen de actividades ilícitas</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_39x1='';
                  $pregunta_39x2='';
                  if($pregunta_39==1){
                    $pregunta_39x1='checked';
                    $pregunta_39x2='';
                  }else if($pregunta_39==2){
                    $pregunta_39x1='';
                    $pregunta_39x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 39 -->
                  <div class="col-md-10">
                    <label> El cliente realiza un elevado número de donaciones en efectivo y en cantidades elevadas mensualmente, o en periodos identificablemente cortos </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_39" name="pregunta_39" id="39" value="1" <?php echo $pregunta_39x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_39" name="pregunta_39" value="2" <?php echo $pregunta_39x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 13 -->

              <?php if($id_actividad==19){ ?>
              <!-- actividad 15 -->
                <?php
                  $pregunta_35x1='';
                  $pregunta_35x2='';
                  if($pregunta_35==1){
                    $pregunta_35x1='checked';
                    $pregunta_35x2='';
                  }else if($pregunta_35==2){
                    $pregunta_35x1='';
                    $pregunta_35x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 35 -->
                  <div class="col-md-10">
                    <label>  El cliente paga o ofrece pagar en efectivo, y por adelantado, las rentas correspondientes a un periodo largo de tiempo sin justificación lógica para ello </label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_35" name="pregunta_35" id="35" value="1" <?php echo $pregunta_35x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_35" name="pregunta_35" value="2" <?php echo $pregunta_35x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_36x1='';
                  $pregunta_36x2='';
                  if($pregunta_36==1){
                    $pregunta_36x1='checked';
                    $pregunta_36x2='';
                  }else if($pregunta_36==2){
                    $pregunta_36x1='';
                    $pregunta_36x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 36 -->
                  <div class="col-md-10">
                    <label>El cliente paga por adelantado las rentas de un periodo largo de tiempo en efectivo y posteriormente pide la cancelación del contrato y solicita el reembolso del monto pagado por medio de otro instrumento monetario diferente al efectivo</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_36" name="pregunta_36" id="36" value="1" <?php echo $pregunta_36x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_36" name="pregunta_36" value="2" <?php echo $pregunta_36x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_37x1='';
                  $pregunta_37x2='';
                  if($pregunta_37==1){
                    $pregunta_37x1='checked';
                    $pregunta_37x2='';
                  }else if($pregunta_37==2){
                    $pregunta_37x1='';
                    $pregunta_37x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 37 -->
                  <div class="col-md-10">
                    <label>El pago de las rentas del inmueble es realizado por un tercero sin relación aparente con el cliente</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_37" name="pregunta_37" id="37" value="1" <?php echo $pregunta_37x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_37" name="pregunta_37" value="2" <?php echo $pregunta_37x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_38x1='';
                  $pregunta_38x2='';
                  if($pregunta_38==1){
                    $pregunta_38x1='checked';
                    $pregunta_38x2='';
                  }else if($pregunta_38==2){
                    $pregunta_38x1='';
                    $pregunta_38x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 38 -->
                  <div class="col-md-10">
                    <label>Hay indicios, o certeza, de que el inmueble arrendado no está siendo utilizado para el propósito expresado</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_38" name="pregunta_38" id="38" value="1" <?php echo $pregunta_38x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_38" name="pregunta_38" value="2" <?php echo $pregunta_38x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_39x1='';
                  $pregunta_39x2='';
                  if($pregunta_39==1){
                    $pregunta_39x1='checked';
                    $pregunta_39x2='';
                  }else if($pregunta_39==2){
                    $pregunta_39x1='';
                    $pregunta_39x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 39 -->
                  <div class="col-md-10">
                    <label>El cliente no muestra tener interés en las características del inmueble objeto de arrendamiento, o en el monto de la renta y condiciones del contrato</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_39" name="pregunta_39" id="39" value="1" <?php echo $pregunta_39x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_39" name="pregunta_39" value="2" <?php echo $pregunta_39x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_40x1='';
                  $pregunta_40x2='';
                  if($pregunta_40==1){
                    $pregunta_40x1='checked';
                    $pregunta_40x2='';
                  }else if($pregunta_40==2){
                    $pregunta_40x1='';
                    $pregunta_40x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 40 -->
                  <div class="col-md-10">
                    <label>El cliente ofrece pagar un monto de arrendamiento superior al solicitado con la finalidad de que la operación se realice fuera de los parámetros establecidos</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_40" name="pregunta_40" id="40" value="1" <?php echo $pregunta_40x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_40" name="pregunta_40" value="2" <?php echo $pregunta_40x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_41x1='';
                  $pregunta_41x2='';
                  if($pregunta_41==1){
                    $pregunta_41x1='checked';
                    $pregunta_41x2='';
                  }else if($pregunta_41==2){
                    $pregunta_41x1='';
                    $pregunta_41x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 40 -->
                  <div class="col-md-10">
                    <label>Se tiene sospecha de que el inmueble es utilizado como centro que brinda ayuda a la comunidad o grupos vulnerables</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_41" name="pregunta_41" id="41" value="1" <?php echo $pregunta_41x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_41" name="pregunta_41" value="2" <?php echo $pregunta_41x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
                <?php
                  $pregunta_42x1='';
                  $pregunta_42x2='';
                  if($pregunta_42==1){
                    $pregunta_42x1='checked';
                    $pregunta_42x2='';
                  }else if($pregunta_42==2){
                    $pregunta_42x1='';
                    $pregunta_42x2='checked';
                  }
                ?>
                <div class="row"><!-- pregunta 40 -->
                  <div class="col-md-10">
                    <label>Un mismo representante legal solicita arrendamiento a nombre de distintas personas (físicas o morales) que no pertenecen al mismo grupo</label>
                  </div>  
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="3" class="form-check-input pregunta_42" name="pregunta_42" id="42" value="1" <?php echo $pregunta_42x1 ?>>
                        SI
                      </label>
                    </div>
                  </div>
                  <div class="col-md-1 form-group">
                    <div class="form-check form-check-success">
                      <label class="form-check-label">
                        <input type="radio" data-valord="1" class="form-check-input pregunta_42" name="pregunta_42" value="2" <?php echo $pregunta_42x2 ?>>
                        NO
                      </label>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <!-- termina actividad 15 -->
              </div>
            </div>

            
          <?php } ?>

          <div class="col-md-12 form-group">
            <label>Conclusiones:</label><br>
            <textarea name="conclusiones" class="form-control"><?php echo $conclusiones; ?></textarea>
          </div> 
          <div class="col-md-12 form-group" id="cont_conc_dir">
            <label>Conclusiones de Director:</label><br>
            <textarea name="conclusiones_dir" class="form-control"><?php echo $conclusiones_dir; ?></textarea>
          </div>

          <?php /* if(isset($grado[1]) && $avanza==0 && $grado[1]>2.0 || isset($grado[2]) && $grado[2]>2.0  && $avanza==0 || isset($grado[1]) && $grado[1]>2.0  && !isset($avanza)){ //debe ser por grado de guia de alertas ?>
            <div class="col-md-12 form-group">
              <br>
              <a title="Autorizar continuidad de operación" href="#modal_autoriza" data-toggle="modal"><button type="button" class="btn gradient_nepal2" id="ver_autoriza"><i class="fa fa-arrow-left"></i> Autorizar</button></a>
            </div>
          <?php } */ ?>
          <?php if($avanza==0 && !isset($avanza)) //debe ser por grado de guia de alertas 
                  $avanza=0;
          ?>
          <input type="hidden" id="val_avanza" value="<?php echo $avanza; ?>">
          <div class="col-md-12 form-group" id="cont_btn_auto" style="display: none;">
            <br>
            <a title="Autorizar continuidad de operación" href="#modal_autoriza" data-toggle="modal"><button type="button" class="btn gradient_nepal2" id="ver_autoriza"><i class="fa fa-arrow-left"></i> Autorizar</button></a>
          </div>

          <?php if($ver_diag_pep==0 && $pep_comp==0) { ?>
            <div class="row d-flex flex-row justify-content-center alig-items-center">
              <div class="col-md-6 grid-margin stretch-card ">
                <div class="card">
                  <div class="card-body">
                    <input type="hidden" id="calificacion" name="calificacion" value="0">
                    <h2 align="center" id="cal_txt"></h2>
                    <div class="template-demo">
                      <div class="progress progress-sm" id="cont_barra" style="height: 20px;">
                        <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 1%" aria-valuenow="1" aria-valuemin="1" aria-valuemax="1"></div><p style="font-size:17px"><b>0.00</b></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>


        </form> 
        <div class="texto_firma" style="display: none">
          <div class="row">
            <div class="col-md-12">
              <h6 style="color: black;">Revisado por: Firma de ____________________________________________________________________</h6>
              
            </div>
          </div>
          <br>
          <br>
          <br>
          <br>
          <div class="row texto_firma">
            <div class="col-md-12">
              <h6 style="color: black;">Autorizado por: Firma de ___________________________________________________________________</h6>
            </div>
          </div>

          <br>
        </div> 
        <div class="btn_guardar_text">
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<button type="button" class="btn gradient_nepal2" onclick="imprimir_for()"><i class="fa fa-print"></i> Imprimir formato</button>-->
              <button type="button" class="btn gradient_nepal2" onclick="guardar_diagnostico_alertas()"><i class="fa  fa-floppy-o" ></i> Guardar</button>
            </div>
          </div> 
        </div>  
      </div>
    </div>
  </div>
</div>         

<div class="modal fade" id="modalimprimir" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" align="center">
            <label>¿Deseas imprimir el formulario?                
            </label>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4 form-group">
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="1">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="2" checked>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_registro" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_autoriza" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Autorización</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5><strong>Ingresa los datos para autorizar la continuidad de la operación</strong></h5>
        <input class="form-control" type="hidden" id="id_autorizado" value="0">
        <div class="row">
          <div class="col-md-12 form-group">
            <label> <i class="fa fa-user"></i> Usuario:</label>
            <input autocomplete="off" class="form-control" type="text" name="Usuario" id="usuario">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-group">
            <label><i class="fa fa-key"></i> Contraseña:</label>
            <input autocomplete="off" class="form-control" type="password" name="contrasena" id="contrasena">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-group">
            <label>Motivo de autorización:</label>
            <input class="form-control" type="text" name="motivo" id="motivo">
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="aceptarValida()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>