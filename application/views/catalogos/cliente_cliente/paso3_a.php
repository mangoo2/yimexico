<hr class="subtitle"> 
<!--<div class="row">
  <div class="col-md-12 form-group">
    <label>Razón social:</label>
    <input class="form-control" type="text" name="" value="">
  </div>
</div> -->
<!--<div class="row">
  <div class="col-md-12">    
    <h3>IDENTIFICACIÓN DEL BENEFICIARIO CONTROLADOR</h3>
  </div>
</div> 
<div class="row">
  <div class="col-md-12 form-group">
    <div class="form-check form-check-primary">
      <label class="form-check-label">A) Nombrar a la(s) persona(s) fisica(s) que directa o indirectamente, posea el 25% o más de la composición accionaria o dl capital social de la persona moral.   
        <input type="radio" checked class="form-check-input" id="accionaria_socio_a" name="tipo_inciso" value="a">
      </label>
    </div>
  </div>
</div>-->
<div class="row accion_socio_a" style="display: none; border: 1px solid black; border-radius: 10px; box-shadow: 2px 2px 10px #666;">
	<div class="col-md-12">
		<!-- -->
		<div class="row">
			<div class="col-md-12">
				<br>
				<h5>Socio 1</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<hr class="subtitle"> 
			</div>
		</div>
        <div class="row">
	        <div class="col-md-6 form-group">
	          <div class="form-check form-check-success" hidden>
	            <label class="form-check-label">
	              <input type="radio" checked class="form-check-input" name="tipo_personaA" id="tipo_persona_mfs1">
	              Persona fisica
	            </label>
	          </div>
	          <div class="form-check form-check-success" hidden>
	            <label class="form-check-label">
	              <input type="radio" class="form-check-input" name="tipo_personaA" id="tipo_persona_mms1">
	              Persona moral
	            </label>
	          </div>
	        </div>
	        <div class="col-md-2 form-group"></div>
	        <div class="col-md-4 form-group">
	           <label>% de acciones:</label>
               <input class="form-control" type="text" id="porcentaje1" name="porcentaje" required>
	        </div>
		</div>
		<form class="form" method="post" role="form" id="form_bene_moral_fisica_s1">     
			<div class="pers_fisica_s1">
		        <div class="row"><!-- PERSONA FISICA -->
		          <div class="col-md-4 form-group">
		            <label>Nombre(s):</label>
		            <input class="form-control" type="text" name="nombre" required>
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido paterno:</label>
		            <input class="form-control" type="text" name="apellido_paterno" required>
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido materno:</label>
		            <input class="form-control" type="text" name="apellido_materno" required>
		          </div>
		        </div>
		        <div class="row">
			      <div class="col-md-4 form-group">
			        <label>Nombre de identificación:</label>
			        <input class="form-control" type="text" name="nombre_identificacion" required>
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Autoridad que emite la identificación:</label>
			        <input class="form-control" type="text" name="autoridad_emite" required>
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Número de identificación:</label>
			        <input class="form-control" type="text" name="numero_identificacion" required>
			      </div>
			    </div>
		        <div class="row">
		        	<div class="col-md-4">
				        <label>Género:</label>
				        <div class="row">
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generom1">
				                Masculino
				              </label>
				            </div>
				          </div>
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generof1">
				                Femenino
				              </label>
				            </div>
				          </div>
				        </div>
				    </div>    
		        </div>
		        <div class="row">
				  <div class="col-md-4 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" required>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País de nacimiento:</label><br>
				    <select class="form-control pais1" id="pais" name="pais_nacimiento" onclick="getpais(1)" required>
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>Pais de nacionalidad:</label><br>
				    <select class="form-control pais2" id="pais" name="pais_nacionalidad" onclick="getpais(2)" required>
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" required>
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext" required>
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio"required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Estado:</label>
				    <input readonly class="form-control estado1" type="hidden" name="estado" required>
				    <span class="span_div_3"></span>
				  </div>
				    <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" required>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais3" id="pais" name="pais" onclick="getpais2(3)" required>
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12 form-group">
				    <div class="form-check form-check-primary">
				      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
				        <input type="checkbox" class="form-check-input" name="residencia_extran" id="lugar_recidencia_socio" onchange="lugar_recidencia_btn_socio()">
				      </label>
				    </div>
				  </div>
				</div>
		        <div class="residencia_extranjero" style="display: none">
				  <div class="row">
				    <div class="col-md-12">
				      <span>Dirección en México.</span>
				    </div>
				  </div><br>
				  <div class="row">
				    <div class="col-md-2 form-group">
				       <label>Tipo de vialidad:</label>
				        <select class="form-control" name="tipo_vialidad_t">
	                      <option disabled selected>Selecciona un tipo</option>
	                      <?php foreach ($get_tipo_vialidad as $item){ ?>
	                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
	                      <?php } ?>
	                    </select>
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Calle:</label>
				      <input class="form-control" type="text" name="calle_t" value="">
				    </div>
				    <div class="col-md-2 form-group">
				      <label>No.ext:</label>
				      <input class="form-control" type="number" name="no_ext_t" value="">
				    </div>
				    <div class="col-md-2 form-group">
				      <label>No.int:</label>
				      <input class="form-control" type="number" name="no_int_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Colonia:</label>
				      <input class="form-control" type="text" name="colonia_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Municipio o delegación:</label>
				      <input class="form-control" type="text" name="municipio_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Localidad:</label>
				      <input class="form-control" type="text" name="localidad_t" value="">
				    </div>
				    <!--<div class="col-md-3 form-group estado2">
				      <label>Estado:</label>
				      <input class="form-control" type="text" name="estado_t" value="">
				    </div>-->
				    <div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado_t">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>
				    <div class="col-md-3 form-group">
				      <label>C.P:</label>
				      <input class="form-control" type="text" name="cp_t" value="">
				    </div>
				  </div>
				  <div class="row">
				    <div class="col-md-6 form-group">
				      <label>Correo electrónico:</label>
				      <input class="form-control" type="email" name="correo" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>R.F.C:</label>
				      <input class="form-control" type="text" name="r_f_c" value=""><span>*Cuando cuente con R.F.C</span>
				    </div>
				    <div class="col-md-3 form-group">
				      <label>CURP:</label>
				      <input class="form-control" type="text" name="curp" value="">
				      <span>*Cuando cuente con CURP</span>
				    </div>
				  </div>
				</div> <!-- TERMINA PERSONA FISICA -->
			</div>
		</form>
		
		<div class="pers_moral_s1" style="display: none">
			<form class="form" method="post" role="form" id="form_bene_moral_moral_s1"> 
				<!-- PERSONA MORAL 1-->
				<div class="row">
				  <div class="col-md-12 form-group">
				    <label>Razón social:</label>
				    <input class="form-control" type="text" name="razon_social" value="" required>
				  </div>
				</div> 
				<div class="row">
				  <div class="col-md-12">
				    <span>Clave del registro federal de contribuyentes con homoclave / # identificacion fiscal en otro país (cuando aplique) y país que la asigno.</span>
				  </div>
				</div><br>
				<div class="row">
				  <div class="col-md-6 form-group">
				    <input class="form-control" type="text" name="clave_registro" required>
				  </div>
				  <div class="col-md-3">
				    <div class="form-check form-check-flat form-check-primary">
				      <label class="form-check-label">
				        <input type="checkbox" class="form-check-input" id="extranjero" name="extranjero">
				        Es extranjero
				      </label>
				    </div>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Pais:</label><br>
				    <select class="form-control pais4" id="pais" name="pais" onclick="getpais(4)" required>
				       <option value="MX">MEXICO</option>
				    </select>
				  </div> 
				  <div class="col-md-3 form-group">
				    <label>Nacionalidad:</label><br>
				    <select class="form-control pais5" name="nacionalidad" onclick="getpais(5)" id="pais" required>
				       <option value="MX">MEXICO</option>
				    </select>
				  </div>  
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" required>
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" required>
				  </div>
				  <div class="col-md-3 form-group estado2">
				    <label>Estado:</label>
				    <input class="form-control" type="hidden" name="estado" required>
				    <span class="span_div_6"></span>
				  </div>
				    <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" required>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais6" name="pais_d" id="pais2" onclick="getpais2(6)" required>
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
		          <div class="col-md-3 form-group">
				    <label>Número de teléfono:</label>
				    <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="telefono" value="">
				  </div>
				  <div class="col-md-5 form-group">
				    <label>Correo electrónico:</label>
				    <input class="form-control" type="email" name="correo" value="">
				  </div>	
		        </div>
		        <div class="row">
				  <div class="col-md-3 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_constitucion" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Número de acta constitutiva:</label>
				    <input class="form-control" type="number" name="numero_acta_constitutiva"required>
				  </div>
				  <div class="col-md-6 form-group">
				    <label>Nombre del notario:</label>
				    <input class="form-control" type="text" name="nombre_notario" required>
				  </div>
				</div>
			</form>		
		        <hr class="subtitle"> <!-- COMIENZA ESTRUCTURA DE SOCIO 1 PERS MORAL-->
		        <div class="row">
		        	<div class="col-md-12" align="center">
		        		<span>Definir estructura accionaria</span>
		        	</div>
		        </div>
		    <form class="form" method="post" role="form" id="form_bene_moral_fisica_s12"> 
		        <div class="row"><!-- SOCIO 1 PERSONA FISICA -->
		        	<div class="col-md-8">
		        	  <p>Socio 1:</p>	
		        	</div>
		            <div class="col-md-4">
		              <label>% de acciones:</label>
		              <input class="form-control" type="text" id="porcentaje2" name="porcentaje" required>
		            </div>	
		        </div>
		        <div class="row">
		          <div class="col-md-4 form-group">
		            <label>Nombre(s):</label>
		            <input class="form-control" type="text" name="nombre" required>
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido paterno:</label>
		            <input class="form-control" type="text" name="apellido_paterno" required>
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido materno:</label>
		            <input class="form-control" type="text" name="apellido_materno" required>
		          </div>
		        </div>
		        <div class="row">
			      <div class="col-md-4 form-group">
			        <label>Nombre de identificación:</label>
			        <input class="form-control" type="text" name="nombre_identificacion" required>
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Autoridad que emite la identificación:</label>
			        <input class="form-control" type="text" name="autoridad_emite" required>
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Número de identificación:</label>
			        <input class="form-control" type="text" name="numero_identificacion" required>
			      </div>
			    </div> 
		        <div class="row">
		        	<div class="col-md-4">
				        <label>Género:</label>
				        <div class="row">
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generom2">
				                Masculino
				              </label>
				            </div>
				          </div>
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generof2">
				                Femenino
				              </label>
				            </div>
				          </div>
				        </div>
				    </div>    
		        </div>
		        <div class="row">
				  <div class="col-md-4 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" required>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País de nacimiento:</label><br>
				    <select class="form-control pais7" id="pais" name="pais_nacimiento" onclick="getpais(7)" required>
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>Pais de nacionalidad:</label><br>
				    <select class="form-control pais8" id="pais" name="pais_nacionalidad" onclick="getpais(8)" required>
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" required>
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext" required>
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio" required>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" required>
				  </div>
				  <div class="col-md-3 form-group estado3">
				    <label>Estado:</label>
				    <input class="form-control" type="hidden" name="estado" required>
				    <span class="span_div_9"></span>
				  </div>
				    <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" required>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais9" id="pais3" name="pais" onclick="getpais2(9)">
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12 form-group">
				    <div class="form-check form-check-primary">
				      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
				        <input type="checkbox" class="form-check-input" name="residencia_extran" id="lugar_recidencia_socio_2_a" onchange="lugar_recidencia_btn_socio_2_accion_a()">
				      </label>
				    </div>
				  </div>
				</div>
		        <div class="residencia_extranjero_socio2_accion_a" style="display: none">
					<div class="row">
					    <div class="col-md-12">
					      <span>Dirección en México.</span>
					    </div>
					</div><br>
					<div class="row">
					    <div class="col-md-2 form-group">
					        <label>Tipo de vialidad:</label>
					        <select class="form-control" name="tipo_vialidad_t">
		                      <option disabled selected>Selecciona un tipo</option>
		                      <?php foreach ($get_tipo_vialidad as $item){ ?>
		                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
		                      <?php } ?>
		                    </select>
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Calle:</label>
					      <input class="form-control" type="text" name="calle_t" value="">
					    </div>
					    <div class="col-md-2 form-group">
					      <label>No.ext:</label>
					      <input class="form-control" type="number" name="no_ext_t" value="">
					    </div>
					    <div class="col-md-2 form-group">
					      <label>No.int:</label>
					      <input class="form-control" type="number" name="no_int_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Colonia:</label>
					      <input class="form-control" type="text" name="colonia_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Municipio o delegación:</label>
					      <input class="form-control" type="text" name="municipio_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Localidad:</label>
					      <input class="form-control" type="text" name="localidad_t" value="">
					    </div>
					    <!--<div class="col-md-3 form-group">
					      <label>Estado:</label>
					      <input class="form-control" type="text" name="estado_t" value="">
					    </div>-->
					    <div class="col-md-3 form-group">
						    <label>Estado:</label><br>
						    <select class="form-control" name="estado_t">
			                    <?php foreach ($get_estado as $e) {
			                      echo "<option value='$e->clave'>$e->estado</option>";
			                    } ?>
			                  </select>
						</div>
					    <div class="col-md-3 form-group">
					      <label>C.P:</label>
					      <input class="form-control" type="text" name="cp_t" value="">
					    </div>
					</div>
					<div class="row">
					    <div class="col-md-6 form-group">
					      <label>Correo electrónico:</label>
					      <input class="form-control" type="email" name="correo" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>R.F.C:</label>
					      <input class="form-control" type="text" name="r_f_c" value=""><span>*Cuando cuente con R.F.C</span>
					    </div>
					    <div class="col-md-3 form-group">
					      <label>CURP:</label>
					      <input class="form-control" type="text" name="curp" value="">
					      <span>*Cuando cuente con CURP</span>
					    </div>
					</div>
				</div>
			</form>
				<br>
				<hr class="subtitle">
			<form class="form" method="post" role="form" id="form_bene_fisica_fisica_s12_2"> 
				<div class="row"><!-- SOCIO PERSONA FISICA 2 -->
			        <div class="col-md-6 form-group">
			          <p>Socio 2:</p>
			        </div>
			        <div class="col-md-2 form-group"></div>
			        <div class="col-md-4 form-group">
			           <label>% de acciones:</label>
		               <input class="form-control" type="text" name="porcentaje" id="porcentaje3" value="">
			        </div>
				</div>
				<div class="row">
		          <div class="col-md-4 form-group">
		            <label>Nombre(s):</label>
		            <input class="form-control" type="text" name="nombre" value="">
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido paterno:</label>
		            <input class="form-control" type="text" name="apellido_paterno" value="">
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido materno:</label>
		            <input class="form-control" type="text" name="apellido_materno" value="">
		          </div>
		        </div>
		        <div class="row">
			      <div class="col-md-4 form-group">
			        <label>Nombre de identificación:</label>
			        <input class="form-control" type="text" name="nombre_identificacion" value="">
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Autoridad que emite la identificación:</label>
			        <input class="form-control" type="text" name="autoridad_emite" value="">
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Número de identificación:</label>
			        <input class="form-control" type="text" name="numero_identificacion" value="">
			      </div>
			    </div>
		        <div class="row">
		        	<div class="col-md-4">
				        <label>Género:</label>
				        <div class="row">
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generom3" value="m">
				                Masculino
				              </label>
				            </div>
				          </div>
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generof3" value="f">
				                Femenino
				              </label>
				            </div>
				          </div>
				        </div>
				    </div>    
		        </div>
		        <div class="row">
				  <div class="col-md-4 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País de nacimiento:</label><br>
				    <select class="form-control pais10" id="pais" name="pais_nacimiento" onclick="getpais(10)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>Pais de nacionalidad:</label><br>
				    <select class="form-control pais11" id="pais" name="pais_nacionalidad" onclick="getpais(11)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" value="">
				  </div>
				  <div class="col-md-3 form-group estado4">
				    <label>Estado:</label>
				    <input class="form-control" type="hidden" name="estado" value="">
				    <span class="span_div_12"></span>
				  </div>
				    <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais12" id="pais" name="pais" onclick="getpais2(12)">
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12 form-group">
				    <div class="form-check form-check-primary">
				      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
				        <input type="checkbox" class="form-check-input" name="residencia_extran" id="lugar_recidencia_socio2_accion_a" onchange="lugar_recidencia_btn_socio_accion_a_socio2()">
				      </label>
				    </div>
				  </div>
				</div>
		        <div class="residencia_extranjero_accion_a_socio2" style="display: none">
				  	<div class="row">
					    <div class="col-md-12">
					      <span>Dirección en México.</span>
					    </div>
				  	</div><br>
					<div class="row">
					    <div class="col-md-2 form-group">
					        <label>Tipo de vialidad:</label>
					        <select class="form-control" name="tipo_vialidad_t">
		                      <option disabled selected>Selecciona un tipo</option>
		                      <?php foreach ($get_tipo_vialidad as $item){ ?>
		                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
		                      <?php } ?>
		                    </select>
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Calle:</label>
					      <input class="form-control" type="text" name="calle_t" value="">
					    </div>
					    <div class="col-md-2 form-group">
					      <label>No.ext:</label>
					      <input class="form-control" type="number" name="no_ext_t" value="">
					    </div>
					    <div class="col-md-2 form-group">
					      <label>No.int:</label>
					      <input class="form-control" type="number" name="no_int_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Colonia:</label>
					      <input class="form-control" type="text" name="colonia_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Municipio o delegación:</label>
					      <input class="form-control" type="text" name="municipio_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Localidad:</label>
					      <input class="form-control" type="text" name="localidad_t" value="">
					    </div>
					    <!--<div class="col-md-3 form-group">
					      <label>Estado:</label>
					      <input class="form-control" type="text" name="estado_t" value="">
					    </div>-->
					    <div class="col-md-3 form-group">
						    <label>Estado:</label><br>
						    <select class="form-control" name="estado_t">
			                    <?php foreach ($get_estado as $e) {
			                      echo "<option value='$e->clave'>$e->estado</option>";
			                    } ?>
			                  </select>
						</div>
					    <div class="col-md-3 form-group">
					      <label>C.P:</label>
					      <input class="form-control" type="text" name="cp_t" value="">
					    </div>
					</div>
				  	<div class="row">
					    <div class="col-md-6 form-group">
					      <label>Correo electrónico:</label>
					      <input class="form-control" type="email" name="correo" value="">
					    </div>
				    <div class="col-md-3 form-group">
				      <label>R.F.C:</label>
				      <input class="form-control" type="text" name="r_f_c" value=""><span>*Cuando cuente con R.F.C</span>
				    </div>
				    <div class="col-md-3 form-group">
				      <label>CURP:</label>
				      <input class="form-control" type="text" name="curp" value="">
				      <span>*Cuando cuente con CURP</span>
				    </div>
				  </div>
				</div>
				<!-- TERMINA PERSONA FISICA 2 -->
			</form>	
		</div><!-- DIV SE SOCIOS 2-->
	</div>
</div> <!-- TERMINA ESTRUCTURA DEL SOCIO 1-->
<br>	

<!-- TERMINA ESTRUCTURA DEL SOCIO 2-->
<div class="row accion_socio_a" style="display: none; border: 1px solid black; border-radius: 10px; box-shadow: 2px 2px 10px #666;">
	<div class="col-md-12">
		<!-- -->
		<br>
		<div class="row">
			<div class="col-md-12">
				<h5>Socio 2</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<hr class="subtitle"> 
			</div>
		</div>
        <div class="row">
	        <div class="col-md-6 form-group">
	          <div class="form-check form-check-success" hidden>
	            <label class="form-check-label">
	              <input type="radio" checked class="form-check-input" name="tipo_persona2" id="tipo_persona_mfs2">
	              Persona fisica
	            </label>
	          </div>
	          <div class="form-check form-check-success" hidden>
	            <label class="form-check-label">
	              <input type="radio" class="form-check-input" name="tipo_persona2" id="tipo_persona_mms2">
	              Persona moral
	            </label>
	          </div>
	        </div>
	        <div class="col-md-2 form-group"></div> 
	        <div class="col-md-4 form-group">
	           <label>% de acciones:</label>
               <input class="form-control" type="text" name="porcentaje" id="porcentaje4" value="">
	        </div>
		</div>
		<form class="form" method="post" role="form" id="form_bene_moral_fisica_s2">
			<div class="pers_fisica_s2"><!-- PERSONA FISICA -->
		        <div class="row">
		          <div class="col-md-4 form-group">
		            <label>Nombre(s):</label>
		            <input class="form-control" type="text" name="nombre" >
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido paterno:</label>
		            <input class="form-control" type="text" name="apellido_paterno" >
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido materno:</label>
		            <input class="form-control" type="text" name="apellido_materno" >
		          </div>
		        </div>
		        <div class="row">
			      <div class="col-md-4 form-group">
			        <label>Nombre de identificación:</label>
			        <input class="form-control" type="text" name="nombre_identificacion">
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Autoridad que emite la identificación:</label>
			        <input class="form-control" type="text" name="autoridad_emite" >
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Número de identificación:</label>
			        <input class="form-control" type="text" name="numero_identificacion">
			      </div>
			    </div>
		        <div class="row">
		        	<div class="col-md-4">
				        <label>Género:</label>
				        <div class="row">
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generom4">
				                Masculino
				              </label>
				            </div>
				          </div>
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generof4">
				                Femenino
				              </label>
				            </div>
				          </div>
				        </div>
				    </div>    
		        </div>
		        <div class="row">
				  <div class="col-md-4 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País de nacimiento:</label><br>
				    <select class="form-control pais13" id="pais" name="pais_nacimiento" onclick="getpais(13)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>Pais de nacionalidad:</label><br>
				    <select class="form-control pais14" id="pais" name="pais_nacionalidad" onclick="getpais(14)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" value="">
				  </div>
				  <div class="col-md-3 form-group estado5">
				    <label>Estado:</label>
				    <input class="form-control" type="hidden" name="estado" value="">
				    <span class="span_div_14"></span>
				  </div>
				    <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais14" id="pais5" name="pais" onclick="getpais2(14)">
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12 form-group">
				    <div class="form-check form-check-primary">
				      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
				        <input type="checkbox" class="form-check-input" name="residencia_extran" id="lugar_recidencia_socio2" onchange="lugar_recidencia_btn_socio2()">
				      </label>
				    </div>
				  </div>
				</div>
		        <div class="residencia_extranjero2" style="display: none">
				  <div class="row">
				    <div class="col-md-12">
				      <span>Dirección en México.</span>
				    </div>
				  </div><br>
				  <div class="row">
				    <div class="col-md-2 form-group">
				        <label>Tipo de vialidad:</label>
				        <select class="form-control" name="tipo_vialidad_t">
	                      <option disabled selected>Selecciona un tipo</option>
	                      <?php foreach ($get_tipo_vialidad as $item){ ?>
	                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
	                      <?php } ?>
	                    </select>
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Calle:</label>
				      <input class="form-control" type="text" name="calle_t" value="">
				    </div>
				    <div class="col-md-2 form-group">
				      <label>No.ext:</label>
				      <input class="form-control" type="number" name="no_ext_t" value="">
				    </div>
				    <div class="col-md-2 form-group">
				      <label>No.int:</label>
				      <input class="form-control" type="number" name="no_int_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Colonia:</label>
				      <input class="form-control" type="text" name="colonia_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Municipio o delegación:</label>
				      <input class="form-control" type="text" name="municipio_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Localidad:</label>
				      <input class="form-control" type="text" name="localidad_t" value="">
				    </div>
				    <!--<div class="col-md-3 form-group">
				      <label>Estado:</label>
				      <input class="form-control" type="text" name="estado_t" value="">
				    </div>-->
				    <div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado_t">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>
				    <div class="col-md-3 form-group">
				      <label>C.P:</label>
				      <input class="form-control" type="text" name="cp_t" value="">
				    </div>
				  </div>
				  <div class="row">
				    <div class="col-md-6 form-group">
				      <label>Correo electrónico:</label>
				      <input class="form-control" type="email" name="correo" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>R.F.C:</label>
				      <input class="form-control" type="text" name="r_f_c" value=""><span>*Cuando cuente con R.F.C</span>
				    </div>
				    <div class="col-md-3 form-group">
				      <label>CURP:</label>
				      <input class="form-control" type="text" name="curp" value="">
				      <span>*Cuando cuente con CURP</span>
				    </div>
				  </div>
				</div> 
			</div><!-- TERMINA PERSONA FISICA -->
		</form>
		<div class="pers_moral_s2" style="display: none">
			<form class="form" method="post" role="form" id="form_bene_moral_moral_s2"> 
				<div class="row">
				  <div class="col-md-12 form-group">
				    <label>Razón social:</label>
				    <input class="form-control" type="text" name="razon_social" value="">
				  </div>
				</div> 
				<div class="row">
				  <div class="col-md-12">
				    <span>Clave del registro federal de contribuyentes con homoclave / # identificacion fiscal en otro país (cuando aplique) y país que la asigno.</span>
				  </div>
				</div><br>
				<div class="row">
				  <div class="col-md-6 form-group">
				    <input class="form-control" type="text" name="clave_registro" value="">
				  </div>
				  <div class="col-md-3">
				    <div class="form-check form-check-flat form-check-primary">
				      <label class="form-check-label">
				        <input type="checkbox" class="form-check-input" id="extranjero" name="extranjero">
				        Es extranjero
				      </label>
				    </div>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Pais:</label><br>
				    <select class="form-control pais15" name="pais" id="pais" onclick="getpais(15)">
				       <option value="MX">MEXICO</option>
				    </select>
				  </div> 
				  <div class="col-md-3 form-group">
				    <label>Nacionalidad:</label><br>
				    <select class="form-control pais16" name="nacionalidad" id="pais" onclick="getpais(16)">
				       <option value="MX">MEXICO</option>
				    </select>
				  </div>  
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" value="">
				  </div>
				  <div class="col-md-3 form-group estado6">
				    <label>Estado:</label>
				    <input class="form-control" type="hidden" name="estado" value="">
				    <span class="span_div_17"></span>
				  </div>
				    <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais17" id="pais6" onclick="getpais2(17)" name="pais_d">
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
		          <div class="col-md-3 form-group">
				    <label>Número de teléfono:</label>
				    <input class="form-control" type="text" placeholder="(Lada) + Teléfono" name="telefono" value="">
				  </div>
				  <div class="col-md-5 form-group">
				    <label>Correo electrónico:</label>
				    <input class="form-control" type="email" name="correo" value="">
				  </div>	
		        </div>
		        <div class="row">
				  <div class="col-md-3 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_constitucion" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Número de acta constitutiva:</label>
				    <input class="form-control" type="number" name="numero_acta_constitutiva" value="">
				  </div>
				  <div class="col-md-6 form-group">
				    <label>Nombre del notario:</label>
				    <input class="form-control" type="text" name="nombre_notario" value="">
				  </div>
				</div>
			</form>
	        <hr class="subtitle"> 
	        <div class="row">
	        	<div class="col-md-12" align="center">
	        		<span>Definir estructura accionaria</span>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-8">
	        	  <p>Socio 1:</p>	
	        	</div>
	            <div class="col-md-4">
	              <label>% de acciones:</label>
	              <input class="form-control" type="text" name="porcentaje" id="porcentaje5">
	            </div>	
	        </div>
	        <form class="form" method="post" role="form" id="form_bene_moral_fisica_s22">
		        <div class="row">
		          <div class="col-md-4 form-group">
		            <label>Nombre(s):</label>
		            <input class="form-control" type="text" name="nombre" value="">
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido paterno:</label>
		            <input class="form-control" type="text" name="apellido_paterno" value="">
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido materno:</label>
		            <input class="form-control" type="text" name="apellido_materno" value="">
		          </div>
		        </div>
		        <div class="row">
			      <div class="col-md-4 form-group">
			        <label>Nombre de identificación:</label>
			        <input class="form-control" type="text" name="nombre_identificacion" value="">
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Autoridad que emite la identificación:</label>
			        <input class="form-control" type="text" name="autoridad_emite" value="">
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Número de identificación:</label>
			        <input class="form-control" type="text" name="numero_identificacion" value="">
			      </div>
			    </div> 
		        <div class="row">
		        	<div class="col-md-4">
				        <label>Género:</label>
				        <div class="row">
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generom5">
				                Masculino
				              </label>
				            </div>
				          </div>
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generof5">
				                Femenino
				              </label>
				            </div>
				          </div>
				        </div>
				    </div>    
		        </div>
		        <div class="row">
				  <div class="col-md-4 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País de nacimiento:</label><br>
				    <select class="form-control pais18" id="pais" name="pais_nacimiento" onclick="getpais(18)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>Pais de nacionalidad:</label><br>
				    <select class="form-control pais19" id="pais" name="pais_nacionalidad" onclick="getpais(19)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Estado:</label>
				    <input class="form-control" type="hidden" name="estado" value="">
				    <span class="span_div_20"></span>
				  </div>
				  <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais20" id="pais" name="pais" onclick="getpais2(20)">
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12 form-group">
				    <div class="form-check form-check-primary">
				      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
				        <input type="checkbox" class="form-check-input" name="residencia_extran" id="lugar_recidencia_socio_2_2_a" onchange="lugar_recidencia_btn_socio_2_2_accion_a()">
				      </label>
				    </div>
				  </div>
				</div>
		        <div class="residencia_extranjero_socio2_2_accion_a" style="display: none">
					<div class="row">
					    <div class="col-md-12">
					      <span>Dirección en México.</span>
					    </div>
					</div><br>
					<div class="row">
					    <div class="col-md-2 form-group">
					       <label>Tipo de vialidad:</label>
					       <select class="form-control" name="tipo_vialidad_t">
		                      <option disabled selected>Selecciona un tipo</option>
		                      <?php foreach ($get_tipo_vialidad as $item){ ?>
		                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
		                      <?php } ?>
		                    </select>
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Calle:</label>
					      <input class="form-control" type="text" name="calle_t" value="">
					    </div>
					    <div class="col-md-2 form-group">
					      <label>No.ext:</label>
					      <input class="form-control" type="number" name="no_ext_t" value="">
					    </div>
					    <div class="col-md-2 form-group">
					      <label>No.int:</label>
					      <input class="form-control" type="number" name="no_int_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Colonia:</label>
					      <input class="form-control" type="text" name="colonia_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Municipio o delegación:</label>
					      <input class="form-control" type="text" name="municipio_t" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>Localidad:</label>
					      <input class="form-control" type="text" name="localidad_t" value="">
					    </div>
					    <!--<div class="col-md-3 form-group">
					      <label>Estado:</label>
					      <input class="form-control" type="text" name="estado_t" value="">
					    </div>-->
					    <div class="col-md-3 form-group">
						    <label>Estado:</label><br>
						    <select class="form-control" name="estado_t">
			                    <?php foreach ($get_estado as $e) {
			                      echo "<option value='$e->clave'>$e->estado</option>";
			                    } ?>
			                  </select>
						</div>
					    <div class="col-md-3 form-group">
					      <label>C.P:</label>
					      <input class="form-control" type="text" name="cp_t" value="">
					    </div>
					</div>
					<div class="row">
					    <div class="col-md-6 form-group">
					      <label>Correo electrónico:</label>
					      <input class="form-control" type="email" name="correo" value="">
					    </div>
					    <div class="col-md-3 form-group">
					      <label>R.F.C:</label>
					      <input class="form-control" type="text" name="r_f_c" value=""><span>*Cuando cuente con R.F.C</span>
					    </div>
					    <div class="col-md-3 form-group">
					      <label>CURP:</label>
					      <input class="form-control" type="text" name="curp" value="">
					      <span>*Cuando cuente con CURP</span>
					    </div>
					</div>
				</div>
			</form>
			<br>
			<hr class="subtitle">
				<div class="row">
		        <div class="col-md-6 form-group">
		          <p>Socio 2:</p>
		        </div>
		        <div class="col-md-2 form-group"></div>
		    <form class="form" method="post" role="form" id="form_bene_fisica_fisica_s22_2">
		        <div class="col-md-4 form-group">
		           <label>% de acciones:</label>
	               <input class="form-control" type="text" name="porcentaje" id="porcentaje6">
		        </div>
				</div>
				<div class="row">
		          <div class="col-md-4 form-group">
		            <label>Nombre(s):</label>
		            <input class="form-control" type="text" name="nombre" value="">
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido paterno:</label>
		            <input class="form-control" type="text" name="apellido_paterno" value="">
		          </div>
		          <div class="col-md-4 form-group">
		            <label>Apellido materno:</label>
		            <input class="form-control" type="text" name="apellido_materno" value="">
		          </div>
		        </div>
		        <div class="row">
			      <div class="col-md-4 form-group">
			        <label>Nombre de identificación:</label>
			        <input class="form-control" type="text" name="nombre_identificacion" value="">
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Autoridad que emite la identificación:</label>
			        <input class="form-control" type="text" name="autoridad_emite" value="">
			      </div>
			      <div class="col-md-4 form-group">
			        <label>Número de identificación:</label>
			        <input class="form-control" type="text" name="numero_identificacion" value="">
			      </div>
			    </div>
		        <div class="row">
		        	<div class="col-md-4">
				        <label>Género:</label>
				        <div class="row">
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generom6">
				                Masculino
				              </label>
				            </div>
				          </div>
				          <div class="col-md-6 form-group">
				            <div class="form-check form-check-success">
				              <label class="form-check-label">
				                <input type="radio" class="form-check-input" name="genero" id="generof6">
				                Femenino
				              </label>
				            </div>
				          </div>
				        </div>
				    </div>    
		        </div>
		        <div class="row">
				  <div class="col-md-4 form-group">
				    <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
				    <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País de nacimiento:</label><br>
				    <select class="form-control pais21" id="pais" name="pais_nacimiento" onclick="getpais(21)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				  <div class="col-md-4 form-group">
				    <label>Pais de nacionalidad:</label><br>
				    <select class="form-control pais22" id="pais" name="pais_nacionalidad" onclick="getpais(22)">
				      <option value="MX">MEXICO</option>
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12">
				    <h4>Dirección</h4>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 form-group">
				    <label>Tipo de vialidad:</label>
				    <select class="form-control" name="tipo_vialidad">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Calle:</label>
				    <input class="form-control" type="text" name="calle" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.ext:</label>
				    <input class="form-control" type="number" name="no_ext" value="">
				  </div>
				  <div class="col-md-2 form-group">
				    <label>No.int:</label>
				    <input class="form-control" type="number" name="no_int" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Colonia:</label>
				    <input class="form-control" type="text" name="colonia" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Municipio o delegación:</label>
				    <input class="form-control" type="text" name="municipio" value="">
				  </div>
				  <div class="col-md-3 form-group">
				    <label>Localidad:</label>
				    <input class="form-control" type="text" name="localidad" value="">
				  </div>
				  <div class="col-md-3 form-group estado7">
				    <label>Estado:</label>
				    <input class="form-control" type="hidden" name="estado" value="">
				    <span class="span_div_23"></span>
				  </div>
				    <!--<div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>-->
				  <div class="col-md-2 form-group">
				    <label>C.P:</label>
				    <input class="form-control" type="text" name="cp" value="">
				  </div>
				  <div class="col-md-4 form-group">
				    <label>País:</label><br>
				    <select class="form-control pais23" id="pais7" name="pais" onclick="getpais2(23)">
				      <!--<option value="MX">MEXICO</option>-->
				    </select>
				  </div>
				</div>
		        <div class="row">
				  <div class="col-md-12 form-group">
				    <div class="form-check form-check-primary">
				      <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
				        <input type="checkbox" class="form-check-input" name="residencia_extran" id="lugar_recidencia_socio2_accion_a2" onchange="lugar_recidencia_btn_socio_accion_a_socio2_2()">
				      </label>
				    </div>
				  </div>
				</div>
		        <div class="residencia_extranjero_accion_a_socio2_2" style="display: none">
				  <div class="row">
				    <div class="col-md-12">
				      <span>Dirección en México.</span>
				    </div>
				  </div><br>
				  <div class="row">
				    <div class="col-md-2 form-group">
				        <label>Tipo de vialidad:</label>
				        <select class="form-control" name="tipo_vialidad_t">
	                      <option disabled selected>Selecciona un tipo</option>
	                      <?php foreach ($get_tipo_vialidad as $item){ ?>
	                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
	                      <?php } ?>
	                    </select>
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Calle:</label>
				      <input class="form-control" type="text" name="calle_t" value="">
				    </div>
				    <div class="col-md-2 form-group">
				      <label>No.ext:</label>
				      <input class="form-control" type="number" name="no_ext_t" value="">
				    </div>
				    <div class="col-md-2 form-group">
				      <label>No.int:</label>
				      <input class="form-control" type="number" name="no_int_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Colonia:</label>
				      <input class="form-control" type="text" name="colonia_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Municipio o delegación:</label>
				      <input class="form-control" type="text" name="municipio_t" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>Localidad:</label>
				      <input class="form-control" type="text" name="localidad_t" value="">
				    </div>
				    <!--<div class="col-md-3 form-group">
				      <label>Estado:</label>
				      <input class="form-control" type="text" name="estado_t" value="">
				    </div>-->
				    <div class="col-md-3 form-group">
					    <label>Estado:</label><br>
					    <select class="form-control" name="estado_t">
		                    <?php foreach ($get_estado as $e) {
		                      echo "<option value='$e->clave'>$e->estado</option>";
		                    } ?>
		                  </select>
					</div>
				    <div class="col-md-3 form-group">
				      <label>C.P:</label>
				      <input class="form-control" type="text" name="cp_t" value="">
				    </div>
				  </div>
				  <div class="row">
				    <div class="col-md-6 form-group">
				      <label>Correo electrónico:</label>
				      <input class="form-control" type="email" name="correo" value="">
				    </div>
				    <div class="col-md-3 form-group">
				      <label>R.F.C:</label>
				      <input class="form-control" type="text" name="r_f_c" value=""><span>*Cuando cuente con R.F.C</span>
				    </div>
				    <div class="col-md-3 form-group">
				      <label>CURP:</label>
				      <input class="form-control" type="text" name="curp" value="">
				      <span>*Cuando cuente con CURP</span>
				    </div>
				  </div>
				</div>
			</form>
			<!-- -->
			</div>
		</div>
</div>	

<script src="<?php echo base_url(); ?>/public/js/catalogos/clientes_cliente/paso3.js?v=20201404"></script>