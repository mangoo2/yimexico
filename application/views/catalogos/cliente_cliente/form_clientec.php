<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 ><?php echo $titulo ?> Cliente</h3>
        <hr class="subtitle">
        <!---------------->
        <form class="form" method="post" role="form" id="form_clientec">
          <?php if(isset($cc->id)){ ?>
            <input type="hidden" name="id" id="id" value="<?php echo $cc->id ?>">
          <?php } ?>
          <input type="hidden" name="idcliente" id="idcliente" value="<?php echo $this->session->userdata('idcliente'); ?>">
          <div class="row">
          	<div class="col-md-4"> 
              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_per" id="tipo1" value="1" checked <?php if(isset($cc->tipo_per) && $cc->tipo_per==1) echo "checked"; ?>>
                  Persona fisíca
                <i class="input-helper"></i></label>
              </div>

              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_per" id="tipo2" value="2" <?php if(isset($cc->tipo_per) && $cc->tipo_per==2) echo "checked"; ?>>
                  Persona moral
                <i class="input-helper"></i></label>
              </div>

              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="tipo_per" id="tipo3" value="3" <?php if(isset($cc->tipo_per) && $cc->tipo_per==3) echo "checked"; ?>>
                  Fideicomiso
                <i class="input-helper"></i></label>
              </div>
          	</div>
          </div>
          <br>
        <!---------------->
          <div class="row" id="cont_pf" style="display: none"> 
            <div class="row col-md-12">
              <div class="col-md-6 form-group">
                <label>Nombre(s):</label>
                <input class="form-control" type="text" name="nombre" <?php if(isset($cc->nombre) && $cc->tipo_per==1) echo "value='$cc->nombre'"; ?>>
              </div>
              <div class="col-md-6 form-group">
                <label>Apellido paterno:</label>
                <input class="form-control" type="text" name="apellido_paterno" <?php if(isset($cc->apellido_paterno) && $cc->tipo_per==1) echo "value='$cc->apellido_paterno'"; ?>>
              </div>
              <div class="col-md-6 form-group">
                <label>Apellido materno:</label>
                <input class="form-control" type="text" name="apellido_materno" <?php if(isset($cc->apellido_materno) && $cc->tipo_per==1) echo "value='$cc->apellido_paterno'"; ?>>
              </div>
            
              <div class="col-md-6 form-group">
                <label>R.F.C:</label>
                <input class="form-control" type="text" name="rfc" id="rfc_fisica" <?php if(isset($cc->apellido_materno) && $cc->tipo_per==1) echo "value='$cc->rfc'"; ?>>
              </div>
            </div>
          </div>
      
          <div class="row" id="cont_pm" style="display: none">
            <div class="col-md-12 form-group">
              <label>Razón social:</label>
              <input class="form-control" type="text" name="razon_social" <?php if(isset($cc->razon_social) && $cc->tipo_per==2) echo "value='$cc->razon_social'"; ?>>
            </div>
            <div class="col-md-12 form-group">
            <label>R.F.C:</label>
            <input class="form-control" type="text" name="rfc" <?php if(isset($cc->rfc) && $cc->tipo_per==2) echo "value='$cc->rfc'"; ?>>
           </div>
          </div>
        
        <div class="row" id="cont_fc" style="display: none">
          <div class="col-md-12 form-group">
            <label>Denominación o razón social del fiduciario:</label>
            <input class="form-control" type="text" name="razon_social" <?php if(isset($cc->razon_social) && $cc->tipo_per==3) echo "value='$cc->razon_social'"; ?>>
          </div>
          <div class="col-md-12 form-group">
            <label>R.F.C del fideicomiso:</label>
            <input class="form-control" type="text" name="rfc" <?php if(isset($cc->rfc) && $cc->tipo_per==3) echo "value='$cc->rfc'"; ?>>
          </div>
        </div>
        <br>
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-3 form-group">
            <label>Calle</label>
            <input type="text" name="calle" class="form-control" <?php if(isset($cc->calle)) echo "value='$cc->calle'"; ?>>
          </div>
          <div class="col-md-1 form-group">
            <label>No. ext</label>
            <input type="text" name="no_ext" class="form-control" <?php if(isset($cc->no_ext)) echo "value='$cc->no_ext'"; ?>>
          </div>
          <div class="col-md-1 form-group">
            <label>No. int</label>
            <input type="text" name="no_int" class="form-control" <?php if(isset($cc->no_int)) echo "value='$cc->no_int'"; ?>>
          </div>
          <div class="col-md-4 form-group">
            <label>Colonia</label>
            <input type="text" name="colonia" class="form-control" <?php if(isset($cc->colonia)) echo "value='$cc->colonia'"; ?>>
          </div>
          <div class="col-md-3 form-group">
            <label>Codigo postal</label>
            <input type="text" name="cp" class="form-control"  <?php if(isset($cc->cp)) echo "value='$cc->cp'"; ?>>
          </div>
          <div class="col-md-3 form-group">
            <label>Teléfono Local:</label>
            <input class="form-control" type="number" name="tel" <?php if(isset($cc->tel)) echo "value='$cc->tel'"; ?>>
          </div>
          <div class="col-md-3 form-group">
            <label>Teléfono móvil:</label>
            <input class="form-control" type="number" name="cel" <?php if(isset($cc->cel)) echo "value='$cc->cel'"; ?>>
          </div>
          <div class="col-md-6 form-group">
            <label>Correo electrónico:</label>
            <input class="form-control" type="email" name="correo" <?php if(isset($cc->correo)) echo "value='$cc->correo'"; ?>>
          </div>
        </div>
        <hr class="subtitle">
        <div class="modal-footer">
            <button class="btn btn-yimexico pull-right" id="btn_submit" type="submit">Resgistrar Cliente</button>
            <button class="btn btn-secondary pull-right" type="">Cancelar</button>
        </div>
      </form>


      </div>
    </div>
  </div>
</div>
