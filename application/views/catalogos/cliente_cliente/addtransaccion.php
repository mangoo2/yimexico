<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <!---------------->
          <?php echo $html_text ?>
          <br>
          <div align="center">
             <h5>¿Tiene conocimiento de la existencia del dueño beneficiario?</h5>
          </div>
          <div class="row">
              <div class="col-md-5 form-group"></div>
              <div class="col-md-2 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="genero" id="genero_1" value="1" onclick="click_beneficiario()">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-2 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="genero" id="genero_2" value="2" onclick="click_beneficiario()">
                    No
                  </label>
                </div>
              </div>
              <div class="col-md-3 form-group"></div>
          </div>
          <span class="text_select_beneficiario" style="display: none">
              <span class="select_beneficiario"></span>
          </span>
          <span class="beneficiario_text" style="display: none">
              <br>
              <!--
              <div class="row">
                <div class="col-md-12" align="center">
                  <h5>¿Es persona fisica o moral?</h5>
                </div>
              </div>
              -->
              <div class="row">
                <div class="col-md-4 form-group"></div>
                <div class="col-md-4 form-group">
                  <label>Seleccione el tipo de persona:</label>
                  <select class="form-control" name="tipo_persona" id="tipo_persona" onchange="check_tipo_persona()">
                    <option value="0" selected="" disabled="">Seleccione alguna opción </option>
                    <option value="1">Persona Física</option>
                    <option value="2">Persona Moral</option>      
                    <option value="3">Fideicomiso</option>      
                  </select>
                    <!-- 
                      <div class="row">
                        <div class="col-md-6 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="tipo_persona" id="tipo_persona" value="1" onclick="check_tipo_persona()">
                              Persona fisica
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6 form-group">
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="tipo_persona" id="tipo_persona2" value="2" onclick="check_tipo_persona()">
                              Persona moral
                            </label>
                          </div>
                        </div>
                      </div>
                    -->
                    </div>
                    <div class="col-md-3 form-group"></div>
              </div>
          </span> 
          <form class="form" method="post" role="form" id="form_beneficiario_fisica"> 
            <span class="text_persona_fisica" style="display: none;">
              <hr class="subtitle"> 
              <div class="row">
                <div class="col-md-12">    
                  <h3>Datos de la persona física</h3>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-12">
                  <span >Nombre del Cliente o Usuario</span>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-12">
                  <span >Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).</span>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Nombre(s):</label>
                  <input class="form-control" type="text" name="nombre" value="">
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido paterno:</label>
                  <input class="form-control" type="text" name="apellido_paterno" value="">
                </div>
                <div class="col-md-4 form-group">
                  <label>Apellido materno:</label>
                  <input class="form-control" type="text" name="apellido_materno" value="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Nombre de identificación:</label>
                  <input class="form-control" type="text" name="nombre_identificacion" value="">
                </div>
                <div class="col-md-4 form-group">
                  <label>Autoridad que emite la identificación:</label>
                  <input class="form-control" type="text" name="autoridad_emite" value="">
                </div>
                <div class="col-md-4 form-group">
                  <label>Número de identificación:</label>
                  <input class="form-control" type="text" name="numero_identificacion" value="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-7 form-group">
                  <label>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario:</label>
                  <select class="form-control" name="actividad_giro">
                    <?php foreach ($get_actividad as $item) { ?>
                      <option value="<?php echo $item->clave ?>"><?php echo $item->acitividad ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>    
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>Género:</label>
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="genero" id="ExampleRadio2" value="1">
                          Masculino
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6 form-group">
                      <div class="form-check form-check-success">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="genero" id="ExampleRadio2" value="2">
                          Femenino
                        </label>
                      </div>
                    </div>
                  </div>
                </div>  
              </div>
              <div class="row"> 
                <div class="col-md-4 form-group">
                  <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
                  <input class="form-control"  max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" value="">
                </div>
                <div class="col-md-4 form-group">
                  <label>País de nacimiento:</label><br>
                  <select class="form-control pais1" name="pais_nacimiento">
                    <option value="MX">MEXICO</option>
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  <label>Pais de nacionalidad:</label><br>
                  <select class="form-control pais2" name="pais_nacionalidad">
                    <option value="MX">MEXICO</option>
                  </select>
                </div>
              </div>  
              <div class="row">
                <div class="col-md-12">
                  <h4>Dirección</h4>
                </div>
              </div>
              <div class="row">  
                <div class="col-md-2 form-group">
                  <label>Tipo de vialidad:</label>
                  <select class="form-control" name="tipo_vialidad">
                    <option disabled selected>Selecciona un tipo</option>
                    <?php foreach ($get_tipo_vialidad as $item){ ?>
                        <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3 form-group">
                  <label>Calle:</label>
                  <input class="form-control" type="text" name="calle" value="">
                </div>
                <div class="col-md-2 form-group">
                  <label>No.ext:</label>
                  <input class="form-control" type="text" name="no_ext" value="">
                </div>
                <div class="col-md-2 form-group">
                  <label>No.int:</label>
                  <input class="form-control" type="text" name="no_int" value="">
                </div>
                <div class="col-md-3 form-group">
                  <label>Colonia:</label>
                  <input class="form-control" type="text" name="colonia" value="">
                </div>
                <div class="col-md-3 form-group">
                  <label>Municipio o delegación:</label>
                  <input class="form-control" type="text" name="monicipio" value="">
                </div>
                <div class="col-md-3 form-group">
                  <label>Localidad:</label>
                  <input class="form-control" type="text" name="localidad" value="">
                </div>
                <div class="col-md-3 form-group">
                  <label>Estado:</label>
                  <input readonly="" type="hidden" class="estado_text_d_2" value="">
                  <span class="span_div_t_12"></span>
                </div>
                <div class="col-md-3 form-group">
                  <label>C.P:</label>
                  <input class="form-control" type="text" name="cp" value="">
                </div>
                <div class="col-md-4 form-group">
                  <label>País:</label><br>
                  <select class="form-control idpais_t_12" name="pais">
                    <option value="MX">MEXICO</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <div class="form-check form-check-primary">
                    <label class="form-check-label">Tratandose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expesiente los datos relativos a dicho domicilio.
                      <input type="checkbox" class="form-check-input" id="lugar_recidencia" name="trantadose_persona" onchange="lugar_recidencia_btn_paso3()">
                    </label>
                  </div>
                </div>
              </div>
              <div class="residenciaEx" style="display: none">
                <div class="row">
                  <div class="col-md-12">
                    <span>Dirección en México.</span>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col-md-2 form-group">
                    <label>Tipo de vialidad:</label>
                    <select class="form-control" name="tipo_vialidad_d">
                      <option disabled selected>Selecciona un tipo</option>
                      <?php foreach ($get_tipo_vialidad as $item){ ?>
                          <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option> 
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Calle:</label>
                    <input class="form-control" type="text" name="calle_d" value="">
                  </div>
                  <div class="col-md-2 form-group">
                    <label>No.ext:</label>
                    <input class="form-control" type="text" name="no_ext_d" value="">
                  </div>
                  <div class="col-md-2 form-group">
                    <label>No.int:</label>
                    <input class="form-control" type="text" name="no_int_d" value="">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Colonia:</label>
                    <input class="form-control" type="text" name="colonia_d" value="">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Municipio o delegación:</label>
                    <input class="form-control" type="text" name="municipio_d" value="">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Localidad:</label>
                    <input class="form-control" type="text" name="localidad_d" value="">
                  </div>
                  <div class="col-md-3 form-group">
                    <label>Estado:</label><br>
                    <select class="form-control estado_fisica" name="estado_d">
                      <?php foreach ($get_estado as $item) {?>
                        <option value="<?php echo $item->clave?>"><?php echo $item->estado?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-3 form-group">
                    <label>C.P:</label>
                    <input class="form-control" type="text" name="cp_d" value="">
                  </div>
                </div>
              </div>
              <hr class="subtitle"> 
              <div class="row">
                <div class="col-md-6 form-group">
                  <label>Correo electrónico:</label>
                  <input class="form-control" type="email" name="correo_d" value="">
                </div>
                <div class="col-md-3 form-group">
                  <label>R.F.C:</label>
                  <input class="form-control" type="text" name="r_f_c_d" value=""><span>*Cuando cuente con R.F.C</span>
                </div>
                <div class="col-md-3 form-group">
                  <label>CURP:</label>
                  <input class="form-control" type="text" name="curp_d" value="">
                  <span>*Cuando cuente con CURP</span>
                </div>
              </div>
            </span>
          </form>

          <!-- PERSONA MORAL PRIMER CHECK -->
          <span class="text_persona_moral" style="display: none;">
            <form class="form" method="post" role="form" id="form_beneficiario_moral"> 
              <hr class="subtitle"> 
              <div class="row">
                <div class="col-md-12">    
                  <h3>Datos de la persona moral</h3>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-6 form-group">
                  <label>Razón social</label>
                  <input class="form-control" type="text" name="razon_social" value="">
                </div>
              </div>
              <div class="row">    
                <div class="col-md-3 form-group">
                  <label><i class="fa fa-calendar"></i> Fecha de constitución:</label>
                  <input class="form-control" type="date" name="fecha_constitucion">
                </div>
              </div>
              <div class="row">
                <div class="col-md-3 form-group">
                  <label>R.F.C:</label>
                  <input class="form-control" type="text" name="rfc" value="">
                </div>
                <div class="col-md-4 form-group">
                  <label>País de nacionalidad:</label><br>
                  <select class="form-control pais3" name="pais">
                    <option value="MX">MEXICO</option>
                  </select>
                </div>
              </div>
            </form>
            <!--
            <div class="row">
              <div class="col-md-12">    
                <h3>IDENTIFICACIÓN DEL BENEFICIARIO CONTROLADOR</h3>
              </div>
            </div> 
            
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">A) Nombrar a la(s) persona(s) fisica(s) que directa o indirectamente, posea el 25% o más de la composición accionaria o dl capital social de la persona moral.   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_a" name="tipo_inciso" value="a">
                  </label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">B) En caso de no tener accionistas que ostenten más del 25% de la composícion accionaria de la persona moral en forma individual o conjunta, favor de elistar a todos los accionistas personas física o morales, siempre y cuando matengan alguna relación juridica.   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_b" name="tipo_inciso" value="b">
                  </label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">C) Nombre del presidente del consejo de administración o administrador unico (según aplique, proviniente del acta constitutiva poderes).   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_c" name="tipo_inciso" value="c">
                  </label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="form-check form-check-primary">
                  <label class="form-check-label">D) Nombre del director general(proviniente del documento de identificación).   
                    <input type="radio" class="form-check-input tipo_inciso" id="accionaria_socio_d" name="tipo_inciso" value="d">
                  </label>
                </div>
              </div>
            </div>
            -->
            <!--<div id="contForm"> </div>
            -->
          </span>
          <!-- PERSONA MORAL PRIMER CHECK -->
          <span class="text_persona_fideicomiso" style="display: none;">
            <form class="form" method="post" role="form" id="form_beneficiario_fideicomiso"> 
              <hr class="subtitle"> 
              <div class="row">
                <div class="col-md-12">    
                  <h3>Datos del fideicomiso</h3>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-12 form-group">
                  <label>Razón social del fiduciario</label>
                  <input class="form-control" type="text" name="razon_social" value="">
                </div>
              </div>
              <div class="row">    
                <div class="col-md-6 form-group">
                  <label>R.F.C del fideicomiso</label>
                  <input class="form-control" type="text" name="rfc">
                </div>
                <div class="col-md-6 form-group">
                  <label>Número o referencia del fideicomiso:</label>
                  <input class="form-control" type="text" name="referencia" value="">
                </div>
              </div>
            </form>  
          </span>  
          <br>

          <span class="btn_siguiente" style="display: none">
            <div class="row">
              <div class="col-md-12" align="right">
                <button type="button" disabled id="sig_si" class="btn btn-yimexico" onclick="otro_beneficiario_siguiente()"><i class="fa fa-arrow-right"></i> Siguiente</button>
              </div>
            </div>
          </span>
          <span class="btn_siguiente2" style="display: none">
            <div class="row">
              <div class="col-md-12" align="right">
                <button type="button" class="btn btn-yimexico" onclick="otro_beneficiario_siguiente2()"><i class="fa fa-arrow-right"></i> Siguiente</button>
              </div>
            </div>
          </span>        

      </div>
    </div>
  </div>
</div>
