<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3>Unión de clientes (clientes mancomunados)</h3>
        <hr class="subtitle">
        <!---------------->
        <form class="form" method="post" role="form" id="form_cliente">
          <input type="hidden" name="idcliente" id="idcliente" value="">
          <input type="hidden" name="id_union" id="id_union" value="<?php echo $id_union; ?>">
          <input type="hidden" id="desdeopera" value="<?php echo $desdeopera; ?>">
          <div class="row">
          	<div class="col-md-8 form-group">
              <div class="row">
                <div class="col-md-12 form-group">
                  <select class="form-control" id="cliente">
                    <?php foreach ($clis as $c) {
                      if($c->idtipo_cliente==1){
                        $id=$c->id_perfilamiento;
                        if($id==null){
                          $id=$c->idperfilamientop;
                        }
                        $cliente=$c->nombre.' '.$c->apellido_paterno.' '.$c->apellido_materno;
                      }else if($c->idtipo_cliente==2){
                        $id=$c->id_perfilamiento;
                        if($id==null){
                          $id=$c->idperfilamientop;
                        }
                        $cliente=$c->nombre2.' '.$c->apellido_paterno2.' '.$c->apellido_materno2;
                      }else if($c->idtipo_cliente==3){
                        $id=$c->id_perfilamiento;
                        if($id==null){
                          $id=$c->idperfilamientop;
                        }
                        $cliente=$c->razon_social;
                      }else if($c->idtipo_cliente==4){
                        $id=$c->id_perfilamiento;
                        if($id==null){
                          $id=$c->idperfilamientop;
                        }
                        $cliente=$c->nombre_persona;
                      }else if($c->idtipo_cliente==5){
                        $id=$c->id_perfilamiento;
                        if($id==null){
                          $id=$c->idperfilamientop;
                        }
                        $cliente=$c->denominacion2;
                      }else if($c->idtipo_cliente==6){
                        $id=$c->id_perfilamiento;
                        if($id==null){
                          $id=$c->idperfilamientop;
                        }
                        $cliente=$c->denominacion;
                      } 
                      if($c->idcc1!=0){
                        $idcc=$c->idcc1;
                      }
                      if($c->idcc2!=0){
                        $idcc=$c->idcc2;
                      }
                      if($c->idcc3!=0){
                        $idcc=$c->idcc3;
                      }
                      if($c->idcc4!=0){
                        $idcc=$c->idcc4;
                      }
                      if($c->idcc5!=0){
                        $idcc=$c->idcc5;
                      }
                      if($c->idcc6!=0){
                        $idcc=$c->idcc6;
                      }
                      $idcp=$id."-".$idcc
                      ?>
                      <option value="<?php echo $idcp; ?>"><?php echo $cliente; ?></option>
                    <?php } ?>
                  </select>       
                </div>
                <div class="col-md-12">
                  <table class="table table-striped" id="table_uniones">
                    <tbody id="tbody_uniones">     
                      <?php
                      if($band_uc==1){
                        foreach ($uc as $i){ ?>
                          <tr class="clienteselectd_<?php echo $i->iduc;?>">
                            <td class="colprofmat">
                              <input type="hidden" name="id_union" id="id_union" value="<?php echo $i->id_union;?>">
                              <input type="hidden" name="id_union_cli" id="id_union_cli" value="<?php echo $i->iduc;?>">
                              <input type="hidden" name="ncli" id="ncli" value="<?php echo $i->id_perfilamiento.'-'.$i->id_clientec;?>">
                              <?php if($i->idtipo_cliente==1){
                                $cliente=$i->nombre.' '.$i->apellido_paterno.' '.$i->apellido_materno;
                              }else if($i->idtipo_cliente==2){
                                $cliente=$i->nombre2.' '.$i->apellido_paterno2.' '.$i->apellido_materno2;
                              }else if($i->idtipo_cliente==3){
                                $cliente=$i->razon_social;
                              }else if($i->idtipo_cliente==4){
                                $cliente=$i->nombre_persona;
                              }else if($i->idtipo_cliente==5){
                                $cliente=$i->denominacion2;
                              }else if($i->idtipo_cliente==6){
                                $cliente=$i->denominacion;
                              }  ?>
                              <?php echo $cliente; ?>
                            </td>
                            <td class="colpro5">
                              <button type="button" class="btn btn_ayuda" onclick="deleteCliente(<?php echo $i->iduc;?>)"><i class="fa fa-minus"></i></button>
                            </td>
                          </tr>
                        <?php
                       } } ?>
                    </tbody>
                  </table>
                </div>  
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 form-group" align="right">
                  <div align="right">
                    <button type="button" class="btn gradient_nepal2" id="add_cli"><i class="fa fa-plus"></i> Agregar cliente</button>
                  </div>         
                </div>
              </div>
          	</div>
          </div>
          <div class="row">
            <div class="col-md-12" align="right">
              <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Clientes_cliente"><i class="fa fa-reply"></i> Regresar</a>
              <button type="submit" class="btn gradient_nepal2 guardar_registro"><i class="fa  fa-floppy-o"></i> Guardar</button>
            </div>
          </div>
        </form>     

        
         
        <!---------------->   
      </div>
    </div>
  </div>
</div>
