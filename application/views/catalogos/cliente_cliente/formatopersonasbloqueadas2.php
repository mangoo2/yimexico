<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<div class="card">
    <div class="card-body">	
		<div class="row">
			<div class="col-md-12">    
				<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
				<input type="hidden" id="tipo_pers" value="<?php echo $tipo_pers; ?>">
				<input type="hidden" id="id_benef_m" value="<?php echo $id_benef_m; ?>">
				<input type="hidden" id="idopera" value="<?php echo $idopera; ?>">
			    <h3>Formato: Consulta del cliente en listas de personas bloqueadas.</h3>
			    <h3 id="fecha_busqueda">Fecha consulta:</h3>
			    <h3 id="folio_busqueda">Folio de búsqueda: 1</h3>
			 </div>
		</div> 
		<div class="row">
			<div class="col-md-12">    
			    <h5>Tipo de persona: 
			    	<?php if($tipo_pers==1){ echo "Persona Física"; 
			    	} if($tipo_pers==2){ 
			    		echo "Persona Moral"; } 
			    	  if($tipo_pers==3) { echo "Fideicomiso"; } ?> 
                </h5>
			    <!--Nombre o entidad buscada-->
           <?php if($tipo_pers==1){ ?> 
	          <h5>Nombre: <?php echo $nombre_tipo ?></h5>
	          <h5>Nombre de identificación: <?php echo $nombre_identificacion ?></h5>
	          <h5>Número de identificación: <?php echo $numero_identificacion ?></h5>
		    	<?php } ?>	
		    	<?php if($tipo_pers==2){ ?> 
	            <h5>Razón social del fiduciario: <?php echo $nombre_tipo ?></h5>
		    	<?php } ?>	
		    	<?php if($tipo_pers==3){ ?> 
		    	<h5>Razón social: <?php echo $nombre_tipo ?></h5>
		    	<?php } ?>  
	            
			</div>
		</div> 
		<hr>
		<div class="table-responsive">
          <table class="table" id="table_opera">
            <thead>
	            <tr>
	                <th><strong>Lista de búsqueda</strong></th>
	                <th><strong>Resultado</strong></th>
	            </tr>
            </thead>
            <tbody>
            	<tr>
            	    <td id="lista_result"><a href="javascript:void()"> Ver información de busqueda </a></td>	
            	    <td id="result_pep"></td>
            	</tr>
            	<!--<tr>
            	    <td>GAFI</td>	
            	    <td>No encontrado</td>
            	</tr>
            	<tr>
            	    <td>OCDE</td>	
            	    <td>No encontrado</td>
            	</tr>
            	<tr>
            	    <td>REFIPRE</td>	
            	    <td>No encontrado</td>
            	</tr>-->
            </tbody>
          </table>
        </div>
        <div class="row">
        	<div class="col-md-12" align="right">
        		<button type="button" class="btn btn-secondary btnf_guardar" onclick="imprimir()"><i class="fa fa-print"></i> Imprimir consulta de persona en listas bloqueadas</button>
        	</div>
        </div>
	</div>	 
</div>	

<div class="modal fade" id="modal_detalle" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Información de busqueda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
      	<div class="table-responsive">
	        <div class="row">
	          <div class="col-md-12">
							 <img id="img_detalle" width="750px">
	          </div>
	        </div>
	      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url(); ?>template/vendors/js/vendor.bundle.base.js"></script>
<script type="text/javascript">
	var band_bus=0;
	function imprimir(){
		$('.btnf_guardar').css('display','none');
		window.print();
		setTimeout(function () {  $('.btnf_guardar').css('display','block'); }, 1000);
	}
	$("#lista_result").on("click",function(){
		$("#modal_detalle").modal();
		if(band_bus==0)
			$('#img_detalle').attr("src", base_url+"public/img/resultados/result_inexistente.jpg");
		else
			$('#img_detalle').attr("src", base_url+"public/img/resultados/listas_pagadas.png");
	});

	base_url = $("#base_url").val();
	$.ajax({
	    type: "POST",
	    url: base_url+"Clientes_cliente/verificaListado",
	    data:{
	      id_operacion: $('#idopera').val(), id_bene:$("#id_benef_m").val(), tipo_bene: $("#tipo_pers").val()
	    },
	    success: function (data){
	      	console.log(data);
	      	var resultado_bene="";
	      	if(data!="0"){
	      		var array = $.parseJSON(data);   
	      		$('#folio_busqueda').html("Folio de búsqueda: "+array[0].id);
	      		$('#fecha_busqueda').html("Fecha consulta: "+array[0].fecha_consulta);
	      		//$.each(array[0].resultado, function(index, item) {
			    	var resultadox='['+array[0].resultado_bene+']';
			    	//console.log(resultadox);
			    	var resultadox = $.parseJSON(resultadox);
			    	//console.log(resultadox);
			    	$('#result_pep').html('');
			    	resultadox.forEach(function(item) {

			        if(item.resultado_bene!=""){
	            	//resultado_bene = $.parseJSON(item.resultado_bene);
	            	resultado_bene = item.resultado_bene;
	          	}
		         	if(resultado_bene==""){
	            	$('#result_pep').html('<p>Cliente no se encuentra en el listado de busqueda</p>');
	          	}
	          	else if(item.Status=="OK"){
	            	$('#result_pep').html('<p>'+item.Message+'</p>');
	          	}
	          	else if(item.Status=="ERROR"){
	            	$('#result_pep').html('<p>'+item.Message+'</p>');
	          	}
			        else if(item.Estado=="ACTIVO" || item.Estado=="INACTIVO"){
			        	fina_cargo="";
		        		if(item.Finalizacion_cargo!=undefined){
		        			fina_cargo=item.Finalizacion_cargo;
		        		}
		            $('#result_pep').html("<p><strong>Cliente encontrado en listas de busqueda </strong></p>\
		           			<p> <strong>Denominación: </strong>"+item.Denominacion+"</p>\
		           			<p> <strong>Identificación: </strong>"+item.Identificacion+" </p>\
								    <p> <strong>Id_Tributaria: </strong>"+item.Id_Tributaria+" </p>\
								    <p> <strong>Otra_Identificacion: </strong>"+item.Otra_Identificacion+" </p>\
								    <p> <strong>Cargo: </strong>"+item.Cargo+" </p>\
								    <p> <strong>Lugar_Trabajo: </strong>"+item.Lugar_Trabajo+" </p>\
								    <p> <strong>Dirección: </strong>"+item.Direccion+" </p>\
								    <p> <strong>Enlace: </strong>"+item.Enlace+" </p>\
								    <p> <strong>Tipo: </strong>"+item.Tipo+" </p>\
								    <p> <strong>Estado: </strong>"+item.Estado+" </p>\
								    <p> <strong>Lista: </strong>"+item.Lista+" </p>\
								    <p> <strong>Pais_Lista: </strong>"+item.Pais_Lista+" </p>\
								    <p> <strong>Cod_Individuo: </strong>"+item.Cod_Individuo+" </p>\
								    <p> <strong>Exactitud_Denominacion: </strong>"+item.Exactitud_Denominacion+" </p>\
								    <p> <strong>Exactitud_Identificacion: </strong>"+item.Exactitud_Identificacion+" </p><hr>\
								");
								band_bus=1;

			        }
	      		}); 
	      	}else{
	      		band_bus=0;
	      		$('#result_pep').html("<p><strong>Cliente no encontrado en listas de busqueda </strong></p>");
	      		
	      	}
	    }
	});

</script>
