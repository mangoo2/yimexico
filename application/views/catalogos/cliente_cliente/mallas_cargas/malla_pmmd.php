<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=malla_pmmd".date('Y-m-d').".xls");	
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>Nombre de la persona moral de derecho público</th>
          <th>Nacionalidad (por clave de país)</th>
          <th>Giro mercantil / Actividad / Objetivo social</th>
          <th>Dirección: Tipo de vialidad</th>
          <th>Calle</th>
          <th>No.ext</th>
          <th>No.int</th>
          <th>C.P.</th>
          <th>Municipio o delegación</th>
          <th>Localidad</th>
          <th>Estado (por número de estado)</th>
          <th>Colonia</th>
          <th>Fecha de constitución</th>
          <th>DATOS GENERALES DE LOS SERVIDORES PÚBLICOS: Nombre(s)</th>
          <th>Apellido paterno</th>
          <th>Apellido materno</th>
          <th>Género (1=masculino, 2=femenino)</th>
          <th>Fecha de nacimiento</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>