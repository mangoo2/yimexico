<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=malla_pme".date('Y-m-d').".xls");	
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>Razón social</th>
          <th>Giro mercantil / Actividad / Objetivo social (por clave)</th>
          <th>Clave del registro federal de contribuyentes con homoclave / número de identificacion fiscal en otro país (cuando aplique) y país que la asignó.</th>
          <th>Extranjero (on=si, off=no)</th>
          <th>País (por clave de país)</th>
          <th>Nacionalidad (por clave de país)</th>
          <th>Dirección: País (por clave de país)</th>
          <th>Tipo de vialidad</th>
          <th>Calle</th>
          <th>No.ext</th>
          <th>No.int</th>
          <th>C.P.</th>
          <th>Municipio o delegación</th>
          <th>Localidad</th>
          <th>Estado (por número de estado)</th>
          <th>Colonia</th>
          <th>Número de teléfono</th>
          <th>Correo electrónico</th>
          <th>Fecha de constitución</th>
          <th>Número de acta constitutiva</th>
          <th>Nombre del notario</th>
          <th>DATOS GENERALES DEL APODERADO: Nombre(s)</th>
          <th>Apellido paterno</th>
          <th>Apellido materno</th>
          <th>Nombre de identificación</th>
          <th>Autoridad que emite la identificación</th>
          <th>Número de identificación</th>
          <th>Fecha de nacimiento</th>
          <th>Género (1=masculino, 2=femenino)</th>
          <th>RFC</th>
          <th>CURP</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>