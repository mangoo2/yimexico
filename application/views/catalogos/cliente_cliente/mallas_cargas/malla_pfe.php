<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=malla_pfe".date('Y-m-d').".xls");	
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>Nombre</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Nombre de identificación</th>
          <th>Autoridad que emite la identificación</th>
          <th>Número de identificación</th>
          <th>Género (1=masculino, 2=femenino)</th>
          <th>Fecha de nacimiento</th>
          <th>País de nacimiento (por clave de país)</th>
          <th>Pais de nacionalidad (por clave de país)</th>
          <th>Actividad, ocupación, profesión, actividad o giro del negocio al que se le dedique el cliente o usuario(por clave de actividad)</th>
          <th>Dirección: País(por clave de país)</th>
          <th>Tipo de vialidad</th>
          <th>Calle</th>
          <th>No.ext</th>
          <th>No.int</th>
          <th>C.P.</th>
          <th>Municipio o delegación</th>
          <th>Localidad</th>
          <th>Estado (por número de estado)</th>
          <th>Colonia</th>
          <th>Tratándose de personas que tengan su lugar de residencia en el extrajero y a la vez cuenten con domicilio en territorio nacional en donde puedan recibir correspondencia dirigida a ellas, se deberá asentar en el expediente los datos relativos a dicho domicilio.(on=si,0ff=no)</th>
          <th>Dirección en México: Tipo de vialidad</th>
          <th>Calle</th>
          <th>No.ext</th>
          <th>No.int</th>
          <th>C.P.</th>
          <th>Municipio o delegación</th>
          <th>Localidad</th>
          <th>Estado</th>
          <th>Colonia</th>
          <th>Correo electrónico</th>
          <th>R.F.C</th>
          <th>CURP</th>
          <th>Número de teléfono</th>

          <th>Beneficiario: Nombre(s)</th>
          <th>Apellido paterno</th>
          <th>Apellido materno</th>
          <th>Fecha de nacimiento</th>
          <th>Género (1=masculino, 2=femenino)</th>
          <th>% Porcentaje de propiedad</th>
          <th>Tipo de vialidad</th>
          <th>Calle</th>
          <th>No.ext</th>
          <th>No.int</th>
          <th>C.P.</th>
          <th>Municipio o delegación</th>
          <th>Localidad</th>
          <th>Estado (por número de estado)</th>
          <th>Colonia</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>