<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=malla_ecoi".date('Y-m-d').".xls");	
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>Denominación</th>
          <th>Fecha de establecimiento en Territorio Nacional</th>
          <th>Nacionalidad (por clave de país)</th>
          <th>Clave del Registro Federal de Constribuyentes con homoclave / número de identificación fiscal en otro pais (cuando aplique) y pais que la asignó</th>
          <th>País (por clave de país)</th>
          <th>Dirección: País (por clave de país)</th>
          <th>Tipo de vialidad</th>
          <th>Calle</th>
          <th>No.ext</th>
          <th>No.int</th>
          <th>C.P.</th>
          <th>Municipio o delegación</th>
          <th>Localidad</th>
          <th>Estado (por número de estado)</th>
          <th>Colonia</th>
          <th>Número de teléfono</th>
          <th>Correo electrónico</th>
          <th>Certificado de Matrícula Consular</th>
          <th>DATOS GENERALES DEL APODERADO: Nombre(s)</th>
          <th>Apellido paterno</th>
          <th>Apellido materno</th>
          <th>Género (1=masculino, 2=femenino)</th>
          <th>Fecha de nacimiento</th>
          <th>Nombre de identificación</th>
          <th>Autoridad que emite la identificación</th>
          <th>Número de identificación</th>
          <th>RFC</th>
          <th>CURP</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>