<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=malla_fide".date('Y-m-d').".xls");	
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>Denominación o razón social del fiduciario</th>
          <th>Número / Referencia o identicador del fideicomiso</th>
          <th>R.F.C. del fideicomiso</th>
          <th>DATOS GENERALES DEL APODERADO: Nombre(s)</th>
          <th>Apellido paterno</th>
          <th>Apellido materno</th>
          <th>Nombre de identificación</th>
          <th>Autoridad que emite la identificación</th>
          <th>Número de identificación</th>
          <th>Fecha de nacimiento</th>
          <th>Género (1=masculino, 2=femenino)</th>
          <th>RFC</th>
          <th>CURP</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>