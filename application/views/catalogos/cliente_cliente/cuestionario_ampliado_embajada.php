<style type="text/css">
.check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c;
}
.form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c;
}
.form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #212b4c;
}
.form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
}
.form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
}
</style>
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <!---------------->
          <br>
          <div align="center">
             <h3>CUESTIONARIO AMPLIADO (PARA EMBAJADAS, CONSULADOS U ORGANISMOS INTERNACIONALES Y PERSONAS MORALES DE DERECHO PÚBLICO)</h3>
          </di>
          <hr class="subtitle">
          <br>
          <div class="row">
            <input id="idopera" type="hidden" value="<?php echo $idopera; ?>">
            <div align="left" class="col-md-12 barra_menu">
              <h4>Tipo de persona: Embajadas, consulados, u organismos internacionales y personas morales de derecho público.</h4>
            </div>  
          </div>
          <form class="form" method="post" role="form" id="form_cuestionario" required> 
            <input type="hidden" id="id" name="id" value="<?php if(isset($ca->id)) echo $ca->id; else echo "0"; ?>">
            <input type="hidden" id="tipoc" value="<?php echo $tipo; ?>">
            <input type="hidden" name="id_perfilamiento" id="idperfilamiento" value="<?php echo $idperfilamiento; ?>">
            <input type="hidden" id="idclientec" value="<?php echo $idclientec; ?>"> 
            <input id="idopera" name="id_operacion" type="hidden" value="<?php echo $idopera; ?>">
            <input id="idcgr" type="hidden" name="id_grado_riesgo" value="<?php echo $idcgr; ?>">
            <div align="left" class="row">
              <div class="col-md-12 form-group"></div>
              <div class="col-md-12 form-group">
                <label>Motivo por el cual el fideicomiso solicita la transacción / operación:</label>
                <textarea name="motivo" id="motivo" class="form-control"><?php if(isset($ca)) echo $ca->motivo; ?></textarea>
              </div>
            </div>

            <div class="firma">
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="firma" style="display: none;">
                <div class="row">
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_cli_imp">Firma de Cliente</span></center><br>
                      Doy fé que los datos proporcionados son verídicos y están vigentes
                  </div>
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_ejec_imp">Firma Ejecutivo Comercial / Vendedor</span></center><br>
                      Doy fé que la información capturada en el Cuestionario Ampliado, es proporcionada por el cliente, en forma presencial.
                  </div>  
                  <div class="col-md-12">
                    <br><br><br><br><br><br><br>
                  </div>
                  <div class="col-md-6" style="text-align: justify !important;">
                    <hr>
                    <center><span id="nombre_cli_imp">Firma de aprobación del Director Comercial</span></center><br>
                      Apruebo continuar con la transacción comercial
                  </div>
                </div>
              </div>
            </div> 

            <div class="row">
              <div class="col-md-12" align="right">
                <button type="button" class="btn gradient_nepal2 barra_menu" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Atrás</button>
                <?php if(isset($ca->id) && $ca->id>0){ ?>
                  <button type="button" class="btn gradient_nepal2 barra_menu" onclick="imprimir_for()"><i class="fa fa-print"></i> Imprimir cuestionario</button>
                <?php } ?>
                <button type="button" class="btn gradient_nepal2 barra_menu" id="btn_submit"><i class="fa fa-arrow-right"></i> Siguiente</button>
              </div>
            </div>
          </form>
          
    

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalimprimir" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" align="center">
            <label>¿Deseas imprimir el formulario?                
            </label>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4 form-group">
            <div class="row">
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="1">
                    Si
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="imprimir" value="2" checked>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 btn_registro" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

