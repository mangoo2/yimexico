<span class="id_benef_m" hidden><?php echo $id_benef_m ?></span>
<span class="id_trans_bene" hidden><?php echo $id_trans_bene ?></span>
<span class="id_actividad" hidden><?php echo $idactividad ?></span>
<span class="tipo_persona" hidden><?php echo $tipo_persona ?></span>
<span class="idperfilamiento_cliente" hidden><?php echo $idperfilamiento_cliente ?></span>
<span class="idcientec" hidden><?php echo $idcientec ?></span>


<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
		  <div class="card-body">
		  	<div class="row">
		      <div class="col-md-12">  
		        <h3>Recopilación de documentos del dueño beneficiario de <?php echo $titulo_t ?></h3>
		      </div>
		    </div>  
		    <hr class="subtitle"> 
		    <!--- -->
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
					      <?php echo $text ?><!---->
						</div>
					</div>	    
		        </div>	
	        	<div class="col-md-6">
	        		<form id="form_docs_cliente" class="form">
	        			<?php if(isset($docs)) {?>
				            <input type="hidden" name="id" id="id" value="<?php echo $docs->id ?>">
				          <?php } else { ?>
				          	<input type="hidden" name="id" id="id" value="0">
				          <?php }?>
		        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
						    <div class="card-body">
						    <!-- -->
	                           <div class="row">
					              <div class="col-md-12">
					                <div class="d-flex mb-2">
						                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
						                	<h5>Documentos a recolectar (El nombre de los documentos debe ir sin punto)</h5>
						                </div>
						                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="tamano_ayuda()">
						                  <i class="mdi mdi-help"></i>
						                </button>
              						</div>
					              </div>
					            </div> 
					            <div class="col-md-12">
					            	<span>*Por favor, recolecte una copia digital y coteje contra original los siguientes documentos.</span>
					            </div>
					           
					            <div class="col-md-12 form-group" id="cont1">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Consulta en listado de personas bloqueadas":
					                  <input type="checkbox" class="form-check-input" id="archt_1" onchange="guadar_checkec(1)">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_1" type="file" onchange="guardar_img(1)">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <!--
					            <div class="col-md-12 form-group" id="cont2">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato "Conoce a tu cliente":
					                  <input type="checkbox" class="form-check-input" id="archt_2" onchange="guadar_checkec(2)">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_2" type="file" onchange="guardar_img(2)">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
                                -->
					            <div class="col-md-12 form-group" id="cont3">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Identificación oficial:
					                  <input type="checkbox" class="form-check-input" id="archt_3" onchange="guadar_checkec(3)">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_3" type="file" onchange="guardar_img(3)">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <div class="col-md-12 form-group" id="cont4">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Cédula de identificación fiscal:
					                  <input type="checkbox" class="form-check-input" id="archt_4">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_4" type="file" onchange="guardar_img(4)" onchange="guadar_checkec(4)">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <div class="col-md-12 form-group" id="cont9">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">CURP:
					                  <input type="checkbox" class="form-check-input" id="archt_5" onchange="guadar_checkec(5)">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_5" type="file" onchange="guardar_img(5)" >
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <div class="col-md-12 form-group" id="cont10">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Comprobante de domicilio:
					                  <input type="checkbox" class="form-check-input" id="archt_6" onchange="guadar_checkec(6)">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_6" type="file" onchange="guardar_img(6)">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <!--
					            <div class="col-md-12 form-group" id="cont11">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Formato consultas de dueño beneficiario:
					                  <input type="checkbox" class="form-check-input" id="archt_7" onchange="guadar_checkec(7)">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_7" type="file" onchange="guardar_img(7)"> 
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <div class="col-md-12 form-group" id="cont6">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Carta poder:
					                  <input type="checkbox" class="form-check-input" id="archt_8" onchange="guadar_checkec(8)">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="arch_8" type="file" onchange="guardar_img(8)">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            -->
					            <div class="row row_file valid">
	                        		<div class="form-check form-check-purple">
					                	<label class="form-check-label"><h4>Documentos Validados</h4>
					                  	<input  type="checkbox" id="validado" class="form-check-input" name="validado" value="">
					                	</label>
					              	</div>
					          	</div>
					          	
						    </div>
						</div>
						<br>
			            <div class="row"> 
				            <div class="col-md-12" align="right">
				              <button class="btn btn-yimexico pull-right" id="btn_submit" type="button" onclick="guardar_documentos()">siguiente</button>
				            </div>
			          	</div>
					</form>    	
	        	</div>

	        </div>
            <!-- -->
	    </div>
	</div>
</div>

<div class="modal fade" id="modal_carga_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Tamaño y caracteristicas en carga de archivos</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>El tamaño maximo es de 2.5mb por archivo, los nombres de los documentos no deben presentar puntos medios, los 
           formaros permitidos son: "jpg", "png", "pdf", "jpeg"</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>