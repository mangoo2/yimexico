
<span class="idopera" hidden><?php echo $idopera ?></span>
<span class="idperfilamiento" hidden><?php echo $idperfilamiento_cliente ?></span>
<span class="idclientec" hidden><?php echo $idclientec ?></span>


<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
		  <div class="card-body">
		  	<div class="row">
		      <div class="col-md-9">  
		        <h3>Acuse de documento de transacción sin dueño beneficiario</h3>
		      </div>
		      <div class="col-md-3" align="right">
		      	<?php if($idopera>0) { ?>
		      		<a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $idopera; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</button></a>
		      	<?php } else { ?>	
              		<button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar</button>
              	<?php } ?>	
            </div> 
		    </div>  
		    <hr class="subtitle"> 
		    <!--- -->
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
					    <div class="card-body">
					    	<h4>Operación: <?php echo /*$idopera;*/ $folio; ?></h4>
					    	<h4>Cliente: <?php echo $nombre; ?></h4>
						</div>
					</div>	    
		        </div>	
		         
	        	<div class="col-md-6">
	        		<form id="form_doc_sin" class="form">
	        			<?php if(isset($doc)) { ?>
				            <input type="hidden" name="id" id="id" value="<?php echo $doc->id ?>">
				          <?php } else { ?>
				          	<input type="hidden" name="id" id="id" value="0">
				          <?php } ?>
		        		<div class="card" style="border-radius: 10px; box-shadow: 2px 2px 10px #666;">
						    <div class="card-body">
						    <!-- -->
	                           <div class="row">
					              <div class="col-md-12">
					                <div class="d-flex mb-2">
						                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
						                	<h5>Documentos a recolectar (El nombre de los documentos debe ir sin punto)</h5>
						                </div>
						                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="tamano_ayuda()">
						                  <i class="mdi mdi-help"></i>
						                </button>
              						</div>
					              </div>
					            </div> 
					            <div class="col-md-12 form-group" id="cont1">
					              <div class="form-check form-check-purple">
					                <label class="form-check-label">Acuse - constancia de inexistencia de dueño beneficiario:
					                  <input type="checkbox" class="form-check-input" id="archt_1">
					                </label>
					              </div>
					              <div class="row row_file">
						              <div class="col-md-10">
						                <input width="50px" height="80px" class="img" id="sin_bene" type="file">
						              </div>
						              <div class="col-md-10"></div>
						           </div>
					            </div>
					            <?php 
					            $idcliente_usuario=$this->session->userdata('idcliente_usuario');
					            $tipo=$this->session->userdata('tipo');
					            $menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,2);
					            $perfilid=$this->session->userdata('perfilid');
								//echo "perfilid: ".$perfilid;
					            if($perfilid==3){
					            	echo '<div class="row row_file valid" style="display:none">
				                        		<div class="form-check form-check-purple">
								                	<label class="form-check-label"><h4>Documentos Validados</h4>
								                  	<input  type="checkbox" id="validado" class="form-check-input" name="validado" value="">
								                	</label>
								              	</div>
								          	</div>';
					            }else{
					            	foreach ($menu as $x) { 
					            		//echo "id_menusub: ".$x->id_menusub;
						                if($x->id_menusub==16){ // Valdiar docs
						                  	if($x->check_menu==1){ ?>
						                    	<div class="row row_file valid">
					                        		<div class="form-check form-check-purple">
									                	<label class="form-check-label"><h4>Documentos Validados</h4>
									                  	<input  type="checkbox" id="validado" class="form-check-input" name="validado" value="">
									                	</label>
									              	</div>
									          	</div>
						                 	<?php }
						                }
						            } 
					            }
	                        	?>
						    </div>
						</div>
						<br>
			            <div class="row"> 
				            <div class="col-md-12" align="right">
				              <button class="btn gradient_nepal2" id="btn_submit" type="button" onclick="guarda_acuse()"><i class="fa fa-save"></i> Guardar </button>
				            </div>
			          	</div>
					</form>    	
	        	</div>

	        </div>
            <!-- -->
	    </div>
	</div>
</div>

<div class="modal fade" id="modal_carga_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Tamaño y caracteristicas en carga de archivos</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>El tamaño maximo es de 2.5mb por archivo, los nombres de los documentos no deben presentar puntos medios, los 
           formaros permitidos son: "jpg", "png", "pdf", "jpeg"</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>