<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">¿Desea agregar otro cliente a esta operación?</h1>
          <br>
          <div class="col-md-12" align="center">
            <button type="button" class="btn gradient_nepal2" id="si_mas"><i class="fa fa-check"></i> Si</button>
            <button type="button" class="btn btn_yimexicov2" id="no_mas"><i class="fa fa-close"></i> No</button>
          </div>
          <br><br>
      </div>
    </div>
  </div>
</div>
