<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $id_operacion; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-rocket"></i> Regresar a Proceso</button></a>
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">Clientes asignados a la operación <strong><?php echo /*$id_operacion;*/ $folio; ?></strong></h1>
          <br>
          <div class="col-md-12">
            <h3 class="barra_menu">Lista de Clientes:</h3>
            <?php $btn_sig_bene=""; $title=""; if($cont_db==0){ $btn_sig_bene="disabled"; $title='title="Asignar un dueño beneficiario o declarar sin dueño beneficiario para poder avanzar"'; }
            foreach ($info as $k) {
              //echo "<br>perfila: ".$k->id_perfilamiento;
              $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$k->id_perfilamiento));
              foreach ($get_pp as $g) {
                $btn_pep="";
                //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                $tipoccon = $g->idtipo_cliente;
                //echo "<br>tipoccon: ".$tipoccon;
                if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                if($tipoccon==6) $tabla = "tipo_cliente_f";
                //echo "<br>tabla: ".$tabla;
                $get_result=$this->ModeloCatalogos->getselectwherestatus("resultado","historico_consulta_pb",array('id_perfilamiento'=>$g->idperfilamiento,"id_operacion"=>$id_operacion));
                foreach ($get_result as $r) {
                  $btn_pep="";
                  //echo "$r->resultado: ".strlen($r->resultado);
                  if(strlen($r->resultado)>78){
                    if($tipoccon==1 || $tipoccon==2)
                      $btn_pep="<a target='_blank' href='".base_url()."Perfilamiento/formulariopep/".$k->id_perfilamiento."/".$tipoccon."' title='Ver datos de Cliente' class='btn gradient_nepal2'><i class='fa fa-book'></i> Cuestionario PEP</a>";
                    else
                      $btn_pep="<a target='_blank' href='".base_url()."Perfilamiento/formulariopep_moral/".$k->id_perfilamiento."/".$tipoccon."' title='Ver datos de Cliente' class='btn gradient_nepal2'><i class='fa fa-book'></i> Cuestionario PEP</a>";
                  }else{
                    $btn_pep="";
                  }
                }

                $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
                foreach ($get_per as $g2) {
                  //echo "<br>k->id_cliente: ".$k->id_cliente;
                  if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
                  if($tipoccon==3) $nombre = $g2->razon_social;
                  if($tipoccon==4) $nombre = $g2->nombre_persona;
                  if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
                  echo "<br><div class='col-md-12'>
                    <table width='100%' class='table'>
                      <tr>
                        <td width='20%'>
                          <h3 class='barra_menu'>".$nombre."</h3>
                        </td>
                        <td width='40%'>
                          <!--<button type='button' title='Eliminar Cliente de operación' class='btn gradient_nepal2' onclick='eliminaClie(".$k->id.")'><i class='fa fa-trash'></i></button>-->
                          <a target='_blank' href='".base_url()."Perfilamiento/vizualizar/".$k->id_perfilamiento."/".$tipoccon."/".$id_operacion."' title='Ver datos de Cliente' class='btn gradient_nepal2'><i class='fa fa-eye'></i> Ver Cliente</a>

                          ".$btn_pep."
                        </td>
                      </tr>
                    </table>
                    <br> ";
                    /*if($id_union==0){
                      echo "<a href='".base_url()."Operaciones/asignarBene/".$id_operacion."/".$k->id_clientec."/".$k->id_perfilamiento."/".$tipoccon."/".$id_union."' title='Declarar con/sin Dueño Beneficiario' class='btn gradient_nepal2'><i class='fa fa-user'></i> Dueño Beneficiario</a>";
                    }*/
                  echo "</div>";
                }
              }
            } 
            /*if($id_union>0){
              echo "<div class='col-md-12'><a href='".base_url()."Operaciones/asignarBene/".$id_operacion."/".$k->id_clientec."/".$k->id_perfilamiento."/".$tipoccon."/".$id_union."' title='Declarar con/sin Dueño Beneficiario' class='btn gradient_nepal2'><i class='fa fa-user'></i> Dueño Beneficiario</a></div>";
            }*/
                ?>
          </div>
          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<a href="<?php echo base_url();?>Operaciones" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Atrás</a>-->
              <a  href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>"><button <?php /*echo $btn_sig_bene;*/ echo " ".$title; ?> class="btn gradient_nepal2" type="button"><i class="fa fa-arrow-right"></i> Aceptar</button></a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
