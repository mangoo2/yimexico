<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #212b4c;
  }
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #212b4c;
  }
  .desactivado{
    background-color:#cfc5c4;
  }
  .btn_operacion{
    border-radius: 60px; 
    width: 180px;
    height: 125px;
    color: white;
    background-image: linear-gradient(177deg, #b17f4a, #212b4c) !important;
  }
  .btn_mas{
    border-radius: 60px; 
    width: 30px;
    height: 25px;
    color: white;
    background-image: linear-gradient(177deg, #b17f4a, #212b4c) !important;
  }
  .btn-outline-dark.disabled, .btn-outline-dark:disabled {
    color: #ffffff;
    background-color: transparent;
  }
  
</style>
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-8">
            <h3 class="barra_menu">Operación</h3>
          </div>
          <div class="col-lg-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div>           
        </div>
        <hr class="subtitle">
        <input id="id" type="hidden" <?php if(isset($id)) echo "value='$id'"; ?>>
        <input id="idcliente" type="hidden" value="<?php echo $idcliente; ?>">
        <input id="idclientec" type="hidden" value="<?php echo $idclientec; ?>">
        <input id="idperfilamiento" type="hidden" value="<?php echo $idperfilamiento; ?>">
        <input id="tipo_cliente" type="hidden" value="<?php echo $tipo_cliente->idtipo_cliente; ?>">
        <input id="id_ob" type="hidden" value="0">
        <input id="id_bene" type="hidden" value="0">
        <input id="tipo_bene" type="hidden" value="0">
        <div class="row firma_tipo_cliente" style="display: none">
          <div class="col-md-12">    
            <h3>Formato: Acuse de inexistencia de dueño beneficiario </h3>
            <h3>Fecha: <?php echo date("m-d-Y"); ?></h3><br>
            <?php 
            if($tipo_cliente->idtipo_cliente==1 || $tipo_cliente->idtipo_cliente==2)
                $nombre = $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno;
            if($tipo_cliente->idtipo_cliente==3 || $tipo_cliente->idtipo_cliente==4 || $tipo_cliente->idtipo_cliente==5 || $tipo_cliente->idtipo_cliente==6)
              $nombre = $info->nombre_g." ".$info->apellido_paterno_g." ".$info->apellido_materno_g;
            ?>
            <span>DECLARA LA NO EXISTENCIA DEL DUEÑO BENEFICIARIO PARA LA OPERACIÓN CON NÚMERO <strong><?php echo $id; ?></strong> A REALIZARSE CON <strong><?php echo $nombre ?></strong></span>
          </div>
        </div> 
        <div class="row barra_menu">
          <div class="col-lg-4">
            <h4> Operación: <?php echo $id; ?></h4>
          </div>
          <div class="col-lg-8">
            <?php if($tipo_cliente->idtipo_cliente==1 || $tipo_cliente->idtipo_cliente==2){ ?>
              <div class="col-md-8" align="left">
                <h4>Nombre:
                <?php echo $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno."</h4>"; ?>
              </div>
            <?php } ?>
            <?php if($tipo_cliente->idtipo_cliente==3){ ?> 
              <div align="left" class="col-md-8">
                <h4>Razón social:
                <?php echo $info->razon_social."</h4>"; ?>
              </div>
            <?php } ?>
            <?php if($tipo_cliente->idtipo_cliente==4){ ?> 
              <div align="left" class="col-md-8">
                <h4>Razón social:
                <?php echo $info->nombre_persona."</h4>"; ?>
              </div>
            <?php } ?>
            <?php if($tipo_cliente->idtipo_cliente==5 || $tipo_cliente->idtipo_cliente==6){ ?> 
                <div align="left" class="col-md-8">
                  <h4>Denominación:
                  <?php echo $info->denominacion."</h4>"; ?>
                </div>
            <?php } ?>
          </div>
          <div align="left" class="col-md-12 barra_menu">
            <?php if($tipo_cliente->idtipo_cliente==1){ ?> 
            <h4>Persona fisica mexicana o extranjera con condición de estancia temporal permanente.</h4>
            <?php }else if($tipo_cliente->idtipo_cliente==2){ ?>
                <h4>Persona fisica extranjera con condición de estancia de visitante.</h4>
            <?php }else if($tipo_cliente->idtipo_cliente==3){ ?>
            <h4>Persona moral de nacionalidad mexicana, extranjera y persona moral mexicana del derecho público.</h4>
            <?php }else if($tipo_cliente->idtipo_cliente==4){ ?>
            <h4>Persona moral mexicana del derecho público (Anexo 7 Bis A).</h4>
            <?php }else if($tipo_cliente->idtipo_cliente==5){  ?>
            <h4>Embajadas, consulados u organismo internacionales.</h4>
            <?php }else if($tipo_cliente->idtipo_cliente==6){ ?>
                <h4>Fideicomisos.</h4>
            <?php } ?>  
          </div> 
        </div>
        <br>
        <div class="firma_tipo_cliente">
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <div class="firma_tipo_cliente" style="display: none;">
            <div class="row">
              <div class="col-md-6" style="text-align: justify !important;">
                <hr>
                <center><span id="nombre_cli_imp"><?php echo $nombre; ?></span></center><br>
                  Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
              </div>
              <div class="col-md-6" style="text-align: justify !important;">
                <hr>
                <center><span id="nombre_ejec_imp"></span></center><br>
                  Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados  
              </div>  
            </div>
          </div>
        </div> 
        <div class="col-md-12 barra_menu" align="center" id="barr_men">
          <span id="table_prin">
            <table width="100%" id="tabla_principal">
              <thead>
                <tr>
                  <td width="16.66%">
                    <button class="btn btn-icon-text btn_operacion" id="btn_principal" onclick="paso1(0)">
                      <i class="mdi mdi-account btn-icon-prepend mdi-36px"></i><br>
                      <span class="d-inline-block text-left">
                        <h4 align="center" style="color: white">Cliente</h4>
                      </span>
                    </button>
                  </td>
                  <td width="16.66%">
                    <button disabled class="btn btn-outline-dark btn-icon-text btn_operacion" onclick="paso2(0)" id="db1">
                      <i class="mdi mdi-account-plus btn-icon-prepend mdi-36px"></i><br>
                      <span class="d-inline-block text-left">
                        <h4 align="center" style="color: white">Dueño beneficiario</h4>
                      </span>
                    </button>
                  </td>
                  <td width="16.66%">
                    <button disabled class="btn btn-outline-dark btn-icon-text btn_operacion" onclick="paso3(0)" id="gr1">
                      <i class="mdi mdi-alert btn-icon-prepend mdi-36px"></i><br>
                      <span class="d-inline-block text-left">
                        <h4 align="center" style="color: white">Grado de riesgo</h4>
                      </span>
                    </button>
                  </td>
                  <td width="16.66%">
                    <button disabled class="btn btn-outline-dark btn-icon-text btn_operacion" onclick="paso4(0)" id="ec1">
                      <i class="mdi mdi-file-account btn-icon-prepend mdi-36px"></i><br>
                      <span class="d-inline-block text-left">
                        <h4 align="center" style="color: white">Expediente cliente</h4>
                      </span>
                    </button>
                  </td>
                  <td width="16.66%">
                    <button disabled class="btn btn-outline-dark btn-icon-text btn_operacion" onclick="paso5(0)" id="edb1">
                      <i class="mdi mdi-file-account btn-icon-prepend mdi-36px"></i><br>
                      <span class="d-inline-block text-left">
                        <h4 align="center" style="color: white">Expediente DB</h4>
                      </span>
                    </button>
                  </td>
                  <td width="16.66%">
                    <button disabled class="btn btn-outline-dark btn-icon-text btn_operacion" onclick="paso6()" id="tran">
                      <i class="mdi mdi-camera-iris btn-icon-prepend mdi-36px"></i><br>
                      <span class="d-inline-block text-left">
                        <h4 align="center" style="color: white">Transacción</h4>
                      </span>
                    </button>
                  </td>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <td width="16.66%">
                    <center>
                    <button class="btn_mas" onclick="paso1_1()">
                      <i class="mdi mdi-plus btn-icon-prepend mdi-6px"></i>
                    </button></center>
                  
                  </td>
                  <!--<td width="16.66%">
                    <center>
                    <button class="btn_mas" onclick="paso2('2_1')">
                      <i class="mdi mdi-plus btn-icon-prepend mdi-6px"></i>
                    </button></center>
                    <?php 
                    /*$dataa=$this->ModeloCliente->verificaOperacionBenes($id,$idperfilamiento);
                    foreach ($dataa as $k) {
                      $tipo_bene = $k->tipo_bene_opera;
                      $id_duenio_bene_nvo=$k->id_duenio_bene_nvo;
                      echo '<td width="16.66%">
                        <button disabled class="btn btn-outline-dark btn-icon-text btn_operacion" onclick="paso2(0)" id="db1">
                          <i class="mdi mdi-account-plus btn-icon-prepend mdi-36px"></i><br>
                          <span class="d-inline-block text-left">
                            <h4 align="center" style="color: white">Dueño beneficiario</h4>
                          </span>
                        </button><br>
                      </td>';
                    }  */ ?>
                  </td>-->
                  <td width="16.66%">
                    <br>
                    <div class="col-md-12 infobene0 barra_menu"><br></div>
                  </td>
                </tr>
              </tfoot>
            </table>
            <div class="row">
              <br><br>
              
              <div class="col-md-2 infocliente0 barra_menu">
                
              </div>
              <div class="col-md-3" align="right">
                <br><br>
              </div>
            </div>
          </span>
        </div><br> 

        <!--<div class="row">

          <br><br>
          <div class="col-md-2 infocliente0 barra_menu">
            
          </div>
          <div class="col-md-2 infobene0 barra_menu">
            
          </div>
          <div class="col-md-3" align="right">
            <br><br>
          </div>
        </div>-->
      </div>
    </div>
  </div>
</div>          


<div class="modal fade barra_menu" id="modal_bene" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header barra_menu">
        <h4 class="modal-title">Dueño Beneficiario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <!--<h5>Elige un beneficiario</h5>-->
        <div class="row">
          <div class="col-md-5"></div>
          <div class="col-md-7">
            <div class="form-check form-check-flat form-check-primary">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" id="sin_beneficiario">
                  Sin dueño beneficiario
              <i class="input-helper"></i></label>
            </div>
            <button style="display: none" type="button" class="btn btn-secondary btn-impresion barra_menu" onclick="imprimir_formato1()"><i class="fa fa-print"></i> Imprimir formato conoce dueño beneficiario</button>
          </div>
        </div>
      </div>
      <div class="col-md-12 form-group" id="cont_select_bene">
        <span class="text_select_beneficiario">
            <span class="select_beneficiario"></span>
        </span>
      </div>
      <div class=" col-md-12 form-group" style="display: none" id="cont_tipo_bene">
        <h5 align="center">Elige un tipo de beneficiario</h5>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Física  
            <input type="radio" class="form-check-input" name="tipoben" id="fisica" value="1">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Moral  
            <input type="radio" class="form-check-input" name="tipoben" id="moral" value="2">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Fideicomiso  
            <input type="radio" class="form-check-input" name="tipoben" id="fideicomiso" value="3">
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 barra_menu" onclick="aceptarAdd()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2 barra_menu" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="transacción_actividades" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title ddaa">Transacciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <span class="transaccion_acti"></span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="aceptarAct()" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bd-example-modal-md" id="modal_mas_clientes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title ddaa">Agregar cliente - operación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <span class="select_clientes"></span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="aceptarCliente()" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row">

</div>