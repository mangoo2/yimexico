<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
</style>
<input type="hidden" id="mes_aux" value="<?php echo date("m"); ?>">
<div class="row">
      <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <h3>Aviso 24 hrs.</h3>  
            <h4>Registro de aviso</h4>
          </div>
        </div>
        <hr class="subtitle">
        <form id="form_aviso" class="forms-sample">
          <!-- -->
          <input type="hidden" id="id_perfilamiento" name="id_perfilamiento" value="<?php echo $idperfilamiento; ?>">
          <input type="hidden" id="id_opera" value="<?php echo $ido; ?>">
          <input type="hidden" id="idtran" value="<?php echo $idtran; ?>"> 
          <input type="hidden" id="idpagoa" value="<?php echo $idpagoa; ?>"> 
          <input type="hidden" id="desdebit" value="<?php echo $desdebit; ?>"> 
          <div class="row">
            <div class="col-md-2 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-2 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Actividad a reportar:</label>
              <select class="form-control" name="actividad" id="id_act">
                <?php $sel=""; foreach ($actividades as $av) { ?>
                    <option value="<?php echo $av->idactividad?>" <?php if($id_act==$av->idactividad) echo 'selected' ?> ><?php echo $av->nombre?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-5 form-group">
              <label>Tipo de Alerta:</label>
              <select class="form-control" name="id_alerta" id="id_alerta">

              </select>
            </div>
            <div class="col-md-11 form-group" id="cont_otra" style="display: none">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="exampleInputUsername1">Descripción de alerta</label>
                  <input type="text" class="form-control" id="descripcion" >
                </div>
              </div>
            </div>
          </div> 
        </form>            
        <!-- -->
        <div class="row"> 
          <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
              <?php $num_cliente=0;
                if($tipoc==6){ 
                echo mb_strtoupper($cc->denominacion);
                  $num_cliente=$cc->idtipo_cliente_f;
                }
                if($tipoc==3){
                echo mb_strtoupper($cc->razon_social);  
                  $num_cliente=$cc->idtipo_cliente_p_m_m_e;
                }
                if($tipoc==4){
                echo mb_strtoupper($cc->nombre_persona);
                  $num_cliente=$cc->idtipo_cliente_p_m_m_d;
                }
                if($tipoc==5){
                echo mb_strtoupper($cc->denominacion);
                  $num_cliente=$cc->idtipo_cliente_e_c_o_i;
                }
                if($tipoc==1 || $tipoc==2){
                echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                  if($tipoc==1){
                    $num_cliente=$cc->idtipo_cliente_p_f_m;
                  }
                  if($tipoc==2){
                    $num_cliente=$cc->idtipo_cliente_p_f_e;
                  }
                }
              ?> 
              </span>
          </div>
          <div class="col-md-6 form-group">
            <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
            <span style="font-size: 25px; color: #b57532;">
              <?php echo $num_cliente ?>
            </span>  
          </div>
        </div>
        
     
      <div class="modal-footer">
        <button class="btn gradient_nepal2" type="button" onclick="registrar_aviso()"><i class="fa fa-save"></i> Registrar aviso</button>
        <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
      </div>

    </div>
  </div>
</div>
