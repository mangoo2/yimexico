<?php 
  $perfilid=$this->session->userdata('perfilid'); 
  $idcliente_usuario=$this->session->userdata('idcliente_usuario');
  $menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,4);
?>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Operaciones</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>    
        <hr class="subtitle">
        <div class="col-md-12" align="center">
          <input id="idcliente" type="hidden" value="<?php echo $idcliente; ?>">
          <?php 
            if($perfilid==3){
              echo '<a href="'.base_url().'Perfilamiento/index/1"><button style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-user-plus btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Alta de Cliente</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
               echo '<a href="'.base_url().'Clientes_cliente/union/0/1"><button  style="width: 325px;" class="btn gradient_nepal tam_btn">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-user-plus btn-icon-prepend icon_yi2"></i><i class="fa fa-user btn-icon-prepend icon_yi2"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Mancomunar Clientes</span>
                        </div>
                      </div>  
                    </button></a>&nbsp;&nbsp;';
              echo '<button style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="opera_exist()">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="mdi mdi-rocket btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Nueva Operación</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;';
              echo '<br><br><button style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="href_aviso_cero()">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Aviso en ceros</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;';
                    echo '<button style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="href_aviso_exento()">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="fa fa-external-link btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Aviso exentos</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;';
              
            }else{  
              echo '<a href="'.base_url().'Perfilamiento/index/1"><button style="width: 325px;" class="btn gradient_nepal tam_btn">
                  <div class="row texto_centro">
                    <div class="col-md-3" align="right">
                      <i class="fa fa-user-plus btn-icon-prepend icon_yi"></i>
                    </div>
                    <div class="col-md-9" align="left">
                      <span>Alta de Cliente</span>
                    </div>
                  </div>  
                </button></a>&nbsp;&nbsp;&nbsp;&nbsp;';
              echo '<a href="'.base_url().'Clientes_cliente/union/0/1"><button  style="width: 325px;" class="btn gradient_nepal tam_btn">
                  <div class="row texto_centro">
                    <div class="col-md-3" align="right">
                      <i class="fa fa-user-plus btn-icon-prepend icon_yi2"></i><i class="fa fa-user btn-icon-prepend icon_yi2"></i>
                    </div>
                    <div class="col-md-9" align="left">
                      <span>Mancomunar Clientes</span>
                    </div>
                  </div>  
                </button></a>&nbsp;&nbsp;&nbsp;&nbsp;';
              foreach ($menu as $x) { 
                if($x->id_menusub==9){ // Mis datos
                  if($x->check_menu==1){
                    echo '<button style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="opera_exist()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="mdi mdi-rocket btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Nueva Operación</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                if($x->id_menusub==10){ // Mis datos
                  if($x->check_menu==1){
                    echo '<br><br><button  style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="href_aviso_cero()">
                          <div class="row texto_centro">
                            <div class="col-md-3" align="right">
                              <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                            </div>
                            <div class="col-md-9" align="left">
                              <span>Avisos en ceros</span>
                            </div>
                          </div>  
                        </button>&nbsp;&nbsp;';
                    echo '<button style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="href_aviso_exento()">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class=fa fa-external-link btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Avisos exentos</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;';
                  }
                }
              }
            }
            ?>        
          <!-- 6 Director General --><!-- 7-Responsable Cumplimiento --><!-- 9-Ejecutivo Comercial --><!-- 10-Ejecutivo de Operación -->   
          <?php if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==9 || $perfilid==10){?>
            <?php /* if($perfilid!=10){ ?>   
              <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px" onclick="opera_nvo()">
                <i class="mdi mdi-account-plus btn-icon-prepend mdi-36px"></i><br>
                <span class="d-inline-block text-left">
                  <h4 align="center">Operación de cliente nuevo</h4>
                </span>
              </button>
            <?php } */  /*?>
            <button style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="opera_exist()">
              <div class="row texto_centro">
                <div class="col-md-3" align="right">
                  <i class="mdi mdi-rocket btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-9" align="left">
                  <span>Nueva Operación</span>
                </div>
              </div>  
            </button>
            
            <button  style="width: 325px;" class="btn gradient_nepal tam_btn" onclick="href_aviso_cero()">
              <div class="row texto_centro">
                <div class="col-md-3" align="right">
                  <i class="mdi mdi-checkbox-blank-circle-outline btn-icon-prepend icon_yi"></i>
                </div>
                <div class="col-md-9" align="left">
                  <span>Aviso en ceros</span>
                </div>
              </div>  
            </button>
          <?php */ } ?> 
          <?php if($perfilid==7){ /*?><!-- 7-Responsable Cumplimiento -->
            <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px"  onclick="href_generacion_xml()">
              <i class="mdi mdi-note-multiple-outline btn-icon-prepend mdi-36px"></i><br>
              <span class="d-inline-block text-left">
                <h4 align="center">Generación XMLS</h4>
              </span>
            </button>
            <hr>
          <?php */ } ?> 

        </div><br> 
        <div class="table-responsive">
          <table class="table" id="table_opera">
            <thead>
              <tr><th colspan="11" id="tit_rango" style="background-color: #b07e49; color:white; text-align: center; font-weight: bold">LISTA DE OPERACIONES REALIZADAS Y ORGANIZADAS POR NÚMERO DE OPERACIÓN</th></tr>
              <tr>
                <th>No. de Operación</th>
                <th>Fecha de inicio de operación</th>
                <th>Cliente</th>
                <!--<th>Estatus</th>-->
                <th>Subprocesos</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-md-12" align="right">
            <br>
            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Inicio_cliente"><i class="fa fa-reply"></i> Regresar </a> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>          
<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_aviso_cero" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aviso en ceros</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>¿Deseas eliminar este cliente?</h5>
      </div>
      <input type="hidden" id="idcliente">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-rounded btn-fw" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-yimexico btn-rounded btn-fw">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="detalles_alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel1">Alertas Generadas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 form-group">
                <h4>Descripción:</h4>
              </div>
              <div class="col-md-12 form-group">
                <span aria-hidden="true" class="body_alert"></span>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn grey btn-outline-primary" data-dismiss="modal">Aceptar</button>
            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
    </div>
</div>