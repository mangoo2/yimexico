<input type="hidden" id="id_opera" value="<?php echo $id_operacion; ?>">
<input type='hidden' id="desde_cal" value="<?php echo $desde_cal; ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $id_operacion; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-rocket"></i> Regresar a Proceso</button></a>
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">Calcular grado de riesgo - Clientes asignados a la operación <strong><?php echo /*$id_operacion;*/ $folio; ?></strong></h1>
          <br>
          <div class="col-md-12">
            <h3 class="barra_menu">Lista de Clientes:</h3><hr class="">
            <?php $cont_cli=0; 
            foreach ($info as $k) {
              $cont_cli++; //echo "<br>cont_cli: ".$cont_cli;
            }
            foreach ($info as $k) {
              //echo "<br>perfila: ".$k->id_perfilamiento;
              $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$k->id_perfilamiento));
              foreach ($get_pp as $g) {
                //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                $tipoccon = $g->idtipo_cliente;
                //echo "<br>tipoccon: ".$tipoccon;
                if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                if($tipoccon==6) $tabla = "tipo_cliente_f";
                //echo "<br>tabla: ".$tabla;
                $act_clic=0;
                $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
                foreach ($get_per as $g2) {
                  //echo "<br>k->id_cliente: ".$k->id_cliente;
                  if($tipoccon==1){ $idcc = $g2->idtipo_cliente_p_f_m; $tipocc=1; $act_clic=$g2->activida_o_giro;}
                  if($tipoccon==2){ $idcc = $g2->idtipo_cliente_p_f_e; $tipocc=1; $act_clic=$g2->actividad_o_giro;}
                  if($tipoccon==3){ $idcc = $g2->idtipo_cliente_p_m_m_e; $tipocc=2;}
                  if($tipoccon==4){ $idcc = $g2->idtipo_cliente_p_m_m_d; $tipocc=2; $act_clic=$g2->giro_mercantil;}
                  if($tipoccon==5){ $idcc = $g2->idtipo_cliente_e_c_o_i; $tipocc=0; $act_clic=$g2->giro_mercantil;}
                  if($tipoccon==6){ $idcc = $g2->idtipo_cliente_f; $tipocc=3;}
                  if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
                  if($tipoccon==3) $nombre = $g2->razon_social;
                  if($tipoccon==4) $nombre = $g2->nombre_persona;
                  if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
                  $btn_ca="";
                  $bandca=0;
                  $bandcaP=0;
                  $pep_exist=0;
                  $nopep_exist=0;
                  $get_gr=$this->General_model->get_tableRowC('grado,id,id_actividad,grado','grado_riesgo_actividad',array('id_operacion'=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento));
                  if(isset($get_gr->grado) && $get_gr->grado>2.0){ //AQUI DEBE IR QUE TIPO DE CLIENTE FUE -- 
                    $get_gr=$this->General_model->get_tableRowC('idtipo_cliente,grado,id,id_actividad','grado_riesgo_actividad',array('id_operacion'=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento));
                    //echo "idtipo_cliente: ".$get_gr->idtipo_cliente."<br>";
                    //echo "grado: ".$get_gr->grado."<br>";
                    if(isset($get_gr)){ //ESTE ES PARA CLIENTES NO PEPS - agregar validacion que sea tipo no pep
                      if($get_gr->idtipo_cliente==5){
                        //$btn_ca.="<a href='".base_url()."Clientes_cliente/cuestionario_pep/".$id_operacion."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i></a>";
                        $btn_ca.="<a href='".base_url()."Clientes_cliente/cuestionario_pep/".$idcc."/".$g->idperfilamiento."/".$tipoccon."/".$id_operacion."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i> Cuestionario Alto Riesgo</a>";
                        $cuest_amp_pep = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado_pep',array('id_operacion'=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento));
                        foreach ($cuest_amp_pep as $key) {
                          $bandcaP=1;
                        }
                        $pep_exist=1;
                        /*echo "bandcaP: ".$bandcaP."<br>";
                        echo "pep_exist: ".$pep_exist."<br>";*/
                      }else if($tipoccon==1 || $tipoccon==2){ //ok
                        $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliado/".$idcc."/".$g->idperfilamiento."/".$tipoccon."/".$id_operacion."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i> Cuestionario Alto Riesgo</a>";
                        $cuest_amp = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado',array('id_operacion'=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento));
                        foreach ($cuest_amp as $key) {
                          $bandca=1;
                        }
                        $nopep_exist=1;
                        /*echo "bandca: ".$bandca."<br>";
                        echo "nopep_exist: ".$nopep_exist."<br>";*/
                      }
                      //else if($tipoccon==3 || $tipoccon==4){ //morales
                      else if($tipoccon==3){ //morales
                        $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliadoMoral/".$idcc."/".$g->idperfilamiento."/".$tipoccon."/".$id_operacion."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i> Cuestionario Alto Riesgo</a>";
                        $cuest_amp = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado_moral',array('id_operacion'=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento));
                        foreach ($cuest_amp as $key) {
                          $bandca=1;
                        }
                        $nopep_exist=1;
                        /*echo "bandca: ".$bandca."<br>";
                        echo "nopep_exist: ".$nopep_exist."<br>";*/
                      }
                      else if($tipoccon==6){ //fideicomiso
                        $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliadoFide/".$idcc."/".$g->idperfilamiento."/".$tipoccon."/".$id_operacion."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i> Cuestionario Alto Riesgo</a>";
                        $cuest_amp = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado_fide',array('id_operacion'=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento));
                        foreach ($cuest_amp as $key) {
                          $bandca=1;
                        }
                        $nopep_exist=1;
                        /*echo "bandca: ".$bandca."<br>";
                        echo "nopep_exist: ".$nopep_exist."<br>";*/
                      }
                      else if($tipoccon==4 || $tipoccon==5){ //embajadas
                        $btn_ca="<a href='".base_url()."Clientes_cliente/cuestionarioAmpliadoEmbajada/".$idcc."/".$g->idperfilamiento."/".$tipoccon."/".$id_operacion."/".$get_gr->id."/".$tipocc."/".$act_clic."' title='Cuestionario ampliado de grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-edit'></i> Cuestionario Alto Riesgo</a>";
                        $cuest_amp = $this->ModeloCatalogos->getselectwherestatus('*','cuestionario_ampliado_embajada',array('id_operacion'=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento));
                        foreach ($cuest_amp as $key) {
                          $bandca=1;
                        }
                        $nopep_exist=1;
                        /*echo "bandca: ".$bandca."<br>";
                        echo "nopep_exist: ".$nopep_exist."<br>";*/
                      }

                      $btn_ca.=" <a href='".base_url()."Clientes_cliente/diagnostico_alertas/".$id_operacion."/".$g->idperfilamiento."' title='Diagnóstico de Alertas' class='btn gradient_nepal2'><i class='fa fa-warning'></i> Guia de alertas</a>";
                    }
                  }
                  if(isset($get_gr->grado) && $get_gr->grado>=1.8 && $get_gr->grado<=2.0 && isset($get_gr)){
                    $btn_ca.=" <a href='".base_url()."Clientes_cliente/diagnostico_alertas/".$id_operacion."/".$g->idperfilamiento."' title='Diagnóstico de Alertas' class='btn gradient_nepal2'><i class='fa fa-warning'></i> Guia de alertas</a>";
                  }
                  //echo "grado: ".$get_gr->grado."<br>";
                  //echo "cont_cli: ".$cont_cli."<br>";
                  if(isset($get_gr->grado) && $get_gr->grado<=2.0 && $cont_cli==1){
                    echo "<input type='hidden' id='re_ok' value='1'>";
                    echo "<input type='hidden' id='idcc' value='".$idcc."'>";
                    echo "<input type='hidden' id='idp' value='".$k->id_perfilamiento."'>";
                    echo "<input type='hidden' id='tipoccon' value='".$tipoccon."'>";
                  }else if(isset($get_gr->grado) && $get_gr->grado<=2.0 && $cont_cli>1){
                    echo "<input type='hidden' id='re_ok' value='0'>";
                  }
                  echo "<br><div class='col-md-12'>
                    <table width='100%' class='table'>
                      <tr>
                        <td width='10%'>
                          <h3 class='barra_menu'>".$nombre."</h3>
                        </td>
                        <td width='40%'>
                          <!--<a href='".base_url()."Clientes_cliente/grado_riesgo/".$idcc."/".$k->id_perfilamiento."/".$tipoccon."/".$id_operacion."' title='Calificar grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-calculator'></i></a>-->
                          <button onclick='precalificar(".$idcc.",".$k->id_perfilamiento.",".$tipoccon.",".$id_operacion.");' title='Calificar grado de riesgo' class='btn gradient_nepal2'><i class='fa fa-calculator'></i> Calculadora</button>
                          ".$btn_ca."
                        </td>
                      </tr>
                    </table>
                    <br> 
                  </div>";
                }
              }
            } ?>
          </div>
          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<a href="<?php echo base_url();?>Operaciones/resultadosBusqueda/<?php echo $id_operacion;?>" class="btn gradient_nepal2" ><i class="fa fa-arrow-left"></i> Atrás</a>-->
              <?php /*echo "avance: ".$config->avance; echo "bandcaP ".$bandcaP; echo " pep_exist". $pep_exist; echo " bandca ".$bandca; echo " nopep_exist ".$nopep_exist;*/
              //echo  $get_gr->grado;
              if(isset($config) && $config->avance=="1"){ 
                //echo "existe config y es 1";
                if($bandcaP==1 && $pep_exist==1 || $bandca==1 && $nopep_exist==1 || $bandca==0 && $nopep_exist==0 && isset($get_gr->grado) && $get_gr->grado<=2.0) { ?>
                  <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2"><i class="fa fa-arrow-right"></i> Aceptar</a>
                <?php } else { //echo "existe config y es 0"; //checar esto ?>
                  <a href="javascript:void(0)" class="btn gradient_nepal2" title="No se permite continuar con el proceso, llenar formulario faltante"><i class="fa fa-close"></i> No se permite avanzar</a>
                  <p style="font-size: 16px; font-weight: bold; color: red">No se permite continuar con el proceso, llenar formulario faltante</p>
                <?php } ?>
              <?php } 
              else if(isset($config) && $config->avance=="0" && isset($get_gr->grado) && $get_gr->grado<=2.0) { ?>  
                <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2" title="Aceptar"><i class="fa fa-arrow-right"></i> Aceptar</a>
              <?php }
              else if(isset($config) && $config->avance=="0" && isset($get_gr->grado) && $get_gr->grado>2.0 && $autorizado==0 && $this->session->userdata('tipo')!=3) { ?>  
                <a href="javascript:void(0)" class="btn gradient_nepal2" title="No se permite continuar con el proceso por la configuración realizada en el diagnostico de alertas"><i class="fa fa-close"></i> No se permite avanzar</a>
                <p style="font-size: 16px; font-weight: bold; color: red">No se permite continuar con el proceso por la configuración realizada en el diagnostico de alertas</p>
              <?php } 
              else if(isset($config) && $config->avance=="0" && isset($get_gr->grado) && $get_gr->grado>2.0 && $autorizado==0 && $this->session->userdata('tipo')==3) { ?>  
                <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2" title="Aceptar"><i class="fa fa-arrow-right"></i> Aceptar</a>
              <?php } 
              else if(isset($config) && $config->avance=="0" && isset($get_gr->grado) && $get_gr->grado>2.0 && $autorizado>0) { ?>  
                <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2" title="Aceptar"><i class="fa fa-arrow-right"></i> Aceptar</a>
              <?php } 
              else if(!isset($config)) { ?>  
                <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2"><i class="fa fa-arrow-right"></i> Aceptar</a>
              <?php }  
              else if(!isset($get_gr->grado)) { //echo "ultimo"; ?>  
                <a href="javascript:void(0)" class="btn gradient_nepal2" title="No se permite continuar, falta calificar grado de riesgo"><i class="fa fa-close"></i> No se permite avanzar</a>
                <p style="font-size: 16px; font-weight: bold; color: red">No se permite continuar, falta calificar grado de riesgo</p>
              <?php } ?>    
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-md" id="transacción_actividades" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title ddaa">Actividades</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
      </div>
      <input type="hidden" id="idcc" value="0">
      <input type="hidden" id="idp" value="0">
      <input type="hidden" id="tipoccon" value="0">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <span class="transaccion_acti"></span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="aceptarAct()" class="btn gradient_nepal2" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->