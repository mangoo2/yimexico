<?php $perfilid=$this->session->userdata('perfilid'); ?>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3>Generación XMLS</h3>
        <div class="col-md-12" align="right">
          <button type="button" class="btn btn-outline-primary btn-fw" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div> 
        <hr class="subtitle">
        <div class="col-md-12" align="center">
          <?php if($perfilid==7){?><!-- Responsable Cumplimiento -->
            <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px" >
              <i class="mdi mdi-account-multiple btn-icon-prepend mdi-36px"></i><br>
              <span class="d-inline-block text-left">
                <h5 align="center">24 Horas</h5>
              </span>
            </button>
            <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px" >
              <i class="mdi mdi-note-multiple-outline btn-icon-prepend mdi-36px"></i><br>
              <span class="d-inline-block text-left">
                <h4 align="center">Normal</h4>
              </span>
            </button>
            <button class="btn btn-outline-dark btn-icon-text" style="width: 250px; height: 125px" >
              <i class="mdi mdi-note-multiple-outline btn-icon-prepend mdi-36px"></i><br>
              <span class="d-inline-block text-left">
                <h4 align="center">Sin operaciones</h4>
              </span>
            </button>
          <?php } ?> 
        </div><br> 
      </div>
    </div>
  </div>
</div>     