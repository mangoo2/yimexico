<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
</style>
<div class="row">
      <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">  
        <div class="row">
          <div class="col-md-12">  
            <?php if($exento==1) { $exento_aux=0; ?>
              <h4>Generación de Aviso exento.
            <?php } ?>
            <?php if($exento==0) { $exento_aux=1; ?>
              <h4>Generación de Aviso en ceros.
            <?php } ?>
              <?php if($exento==1) { ?>
                <a data-toggle="modal" data-target="#modal_ayuda"><button type="button" class="btn btn-warning btn-rounded btn-icon" >
                  <i class="mdi mdi-help"></i>
                </button></a> 
              <?php } ?>    
            </h4>  
            <h3>Registro de transacción</h3>
          </div>
        </div>
        <hr class="subtitle">
        <form id="form_aviso" class="forms-sample">
          <!-- -->
          <input type="hidden" name="id" value="<?php echo $id ?>"> 
          <input type="hidden" name="id_perfilamiento" value="<?php echo $idperfilamiento ?>"> 
          <input type="hidden" id="exento" value="<?php echo $exento_aux ?>"> 
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Año reportado:</label>
              <input type="hidden" id="anio_acuse_aux" value="<?php echo $anio_acuse ?>">
              <select class="form-control" id="anio" name="anio_acuse">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Mes reportado:</label>
              <input type="hidden" id="mes_aux" value="<?php echo $mes_acuse ?>">
              <select class="form-control" id="mes" name="mes_acuse">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Actividad a reportar:</label>
              <select class="form-control" name="actividad">
                <?php foreach ($actividades as $av) {?>
                    <option value="<?php echo $av->idactividad?>" <?php if($actividad==$av->idactividad) echo 'selected' ?> ><?php echo $av->nombre?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <?php if($exento==1){ ?>
          <div class="row">
            <div class="col-md-12" <?php if($exento==1){ echo 'style="display:none"'; } ?>>
              <div>
                <div class="form-check form-check-primary">
                  <label class="form-check-label">Selecciona si el aviso es exento, porque corresponde a la realización de operaciones previsas en el artículo 27 Bis de las Reglas de Carácter General de la LFPIORPI (Aviso exento)
                    <input disabled type="checkbox" class="form-check-input" name="informe" id="informe" value="1" <?php if($exento==1 || $informe==1) echo 'checked' ?>>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>  
        </form>  
          
          <!-- -->
          <div class="row"> 
            <!--<div class="col-md-6 form-group">
                <span style="font-size: 25px; color: #b57532;">Cliente: </span>
                <span style="font-size: 25px; color: #b57532;">
                <?php $num_cliente=0;
                      /*if($tipoc==6){ 
                      echo mb_strtoupper($cc->denominacion);
                        $num_cliente=$cc->idtipo_cliente_f;
                      }
                      if($tipoc==3){
                      echo mb_strtoupper($cc->razon_social);  
                        $num_cliente=$cc->idtipo_cliente_p_m_m_e;
                      }
                      if($tipoc==4){
                      echo mb_strtoupper($cc->nombre_persona);
                        $num_cliente=$cc->idtipo_cliente_p_m_m_d;
                      }
                      if($tipoc==5){
                      echo mb_strtoupper($cc->denominacion);
                        $num_cliente=$cc->idtipo_cliente_e_c_o_i;
                      }
                      if($tipoc==1 || $tipoc==2){
                      echo mb_strtoupper($cc->nombre.' '.$cc->apellido_paterno.' '.$cc->apellido_materno);  
                        if($tipoc==1){
                          $num_cliente=$cc->idtipo_cliente_p_f_m;
                        }
                        if($tipoc==2){
                          $num_cliente=$cc->idtipo_cliente_p_f_e;
                        }
                      }*/
                ?> 
                </span>
            </div>
            <div class="col-md-6 form-group">
              <span style="font-size: 25px; color: #b57532;">No. de Cliente: </span>
              <span style="font-size: 25px; color: #b57532;">
                <?php echo $num_cliente ?>
              </span>  
            </div>-->
          </div>
        
     
      <div class="modal-footer">
        <button class="btn gradient_nepal2" type="button" onclick="registrar_aviso()"><i class="fa fa-save"></i> Registrar aviso</button>
        <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="modal_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Aviso exento</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>Aviso exento: El aviso exento lo presentan las actividades vulnerables que formen parte de un Grupo Empresarial y que realizan las siguientes operaciones:
          A) Operaciones de mutuo, de otorgamiento de préstamos o créditos entre empresas del mismo Grupo Empresarial <br>
          B) Operaciones de mutuo, de otorgamiento de préstamos o créditos a empleados de las empresas integrantes del Grupo Empresarial
          Administren recursos aportados y otorguen préstamos o créditos a los trabajadores que conforman el Grupo Empresarial 
          Grupo Empresarial: conjunto de personas morales en las que una misma sociedad mantiene el control de las personas morales.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>