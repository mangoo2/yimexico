<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $id_operacion; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-rocket"></i> Regresar a Proceso</button></a>
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">Expedientes de dueños beneficiarios asignados a la operación <strong><?php echo /*$id_operacion;*/ $folio; ?></strong></h1>
          <br>
          <hr class="subtitle">
          <div class="col-md-12">
            <h3 class="barra_menu">Lista de Dueños Beneficiarios:</h3><hr class="">
            <?php 
            $con_benes=0;
            $con_benes_aux=0;
            $con_valida=0;
            $nombreb="";
            $uri="javascript:void(0);";
            $id_clientec=0;
            foreach ($infob as $b) {
              $con_benes++;
              $id_clientec=$b->id_clientec;
            }
            if($con_benes>0){
              foreach ($infob as $b) {
                $con_benes_aux++;
                $tipo_bene=$b->tipo_bene;
                $info="";
                $nombreb="";
                $apellidosb="";
                
                if($tipo_bene==1){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$b->id_duenio_bene);
                }else if($tipo_bene==2){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$b->id_duenio_bene);
                }else if($tipo_bene==3){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$b->id_duenio_bene);
                }
                //echo $tipo_bene;
                //echo $b->id_duenio_bene;
                $info="";
                $nombreb="";
                if($tipo_bene>0){
                  foreach ($results as $k) {
                    //EXPEDIENTES - VER VALIDACION
                    if($tipo_bene>0){
                      $expb=$this->ModeloCatalogos->getselectwherestatus('*','docs_beneficiario',array('id_beneficiario'=>$b->id_duenio_bene,"tipo_bene"=>$tipo_bene));
                      foreach ($expb as $eb) {
                        //echo $eb->validado;
                        if($eb->validado==1){
                          $con_valida++;
                        }
                      }
                    }
                    if($tipo_bene==1){
                      $nombreb = $k->nombre;
                      $apellidosb = $k->apellido_paterno." ".$k->apellido_materno;
                    }
                    if($tipo_bene==2 || $tipo_bene==3){
                      $nombreb = "";
                      $apellidosb = $k->razon_social;
                      $id_bene_moral = $k->id_bene_moral;
                    }
                    if($tipo_bene==0){
                      $nombreb="";
                      $apellidosb='Declarado "sin dueño beneficiario"';
                    }
                    //echo "con_benes: ".$con_benes;
                    if($con_benes==1){ //
                      if($b->id_duenio_bene==0) 
                        redirect(base_url().'Clientes_c_beneficiario/sin_beneficiario/'.$b->idtipo_cliente."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion);
                      if($tipo_bene=="1")
                        redirect(base_url().'Clientes_c_beneficiario/docs_beneficiario/'.$b->id_duenio_bene."/".$tipo_bene."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion);
                      if($tipo_bene=="2" || $tipo_bene=="3")
                        redirect(base_url().'Clientes_c_beneficiario/docs_beneficiario/'.$id_bene_moral."/".$tipo_bene."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion);
                        
                    }

                    if($b->id_duenio_bene==0) 
                      $url=base_url().'Clientes_c_beneficiario/sin_beneficiario/'.$b->idtipo_cliente."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion;
                    if($tipo_bene=="1")
                      $url=base_url().'Clientes_c_beneficiario/docs_beneficiario/'.$b->id_duenio_bene."/".$tipo_bene."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion;
                    if($tipo_bene=="2" || $tipo_bene=="3")
                      $url=base_url().'Clientes_c_beneficiario/docs_beneficiario/'.$id_bene_moral."/".$tipo_bene."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion;

                    $info .= "<div>
                          <div class='col-md-12'>
                            <table width='100%' class='table'>
                              <tr>
                                <td width='10%'>
                                  <h3 class='barra_menu'>".$nombreb." ".$apellidosb."</h3>
                                </td>
                                <td width='40%'>
                                  <a href='".$url."' title='Expediente del beneficiario.' class='btn gradient_nepal2'><i class='fa fa-files-o'></i></button>
                                </td>
                              </tr>
                            </table>
                            </div></div>";
                    //echo "<br>con validado ".$con_valida;
                    //echo "<br>con_benes ".$con_benes;
                  }
                }else{ //benes fide,moral o fisico
                  //echo $con_benes;
                  if($b->id_duenio_bene==0) { //asignado sin dueño bene
                    if($con_benes==1){ //
                      redirect(base_url().'Clientes_c_beneficiario/sin_beneficiario/'.$b->idtipo_cliente."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion);
                    }
                    $url=base_url().'Clientes_c_beneficiario/sin_beneficiario/'.$b->idtipo_cliente."/".$b->id_perfilamiento."/".$id_clientec."/".$id_operacion;
                  }

                    $info .= "<div>
                        <div class='col-md-12'>
                          <table width='100%' class='table'>
                            <tr>
                              <td width='10%'>
                                <h3 class='barra_menu'>Declarado sin dueño beneficiario:</h3>
                              </td>
                              <td width='40%'>
                                <a href='".$url."' title='Expediente del beneficiario' class='btn gradient_nepal2'><i class='fa fa-files-o'></i></button>
                              </td>
                            </tr>
                          </table>
                          </div>";
                  $info .= "";
                  $con_valida++;
                }
                echo $info;
              } //foreach getbenes
            }else{
              echo "<br><div class='col-md-12'><h3 class='barra_menu'>".$nombreb."</h3> 
                    <br>Sin dueño beneficiario asignado</div><hr>";
            }
            //echo "<br>con validado ".$con_valida;
            //echo "<br>con_benes ".$con_benes;
            if($con_benes==$con_valida){
              //$uri=base_url().'Operaciones/Transaccion/'.$id_operacion.'';
              $uri=base_url().'Operaciones/procesoInicial/'.$id_operacion.'';
            } 
            ?>
          </div>
          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<a href="<?php echo base_url();?>Operaciones/documentosCliente/<?php echo $id_operacion;?>" class="btn gradient_nepal2" ><i class="fa fa-arrow-left"></i> Atrás</a>-->
              <a href="<?php echo $uri; ?>" title="Favor de validar el o los expedientes" class="btn gradient_nepal2"><i class="fa fa-arrow-right"></i> Aceptar</a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
