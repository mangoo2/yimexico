<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">Anexo - transacción a la operación <strong><?php echo /*$id_operacion;*/ $folio; ?></strong></h1>
          <br>
          <div class="col-md-12" style="display: none;">
            <h3 class="barra_menu">Anexo(s):</h3><hr class="">
            <div class="col-lg-12">
              <span class="transaccion_acti"></span>
            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <a href="<?php echo base_url();?>Operaciones/documentosBenes/<?php echo $id_operacion;?>" class="btn gradient_nepal2" ><i class="fa fa-arrow-left"></i> Atrás</a>
              <button disabled type="button" id="btn_transacc" class="btn gradient_nepal2" onclick="aceptarAct()"><i class="fa fa-arrow-right"></i> Aceptar</a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
