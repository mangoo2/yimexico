<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $id_operacion; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-rocket"></i> Regresar a Proceso</button></a>
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">Expedientes de clientes asignados a la operación <strong><?php echo /*$id_operacion;*/ $folio; ?></strong></h1>
          <hr class="subtitle">
          <div class="col-md-12">
            <h3 class="barra_menu">Lista de Clientes:</h3><hr class="">
            <?php $i=0; foreach ($info as $k) { $i++;
              //echo "<br>perfila: ".$k->id_perfilamiento;
              //echo "<br>i: ".$i;
              $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$k->id_perfilamiento));
              foreach ($get_pp as $g) {
                //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                $tipoccon = $g->idtipo_cliente;
                //echo "<br>tipoccon: ".$tipoccon;
                if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                if($tipoccon==6) $tabla = "tipo_cliente_f";
                //echo "<br>tabla: ".$tabla;
                $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
                foreach ($get_per as $g2) {
                  //echo "<br>k->id_cliente: ".$k->id_cliente;
                  if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
                  if($tipoccon==3) $nombre = $g2->razon_social;
                  if($tipoccon==4) $nombre = $g2->nombre_persona;
                  if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
                  //echo "i: ".$i;
                  //echo "union: ".$id_union;
                  if($i==1 && $id_union==0){
                    redirect(base_url()."Clientes_cliente/docs_cliente/".$k->id_clientec."/".$tipoccon."/".$k->id_perfilamiento."/".$id_operacion);
                  }else{   
                    echo "<br><div class='col-md-12'>
                      <table width='100%' class='table'>
                        <tr>
                          <td width='10%'>
                            <h3 class='barra_menu'>".$nombre."</h3>
                          </td>
                          <td width='40%'>
                            <a href='".base_url()."Clientes_cliente/docs_cliente/".$k->id_clientec."/".$tipoccon."/".$k->id_perfilamiento."/".$id_operacion."' title='Expediente del cliente' class='btn gradient_nepal2'><i class='fa fa-files-o'></i></button>
                          </td>
                        </tr>
                      </table>
                      <br> 
                    </div>";
                  }
                }
              }
            } ?>
          </div>
          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<a href="<?php echo base_url();?>Operaciones/gradoRiesgo/<?php echo $id_operacion;?>" class="btn gradient_nepal2" ><i class="fa fa-arrow-left"></i> Atrás</a> -->
              <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2"><i class="fa fa-arrow-right"></i> Aceptar</a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
