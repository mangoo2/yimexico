<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">Beneficiarios asignados a la operación <strong><?php echo $id_operacion ?></strong></h1>
          <br>
          <div class="col-md-12">
            <h3 class="barra_menu">Lista de Beneficiarios por cliente:</h3><hr class="">
            <?php 
            $get_o=$this->General_model->get_tableRowC('id_union','operaciones',array('id'=>$id_operacion));
            if($get_o->id_union==0){
              foreach ($info as $k) {
              //echo "<br>perfila: ".$k->id_perfilamiento;
              $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$k->id_perfilamiento));
              foreach ($get_pp as $g) {
                //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                $tipoccon = $g->idtipo_cliente;
                //echo "<br>tipoccon: ".$tipoccon;
                if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                if($tipoccon==6) $tabla = "tipo_cliente_f";
                //echo "<br>tabla: ".$tabla;
                $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
                foreach ($get_per as $g2) {
                  //echo "<br>k->id_cliente: ".$k->id_cliente;
                  if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
                  if($tipoccon==3) $nombre = $g2->razon_social;
                  if($tipoccon==4) $nombre = $g2->nombre_persona;
                  if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;

                  $get_benes = $this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento,"activo"=>1));
                  $con_benes=0;
                  $con_benes_aux=0;

                  foreach ($get_benes as $b) {
                    $con_benes++;
                  }
                  if($con_benes>0){
                    foreach ($get_benes as $b) {
                      $con_benes_aux++;
                      $tipo_bene=$b->tipo_bene;
                      if($tipo_bene==1){
                        $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$b->id_duenio_bene);
                      }else if($tipo_bene==2){
                        $results=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$b->id_duenio_bene);
                      }else if($tipo_bene==3){
                        $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$b->id_duenio_bene);
                      }
                      else if($tipo_bene==0){
                        $nombreb="";
                        $apellidosb='Declarado "sin dueño beneficiario"';
                      }
                      $info="";
                      //echo "con_benes_aux".$con_benes_aux;
                      if($con_benes_aux==1){
                        $info ="<br><div class='col-md-12'><h3 class='barra_menu'>Cliente: ".$nombre."</h3> ";
                      }
                      if($tipo_bene>0){
                        foreach ($results as $k) {
                          if($tipo_bene==1){
                            $nombreb = $k->nombre;
                            $apellidosb = $k->apellido_paterno." ".$k->apellido_materno;
                          }
                          if($tipo_bene==2 || $tipo_bene==3){
                            $nombreb = "";
                            $apellidosb = $k->razon_social;
                          }
                          if($tipo_bene==0){
                            $nombreb="";
                            $apellidosb='Declarado "sin dueño beneficiario"';
                          }
                          $info .= "<div>
                                <br><div class='col-md-12'>
                                  <table width='100%' class='table'>
                                    <tr>
                                      <td width='5%'>
                                        Beneficiario:
                                      </td>
                                      <td width='10%'>
                                        ".$nombreb." ".$apellidosb." 
                                      </td>
                                      <td width='40%'>
                                        <button type='button' class='btn gradient_nepal2' onclick='eliminaBene(".$b->id.")'><i class='fa fa-trash'></i></button>
                                      </td>
                                    </tr>
                                  </table>
                                  </div><hr>";
                          
                        }
                      }else{
                        $info .= '<p>Declarado "sin dueño beneficiario"
                                    <button type="button" class="btn gradient_nepal2" onclick="eliminaBene('.$b->id.')""><i class="fa fa-trash"></i></button>
                                  </p>';
                      }
                      echo $info;
                    }//foreach getbenes
                    echo "</div><hr>";
                  }else{
                    echo "<br><div class='col-md-12'><h3 class='barra_menu'>Cliente: ".$nombre.":</h3> 
                          <br><p>Sin dueño beneficiario asignado</p><hr>";
                  }
                  //echo $con_benes;
                }
              }
            }
          }else{
            $nombre="";
            $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","uniones_clientes",array("id_union"=>$get_o->id_union));
            foreach ($get_pp as $g) {
              $get_ppd = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$g->id_perfilamiento));
              foreach ($get_ppd as $g2) {
                //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                $tipoccon = $g2->idtipo_cliente;
                //echo "<br>tipoccon: ".$tipoccon;
                if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                if($tipoccon==6) $tabla = "tipo_cliente_f";
                //echo "<br>tabla: ".$tabla;
                $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g2->idperfilamiento));
                foreach ($get_per as $p) {
                  //echo "<br>k->id_cliente: ".$k->id_cliente;
                  if($tipoccon==1 || $tipoccon==2) $nombre .= $p->nombre." ".$p->apellido_paterno." ".$p->apellido_materno."/ ";
                  if($tipoccon==3) $nombre .= $p->razon_social."/ ";
                  if($tipoccon==4) $nombre .= $p->nombre_persona."/ ";
                  if($tipoccon==5 || $tipoccon==6) $nombre .= $p->denominacion."/ ";
                  $get_benes = $this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_operacion,"activo"=>1));
                }
              }
            }
            $con_benes=0;
            $con_benes_aux=0;
            foreach ($get_benes as $b) {
              $con_benes++;
            }
            if($con_benes>0){
              foreach ($get_benes as $b) {
                $con_benes_aux++;
                $tipo_bene=$b->tipo_bene;
                if($tipo_bene==1){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$b->id_duenio_bene);
                }else if($tipo_bene==2){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$b->id_duenio_bene);
                }else if($tipo_bene==3){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$b->id_duenio_bene);
                }
                else if($tipo_bene==0){
                  $nombreb="";
                  $apellidosb='Declarado "sin dueño beneficiario"';
                }
                $info="";
                //echo "con_benes_aux".$con_benes_aux;
                if($con_benes_aux==1){
                  $info ="<br><div class='col-md-12'><h3 class='barra_menu'>Cliente: ".$nombre."</h3> ";
                }
                if($tipo_bene>0){
                  foreach ($results as $k) {
                    if($tipo_bene==1){
                      $nombreb = $k->nombre;
                      $apellidosb = $k->apellido_paterno." ".$k->apellido_materno;
                    }
                    if($tipo_bene==2 || $tipo_bene==3){
                      $nombreb = "";
                      $apellidosb = $k->razon_social;
                    }
                    if($tipo_bene==0){
                      $nombreb="";
                      $apellidosb='Declarado "sin dueño beneficiario"';
                    }
                    $info .= "<div>
                          <br><div class='col-md-12'>
                            <table width='100%' class='table'>
                              <tr>
                                <td width='5%'>
                                  Beneficiario:
                                </td>
                                <td width='10%'>
                                  ".$nombreb." ".$apellidosb." 
                                </td>
                                <td width='40%'>
                                  <button type='button' class='btn gradient_nepal2' onclick='eliminaBene(".$b->id.")'><i class='fa fa-trash'></i></button>
                                </td>
                              </tr>
                            </table>
                            </div><hr>";
                    
                  }
                }else{
                  $info .= '<p>Declarado "sin dueño beneficiario" <button type="button" class="btn gradient_nepal2" onclick="eliminaBene('.$b->id.')""><i class="fa fa-trash"></i></button></p><hr>';
                }
                echo $info;
              }//foreach getbenes
              echo "</div><hr>";
            }else{
              echo "<br><div class='col-md-12'><h3 class='barra_menu'>Cliente: ".$nombre.":</h3> 
                    <br><p>Sin dueño beneficiario asignado</p></div><hr>";
            }
            
          }
          ?>
          
          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<a href="<?php echo base_url();?>Operaciones/clientesOperacion/<?php echo $id_operacion;?>" class="btn gradient_nepal2" ><i class="fa fa-arrow-left"></i> Atrás</a>-->
              <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2"><i class="fa fa-arrow-right"></i> Aceptar</a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>