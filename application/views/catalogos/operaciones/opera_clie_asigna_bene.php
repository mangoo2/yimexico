<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<input type="hidden" id="id_union" value="<?php echo $id_union; ?>">
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $id_operacion; ?>"><button type="button" class="btn gradient_nepal2 barra_menu"><i class="fa fa-rocket"></i> Regresar a Proceso</button></a>
          <button type="button" class="btn gradient_nepal2 barra_menu" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <div class="row firma_tipo_cliente" style="display: none">
          <div class="col-md-12">    
            <h3>Formato: Acuse de inexistencia de dueño beneficiario </h3>
            <h3>Fecha: <?php echo date("m-d-Y"); ?></h3><br>
            <span>DECLARA LA NO EXISTENCIA DEL DUEÑO BENEFICIARIO PARA LA OPERACIÓN CON NÚMERO <strong><?php echo /*$id_operacion;*/ $folio; ?></strong> A REALIZARSE CON <strong id="nombre_cli"></strong></span>
          </div>
        </div> 

        <div class="firma_tipo_cliente">
          <br><br><br><br><br><br>
          <div class="firma_tipo_cliente" style="display: none;">
            <div class="row">
              <div class="col-md-12" style="text-align: justify !important;">
                <hr>
                <center><span id="nombre_cli_imp"></span></center><br>
                  Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
              </div>
              <!--<div class="col-md-6" style="text-align: justify !important;">
                <hr>
                <center><span id="nombre_ejec_imp"></span></center><br>
                  Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados  
              </div> --> 
            </div>
          </div>
        </div> 
        <hr class="subtitle">
        <!---------------->
          <br>

          <h1 style="color: #b57532;" class="barra_menu" align="center">Dueños Beneficiarios asignados a la operación <strong><?php echo $folio ?></strong></h1>
          <br>
          <div class="col-md-12">
            <h3 class="barra_menu">Lista de Cliente:</h3>
            <?php $cont_aux=0; $btn_sig_bene=""; $title=""; if($cont_db==0){ $btn_sig_bene="disabled"; $title='title="Asignar un dueño beneficiario o declarar sin dueño beneficiario para poder avanzar"'; }
            foreach ($info as $k) {
              $idp=$k->id_perfilamiento;
              $idcc=$k->id_clientec;
              
              echo "<input id='idp' type='hidden' value='".$idp."'>";
              echo "<input id='idcc' type='hidden' value='".$idcc."'>";
              
              
              //echo "<br>perfila: ".$k->id_perfilamiento;
              $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$k->id_perfilamiento));
              foreach ($get_pp as $g) {
                $idc=$g->idcliente;
                echo "<input id='id_cliente' type='hidden' value='".$idc."'>";
                //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                $tipoccon = $g->idtipo_cliente;
                echo "<input id='tipo_cliente' type='hidden' value='".$tipoccon."'>";
                //echo "<br>tipoccon: ".$tipoccon;
                if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                if($tipoccon==6) $tabla = "tipo_cliente_f";
                //echo "<br>tabla: ".$tabla;
                $nombre="";
                $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
                foreach ($get_per as $g2) {
                  //echo "<br>k->id_cliente: ".$k->id_cliente;
                  if($id_union==0){
                    if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
                    if($tipoccon==3) $nombre = $g2->razon_social;
                    if($tipoccon==4) $nombre = $g2->nombre_persona;
                    if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
                    echo "<br><div class='col-md-12'>
                    <table width='100%' class='table barra_menu'>
                      <tr>
                        <td width='20%'>
                          <h3 id='name_cli' class='barra_menu'>".$nombre."</h3>
                        </td>
                        <td width='40%'>
                          <!--<button type='button' title='Eliminar Cliente de operación' class='btn gradient_nepal2' onclick='eliminaClie(".$k->id.")'><i class='fa fa-trash'></i></button>-->";
                    }else{
                      $nombre_mancomunado_aux="";
                      $cont_aux++;
                      //echo "";
                      if($cont_aux==1){
                        $nombre_mancomunado_aux=$nombre_mancomunado;
                        echo "<div class='col-md-12'>
                        <table width='100%' class='table barra_menu'>
                          <tr>
                            <td width='20%'>
                              <h3 class='barra_menu'>".$nombre_mancomunado_aux."</h3>
                            </td>
                            <td width='40%'>
                              <!--<button type='button' title='Eliminar Cliente de operación' class='btn gradient_nepal2' onclick='eliminaClie(".$k->id.")'><i class='fa fa-trash'></i></button>-->";
                      }
                      
                    }
                    if($id_union==0){
                      $dis_sin_db="";
                      if($cont_db>0 && $tipo_bene_ctrl==1){
                        $dis_sin_db="disabled";
                      }
                      //echo "<a href='".base_url()."Operaciones/asignarBene/".$id_operacion."/".$k->id_clientec."/".$k->id_perfilamiento."/".$tipoccon."/".$id_union."' title='Asignar Dueño Beneficiario' class='btn gradient_nepal2'><i class='fa fa-user'></i> Dueño Beneficiario</a>";
                      echo "<div class='row'>
                        <div class='col-md-4 barra_menu' id='a_benes_funt'>
                        <a href='#modal_bene' data-toggle='modal' title='Asignar Dueño Beneficiario' onclick='get_beneficiarios()' class='btn gradient_nepal2' id='asigna_db'><i class='fa fa-user'></i> Seleccionar Dueño Beneficiario</a>
                        </div>
                        <div class='col-md-3 barra_menu' id='a_benes_funt'>
                        <a href='#modal_bene_nuevo' id='agrega_db' data-toggle='modal' title='Agregar Dueño Beneficiario' onclick='get_beneficiarios()' class='btn gradient_nepal2'><i class='fa fa-plus'></i> Nuevo Dueño Beneficiario</a>
                        </div>";
                      echo "<div class='barra_menu' id='cont_sin_bene'>
                        <div class='form-check-flat form-check-primary'>
                          <label class='form-check-label'><br>
                            <input ".$dis_sin_db." type='checkbox' class='' id='sin_beneficiario'>
                              Sin dueño beneficiario
                          <i class='input-helper'></i></label>
                        </div>
                        <button style='display: none' type='button' class='btn btn-secondary btn-impresion barra_menu' onclick='imprimir_formato1(".$tipoccon.",".$k->id_clientec.")' id='bton_impri'><i class='fa fa-print'></i> Imprimir constancia de inexistencia de dueño beneficiario</button>
                      </div></div>";
                    }
                  echo"</td>
                  </tr>
                </table>
                <br> ";
                }
              }
            } 
            if($id_union>0){
              $dis_sin_db="";
                if($cont_db>0 && $tipo_bene_ctrl==1){
                  $dis_sin_db="disabled";
                }
              //echo "<div class='row'><div class='col-md-6 barra_menu'><a href='".base_url()."Operaciones/asignarBene/".$id_operacion."/".$k->id_clientec."/".$k->id_perfilamiento."/".$tipoccon."/".$id_union."' title='Asignar Dueño Beneficiario' class='btn gradient_nepal2'><i class='fa fa-user'></i> Dueño Beneficiario</a></div>";
              echo "<div class='row'>
                <div class='col-md-3 barra_menu' id='a_benes_funt'>
                  <a href='#modal_bene' data-toggle='modal' title='Asignar Dueño Beneficiario' onclick='get_beneficiarios()' class='btn gradient_nepal2' id='asigna_db'><i class='fa fa-user'></i> Seleccionar Dueño Beneficiario</a>
                </div>
                <div class='col-md-3 barra_menu' id='a_benes_funt'>
                  <a href='#modal_bene_nuevo' data-toggle='modal' title='Agregar Dueño Beneficiario' onclick='get_beneficiarios()' class='btn gradient_nepal2' id='agrega_db'><i class='fa fa-plus'></i> Nuevo Dueño Beneficiario</a>
              </div>";
              echo '<div class=" barra_menu" id="cont_sin_bene">
                    <div class="form-check-flat form-check-primary">
                      <label class="form-check-label"><br>
                        <input '.$dis_sin_db.' type="checkbox" class="" id="sin_beneficiario">
                          Sin dueño beneficiario
                      <i class="input-helper"></i></label>
                    </div>
                    <button style="display: none" type="button" class="btn btn-secondary btn-impresion barra_menu" onclick="imprimir_formatoUnion('.$id_operacion.')" id="bton_impri"><i class="fa fa-print"></i> Imprimir formato conoce dueño beneficiario</button>
                  </div></div>';
            }
            //$get_benes = $this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_operacion,"id_perfilamiento"=>$g->idperfilamiento,"activo"=>1));
            $get_benes = $this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_operacion,"activo"=>1));
            $con_benes=0;
            $con_benes_aux=0;

            foreach ($get_benes as $b) {
              $con_benes++;
            }
            echo $preinfo="<br><h3 class='barra_menu'>Dueños Beneficiarios asignados:</h3>";
            if($con_benes>0){
              foreach ($get_benes as $b) {
                $con_benes_aux++;
                $tipo_bene=$b->tipo_bene;
                if($tipo_bene==1){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$b->id_duenio_bene);
                }else if($tipo_bene==2){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$b->id_duenio_bene);
                }else if($tipo_bene==3){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$b->id_duenio_bene);
                }
                else if($tipo_bene==0){
                  $nombreb="";
                  $apellidosb='Declarado "sin dueño beneficiario"';
                }
                $info="";
                if($tipo_bene>0){
                  foreach ($results as $k) {
                    if($tipo_bene==1){
                      $nombreb = $k->nombre;
                      $apellidosb = $k->apellido_paterno." ".$k->apellido_materno;
                    }
                    if($tipo_bene==2 || $tipo_bene==3){
                      $nombreb = "";
                      $apellidosb = $k->razon_social;
                    }
                    if($tipo_bene==0){
                      $nombreb="";
                      $apellidosb='Declarado sin Dueño Beneficiario';
                    }
                    $info .= "<div><input id='band_sin_dueno' type='hidden' value='0'>
                          <br><div class='col-md-12 barra_menu'>
                            <table width='100%' class='table' id='table_benes'>
                              <tr>
                                <td width='10%'>
                                  Dueño Beneficiario:
                                </td>
                                <td width='50%'>
                                  ".$nombreb." ".$apellidosb." 
                                </td>
                                <td width='10%'>
                                  <button type='button' class='btn gradient_nepal2' onclick='eliminaBene(".$b->id.")'><i class='fa fa-trash'></i></button>
                                </td>
                                <td width='10%'>
                                  <button type='button' class='btn gradient_nepal2' onclick='verDBAsignado(".$tipo_bene.",".$b->id_duenio_bene.",".$b->id_perfilamiento.")'><i class='fa fa-eye'></i></button>
                                </td>
                                <td width='20%'>

                                </td>
                              </tr>
                            </table>
                            </div><hr class='barra_menu'>";
                    
                  }
                }else{
                  $info .= "<div  id='cont_benes_asigna'><input id='band_sin_dueno' type='hidden' value='1'>
                          <br><div class='col-md-12 barra_menu'>
                            <table width='100%' class='table'>
                              <tr>
                                <td width='5%'>
                                  Dueño Beneficiario:
                                </td>
                                <td width='10%'>
                                  Declarado sin Dueño Beneficiario
                                </td>
                                <td width='40%'>
                                  <button type='button' class='btn gradient_nepal2' onclick='eliminaBene(".$b->id.")'><i class='fa fa-trash'></i></button>
                                </td>
                              </tr>
                            </table>
                            </div><hr class='barra_menu'>";
                }
                echo $info;
              }//foreach getbenes
              echo "</div><hr class='barra_menu'>";
            }else{
              echo "<br><div class='col-md-12'><h3 class='barra_menu'></h3> 
                    <br><p class='barra_menu'>Aun no se declara tipo de dueño beneficiario, favor de asignar uno o declarar sin dueño beneficiario</p><hr class='barra_menu'>";
            }

            ?>
          </div>

          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<a href="<?php echo base_url();?>Operaciones" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Atrás</a>-->
              <!--<a onclick="aceptarAdd()" href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>"><button <?php /*echo $btn_sig_bene;*/ echo " ".$title; ?> class="btn gradient_nepal2 barra_menu" type="button"><i class="fa fa-arrow-right"></i> -->
              <button <?php /*echo $btn_sig_bene;*/ echo " ".$title; ?> onclick="aceptarAddNext()" class="btn gradient_nepal2 barra_menu" type="button"><i class="fa fa-arrow-right"></i> Aceptar</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade barra_menu" id="modal_bene" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header barra_menu">
        <h4 class="modal-title">Dueño Beneficiario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <!--<h5>Elige un beneficiario</h5>-->
        <div class="row">
          <div class="col-md-5"></div>

        </div>
      </div>
      <div class="col-md-12 form-group" id="cont_select_bene">
        <span class="text_select_beneficiario">
            <span class="select_beneficiario"></span>
        </span>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 barra_menu" id="btn_submit" onclick="aceptarAdd()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2 barra_menu" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade barra_menu" id="modal_bene_nuevo" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header barra_menu">
        <h4 class="modal-title">Dueño Beneficiario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <!--<h5>Elige un beneficiario</h5>-->
        <div class="row">
          <div class="col-md-5"></div>

        </div>
      </div>
      <div class=" col-md-12 form-group" id="cont_tipo_bene">
        <h5 align="center">Elige un tipo de beneficiario</h5>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Física  
            <input type="radio" class="form-check-input" name="tipoben" id="fisica" value="1">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Moral  
            <input type="radio" class="form-check-input" name="tipoben" id="moral" value="2">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Fideicomiso  
            <input type="radio" class="form-check-input" name="tipoben" id="fideicomiso" value="3">
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 barra_menu" id="btn_submit" onclick="aceptarAddNvo()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2 barra_menu" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--- Modal FechaFormato -->
<div class="modal fade" id="modal_fecha" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Fecha de formato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="tipo_con" readonly>
        <input type="hidden" id="id_cc" readonly>
        <input type="date" style="width: 100%" id="fecha_f" class="form-control">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal" onclick="imprimir_formato()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>