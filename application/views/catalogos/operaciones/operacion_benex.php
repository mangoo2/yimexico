<input type="hidden" id="id_opera" value="<?php echo $id_operacion; ?>">
<input type="hidden" id="id_clientec" value="<?php echo $id_clientec; ?>">
<input type="hidden" id="id_perfilamiento" value="<?php echo $id_perfilamiento; ?>">
<input type="hidden" id="id_cliente" value="<?php echo $id_cliente; ?>">
<input type="hidden" id="tipo_cliente" value="<?php echo $tipo_cliente; ?>">
<input type="hidden" id="id_union" value="<?php echo $id_union; ?>">
<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url() ?>template/css/imprimir1.css">

<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12 barra_menu" align="right">
          <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-arrow-left"></i> Regresar a inicio</button>
        </div>
        <div class="row firma_tipo_cliente" style="display: none">
          <div class="col-md-12">    
            <h3>Formato: Acuse de inexistencia de dueño beneficiario </h3>
            <h3>Fecha: <?php echo date("m-d-Y"); ?></h3><br>
            <?php 
            if($tipo_cliente_info->idtipo_cliente==1 || $tipo_cliente_info->idtipo_cliente==2)
                $nombre = $info->nombre." ".$info->apellido_paterno." ".$info->apellido_materno;
            if($tipo_cliente_info->idtipo_cliente==3 || $tipo_cliente_info->idtipo_cliente==4 || $tipo_cliente_info->idtipo_cliente==5 || $tipo_cliente_info->idtipo_cliente==6)
              $nombre = $info->nombre_g." ".$info->apellido_paterno_g." ".$info->apellido_materno_g;
            ?>
            <span>DECLARA LA NO EXISTENCIA DEL DUEÑO BENEFICIARIO PARA LA OPERACIÓN CON NÚMERO <strong><?php echo $id_operacion; ?></strong> A REALIZARSE CON <strong><?php echo $nombre ?></strong></span>
          </div>
        </div> 
        <hr class="subtitle">
        <!---------------->
          <br>
          <h1 style="color: #b57532;" class="barra_menu" align="center">¿El dueño beneficiario se encuentra guardado en la base de datos o es declarado sin dueño beneficiario?</h1>
          <br>
          <div class="col-md-12 barra_menu" align="center">
            <button type="button" class="btn gradient_nepal2" id="si_exist_bene"><i class="fa fa-check"></i> Si existe</button>
            <button type="button" class="btn btn_yimexicov2" id="no_existe_bene"><i class="fa fa-close"></i> No existe</button>
          </div>
          <br><br>
          <div id="cont_benes" style="display: none" class="barra_menu">
            <div align="center">
               <h5>Elige un beneficiario para iniciar la operación</h5>
            </div>
            <span class="select_benes"></span>
          </div>
          <div class="firma_tipo_cliente">
            <br><br><br><br><br><br>
            <div class="firma_tipo_cliente" style="display: none;">
              <div class="row">
                <div class="col-md-6" style="text-align: justify !important;">
                  <hr>
                  <center><span id="nombre_cli_imp"><?php echo $nombre; ?></span></center><br>
                    Declaro bajo protesta de decir verdad, que la información contenida en el presente formato es veraz. Afirmo que soy legalmente responsable de la autenticidad y veracidad de la información, asumiendo todo tipo de responsabilidad derivada de cualquier declaración en falso sobre la misma.
                </div>
                <div class="col-md-6" style="text-align: justify !important;">
                  <hr>
                  <center><span id="nombre_ejec_imp"></span></center><br>
                    Bajo protesta de decir verdad por este medio ratifico que me fueron presentados en original o en copia certificada por Fedatario Público los documentos de los que se desprendieron los datos arriba detallados  
                </div>  
              </div>
            </div>
          </div> 

          <!--<div class="row">
            <div class="col-md-12" align="right">
              <button type="button" class="btn gradient_nepal2" onclick="siguiente_exist()"><i class="fa fa-arrow-right"></i> Siguiente</button>
            </div>
          </div>-->
      </div>
    </div>
  </div>
</div>


<div class="modal fade barra_menu" id="modal_bene" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header barra_menu">
        <h4 class="modal-title">Dueño Beneficiario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <!--<h5>Elige un beneficiario</h5>-->
        <div class="row">
          <div class="col-md-5"></div>
          <div class="col-md-7" id="cont_sin_bene">
            <div class="form-check form-check-flat form-check-primary">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" id="sin_beneficiario">
                  Sin dueño beneficiario
              <i class="input-helper"></i></label>
            </div>
            <button style="display: none" type="button" class="btn btn-secondary btn-impresion barra_menu" onclick="imprimir_formato1()"><i class="fa fa-print"></i> Imprimir formato conoce dueño beneficiario</button>
          </div>
        </div>
      </div>
      <div class="col-md-12 form-group" id="cont_select_bene">
        <span class="text_select_beneficiario">
            <span class="select_beneficiario"></span>
        </span>
      </div>
      <div class="row" id="cont_nvo_bene">
        <div class="col-md-5"></div>
        <div class="col-md-7">
          <div class="form-check form-check-flat form-check-primary">
            <label class="form-check-label">
              <input type="checkbox" class="form-check-input" id="otro_beneficiario" onchange="check_beneficiario()">
              Registrar nuevo Dueño beneficiario
            <i class="input-helper"></i></label>
          </div>
        </div>
      </div>
      <div class=" col-md-12 form-group" style="display: none" id="cont_tipo_bene">
        <h5 align="center">Elige un tipo de beneficiario</h5>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Física  
            <input type="radio" class="form-check-input" name="tipoben" id="fisica" value="1">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Persona Moral  
            <input type="radio" class="form-check-input" name="tipoben" id="moral" value="2">
          </label>
        </div>
        <div class="form-check form-check-primary">
          <label class="form-check-label">Fideicomiso  
            <input type="radio" class="form-check-input" name="tipoben" id="fideicomiso" value="3">
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2 barra_menu" id="btn_submit" onclick="aceptarAdd()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2 barra_menu" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>