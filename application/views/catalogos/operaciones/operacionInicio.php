<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<input type="hidden" id="id_docs_sinb" value="<?php echo $id_docs_sinb ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3 class="">Proceso de Operación</h3>
          </div>
          <div class="col-md-4" align="right">
            <a href="<?php echo base_url();?>Operaciones"><button type="button" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar</button></a>
            <a href="<?php echo base_url();?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-home"></i></button></a>
          </div>  
        </div>  
        <hr class="subtitle">
        <!---------------->
        <div class="row col-md-12" align="center">
          <?php if($id_operacion==0) $url=base_url()."Operaciones/existente"; else $url=base_url()."Operaciones/clientesOperacion/".$id_operacion; ?>
          <div class="col-md-4" align="center">
            <a href="<?php echo $url;?>"><button style="width: 260px;" class="btn gradient_nepal tam_btn">
              <div class="row texto_centro">
                <div class="col-md-2" align="right">
                  <i class="fa fa-user btn-icon-prepend icon_yi2"></i>
                </div>
                <div class="col-md-10" align="left">
                  <span style="font-size: 22px; font-weight: bold;">Asignar Cliente</span>
                </div>
              </div>  
            </button></a>
          </div>

         <?php if($idtipo_cliente!="6" && $idtipo_cliente!="7"){
            if($id_operacion==0) $url="javascript:void(0)"; else $url=base_url()."Operaciones/clientesOperacionBenes/".$id_operacion; ?>
            <div class="col-md-4" align="center">
              <a disabled href="<?php echo $url; ?>"><button style="width: 260px;" class="btn gradient_nepal tam_btn" <?php if($id_operacion==0) echo "disabled"; ?>>
                <div class="row texto_centro">
                  <div class="col-md-2" align="right">
                    <i class="fa fa-users btn-icon-prepend icon_yi2"></i>
                  </div>
                  <div class="col-md-10" align="left">
                    <span style="font-size: 22px; font-weight: bold;">Asignar Dueño Beneficiario</span>
                  </div>
                </div>  
              </button></a>
            </div>
          <?php } ?>
          <?php $url="javascript:void(0)"; $dis="disabled"; if($id_operacion>0 && $cont_db>0) { $url=base_url()."Operaciones/benesOperacion/".$id_operacion; $dis=""; } ?>
          <!--<a disabled href="<?php echo $url; ?>"><button style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $dis; ?>>
            <div class="row texto_centro">
              <div class="col-md-2" align="right">
                <i class="fa fa-users btn-icon-prepend icon_yi2"></i>
              </div>
              <div class="col-md-10" align="left">
                <span style="font-size: 22px; font-weight: bold;">Ver Dueño Beneficiario</span>
              </div>
            </div>  
          </button></a>&nbsp;&nbsp;&nbsp;&nbsp; -->

          <?php $url="javascript:void(0)"; $dis="disabled"; if($id_operacion>0 && $cont_db>0 || $id_operacion>0 && $idtipo_cliente=="6" || $id_operacion>0 && $idtipo_cliente=="7"){ $url=base_url()."Operaciones/resultadosBusqueda/".$id_operacion; $dis=""; } ?>
          <div class="col-md-4" align="center">
            <a disabled href="<?php echo $url; ?>"><button style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $dis; ?>>
              <div class="row texto_centro">
                <div class="col-md-2" align="right">
                  <i class="fa fa-search btn-icon-prepend icon_yi2"></i>
                </div>
                <div class="col-md-10" align="left">
                  <span style="font-size: 22px; font-weight: bold;">Resultados de Búsqueda </span>
                </div>
              </div>  
            </button></a><br><br>
          </div>

          <?php 
          $title_docsc="Pendiente por validar expediente";
          $title_docsb="Pendiente por validar expediente";
          $title="Falta realizar cálculo de grado de riesgo";
          $titleda="";
          $titleca="";
          if(isset($grado[0]) && $grado[0]>0 || isset($grado[1]) && $grado[1]>0){
            $title="";
          }
          //echo "grado 0 : ". $grado[0];
          //echo "grado 1: ". $grado[1];
          //echo "cont_co:  ". $cont_co; 
          $con_cl=0; $con_grado=0;
          $get_co=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$id_operacion,"activo"=>1));
          foreach ($get_co as $key) {
            $con_cl++;
            $get_gra=$this->ModeloTransaccion->getGradoCli($key->id_perfilamiento,$id_operacion);
            foreach ($get_gra as $key2) {
              /*echo "grado : ". $key2->grado."<br>";
              echo "cont_co : ". $cont_co."<br>";
              echo "con_cl : ". $con_cl."<br>";*/
              if($key2->grado>0){
                $con_grado++;
                //$title="Falta realizar cálculo de grado de riesgo";
              }else{
                $title="";
              }
            }
          }
          //echo "con_grado : ". $con_grado."<br>";
          if($con_grado==$cont_co && $cont_co==$con_cl){
            $title="";
          }else{
            $title="Falta realizar cálculo de grado de riesgo";
          }
          //echo "cont_diag: ". $cont_diag;
          if($cont_diag<$cont_gmalto){
            $titleda="Falta realizar Diagnóstico de Alerta";
          }else{
            $titleda="";
          }
          //echo "cont_ca: ". $cont_ca;
          if($cont_ca<$cont_alto){
            $titleca="Falta realizar el Cuestionario Ampliado";
          }else{
            $titleca="";
          }
          $url="javascript:void(0)"; $dis="disabled"; if($id_operacion>0 && $cont_db>0 || $id_operacion>0 && $idtipo_cliente=="6" || $id_operacion>0 && $idtipo_cliente=="7"){ $url=base_url()."Operaciones/gradoRiesgo/".$id_operacion; $dis=""; } ?>
          <div class="col-md-3" align="center">
            <a disabled href="<?php echo $url; ?>"><button style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $dis; ?>>
              <div class="row texto_centro">
                <div class="col-md-2" align="right">
                  <i class="fa fa-calculator btn-icon-prepend icon_yi2"></i>
                </div>
                <div class="col-md-10" align="left">
                  <span style="font-size: 22px; font-weight: bold;">Calculadora de Riesgo </span>
                </div>
              </div>  
            </button></a>
            <p style="font-size: 16px; font-weight: bold; color: red"><?php 
            if($title!="") echo $title; 
            if($titleca!="") echo "<br>".$titleca;
            if($titleda!="") echo "<br>".$titleda; 
            //echo $title; echo "<br>". $titleca; echo "<br>".$titleda;
            ?></p>
          </div>

          <?php 
          $url="javascript:void(0)"; $dis="disabled"; $disc[0]="disabled"; $disb="disabled";
          $con_clic=0; $i=0; $i2=0; $i2_aux=0; $validado="validado_"; $band_muestra=false;
          $ya_pintado=0; $cont_doc_cli=0; $doc_val=0;
          $get_val=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$id_operacion,"activo"=>1));
          foreach ($get_val as $k) {
            $con_clic++;
            $get_vc=$this->ModeloCatalogos->getselectwherestatus("*","docs_cliente",array("id_perfilamiento"=>$k->id_perfilamiento));
            foreach ($get_vc as $kc) {  
              $cont_doc_cli++;
            }
          }
          foreach ($get_val as $k) {
            $get_vc=$this->ModeloCatalogos->getselectwherestatus("*","docs_cliente",array("id_perfilamiento"=>$k->id_perfilamiento));
            foreach ($get_vc as $kc) {  
              //$cont_doc_cli++;
              $val[$i]=$kc->validado;
              /*echo "validado: ".$kc->validado."<br>";
              echo "con_clic: ".$con_clic."<br>";
              echo "cont_doc_cli: ".$cont_doc_cli."<br>";*/
              //if($val[$i]==1 && $cont_doc_cli==$con_clic){
              if($kc->validado==1){
                $doc_val++;
              }
              //echo "validado: ".$kc->validado."<br>";
              /*log_message('error', 'validado: '.$kc->validado);
              log_message('error', 'cont_doc_cli: '.$cont_doc_cli);
              log_message('error', 'con_clic: '.$con_clic);
              log_message('error', 'doc_val: '.$doc_val);
              log_message('error', 'con_clic: '.$con_clic);*/
           
              if($kc->validado==1 && $cont_doc_cli==$con_clic && $doc_val==$con_clic){
                //echo "validado: ".$kc->validado."<br>";
                /*echo "con_clic: ".$con_clic."<br>";
                echo "cont_doc_cli: ".$cont_doc_cli."<br>";
                echo "id_operacion: ".$id_operacion."<br>";
                echo "cont_db: ".$cont_db."<br>";
                echo "grado indice i: ".$grado[$i]."<br>";
                echo "id_docs_sinb: ".$id_docs_sinb."<br>";
                echo "valida_sin_bene: ".$valida_sin_bene."<br>";*/
                $disc[$i]="";
                $title_docsc="";
                if($id_operacion>0 && $cont_db>0 && isset($grado[1]) && $grado[1]>0 && $id_docs_sinb>0 && $valida_sin_bene>0)
                  $url=base_url()."Operaciones/Transaccion/".$id_operacion;
              }else{
                //echo "validado: ".$kc->validado."<br>";
                $disc[$i]="disabled";
                $url="javascript:void(0)";
                $title_docsc="Pendiente por validar expediente";
              }
              //echo "<br>disc: ".$i." " .$disc[$i]."<br>";
              $i++;
            } 
          }//foreach


          if($doc_val==$con_clic && $cont_doc_cli==$con_clic){
            $disc[0]=""; if(isset($disc[1])) { $disc[1]=""; } if(isset($disc[2])) { $disc[2]=""; }
            $title_docsc="";
            if($id_operacion>0 && $cont_db>0 && $id_docs_sinb>0 && $valida_sin_bene>0)
              $url=base_url()."Operaciones/Transaccion/".$id_operacion;
          }else{
            $disc[0]="disabled";
            if(isset($disc[1])) { $disc[1]="disabled"; } if(isset($disc[2])) { $disc[2]="disabled"; }
            $url="javascript:void(0)";
            $title_docsc="Pendiente por validar expediente";
          }

          //$title_docsb="";
          $cont_ex_be_val=0; $cont_exp_bene=0; $cont_benes = 0;
          $get_db=$this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_operacion,"activo"=>1));
          foreach ($get_db as $key) {
            $cont_db++;
            $cont_db=1;
            $cont_benes++;
            $tipo_bene=$key->tipo_bene;
            //echo "tipo_bene: ".$tipo_bene."<br>";
            if($key->tipo_bene==0){ //es declarado sin sueño beneficiario
              $valida_bene=0;
              $get_doc=$this->General_model->get_tableRow("doc_sin_bene",array("id_operacion"=>$id_operacion,'id_perfilamiento'=>$key->id_perfilamiento));
              if(isset($get_doc)){
                $id_docs_sinb=$get_doc->id;
                $acuse=$get_doc->acuse_sin_doc;
                $valida_sin_bene=$get_doc->validado;
                //log_message('error', 'valida_sin_bene : '.$get_doc->validado);
                //echo "valida_sin_bene: ".$get_doc->validado."<br>";
                //echo "tipo_bene: ".$tipo_bene."<br>";
              }
            }
            if($valida_sin_bene==0 && $tipo_bene==0){
              $title_docsb="Pendiente por validar expediente";
            }else if($valida_sin_bene>0 && $tipo_bene==0){
              $title_docsb="";
              $url=base_url()."Operaciones/Transaccion/".$id_operacion;
            }
            /*if($valida_bene==0 && $tipo_bene>0){
              $title_docsb="Pendiente por validar expediente";
            }else if($valida_bene>0 && $tipo_bene>0){
              $title_docsb="";
            }*/
            //echo "valida_bene: ".$valida_bene;
            //echo "tipo_bene: ".$tipo_bene."<br>";
            //echo "url: ".$url."<br>";
            
            $get_vb=$this->ModeloCatalogos->getselectwherestatus("*","docs_beneficiario",array("id_beneficiario"=>$key->id_duenio_bene,"tipo_bene"=>$key->tipo_bene));
            foreach ($get_vb as $kb) {
              $cont_exp_bene++;  
              //echo "valida_bene: ".$valida_bene;
              $val[$i2]=$kb->validado;
              if($val[$i2]==1){
                $dis="";
                //$title_docsb="";
                $cont_ex_be_val++;
                //echo "cont_exp_bene: ".$cont_exp_bene;
                //echo "cont_ex_be_val: ".$cont_ex_be_val."<br>";
                if($id_operacion>0 && $cont_db>0 && isset($grado[1]) && $grado[1]>0 && $cont_exp_bene==$cont_ex_be_val){
                  $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                  $band_pasoant=1;
                }
              }else{
                $dis="disabled";
                $url="javascript:void(0)";
                $title_docsb="Pendiente por validar expediente";
                //echo "<script type='text/javascript'>$('#btn_next').attr('disabled',true);</script>";
              }
              /*if($avanza==0 && isset($grado[1]) && $grado[1]>2.0 && $this->session->userdata('tipo')!=3 || $cont_exp_bene>$cont_ex_be_val){
                $dis_avance="disabled";
                $url="javascript:void(0)";
                $title_docsb="Pendiente por validar expediente";
              }*/
              
              if($cont_exp_bene==$cont_ex_be_val){
                $dis_avance="";
                $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                $title_docsb="";
              }
              if($cont_exp_bene>$cont_ex_be_val){
                $dis_avance="disabled";
                $url="javascript:void(0)";
                $title_docsb="Pendiente por validar expediente";
              }
              //echo "<br>url: " .$url."<br>";
              //echo "<br>id_operacion: " .$id_operacion."<br>";
              //echo "<br>grado[1]: " .$grado[1]."<br>";
              //echo "<br>val[i2]: " .$val[$i2]."<br>";
            }
          }
          //echo "cont_exp_bene: ".$cont_exp_bene."<br>";
          //echo "cont_ex_be_val: ".$cont_ex_be_val."<br>";
          //echo "cont_benes: ".$cont_benes."<br>";
          if($cont_benes==$cont_ex_be_val && $tipo_bene>0){
            $dis_avance="";
            $url=base_url()."Operaciones/Transaccion/".$id_operacion;
            $title_docsb="";
          }
          if($cont_benes>$cont_ex_be_val && $tipo_bene>0){
            $dis_avance="disabled";
            $url="javascript:void(0)";
            $title_docsb="Pendiente por validar expediente";
          }
          //echo "url: ".$url."<br>";
          /*if($valida_sin_bene>0 && $tipo_bene==0){
            $title_docsb="";
          }else if($valida_sin_bene==0 && $tipo_bene==0){
            $title_docsb="Pendiente por validar expediente";
          }
          if($valida_bene>0 && $tipo_bene>0){
            $title_docsb="";
          }else if($valida_bene==0 && $tipo_bene>0){
            $title_docsb="Pendiente por validar expediente";
          }*/

          $dis_ga="";
          if(isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==15 && $this->session->userdata('tipo')!=3 ||
            isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==16 && $this->session->userdata('tipo')!=3 ||
            isset($calificacion) && $avanza==0 && $calificacion>=1.07 && $id_act_calc!=15 && $id_act_calc!=16 && $this->session->userdata('tipo')!=3){
            //echo "autorizado: ".$autorizado;
            if($autorizado==0){
              $dis_ga="disabled";
              }else{
                $dis_ga="";
              }
          }
          //echo $dis_ga;
          if($idtipo_cliente=="6" || $idtipo_cliente=="7"){ ?>
          <div class="col-md-3" align="center">
              <div class="row texto_centro">

              </div>  
          </div>
        <?php }
          $url="javascript:void(0)"; $dis="disabled"; if($id_operacion>0 && $cont_db>0 || $id_operacion>0 && $idtipo_cliente=="6" || $id_operacion>0 && $idtipo_cliente=="7"){ $url=base_url()."Operaciones/documentosCliente/".$id_operacion; $dis=""; } if($title!="" || $dis_ga!="") { $dis="disabled"; $url="javascript:void(0)"; } ?>
          <div class="col-md-3" align="center">
            <a disabled href="<?php echo $url; ?>" <?php echo "title='$title_docsc'"; ?>><button style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $dis; ?>>
              <div class="row texto_centro">
                <div class="col-md-2" align="right">
                  <i class="fa fa-files-o btn-icon-prepend icon_yi2"></i>
                </div>
                <div class="col-md-10" align="left">
                  <span style="font-size: 22px; font-weight: bold;">Expediente de Cliente </span>
                </div>
              </div>  
            </button></a>
            <p style="font-size: 16px; font-weight: bold; color: red"><?php echo $title_docsc; if($title_docsc=="") echo $title; ?></p>
          </div>

          <?php if($idtipo_cliente!="6" && $idtipo_cliente!="7"){ 
            $url="javascript:void(0)"; $dis="disabled"; if($id_operacion>0 && $cont_db>0){ $url=base_url()."Operaciones/documentosBenes/".$id_operacion; $dis=""; } if($title!="" || $dis_ga!=""){ $dis="disabled"; $url="javascript:void(0)"; } ?>
            <div class="col-md-3" align="center">
              <a disabled href="<?php echo $url; ?>" <?php echo "title='$title_docsb'"; ?>><button style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $dis; ?>>
                <div class="row texto_centro">
                  <div class="col-md-2" align="right">
                    <i class="fa fa-files-o btn-icon-prepend icon_yi2"></i>
                  </div>
                  <div class="col-md-10" align="left">
                    <span style="font-size: 22px; font-weight: bold;">Expediente de Dueño Beneficiario </span>
                  </div>
                </div>  
              </button></a>
              <p style="font-size: 16px; font-weight: bold; color: red"><?php echo $title_docsb; ?></p>
            </div>
          <?php }

          /*$title="Falta realizar calculo de grado de riesgo";
          if(isset($grado[0]) && $grado[0]>0 || isset($grado[1]) && $grado[1]>0){
            $title="";
          }
          $url="javascript:void(0)"; $dis="disabled"; $disc[0]="disabled"; $disb="disabled";
          $con_clic=0; $i=0; $i2=0; $i2_aux=0; $validado="validado_"; $band_muestra=false;
          $ya_pintado=0;
          $get_val=$this->ModeloCatalogos->getselectwherestatus("*","operacion_cliente",array("id_operacion"=>$id_operacion,"activo"=>1));
          foreach ($get_val as $k) {
            $con_clic++;
            $get_vc=$this->ModeloCatalogos->getselectwherestatus("*","docs_cliente",array("id_perfilamiento"=>$k->id_perfilamiento));
            foreach ($get_vc as $kc) {  
              
              $val[$i]=$kc->validado;
              //echo "validado: ".$kc->validado."<br>";
              if($val[$i]==1){
                $disc[$i]="";
                if($id_operacion>0 && $cont_db>0 && isset($grado[1]) && $grado[1]>0 && $id_docs_sinb>0 && $valida_sin_bene>0)
                  $url=base_url()."Operaciones/Transaccion/".$id_operacion;
              }else{
                $disc[$i]="disabled";
                $url="javascript:void(0)";
              }
              //echo "<br>disc: ".$i." " .$disc[$i]."<br>";
              $i++;
            } 
          }//foreach
          */

          $get_val=$this->ModeloCatalogos->getselectwherestatus("*","operacion_beneficiario",array("id_operacion"=>$id_operacion,"activo"=>1));
          $dis_avance="";
          if($avanza==1){
            $dis_avance="";
          }/*else if($avanza==0 && isset($grado[1]) && $grado[1]>2.0 && $this->session->userdata('tipo')!=3){
            $dis_avance="disabled";
            $url="javascript:void(0)";
          }*/else if(isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==15 && $this->session->userdata('tipo')!=3 ||
            isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==16 && $this->session->userdata('tipo')!=3 ||
            isset($calificacion) && $avanza==0 && $calificacion>=1.07 && $id_act_calc!=15 && $id_act_calc!=16 && $this->session->userdata('tipo')!=3){
              $dis_avance="disabled";
              $url="javascript:void(0)";
          }

          /* *************************************************** */
          $url_fide="javascript:void(0)"; $band_pasoant_fide=0;
          if($id_operacion>0 && $idtipo_cliente=="6" && isset($grado[1]) && $grado[1]>0 && $title_docsc=="" || 
            $id_operacion>0 && $idtipo_cliente=="7" && isset($grado[1]) && $grado[1]>0 && $title_docsc=="" ){
            $url_fide=base_url()."Operaciones/Transaccion/".$id_operacion;
            $band_pasoant_fide=1;
          }
          if($autorizado>0 && $disc[0]=="" && $band_pasoant_fide==1 && $titleda=="" && $titleca=="" && $title_docsc==""){
            $dis_avance="";
            $url_fide=base_url()."Operaciones/Transaccion/".$id_operacion;
            if($id_union>0) {
              if($disc[1]=="")
                $url_fide=base_url()."Operaciones/Transaccion/".$id_operacion;
              else
                $url_fide="javascript:void(0)";
            } 
            //echo "<br>url: " .$url."<br>";
          }
          if($idtipo_cliente=="6" || $idtipo_cliente=="7"){ $ya_pintado=1; //pinta btn de transaccion para fideicomisos ?>
            <div class="col-md-3" align="center">
              <a href="<?php echo $url_fide; ?>"><button id="btn_next" style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $dis_avance." "; echo $dis; if($disc[0]!="") echo " ".$disc[0]; if(isset($disc[1]) && $disc[1]!="") echo " ".$disc[1]; ?>>
                <div class="row texto_centro">
                  <div class="col-md-2" align="right">
                    <i class="fa fa-gear btn-icon-prepend icon_yi2"></i>
                  </div>
                  <div class="col-md-10" align="left">
                    <span style="font-size: 22px; font-weight: bold;">Transacción </span>
                  </div>
                </div>  
              </button></a>
            </div>
          <?php }

           /************************************************************ */

          foreach ($get_val as $k) {
            if($k->tipo_bene>0 && $idtipo_cliente!="6" && $idtipo_cliente!="7"){
              //echo "<br>tipo_bene: " .$k->tipo_bene."<br>";
              $band_muestra=true;
              $band_pasoant=0;
              $get_vb=$this->ModeloCatalogos->getselectwherestatus("*","docs_beneficiario",array("id_beneficiario"=>$k->id_duenio_bene,"tipo_bene"=>$k->tipo_bene));
              foreach ($get_vb as $kb) {  
                $val[$i2]=$kb->validado;
                if($val[$i2]==1){
                  $dis="";
                  if($id_operacion>0 && $cont_db>0 && isset($grado[1]) && $grado[1]>0){
                    $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                    $band_pasoant=1;
                  }
                }else{
                  $dis="disabled";
                  $url="javascript:void(0)";
                  //echo "<script type='text/javascript'>$('#btn_next').attr('disabled',true);</script>";
                }
                if($avanza==0 && isset($grado[1]) && $grado[1]>2.0 && $this->session->userdata('tipo')!=3){
                  $dis_avance="disabled";
                  $url="javascript:void(0)";
                }
                //echo "<br>url: " .$url."<br>";
                //echo "<br>id_operacion: " .$id_operacion."<br>";
                //echo "<br>grado[1]: " .$grado[1]."<br>";

              }
              //echo "<br>avanza: " .$avanza."<br>";
              if($avanza==1){
              }/*else if($avanza==0 && isset($grado[1]) && $grado[1]>2.0 && $this->session->userdata('tipo')!=3 || $titleda!="" || $titleca!=""){
                $url="javascript:void(0)";
              }*/else if(isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==15 && $this->session->userdata('tipo')!=3 ||
                isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==16 && $this->session->userdata('tipo')!=3 ||
                isset($calificacion) && $avanza==0 && $calificacion>=1.07 && $id_act_calc!=15 && $id_act_calc!=16 && $this->session->userdata('tipo')!=3 || $titleda!="" || $titleca!=""){
                  $url="javascript:void(0)";
              }
              if($autorizado>0 && $disc[0]=="" && $band_pasoant==1 && $titleda=="" && $titleca==""){
                $dis_avance="";
                $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                if($id_union>0) {
                  if($disc[1]=="")
                    $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                  else
                    $url="javascript:void(0)";
                } 
                //echo "<br>url: " .$url."<br>";
              }
              $i2_aux++;
              //echo "<br>i2_aux: " .$i2_aux."<br>";
              //echo "<br>id_union: " .$id_union."<br>";
              //echo "<br>band_pasoant: " .$band_pasoant."<br>";
              if($title_docsb!="" || $title_docsc!="" || $title!=""){
                $com_url="disabled";
                $url="javascript:void(0)";
                //echo "<br>url: " .$url."<br>";
              }
              if($i2_aux<=1 && $band_pasoant==1 && $ya_pintado==0){ $ya_pintado=1; 
                $com_url="";
                if($url=="javascript:void(0)" || $title_docsb!="" || $title_docsc!="" || $title!=""){
                  $com_url="disabled";
                }
                //echo "<br>url: " .$url."<br>";
                ?>
                <div class="col-md-3" align="center">
                  <a href="<?php echo $url; ?>"><button id="btn_next" style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $com_url; ?> <?php echo $dis_avance." "; echo $dis; if($disc[0]!="") echo " ".$disc[0]; if(isset($disc[1]) && $disc[1]!="") echo " ".$disc[1]; ?>>
                    <div class="row texto_centro">
                      <div class="col-md-2" align="right">
                        <i class="fa fa-gear btn-icon-prepend icon_yi2"></i>
                      </div>
                      <div class="col-md-10" align="left">
                        <span style="font-size: 22px; font-weight: bold;">Transacción </span>
                      </div>
                    </div>  
                  </button></a>
                </div>
            <?php }
              $i2++;
            }else if($k->tipo_bene==0 && $idtipo_cliente!="6" && $idtipo_cliente!="7"){  //declarado sin dueño beneficiario
              //echo "id_docs_sinb: ". $id_docs_sinb."<br>";
              //echo "acuse: ". $acuse."<br>";
              $band_muestra=true;
              $band_pasoant=0;
              /*echo "<br>id_operacion: " .$id_operacion."<br>";
              echo "<br>cont_db: " .$cont_db."<br>";
              echo "<br>grado[1]: " .$grado[1]."<br>";
              echo "<br>id_docs_sinb: " .$id_docs_sinb."<br>";
              echo "<br>valida_sin_bene: " .$valida_sin_bene."<br>";
              echo "<br>acuse: " .$acuse."<br>";
              echo "<br>id_union: " .$id_union."<br>";
              echo "<br>disc[0]: " .$disc[0]."<br>";*/

              if($id_operacion>0 && $cont_db>0 && isset($grado[1]) && $grado[1]>0 && $id_docs_sinb>0 && $valida_sin_bene>0 && $acuse!="" && $disc[0]=="" && $id_union==0 && $titleda=="" && $titleca=="" && $title==""){ $url=base_url()."Operaciones/Transaccion/".$id_operacion; $dis=""; $band_pasoant=1; } 
              else if($id_operacion>0 && $cont_db>0 && isset($grado[1]) && $grado[1]>0 && $id_docs_sinb>0 && $valida_sin_bene>0 && $acuse!="" && $disc[0]=="" && $id_union>0 && $disc[1]=="" /*&& $grado[2]>0*/ && $titleda=="" && $titleca=="" && $title==""){ 
                $url=base_url()."Operaciones/Transaccion/".$id_operacion; $dis="";  //echo "id_union: ". $id_union."<br>"; 
                $band_pasoant=1;
              } 
              else { $url="javascript:void(0)"; } 
              //echo "<br>url: " .$url."<br>";
              //echo "url: ". $url;
              //echo " avanza: ". $avanza;
              //echo " grado: ". $grado;
              if($avanza==1){
              }/*else if($avanza==0 && isset($grado[1]) && $grado[1]>2.0 && $titleda!="" && $titleca!="" && $this->session->userdata('tipo')!=3){
                $url="javascript:void(0)";
              }*/
              else if(isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==15 && $this->session->userdata('tipo')!=3 && $titleda!="" && $titleca!="" ||
                isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==16 && $this->session->userdata('tipo')!=3 && $titleda!="" && $titleca!="" ||
                isset($calificacion) && $avanza==0 && $calificacion>=1.07 && $id_act_calc!=15 && $id_act_calc!=16 && $this->session->userdata('tipo')!=3 && $titleda!="" && $titleca!=""){
                  $url="javascript:void(0)";
              }

              if($autorizado>0 && $disc[0]=="" && $band_pasoant==1 && $titleda=="" && $titleca=="" && $title==""){
                $dis_avance="";
                $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                if($id_union>0) {
                  if($disc[1]=="")
                    $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                  else
                    $url="javascript:void(0)";
                } 
              }
              /*if($avanza==0 && isset($grado[1]) && $grado[1]>2.0 && $this->session->userdata('tipo')!=3){
                $dis_avance="disabled";
                $url="javascript:void(0)";
              }*/
              if(isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==15 && $this->session->userdata('tipo')!=3 ||
                isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==16 && $this->session->userdata('tipo')!=3 ||
                isset($calificacion) && $avanza==0 && $calificacion>=1.07 && $id_act_calc!=15 && $id_act_calc!=16 && $this->session->userdata('tipo')!=3 ){
                  $dis_avance="disabled";
                  $url="javascript:void(0)";
                  if($autorizado>0 && $title_docsc=="" && $title_docsb==""){
                    $dis_avance="";
                    $url=base_url()."Operaciones/Transaccion/".$id_operacion;
                  }
              }
              
              /*echo " disc 0: ". $disc[0];
              //echo " disc1: ". $disc[1];
              echo "url: ". $url;
              echo " autorizado: ". $autorizado;
              //echo " cont_db: ". $cont_db;
              echo " valida_sin_bene: ". $valida_sin_bene;
              echo " band_pasoant: ". $band_pasoant;
              echo " id_docs_sinb: ". $id_docs_sinb;*/
              //echo "<br>i2_aux: " .$i2_aux."<br>";
              if($i2_aux==0 && $ya_pintado==0){ //osea que no existe otro mas que declarado sin dueño 
                $ya_pintado=1;
                $com_url="";
                if($url=="javascript:void(0)" || $title!="" || $titleda!="" || $titleca!=""){
                  $com_url="disabled";
                } ?>
                <div class="col-md-3" align="center">
                  <a href="<?php echo $url; ?>" <?php echo "title='$title'"; ?>><button id="btn_next" style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $com_url; ?> <?php echo $dis_avance." "; echo $dis; if($disc[0]!="") echo " ".$disc[0]; 
                  if($id_union>0) {
                    if(isset($disc[1]) && $disc[1]!="") echo " ".$disc[1]; 
                  }?>
                  >
                    <div class="row texto_centro">
                      <div class="col-md-2" align="right">
                        <i class="fa fa-gear btn-icon-prepend icon_yi2"></i>
                      </div>
                      <div class="col-md-10" align="left">
                        <span style="font-size: 22px; font-weight: bold;">Transacción </span>
                      </div>
                    </div>  
                  </button></a>
                </div>
              <?php }
              ?>
            <?php } //else
          }
          /*echo "<br>_____________<br>";
          echo "<br>i2_aux: " .$i2_aux."<br>";
          echo "con_clic: ".$con_clic; 
          echo "<br> i2 ".$i2."<br>";
          echo " grado[1] ".$grado[1];*/
          if($id_operacion==0 || $con_clic==0 && $i2==0 || $con_clic>0 && $i2==0 && $band_muestra==false && $ya_pintado==0 || !isset($grado[1]) && $i2_aux>0 && $ya_pintado==0 ){ $ya_pintado=1; 
            $com_url="";
            if($url=="javascript:void(0)" || $title!="" || $titleda!="" || $titleca!="" || $title_docsb!="" || $title_docsc!=""){
              $com_url="disabled";
            }
            if($idtipo_cliente!="6" || $idtipo_cliente!="7"){
            ?>
              <div class="col-md-3" align="center">
                <a href="<?php echo $url; ?>" <?php echo "title='$title'"; ?>><button id="btn_next" style="width: 260px;" class="btn gradient_nepal tam_btn" <?php echo $com_url; ?> <?php echo $dis_avance." "; echo $dis; ?>>
                  <div class="row texto_centro">
                    <div class="col-md-2" align="right">
                      <i class="fa fa-gear btn-icon-prepend icon_yi2"></i>
                    </div>
                    <div class="col-md-10" align="left">
                      <span style="font-size: 22px; font-weight: bold;">Transacción </span>
                    </div>
                  </div>  
                </button></a>
              </div>
          <?php  }
           }
          //echo $ya_pintado;
          if($ya_pintado==0 && $idtipo_cliente!="6" && $idtipo_cliente!="7"){
          $com_url="";
          if($url=="javascript:void(0)" || $title!="" || $titleda!="" || $titleca!="" || $title_docsb!="" || $title_docsc!=""){
            $com_url="disabled";
          }
          ?>
          <div class="col-md-3" align="center">
            <a href="javascript:void(0)" <?php echo "title='$title'"; ?>><button id="btn_next" style="width: 260px;" class="btn gradient_nepal tam_btn" disabled="" <?php echo $com_url; ?>>
              <div class="row texto_centro">
                <div class="col-md-2" align="right">
                  <i class="fa fa-gear btn-icon-prepend icon_yi2"></i>
                </div>
                <div class="col-md-10" align="left">
                  <span style="font-size: 22px; font-weight: bold;">Transacción </span>
                </div>
              </div>  
            </button></a>
          </div>
          <?php }
          //echo "avanza: ".$avanza;
          //echo " grado: ".$grado[1];
          /*if(isset($grado[1]) && $avanza==0 && $grado[1]>2.0 && $this->session->userdata('tipo')!=3 || isset($grado[2]) && $grado[2]>2.0  && $avanza==0 && $this->session->userdata('tipo')!=3)*/
          /*echo "calificacion: ".$calificacion;
          echo "avanza: ".$avanza;
          echo "id_act_calc: ".$id_act_calc;
          echo "tipo de sesion: ".$this->session->userdata('tipo');*/
          if(isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==15 && $this->session->userdata('tipo')!=3 ||
            isset($calificacion) && $avanza==0 && $calificacion>=1.14 && $id_act_calc==16 && $this->session->userdata('tipo')!=3 ||
            isset($calificacion) && $avanza==0 && $calificacion>=1.07 && $id_act_calc!=15 && $id_act_calc!=16 && $this->session->userdata('tipo')!=3){
            //echo "autorizado: ".$autorizado;
            if($autorizado==0){
              echo '<div class="col-md-6" align="center"></div>
              <div class="col-md-6" align="center">
                <p style="font-size: 16px; font-weight: bold; color: red">No se permite continuar con el proceso por la configuración realizada en el diagnostico de alertas</p>
                </div>';
              }else{
                echo '<div class="col-md-6" align="center"></div>
                <div class="col-md-6" align="center">
                  <p style="font-size: 16px; font-weight: bold; color: red">Operación autorizada para continuar</p>
                </div>';
              }
            /*echo '
              <a title="Autorizar continuidad de operación" href="#modal_autoriza" data-toggle="modal"><button type="button" class="btn gradient_nepal2" id="ver_autoriza"><i class="fa fa-arrow-left"></i> Autorizar</button></a>';  */
          }

          //echo "<br>dis: ".$dis."<br>";
          //echo "<br>disc: ".$disc."<br>";
          //echo "<br>disb: ".$disb."<br>";
          //if($id_operacion>0 && $cont_db>0 && $grado>0 && $id_docs_sinb>0){ $url=base_url()."Operaciones/Transaccion/".$id_operacion; $dis=""; } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<!--- Modal rfc -->
<div class="modal fade" id="modal_autoriza" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Autorización</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5><strong>Ingresa los datos para autorizar la continuidad de la operación</strong></h5>
        <input class="form-control" type="hidden" id="id_autorizado" value="0">
        <div class="row">
          <div class="col-md-12 form-group">
            <label> <i class="fa fa-user"></i> Usuario:</label>
            <input autocomplete="off" class="form-control" type="text" name="Usuario" id="usuario">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-group">
            <label><i class="fa fa-key"></i> Contraseña:</label>
            <input autocomplete="off" class="form-control" type="password" name="contrasena" id="contrasena">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-group">
            <label>Motivo de autorización:</label>
            <input class="form-control" type="text" name="motivo" id="motivo">
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="aceptarValida()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
