<input type="hidden" id="id_opera" value="<?php echo $id_operacion ?>">
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-12" align="right">
          <a href="<?php echo base_url() ?>Operaciones/procesoInicial/<?php echo $id_operacion; ?>"><button type="button" class="btn gradient_nepal2"><i class="fa fa-rocket"></i> Regresar a Proceso</button></a>
          <a href="<?php echo base_url() ?>Operaciones"><button type="button" class="btn gradient_nepal2"><i class="fa fa-arrow-left"></i> Regresar a inicio</button></a>
        </div>
        <hr class="subtitle">
        <!---------------->
          <br>
          <!--<h1 style="color: #b57532;" class="barra_menu" align="center">Resultados Clientes-Beneficiarios asignados a la operación <strong><?php echo /*$id_operacion;*/ $folio; ?></strong></h1>-->
          <h1 style="color: #b57532;" class="barra_menu" align="center">Resultados de consulta a listados </h1>
          <br>
          <div class="col-md-12">
            <h3 class="barra_menu"></h3><hr class="">
            <?php $i=0; $nombre=""; foreach ($infoc as $k) {
                $infoc_det = explode(",", $k->resultado);
                //$impc=$infoc_det[$i];
              //echo "<br>perfila: ".$k->id_perfilamiento;
              $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$k->id_perfilamiento));
              foreach ($get_pp as $g) {
                //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                $tipoccon = $g->idtipo_cliente;
                //echo "<br>tipoccon: ".$tipoccon;
                if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                if($tipoccon==6) $tabla = "tipo_cliente_f";
                //echo "<br>tabla: ".$tabla;
                
                $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
                foreach ($get_per as $g2) {
                  //echo "<br>k->id_cliente: ".$k->id_cliente;
                  if($tipoccon==1 || $tipoccon==2) $nombre .= $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno."<span id='cont_btn_pep'><button type='button' onclick='cuestionarioPEP(".$k->id_perfilamiento.",".$tipoccon.")' class='btn cuestionario' style='color: #212b4c;' title='Cuestionario PEP'><i class='fa fa-book' style='font-size: 22px;'></i> Cuestionario PEP</button> </span> 
                    <button type='button' class='btn btn-warning btn-rounded btn-icon' onclick='ayuda_pep()'>
                      <i class='mdi mdi-help'></i>
                    </button>";
                  if($tipoccon==3) $nombre .= $g2->razon_social."<span id='cont_btn_pep'><button type='button' onclick='cuestionarioPEP(".$k->id_perfilamiento.",".$tipoccon.")' class='btn cuestionario' style='color: #212b4c;' title='Cuestionario PEP'><i class='fa fa-book' style='font-size: 22px;'></i> Cuestionario PEP</button></span> 
                    <button type='button' class='btn btn-warning btn-rounded btn-icon' onclick='ayuda_pep()'>
                      <i class='mdi mdi-help'></i>
                    </button> ";
                  if($tipoccon==4) $nombre .= $g2->nombre_persona."<span id=spannt_btn_pep'><button type='button' onclick='cuestionarioPEP(".$k->id_perfilamiento.",".$tipoccon.")' class='btn cuestionario' style='color: #212b4c;' title='Cuestionario PEP'><i class='fa fa-book' style='font-size: 22px;'></i> Cuestionario PEP</button></span> 
                    <button type='button' class='btn btn-warning btn-rounded btn-icon' onclick='ayuda_pep()'>
                      <i class='mdi mdi-help'></i>
                    </button> ";
                  if($tipoccon==5 || $tipoccon==6) $nombre .= $g2->denominacion."<span id='cont_btn_pep'><button type='button' onclick='cuestionarioPEP(".$k->id_perfilamiento.",".$tipoccon.")' class='btn cuestionario' style='color: #212b4c;' title='Cuestionario PEP'><i class='fa fa-book' style='font-size: 22px;'></i> Cuestionario PEP</button></span> 
                    <button type='button' class='btn btn-warning btn-rounded btn-icon' onclick='ayuda_pep()'>
                      <i class='mdi mdi-help'></i>
                    </button> ";
                }
              }
            } ?>
            <!--<button type='button' class='btn btn-warning btn-rounded btn-icon' onclick='ayuda_pep()'>
              <i class='mdi mdi-help'></i>
            </button>-->
            <?php 
                  echo "<br><div class='col-md-12'>
                    <!--<table width='100%' class='table'>
                      <thead>
                        <tr>
                          <th width='10%'>
                            
                          </th>
                          <th width='40%'>
                            RESULTADO
                          </th>
                        </tr>
                      </thead>
                      
                      <tr>
                        <td width='5%'>
                          <h3 class='barra_menu'>".$nombre. "</h3>
                        </td>
                        <td width='40%'>
                          ".$k->resultado."<br>
                        </td>
                      </tr>
                    </table>-->
                    <div class='table-responsive'>
                      <h3 class='barra_menu'>Cliente(s): ".$nombre. "</h3>
                      <table class='table' id='tabla_resultc' width='100%'>
                        <thead>
                          <tr>
                            <th colspan='8'></th>
                            <th><a data-toggle='modal' data-target='#ayuda_tipo'><button type='button' class='btn btn-warning btn-rounded btn-icon' style='text-align:center'>
                                <i class='mdi mdi-help'></i>
                              </button></a> 
                            </th>
                            <th colspan='5'></th>
                            <th>
                              <a data-toggle='modal' data-target='#ayuda_denomina'><button type='button' class='btn btn-warning btn-rounded btn-icon' style='text-align:center'>
                                <i class='mdi mdi-help'></i>
                              </button></a> 
                            </th>
                            <th colspan='2'></th>
                          </tr>
                          <tr>
                            <th>Denominacion</th>
                            <th>Identificacion</th>
                            <th>Id_Tributaria</th>
                            <th>Otra Identificacion</th>
                            <th>Cargo</th>
                            <th>Lugar de Trabajo</th>
                            <th>Dirección</th>
                            <th>Enlace</th>
                            <th>Tipo</th>
                            <th>SubTipo</th>
                            <th>Estado</th>
                            <th>Lista</th>
                            <th>Pais_Lista</th>
                            <th>Cod_Individuo</th>
                            <th>Exactitud_Denominacion</th>
                            <th>Exactitud_Identificacion</th>
                            <th>Aviso 24 hrs.</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div><hr>
                  </div>";
                //}
              //}
             // $i++;
              //echo "i: ".$i;
            //} ?>
          </div>
          <hr class="subtitle">
          <div class="col-md-12">
            <h3 class="barra_menu">Resultado de la consulta a listas para el Dueño Beneficiario:</h3><hr class="">
            <?php 
            $con_benes=0;
            $con_benes_aux=0;
            foreach ($infob as $b) {
              $con_benes++;
            }
            if($con_benes>0){
              $nombreb="";
              $apellidosb="";
              foreach ($infob as $b) {
                $con_benes_aux++;
                $tipo_bene=$b->tipo_bene;
                if($tipo_bene==1){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fisica','id',$b->id_duenio_bene);
                }else if($tipo_bene==2){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_moral_moral','id',$b->id_duenio_bene);
                }else if($tipo_bene==3){
                  $results=$this->ModeloCatalogos->getselectwhere('beneficiario_fideicomiso','id',$b->id_duenio_bene);
                }
                $info="";
                if($tipo_bene==0){
                  $info .= '<div><p>Declarado "sin dueño beneficiario"</p></div><hr>';
                }
                if($tipo_bene>0){
                  foreach ($results as $k) {
                    if($tipo_bene==1){
                      $nombreb .= $k->nombre;
                      $apellidosb .= $k->apellido_paterno." ".$k->apellido_materno."<span id='cont_btndb_pep'></span> - ";
                    }
                    if($tipo_bene==2 || $tipo_bene==3){
                      //$nombreb = "";
                      $apellidosb .= $k->razon_social."<span id='cont_btndb_pep'></span> - ";
                    }
                    if($tipo_bene==0){
                      //$nombreb="";
                      $apellidosb.='Declarado "sin dueño beneficiario"';
                    }
                  }
                
                  $info .= "<div>
                        <div class='col-md-12'>
                           <div class='table-responsive'>
                            <h3 class='barra_menu'>".$nombreb." ".$apellidosb."</h3>
                            <table class='table' id='tabla_resultdb' width='100%'>
                              <thead>
                              <tr>
                                <th colspan='8'></th>
                                <th><a data-toggle='modal' data-target='#ayuda_tipo'><button type='button' class='btn btn-warning btn-rounded btn-icon' style='text-align:center'>
                                    <i class='mdi mdi-help'></i>
                                  </button></a> 
                                </th>
                                <th colspan='5'></th>
                                <th>
                                  <a data-toggle='modal' data-target='#ayuda_denomina'><button type='button' class='btn btn-warning btn-rounded btn-icon' style='text-align:center'>
                                    <i class='mdi mdi-help'></i>
                                  </button></a> 
                                </th>
                                <th colspan='2'></th>
                              </tr>
                                <tr>
                                  <th>Denominacion</th>
                                  <th>Identificacion</th>
                                  <th>Id_Tributaria</th>
                                  <th>Otra Identificacion</th>
                                  <th>Cargo</th>
                                  <th>Lugar de Trabajo</th>
                                  <th>Dirección</th>
                                  <th>Enlace</th>
                                  <th>Tipo</th>
                                  <th>SubTipo</th>
                                  <th>Estado</th>
                                  <th>Lista</th>
                                  <th>Pais_Lista</th>
                                  <th>Cod_Individuo</th>
                                  <th>Exactitud_Denominacion</th>
                                  <th>Exactitud_Identificacion</th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>
                          </div><hr>
                        </div>";

                  }
                    
                  //} //forech de datos de benes
                }
                echo $info;
              //}//foreach getbenes


            }else{
              foreach ($infoc as $k) {
                //echo "<br>perfila: ".$k->id_perfilamiento;
                $get_pp = $this->ModeloCatalogos->getselectwherestatus("*","perfilamiento",array("idperfilamiento"=>$k->id_perfilamiento));
                foreach ($get_pp as $g) {
                  //echo "<br>idtipo_cliente: ".$g->idtipo_cliente;
                  $tipoccon = $g->idtipo_cliente;
                  //echo "<br>tipoccon: ".$tipoccon;
                  if($tipoccon==1) $tabla = "tipo_cliente_p_f_m";
                  if($tipoccon==2) $tabla = "tipo_cliente_p_f_e";
                  if($tipoccon==3) $tabla = "tipo_cliente_p_m_m_e";
                  if($tipoccon==4) $tabla = "tipo_cliente_p_m_m_d";
                  if($tipoccon==5) $tabla = "tipo_cliente_e_c_o_i";
                  if($tipoccon==6) $tabla = "tipo_cliente_f";
                  //echo "<br>tabla: ".$tabla;
                  $get_per=$this->ModeloCatalogos->getselectwherestatus("*",$tabla,array('idperfilamiento'=>$g->idperfilamiento));
                  foreach ($get_per as $g2) {
                    //echo "<br>k->id_cliente: ".$k->id_cliente;
                    if($tipoccon==1 || $tipoccon==2) $nombre = $g2->nombre." ".$g2->apellido_paterno." ".$g2->apellido_materno;
                    if($tipoccon==3) $nombre = $g2->razon_social;
                    if($tipoccon==4) $nombre = $g2->nombre_persona;
                    if($tipoccon==5 || $tipoccon==6) $nombre = $g2->denominacion;
                    
                    echo "<br><div class='col-md-12'><h3 class='barra_menu'>Cliente: ".$nombre.":</h3> 
                      <br><p>Sin dueño beneficiario asignado</p></div><hr>";
                  }
                }
              }
            }
            ?>
          
          <br><br>
          <div class="row">
            <div class="col-md-12" align="right">
              <!--<a href="<?php echo base_url();?>Operaciones/benesOperacion/<?php echo $id_operacion;?>" class="btn gradient_nepal2" ><i class="fa fa-arrow-left"></i> Atrás</a>-->
              <a href="<?php echo base_url();?>Operaciones/procesoInicial/<?php echo $id_operacion;?>" class="btn gradient_nepal2"><i class="fa fa-arrow-right"></i> Aceptar</a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>


<div class="modal fade" id="ayuda_tipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Tipo</h5>
       <span style="text-align : justify; font-size: 12px">
          <li>Si el resultado es ONU, OFAC, Reino Unido, DEA, FBI, Interpol, PGR México, 69 y 69b del SAT, se recomienda verificar que no se trate de un homónimo y en caso de que no sea un homónimo se sugiere cancelar la operación y generar un reporte de 24 horas.</li>
          <li>Para verificar la coincidencia de la consulta, apoyarse en la columna “Exactitud-Denominación”.</li>
          <li>Si el resultado es PEP (Persona Políticamente Expuesta) se recomienda validar si no es  un homónimo y en caso de que no sea un homónimo, solicitar mayor información y requisitar el formulario PEP, cuentas con un acceso directo en esta pantalla.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="ayuda_denomina" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Exactitud Denominación</h5>
       <span style="text-align : justify; font-size: 12px">
          <li>0 de 5 significa que la coincidencia es mínima, caso contrario 5 de 5 representa una alta probabilidad de que el cliente dado de alta, sea la persona reportada en la lista. No obstante se debe de validar homonimia.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ayuda_pep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Cuestionario PEP</h5>
         <span style="text-align : justify; font-size: 12px">
            <li>Este formato se debe de requisitar, si el cliente o dueño beneficiario aparece como persona políticamente expuesta como resultado de la búsqueda en listas,  y además se corrobora que no es un homónimo.</li>
         </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
