<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #25294c;
  }
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
</style>
<script src="<?php echo base_url(); ?>template/vendors/js/vendor.bundle.base.js"></script>
<script src="<?php echo base_url(); ?>template/vendors/inputmask/jquery.inputmask.bundle.js"></script>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 >Nuevo usuario general</h3>
        <hr class="subtitle">
        <form class="form" method="post" role="form" id="form_usuario_cliente" autocomplete="off">
          <input type="hidden" name="idcliente_usuario" id="idcliente_usuario" value="<?php echo $idcliente_usuario ?>">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Nombre(s)</label>
              <input class="form-control campo" type="text" name="nombre" value="<?php echo $nombre ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido paterno</label>
              <input class="form-control campo" type="text" name="apellido_paterno" value="<?php echo $apellido_paterno ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido materno</label>
              <input class="form-control campo" type="text" name="apellido_materno" value="<?php echo $apellido_materno ?>">
            </div>
            <div class="col-md-8 form-group">
              <label>Función/Puesto</label>
              <!--- 
              <select class="form-control campos" name="funcion_puesto" id="funcion_puesto">
                  <option selected="" disabled="" value="0">Seleccione una opción</option>
                <?php foreach ($get_puesto as $item) { ?>
                   <option value="<?php echo $item->perfilId ?>" <?php if($item->perfilId==$perfilId) echo 'selected' ?>><?php echo $item->perfil ?></option>
                <?php } ?>
              </select> -->
              <input class="form-control" type="text" name="funcion_puesto" value="<?php echo $funcion_puesto ?>">
            </div>
          </div>
          <div class="row">  
            <div class="col-md-12">
              <hr class="subtitle">
              <h4>Dirección</h4>
            </div>
            <div class="col-md-3 form-group">
              <label>Tipo de vialidad</label>
              <select class="form-control campos" name="tipo_vialidad">
                <option disabled selected>Selecciona un tipo</option>
                <?php foreach ($get_tipo_vialidad as $item){ ?>
                    <option value="<?php echo $item->id ?>" <?php if($item->id==$tipo_vialidad) echo 'selected' ?>><?php echo $item->nombre ?></option> 
                <?php } ?>
              </select>
            </div>
            <div class="col-md-5 form-group">
              <label>Calle</label>
              <input class="form-control campo" type="text" name="calle" value="<?php echo $calle ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>No.ext</label>
              <input class="form-control campo" type="text" name="no_ext" value="<?php echo $no_ext ?>">
            </div>
            <div class="col-md-2 form-group">
              <label>No.int</label>
              <input class="form-control campo" type="text" name="no_int" value="<?php echo $no_int ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Colonia</label>
              <input class="form-control campo" type="text" name="colonia" value="<?php echo $colonia ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Municipio o Delegación</label>
              <input class="form-control campo" type="text" name="municipio_delegacion" value="<?php echo $municipio_delegacion ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Localidad</label>
              <input class="form-control campo" type="text" name="localidad" value="<?php echo $localidad ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Estado</label>
              <input type="hidden" readonly="" id="estado_text" value="<?php echo $estado ?>">
              <span class="span_div"></span>
            </div>
            <div class="col-md-4 form-group">
              <label>País</label>
              <select class="form-control campos idpais_u_1" name="pais" id="pais">
                <option value="MX">MEXICO</option>
                <?php if($pais!='') echo '<option value="'.$pais.'" selected>'.$pais_nombre_u.'</option>'?>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr class="subtitle">
              <h4>Acceso a sistema</h4>
            </div>
          </div>  
          <div class="row">
            <div class="col-md-6"> 
              <input type="hidden" name="UsuarioID" id="UsuarioID" value="<?php echo $UsuarioID ?>">
              <div class="row">
                <div class="col-md-11 form-group">
                  <label>Usuario</label>
                  <input class="form-control" type="text" name="Usuario" id="Usuario" oninput="verificauser()" value="<?php echo $Usuario ?>" autocomplete="nope">
                  <label class="vefi_iser" style="color: red;"></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-11 form-group">
                  <label>Contraseña</label>
                  <input class="form-control" type="password" name="contrasena" id="contrasena" oninput="verificarpass()" value="<?php echo $contrasena ?>" autocomplete="nope" min="6">
                  <label style="color: red">Estimado usuario, favor de ingresar al menos 6 caracteres.</label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-11 form-group">
                  <label>Verificar Contraseña</label>
                  <input class="form-control" type="password" id="contrasenav" oninput="verificarpass()" value="<?php echo $contrasena ?>">
                  <label class="text_validar_pass" style="color: red;"></label>
                </div>
              </div>
              <!--<div class="row">
                <div class="col-md-11 form-group">
                  <label>Perfil</label>
                  <select class="form-control" name="perfilId" id="perfilId">
                      <option selected="" disabled="" value="0">Seleccione una opción</option>
                    <?php foreach ($get_perfiles as $item) { ?>
                       <option value="<?php echo $item->perfilId ?>" <?php if($item->perfilId==$perfilId) echo 'selected' ?>><?php echo $item->perfil ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>-->
              <div class="row">
                <div class="col-md-11 form-group">
                  <label>Sucursal</label>
                  <select class="form-control" name="idsucursal" id="idsucursal">
                    <?php foreach ($get_sucursal as $item) { ?>
                      <option value="<?php echo $item->iddireccion ?>" <?php if($item->iddireccion==$idsucursal) echo 'selected' ?>><?php echo $item->alias ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <h4>Accesos a menu</h4>
              <hr>
              <div class="tabla_usuario_permiso">
                <div class="row tabla_usuario_permiso2">
                  <?php 
                  $check_md="";
                  $dis_md="disabled";
                  $check_md2="";
                  $dis_md2="";
                  foreach ($menu_get as $m){ 
                    if($m->idmenu!=5 && $m->idmenu!=6){
                      $arraywhere = array('idcliente_usuario'=>$idcliente_usuario,'id_menu'=>$m->idmenu);
                      $get_menu_sub_get=$this->ModeloCatalogos->getselectwherestatus('*','usuarios_clientes_menu',$arraywhere);
                      $aux_check_style='style="display: none"';
                      $idpermisox_aux=0;
                      $aux_check='dysplay';
                      if($m->idmenu==1){
                        $check_md="checked";
                        $dis_md="disabled";
                      }else{
                        $check_md="";
                        $dis_md="";
                      }
                      foreach ($get_menu_sub_get as $sg){
                        if($sg->check_menu==1){
                          $aux_check='checked';
                          $aux_check_style='style="display: block"';
                        }else{
                          //$aux_check='checked';
                          $aux_check='';
                          $aux_check_style='style="display: none"';
                        }
                        
                        $idpermisox_aux=$sg->idpermiso;
                      }?>
                      <script type="text/javascript">
                        setTimeout(function(){
                          previzualizar_submenu(<?php echo $m->idmenu ?>);
                        }, 1500);
                        function previzualizar_submenu(id){
                          if($('.check_menux_'+id).is(':checked')){
                            $('.sub_menu_permiso_'+id).css('display','block');
                          }else{
                            $('.sub_menu_permiso_'+id).css('display','none');
                          }
                        }

                      </script>
                      <div class="col-md-12 menu_usuario">
                        <input type="hidden" id="idpermisox" value="<?php echo $idpermisox_aux ?>">
                        <input type="hidden" id="id_menux" value="<?php echo $m->idmenu ?>">
                        <div class="form-check form-check-primary">
                          <label class="form-check-label"> <?php echo $m->nombre ?>
                            <input type="checkbox" <?php echo $check_md." ".$dis_md;?> class="form-check-input check_menux_<?php echo $m->idmenu ?> check_menux" id="check_menux" onclick="vizualizar_submenu(<?php echo $m->idmenu ?>)" <?php echo $aux_check ?>>
                          <i class="input-helper"></i></label>
                        </div>
                        <?php 
                        $get_menu_sub=$this->ModeloCatalogos->getselectwhere('menu_sub_usuario_cliente','idmenu',$m->idmenu);
                        $menu_aux=0;
                        $idmenu_aux=$m->idmenu;
                        $cont_sm=0;
                        foreach ($get_menu_sub as $x){ 
                          $menu_aux=1;
                        } 
                        if($menu_aux==1){ ?>
                          <ul class="sub_menu_permiso sub_menu_permiso_<?php echo $idmenu_aux ?>" <?php echo $aux_check_style ?>>
                            <div class="sub_menu_permiso2">
                          <?php foreach ($get_menu_sub as $me){
                            //var_dump($idcliente_usuario);
                            $arraywheresub = array('idcliente_usuario'=>$idcliente_usuario,'id_menu'=>$idmenu_aux,'id_menusub'=>$me->id);
                            $get_menu_sub_get_all=$this->ModeloCatalogos->getselectwherestatus('*','usuarios_clientes_menu_sub',$arraywheresub);
                            $aux_check_sub=''; $cont_sb_act=0;
                            $idpermisosub_aux=0;
                            $cont_sm++;
                            if($m->idmenu==2 && $me->id==1){
                              $check_md2="checked";
                              $dis_md2="disabled";
                              $ocultar='style="display:none";';
                            }else{
                              $check_md2="";
                              $dis_md2="";
                              $ocultar="";
                            }
                            foreach ($get_menu_sub_get_all as $sgs){
                              if($sgs->check_menu==1){
                                $aux_check_sub='checked';
                                $cont_sb_act++;
                              }else{
                                $aux_check_sub='';
                              }
                              $idpermisosub_aux=$sgs->idpermisosub;
                              //echo $cont_sb_act;
                            }
                            //echo $id_act;
                            if($id_act==7 && $me->id==12 || $id_act==8 && $me->id==12 || $id_act==19 && $me->id==12 || $id_act==8 && $me->id==17 || $me->id!=12){
                            ?>
                            <div class="form-check form-check-primary" <?php echo $ocultar; ?>>
                              <label class="form-check-label"> <?php echo $me->nombre ?>
                                <input type="text" id="idpermisosubx" value="<?php echo $idpermisosub_aux ?>">
                                <input type="text" id="id_menusubxx" value="<?php echo $me->id ?>">
                                <input type="text" id="id_menuxx" value="<?php echo $idmenu_aux ?>">
                                <input data-msb="<?php echo $me->id; ?>_<?php echo $idmenu_aux; ?>" type="checkbox" <?php echo $check_md2." ".$dis_md2;?> class="form-check-input sub_menu_checkx sub_menu_checkx_<?php echo $m->idmenu; ?> <?php echo $me->id; ?>_<?php echo $idmenu_aux; ?> submen_<?php echo $me->id; ?>_<?php echo $idmenu_aux; ?>" id="sub_menu_checkx" <?php echo $aux_check_sub ?> onclick="validaChecksDepen(<?php echo $me->id ?>,<?php echo $idmenu_aux; ?>,<?php if($me->id==15 )echo 1; else echo 0; ?>)">
                              <i class="input-helper"></i></label>
                            </div>
                          <?php } 
                          }?>
                            </div> 
                          </ul>
                        <?php } ?>
                      </div>
                  <?php }
                  } ?>
                </div>
              </div>
              <!--
              <div class="row">
                <div class="col-md-9">
                  <select class="form-control" id="menu_permiso">
                    <option value="1">Mis datos</option>
                    <option value="2">Administración del Sistema</option>
                    <option value="3">Bitácoras y Estadísticas</option>
                    <option value="4">Operaciones</option>
                    <option value="5">Divisas</option>
                  </select> 
                </div>
                <div class="col-md-3">
                  <button type="button" class="btn btn-primary btn-rounded btn-icon" onclick="agregar_menu()">
                    <i class="mdi mdi-plus-circle-outline"></i>
                  </button>
                </div>
              -->
              <!---  
              </div> 
              <br>
              <div class="row">
                <div class="col-md-12">
                  <table class="table" id="tabla_permisos">
                    <tbody id="menu_tbody">
                      
                    </tbody>
                  </table>
                </div>
              </div>
             
              --->  
            </div>
          </div>    
        </form>
        <button type="button" class="btn gradient_nepal2" onclick="guardar_usuario()"><i class="fa  fa-floppy-o"></i> Guardar</button>
        <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>MisUsuarios">Regresar</a>
      </div>
    </div>
  </div>
</div>  

<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_eliminar_menu" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Deseas eliminar este acceso?</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="delete_registro()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>