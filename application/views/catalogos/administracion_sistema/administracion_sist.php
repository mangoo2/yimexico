<?php 
  $perfilid=$this->session->userdata('perfilid'); 
  $idcliente_usuario=$this->session->userdata('idcliente_usuario');
  $menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,2);
  $get_act = $this->ModeloCliente->getActividadesCliente($this->session->userdata("idcliente"));

?>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <?php /*foreach ($menu as $x) { 
              if($x->id_menusub==1 && $x->check_menu==1 || $x->id_menusub==2 && $x->check_menu==1 || $x->id_menusub==4 && $x->check_menu==1
                || $x->id_menusub==11 && $x->check_menu==1 || $x->id_menusub==12 && $x->check_menu==1){
                echo '<div class="col-md-8">
                  <h3>Administración del Sistema</h3>
                </div>';
              }
            } */
          ?>
          <div class="col-md-8">
            <h3>Administración del Sistema</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>
        <hr class="subtitle">
        <div class="row" style="height: 315px !important; align-items: center;display: flex;">
          <div class="col-md-12" align="center" >
            <?php 
            if($perfilid==3){
                    // Mis datos
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_datos()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="mdi mdi-file-document btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Mis datos (Generales del sujeto obligado)</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                    // Mis datos
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_usuarios()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Mis usuarios</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                    // Mis datos
                    /*echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_hacienda()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                 <i class="mdi mdi-bank btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Alta Hacienda</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;'; */
                    // Mis datos
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_limi_config()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="mdi mdi-cash-usd btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Config. limite efectivo de anexo</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                    // Config Alertas
                    echo '<br><br><button class="btn gradient_nepal tam_btn2" onclick="href_config_alert()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="fa fa-bell btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Config. Diagnostico Alerta</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  foreach ($get_act as $ac) { 
                    if($ac->idactividad==1 || $ac->idactividad==19){ // inmuebles
                      echo '<button class="btn gradient_nepal tam_btn" id="cata_inmueble">
                        <div class="row texto_centro">
                          <div class="col-md-3" align="right">
                            <i class="mdi mdi-city btn-icon-prepend icon_yi"></i>
                          </div>
                          <div class="col-md-9" align="left">
                            <span>Catálogo de Inmuebles</span>
                          </div>
                        </div>  
                      </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                      
                    }
                    if($ac->idactividad==8){ // anexo 5b
                      echo '<button class="btn gradient_nepal tam_btn" id="cata_inmueble_5b">
                        <div class="row texto_centro">
                          <div class="col-md-3" align="right">
                            <i class="mdi mdi-city btn-icon-prepend icon_yi"></i>
                          </div>
                          <div class="col-md-9" align="left">
                            <span>Catálogo de Inmuebles</span>
                          </div>
                        </div>  
                      </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                      
                    }
                  }//foreach menu2
                    /*echo '<button class="btn gradient_nepal tam_btn" id="cata_inmueble">
                      <div class="row texto_centro">
                        <div class="col-md-3" align="right">
                          <i class="mdi mdi-city btn-icon-prepend icon_yi"></i>
                        </div>
                        <div class="col-md-9" align="left">
                          <span>Catálogo de Inmuebles</span>
                        </div>
                      </div>  
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;';*/
                  /*echo '<button class="btn gradient_nepal tam_btn" onclick="href_divisas()">
                        <div class="row texto_centro">
                          <div class="col-md-4">
                            <i class="fa fa-money icon_yi"></i>
                          </div>
                          <div class="col-md-8">
                            <span>Divisas</span>
                          </div>
                        </div>  
                      </button>&nbsp;&nbsp;&nbsp;&nbsp';*/
            }else{  
              foreach ($menu as $x) { 
                if($x->id_menusub==1){ // Mis datos
                  if($x->check_menu==1 && $perfilid==3){
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_datos()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="mdi mdi-file-document btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Mis datos (Generales del sujeto obligado)</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                if($x->id_menusub==2){ // Mis datos
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_usuarios()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Mis usuarios</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }
                /*if($x->id_menusub==3){ // Mis datos
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_hacienda()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                 <i class="mdi mdi-bank btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Alta Hacienda</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }*/
                if($x->id_menusub==4){ // Mis datos
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_mis_limi_config()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="mdi mdi-cash-usd btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Config. limite efectivo de anexo</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }if($x->id_menusub==11){ // Diagnostico de alerta
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn2" onclick="href_config_alert()">
                            <div class="row texto_centro">
                              <div class="col-md-3" align="right">
                                <i class="fa fa-bell btn-icon-prepend icon_yi"></i>
                              </div>
                              <div class="col-md-9" align="left">
                                <span>Config. Diagnostico Alerta</span>
                              </div>
                            </div>  
                          </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }if($x->id_menusub==12){ // inmuebles
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn" id="cata_inmueble">
                          <div class="row texto_centro">
                            <div class="col-md-3" align="right">
                              <i class="mdi mdi-city btn-icon-prepend icon_yi"></i>
                            </div>
                            <div class="col-md-9" align="left">
                              <span>Catálogo de Inmuebles</span>
                            </div>
                          </div>  
                        </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }if($x->id_menusub==17){ // inmuebles del 5b
                  if($x->check_menu==1){
                    echo '<button class="btn gradient_nepal tam_btn" id="cata_inmueble_5b">
                          <div class="row texto_centro">
                            <div class="col-md-3" align="right">
                              <i class="mdi mdi-city btn-icon-prepend icon_yi"></i>
                            </div>
                            <div class="col-md-9" align="left">
                              <span>Catálogo de Inmuebles</span>
                            </div>
                          </div>  
                        </button>&nbsp;&nbsp;&nbsp;&nbsp;';
                  }
                }

              }//foreach menu2
              /*$menu=$this->Login_model->getmenus_permiso_sub($idcliente_usuario,3);
              foreach ($menu as $x) { 
                if($x->id_menusub==13){ // Divisas
                  if($x->check_menu==1){ 
                    echo '<button class="btn gradient_nepal tam_btn" onclick="href_divisas()">
                          <div class="row texto_centro">
                            <div class="col-md-4">
                              <i class="fa fa-money icon_yi"></i>
                            </div>
                            <div class="col-md-8">
                              <span>Divisas</span>
                            </div>
                          </div>  
                        </button>&nbsp;&nbsp;&nbsp;&nbsp';
                  }
                }
              }//foreach menu3
              */
              
            }
            ?>  
            <?php /*
            <!-- 7-Responsable Cumplimiento --><!-- 8-Supervisor --> <!-- 6-Director General -->
            <?php if($perfilid==3 || $perfilid==6 || $perfilid==7){?>
              <button class="btn gradient_nepal tam_btn2" onclick="href_mis_datos()">
                <div class="row texto_centro">
                  <div class="col-md-3" align="right">
                    <i class="mdi mdi-file-document btn-icon-prepend icon_yi"></i>
                  </div>
                  <div class="col-md-9" align="left">
                    <span>Mis datos (Generales del sujeto obligado)</span>
                  </div>
                </div>  
              </button>&nbsp;&nbsp;&nbsp;&nbsp;
            <?php } if($perfilid==3 || $perfilid==6 || $perfilid==7 || $perfilid==8){?>  
              <button class="btn gradient_nepal tam_btn2" onclick="href_mis_usuarios()">
                <div class="row texto_centro">
                  <div class="col-md-3" align="right">
                    <i class="mdi mdi-account-multiple btn-icon-prepend icon_yi"></i>
                  </div>
                  <div class="col-md-9" align="left">
                    <span>Mis usuarios</span>
                  </div>
                </div>  
              </button>&nbsp;&nbsp;&nbsp;&nbsp;
            <?php } if($perfilid==3 || $perfilid==6 || $perfilid==7){?>
              <button class="btn gradient_nepal tam_btn2" onclick="href_mis_hacienda()">
                <div class="row texto_centro">
                  <div class="col-md-3" align="right">
                     <i class="mdi mdi-bank btn-icon-prepend icon_yi"></i>
                  </div>
                  <div class="col-md-9" align="left">
                    <span>Alta Hacienda</span>
                  </div>
                </div>  
              </button>
            <?php } ?> 
            <?php if($perfilid==3){ ?> <!-- perfil 3 = Director General -->
              <button class="btn gradient_nepal tam_btn2" onclick="href_mis_limi_config()">
                <div class="row texto_centro">
                  <div class="col-md-3" align="right">
                     <i class="mdi mdi-cash-usd btn-icon-prepend icon_yi"></i>
                  </div>
                  <div class="col-md-9" align="left">
                    <span>Config. limite efectivo de anexo</span>
                  </div>
                </div>  
              </button>
            <?php } ?> 
            */ ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>     