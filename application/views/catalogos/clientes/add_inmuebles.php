<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Agregar inmueble</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div>  
        </div> 
        <hr class="subtitle">
        <h5>Datos del inmueble objeto del arrendamiento</h5>
        <form id="form_inmueble_cli" class="forms-sample">
          <div class="row">  
            <?php if(isset($a->id)){ ?>
              <input type="hidden" name="id" id="id" value="<?php echo $a->id ?>">
            <?php } else{
              echo '<input type="hidden" name="id" id="id" value="0">';
            } ?>
            <div class="col-md-6 form-group">
              <label>Tipo de inmueble</label>
              <select class="form-control campo" name="tipo_inmueble">
                <option value="1" <?php if(isset($a) && $a->tipo_inmueble=="1"){ echo "selected"; }?>>Casa/casa en condominio</option>
                <option value="2" <?php if(isset($a) && $a->tipo_inmueble=="2"){ echo "selected"; }?>>Departamento</option>
                <option value="3" <?php if(isset($a) && $a->tipo_inmueble=="3"){ echo "selected"; }?>>Edificio habitacional</option>
                <option value="4" <?php if(isset($a) && $a->tipo_inmueble=="4"){ echo "selected"; }?>>Edificio comercial</option>
                <option value="5" <?php if(isset($a) && $a->tipo_inmueble=="5"){ echo "selected"; }?>>Edificio oficinas</option>
                <option value="6" <?php if(isset($a) && $a->tipo_inmueble=="6"){ echo "selected"; }?>>Local comercial independiente</option>
                <option value="7" <?php if(isset($a) && $a->tipo_inmueble=="7"){ echo "selected"; }?>>Local en centro comercial</option>
                <option value="8" <?php if(isset($a) && $a->tipo_inmueble=="8"){ echo "selected"; }?>>Oficina</option>
                <option value="9" <?php if(isset($a) && $a->tipo_inmueble=="9"){ echo "selected"; }?>>Bodega comercial</option>
                <option value="10" <?php if(isset($a) && $a->tipo_inmueble=="10"){ echo "selected"; }?>>Bodega industrial</option>
                <option value="11" <?php if(isset($a) && $a->tipo_inmueble=="11"){ echo "selected"; }?>>Nave Industrial</option>
                <option value="12" <?php if(isset($a) && $a->tipo_inmueble=="12"){ echo "selected"; }?>>Terreno urbano habitacional</option>
                <option value="13" <?php if(isset($a) && $a->tipo_inmueble=="13"){ echo "selected"; }?>>Terreno no urbano habitacional</option>
                <option value="14" <?php if(isset($a) && $a->tipo_inmueble=="14"){ echo "selected"; }?>>Terreno urbano comercial o industrial</option>
                <option value="15" <?php if(isset($a) && $a->tipo_inmueble=="15"){ echo "selected"; }?>>Terreno no urbano comercial o industrial</option>
                <option value="16" <?php if(isset($a) && $a->tipo_inmueble=="16"){ echo "selected"; }?>>Terreno ejidal</option>
                <option value="17" <?php if(isset($a) && $a->tipo_inmueble=="17"){ echo "selected"; }?>>Rancho/Hacienda/Quinta</option>
                <option value="18" <?php if(isset($a) && $a->tipo_inmueble=="18"){ echo "selected"; }?>>Huerta</option>
                <option value="99" <?php if(isset($a) && $a->tipo_inmueble=="99"){ echo "selected"; }?>>Otro</option>

              </select>
            </div>
            <div class="col-md-6 form-group">
              <label>Indicar: valor avalúo, valor comercial o valor catastral</label>
              <input class="form-control campo" type="text" name="valor" id="valor" <?php if(isset($a->valor)) { $monto = number_format($a->valor,2,'.',','); echo "value='$ $monto'"; } ?> >
            </div>
            <div class="col-md-2 form-group">
              <label>Código postal</label>
              <input class="form-control " pattern='\d*' maxlength="5" type="text" name="cp" id="cp" <?php if(isset($a->cp)) echo "value='$a->cp'"; ?>>
            </div>
            <div class="col-md-4 form-group">
              <label>Calle, avenida o vía de la ubicación del inmueble</label>
              <input class="form-control campo" type="text" name="calle_avenida" <?php if(isset($a->calle_avenida)) echo "value='$a->calle_avenida'"; ?>>
            </div>
            <div class="col-md-1 form-group">
              <label>No. Ext</label>
              <input class="form-control campo" type="text" name="num_ext" <?php if(isset($a->num_ext)) echo "value='$a->num_ext'"; ?>>
            </div>
            <div class="col-md-1 form-group">
              <label>No. Int</label>
              <input class="form-control campo" type="text" name="num_int" <?php if(isset($a->no_int)) echo "value='$a->no_int'"; ?>>
            </div>
            <div class="col-md-4 form-group">
              <label>Colonia de la ubicación del inmueble</label>
              <!--<input class="form-control" type="text" name="colonia" id="colonia" <?php if(isset($a->colonia)) echo "value='$a->colonia'"; ?>>-->
              <select onclick="autoComplete()" class="form-control campo" name="colonia" id="colonia">
                <option value=""></option>
                <?php if(isset($a->colonia) && $a->colonia!=""){
                  echo '<option value="'.$a->colonia.'" selected>'.$a->colonia.'</option>';      
                } ?>  
              </select>
            </div>
            <div class="col-md-6 form-group">
              <label>Folio real del inmueble o antecedentes catastrales</label>
              <div class="d-flex mb-2">
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                  <input type="text" class="form-control campo form-control-sm" name="folio_inmueble" <?php if(isset($a->folio_inmueble)) echo "value='$a->folio_inmueble'"; ?>>
                </div>
                <button type="button" class="btn btn-warning btn-rounded btn-icon" onclick="btn_folio_ayuda()">
                  <i class="mdi mdi-help"></i>
                </button>              
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn gradient_nepal2" id="btn_submit" type="submit"><i class="fa fa-save"></i> Registrar Inmueble</button>
        </div>
      </form>
    </div>
  </div>
</div>        

<div class="modal fade" id="modal_folio_ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Ayuda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Folio real del inmueble o antecedentes catastrales</h5>
       <span style="text-align : justify; font-size: 12px">
           <li>No dejar espacios en blanco, separar caracteres con guión medio o bajo.</li>
           <li>Capturar el folio real físico o electrónico del inmueble, en caso de no contar con uno, se deben capturar los datos de los antecedentes registrales, si no se cuenta con lo anterior, capturar XXXX.</li>
       </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>