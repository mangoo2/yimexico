<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 class="">Listado de inmuebles</h3>
        <hr class="subtitle">
        <p class="card-description">
          <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Clientes/addInmueble"><i class="fa fa-building"></i> Nuevo inmueble</a>
        </p>
        <div class="table-responsive">
          <table class="table" id="table_inmueble" width="100%">
            <thead>
              <tr>
                <th>#</th>
                <th>Tipo de inmueble</th>
                <th>Valor</th>
                <th>Dirección</th>
                <th>Folio o ant. catastrales</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Administracion_sistema"><i class="fa fa-reply"></i> Regresar</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Deseas eliminar este inmueble?</h5>
      </div>
      <input type="hidden" id="id_inmueble">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" id="eliminar">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
