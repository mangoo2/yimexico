<style type="text/css">
  .form-check-primary.form-check label input[type="checkbox"] + .input-helper:before, .form-check-primary.form-check label input[type="radio"] + .input-helper:before {
    border-color: #25294c;
  }
  .form-check-primary.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-primary.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #323556;
  }
</style>
<input type="hidden" name="idcliente" id="idcliente" value="<?php echo $idcliente ?>">
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-9">
          	<h3>Formato Anexo A</h3>
          </div>
          <div class="col-md-3" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="reft_alta_hacienda()"><i class="fa fa-arrow-left"></i> Regresar a Hacienda</button>
          </div> 
        </div>   
        <hr class="subtitle">
      	<div class="row">
      		<div class="col-md-12 form-group" align="right">
      			<h4>Actividad vulnerable contratada:</h4>
            <h5><?php echo $n_actividad ?></h5>
      		</div>
        </div>  
        <div class="row">
          <input type="hidden" id="tipo_persona" readonly="" value="<?php echo $tipo_persona ?>">
      		<div class="col-md-12 form-group">
      		  <?php if($tipo_persona==1){?>
                <h5>Nombre: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h5>
                <h5>R.F.C: <?php echo $rfc ?></h5>
	          <?php }else if($tipo_persona==2){?>
	            <h5>Razón Social: <?php echo $razon_social ?></h5>
	            <h5>R.F.C: <?php echo $r_c_f ?></h5>
	          <?php }else if($tipo_persona==3){?>
	            <h5>Denominación o razón social del fiduciario: <?php echo $denominacion_razon_social ?></h5>
	            <h5>R.F.C del fideicomiso: <?php echo $rfc_fideicomiso ?></h5>
	          <?php } ?>  
	        </div>
        </div>
        <!------------------>
        <hr class="subtitle">
    		<!------------------>
        <form class="form" method="post" role="form" id="form_anexoa">
          <input type="hidden" name="idanexoa" id="idanexoa" value="<?php echo $idanexoa ?>">
          <input type="hidden" name="actividad_vulnerable" value="<?php echo $id_act ?>">
          <div class="row">
            <div class="col-md-5 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de inicio de la prestación de la actividad vulnerable:</label>
              <input class="form-control campo" type="date" max="<?php echo date("Y-m-d")?>" name="fecha_vulnerable" value="<?php echo $fecha_vulnerable ?>">
            </div> 
            <div class="col-md-7 form-group">
              <label>Tipo de fedatario público:</label>
              <input class="form-control campo" type="text" name="tipo_fedatario" value="<?php echo $tipo_fedatario ?>">
            </div>
            <div class="col-md-5 form-group">
              <label>Categoría de quien realiza la actividad vulnerable:</label>
              <select class="form-control campo" name="categoria_actividad"> 
                <?php foreach ($act as $key) { ?>
                  <option value="<?php echo $key->id; ?>" <?php if($key->id==$categoria_actividad) echo 'selected' ?> ><?php echo $key->actividad; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-12">
              <hr class="subtitle">
            </div>
          </div>
          <!------------------>
          <div class="row">
            <div class="col-md-6 form-group">
              <h6>Datos del permisionario que realiza la actividad vulnerable</h6>
              <label>Modalidad de explotación del permiso correspondiente:</label>
              <input class="form-control campo" type="text" name="modalidad_explotacion" value="<?php echo $modalidad_explotacion ?>">
            </div>
            <div class="col-md-6 form-group">
              <h6>Datos del operador permisionario</h6>
              <label>Denominación o razón social:</label>
              <input class="form-control campo" type="text" name="denominacion_razon_social" value="<?php echo $denominacion_razon_social ?>">
              <br>
              <label>R.F.C:</label>
              <input class="form-control campo" type="text" name="rfc" value="<?php echo $rfc ?>">
            </div>
            <div class="col-md-12">
              <hr class="subtitle">
            </div>
          </div>
          <!------------------>
          <div class="row">
            <div class="col-md-12" align="center">
              <h6>Documento que ampara la actividad vulnerable</h6>
            </div>
            <div class="col-md-6 form-group">
              <label>Número o folio:</label>
              <input class="form-control campo" type="number" name="numero_folio" value="<?php echo $numero_folio ?>">
              <br>
              <label><i class="fa fa-calendar"></i> Fecha de emisión:</label>
              <input class="form-control campo" type="date" max="<?php echo date("Y-m-d")?>" name="fecha_emision" value="<?php echo $fecha_emision ?>">
            </div>
            <div class="col-md-6 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de inicio de periodo que ampara:</label>
              <input class="form-control campo" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_inicio_ampara" value="<?php echo $fecha_inicio_ampara ?>">
              <br>
              <label><i class="fa fa-calendar"></i> Fecha de termino de periodo que ampara:</label>
              <input class="form-control campo" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_termino_ampara" value="<?php echo $fecha_termino_ampara ?>">
            </div>
            <div class="col-md-12">
              <hr class="subtitle">
            </div>
          </div>
        </form>
        <!------------------>
        <div class="row">
          <div class="col-md-12" align="center">
            <h5>
              Domicilio por cada actividad vulnerable realizada
            </h5>
          </div>
          <div class="col-md-12">
            <div class="form-check form-check-flat form-check-primary">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input campo" id="check_direccion_a" onchange="check_direccion_a()">
                Dirección misma que en datos generales
              <i class="input-helper"></i></label>
            </div>
          </div>
        </div>  
        <!-------------->
        <div class="ptext_direccion">
          <div class="text_direccion">
          </div>
        </div>
        <!-------------->
        <div class="row">
          <div class="col-md-12" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="addomicilioanexoa()"><i class="fa fa-plus"></i> Agregar otro domicilio</button>
          </div> 
        </div>
        <div class="row">   
          <div class="col-md-12" align="center">
            <hr class="subtitle">
            <h6>Datos de contacto</h6>
          </div>
        </div>
        <form class="form" method="post" role="form" id="form_anexoa2">
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Teléfono Local:</label>
              <input class="form-control campo" placeholder="(lada) + Télefono" type="number" name="telefono" value="<?php echo $telefono ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Teléfono móvil:</label>
              <input class="form-control campo" placeholder="(lada) + Télefono" type="number" name="celular" value="<?php echo $celular ?>">
            </div>
            <div class="col-md-6 form-group">
              <label>Correo electrónico:</label>
              <input class="form-control campo" type="email" name="correo" value="<?php echo $correo ?>">
            </div>
          </div>
          <div class="row">  
            <div class="col-md-12" align="center">
              <hr class="subtitle">
              <h6>Datos del Representante de cumplimiento</h6>
            </div>
          </div>
          <div class="row">
             <div class="col-md-4 form-group">
              <label>Nombre(s):</label>
              <input class="form-control campo" type="text" name="nombre_r" value="<?php echo $nombre_r ?>">
            </div>  
            <div class="col-md-4 form-group">
              <label>Apellido paterno:</label>
              <input class="form-control campo" type="text" name="apellido_paterno_r" value="<?php echo $apellido_paterno_r ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido materno:</label>
              <input class="form-control campo" type="text" name="apellido_materno_r" value="<?php echo $apellido_materno_r ?>">
            </div>
          </div>  
          <div class="row">
            <div class="col-md-4 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
              <input class="form-control campo" type="date" max="<?php echo date("Y-m-d")?>" name="fecha_nacimiento_r" value="<?php echo $fecha_nacimiento_r ?>">
            </div>  
            <div class="col-md-4 form-group">
              <label>R.F.C:</label>
              <input class="form-control campo" type="text" name="rfc_r" value="<?php echo $rfc_r ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>CURP:</label>
              <input class="form-control campo" type="text" name="curp_r" value="<?php echo $curp_r ?>">
            </div>
          </div>
          <div class="row">  
              <div class="col-md-4 form-group">
                <label>País de nacionalidad:</label><br>
                  <select class="form-control campo idpais_a_1" name="pais_nacionalidad_r">
                     <option value="MX">MEXICO</option>
                  </select>
              </div>
              <div class="col-md-4 form-group">
                <label><i class="fa fa-calendar"></i> Fecha de designacíón:</label>
                <input class="form-control campo" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_desigacion_r" value="<?php echo $fecha_desigacion_r ?>">
              </div>  
          </div> 
        </form>  
        <!------------------>
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn gradient_nepal2" onclick="guardar_anexoa()"><i class="fa  fa-floppy-o"></i> Guardar</button>
            <button type="button" class="btn gradient_nepal2"><i class="fa fa-cloud-download"></i> Generar archivo XML (Anexo A)</button>
          </div>
        </div>
        <!------------------>
      </div>
    </div>
  </div>
</div>          
<!--- Modal banco eliminar -->
<div class="modal fade" id="modal_eliminar_a" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Seguro que deseas eliminar esta dirección?</h5>
      </div>
      <input type="hidden" id="id_direcion_a">
      <input type="hidden" id="delete_div_direcc_a">
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="eliminar_anexoa()">Aceptar</button>
        <button type="button" class="btn btn-secondary btn-rounded btn-fw" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>