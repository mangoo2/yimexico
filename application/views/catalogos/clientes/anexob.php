<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #323556;
  }
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #323556;
  }
</style>
<input type="hidden" id="idcliente" value="<?php echo $idcliente ?>">
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
       <div class="row">
          <div class="col-md-9">
           	<h3>Formato Anexo B</h3>
          </div>
          <div class="col-md-3" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="reft_alta_hacienda()"><i class="fa fa-arrow-left"></i> Regresar a Hacienda</button>
          </div> 
        </div> 
        <hr class="subtitle">

      	<div class="row">
          <div class="col-md-12 form-group" align="right">
            <h4>Actividad vulnerable contratada:</h4>
            <h5><?php echo $n_actividad ?></h5>
          </div>
        </div> 

        <div class="row">
          <input type="hidden" id="tipo_persona" readonly="" value="<?php echo $tipo_persona ?>">
      		<div class="col-md-12 form-group">
      		  <?php if($tipo_persona==1){?>
                <h5>Nombre: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h5>
                <h5>R.F.C: <?php echo $rfc ?></h5>
	          <?php }else if($tipo_persona==2){?>
	            <h5>Razón Social: <?php echo $razon_social ?></h5>
	            <h5>R.F.C: <?php echo $r_c_f ?></h5>
	          <?php }else if($tipo_persona==3){?>
	            <h5>Denominación o razón social del fiduciario: <?php echo $denominacion_razon_social ?></h5>
	            <h5>R.F.C del fideicomiso: <?php echo $rfc_fideicomiso ?></h5>
	          <?php } ?>  
	        </div>
        </div>
        <!------------------>
        <hr class="subtitle">
    		<!------------------>
        <div class="row">
          <div class="col-md-12" align="center">
            <h4>Datos del Representante de cumplimiento</h4>
          </div>
        </div>
        <!------------------>
        <form class="form" method="post" role="form" id="form_producto">
          <input type="hidden" name="idanexob" value="<?php echo $idanexob ?>">
          <input type="hidden" name="actividad_vulnerable" value="<?php echo $id_act ?>">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Nombre(s):</label>
              <input class="form-control campoB" type="text" name="nombre" value="<?php echo $nombre ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido paterno:</label>
              <input class="form-control campoB" type="text" name="apellido_paterno" value="<?php echo $apellido_paterno ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido materno:</label>
              <input class="form-control campoB" type="text" name="apellido_materno" value="<?php echo $apellido_materno ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
              <input class="form-control campoB" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_nacimiento" value="<?php echo $fecha_nacimiento ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>R.F.C:</label>
              <input class="form-control campoB" type="text" name="rcf" value="<?php echo $rcf ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>CURP:</label>
              <input class="form-control campoB" type="text" name="curp" value="<?php echo $curp ?>">
            </div>
          </div>  
          <div class="row">  
              <div class="col-md-4 form-group">
                <label>País de nacionalidad:</label><br>
                  <select class="form-control campoB idpais_p_1" name="pais_nacionalidad" id="pais_nacionalidad">
                     <option value="MX">MEXICO</option>
                  </select>
              </div>
              <div class="col-md-4 form-group">
                <label><i class="fa fa-calendar"></i> Fecha de designacíón:</label>
                <input class="form-control campoB" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_designacion" value="<?php echo $fecha_designacion ?>">
              </div>  
          </div> 
          <div class="row" align="center">
            <div class="col-md-12">
              <h6>Acepto mi designación como responsable de cumplimiento</h6>
            </div>
          </div>  
          <div class="row">  
            <div class="col-md-4 form-group"></div>
            <div class="col-md-2 form-group">
               <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input campoB" name="acepto_designacion" value="1" <?php if($acepto_designacion==1) echo 'checked' ?>>
                  SI
                </label>
               </div>
            </div>
            <div class="col-md-2 form-group"></div>
            <div class="col-md-4 form-group">
               <div class="form-check form-check-success">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input campoB" name="acepto_designacion" value="2" <?php if($acepto_designacion==2) echo 'checked' ?>>
                  NO
                </label>
               </div>
            </div>
          </div>      
          <div class="row">
            <div class="col-md-12">
              <hr class="subtitle">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Teléfono local:</label>
              <input class="form-control campoB" placeholder="(lada) + Télefono" type="text" name="telefono" value="<?php echo $telefono ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Teléfono o móvil:</label>
              <input class="form-control campoB" placeholder="(lada) + Télefono" type="text" name="celular" value="<?php echo $celular ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Correo electrónico:</label>
              <input class="form-control campoB" type="email" name="correo" value="<?php echo $correo ?>">
            </div>
          </div>
          <hr class="subtitle">
          <h4>Domicilio del responsable de cumplimiento</h4>         
          <div class="row">
              <div class="col-md-2 form-group">
                <label>Tipo de vialidad:</label>
                <select class="form-control campoB" name="tipo_vialidad">
                  <option disabled selected>Selecciona un tipo</option>
                    <?php foreach ($get_tipo_vialidad as $item){ ?>
                        <option value="<?php echo $item->id ?>" <?php if($item->id==$tipo_vialidad) echo 'selected' ?>><?php echo $item->nombre ?></option> 
                    <?php } ?>
                </select>
              </div>
              <div class="col-md-3 form-group">
                <label>Calle:</label>
                <input class="form-control campoB" type="text" name="calle" value="<?php echo $calle ?>">
              </div>
              <div class="col-md-2 form-group">
                <label>No.ext:</label>
                <input class="form-control campoB" type="text" name="no_ext" value="<?php echo $no_ext ?>">
              </div>
              <div class="col-md-2 form-group">
                <label>No.int:</label>
                <input class="form-control campoB" type="text" name="no_int" value="<?php echo $no_int ?>">
              </div>
              <div class="col-md-3 form-group">
                <label>Colonia:</label>
                <input class="form-control campoB" type="text" name="colonia" value="<?php echo $colonia ?>">
              </div>
          </div>
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Municipio o delegación:</label>
              <input class="form-control campoB" type="text" name="municipio" value="<?php echo $municipio ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Localidad:</label>
              <input class="form-control campoB" type="text" name="localidad" value="<?php echo $localidad ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Estado:</label>
              <input class="form-control campoB" type="text" name="estado" value="<?php echo $estado ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>C.P:</label>
              <input class="form-control campoB" type="text" name="cp" value="<?php echo $cp ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>País:</label>
              <select class="form-control campoB idpais_b_1" name="idpais">
                <option value="MX">MEXICO</option>
              </select>
            </div>
          </div>
        </form>  
        
        <!------------------>
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn gradient_nepal2" onclick="guardar_anexob()"><i class="fa  fa-floppy-o"></i> Guardar</button>
            <button type="button" class="btn gradient_nepal2"><i class="fa fa-cloud-download"></i> Generar archivo XML (Anexo B)</button>
          </div>
        </div>
        <!------------------>
        
      </div>
    </div>
  </div>
</div>
