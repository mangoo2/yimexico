<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 ><?php echo $titulo ?> usuario</h3>
        <hr class="subtitle">
        <!---------------->
        <form class="form" method="post" role="form" id="form_cliente">
          <input type="hidden" name="idcliente" id="idcliente" value="<?php echo $idcliente ?>">
          <div class="row">
          	<div class="col-md-4"> 
              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input campo" name="tipo_persona" id="tipo1" value="1" checked="" <?php if($tipo_persona==1) echo 'checked' ?> oninput="select_tipo_persona()">
                  Persona fisíca
                <i class="input-helper"></i></label>
              </div>

              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input campo" name="tipo_persona" id="tipo2" value="2" <?php if($tipo_persona==2) echo 'checked' ?> oninput="select_tipo_persona()">
                  Persona moral
                <i class="input-helper"></i></label>
              </div>

              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input campo" name="tipo_persona" id="tipo3" value="3" <?php if($tipo_persona==3 ) echo 'checked' ?> oninput="select_tipo_persona()">
                  Fideicomiso
                <i class="input-helper"></i></label>
              </div>
          	</div>
          	<div class="col-md-8 form-group">
              <div class="row">
                <div class="col-md-12 form-group">
                  <select class="form-control campo" id="actividad_vulnerable">
                    <?php foreach ($act as $key) { ?>
                      <option value="<?php echo $key->id; ?>"><?php echo $key->nombre; ?></option>
                    <?php } ?>
                  </select>       
                </div>
                <div class="col-md-12 tabla_act" style="display: none">
                  <table class="table table-striped" id="table_actividad">
                    <tbody id="tbody_actividad">     
                    </tbody>
                  </table>
                </div>  
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 form-group" align="right">
                  <div align="right">
                    <button type="button" class="btn gradient_nepal2" onclick="btn_add_actividad()"><i class="fa fa-plus"></i> Agregar actividad vulnerable</button>
                  </div>         
                </div>
              </div>
          	</div>
          </div>
        </form>     
        <!---------------->
        <div class="info_tipo_persona"></div>
        <!---------------->
          <hr class="subtitle">
          <h4>Contacto para Yi México</h4>
        <!---------------->
        <form class="form" method="post" role="form" id="form_contacto">  
          <input type="hidden" name="idcontacto" value="<?php echo $idcontacto ?>">
          <div class="row">  
            <div class="col-md-4 form-group">
              <label>Nombre(s):</label>
              <input class="form-control campo" type="text" name="nombre" value="<?php echo $nombre ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido paterno:</label>
              <input class="form-control campo" type="text" name="apellido_paterno" value="<?php echo $apellido_paterno ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido materno:</label>
              <input class="form-control campo" type="text" name="apellido_materno" value="<?php echo $apellido_materno ?>">
            </div>
          </div>
          <div class="row">  
            <div class="col-md-4 form-group">
              <label>Fecha de inicio de operación con Yi México:</label>
              <input class="form-control campo" type="date" max="<?php echo date("Y-m-d")?>" name="fecha_inicio" value="<?php echo $fecha_inicio ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 form-group">
              <label>Teléfono Local:</label>
              <input class="form-control campo" placeholder="(lada) + Télefono" type="number" name="telefono" value="<?php echo $telefono ?>">
            </div>
            <div class="col-md-3 form-group">
              <label>Teléfono móvil:</label>
              <input class="form-control campo" placeholder="(lada) + Télefono" type="number" name="celular" value="<?php echo $celular ?>">
            </div>
            <div class="col-md-6 form-group">
              <label>Correo electrónico:</label>
              <input class="form-control campo" type="email" name="correo" value="<?php echo $correo ?>">
            </div>
          </div>
        </form>  
        <!---------------->
          <hr class="subtitle">
          <h4>Acceso a sistema o (Admin inicial)</h4>
        <!---------------->  
        <form class="form" method="post" role="form" id="form_usuario">      
          <input type="hidden" name="UsuarioID" value="<?php echo $UsuarioID ?>">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Usuario:</label>
              <input class="form-control" type="text" name="Usuario" id="Usuario" oninput="verificauser()" value="<?php echo $Usuario ?>">
              <label class="vefi_iser" style="color: red;"></label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Contraseña:</label>
              <input class="form-control" type="password" name="contrasena" id="contrasena" oninput="verificarpass()" value="<?php echo $contrasena ?>">
              <label style="color: red">Estimado usuario, favor de ingresar al menos 6 caracteres.</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Verificar Contraseña:</label>
              <input class="form-control" type="password" id="contrasenav" oninput="verificarpass()" value="<?php echo $contrasena ?>">
              <label class="text_validar_pass" style="color: red;"></label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr class="subtitle">
            </div>
          </div>
        </form>  
          <div class="row">
            <div class="col-md-12" align="right">
              <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Clientes"><i class="fa fa-reply"></i> Regresar</a>
              <button type="button" class="btn gradient_nepal2 guardar_registro"><i class="fa  fa-floppy-o"></i> <?php echo $boton ?></button>
            </div>
          </div>
         
        <!---------------->   
      </div>
    </div>
  </div>
</div>
<!-------------------->
<!--- Modal actividad eliminar -->
<div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Seguro que deseas eliminar esta actividad?</h5>
      </div>
      <input type="hidden" id="id_activi">
      <input type="hidden" id="delete_div_acti">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>