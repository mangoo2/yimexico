<input type="hidden" id="idcliente" value="<?php echo $idcliente ?>">
<input type="hidden" id="tipo_persona" value="<?php echo $tipo_persona ?>">
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
          <?php if($tipo_persona==1){?>
              <h3>Nombre: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h3>
              <h3>R.F.C: <?php echo $rfc ?></h3>
          <?php }else if($tipo_persona==2){?>
            <h3>Razón Social: <?php echo $razon_social ?></h3>
            <h3>R.F.C: <?php echo $r_c_f ?></h3>
          <?php }else if($tipo_persona==3){?>
            <h3>Denominación o razón social del fiduciario: <?php echo $denominacion_razon_social ?></h3>
            <h3>R.F.C del fideicomiso: <?php echo $rfc_fideicomiso ?></h3>
          <?php } ?> 
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div>  
        </div>  
        <hr class="subtitle">
      	<div class="row">
      		<div class="col-md-12 form-group" align="right">
            <h4>Actividad vulnerable(s) contratada:</h4>
      			<?php
             $aux=1;  
             foreach ($get_actividad_cli as $item) { ?>
              <h5><span style="color: red"><?php echo $aux ?>.-</span> <?php echo $item->actividad ?></h5>
            <?php $aux++;} ?>
      		</div>
        </div>  

        <!------------------>
        <hr class="subtitle">
        <p class="requi">Campos obligatorios *.</p>
        <!------------------>
        <?php if($tipo_persona==1){?><!--Persona fisica -->
        <!------------------>
        <form class="form" method="post" role="form" id="form_fisica">
          <input type="hidden" name="idfisica" id="idfisica" readonly="" value="<?php echo $idfisica ?>">
  	      <div class="row">
            <!--<div class="col-md-4 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de constitución:<span class="requi">*</span></label>
              <input class="form-control" max="<?php echo date("Y-m-d")?>" type="date" name="fecha_constitucion" id="fecha_constitucionf" value="<?php echo $fecha_constitucionf ?>">
            </div>-->
            <div class="col-md-4 form-group">
              <label>País de nacimiento:<span class="requi">*</span></label>
              <select class="form-control sel idpais_p_1" name="pais_nacimiento">
                <option value="MX">MEXICO</option>
                <?php if($pais_nacimiento!='') echo '<option value="'.$pais_nacimiento.'" selected>'.$pais_nacimiento_nombre.'</option>'?>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Nacionalidad o país de origen:<span class="requi">*</span></label>
              <select class="form-control sel idpais_p_2" name="pais_nacionalidad">
                  <option value="MX">MEXICO</option>
                  <?php if($pais_nacionalidad_f!='') echo '<option value="'.$pais_nacionalidad_f.'" selected>'.$pais_nacionalidad_f_nombre.'</option>'?>
              </select>
            </div>
          </div>  
          <div class="row">
            <div class="col-md-4 form-group">
                 <label>CURP:<span class="requi">*</span></label>
                 <input class="form-control campo" type="text" name="curp" value="<?php echo $curp ?>">
            </div>
          </div>
        </form>  
        <!------------------>
  	    <?php }else if($tipo_persona==2){?><!--Persona moral -->
        <form class="form" method="post" role="form" id="form_moral">
          <input type="hidden" name="idmoral" id="idmoral" value="<?php echo $idmoral ?>">
    	    <div class="row">
            <div class="col-md-4 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de constitución:<span class="requi">*</span></label>
              <input class="form-control campo" type="date" max="<?php echo date("Y-m-d")?>" name="fecha_constitucion" id="fecha_constitucion" value="<?php echo $fecha_constitucion ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>País de nacimiento:<span class="requi">*</span></label>
              <select class="form-control sel idpais_m_1" name="pais_nacionalidad">
                  <option value="MX">MEXICO</option>
                  <?php if($pais_nacimiento_m!='') echo '<option value="'.$pais_nacimiento_m.'" selected>'.$pais_nacimiento_nombre_m.'</option>'?>
              </select>
            </div>
          </div>  
    	    <!------------------>
            <hr class="subtitle">
            <!------------------>
    	    <h4>Representante legal</h4>
          <!------------------>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Nombre(s):</label>
              <input class="form-control campo" type="text" name="nombre_l" value="<?php echo $nombre_l ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido paterno:</label>
              <input class="form-control campo" type="text" name="apellido_paterno_l" value="<?php echo $apellido_paterno_l ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido materno:</label>
              <input class="form-control campo" type="text" name="apellido_materno_l" value="<?php echo $apellido_materno_l ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
              <input class="form-control campo" type="date" max="<?php echo date("Y-m-d")?>" name="fecha_nacimiento_l" value="<?php echo $fecha_nacimiento_l ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>R.F.C:</label>
              <input class="form-control campo" type="text" name="rfc_l" value="<?php echo $rfc_l ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>CURP:</label>
              <input class="form-control campo" type="text" name="curp_l" value="<?php echo $curp_l ?>">
            </div>
          </div>
        </form>  
  	    <!------------------>
  	    <?php }else if($tipo_persona==3){?><!--Persona fideicomiso -->
        <!------------------>
        <form class="form" method="post" role="form" id="form_fideicomiso">
          <input type="hidden" name="idfideicomiso" id="idfideicomiso" value="<?php echo $idfideicomiso ?>">
    	    <div class="row">
            <div class="col-md-12 form-group">
              <label>Número, referencia o identificador del fideicomiso:</label>
              <input class="form-control campo" type="text" name="identificador_fideicomiso" id="identificador_fideicomiso" value="<?php echo $identificador_fideicomiso ?>">
            </div>
          </div>  
    	    <!------------------>
          <hr class="subtitle">
          <!------------------>
    	    <h4>Representante legal</h4>
          <!------------------>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Nombre(s):</label>
              <input class="form-control campo" type="text" name="nombre_l" value="<?php echo $nombre_lf ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido paterno:</label>
              <input class="form-control campo" type="text" name="apellido_paterno_l" value="<?php echo $apellido_paterno_lf ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Apellido materno:</label>
              <input class="form-control campo" type="text" name="apellido_materno_l" value="<?php echo $apellido_materno_lf ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label><i class="fa fa-calendar"></i> Fecha de nacimiento:</label>
              <input class="form-control campo" type="date" max="<?php echo date("Y-m-d")?>" name="fecha_nacimiento_l" id="fecha_nacimiento_l" value="<?php echo $fecha_nacimiento_lf ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>R.F.C:</label>
              <input class="form-control campo" type="text" name="rfc_l" value="<?php echo $rfc_lf ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>CURP:</label>
              <input class="form-control campo" type="text" name="curp_l" value="<?php echo $curp_lf ?>">
            </div>
          </div>
        </form>  
  	    <!------------------>
  	    <?php } ?>  
       	<!------------------>
    		<hr class="subtitle">
    		<!------------------>
        <div class="ptext_direccion">
          <div class="text_direccion">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="btn_add_sucursal()"><i class="fa fa-home"></i> Añadir sucursal</button>
          </div> 
        </div>
    		<!------------------>
    		<hr class="subtitle">
    		<!------------------>
        <form class="form" method="post" role="form" id="form_cliente">
          <input type="hidden" name="idcliente" readonly="" value="<?php echo $idcliente ?>">
      		<div class="row">
            <div class="col-md-4 form-group">
              <label>Teléfono local:</label>
              <input class="form-control campoc" placeholder="(lada) + Télefono" type="number" name="telefono" value="<?php echo $telefono ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Teléfono o móvil:</label>
              <input class="form-control campoc" placeholder="(lada) + Télefono" type="number" name="movil" value="<?php echo $movil ?>">
            </div>
            <div class="col-md-4 form-group">
              <label>Correo electrónico:</label>
              <input class="form-control campoc" type="email" name="correo" value="<?php echo $correo ?>">
            </div>
      	  </div>
          <input type="hidden" name="status" readonly="" value="2">
        </form>  
        <!------------------>
        <hr class="subtitle">
        <!------------------>
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn gradient_nepal2" onclick="guardar_registro()"><i class="fa  fa-floppy-o"></i> Guardar</button>
          </div>
        </div>
        <!------------------>
      </div>
    </div>
  </div>
</div>          
<!--- Modal banco eliminar -->
<div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Seguro que deseas eliminar esta sucursal?</h5>
      </div>
      <input type="hidden" id="id_sucursal">
      <input type="hidden" id="delete_div_sucursal">
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn btn-secondary btn-rounded btn-fw" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>