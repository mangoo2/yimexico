<style type="text/css">
  .form-check-success.form-check label input[type="checkbox"]:checked + .input-helper:before, .form-check-success.form-check label input[type="radio"]:checked + .input-helper:before {
    background: #565973;
  }
  .form-check-success.form-check label input[type="checkbox"] + .input-helper:before, .form-check-success.form-check label input[type="radio"] + .input-helper:before {
    border-color: #323556;
  }
</style>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Alta o modificación de datos en Hacienda</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div>  
        </div> 
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-12 form-group" align="right">
            <h4>Actividad vulnerable(s) contratada:</h4>
            <div class="col-md-10 form-group">
              <select class="form-control" id="actividad_vulnerable" onclick="select_actividade()">
                <option disabled="" selected="" value="0"> Seleccione una opción</option>
                <?php foreach ($get_actividad_cli as $key) { ?>
                  <option value="<?php echo $key->id; ?>"><?php echo $key->actividad; ?></option>
                <?php } ?>
              </select>       
            </div>
          </div>
        </div>  
        <div class="row">
          <input type="hidden" id="tipo_persona" readonly="" value="<?php echo $tipo_persona ?>">
          <div class="col-md-12 form-group">
            <?php if($tipo_persona==1){?>
                <h5>Nombre: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h5>
                <h5>R.F.C: <?php echo $rfc ?></h5>
            <?php }else if($tipo_persona==2){?>
              <h5>Razón Social: <?php echo $razon_social ?></h5>
              <h5>R.F.C: <?php echo $r_c_f ?></h5>
            <?php }else if($tipo_persona==3){?>
              <h5>Denominación o razón social del fiduciario: <?php echo $denominacion_razon_social ?></h5>
              <h5>R.F.C del fideicomiso: <?php echo $rfc_fideicomiso ?></h5>
            <?php } ?>  
          </div>
        </div>
        <!--- --->
        <div class="div_hacienda" style="display: none">
          <hr class="subtitle">
          <form class="form" method="post" role="form" id="form_hacienda">
            <input type="hidden" readonly="" name="idhacienda" id="idhacienda">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group"><br>
                 <label> Realizó proceso de alta en Hacienda:</label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="proceso_hacienda" id="proceso_hacienda1" value="1" onclick="guardo_hacienda()">
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="proceso_hacienda" id="proceso_hacienda2" value="2" onclick="guardo_hacienda()">
                    NO
                  </label>
                </div>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-1 form-group"></div> 
              <div class="col-md-3 form-group" align="center"><br><br>
                <label> Generar proceso alta (Anexo A):</label>
              </div>
              <div class="col-md-1 form-group"><br>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="anexo_a" id="anexo_a1" value="1" onclick="guardo_hacienda()">
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group"><br>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="anexo_a" id="anexo_a2" value="2" onclick="guardo_hacienda()">
                    NO
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <button type="button" class="btn gradient_nepal2 btn_disebled_anexoa" disabled onclick="heft_anexoa()">
                  <i class="mdi mdi-file-document btn-icon-prepend mdi-36px"></i><br>
                  <span class="d-inline-block text-left" >Formato ANEXO A
                    <small class="font-weight-light d-block"></small><br>
                  </span>
                </button>

                <button class="btn gradient_nepal2 btn_disebled_anexoa" disabled>
                  <i class="mdi mdi-cloud-upload btn-icon-prepend mdi-36px" style="color: white"></i><br>
                  <span class="d-inline-block text-left" style="color: white">Acuse de recibo Anexo A
                    <small class="font-weight-light d-block"></small><br>
                  </span>
                </button>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 form-group"></div> 
              <div class="col-md-3 form-group" align="center"><br><br>
                <label> Generar proceso alta (Anexo B):</label>
              </div>
              <div class="col-md-1 form-group"><br>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="anexo_b" id="anexo_b1" value="1" onclick="guardo_hacienda()">
                    SI
                  </label>
                </div>
              </div>
              <div class="col-md-1 form-group"><br>
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="anexo_b" id="anexo_b2" value="2" onclick="guardo_hacienda()">
                    NO
                  </label>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <button type="button" class="btn gradient_nepal2 btn_disebled_anexob" disabled onclick="heft_anexob()" href="<?php base_url() ?>Alta_hacienda/anexo_b">
                  <i class="mdi mdi-file-document btn-icon-prepend mdi-36px" style="color: white"></i><br>
                  <span class="d-inline-block text-left" style="color: white">Formato ANEXO B
                    <small class="font-weight-light d-block"></small><br>
                  </span>
                </button>
                <button class="btn gradient_nepal2 btn_disebled_anexob" disabled>
                  <i class="mdi mdi-cloud-upload btn-icon-prepend mdi-36px"></i><br>
                  <span class="d-inline-block text-left">Acuse de recibo Anexo B
                    <small class="font-weight-light d-block"></small><br>
                  </span>
                </button>
              </div>
            </div>
          </form>
        </div>
        <!--- --->

      </div>
    </div>
  </div>
</div>        