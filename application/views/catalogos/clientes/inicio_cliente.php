<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      	<h3>Nuevo usuario</h3>
        <hr class="subtitle">
      	<div class="row">
      		<div class="col-md-12 form-group" align="right">
            <h4>Actividad vulnerable(s) contratada:</h4>
            <?php
              $aux=1; 
              foreach ($get_actividad_cli as $item) { ?>
              <h5><span style="color: red"><?php echo $aux ?>.-</span> <?php echo $item->actividad ?></h5>
            <?php $aux++; } ?>
      		</div>
        </div>  
        <div class="row">
      		<div class="col-md-12 form-group">
      		<?php if($tipo_persona==1){?>
            <h5>Nombre: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h5>
          <?php }else if($tipo_persona==2){?>
            <h5>Razón Social: <?php echo $razon_social ?></h5>
          <?php }else if($tipo_persona==3){?>
            <h5>Denominación o razón social del fiduciario: <?php echo $denominacion_razon_social ?></h5>
          <?php } ?>  
          </div>
        </div>
      	<hr class="subsubtitle">
        <h4>Bienvenido, antes de comenzar a utilizar el sistema, le pedimos restablecer su contraseña de acceso.</h4>
        <form class="form" method="post" role="form" id="form_usuario">      
          <input type="hidden" name="UsuarioID" value="<?php echo $UsuarioID ?>">
          <input type="hidden" name="idcliente" id="idcliente" value="<?php echo $idcliente ?>">
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Usuario:</label>
              <input class="form-control" type="text" name="Usuario" readonly="" value="<?php echo $Usuario ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Contraseña:</label>
              <input class="form-control" type="password" name="contrasena" id="contrasena" oninput="verificarpass()">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Verificar Contraseña:</label>
              <input class="form-control" type="password" id="contrasenav" oninput="verificarpass()">
              <label class="text_validar_pass" style="color: red;"></label>
            </div>
          </div>
        </form>  
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn gradient_nepal2 guardar_registro"><i class="fa  fa-floppy-o"></i> Guardar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>          