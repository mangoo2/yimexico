<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Agregar inmueble</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div>  
        </div> 
        <hr class="subtitle">
        <h5>Características del desarrollo inmobiliario</h5>  
        <form id="form_inmueble_cli" class="forms-sample">
          <div class="row">  
            <?php if(isset($b->id)){ ?>
              <input type="hidden" name="id" id="id" value="<?php echo $b->id ?>">
            <?php } else{
              echo '<input type="hidden" name="id" id="id" value="0">';
            } ?>
            <div class="col-md-3 form-group">
              <label>Código Postal de la ubicación del desarrollo inmobiliario</label>
              <input class="form-control" pattern="\d*" maxlength="5" type="text" name="codigo_postal" id="cp" <?php if(isset($b->codigo_postal)) echo "value='$b->codigo_postal'"; ?>>
            </div>
            <div class="col-md-4 form-group">
              <label>Colonia de la ubicación del desarrollo inmobiliario</label>
              <!--<input class="form-control" type="text" name="colonia" id="colonia">-->
              <select onclick="autoComplete(this.id)" class="form-control" name="colonia" id="colonia">
                <option value=""></option>
                <?php if(isset($b->colonia) && $b->colonia!=""){
                  echo '<option value="'.$b->colonia.'" selected>'.$b->colonia.'</option>';      
                } ?>  
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label>Calle, avenida o vía de la ubicación del desarrollo inmobiliario</label>
              <input class="form-control" type="text" name="calle" <?php if(isset($b->calle)) echo "value='$b->calle'"; ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Tipo de desarrollo inmobiliario</label>
              <select class="form-control" name="tipo_desarrollo">
                <option value="1" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="1") echo "selected"; ?>>Habitacional</option>
                <option value="2" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="2") echo "selected"; ?>>Comercial</option>
                <option value="3" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="3") echo "selected"; ?>>Oficinas</option>
                <option value="4" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="4") echo "selected"; ?>>Industrial</option>
                <option value="5" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="5") echo "selected"; ?>>Mixto (Habitacional y Comercial)</option>
                <option value="99" <?php if(isset($b->tipo_desarrollo) && $b->tipo_desarrollo=="99") echo "selected"; ?>>Otro</option>
              </select>
            </div>
            <div class="col-md-6 form-group" id="cont_inm_otro">
              <label>Descripción del tipo de desarrollo inmobiliario, en caso de ser otro</label>
              <input class="form-control" type="text" name="descripcion_desarrollo" <?php if(isset($b->descripcion_desarrollo)) echo "value='$b->descripcion_desarrollo'"; ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Monto estimado total de costo de desarrollo del proyecto inmobiliario</label>
              <input class="form-control monto" type="text" id="monto_desarrollo" name="monto_desarrollo" <?php if(isset($b->monto_desarrollo)){ $monto_desarrollo = number_format($b->monto_desarrollo,2,'.',','); echo "value='$monto_desarrollo'"; } ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Número de unidades individuales a comercializar</label>
              <input class="form-control" type="text" name="unidades_comercializadas" <?php if(isset($b->unidades_comercializadas)) echo "value='$b->unidades_comercializadas'"; ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>Costo comercial promedio estimado por unidad</label>
              <input class="form-control monto" type="text" id="costo_unidad" name="costo_unidad" <?php if(isset($b->costo_unidad)) { $costo_unidad = number_format($b->costo_unidad,2,'.',','); echo "value='$costo_unidad'"; } ?>>
            </div>
            <div class="col-md-6 form-group">
              <label>¿Existe(n) otra(s) empresa(s) participando en el desarrollo de este proyecto inmobiliario?</label>
              <div class="row">
               <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="otras_empresas" value="SI" <?php if(isset($b->otras_empresas) && $b->otras_empresas=="SI") echo "checked"; ?>>
                    SI 
                  </label>
                </div>
               </div>
               <div class="col-md-6 form-group">
                <div class="form-check form-check-success">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="otras_empresas" value="NO" <?php if(isset($b->otras_empresas) && $b->otras_empresas=="NO") echo "checked"; ?>>
                   NO
                  </label>
                </div>
               </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn gradient_nepal2" id="btn_submit" type="submit"><i class="fa fa-save"></i> Registrar Inmueble</button>
        </div>
      </form>
    </div>
  </div>
</div>        
