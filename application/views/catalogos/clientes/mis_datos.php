<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <h3>Mis datos</h3>
          </div>
          <div class="col-md-4" align="right">
            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Inicio_cliente"> <i class="fa fa-arrow-left"></i> Regresar </a>
            <button type="button" class="btn gradient_nepal2" onclick="inicio_cliente()"><i class="fa fa-home"></i></button>
          </div> 
        </div>
        <hr class="subtitle">
        <div class="row">
          <div class="col-md-12">
            <?php if($tipo_persona==1){?>
              <h5 style="color: black">Nombre: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h5>
              <h5 style="color: black">R.F.C: <?php echo $rfc ?></h5>
            <?php }else if($tipo_persona==2){?>
              <h5 style="color: black">Razón Social: <?php echo $razon_social ?></h5>
              <h5 style="color: black">R.F.C: <?php echo $r_c_f ?></h5>
            <?php }else if($tipo_persona==3){?>
              <h5 style="color: black">Denominación o razón social del fiduciario: <?php echo $denominacion_razon_social ?></h5>
              <h5 style="color: black">R.F.C del fideicomiso: <?php echo $rfc_fideicomiso ?></h5>
            <?php } ?> 
          </div>  
        </div>
        <div class="row">
          <div class="col-md-12">
            <h5 style="color: black">Actividad vulnerable(s) contratada:</h5>
            <?php
             $aux=1;  
             foreach ($get_actividad_cli as $item) { ?>
              <h5 style="color: black"><span style="color: red"><?php echo $aux ?>.-</span> <?php echo $item->actividad ?></h5>
            <?php $aux++;} ?>
          </div>  
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr class="subtitle">
            <h4>Acceso a sistema</h4>
          </div>
        </div>  
        <form class="form" method="post" role="form" id="form_usuario"> 
          <input class="form-control" type="hidden" name="UsuarioID" value="<?php echo $get_u_u->UsuarioID ?>">  
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Usuario</label>
              <input class="form-control" type="text" name="Usuario" readonly="" value="<?php echo $get_u_u->Usuario ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Contraseña</label>
              <input class="form-control" type="password" name="contrasena" id="contrasena" oninput="verificarpass()">
              <label style="color: red">Estimado usuario, favor de ingresar al menos 6 caracteres.</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 form-group">
              <label>Verificar Contraseña</label>
              <input class="form-control" type="password" id="contrasenav" oninput="verificarpass()">
              <label class="text_validar_pass" style="color: red;"></label>
            </div>
          </div>
        </form>  
          <button type="button" class="btn gradient_nepal2 guardar_registro"><i class="fa  fa-floppy-o"></i> Guardar</button>
      </div>
    </div>
  </div>
</div>