<style type="text/css">
  .btn-outline-secondary{
    border-radius: 10px !important;
  }
</style>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h3 class="">Listado de usuarios</h3>
        <hr class="subtitle">
        <p class="card-description">
          <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Clientes/add"><i class="fa fa-user-plus"></i> Nuevo cliente Yi México</a>
        </p>
        <div class="table-responsive">
          <table class="table" id="table_cliente">
            <thead>
              <tr>
                <th>#</th>
                <th>Cliente</th>
                <th>Actividad vulnerable</th>
                <th>Fecha de alta en el sistema</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!--- Modal cliente eliminar -->
<div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <h5>¿Deseas eliminar este cliente?</h5>
      </div>
      <input type="hidden" id="idcliente">
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" onclick="eliminar()">Aceptar</button>
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--- Modal cliente actividad -->
<div class="modal fade" id="modal_actividad" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Listado de actividades vulnerables</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 form-group">
              <div class="tabla_actividad"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

