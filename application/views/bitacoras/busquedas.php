<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
              <h3 class="">Bitácora de Búsquedas (listas pagadas)</h3>
          </div>
          <div class="col-md-4" align="right">
            <button type="button" class="btn gradient_nepal2" onclick="regresar_view()"><i class="fa fa-arrow-left"></i> Regresar</button>
          </div>  
        </div> 
        <hr class="subtitle">
        <div class="row col-lg-12">
          <div class="col-lg-6">
            <label>Cliente:</label>
            <select class="form-control" id="id_cliente">
            </select>
          </div>
          <div class="col-md-6" align="right">
            <button class="btn gradient_nepal2" id="exportar" ><i class="fa fa-download"></i> <i class="fa fa-file-excel-o"></i> Bitácora</button>
          </div>
        </div>
        <div class="col-lg-12"><br></div>
        <div class="table-responsive">
          <table class="table" id="table_bita">
            <thead>
              <tr>
                <th>#</th>
                <th>Operación</th>
                <th>Fecha consulta</th>
                <th>Tipo de Búsqueda</th>
                <th>Persona Búscada</th>
                <th>Resultado</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
