<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=busqueda_clientes".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>#</th>
          <th>Operación</th>
          <th>Fecha consulta</th>
          <th>Tipo de Búsqueda</th>
          <th>Persona Búscada</th>
          <th>Resultado</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
        $tipo=""; $resultado="";
        foreach ($b as $bb) {
            if($bb->id_bene>0){
                $tipo="Búsqueda de DB";
                $name=$bb->beneficiario;
            }
            else{
                $tipo="Búsqueda de Cliente";
                $name=$bb->cliente;
            }

            if($bb->id_bene==0 && $bb->resultado!=""){
                $resultado = $bb->resultado;
            }else if($bb->id_bene>0 && $bb->resultado_bene!=""){
                $resultado = $bb->resultado_bene;
            }

        	echo '
            <tr>
                <td >'.$bb->id.'</td>
                <td >'.$bb->folio_cliente.'</td>
                <td >'.$bb->fecha_consulta.'</td>
                <td >'.$tipo.'</td>
                <td >'.$name.'</td>
                <td >'.$resultado.'</td>';
        }
        echo '</tr>';
      ?>
        
    </tbody>
</table>