<a class="btn" style="color: #212b4c; font-size: 22px;" title="Cancelar" onclick="modaleliminar(39)"><i class="fa fa-trash" style="font-size: 22px;"></i></a>

ALTER TABLE `anexo12a_datos_fideicomitente` ADD `datos_tipo_patrimoniofi` INT(1) NOT NULL AFTER `identificador_fideicomisof`, ADD `monedafi` INT(5) NOT NULL AFTER `datos_tipo_patrimoniofi`, ADD `monto_operacionfi` DECIMAL(17,2) NOT NULL AFTER `monedafi`, ADD `tipo_inmueblefi` INT(5) NOT NULL AFTER `monto_operacionfi`, ADD `codigo_postalfi` INT(10) NOT NULL AFTER `tipo_inmueblefi`, ADD `folio_realfi` VARCHAR(220) NOT NULL AFTER `codigo_postalfi`, ADD `importe_garantiafi` DECIMAL(17,2) NOT NULL AFTER `folio_realfi`, ADD `descripcionfi` TEXT NOT NULL AFTER `importe_garantiafi`, ADD `valor_bienfi` DECIMAL(17,2) NOT NULL AFTER `descripcionfi`;

ALTER TABLE `anexo12a_datos_fideicomitente_aviso` ADD `datos_tipo_patrimoniofi` INT(1) NOT NULL AFTER `identificador_fideicomisof`, ADD `monedafi` INT(5) NOT NULL AFTER `datos_tipo_patrimoniofi`, ADD `monto_operacionfi` DECIMAL(17,2) NOT NULL AFTER `monedafi`, ADD `tipo_inmueblefi` INT(5) NOT NULL AFTER `monto_operacionfi`, ADD `codigo_postalfi` INT(10) NOT NULL AFTER `tipo_inmueblefi`, ADD `folio_realfi` VARCHAR(220) NOT NULL AFTER `codigo_postalfi`, ADD `importe_garantiafi` DECIMAL(17,2) NOT NULL AFTER `folio_realfi`, ADD `descripcionfi` TEXT NOT NULL AFTER `importe_garantiafi`, ADD `valor_bienfi` DECIMAL(17,2) NOT NULL AFTER `descripcionfi`;

http://localhost/yimexico/Transaccion/anexo11/0/21/14/42/0/259/160
3.7.1.2.1 - Compraventa de Inmuebles
3.7.1.2.2 - Cesión de Derechos sobre Inmuebles
3.7.1.2.3 - Administración y manejo de recursos, valores, cuentas bancarias, ahorro o valores o cualquier otro activo 
3.7.1.2.4 - Constitución de personas morales (incluidas las sociedades mercantiles)
3.7.1.2.5 - Organización de aportaciones de capital o cualquier otro tipo de recursos para la constitución, operacion y administración de sociedades mercantiles
3.7.1.2.6 - Fusión
3.7.1.2.7 - Escisión
3.7.1.2.8 - Operación y administración de personas morales y vehículos corporativos
3.7.1.2.9 - Constitución de Fideicomiso
3.7.1.2.10 - Compra o venta de entidades mercantiles


http://localhost/yimexico/Operaciones/asignarBene/24/208/277/1/13
http://localhost/yimexico/Clientes_c_beneficiario/beneficiariosUnion/13

http://localhost/yimexico/Operaciones/existente/28

18/05/2021

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `activar`, `orden`) VALUES (NULL, '4', 'Riesgos', 'Paises', 'fa fa-exclamation-triangle', '', '3');


INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '17');


ALTER TABLE `paises_indice_basilea` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_indice_secrecia_bancaria` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_indice_corrupcion` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_indice_gafi` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_incidencia_delictiva` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_percepcion_corrupcion_estatal_zonas_urbanas` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_guia_terrorismo` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_indice_global_terrorismo` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_indice_corrupcion_global` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;
ALTER TABLE `paises_regimenes_fiscales_preferentes` ADD `activo` INT(1) NOT NULL DEFAULT '1' AFTER `riesgo`;


21/05/2021
ALTER TABLE `formulario_pep` CHANGE `codigo_postal` `codigo_postal` VARCHAR(100) NOT NULL;