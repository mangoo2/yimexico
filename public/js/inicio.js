$(document).ready(function() {
   $('#inline-datepicker-example').datepicker({
        enableOnReadonly: true,
        todayHighlight: true,
        templates: {
          leftArrow: '<i class="mdi mdi-chevron-left"></i>',
          rightArrow: '<i class="mdi mdi-chevron-right"></i>'
        }
    });	


    var areaData = {
	    labels: ["15", "16", "17", "18", "19"],
	    datasets: [{
	      label: 'Avisos generados de marzo',
	      data: [12, 19, 3, 5, 2, 3],
	      backgroundColor: [
	        'rgba(255, 99, 132, 0.2)',
	        'rgba(54, 162, 235, 0.2)',
	        'rgba(255, 206, 86, 0.2)',
	        'rgba(75, 192, 192, 0.2)',
	        'rgba(153, 102, 255, 0.2)',
	        'rgba(255, 159, 64, 0.2)'
	      ],
	      borderColor: [
	        'rgba(255,99,132,1)',
	        'rgba(54, 162, 235, 1)',
	        'rgba(255, 206, 86, 1)',
	        'rgba(75, 192, 192, 1)',
	        'rgba(153, 102, 255, 1)',
	        'rgba(255, 159, 64, 1)'
	      ],
	      borderWidth: 1,
	      fill: true, // 3: no fill
	    }]
	};
	var areaOptions = {
	    plugins: {
	      filler: {
	        propagate: true
	      }
	    }
	}
    if ($("#areaChart").length) {
	    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
	    var areaChart = new Chart(areaChartCanvas, {
	      type: 'line',
	      data: areaData,
	      options: areaOptions
	    });
	}
});