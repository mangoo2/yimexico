var base_url=$('#base_url').val();
$(document).ready(function($) {
	generarcambio();
});
function generarcambio(){
	$.ajax({
        type:'POST',
        url: base_url+'Inicio/generarcambio',
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
        	//console.log(data);
        	var array = $.parseJSON(data);
        	$.each(array, function(index, item) {
        		//console.log(item.fecha+' '+item.fecha_actual);
        		var fecha=item.fecha;
        		var fecha_actual= item.fecha_actual;
        		if (fecha<fecha_actual) {
        			//console.log('menor');
        			consultacambio(item.moneda,item.clave,item.fecha_actual);
        		}
        	});
        }
    });
}
function consultacambio(moneda,tipocambio,fecha){
	var token='cd71b55dcd341486a9b38a45bc03c1c2daf1ad32036c24d900b2c9493b1e5cd3';
	var urlws ='https://www.banxico.org.mx/SieAPIRest/service/v1/series/'+tipocambio+'/datos/oportuno?token='+token;
	$.ajax({
		url : urlws,
		jsonp : "callback",
		dataType : "jsonp", 
		success : function(response) { 
			console.log(response);
			var series=response.bmx.series;
			console.log(series);
			for (var i in series) {
				  var serie=series[i];
				  var reg=serie.datos[0].dato;
				  var valor = parseFloat(reg);
				  if(valor>0){
				  	insertavalor(moneda,reg,fecha);	
				  }
				  
			}
		}
	});	
}
function insertavalor(moneda,reg,fecha){
	$.ajax({
        type:'POST',
        url: base_url+'Inicio/insercambiodivisa',
        data: {
        	moneda:moneda,
        	valor:reg,
        	fecha:fecha
        },
        statusCode:{
            404: function(data){
                //swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                //swal("Error!", "500", "error");
            }
        },
        success:function(data){
        	
        }
    });
}