var Table;
$(document).ready(function($){
    Table=$("#table").DataTable();
    GetTable();
	//loadTable();
});

$("#Filtrar").change(function(event){
    GetTable();
});

function GetTable(){
    var valor=$("#Filtrar").val();
    if (valor==2){
        $("#th1").text('Liga de idetificación');
        $("#th2").text('Lista');
        loadTableD();
    }else{
        $("#th1").text('RFC');
        $("#th2").text('Dirección');
        loadTableP()
    }
}

function loadTableD(){
	$("#table").DataTable({
    "bProcessing": true,
    "destroy": true,
    "serverSide": true,
    "ajax": {
       "url": base_url+"ListaNegra/GetListado",
       type: "post",
       "data": function(d){
            d.fil = $("#Filtrar").val()
        },
        error: function(){
           $("#table").css("display","none");
        }
    },
    "columns": [
        {"data": "id"},
        {"data": "nombre"},
        {"data": "ligaindentificacion"},
        {"data": "listaecuentra"},
        {"data": "razonlista",
            "render": function ( data, type, row, meta ) {
                var cm = "";
                switch (data){
                    case '1':
                        cm="PEP";
                    break;
                    case '2':
                        cm="Delicuencial";
                    break;
                    default:
                    break;
                }
                return cm;
            }
        },
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var cm = "'";
            var html='<div class="btn-group"> <a class="btn" style="color: #212b4c;" title="Editar" href="'+base_url+'ListaNegra/Alta/'+row.id+'"><i class="fa fa-edit" style="font-size: 22px;"></i></a>\
            <a class="btn" style="color: #212b4c;" onclick="Delete('+row.id+')" href="#"><i class="fa fa-trash" style="font-size: 22px;"></i></a></div>';
            return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}

function loadTableP(){
    $("#table").DataTable({
    "bProcessing": true,
    "destroy": true,
    "serverSide": true,
    "ajax": {
       "url": base_url+"ListaNegra/GetListado",
       type: "post",
       "data": function(d){
            d.fil = $("#Filtrar").val()
        },
    },
    "columns": [
        {"data": "id"},
        {"data": null,
            "render": function ( data, type, row, meta ){
                return row.nombre+" "+row.apellido_p+" "+row.apellido_m;
            }
        },
        {"data": "rfc"},
        {"data": "direccion"},
        {"data": "razonlista",
            "render": function ( data, type, row, meta ) {
                var cm = "";
                switch (data){
                    case '1':
                        cm="PEP";
                    break;
                    case '2':
                        cm="Delicuencial";
                    break;
                    default:
                    break;
                }
                return cm;
            }
        },
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var cm = "'";
            var html='<div class="btn-group"> <a class="btn" style="color: #212b4c;" title="Editar" href="'+base_url+'ListaNegra/Alta/'+row.id+'"><i class="fa fa-edit" style="font-size: 22px;"></i></a>\
            <a class="btn" style="color: #212b4c;" onclick="Delete('+row.id+')" href="#"><i class="fa fa-trash" style="font-size: 22px;"></i></a> </div>';
            return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}