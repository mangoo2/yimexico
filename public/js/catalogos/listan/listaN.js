contA=0;
contD=0;
contF=0;
$(document).ready(function($){
	$("#direc").hide();
  VerrifUodate();
  GetInterface();
  GetDataByCp();
});

function VerrifUodate(){
  id=$("#id_lista").val();
  if (id!=0){
    GetAliases(id);
    var CB = document.getElementById('entidad').checked;
    if(CB){
      GetDirections(id);
    }else{
      GetFechas(id);
    }
  }
}

$("#cpv").blur(function(event){
  GetDataByCp();
});

function GetDataByCp(){
  cp=$("#cpv").val();
  $.ajax({
    type: 'POST',
    url : base_url+'ListaNegra/GetDataDir',
    async: false,
    data: {
      'cp':cp,
    },
    success: function(res){
      var info=JSON.parse(res);
      console.log(info['arrdata']);
      FullColonias(info['arrdata']);
      SetGenData(info['estate']);
    }
  });
}

function FullColonias(dta){
  complete="";
  var colval=$("#ColoniPL").val();
  $.each(dta, function (key, dato){
    sel="";
    if(dato.colonia==colval){
      sel="selected";
    }
    complete=complete+'<option '+sel+' value="'+dato.colonia+'">'+dato.colonia+'</option>';
  });
  $("#colonia").append(complete);
}

function SetGenData(dta){
  $("#estado").val(dta.estado);
  $("#mun").val(dta.muni);
  $("#loc").val(dta.ciu);
}

$("#razonlista").change(function(event) {
  GetInterface();  
});

$("#rfc").blur(function(){
  let ex=/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;
  valor=$("#rfc").val();
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      //console.log("valido");
    }else{
      swal("¡Error!", "RFC No valido", "error");
    }
  }
});

function GetInterface(){
  rl=$("#razonlista").val();
  if(rl==1){
    $("#pep").show();
    $("#nomp").text('Nombre');
    $("#delicuencial").hide();
    $("#fechanac").hide();
    $("#direc").hide();
    $("#alias").hide();
  }else{
    $("#nomp").text('Nombre o Entidad');
    $("#pep").hide();
    $("#delicuencial").show();
    $("#fechanac").show();
    $("#alias").show();
  }
}

$("#entidad").click(function(event){
	var CB = document.getElementById('entidad').checked;
	if(CB){
	  console.log('esta seleccionado');
	  $("#direc").show(); 
	  $("#fechanac").hide();
	}else{
		console.log('no esta seleccionado');
		$("#direc").hide();
		$("#fechanac").show();
	}
});

function NuevoAlias(id,alias){
  contA++;
	var element='<form id="form-alias-'+contA+'">\
              <div class="row" id="aliases">\
                <div class="col-md-10 form-group">\
                  <input type="hidden" name="id" id="id" value="'+id+'">\
                	<label>Alias</label>\
               		<input class="form-control" value="'+alias+'" type="text" name="alias" id="alias">\
                </div>\
                <div class="col-md-1">\
                  <br>\
                  <button type="button" class="btn gradient_nepal2" onclick="DeleteAlias('+contA+')"><i class="fa fa-minus"></i></button>\
                </div>\
              </div>\
            </form>';
  $("#alias").append(element);
}

function NuevaDireccion(id,d){
  contD++;
	element='<form id="directions-'+contD+'">\
              <div class="row" id="directions">\
                <div class="col-md-10 form-group">\
                  <label>Dirección</label>\
                  <input type="hidden" name="id" value="'+id+'">\
                  <textarea class="form-control" type="text" name="direccion">'+d+'</textarea>\
                </div>\
                <div class="col-md-1">\
                  <br>\
                  <button type="button" class="btn gradient_nepal2" onclick="DeleteDirecion('+contD+')"><i class="fa fa-minus"></i></button>\
                </div>\
              </div>\
            </form>';
  $("#direc").append(element);
}

function NuevoNacimiento(id,d,m,a,e){
  contF++;
  var cb="";
  sel1=""; sel2=""; sel3=""; sel4=""; sel5=""; sel6=""; sel7=""; sel8=""; sel9=""; sel10=""; sel11=""; sel12="";
  switch (m){
    case "Enero":
      sel1="selected";
    break;
    case "Febrero":
      sel2="selected";
    break;
    case "Marzo":
      sel3="selected";
    break;
    case "Abril":
      sel4="selected";
    break;
    case "Mayo":
      sel5="selected";
    break;
    case "Junio":
      sel6="selected";
    break;
    case "Julio":
      sel7="selected";
    break;
    case "Agosto":
      sel8="selected";
    break;
    case "Septiembre":
      sel9="selected";
    break;
    case "Octubre":
      sel10="selected";
    break;
    case "Noviembre":
      sel11="selected";
    break;
    case "Diciembre":
      sel12="selected";
    break;
    default:
      // statements_def
      break;
  }
  if (e==1){
    cb="checked";
  }

	element='<form id="nacimiento-'+contF+'">\
              <div class="row">\
                <div class="col-md-2 form-group">\
                  <label>Día</label>\
                  <input type="hidden" name="id" value="'+id+'">\
                  <input class="form-control" type="number" name="dia" id="dia" value="'+d+'">\
                </div>\
                <div class="form-group col-md-3">\
                  <label>Mes</label>\
                  <select class="form-control" name="mes" id="mes">\
                    <option '+sel1+' value="Enero">Enero</option>\
                    <option '+sel2+' value="Febrero">Febrero</option>\
                    <option '+sel3+' value="Marzo">Marzo</option>\
                    <option '+sel4+' value="Abril">Abril</option>\
                    <option '+sel5+' value="Mayo">Mayo</option>\
                    <option '+sel6+' value="Junio">Junio</option>\
                    <option '+sel7+' value="Julio">Julio</option>\
                    <option '+sel8+' value="Agosto">Agosto</option>\
                    <option '+sel9+' value="Septiembre">Septiembre</option>\
                    <option '+sel10+' value="Octubre">Octubre</option>\
                    <option '+sel11+' value="Noviembre">Noviembre</option>\
                    <option '+sel12+' value="Diciembre">Diciembre</option>\
                  </select>\
                </div>\
                <div class="col-md-2 form-group">\
                  <label>Año</label>\
                  <input class="form-control" type="number" name="anio" id="anio" value="'+a+'">\
                </div>\
                <div class="col-md-2">\
                  <br>\
                    <label class="form-check-label">\
                    <input type="checkbox" class="form-check-input" '+cb+' id="exacta" name="exacta">\
                     Exacta\
                    </label>\
                </div>\
                <div class="col-md-2">\
                  <br>\
                  <button type="button" class="btn gradient_nepal2" onclick="DeleteFecha('+contF+')"><i class="fa fa-minus"></i></button>\
                </div>\
              </div>\
            </form>';
   $("#fechanac").append(element);
}

function DeleteAlias(T){
  ELE=$("#form-alias-"+T);
  id=ELE[0][0].value;
  if(id!=0){
    DeleteBase(1,id)
    console.log('borrar de base: '+id+"--"+T);
  }
  $("#form-alias-"+T).remove();
  if (contA>T){
    console.log("Clon es mayor");
    for (var i = T+1; i <= contA; i++){
      nnum=i-1;
      Ne=$("#form-alias-"+i);
      Ne[0].removeAttribute('id');
      Ne[0].setAttribute('id', "form-alias-"+nnum);
      Ne[0][2].removeAttribute('onclick');
      Ne[0][2].setAttribute('onclick', "DeleteAlias("+nnum+")");
    }
  }
  contA=contA-1;
  //swal("Éxito", "Pago eliminado correctamente", "success");
}

function DeleteFecha(T){
  ELE=$("#nacimiento-"+T);
  id=ELE[0][0].value;
  if(id!=""){
    DeleteBase(3,id);
    console.log('borrar de base: '+id+"--"+T);
  }
  $("#nacimiento-"+T).remove();
  if (contF>T){
    console.log("Clon es mayor");
    for (var i = T+1; i <= contF; i++){
      nnum=i-1;
      Ne=$("#nacimiento-"+i);
      Ne[0].removeAttribute('id');
      Ne[0].setAttribute('id', "nacimiento-"+nnum);
      Ne[0][5].removeAttribute('onclick');
      Ne[0][5].setAttribute('onclick', "DeleteFecha("+nnum+")");
    }
  }
  contF=contF-1;
}

function DeleteDirecion(T){
  ELE=$("#directions-"+T);
  id=ELE[0][0].value;
  if(id!=""){
    DeleteBase(2,id)
    console.log('borrar de base: '+id+"--"+T);
  }
  $("#directions-"+T).remove();
  if (contD>T){
    console.log("Clon es mayor");
    for (var i = T+1; i <= contD; i++){
      nnum=i-1;
      Ne=$("#directions-"+i);
      Ne[0].removeAttribute('id');
      Ne[0].setAttribute('id', "directions-"+nnum);
      Ne[0][2].removeAttribute('onclick');
      Ne[0][2].setAttribute('onclick', "DeleteDirecion("+nnum+")");
    }
  }
  contD=contD-1;
}

function registrar(){
	info=$("#maleantes").serialize();
	$.ajax({
    type: 'POST',
    url : base_url+'ListaNegra/saveln',
    async: false,
    data: info,
    success: function(res){
    	var CB = document.getElementById('entidad').checked;
			if (CB){
				console.log('es entidad');
				form="directions-";
				contador=contD;
				tabla=2;
			}else{
				console.log('no es entidad');
				form="nacimiento-";
				contador=contF;
				tabla=3;
			}
    	SaveOther(res,1,'form-alias-',contA);
    	SaveOther(res,tabla,form,contador);
      SuccesMesage();
    }
  });
}

function SaveOther(id,table,F,cont){
	info=getDataform(F,cont);
	$.ajax({
    type: 'POST',
    url : base_url+'ListaNegra/saveOther',
    async: false,
    data:{
    	"id_ln":id,
    	'data':info,
    	'tabla': table,
    },
    success: function(res){

    }
  });
}

function getDataform(F,cont){
	ArrayData1=[];
	for (var i = 0;i<=cont;i++){
		dta=$("#"+F+i).serializeArray();
    console.log(ValidVacio(F,i));
    if (ValidVacio(F,i)){
      ArrayData1.push(dta);
    }
	}
	jsnarr=JSON.stringify(ArrayData1);
	return ArrayData1;
}

function ValidVacio(F,i){
  Ele=$("#"+F+i)[0];
  contv=0;
  ret=true;
  num=Ele.length;
  for (var j = 0; j < num; j++){
    if(Ele[j].value==""){
      contv++
    }
  }
  if(contv>=2){
    ret=false;
  }
  return ret;
}

function GetAliases(id){
  $.ajax({
    type: 'POST',
    url : base_url+'ListaNegra/GetInfoOther',
    async: false,
    data:{
      "id_ln":id,
      "tabla": 1,
    },
    success: function(result){
      var data= JSON.parse(result);
      $.each(data, function (key, dato){
        NuevoAlias(dato.id,dato.alias);
     });
    }
  });
}

function GetFechas(id){
  $.ajax({
    type: 'POST',
    url : base_url+'ListaNegra/GetInfoOther',
    async: false,
    data:{
      "id_ln":id,
      "tabla": 3,
    },
    success: function(result){
      var data= JSON.parse(result);
      $.each(data, function (key, dato){
        NuevoNacimiento(dato.id,dato.dia,dato.mes,dato.anio,dato.exacta);
     });
    }
  });
}

function GetDirections(id){
  $.ajax({
    type: 'POST',
    url : base_url+'ListaNegra/GetInfoOther',
    async: false,
    data:{
      "id_ln":id,
      "tabla": 3,
    },
    success: function(result){
      var data= JSON.parse(result);
      $.each(data, function (key, dato){
        NuevaDireccion(dato.id,dato.direccion);
     });
    }
  });
}

function DeleteBase(t,id){
  $.ajax({
    type: 'POST',
    url : base_url+'ListaNegra/DeleteElement',
    async: false,
    data:{
      "id":id,
      "tabla": t,
    },
    success: function(result){
    }
  });
}

function SuccesMesage(){
  swal({
      title: "Éxito",
      text: "¡Se guardó correctamente!",
      type: "success",
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Aceptar",
      closeOnConfirm: true
  }, function (isConfirm) {
    if (isConfirm){
      location.href =base_url+"ListaNegra";
    }
  });
}