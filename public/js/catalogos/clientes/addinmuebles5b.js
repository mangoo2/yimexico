$(function () {
    var cont=0;
    $("#monto_desarrollo").on('change',function(){
        if(cont>0){
            //totalmonto = replaceAll($("#valor").val(), ",", "" );
            //totalmonto = replaceAll(totalmonto, "$", "" );
            var comp = $('#monto_desarrollo').val().indexOf("$");
            if(comp==0){
              tot1 = comp.replace($('#monto_desarrollo').val(), ",", "" );
              tot1 = comp.replace(totot1, "$", "" );
              var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#monto_desarrollo').val());
              $('#monto_desarrollo').val(totalg); 
            }else{
              var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#monto_desarrollo').val());
              $('#monto_desarrollo').val(totalg); 
            }
        }else{
            var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($("#monto_desarrollo").val());
            $("#monto_desarrollo").val(monto);    
        }
        cont++;
        //console.log("cont: "+cont);
    });

    var cont2=0;
    $("#costo_unidad").on('change',function(){
        if(cont2>0){
            //totalmonto = replaceAll($("#valor").val(), ",", "" );
            //totalmonto = replaceAll(totalmonto, "$", "" );
            var comp = $('#costo_unidad').val().indexOf("$");
            if(comp==0){
              tot1 = comp.replace($('#costo_unidad').val(), ",", "" );
              tot1 = comp.replace(totot1, "$", "" );
              var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#costo_unidad').val());
              $('#costo_unidad').val(totalg); 
            }else{
              var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#costo_unidad').val());
              $('#costo_unidad').val(totalg); 
            }
        }else{
            var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($("#costo_unidad").val());
            $("#costo_unidad").val(monto);    
        }
        cont2++;
    });

    $('#form_inmueble_cli').validate({
        rules: {
            codigo_postal: "required",
            colonia: "required",
            calle: "required",
            tipo_desarrollo: "required",
            descripcion_desarrollo: "required",
            monto_desarrollo: "required",
            unidades_comercializadas: "required",
            costo_unidad: "required",
            otras_empresas: "required"
        },
        messages: {
            codigo_postal: "Campo obligatorio",
            colonia: "Campo obligatorio",
            calle: "Campo obligatorio",
            tipo_desarrollo: "Campo obligatorio",
            descripcion_desarrollo: "Campo obligatorio",
            monto_desarrollo: "Campo obligatorio",
            unidades_comercializadas: "Campo obligatorio",
            costo_unidad: "Campo obligatorio",
            otras_empresas: "Campo obligatorio"
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: base_url+"Clientes/submitInmueble5b",
                data: $(form).serialize(),
                beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                },
                success: function (result) {
                    //console.log(result);
                    $("#btn_submit").attr("disabled",false);
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    setTimeout(function () { window.location.href = base_url+"Clientes/catalogoInmuebles5b" }, 1500);
                }
            });
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });
    $(".campo").on("change",function(){
      preGuardar();
    });
});
function preGuardar(){
    $.ajax({
        type: "POST",
        url: base_url+"Clientes/submitInmueble5b",
        data: $('#form_inmueble_cli').serialize(),
        beforeSend: function(){
            $("#btn_submit").attr("disabled",true);
        },
        success: function (result) {
            $("#id").val(result);
            $("#btn_submit").attr("disabled",false);
        }
    });
}

function autoComplete(){
    cp = $("#cp").val(); 
    col = $('#colonia').val(); 
    $('#colonia').select2({
        width: '90%',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione una colonia:',
        ajax: {
            url: base_url+'Perfilamiento/getDatosCPSelect',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp: cp, 
                    col: col ,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.colonia,
                        text: element.colonia,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}


function regresar_view(){
    location.href= base_url+'Clientes/catalogoInmuebles5b';
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}

