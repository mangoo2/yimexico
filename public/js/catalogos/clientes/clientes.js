/* # Este JS se ocupa para el administrador que va a dar de alta al cliente yi # 
*/
var base_url = $('#base_url').val();
var verifi_usuario=0;
var verifi_pass=0;
$(document).ready(function($) {
	  //$("#Moral").hide();
    //$("#Fideicomiso").hide();
    select_tipo_persona();
    //getpais();
    ////==Registro de cliente ==///
    $('.guardar_registro').click(function(event) {
        //////////VAlidacion///////////
        var form_register = $('#form_cliente');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                actividad_vulnerable:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")|| element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_cliente").valid();
        if(valid) {
        // Validar que seleccione una actividad  
        var acti=$('#id_cliente_actividad').val();
        if(acti==undefined){
          swal("Atención!", "No has seleccionado al menos una actividad", "error");
        }else{ 
          //<!----- Verificar tipo de persona ----->
            var aux_c = 0;
            if($("#tipo1").is(':checked')){
              aux_c = 1;
            }
            if($("#tipo2").is(':checked')){
              aux_c = 2;
            }
            if($("#tipo3").is(':checked')){
              aux_c = 3;
            }
            if(aux_c==1){
                var form_register = $('#form_fisica');
                var error_register = $('.alert-danger', form_register);
                var success_register = $('.alert-success', form_register);
                var $validator1=form_register.validate({
                    errorElement: 'div', //default input error message container
                    errorClass: 'vd_red', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "",
                    rules: {
                        rfc:{
                          required: true,
                          rfc: true
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                            element.parent().append(error);
                        } else if (element.parent().hasClass("vd_input-wrapper")){
                            error.insertAfter(element.parent());
                        }else {
                            error.insertAfter(element);
                        }
                    }, 
                    
                    invalidHandler: function (event, validator) { //display error alert on form submit              
                            success_register.fadeOut(500);
                            error_register.fadeIn(500);
                            scrollTo(form_register,-100);

                    },

                    highlight: function (element) { // hightlight error inputs
                
                        $(element).addClass('vd_bd-red');
                        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                    },

                    unhighlight: function (element) { // revert the change dony by hightlight
                        $(element)
                            .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label, element) {
                        label
                            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                        $(element).removeClass('vd_bd-red');
                    }
                });
                var valid2 = $("#form_fisica").valid();
                if(valid2){
                  if(verifi_usuario==1){
                    swal("Error!", "El usuario ya existe.", "error");
                  }else{
                    if(verifi_pass==1){
                      swal("Error!", "Por favor, introduzca el mismo valor de nuevo.", "error");
                    }else{
                      registrocliente();
                    } 
                  }   
                }
            }else if(aux_c==2){
                var form_register = $('#form_moral');
                var error_register = $('.alert-danger', form_register);
                var success_register = $('.alert-success', form_register);
                var $validator1=form_register.validate({
                    errorElement: 'div', //default input error message container
                    errorClass: 'vd_red', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "",
                    rules: {
                        r_c_f:{
                          required: true,
                          rfc: true
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                            element.parent().append(error);
                        } else if (element.parent().hasClass("vd_input-wrapper")){
                            error.insertAfter(element.parent());
                        }else {
                            error.insertAfter(element);
                        }
                    }, 
                    
                    invalidHandler: function (event, validator) { //display error alert on form submit              
                            success_register.fadeOut(500);
                            error_register.fadeIn(500);
                            scrollTo(form_register,-100);

                    },

                    highlight: function (element) { // hightlight error inputs
                
                        $(element).addClass('vd_bd-red');
                        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                    },

                    unhighlight: function (element) { // revert the change dony by hightlight
                        $(element)
                            .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label, element) {
                        label
                            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                        $(element).removeClass('vd_bd-red');
                    }
                });
                var valid3 = $("#form_moral").valid();
                if(valid3){
                  if(verifi_usuario==1){
                    swal("Error!", "El usuario ya existe.", "error");
                  }else{
                    if(verifi_pass==1){
                      swal("Error!", "Por favor, introduzca el mismo valor de nuevo.", "error");
                    }else{
                      registrocliente();
                    } 
                  }   
                }
            }else if(aux_c==3){
                var form_register = $('#form_fideicomiso');
                var error_register = $('.alert-danger', form_register);
                var success_register = $('.alert-success', form_register);
                var $validator1=form_register.validate({
                    errorElement: 'div', //default input error message container
                    errorClass: 'vd_red', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "",
                    rules: {
                        rfc_fideicomiso:{
                          required: true,
                          rfc: true
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                            element.parent().append(error);
                        } else if (element.parent().hasClass("vd_input-wrapper")){
                            error.insertAfter(element.parent());
                        }else {
                            error.insertAfter(element);
                        }
                    }, 
                    
                    invalidHandler: function (event, validator) { //display error alert on form submit              
                            success_register.fadeOut(500);
                            error_register.fadeIn(500);
                            scrollTo(form_register,-100);

                    },

                    highlight: function (element) { // hightlight error inputs
                
                        $(element).addClass('vd_bd-red');
                        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                    },

                    unhighlight: function (element) { // revert the change dony by hightlight
                        $(element)
                            .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label, element) {
                        label
                            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                        $(element).removeClass('vd_bd-red');
                    }
                });
                var valid4 = $("#form_fideicomiso").valid();
                if(valid4){
                  if(verifi_usuario==1){
                    swal("Error!", "El usuario ya existe.", "error");
                  }else{
                    if(verifi_pass==1){
                      swal("Error!", "Por favor, introduzca el mismo valor de nuevo.", "error");
                    }else{
                      registrocliente();
                    } 
                  }   
                }
            }
          //<!----- ----->
          } 
        //   
        
        }
    });
    //<!-- Replicar select actividad
    setTimeout(function(){
      if($("#idcliente").val()==0 || $("input[name*='UsuarioID']").val()==0){
        $("#Usuario").val("");
        $("#contrasena").val("");
      }
    }, 1500);
    var id_cli=$('#idcliente').val();
    if(id_cli!=0){
        tabla_actividades_clientes();
    }
    $(".campo").on("change",function(){
        //preGuardar(); //revisar 
    });
});

function preGuardar(){
    /*var datos = $('#form_cliente').serialize();
    $.ajax({
      type:'POST',
      url: base_url+'Clientes/registro_cliente',
      data: datos,
      statusCode:{
          404: function(data){
            swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
            swal("Error!", "500", "error");
          }
      },
      success:function(data){
        var array = $.parseJSON(data);
        if (array.status==1) {
            //swal("Éxito", "Guardado Correctamente", "success");
        }else if (array.status==2) {
            //swal("Éxito", "Actualizado Correctamente", "success");
        }
        var idcli=array.id;
        $("#idcliente").val(idcli);
        if($('#tipo1').is(':checked')){// Persona fisica
        ///===
          var datos_fisica = $('#form_fisica').serialize()+'&idcliente='+idcli;
          $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro_fisica',
            data: datos_fisica,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                $("input[name*='idfisica']").val(data);
            }  
          }); 
        ///===
        }
        if($('#tipo2').is(':checked')){// Persona moral
        ///===
          var datos_moral = $('#form_moral').serialize()+'&idcliente='+idcli;
          $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro_moral',
            data: datos_moral,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                $("input[name*='idmoral']").val(data);
            }  
          }); 
        ///===
        }
        if($('#tipo3').is(':checked')){// Perosna de fideicomiso
        ///===
          var datos_fideicomiso = $('#form_fideicomiso').serialize()+'&idcliente='+idcli;
          $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro_fideicomiso',
            data: datos_fideicomiso,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                $("input[name*='idfideicomiso']").val(data);
            }  
          }); 
        ///===
        } 
        ///Actividad//
        var DATA  = [];
        var TABLA   = $("#table_actividad #tbody_actividad > .rows_actividad");
        TABLA.each(function(){   
                item = {};
                item ["idcliente"] = idcli;
                item ["id_cliente_actividad"] = $(this).find("input[id*='id_cliente_actividad']").val();
                item ["idactividad"] = $(this).find("input[id*='idactividad']").val();
                DATA.push(item);    
        });
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Clientes/registro_actividades',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            }
        });
        //////////////
        ///Contacto
        var datos_contacto = $('#form_contacto').serialize()+'&idcliente='+idcli;
        $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro_contacto',
            data: datos_contacto,
            statusCode:{
                404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                  swal("Error!", "500", "error");
                }
            },
            success:function(data){
                $("input[name*='idcontacto']").val(data);
            }  
        });  
        ///

      }
  });*/
}

function registrocliente(){
  var datos = $('#form_cliente').serialize();
  $.ajax({
      type:'POST',
      url: base_url+'Clientes/registro_cliente',
      data: datos,
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        var array = $.parseJSON(data);
        if (array.status==1) {
            swal("Éxito", "Guardado Correctamente", "success");
        }else if (array.status==2) {
            swal("Éxito", "Actualizado Correctamente", "success");
        }
        var idcli=array.id;
        if($('#tipo1').is(':checked')){// Persona fisica
        ///===
          var datos_fisica = $('#form_fisica').serialize()+'&idcliente='+idcli;
          $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro_fisica',
            data: datos_fisica,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
            }  
          }); 
        ///===
        }
        if($('#tipo2').is(':checked')){// Persona moral
        ///===
          var datos_moral = $('#form_moral').serialize()+'&idcliente='+idcli;
          $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro_moral',
            data: datos_moral,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
            }  
          }); 
        ///===
        }
        if($('#tipo3').is(':checked')){// Perosna de fideicomiso
        ///===
          var datos_fideicomiso = $('#form_fideicomiso').serialize()+'&idcliente='+idcli;
          $.ajax({
            type:'POST',
            url: base_url+'Clientes/registro_fideicomiso',
            data: datos_fideicomiso,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
            }  
          }); 
        ///===
        } 
        ///Actividad//
        var DATA  = [];
        var TABLA   = $("#table_actividad #tbody_actividad > .rows_actividad");
        TABLA.each(function(){   
                item = {};
                item ["idcliente"] = idcli;
                item ["id_cliente_actividad"] = $(this).find("input[id*='id_cliente_actividad']").val();
                item ["idactividad"] = $(this).find("input[id*='idactividad']").val();
                DATA.push(item);    
        });
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Clientes/registro_actividades',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            }
        });
        //////////////
        ///Contacto
        var datos_contacto = $('#form_contacto').serialize()+'&idcliente='+idcli;
        $.ajax({
          type:'POST',
          url: base_url+'Clientes/registro_contacto',
          data: datos_contacto,
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
          }  
        });  
        ///
        ///Usuario
        var datos_usuario = $('#form_usuario').serialize()+'&idcliente='+idcli;
        $.ajax({
          type:'POST',
          url: base_url+'Clientes/registro_user',
          data: datos_usuario,
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
          }  
        });  
        ///
        setTimeout(function(){ 
            location.href= base_url+'Clientes';
        }, 1500);
      }
  });
}

function select_tipo_persona(){
    var idcliente = $('#idcliente').val();
    var aux = 0;
    if($("#tipo1").is(':checked')){
      aux = 1;
    }
    if($("#tipo2").is(':checked')){
      aux = 2;
    }
    if($("#tipo3").is(':checked')){
      aux = 3;
    }
    $.ajax({
		url: base_url+'Clientes/tipo_persona',
		type: 'POST',
		data:{id:aux,idcli:idcliente},
		success: function(data) {
            $('.info_tipo_persona').html(data);
            setTimeout(function(){ 
            	//getpais();
            }, 2000);
	    },
	    error: function(jqXHR) {
	
     	}
	});
}
/// Verificar que no exista el usuario
function verificauser(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/verificauser',
        data:{user:$('#Usuario').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                $('.vefi_iser').html('El usuario ya existe.');
                verifi_usuario=1;
            }else{
                $('.vefi_iser').html('');
                verifi_usuario=0;
            }
        }
    }); 
}
/// Verificar que el password sea el mismo 
function verificarpass(){
    var pass1=$('#contrasena').val();
    var pass2=$('#contrasenav').val();
    if(pass1!=pass2){
      $('.text_validar_pass').html('Por favor, introduzca el mismo valor de nuevo.');
      verifi_pass=1;
    }else{
      $('.text_validar_pass').html('');
      verifi_pass=0;
    }
}
//<!--- funcion para duclicar -->//
function btn_add_actividad(){
     add_actividad_nva(0,$('#actividad_vulnerable option:selected').val(),$('#actividad_vulnerable option:selected').text());
}
var aux_act_id=1;
function add_actividad(id_cli_act,id_cli,text){
    $('.tabla_act').css('display','block');
    var id_acti=0;
    var text_act='';
    if(id_cli!=0){
       id_cli=id_cli;
       text_act=text;
    }
    else{
       id_acti=$('#actividad_vulnerable option:selected').val(); 
       text_act=$('#actividad_vulnerable option:selected').text();
    }

    var tablerow='<tr class="rows_actividad delete_act_tr_'+aux_act_id+'">\
                  <td>'+text_act+'<input type="hidden" id="id_cliente_actividad" value="'+id_cli_act+'"><input type="hidden" id="idactividad" value="'+id_acti+'">\
                    <input type="hidden" id="id_acti_cont" value="'+id_cli+'"></td>\
                  <td>\
                      <button type="button" class="btn btn_borrar btn-rounded btn-icon" onclick="delete_actividad('+aux_act_id+','+id_cli_act+')">\
                      <i class="fa fa-trash-o"></i>\
                    </button>\
                  </td>\
              </tr>\
              ';
    $('#tbody_actividad').append(tablerow);
    aux_act_id++;
}
function add_actividad_nva(id_cli_act,activ,text){
    $('.tabla_act').css('display','block');
    var id_acti=0;
    var text_act='';
    id_acti=activ; 
    text_act=text;
    
    var repetido=0;
    console.log("length table: "+$("#table_actividad tbody > tr").length);
    if($("#table_actividad tbody > tr").length<1){
        var TABLA   = $("#table_actividad tbody > tr");
        TABLA.each(function(){         
            var activ_asig = $(this).find("input[id*='id_acti_cont']").val();
            if (activ_asig==id_acti) {
                repetido=1;
            }
            console.log("id_acti: "+id_acti);
            console.log("activ_asig: "+activ_asig);
        });
        if(repetido==0){
            var tablerow='<tr class="rows_actividad delete_act_tr_'+aux_act_id+'">\
                          <td>'+text_act+'<input type="hidden" id="id_cliente_actividad" value="'+id_cli_act+'"><input type="hidden" id="idactividad" value="'+id_acti+'">\
                          <input type="hidden" id="id_acti_cont" value="'+activ+'"> </td>\
                          <td>\
                              <button type="button" class="btn btn_borrar btn-rounded btn-icon" onclick="delete_actividad('+aux_act_id+','+id_cli_act+')">\
                              <i class="fa fa-trash-o"></i>\
                            </button>\
                          </td>\
                      </tr>\
                      ';
            $('#tbody_actividad').append(tablerow);
            aux_act_id++;
        }else{
            swal("Error!", "Actividad vulnerable contratada anteriormente", "error");
        }
    }else{
        swal("Error!", "Solo se permite una actividad por cliente", "error");
    }
}
function delete_actividad(id,idact){
    if (idact==0) {
        $('.delete_act_tr_'+id).remove();
    }else{
        $('#modal_eliminar').modal();
        $('#id_activi').val(idact);
        $('#delete_div_acti').val(id);
    } 
}
function tabla_actividades_clientes(){
    var id_cliente = $('#idcliente').val();
    if(id_cliente>0){
        $.ajax({
            type:'POST',
            url: base_url+"Clientes/get_actividades_cli",
            data: {idcliente:id_cliente},
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
               console.log(array);
               if (array.length>0) {
                    array.forEach(function(element) {
                        add_actividad(element.id_cliente_actividad,element.id,element.nombre);
                        console.log("id: "+element.id);
                   });
               }   
            },
            error: function(response){
                swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
            }
        });
    }
}
function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/updateregistro_actividad',
        data:{id:$('#id_activi').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                $('.delete_act_tr_'+$('#delete_div_acti').val()).remove();
            }, 1500);
        }
    });     
}
/*Guadar actividad
function modalactividad(id) {
    $('#modal_actividad').modal();
    $('#actividad').val('');
}
function guardar_actividad(){
  var form_register = $('#form_actividad');
  var error_register = $('.alert-danger', form_register);
  var success_register = $('.alert-success', form_register);
  var $validator1=form_register.validate({
      errorElement: 'div', //default input error message container
      errorClass: 'vd_red', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
          actividad:{
            required: true
          }
      },
      errorPlacement: function(error, element) {
          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
      }, 
      
      invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(500);
              error_register.fadeIn(500);
              scrollTo(form_register,-100);

      },

      highlight: function (element) { // hightlight error inputs
  
          $(element).addClass('vd_bd-red');
          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

      },

      unhighlight: function (element) { // revert the change dony by hightlight
          $(element)
              .closest('.control-group').removeClass('error'); // set error class to the control group
      },

      success: function (label, element) {
          label
              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
          $(element).removeClass('vd_bd-red');
      }
  });
  //////////Registro///////////
  var valid = $("#form_actividad").valid();
  if(valid) {
    //<!------------->// 
    var datos_actividad = $('#form_actividad').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/registro_actividad',
        data:datos_actividad,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Guardado Correctamente", "success");
            setTimeout(function(){ 
              $('#modal_actividad').modal('hide');
              $('#actividad_vulnerable').append('<option value="'+data+'" selected="">'+$('#actividad').val()+'</option>');
            }, 1500);

        }
    });
    //<!--------------->//
  }
}


<div class="col-md-12 form-group">
  <select class="form-control" id="actividad_vulnerable">
    <option selected="" disabled="">Seleccione actividad vulnerable</option>
    <?php foreach ($act as $key) { ?>
      <option value="<?php echo $key->id; ?>" <?php if($key->id==$actividad_vulnerable) echo 'selected' ?>><?php echo $key->actividad; ?></option>
    <?php } ?>
  </select>       
</div>
*/