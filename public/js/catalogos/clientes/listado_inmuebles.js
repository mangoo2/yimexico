var base_url = $('#base_url').val();
$(document).ready(function($) {
    tabla=$("#table_inmueble").DataTable({
        "ajax": {
           "url": base_url+"Clientes/data_records_inmuebles",
           type: "post",
            error: function(){
               $("#table_inmueble").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var tipo='';
                        if(row.tipo_inmueble==1){
                           tipo="Casa/casa en condominio";
                        }else if(row.tipo_inmueble==2){
                           tipo="Departamento";
                        }else if(row.tipo_inmueble==3){
                           tipo="Edificio habitacional";
                        } else if(row.tipo_inmueble==4){
                           tipo="Edificio comercial";
                        }else if(row.tipo_inmueble==5){
                           tipo="Edificio oficinas";
                        }else if(row.tipo_inmueble==6){
                           tipo="Local comercial independiente";
                        }else if(row.tipo_inmueble==7){
                           tipo="Local en centro comercial";
                        } else if(row.tipo_inmueble==8){
                           tipo="Oficina";
                        }else if(row.tipo_inmueble==9){
                           tipo="Bodega comercial";
                        }else if(row.tipo_inmueble==10){
                           tipo="Bodega industrial"; 
                        }else if(row.tipo_inmueble==11){
                           tipo="Nave Industrial";
                        } else if(row.tipo_inmueble==12){
                           tipo="Terreno urbano habitacional";
                        }else if(row.tipo_inmueble==13){
                           tipo="Terreno no urbano habitacional";
                        }else if(row.tipo_inmueble==14){
                           tipo="Terreno urbano comercial o industrial";
                        }else if(row.tipo_inmueble==15){
                           tipo="Terreno no urbano comercial o industrial";
                        }else if(row.tipo_inmueble==16){
                           tipo="Terreno ejidal";
                        }else if(row.tipo_inmueble==17){
                           tipo="Rancho/Hacienda/Quinta";
                        }else if(row.tipo_inmueble==18){
                           tipo="Huerta";
                        }else if(row.tipo_inmueble==99){
                           tipo="Otro";
                        }       
                    return tipo;
                }
            },
            {"data": null,
                "render": function (data, type, row, meta ) {
                    var valor=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.valor)
                    return valor;
                } 
            },
            {"data": null,
                "render": function (data, type, row, meta ) {
                    var dir=row.calle_avenida+" "+row.num_ext+" No. Int. "+row.num_int+" Colonia "+row.colonia+" C.P. "+row.cp
                    return dir;
                } 
            },
            {"data": "folio_inmueble"},
            {"data": null,
                "render": function ( data, type, row, meta ) {
                var html='<button type="button" class="btn edit" style="color: #212b4c;" title="Editar"><i class="fa fa-edit" style="font-size: 22px;"></i></a>\
                          <button type="button" class="btn delete" style="color: #212b4c;" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>';        
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });

    $('#table_inmueble').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var row = tabla.row(tr);
        var data = row.data();
        window.location = base_url+'Clientes/addInmueble/'+data.id;
    });
    $('#table_inmueble').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        modaleliminar(data.id);
    }); 

    $("#eliminar").on("click",function(){
        eliminar();
    });

});

function modaleliminar(id) {
    $('#modal_eliminar').modal();
    $('#id_inmueble').val(id);
}
function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/deleteInmueble',
        data:{id:$('#id_inmueble').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                tabla.ajax.reload();
            }, 1500);
        }
    });     
}

