//* #JS Cuando el cliente se inicio sesion ya esta acompletadno sus datos
var base_url = $('#base_url').val();
$(document).ready(function() {
	// cliente fisica
	getpais('p',1);
	getpais('p',2);
    // cliente moral
    getpais('m',1); 
	// # pais 
    tabla_sucursal();
    var aux=0;
    var auxt = $('#tipo_persona').val();
    if(auxt==1){
    aux = $('#idfisica').val();
    }else if(auxt==2){
    aux = $('#idmoral').val();
    }else if(auxt==3){
    aux = $('#idfideicomiso').val();
    }
    /*if(aux=='' || aux==undefined){
       btn_add_sucursal();
    }else{
    	btn_add_sucursal();
    }*/
    setTimeout(function(){ 
	    var cuenta_sucus=0;
	    var TABLAPR   = $(".ptext_direccion .text_direccion > .rowsucursal");
	    TABLAPR.each(function(){   
	    	cuenta_sucus++;
	    });
	    if(cuenta_sucus==0){
	       btn_add_sucursal();
	       console.log("no tiene sucursales aun");
	    }/*else{
	    	btn_add_sucursal();
	    }*/
    }, 1500);

    $('#fecha_constitucionf').on('change', function() {
    	console.log("fecha cambia");
    	var dado = $("#fecha_constitucionf").val();
    	var f = new Date();
		var act = (f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	    /*if(dado.getTime()>actual.getTime()){
	    	console.log("fecha mayor a hoy");
	    }*/
	    var x = dado.split("-");
	    var z = act.split("-");

	    //Cambiamos el orden al formato americano, de esto dd/mm/yyyy a esto mm/dd/yyyy
	    fecha1 = x[1] + "-" + x[0] + "-" + x[2];
	    fecha2 = z[1] + "-" + z[0] + "-" + z[2];

	    //Comparamos las fechas
	    if (Date.parse(fecha1) > Date.parse(fecha2)){
	        console.log("fecha mayor a hoy");
	    }else{
	        console.log("fecha menor a hoy");
	    }
	});	 
    //$('#fecha_constitucionf').val(new Date().toDateInputValue());

    $(".campo").on("keyup",function(){
    	//ver para guarda cada que cambie un campo en formulario, obtener el tipo de persona y guardar por medio del id si es editar, 
    	//si es guardar nuevo retorne despues de guardar el id dependiedo del tipo de persona
    	preGuardar();
    });
    $(".sel").on("change",function(){
    	//ver para guarda cada que cambie un campo en formulario, obtener el tipo de persona y guardar por medio del id si es editar, 
    	//si es guardar nuevo retorne despues de guardar el id dependiedo del tipo de persona
    	preGuardar();
    });
    $(".campoc").on("keyup",function(){
    	preGuardarCli();
    });
});
function preGuardar(){
	var tipo_persona = $('#tipo_persona').val();
	var url_cliente = '';
	if(tipo_persona==1){//<!---- Persona fisica ---->//
        form_register = $('#form_fisica');   
        url_cliente = 'Mis_datos/registro_fisica';
	}else if(tipo_persona==2){//<!---- Persona moral ---->//
        form_register = $('#form_moral'); 
        url_cliente = 'Mis_datos/registro_moral';
	}else if(tipo_persona==3){//<!---- Persona fideicomiso ---->//
        form_register = $('#form_fideicomiso'); 
        url_cliente = 'Mis_datos/registro_fideicomiso';
	}
 	var datos_cliente = form_register.serialize();
	$.ajax({
	    type:'POST',
	    url: base_url+url_cliente,
	    data:datos_cliente,
	    statusCode:{
	        404: function(data){
	            swal("Error!", "No Se encuentra el archivo", "error");
	        },
	        500: function(){
	            swal("Error!", "500", "error");
	        }
	    },
	    success:function(data){
	    	if(tipo_persona==1){//<!---- Persona fisica ---->//
	    		$("#idfisica").val(data);
			}else if(tipo_persona==2){//<!---- Persona moral ---->//
				$("#idmoral").val(data);
			}else if(tipo_persona==3){//<!---- Persona fideicomiso ---->//
				$("#idfideicomiso").val(data);
			}
	        //swal("Éxito", "Preguardado Correctamente", "success");
	        /*setTimeout(function(){ 
	            location.href= base_url+'Inicio_cliente';
	        }, 1500);*/

	    }
	});
}
function preGuardarCli(){
	var datos_c_cliente = $('#form_cliente').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Mis_datos/registro_cliente',
        data:datos_c_cliente,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
        	var array = $.parseJSON(data);
        	$("input[name*='idcliente']").val(array.id);
        }
    });
}
function preGuardarSuc(){
	//<!---- Sucural ---->//
    /*var DATA  = [];
    var TABLA   = $(".ptext_direccion .text_direccion > .rowsucursal");
    TABLA.each(function(){   
        if ($(this).find("input[id*='vialidad']").val()!='') {
            item = {};
            item ["iddireccion"] = $(this).find("input[id*='iddireccion']").val();
            item ["vialidad"] = $(this).find("select[id*='vialidad']").val();
            item ["calle"] = $(this).find("input[id*='calle']").val();
            item ["no_ext"] = $(this).find("input[id*='no_ext']").val();
            item ["no_int"] = $(this).find("input[id*='no_int']").val();
            //item ["colonia"] = $(this).find("input[id*='colonia']").val();
            item ["colonia"] = $(this).find("select[id*='colonia']").val();
            item ["municipio"] = $(this).find("input[id*='municipio']").val();
            item ["localidad"] = $(this).find("input[id*='localidad']").val();
            if($(this).find("select[class*='idpais_pais']").val()=='MX'){
                item ["estado"] = $(this).find("select[id*='estado']").val();
            }else{
                item ["estado"] = $(this).find("input[id*='estado']").val();
            }
            item ["cp"] = $(this).find("input[id*='cp']").val();
            item ["idpais"] = $(this).find("select[class*='idpais_pais']").val();
            item ["alias"] = $(this).find("input[id*='alias']").val();
            DATA.push(item);
        }     
    });
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO, 
        type: 'POST',
        url : base_url + 'Mis_datos/registro_direccion_sucursal',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
        }
    });*/
}

var form_register = '';
function guardar_registro() {
	var tipo_persona = $('#tipo_persona').val();
	var url_cliente = '';
	if(tipo_persona==1){//<!---- Persona fisica ---->//
        form_register = $('#form_fisica');   
	}else if(tipo_persona==2){//<!---- Persona moral ---->//
        form_register = $('#form_moral'); 
	}else if(tipo_persona==3){//<!---- Persona fideicomiso ---->//
        form_register = $('#form_fideicomiso'); 
	}
    var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);
    if(tipo_persona==1){//<!---- Persona fisica ---->//
        var $validator1=form_register.validate({
		      errorElement: 'div', //default input error message container
		      errorClass: 'vd_red', // default input error message class
		      focusInvalid: false, // do not focus the last invalid input
		      ignore: "",
		      rules: {
		          fecha_nacimiento:{
		            required: true
		          },
		          pais_nacimiento:{
		            required: true
		          },
		          pais_nacionalidad:{
		            required: true
		          },
		          curp:{
		            required: true
		          }
		      },
		      errorPlacement: function(error, element) {
		          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
		              element.parent().append(error);
		          } else if (element.parent().hasClass("vd_input-wrapper")){
		              error.insertAfter(element.parent());
		          }else {
		              error.insertAfter(element);
		          }
		      }, 
		      
		      invalidHandler: function (event, validator) { //display error alert on form submit              
		              success_register.fadeOut(500);
		              error_register.fadeIn(500);
		              scrollTo(form_register,-100);

		      },

		      highlight: function (element) { // hightlight error inputs
		  
		          $(element).addClass('vd_bd-red');
		          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

		      },

		      unhighlight: function (element) { // revert the change dony by hightlight
		          $(element)
		              .closest('.control-group').removeClass('error'); // set error class to the control group
		      },

		      success: function (label, element) {
		          label
		              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
		              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
		          $(element).removeClass('vd_bd-red');
		      }
		  });
        url_cliente = 'Mis_datos/registro_fisica';
	}else if(tipo_persona==2){//<!---- Persona moral ---->//
        var $validator1=form_register.validate({
		      errorElement: 'div', //default input error message container
		      errorClass: 'vd_red', // default input error message class
		      focusInvalid: false, // do not focus the last invalid input
		      ignore: "",
		      rules: {
		          fecha_constitucion:{
		            required: true
		          },
		          pais_nacionalidad:{
		            required: true
		          },
		          rfc_l:{
                    required: true,
                    rfc: true
                  }
		      },
		      errorPlacement: function(error, element) {
		          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
		              element.parent().append(error);
		          } else if (element.parent().hasClass("vd_input-wrapper")){
		              error.insertAfter(element.parent());
		          }else {
		              error.insertAfter(element);
		          }
		      }, 
		      
		      invalidHandler: function (event, validator) { //display error alert on form submit              
		              success_register.fadeOut(500);
		              error_register.fadeIn(500);
		              scrollTo(form_register,-100);

		      },

		      highlight: function (element) { // hightlight error inputs
		  
		          $(element).addClass('vd_bd-red');
		          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

		      },

		      unhighlight: function (element) { // revert the change dony by hightlight
		          $(element)
		              .closest('.control-group').removeClass('error'); // set error class to the control group
		      },

		      success: function (label, element) {
		          label
		              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
		              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
		          $(element).removeClass('vd_bd-red');
		      }
		  });
        url_cliente = 'Mis_datos/registro_moral';
	}else if(tipo_persona==3){//<!---- Persona fideicomiso ---->//
        var $validator1=form_register.validate({
		      errorElement: 'div', //default input error message container
		      errorClass: 'vd_red', // default input error message class
		      focusInvalid: false, // do not focus the last invalid input
		      ignore: "",
		      rules: {
		          identificador_fideicomiso:{
		            required: true
		          },
		          rfc_l:{
                    required: true,
                    rfc: true
                  }
		      },
		      errorPlacement: function(error, element) {
		          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
		              element.parent().append(error);
		          } else if (element.parent().hasClass("vd_input-wrapper")){
		              error.insertAfter(element.parent());
		          }else {
		              error.insertAfter(element);
		          }
		      }, 
		      
		      invalidHandler: function (event, validator) { //display error alert on form submit              
		              success_register.fadeOut(500);
		              error_register.fadeIn(500);
		              scrollTo(form_register,-100);

		      },

		      highlight: function (element) { // hightlight error inputs
		  
		          $(element).addClass('vd_bd-red');
		          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

		      },

		      unhighlight: function (element) { // revert the change dony by hightlight
		          $(element)
		              .closest('.control-group').removeClass('error'); // set error class to the control group
		      },

		      success: function (label, element) {
		          label
		              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
		              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
		          $(element).removeClass('vd_bd-red');
		      }
		  });
        url_cliente = 'Mis_datos/registro_fideicomiso';
	}  
	//////////Registro///////////
	var valid = form_register.valid();
	if(valid) {
		$('#form_cliente').validate({
		      errorElement: 'div', //default input error message container
		      errorClass: 'vd_red', // default input error message class
		      focusInvalid: false, // do not focus the last invalid input
		      ignore: "",
		      rules: {
                  telefono:{
		            required: true,
					minlength: 10,
                    maxlength: 13
		          },
		          movil:{
		            required: true,
					minlength: 10,
                    maxlength: 13
		          },
		          correo:{
		            required: true
		          }
		      },
		      errorPlacement: function(error, element) {
		          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
		              element.parent().append(error);
		          } else if (element.parent().hasClass("vd_input-wrapper")){
		              error.insertAfter(element.parent());
		          }else {
		              error.insertAfter(element);
		          }
		      }, 
		      
		      invalidHandler: function (event, validator) { //display error alert on form submit              
		              success_register.fadeOut(500);
		              error_register.fadeIn(500);
		              scrollTo(form_register,-100);

		      },

		      highlight: function (element) { // hightlight error inputs
		  
		          $(element).addClass('vd_bd-red');
		          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

		      },

		      unhighlight: function (element) { // revert the change dony by hightlight
		          $(element)
		              .closest('.control-group').removeClass('error'); // set error class to the control group
		      },

		      success: function (label, element) {
		          label
		              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
		              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
		          $(element).removeClass('vd_bd-red');
		      }
		});
		var valid2 = $('#form_cliente').valid();
	    if(valid2){
            var calleaux = false;
            var aliasaux = false;
            var cuenta_sucus=0; var cont_tot=0; var cont_comp=0;
	        var TABLAPR   = $(".ptext_direccion .text_direccion > .rowsucursal");
	        TABLAPR.each(function(){   
	        	cuenta_sucus++;
	        	cont_tot++;
	            if($(this).find("input[id*='calle']").val()!=""){
	            	calleaux=true;
	            	cont_comp++;
	            }
	            else{
	            	calleaux=false;
	            }
	            if($(this).find("input[id*='alias']").val()!=""){
	            	aliasaux=true;
	            	cont_comp++;
	            }
	            else{
	            	aliasaux=false;
	            }
	            /*console.log("calle: "+$(this).find("input[id*='calle']").val());
	            console.log("alias: "+$(this).find("input[id*='alias']").val());
	            console.log("calleaux: "+calleaux);
	            console.log("aliasaux: "+aliasaux);
	            console.log("cont_comp: "+cont_comp);
	            console.log("cont_tot: "+cont_tot);*/
	        });

	        cont_comp=cont_comp/2;
	        //console.log("cont_comp final: "+cont_comp);
	        //console.log("cuenta_sucus: "+cuenta_sucus);
            if(calleaux!=true && aliasaux!=true && cont_tot!=cont_comp || cont_tot!=cont_comp){
            	if(cuenta_sucus<1)
                	swal("Atención!","Tienes que agregar al menos una dirección", "error");
                if(cuenta_sucus>=1)
                	swal("Atención!","Favor de llenar todos los campos de sucursal", "error");
            }else{
            	if(aliasaux!=true){
                  swal("Atención!","El campo de Alias / sucursal esta vacío", "error");
            	}if(calleaux!=true){
                  swal("Atención!","El campo Calle esta vacío", "error");
            	}

            	else if(calleaux==true && aliasaux==true && cont_tot==cont_comp){
				    //<!-----Persona----->// 
					    var datos_cliente = form_register.serialize();
					    $.ajax({
					        type:'POST',
					        url: base_url+url_cliente,
					        data:datos_cliente,
					        statusCode:{
					            404: function(data){
					                swal("Error!", "No Se encuentra el archivo", "error");
					            },
					            500: function(){
					                swal("Error!", "500", "error");
					            }
					        },
					        success:function(data){
					            /*swal("Éxito", "Guardado Correctamente", "success");
					            setTimeout(function(){ 
			                        location.href= base_url+'Inicio_cliente';
					            }, 1500);*/

					        }
					    });
					    //<!---- Sucural ---->//
				        var DATA  = [];
				        var TABLA   = $(".ptext_direccion .text_direccion > .rowsucursal");
				        TABLA.each(function(){   
				            if ($(this).find("input[id*='vialidad']").val()!='' && $(this).find("input[id*='alias']").val()!='' && $(this).find("input[id*='calle']").val()!='') {
				                item = {};
				                item ["iddireccion"] = $(this).find("input[id*='iddireccion']").val();
				                item ["vialidad"] = $(this).find("select[id*='vialidad']").val();
				                item ["calle"] = $(this).find("input[id*='calle']").val();
				                item ["no_ext"] = $(this).find("input[id*='no_ext']").val();
				                item ["no_int"] = $(this).find("input[id*='no_int']").val();
				                //item ["colonia"] = $(this).find("input[id*='colonia']").val();
				                item ["colonia"] = $(this).find("select[id*='colonia']").val();
				                item ["municipio"] = $(this).find("input[id*='municipio']").val();
				                item ["localidad"] = $(this).find("input[id*='localidad']").val();
				                if($(this).find("select[class*='idpais_pais']").val()=='MX'){
			                        item ["estado"] = $(this).find("select[id*='estado']").val();
				                }else{
			                        item ["estado"] = $(this).find("input[id*='estado']").val();
				                }
				                item ["cp"] = $(this).find("input[id*='cp']").val();
				                item ["idpais"] = $(this).find("select[class*='idpais_pais']").val();
				                item ["alias"] = $(this).find("input[id*='alias']").val();
				                DATA.push(item);
				            }     
				        });
				        INFO  = new FormData();
				        aInfo   = JSON.stringify(DATA);
				        INFO.append('data', aInfo);
				        $.ajax({
				            data: INFO, 
				            type: 'POST',
				            url : base_url + 'Mis_datos/registro_direccion_sucursal',
				            processData: false, 
				            contentType: false,
				            async: false,
				            statusCode:{
				                404: function(data){
				                    swal("Error!", "No Se encuentra el archivo", "error");
				                },
				                500: function(){
				                    swal("Error!", "500", "error");
				                }
				            },
				            success: function(data){
				            }
				        }); 
				        //<!---- Cliente
				        var datos_c_cliente = $('#form_cliente').serialize();
					    $.ajax({
					        type:'POST',
					        url: base_url+'Mis_datos/registro_cliente',
					        data:datos_c_cliente,
					        statusCode:{
					            404: function(data){
					                swal("Error!", "No Se encuentra el archivo", "error");
					            },
					            500: function(){
					                swal("Error!", "500", "error");
					            }
					        },
					        success:function(data){
					        	swal("Éxito", "Guardado Correctamente", "success");
					            setTimeout(function(){ 
			                        location.href= base_url+'Inicio_cliente';
					            }, 1500);
					        }
					    });
				    //<!----------------->/*/
                }
            }
	    }
	}

}
function btn_add_sucursal(){
     add_sucursal(0,'','','','','','','','','','','','');
}
var aux_sucursal=1;
var cont_cp=1;
function add_sucursal(iddireccion,vialidad,calle,no_ext,no_int,colonia,municipio,localidad,estado,cp,pais,nombre_pais,alias){
	  var tablerow='<h4 class="delete_sucursal_div_titulo_'+aux_sucursal+'">Dirección '+aux_sucursal+'</h4>';           
         tablerow+='<div class="row delete_sucursal_div_btn_'+aux_sucursal+'">\
				      <div class="col-md-12" align="right">\
				        <button type="button" class="btn btn_borrar2" onclick="delete_sucursal('+aux_sucursal+','+iddireccion+')"><i class="fa fa-trash"></i> Eliminar sucursal '+aux_sucursal+'</button>\
				      </div> \
				    </div>\
					';
         tablerow+='<script> $(document).ready(function(){ cambiaCP('+cont_cp+'); autoComplete('+cont_cp+'); }); </script>\
         			<div class="row rowsucursal delete_sucursal_div_'+aux_sucursal+'">\
                       <input type="hidden" id="iddireccion" value="'+iddireccion+'">\
					  <div class="col-md-2 form-group">\
					    <label>Tipo de vialidad:</label>\
					    <span class="span_tv_'+aux_sucursal+'"></span>\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Calle:</label>\
					    <input onchange="preGuardarSuc()" class="form-control" type="text" id="calle" value="'+calle+'">\
					  </div>\
					  <div class="col-md-2 form-group">\
					    <label>No.ext:</label>\
					    <input onchange="preGuardarSuc()" class="form-control" type="text" id="no_ext" value="'+no_ext+'">\
					  </div>\
					  <div class="col-md-2 form-group">\
					    <label>No.int:</label>\
					    <input onchange="preGuardarSuc()" class="form-control" type="text" id="no_int" value="'+no_int+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>C.P:</label>\
					    <input onchange="cambiaCP('+cont_cp+');preGuardarSuc()" pattern="\d*" maxlength="5" class="form-control cp_'+cont_cp+'" type="text" id="cp" value="'+cp+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Municipio o delegación:</label>\
					    <input onchange="preGuardarSuc()" class="form-control muni_'+cont_cp+'" type="text" id="municipio" value="'+municipio+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Localidad:</label>\
					    <input onchange="preGuardarSuc()" class="form-control" type="text" id="localidad" value="'+localidad+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Estado:</label>\
					    <span class="span_div_s_'+aux_sucursal+'"></span>\
					  </div>\
					  <!--<div class="col-md-3 form-group">\
					    <label>Colonia:</label>\
					    <input  class="form-control" type="text" id="colonia" value="'+colonia+'">\
					  </div>-->\
					  <div class="col-md-3 form-group">\
                          <label>Colonia:</label><br>\
                          <select onchange="preGuardarSuc()" onclick="autoComplete('+cont_cp+')" class="form-control colonia_'+cont_cp+'" id="colonia">\
                            <option value=""></option>';
                            if(colonia!=''){
                              tablerow+='<option value="'+colonia+'" selected>'+colonia+'</option>';      
                             }  
                        tablerow+='</select>\
                        </div>\
					  <div class="col-md-4 form-group">\
					    <label>País:</label><br>\
					    <select class="form-control idpais_s_'+aux_sucursal+' idpais_pais" onchange="preGuardarSuc()">';
					if(pais!=''){
                     tablerow+='<option value="'+pais+'">'+nombre_pais+'</option>';
					}else{
                     tablerow+='<option value="MX">MEXICO</option>';
					}    
			        //<input class="form-control" type="text" id="estado" value="'+estado+'">\
			 tablerow+='</select>\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Alias / Sucursal:</label>\
					    <input onchange="preGuardarSuc()" class="form-control" type="text" id="alias" value="'+alias+'">\
					  </div>\
					</div>\
					';
    $('.text_direccion').append(tablerow);
    getpais2('s',aux_sucursal,estado);
    pais_verifica('s',aux_sucursal,estado);
    tipovialidad(aux_sucursal,vialidad);
    aux_sucursal++;
    cont_cp++;
}
function delete_sucursal(id,idsucursal){
    if (idsucursal==0) {
    	$('.delete_sucursal_div_titulo_'+id).remove();
        $('.delete_sucursal_div_btn_'+id).remove();
        $('.delete_sucursal_div_'+id).remove();
        aux_sucursal--;
    	cont_cp--;
    }else{
        $('#modal_eliminar').modal();
        $('#id_sucursal').val(idsucursal);
        $('#delete_div_sucursal').val(id);
    } 
}
function tabla_sucursal(){
    var id_cliente = $('#idcliente').val();
    if(id_cliente>0){
        $.ajax({
            type:'POST',
            url: base_url+"Mis_datos/get_sucursal",
            data: {idcliente:id_cliente},
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
               console.log(array);
               if (array.length>0) {
                    array.forEach(function(element) {
                        add_sucursal(element.iddireccion,element.vialidad,element.calle,element.no_ext,element.no_int,element.colonia,element.municipio,element.localidad,element.estado,element.cp,element.idpais,element.pais,element.alias);
                   });
               }   
            },
            error: function(response){
                swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
            }
        });
    }
}
function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Mis_datos/updateregistro_sucursal',
        data:{id:$('#id_sucursal').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                $('.delete_sucursal_div_titulo_'+$('#delete_div_sucursal').val()).remove();
                $('.delete_sucursal_div_btn_'+$('#delete_div_sucursal').val()).remove();
                $('.delete_sucursal_div_'+$('#delete_div_sucursal').val()).remove();
            }, 1500);
        }
    });     
}
function getpais2(text,id,estado){
    $('.idpais_'+text+'_'+id).select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Mis_datos/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var pais = $('.idpais_'+text+'_'+id+' option:selected').val();       
      if(pais=='MX'){
       //<!-------->
        $.ajax({
          type:'POST',
          data: {text_t:text,id_t:id},
          url: base_url+'Mis_datos/get_estados',
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
          	console.log("estado: "+estado)
            $('.span_div_'+text+'_'+id).html(data);
            $('.estado_'+text+'_'+id+' option[value="'+estado+'"]').attr("selected", "selected");
          }  
        });
       //<!-------->   
      }else{
        $('.span_div_'+text+'_'+id).html('<input class="form-control" type="text" id="estado" value="'+estado+'">'); 
      }
      
    });
}
////<!_-------->
function getpais(text,id){
    $('.idpais_'+text+'_'+id).select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Mis_datos/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
////<!--------->
function inicio_cliente(){
	location.href= base_url+'Inicio_cliente';
}
function pais_verifica(text,id,estado){
  var pais = $('.idpais_'+text+'_'+id+' option:selected').val();       
  if(pais=='MX'){
   //<!-------->
    $.ajax({
      type:'POST',
      data: {text_t:text,id_t:id},
      url: base_url+'Mis_datos/get_estados',
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        $('.span_div_'+text+'_'+id).html(data);
        $('.estado_'+text+'_'+id+' option[value="'+estado+'"]').attr("selected", "selected");
      }  
    });
   //<!-------->   
  }else{
	$('.span_div_'+text+'_'+id).html('<input class="form-control" type="text" id="estado" value="'+estado+'">'); 
  }
}
function tipovialidad(id,nombre){
    $.ajax({
      type:'POST',
      data: {id_t:id},
      url: base_url+'Mis_datos/get_tipo_vialidad',
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        $('.span_tv_'+id).html(data);
        $('.span_ptv_'+id+' option[value="'+nombre+'"]').attr("selected", "selected");
      }  
    });
}
function regresar_view(){
    window.history.back();
}

var cp="";
var col="";
function cambiaCP(cont){
    //console.log("id: "+cont);
    cp = $(".cp_"+cont+"").val();  
    //console.log("cp: "+cpb);
    if(cp!=""){
      $.ajax({
          url: base_url+'Perfilamiento/getDatosCP',
          type: 'POST',
          data: { cp: cp, col: "0" },
          success: function(data){ 
            var array = $.parseJSON(data);
            /*console.log("estado: "+array[0].estado);
            console.log("mnpio: "+array[0].mnpio);
            console.log("ciudad: "+array[0].ciudad);*/
            $(".estado_s_"+cont+"").val(array[0].id_estado);
            $(".estado_s_"+cont+"").attr("disabled",true);
            $(".muni_"+cont+"").val(array[0].mnpio.toUpperCase());
          }
      });
    }else{
      $(".estado_s_"+cont+"").val("");
      $(".estado_s_"+cont+"").attr("disabled",false);
      $(".muni_"+cont+"").val("");
    }
    //});  
}

function autoComplete(cont){
    //console.log("id: "+cont);
    //console.log("auto complete id: "+cont);
    cp= $(".cp_"+cont+"").val(); 
    col = $('.colonia_'+cont+'').val(); 

    $('.colonia_'+cont+'').select2({
        width: '90%',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione una colonia:',
        ajax: {
            url: base_url+'Perfilamiento/getDatosCPSelect',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp: cp, 
                    col: col,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.colonia,
                        text: element.colonia,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
