var base_url = $('#base_url').val();
$(document).ready(function($) {
    /// Tabla de clientes
    tabla_load_cliente();
});
function tabla_load_cliente(){
    tabla=$("#table_cliente").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
           "url": base_url+"Clientes/getlistado_cliente",
           type: "post",
            /*error: function(){
               $("#table_cliente").css("display","none");
            }*/
        },
        "columns": [
            {"data": "idcliente"},
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var html_cliente='';
                        if(row.tipo_persona==1){
                           html_cliente=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                        }else if(row.tipo_persona==2){
                           html_cliente=row.razon_social;
                        }else if(row.tipo_persona==3){
                           html_cliente=row.denominacion_razon_social;
                        }        
                    return html_cliente;
                }
            },
            /*{"data": null,
                "render": function (data, type, row, meta ) {
                      var html='<button type="button" class="btn btn-outline-secondary btn-sm" style="color: #2b254e;" onclick="modalactividad('+row.idcliente+')"><i class="fa fa-eye"></i> Actividades</button>';          
                    //var html= '<a class="btn " title="Editar" style="color: #2b254e;"><i class="fa fa-eye"></i> Actividades</a>'
                    return html;
                } 
            },*/
            {"data": null,
                "render": function (data, type, row, meta ) {
                    html= row.nombreAnexo
                    return html;
                } 
            },
            {"data": null,
                "render": function (data, type, row, meta ) {
                    var html= ChangeDate(row.fecha_alta_sistema);
                    return html;
                } 
            },
            {"data": null,
                "render": function ( data, type, row, meta ) {
                var html='';
                    //html+='<button type="button" class="btn btn-secondary btn-sm" title="Suspender"><i class="fa fa-pause"></i></button>\
                          //<button type="button" class="btn btn-yimexico btn-sm" title="Ver listado actividad: Último"><i class="fa fa-clock-o"></i></button>';
                   html+='<a class="btn" title="Editar" style="color: #212b4c; font-size: 22px;" href="'+base_url+'Clientes/add/'+row.idcliente+'"><i class="fa fa-edit" style="font-size: 22px;"></i></a>\
                          <a class="btn" style="color: #212b4c; font-size: 22px;" title="Cancelar" onclick="modaleliminar('+row.idcliente+')"><i class="fa fa-trash" style="font-size: 22px;"></i></a>';        
                    /*
                    html+='<a class="btn btn-dark btn-sm" title="Editar" style="color: white;" href="'+base_url+'Clientes/add/'+row.idcliente+'"><i class="fa fa-edit"></i></a>\
                          <button type="button" class="btn btn-info btn-sm" title="Cancelar" onclick="modaleliminar('+row.idcliente+')"><i class="fa fa-trash"></i></button>';        
                    */
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });
}
function modaleliminar(id) {
    $('#modal_eliminar').modal();
    $('#idcliente').val(id);
}
function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/updateregistro',
        data:{id:$('#idcliente').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                tabla.ajax.reload();
            }, 1500);
        }
    });     
}

function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"-"+parte[1]+"-"+parte[0];
}
function modalactividad(id) {
    $('#modal_actividad').modal();
    $('.tabla_actividad').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/get_actividades_cliente',
        data:{id:id},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.tabla_actividad').html(data);
        }
    }); 
}