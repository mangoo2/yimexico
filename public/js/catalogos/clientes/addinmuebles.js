$(function () {
    var cont=0;
    $("#valor").on('change',function(){
        if(cont>0){
            //totalmonto = replaceAll($("#valor").val(), ",", "" );
            //totalmonto = replaceAll(totalmonto, "$", "" );
            var comp = $('#valor').val().indexOf("$");
            if(comp==0){
              tot1 = replaceAll($('#valor').val(), ",", "" );
              tot1 = replaceAll(totot1, "$", "" );
              var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#valor').val());
              $('#valor').val(totalg); 
            }else{
              var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#valor').val());
              $('#valor').val(totalg); 
            }
        }else{
            var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($("#valor").val());
            $("#valor").val(monto);    
        }
        cont++;
        //console.log("cont: "+cont);
    });

    $('#form_inmueble_cli').validate({
        rules: {
            tipo_inmueble: "required",
            valor: "required",
            cp: "required",
            calle_avenida: "required",
            num_ext: "required",
            //num_int: "required",
            colonia: "required",
            folio_inmueble: "required"
        },
        messages: {
            tipo_inmueble: "Campo obligatorio",
            valor: "Campo obligatorio",
            cp: "Campo obligatorio",
            calle_avenida: "Campo obligatorio",
            num_ext: "Campo obligatorio",
            //num_int: "Campo obligatorio",
            colonia: "Campo obligatorio",
            folio_inmueble: "Campo obligatorio"
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: base_url+"Clientes/submitInmueble/",
                data: $(form).serialize(),
                beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                },
                success: function (result) {
                    //console.log(result);
                    $("#btn_submit").attr("disabled",false);
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    setTimeout(function () { window.location.href = base_url+"Clientes/catalogoInmuebles" }, 1500);
                }
            });
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });
    $(".campo").on("change",function(){
      preGuardar();
    });
});
function preGuardar(){
    $.ajax({
        type: "POST",
        url: base_url+"Clientes/submitInmueble/",
        data: $('#form_inmueble_cli').serialize(),
        beforeSend: function(){
            $("#btn_submit").attr("disabled",true);
        },
        success: function (result) {
            $("#id").val(result);
            $("#btn_submit").attr("disabled",false);
        }
    });
}

function autoComplete(){
    cp = $("#cp").val(); 
    col = $('#colonia').val(); 
    $('#colonia').select2({
        width: '90%',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione una colonia:',
        ajax: {
            url: base_url+'Perfilamiento/getDatosCPSelect',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp: cp, 
                    col: col ,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.colonia,
                        text: element.colonia,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}


function regresar_view(){
    location.href= base_url+'Clientes/catalogoInmuebles';
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}

function btn_folio_ayuda(){
  $('#modal_folio_ayuda').modal();
}