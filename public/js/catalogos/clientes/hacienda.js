var base_url = $('#base_url').val();
$(document).ready(function() {
	
	getpais('a',1);
    getpais('b',1);
    getpais('p',1);
	var id_axa = $('#idanexoa').val();
	if(id_axa==0){
       addomicilioanexoa();
	}else{
       tabla_direcciones();
	}
    $(".campo").on("change",function(){
      preGuardarA();
    });

    $(".campoB").on("change",function(){
      preGuardarB();
    });
});

function preGuardarA(){
    var form_register = $('#form_anexoa');
    var datos_anexoa = form_register.serialize()+'&'+$('#form_anexoa2').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Alta_hacienda/registro_anexoa',
        data: datos_anexoa,
        statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
        },
        success:function(data){
            var array = $.parseJSON(data);
            if (array.status==1) {
                //swal("Éxito", "Guardado Correctamente", "success");
            }else if (array.status==2) {
                //swal("Éxito", "Actualizado Correctamente", "success");
            }
            var idcli=array.id;
            $("#idanexoa").val(idcli);
            //<!-------------------------------------->
            var DATA  = [];
            var TABLA   = $(".ptext_direccion .text_direccion > .rowsucursal");
            TABLA.each(function(){   
                if ($(this).find("select[id*='vialidad']").val()!='' && $(this).find("input[id*='calle']").val()!='') {
                    item = {};
                    item ["idanexoa"] = idcli;
                    item ["iddireccion"] = $(this).find("input[id*='iddireccion']").val();
                    item ["vialidad"] = $(this).find("select[id*='vialidad']").val();
                    item ["calle"] = $(this).find("input[id*='calle']").val();
                    item ["no_ext"] = $(this).find("input[id*='no_ext']").val();
                    item ["no_int"] = $(this).find("input[id*='no_int']").val();
                    item ["colonia"] = $(this).find("input[id*='colonia']").val();
                    item ["municipio"] = $(this).find("input[id*='municipio']").val();
                    item ["localidad"] = $(this).find("input[id*='localidad']").val();
                    item ["estado"] = $(this).find("input[id*='estado']").val();
                    item ["cp"] = $(this).find("input[id*='cp']").val();
                    item ["idpais"] = $(this).find("select[class*='idpais_pais']").val();
                    item ["idactividad"] = $(this).find("select[class*='idactividad']").val();
                    DATA.push(item);
                }     
            });
            INFO  = new FormData();
            aInfo   = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO, 
                type: 'POST',
                url : base_url + 'Alta_hacienda/registro_direccion_anexoa',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success: function(data){
                }
            }); 
            //<!-------------------------------------->
            /*setTimeout(function(){
                location.href= base_url+'Alta_hacienda';
            }, 1500);*/
        }  
    });
}
function preGuardarB(){
    var form_register = $('#form_producto');
    var datos_anexoa = form_register.serialize();
    $.ajax({
      type:'POST',
      url: base_url+'Alta_hacienda/registro_anexob',
      data: datos_anexoa,
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        var array = $.parseJSON(data);
        $("#idanexob").val(array.id);
        
        if (array.status==1) {
            //swal("Éxito", "Guardado Correctamente", "success");
        }else if (array.status==2) {
            //swal("Éxito", "Actualizado Correctamente", "success");
        }
      }  
    });  
}
function guardo_hacienda() {

	var datos_hacienda = $('#form_hacienda').serialize()+'&actividad_vulnerable='+$('#actividad_vulnerable option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"Alta_hacienda/registrar_hacienda",
        data:datos_hacienda,
        success: function (response){
          //  console.log(response);
            var array = $.parseJSON(response);
            $('#idhacienda').val(array.id);
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
    if($('#proceso_hacienda1').is(':checked')){
       $('.btn_disebled_anexoa').prop('disabled', false); 
       $('.btn_disebled_anexob').prop('disabled', false); 
    }
    if($('#proceso_hacienda2').is(':checked')){
       $('.btn_disebled_anexoa').prop('disabled', false); 
       $('.btn_disebled_anexob').prop('disabled', false); 
    }
}
function guardar_anexoa(){
	var form_register = $('#form_anexoa');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_vulnerable:{
                  required: true
                },
                tipo_fedatario:{
                  required: true
                }

            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = form_register.valid();
        if(valid) {
            var datos_anexoa = form_register.serialize()+'&'+$('#form_anexoa2').serialize();
	        $.ajax({
	          type:'POST',
	          url: base_url+'Alta_hacienda/registro_anexoa',
	          data: datos_anexoa,
	          statusCode:{
	              404: function(data){
	                  swal("Error!", "No Se encuentra el archivo", "error");
	              },
	              500: function(){
	                  swal("Error!", "500", "error");
	              }
	          },
	          success:function(data){
	          	var array = $.parseJSON(data);
		        if (array.status==1) {
		            swal("Éxito", "Guardado Correctamente", "success");
		        }else if (array.status==2) {
		            swal("Éxito", "Actualizado Correctamente", "success");
		        }
		        var idcli=array.id;
	          	//<!-------------------------------------->
	          	var DATA  = [];
		        var TABLA   = $(".ptext_direccion .text_direccion > .rowsucursal");
		        TABLA.each(function(){   
		            if ($(this).find("select[id*='vialidad']").val()!='' && $(this).find("input[id*='calle']").val()!='') {
		                item = {};
		                item ["idanexoa"] = idcli;
		                item ["iddireccion"] = $(this).find("input[id*='iddireccion']").val();
		                item ["vialidad"] = $(this).find("select[id*='vialidad']").val();
		                item ["calle"] = $(this).find("input[id*='calle']").val();
		                item ["no_ext"] = $(this).find("input[id*='no_ext']").val();
		                item ["no_int"] = $(this).find("input[id*='no_int']").val();
		                item ["colonia"] = $(this).find("input[id*='colonia']").val();
		                item ["municipio"] = $(this).find("input[id*='municipio']").val();
		                item ["localidad"] = $(this).find("input[id*='localidad']").val();
		                item ["estado"] = $(this).find("input[id*='estado']").val();
		                item ["cp"] = $(this).find("input[id*='cp']").val();
		                item ["idpais"] = $(this).find("select[class*='idpais_pais']").val();
		                item ["idactividad"] = $(this).find("select[class*='idactividad']").val();
		                DATA.push(item);
		            }     
		        });
		        INFO  = new FormData();
		        aInfo   = JSON.stringify(DATA);
		        INFO.append('data', aInfo);
		        $.ajax({
		            data: INFO, 
		            type: 'POST',
		            url : base_url + 'Alta_hacienda/registro_direccion_anexoa',
		            processData: false, 
		            contentType: false,
		            async: false,
		            statusCode:{
		                404: function(data){
		                    swal("Error!", "No Se encuentra el archivo", "error");
		                },
		                500: function(){
		                    swal("Error!", "500", "error");
		                }
		            },
		            success: function(data){
		            }
		        }); 
	          	//<!-------------------------------------->
                setTimeout(function(){
                    location.href= base_url+'Alta_hacienda';
                }, 1500);
	          }  
	        });  
	        ///
        }
}
function getpais(text,id){
    $('.idpais_'+text+'_'+id).select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Clientes/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
//<!------Agregar nueva direccion------->
function addomicilioanexoa(){
     add_direcc(0,'','','','','','','','','','','',0,'');
}
var aux_direcc=1;
function add_direcc(iddireccion,vialidad,calle,no_ext,no_int,colonia,municipio,localidad,estado,cp,pais,nombre_pais,id_act,actividad){
	  var tablerow='<h4 class="delete_direcc_div_btn_'+aux_direcc+'">Dirección '+aux_direcc+'</h4>';           
         tablerow+='<div class="row delete_direcc_div_btn_'+aux_direcc+'">\
				      <div class="col-md-12" align="right">\
				        <button type="button" class="btn btn_borrar" onclick="delete_direcc('+aux_direcc+','+iddireccion+')"><i class="fa fa-trash"></i> Eliminar sucursal '+aux_direcc+'</button>\
				      </div> \
				    </div>\
					';
         tablerow+='<div class="row rowsucursal delete_direcc_div_btn_'+aux_direcc+'">\
                       <input type="hidden" id="iddireccion" value="'+iddireccion+'">\
					  <div class="col-md-2 form-group">\
					    <label>Tipo de vialidad:</label>\
                        <span class="span_tv_'+aux_direcc+'"></span>\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Calle:</label>\
					    <input class="form-control campo" type="text" id="calle" value="'+calle+'">\
					  </div>\
					  <div class="col-md-2 form-group">\
					    <label>No.ext:</label>\
					    <input class="form-control campo" type="text" id="no_ext" value="'+no_ext+'">\
					  </div>\
					  <div class="col-md-2 form-group">\
					    <label>No.int:</label>\
					    <input class="form-control campo" type="text" id="no_int" value="'+no_int+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Colonia:</label>\
					    <input class="form-control campo" type="text" id="colonia" value="'+colonia+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Municipio o delegación:</label>\
					    <input class="form-control campo" type="text" id="municipio" value="'+municipio+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Localidad:</label>\
					    <input class="form-control campo" type="text" id="localidad" value="'+localidad+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>Estado:</label>\
					    <input class="form-control campo" type="text" id="estado" value="'+estado+'">\
					  </div>\
					  <div class="col-md-3 form-group">\
					    <label>C.P:</label>\
					    <input class="form-control campo" type="number" id="cp" value="'+cp+'">\
					  </div>\
					  <div class="col-md-4 form-group">\
					    <label>País:</label><br>\
					    <select class="form-control campo idpais_d_'+aux_direcc+' idpais_pais">';
							if(pais!=''){
		                     tablerow+='<option value="'+pais+'">'+nombre_pais+'</option>';
							}else{
		                     tablerow+='<option value="MX">MEXICO</option>';
							}   
			 tablerow+='</select>\
			          </div>\
			          <div class="col-md-8 form-group">\
			            <label>Actividad vulnerable realizada en el domicilio:</label>\
			            <select class="form-control campo actividad_'+aux_direcc+' idactividad">';
							if(pais!=0){
		                     tablerow+='<option value="'+id_act+'">'+actividad+'</option>';
							} 
			 tablerow+='</select>\
			          </div>\
			        </div> ';
    $('.text_direccion').append(tablerow);
    get_actividades(aux_direcc);
    getpais('d',aux_direcc);
    tipovialidad(aux_direcc,vialidad);
    aux_direcc++;
}
function delete_direcc(id,iddirecc){
    if (iddirecc==0) {
        $('.delete_direcc_div_btn_'+id).remove();
    }else{
        $('#modal_eliminar_a').modal();
        $('#id_direcion_a').val(iddirecc);
        $('#delete_div_direcc_a').val(id);
    } 
}
function get_actividades(id){
    $.ajax({
        type:'POST',
        url: base_url+"Alta_hacienda/get_actidades",
        success: function (response){
            //console.log(response);
            var array = $.parseJSON(response);
            if (array.length>0) {
                array.forEach(function(element) {
                 //  console.log(element.actividad);
                   $('.actividad_'+id).append('<option value="'+element.id+'">'+element.actividad+'</option>');
                });
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}
function tabla_direcciones(){
    var id_cliente = $('#idcliente').val();
    if(id_cliente>0){
        $.ajax({
            type:'POST',
            url: base_url+"Alta_hacienda/get_direcciones_anexoa",
            data: {idcliente:id_cliente},
            success: function (response){
               // console.log(response);
                var array = $.parseJSON(response);
             //  console.log(array);
               if (array.length>0) {
                    array.forEach(function(element) {
                        add_direcc(element.iddireccion,element.vialidad,element.calle,element.no_ext,element.no_int,element.colonia,element.municipio,element.localidad,element.estado,element.cp,element.idpais,element.pais,element.idactividad,element.actividad);
                   });
               }   
            },
            error: function(response){
                swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
            }
        });
    }
}
function eliminar_anexoa(){
    $.ajax({
        type:'POST',
        url: base_url+'Alta_hacienda/updateregistro_anexo_a',
        data:{id:$('#id_direcion_a').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar_a').modal('hide');
                $('.delete_direcc_div_btn_'+$('#delete_div_direcc_a').val()).remove();
            }, 1500);
        }
    });     
}
function check_direccion_a(){
    if($('#check_direccion_a').is(':checked')){
        $.ajax({
            type:'POST',
            url: base_url+'Alta_hacienda/check_direccion__cliente',
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $('#vialidad').val(array.vialidad);
                $('#calle').val(array.calle);
                $('#no_ext').val(array.no_ext);
                $('#no_int').val(array.no_int);
                $('#colonia').val(array.colonia);
                $('#municipio').val(array.municipio);
                $('#localidad').val(array.localidad);
                $('#estado').val(array.estado);
                $('#cp').val(array.cp);
                $(".idpais_pais").append('<option value="'+array.idpais+'" selected>'+array.pais+'</option>');
            }
        });     
    }else{
        $('#vialidad').val("");
        $('#calle').val("");
        $('#no_ext').val("");
        $('#no_int').val("");
        $('#colonia').val("");
        $('#municipio').val("");
        $('#localidad').val("");
        $('#estado').val("");
        $('#cp').val("");
        $(".idpais_pais").val('');
    }
}
function guardar_anexob(){
    var form_register = $('#form_producto');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nombre:{
                  required: true
                },
                apellido_paterno:{
                  required: true
                },
                apellido_materno:{
                  required: true
                }

            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = form_register.valid();
        if(valid) {
            var datos_anexoa = form_register.serialize();
            $.ajax({
              type:'POST',
              url: base_url+'Alta_hacienda/registro_anexob',
              data: datos_anexoa,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              success:function(data){
                var array = $.parseJSON(data);
                if (array.status==1) {
                    swal("Éxito", "Guardado Correctamente", "success");
                }else if (array.status==2) {
                    swal("Éxito", "Actualizado Correctamente", "success");
                }
                //<!-------------------------------------->
                setTimeout(function(){
                    location.href= base_url+'Alta_hacienda';
                }, 1500);
              }  
            });  
            ///
        }
}
function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}
function reft_alta_hacienda(){
    location.href= base_url+'Alta_hacienda';
}
function select_actividade(){
    var id = $('#actividad_vulnerable option:selected').val();
    if(id!=0){
       $('.div_hacienda').css('display','block');    
    }
    $.ajax({
        type:'POST',
        url: base_url+"Alta_hacienda/get_hacienda",
        data: {idactividad:id},
        success: function (response){
          //  console.log(response);
            var array = $.parseJSON(response);
            if(array.idhacienda==0){
                $('#idhacienda').val(0);
                $('#proceso_hacienda1').prop("checked", false);  
                $('#proceso_hacienda2').prop("checked", false);  
                $('#anexo_a1').prop("checked", false);  
                $('#anexo_a2').prop("checked", false);  
                $('#anexo_b1').prop("checked", false);  
                $('#anexo_b2').prop("checked", false);  
                $('.btn_disebled_anexoa').prop('disabled', true); 
                $('.btn_disebled_anexob').prop('disabled', true); 
            }else{
                $('#idhacienda').val(array.idhacienda);
                if(array.proceso_hacienda==1){
                   $('#proceso_hacienda1').prop("checked", true);  
                }else if(array.proceso_hacienda==2){
                   $('#proceso_hacienda2').prop("checked", true);  
                }
                if(array.anexo_a==1){
                   $('#anexo_a1').prop("checked", true);  
                }else if(array.anexo_a==2){
                   $('#anexo_a2').prop("checked", true);  
                }
                if(array.anexo_b==1){
                   $('#anexo_b1').prop("checked", true);  
                }else if(array.anexo_b==2){
                   $('#anexo_b2').prop("checked", true);  
                }
                $('.btn_disebled_anexoa').prop('disabled', false); 
                $('.btn_disebled_anexob').prop('disabled', false); 
            }

        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}
function heft_anexoa(){
    var id = $('#actividad_vulnerable option:selected').val();
    location.href= base_url+'Alta_hacienda/anexo_a/'+id;
}
function heft_anexob(){
    var id = $('#actividad_vulnerable option:selected').val();
    location.href= base_url+'Alta_hacienda/anexo_b/'+id;
}
function tipovialidad(id,nombre){
    $.ajax({
      type:'POST',
      data: {id_t:id},
      url: base_url+'Alta_hacienda/get_tipo_vialidad',
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        $('.span_tv_'+id).html(data);
        $('.span_ptv_'+id+' option[value="'+nombre+'"]').attr("selected", "selected");
      }  
    });
}
function regresar_view(){
    window.history.back();
}