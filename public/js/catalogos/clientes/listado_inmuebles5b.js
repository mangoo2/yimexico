var base_url = $('#base_url').val();
$(document).ready(function($) {
    tabla=$("#table_inmueble").DataTable({
        "ajax": {
           "url": base_url+"Clientes/data_records_inmuebles5b",
           type: "post",
            error: function(){
               $("#table_inmueble").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var tipo='';
                    if(row.tipo_desarrollo=="1"){
                       tipo="Habitacional";
                    }else if(row.tipo_desarrollo=="2"){
                       tipo="Comercial";
                    }else if(row.tipo_desarrollo=="3"){
                       tipo="Oficinas";
                    } else if(row.tipo_desarrollo=="4"){
                       tipo="Industrial";
                    }else if(row.tipo_desarrollo=="5"){
                       tipo="Mixto (Habitacional y Comercial)";
                    }else if(row.tipo_desarrollo=="99"){
                       tipo="Otro";
                    }      
                    return tipo;
                }
            },
            {"data": null,
                "render": function (data, type, row, meta ) {
                    var valor=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto_desarrollo)
                    return valor;
                } 
            },
            {"data": null,
                "render": function (data, type, row, meta ) {
                    var dir=row.calle+", Colonia "+row.colonia+" C.P. "+row.codigo_postal;
                    return dir;
                } 
            },
            {"data": "unidades_comercializadas"},
            {"data": null,
                "render": function ( data, type, row, meta ) {
                var html='<button type="button" class="btn edit" style="color: #212b4c;" title="Editar"><i class="fa fa-edit" style="font-size: 22px;"></i></a>\
                          <button type="button" class="btn delete" style="color: #212b4c;" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button>';        
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });

    $('#table_inmueble').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var row = tabla.row(tr);
        var data = row.data();
        window.location = base_url+'Clientes/addInmueble5b/'+data.id;
    });
    $('#table_inmueble').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        modaleliminar(data.id);
    }); 

    $("#eliminar").on("click",function(){
        eliminar();
    });

});

function modaleliminar(id) {
    $('#modal_eliminar').modal();
    $('#id_inmueble').val(id);
}
function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/deleteInmueble5b',
        data:{id:$('#id_inmueble').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                tabla.ajax.reload();
            }, 1500);
        }
    });     
}

