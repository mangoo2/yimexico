$(document).ready(function(){
	pais_aux(3);
	var idp = $("#idtipo_cliente").val();
	var idt = $("#tipoc").val();
	var idc = $("#id_clientec").val();
  var ido = $("#idopera").val();
  var col="";
  var cp="";
  var col2="";
  var cp2="";

	$('#form_beneficiario_fisica').validate({
	      rules: {
	          nombre: "required",
	          apellido_paterno: "required",
	          apellido_materno: "required",
	          nombre_identificacion: "required",
	          autoridad_emite: "required",
	          numero_identificacion: "required",
	          genero: "required",
	          pais_nacimiento: "required",
	          pais_nacionalidad: "required",
	          calle: "required",
	          no_ext: "required",
	          colonia: "required",
	          monicipio: "required",
	          localidad: "required",
	          estado: "required",
	          cp: "required",
            r_f_c_d:{
              rfc: true
            },
            /*curp_d:{
              curp: true
            },*/
	      },
	      messages: {
	          nombre: "Campo requerido",
	          apellido_paterno: "Campo requerido",
	          apellido_materno: "Campo requerido",
	          nombre_identificacion: "Campo requerido",
	          autoridad_emite: "Campo requerido",
	          numero_identificacion: "Campo requerido",
	          genero: "Campo requerido",
	          pais_nacimiento: "Campo requerido",
	          pais_nacionalidad: "Campo requerido",
	          calle: "Campo requerido",
	          no_ext: "Campo requerido",
	          colonia: "Campo requerido",
	          monicipio: "Campo requerido",
	          localidad: "Campo requerido",
	          estado: "Campo requerido",
	          cp: "Campo requerido",
	      },
	      submitHandler: function (form) {
          var trantadose_persona="";
          if($("#lugar_recidencia").is(":checked")==true)
            trantadose_persona="on";
          else
            trantadose_persona="off";

          var estado_d="";
          estado_d = $("#estado_d option:selected").val();
	         $.ajax({
	             type: "POST",
	             url: base_url+"index.php/Clientes_c_beneficiario/guardar_beneficiario_fisica",
	             data: $(form).serialize()+"&trantadose_persona="+trantadose_persona+"&estado_d="+estado_d,
	             beforeSend: function(){
	                $("#btn_submit").attr("disabled",true);
	             },
	             success: function (result) {
	                console.log(result);
	                $("#btn_submit").attr("disabled",false);
	                swal("Éxito!", "Se han realizado los cambios correctamente", "success");
	                //setTimeout(function () { window.location.href = base_url+"Clientes_c_beneficiario/beneficiarios/"+idp+"/"+idt+"/"+idc }, 1500);
                  //////////////////////////////
                  id = result;
                  idc = $("#id_clientec").val();
                  idp = $("#idp").val();
                  tipoc = $("#tipoc").val();
                  idopera = $("#idopera").val();
                  idcc = $("#idcc").val();
                  id_union = $("#id_union").val();
                  window.open(base_url+'Clientes_c_beneficiario/generarPDF_BeneFisica/'+id+"/"+idc+"/"+idp+"/"+tipoc+"/"+idopera+"/"+idcc+"/"+id_union);
                  ///////////////////////////////////
                  if(ido==0){
                    setTimeout(function() { history.back() }, 2500);
                  }else{
                    //setTimeout(function() { window.location.href = base_url+"Operaciones/procesoInicial/"+ido }, 1500); 
                    setTimeout(function() { history.back() }, 2500);
                  }
	             }
	         });
	         return false; // required to block normal submit for ajax
	       },
	      errorPlacement: function(label, element) {
	        label.addClass('mt-2 text-danger');
	        label.insertAfter(element);
	      },
	      highlight: function(element, errorClass) {
	        $(element).parent().addClass('has-danger')
	        $(element).addClass('form-control-danger')
	        }
  	});
  getpais2(3);
  $("#nombre_ejec_imp").text($(".nav-profile-name").text());

  if($('#lugar_recidencia').is(':checked')){
      $('.residenciaEx').css('display','block');
  }else{
    $('.residenciaEx').css('display','none'); 
  }
  cambiaCP("cp");
  cambiaCP2("cp_d");
  autoComplete("colonia");
  autoComplete2("colonia_d");
});

function imprimir_formato1(){
  /*$('.firma_tipo_cliente').css('display','block');
  ////////////////////////////
  //$("#nombre_cli_imp").text($("#nombre").val()+" "+$("#apellido_paterno").val()+" "+$("#apellido_materno").val());
  ///////////////////////////
  setTimeout(function(){ 
    $('.firma_tipo_cliente').css('display','none');
  }, 500);
  window.print();*/
  /*id = $("#id").val();
  idc = $("#id_clientec").val();
  idp = $("#idp").val();
  tipoc = $("#tipoc").val();
  idopera = $("#idopera").val();
  idcc = $("#idcc").val();
  id_union = $("#id_union").val();
  window.open(base_url+'Clientes_c_beneficiario/generarPDF_BeneFisica/'+id+"/"+idc+"/"+idp+"/"+tipoc+"/"+idopera+"/"+idcc+"/"+id_union);*/
  $('#modal_fecha').modal('show');
}

function imprimir_formato(){
  fechaf = $('#fecha_f').val();
  id = $("#id").val();
  idc = $("#id_clientec").val();
  idp = $("#idp").val();
  tipoc = $("#tipoc").val();
  idopera = $("#idopera").val();
  idcc = $("#idcc").val();
  id_union = $("#id_union").val();
  window.open(base_url+'Clientes_c_beneficiario/generarPDF_BeneFisica/'+id+"/"+idc+"/"+idp+"/"+tipoc+"/"+idopera+"/"+idcc+"/"+id_union+"/"+fechaf);
}


function lugar_recidencia_btn_paso3() {
	if($('#lugar_recidencia').is(':checked')){
      $('.residenciaEx').css('display','block');
	}else{
	  $('.residenciaEx').css('display','none');	
	}
}

function getpais(id){
	//console.log("get pais");
  $('.pais_'+id).select2({
      width: 255,
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Clientes_cliente/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
function getpais2(id){
	//console.log("pais2");
    $('.pais_'+id).select2({
        width: 255,
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Clientes_cliente/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var pais = $('.pais_'+id+' option:selected').val(); 
      if(pais=='MX'){
       //<!-------->
       //console.log(pais); 
        $.ajax({
          type:'POST',
          data: {id:id},
          url: base_url+'Clientes_cliente/get_estadoMoral',
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
            //console.log(id);
            $('.span_div_'+id).html(data);
            //console.log(data);
            //$('.estado'+id+' option[value=""]').attr("selected", "selected");
          }  
        });
       //<!-------->   
      }else{
        $('.span_div_'+id).html('<input class="form-control" type="text" name="estado" id="estado">'); 
      }
      
    });
}

function pais_aux(id){
  var pais = $('.pais_'+id+' option:selected').val();       
  if(pais=='MX'){
   //<!-------->
    $.ajax({
      type:'POST',
      data: { id: id, edo: $("#edo").val()},
      url: base_url+'Clientes_c_beneficiario/get_estadoFisica',
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        $('.span_div_'+id).html(data);
      }  
    });
   //<!-------->   
  }else{
    $('.span_div_'+id).html('<input class="form-control" type="text" name="estado" id="estado" value="'+$('#edo').val()+'">'); 
  }
}

function rfcValido(rfc, aceptarGenerico = true) {
    //const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
    const re = /^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){2,3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))$/;
    var   validado = rfc.match(re);

    if (!validado)  //Coincide con el formato general del regex?
        return false;

    //Separar el dígito verificador del resto del RFC
    const digitoVerificador = validado.pop(),
          rfcSinDigito      = validado.slice(1).join(''),
          len               = rfcSinDigito.length,

    //Obtener el digito esperado
          diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
          indice            = len + 1;
    var   suma,
          digitoEsperado;

    if (len == 12) suma = 0
    else suma = 481; //Ajuste para persona moral

    for(var i=0; i<len; i++)
        suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
    digitoEsperado = 11 - suma % 11;
    if (digitoEsperado == 11) digitoEsperado = 0;
    else if (digitoEsperado == 10) digitoEsperado = "A";

    //El dígito verificador coincide con el esperado?
    // o es un RFC Genérico (ventas a público general)?
    if ((digitoVerificador != digitoEsperado)
     && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
        return false;
    else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
        return false;
    return rfcSinDigito + digitoVerificador;
}

function validarInput(input) {
    var rfc         = input.value.trim().toUpperCase(),
        resultado   = document.getElementById("resultado"),
        valido;
    
    if($(".rfc").val()!=""){
      $("#resultado").show();
      var rfcCorrecto = rfcValido(rfc);   // ⬅️ Acá se comprueba
    
      if (rfcCorrecto) {
        valido = "Válido";
        resultado.classList.add("ok");
      } else {
        valido = "No válido"
        resultado.classList.remove("ok");
      }
      resultado.innerText ="Formato: " + valido;
    }else{
      $("#resultado").hide();
    }
  }  
  function c_beneficiarios(){
      window.history.back();
  }  


function cambiaCP(id){
  //console.log("id: "+id);
  cp = $("#"+id+"").val();  
  //console.log("cp: "+cp);
  if(cp!=""){
      $.ajax({
          url: base_url+'Perfilamiento/getDatosCP',
          type: 'POST',
          data: { cp: cp, col: "0" },
          success: function(data){ 
            var array = $.parseJSON(data);
            /*console.log("estado: "+array[0].estado);
            console.log("mnpio: "+array[0].mnpio);
            console.log("ciudad: "+array[0].ciudad);*/
            $("#estado").val(array[0].id_estado);
            $("#edo").val(array[0].id_estado);
            $("#estado").attr("disabled",true);
            $("#municipio").val(array[0].mnpio.toUpperCase());
          }
      });
  }else{
    $("#estado").val("");
    $("#estado").attr("disabled",false);
    $("#municipio").val("");
  }
    //});  
}

function autoComplete(id){
    //console.log("id: "+id);
    col = $('#'+id+'').val();
    //console.log("col: "+col);
    
    $('#'+id+'').select2({
        width: '90%',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione una colonia:',
        ajax: {
            url: base_url+'Perfilamiento/getDatosCPSelect',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp: cp, 
                    col: col ,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.colonia,
                        text: element.colonia,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

}
function cambiaCP2(id){
  //console.log("id: "+id);
  cp2 = $("#"+id+"").val();  
  //console.log("cp: "+cp2);
  if(cp2!=""){
      $.ajax({
          url: base_url+'Perfilamiento/getDatosCP',
          type: 'POST',
          data: { cp: cp2, col: "0" },
          success: function(data){ 
            var array = $.parseJSON(data);
            $("#estado_d").val(array[0].id_estado);
            $("#estado_d").attr("disabled",true);
            $("#municipio_d").val(array[0].mnpio.toUpperCase());
          }
      });
  }else{
    $("#estado_d").val("");
    $("#estado_d").attr("disabled",false);
    $("#municipio_d").val("");
  }
    //});  
}

function autoComplete2(id){
    //console.log("id: "+id);
    col2 = $('#'+id+'').val();
    
    $('#'+id+'').select2({
        width: '90%',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione una colonia:',
        ajax: {
            url: base_url+'Perfilamiento/getDatosCPSelect',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp: cp2, 
                    col: col2 ,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.colonia,
                        text: element.colonia,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

}