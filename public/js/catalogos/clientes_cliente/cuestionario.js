$(document).ready(function(){
  btn_cuenta_alguna_otra_fuente_ingresos();
  btn_recibe_ingresos_extranjero();
  btn_mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjera();
  btn_usted_casado_vive_concubinato();
  btn_tiene_dependientes_economicos();
  getpais(4);

  /*
	$('#cont_pep_extra').addClass('animated bounceOutLeft');

  	$("#accionista_pep").on("click",function(){
  		$("#cont_pep").show();
  		$('#cont_pep').removeClass('animated bounceOutLeft');
  		$('#cont_pep').addClass('animated bounceInLeft');
  	});
  	$("#accionista_pep2").on("click",function(){
  		$('#cont_pep').removeClass('animated bounceInLeft');
  		$('#cont_pep').addClass('animated bounceOutLeft');
  		setTimeout(function () { $("#cont_pep").hide(); }, 1000);  		
  	});

  	$("#otro_accionista_pep").on("click",function(){
  		$("#cont_pep_extra").show();
  		$('#cont_pep_extra').removeClass('animated bounceOutLeft');
  		$('#cont_pep_extra').addClass('animated bounceInLeft');
  	});
  	$("#otro_accionista_pep2").on("click",function(){
  		$('#cont_pep_extra').removeClass('animated bounceInLeft');
  		$('#cont_pep_extra').addClass('animated bounceOutLeft');
  		setTimeout(function () { $("#cont_pep_extra").hide(); }, 1000);
  	});

  	if($("#accionista_pep").is(":checked")==true){
  		$("#cont_pep").show();
  		$('#cont_pep').removeClass('animated bounceOutLeft');
  		$('#cont_pep').addClass('animated bounceInLeft');
  	}
  	if($("#accionista_pep2").is(":checked")==true){
  		$('#cont_pep').removeClass('animated bounceInLeft');
  		$('#cont_pep').addClass('animated bounceOutLeft');
  		setTimeout(function () { $("#cont_pep").hide(); }, 1000);
  	}

  	if($("#otro_accionista_pep").is(":checked")==true){
  		$("#cont_pep_extra").show();
  		$('#cont_pep_extra').removeClass('animated bounceOutLeft');
  		$('#cont_pep_extra').addClass('animated bounceInLeft');
  	}
  	if($("#otro_accionista_pep2").is(":checked")==true){
  		$('#cont_pep_extra').removeClass('animated bounceInLeft');
  		$('#cont_pep_extra').addClass('animated bounceOutLeft');
  		setTimeout(function () { $("#cont_pep_extra").hide(); }, 1000);
  	}

  	var form_register = $('#form_cuestionario');
   	var error_register = $('.alert-danger', form_register);
  	var success_register = $('.alert-success', form_register);
	  var $validator1=form_register.validate({
		errorElement: 'div', //default input error message container
      	errorClass: 'vd_red', // default input error message class
      	focusInvalid: false, // do not focus the last invalid input
      	ignore: "",
	    rules: {
          descrip_nego: "required",
          ingreso_anual: "required",
          rfc:{
            rfc: true
          },
          rfc_esposo:{
            rfc: true
          },
          rfc_ext:{
            rfc: true
          },
          rfc_esposo_ext:{
            rfc: true
          }
      	},
      	messages: {
          forma_pago: "Campo requerido",
          ingreso_anual: "Campo requerido",
      	},

	    submitHandler: function (form) {
	    	$("#edo").attr("disabled",false);
	    	$("#edo_ext").attr("disabled",false);
	        $.ajax({
	             type: "POST",
	             url: base_url+"Clientes_cliente/guardar_cuestionario",
	             data: $(form).serialize(),
	             beforeSend: function(){
	                $("#btn_submit").attr("disabled",true);
	             },
	             success: function (result) {
	                console.log(result);
	                $("#edo").attr("disabled",true);
	                $("#edo_ext").attr("disabled",true);
	                $("#btn_submit").attr("disabled",false);
	                swal("Éxito!", "Se han guardado los datos correctamente", "success");
	                setTimeout(function () { history.back(); }, 1500);
	             }
	        });
	        return false; // required to block normal submit for ajax
	    },
	    errorPlacement: function(error, element) {
          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
      }, 
      
      invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(500);
              error_register.fadeIn(500);
              scrollTo(form_register,-100);

      },

      highlight: function (element) { // hightlight error inputs
  
          $(element).addClass('vd_bd-red');
          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

      },

      unhighlight: function (element) { // revert the change dony by hightlight
          $(element)
              .closest('.control-group').removeClass('error'); // set error class to the control group
      },

      success: function (label, element) {
          label
              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
          $(element).removeClass('vd_bd-red');
      }
  	});
  	
   */

    $('.propietario_empresa').click(function(event){
        propietario_empresa();
    });
    $('.trabajador_autonomo').click(function(event){
        trabajador_autonomo();
    });
    $('.es_empleado').click(function(event){
        es_empleado();
    });
    $('.riquezas_otras').click(function(event){
        riquezas_otras();
    });

    $('#cuantos').change(function(event){
        cuantos_dependientes();
    });

    if($("#id").val()>0){
        setTimeout(function () { 
            propietario_empresa();
            trabajador_autonomo();
            es_empleado();
            riquezas_otras();
        }, 1000);
    }

});

function propietario_empresa(){
    var pre=$('input:radio[name=propietario_empresa]:checked').val();  //si
    if(pre==1){
        $("#princ2").hide();
        $("#princ3").hide();
        $("#princ4").hide();
        $("#div_cont_riqueza").hide();
        $("input:radio[name=otras_actividades_generadoras_recurso]").prop("checked",false);
    }else if(pre==2 || pre==undefined){
        $("#princ2").show();
        $("#princ3").show();
        $("#princ4").show();
    }

    if($("input[name=propietario_empresa]:checked").val()==2){ //no
        $("#cont_porcs").hide();
    }
    else if($("input[name=propietario_empresa]:checked").val()==1){
        $("#cont_porcs").show();
    }

}
function trabajador_autonomo(){
    var pre2=$('input:radio[name=trabajador_autonomo]:checked').val();  //si
    if(pre2==1){
        $("#princ3").hide();
        $("#princ4").hide();
        $("#div_cont_riqueza").hide();
        $("input:radio[name=otras_actividades_generadoras_recurso]").prop("checked",false);
    }else if(pre2==2 || pre2==undefined){
        $("#princ3").show();
        $("#princ4").show();
    }
}
function es_empleado(){
    var pre3=$('input:radio[name=es_empleado]:checked').val();  //si
    if(pre3==1){
        $("#princ4").hide();
        $("#div_cont_riqueza").hide();
        $("input:radio[name=otras_actividades_generadoras_recurso]").prop("checked",false);
    }else if(pre3==2 || pre3==undefined){
        $("#princ4").show();
    }
}
function riquezas_otras(){
    var otras_act=$('input:radio[name=otras_actividades_generadoras_recurso]:checked').val();
    if(otras_act==1 || otras_act==2 || otras_act==3 || otras_act==4)
        $("#div_cont_riqueza").show();
}
var cont_tabla=0;
function cuantos_dependientes(){
    var cant_depen = $("#cuantos").val();
    if(Math.sign(cant_depen)==-1){
        swal("Advertencia!", "Ingresar un número positivo", "error");
        $("#cuantos").val('');
    }
    /*if(cant_depen==0){
        swal("Advertencia!", "Ingresar un numero mayor a 0", "error");
        $("#cuantos").val('');
    }*/
    /*if(cant_depen==1){
        $("#otros_dependientes").html('');
    }*/

    if($("#id").val()>0){
        cont_tabla = $(".row_otros_dependientes .datos_otros > div").length;
    }
    var cant_depen_ant=0; 
    var cont_row=0;
    if(cant_depen>=1){
        var html_dep='';
        //cant_depen=cant_depen-1;
        //cant_depen = cant_depen-cant_depen_ant;
        
        if(cont_tabla>1 && cont_tabla<=1){
            cant_depen = cant_depen-cont_tabla-1;
        }
        else{
            cant_depen = cant_depen-cont_tabla;
        }
        /*if(Math.sign(cant_depen)==-1){
            nega = (-1);
            cant_depen = cant_depen * nega;
        }
        if(cont_tabla>cant_depen){
            cant_depen = cont_tabla-cant_depen;
        }*/

        //console.log("cant_depen: "+cant_depen);
        //console.log("cont_tabla: "+cont_tabla);
        for(c=0; c<cant_depen; c++){
            cont_row++;
            html_dep+='<!--<form class="form" method="post" role="form" id="form_cuestionario_otros" required>-->\
                <div class="row" id="row_nvo'+cont_row+'">\
                <input class="form-control" type="hidden" id="id" value="0">\
                <div class="col-md-3 form-group">\
                  <label>Parentesco</label>\
                  <input class="form-control" type="text" id="parentesco">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Nombre Completo</label>\
                  <input class="form-control" type="text" id="nombre_ompleto">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Género</label>\
                  <select class="form-control" id="genero">\
                    <option value="1">Masculino</option>\
                    <option value="2">Femenino</option>\
                  </select>\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Ocupación</label>\
                  <input class="form-control" type="text" id="ocupacion">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label style="color:transparent">eliminar</label>\
                  <button type="button" class="btn btn_borrar" onclick="eliminaDependiente('+cont_row+')"><i class="fa fa-minus"></i> </button>\
                </div>\
              </div>\
              <!--</form>-->';
        }
        $(".datos_otros").append(html_dep);
        //cant_depen_ant = cant_depen+1;
        cont_tabla = $(".row_otros_dependientes .datos_otros > div").length;   
        //console.log("cant_depen_ant: "+cant_depen_ant);
    }
}

function eliminaDependiente(num_row){
    $('#row_nvo'+num_row).remove();
    $("#cuantos").val($(".row_otros_dependientes .datos_otros > div").length);
}

function eliminaDependienteId(id){
    swal({
        title: "¿Desea eliminar este registro?",
        text: "Se eliminará del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            url: base_url+'Clientes_cliente/eliminarOtroDepend',
            type: 'POST',
            data: { id: id},
            success: function(data){ 
                $('#row_'+id).remove();
            }
        });
    });
    
}

function guardar_form_cuestionario(){
    var form_register = $('#form_cuestionario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules:{
          descrip_nego: "required",
          propietario_empresa: "required",
          //trabajador_autonomo: "required",
          //es_empleado: "required",
          //otras_actividades_generadoras_recurso: "required",
          cuenta_alguna_otra_fuente_ingresos: "required",
          recibe_ingresos_extranjero: "required",
          //efectivo: "required",
          //transfer_otro: "required",
          //transfer_inter: "required",
          //tarjeta: "required",
          //cheques: "required",
          usted_casado_vive_concubinato: "required",
          //edad: "required",
          //nacionalidad: "required",
          tiene_dependientes_economicos: "required",
          //apoyo_conyugal_parental_pareja: "required",
          //apoyo_gubernamental: "required",
          //herencia_donacion: "required",
          //fondo_fiduciario: "required",
          //prestamo_subvencion: "required",
          //inversiones: "required"
        },
        errorPlacement: function(error, element) {
          if ( element.is(":radio") ) {
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_cuestionario").valid();

    var otros_dep=$('input:radio[name=tiene_dependientes_economicos]:checked').val();
    if(otros_dep==1){
        var form_register2 = $('#form_cuestionario_otros');
        var error_register = $('.alert-danger', form_register2);
        var success_register = $('.alert-success', form_register2);
        var $validator1=form_register2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules:{
              parentesco: "required",
              nombre: "required",
              genero: "required",
              fecha_nacimiento: "required",
              ocupacion: "required"
            },
            errorPlacement: function(error, element) {
              if ( element.is(":radio") ) {
                  element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                  error.insertAfter(element.parent());
              }else {
                  error.insertAfter(element);
              }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register2,-100);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //var valid2 = $("#form_cuestionario_otros").valid();
    }
    var valid = $("#form_cuestionario").valid();
    if(valid){
      //modal_imprimir();
      guardar_form_cuestionario2();
    }
    /*if(valid && otros_dep==2){
      modal_imprimir();
    }
    if(otros_dep==1 && valid && valid2){
      modal_imprimir();
    }*/
}

function modal_imprimir(){
    $('#modalimprimir').modal();
}
function guardar_form_cuestionario2(){
  $('.btn_registro').attr('disabled',true);
    var datos = $('#form_cuestionario').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Clientes_cliente/add_cuestionario',
        data: datos,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            guardaOtrosDependientes(data);
            swal("Éxito", "Actualizado Correctamente", "success");
            idcc = $("#idclientec").val();
            idp = $("#idperfilamiento").val();
            tipo = $("#tipoc").val();
            ido = $("#id_operacion").val();
            idcgr = $("#idcgr").val();
            window.open(base_url+'Clientes_cliente/generarPDF_CA/'+idcc+"/"+idp+"/"+tipo+"/"+ido+"/"+idcgr+"/1", '_blank');
            setTimeout(function () { 
                location.href= base_url+'Operaciones/gradoRiesgo/'+$("#id_operacion").val()+"/2/"+$("#idperfilamiento").val();
            }, 2000);
        }
    });
}

function guardaOtrosDependientes(id){
    var DATAP= [];
    if($(".row_otros_dependientes .datos_otros > div").length>0){
        var TABLAP = $(".row_otros_dependientes .datos_otros > div");                  
        TABLAP.each(function(){         
            item = {};
            item ["id"]=$(this).find("input[id*='id']").val();
            item ["id_cuest_amp"]=id;
            item ["parentesco"]=$(this).find("input[id*='parentesco']").val();
            item ["nombre"]=$(this).find("input[id*='nombre']").val();
            item ["genero"]=$(this).find("select[id*='genero'] option:selected").val();
            item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento']").val();
            item ["ocupacion"] = $(this).find("input[id*='ocupacion']").val();
            DATAP.push(item);
        });
        INFOP  = new FormData();
        aInfop   = JSON.stringify(DATAP);
        INFOP.append('data', aInfop);
        $.ajax({
            data: INFOP,
            type: 'POST',
            url : base_url+'Clientes_cliente/insert_otros_dependientes',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data2){
                    //toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    //toastr.error('Error', '500');
                }
            },
            success: function(data2){
              
            }
        }); 
    }
}


function btn_cuenta_alguna_otra_fuente_ingresos(){
  var pre=$('input:radio[name=cuenta_alguna_otra_fuente_ingresos]:checked').val();
  if(pre==1){
    $('.text_cuenta_alguna_otra_fuente_ingresos').css('display','block');
  }else{
    $('.text_cuenta_alguna_otra_fuente_ingresos').css('display','none');
  }
}

function btn_recibe_ingresos_extranjero(){
  var pre=$('input:radio[name=recibe_ingresos_extranjero]:checked').val();
  if(pre==1){
    $('.texto_recibe_ingresos_extranjero').css('display','block');
  }else{
    $('.texto_recibe_ingresos_extranjero').css('display','none');
  }
}

function btn_mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjera(){
  var pre=$('input:radio[name=mantiene_relaciones_comerciales_dependencia_entidad_gubernamenta]:checked').val();
  if(pre==1){
    $('.text_mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjera').css('display','block');
  }else{
    $('.text_mantiene_relaciones_comerciales_dependencia_entidad_gubernamental_nacional_extranjera').css('display','none');
  }
}

function btn_usted_casado_vive_concubinato(){
  var pre=$('input:radio[name=usted_casado_vive_concubinato]:checked').val();
  if(pre==1){
    $('.text_usted_casado_vive_concubinato').css('display','block');
  }else{
    $('.text_usted_casado_vive_concubinato').css('display','none');
  }
}

function btn_tiene_dependientes_economicos(){
  var pre=$('input:radio[name=tiene_dependientes_economicos]:checked').val();
  if(pre==1){
    $('.txto_tiene_dependientes_economicos').css('display','block');
    if($("#id").val()<=0)
        $("#cuantos").val('');
  }else{
    $('.txto_tiene_dependientes_economicos').css('display','none');
    if($("#id").val()<=0){
        $("#cuantos").val('');
    }
    $(".datos_otros").html("");
    cont_tabla=0;
  }
}

/*
function imprimir_formato1(){
  $('.firma').css('display','block');
  setTimeout(function(){ 
    $('.firma').css('display','none');
  }, 500);
  window.print();
}
*/
/*
function cambiaCP(id){
   	console.log("id: "+id);
    if(id=="cp")
    	cp = $("#cp").val();  
    else
    	cp = $("#cp_ext").val();  
    //console.log("cp: "+cpb);
    if(cp!=""){
      $.ajax({
          url: base_url+'Perfilamiento/getDatosCP',
          type: 'POST',
          data: { cp: cp, col: "0" },
          success: function(data){ 
            var array = $.parseJSON(data);
            //console.log(array[0].id_estado);
            if(id=="cp"){
	            $("#edo").val(array[0].id_estado);
	            $("#ciudad").val(array[0].ciudad.toUpperCase());
	            $("#municipio").val(array[0].mnpio.toUpperCase());
	        }else{
	        	$("#edo_ext").val(array[0].id_estado);
	            $("#ciudad_ext").val(array[0].ciudad.toUpperCase());
	            $("#municipio_ext").val(array[0].mnpio.toUpperCase());
	        }
          }
      });
    }else{
    	if(cp=="cp"){
	      	$("#edo").val();
	        $("#ciudad").val();
	        $("#municipio").val();
	    }else{
	    	$("#edo_ext").val();
	        $("#ciudad_ext").val();
	        $("#municipio_ext").val();	
	    }
    }
    //});  
}
*/
/*
function autoComplete(id){
	console.log("id colonia: "+id);
	if(id=="colonia"){
	    cp = $("#cp").val(); 
	    col = $('#colonia').val();
	    $("#colonia").select2({
	        width: 'resolve',
	        minimumInputLength: 0,
	        minimumResultsForSearch: 10,
	        placeholder: 'Seleccione una colonia:',
	        ajax: {
	            url: base_url+'Perfilamiento/getDatosCPSelect',
	            dataType: "json",
	            data: function(params) {
	                var query = {
	                    search: params.term,
	                    cp: cp, 
	                    col: col ,
	                    type: 'public'
	                }
	                return query;
	            },
	            processResults: function(data) {
	                var itemscli = [];
	                data.forEach(function(element) {
	                    itemscli.push({
	                        id: element.colonia,
	                        text: element.colonia,
	                    });
	                });
	                return {
	                    results: itemscli
	                };
	            },
	        }
	    }); 
	}else{
		cp = $("#cp_ext").val(); 
	    col = $('#colonia_ext').val(); 	
	    $("#colonia_ext").select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione una colonia:',
        ajax: {
            url: base_url+'Perfilamiento/getDatosCPSelect',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp: cp, 
                    col: col ,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.colonia,
                        text: element.colonia,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
	}
    
}


function regresar_view(){
    location.href= base_url+'Operaciones/gradoRiesgo/'+$("#idopera").val();
}
*/

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}

function imprimir_formato(){
    var pre=$('input:radio[name=imprimir]:checked').val();
    if(pre==1){
        $('#modalimprimir').modal('hide');
        $('.btn_guardar_text').css('display','none');
        $('.texto_firma').css('display','block');
        setTimeout(function () { 
            window.print();
        }, 500);
        setTimeout(function () { 
            $('.btn_guardar_text').css('display','block');
            $('.texto_firma').css('display','none');
        }, 1000);
    }else{
        $('#modalimprimir').modal('hide');
        guardar_form_cuestionario2();
    }
}

function imprimir_for(){
    /*$('.btn_guardar_text').css('display','none');
    $('.texto_firma').css('display','block');
    setTimeout(function () { 
        window.print();
    }, 500);
    setTimeout(function () { 
        $('.btn_guardar_text').css('display','block');
        $('.texto_firma').css('display','none');
    }, 1000);*/
    idcc = $("#idclientec").val();
    idp = $("#idperfilamiento").val();
    tipo = $("#tipoc").val();
    ido = $("#id_operacion").val();
    idcgr = $("#idcgr").val();
    window.open(base_url+'Clientes_cliente/generarPDF_CA/'+idcc+"/"+idp+"/"+tipo+"/"+ido+"/"+idcgr+"/1", '_blank');
}

function getpais(id){
  //console.log("get pais2");
    $('#pais_'+id).select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Perfilamiento/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}