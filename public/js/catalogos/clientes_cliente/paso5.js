var base_url = $('#base_url').val();
var img=""; var img2=""; var img3=""; var img4=""; var img5="";var img6=""; var img7=""; var img8=""; var img9="";var img10=""; var img11="";
var img12="";var img13=""; var img14=""; var img15="";var img16=""; var img17=""; var img18=""; var img19=""; var chk_valid; var val=""; var presenApo = 0; var id_docs = 0;
var validado="";
var imgdet=""; var typePDF=""; var imgprev="";
var imgdet2=""; var typePDF2=""; var imgprev2="";
var imgdet3=""; var typePDF3=""; var imgprev3="";
var imgdet4=""; var typePDF4=""; var imgprev4="";
var imgdet5=""; var typePDF5=""; var imgprev5="";
var imgdet6=""; var typePDF6=""; var imgprev6="";
var imgdet7=""; var typePDF7=""; var imgprev7="";
var imgdet8=""; var typePDF8=""; var imgprev8="";
var imgdet9=""; var typePDF9=""; var imgprev9="";
var imgdet10=""; var typePDF10=""; var imgprev10="";
var imgdet11=""; var typePDF11=""; var imgprev11="";
var imgdet12=""; var typePDF12=""; var imgprev12="";
var imgdet13=""; var typePDF13=""; var imgprev13="";
var imgdet14=""; var typePDF14=""; var imgprev14="";
var imgdet15=""; var typePDF15=""; var imgprev15="";
var imgdet16=""; var typePDF16=""; var imgprev16="";
var imgdet17=""; var typePDF17=""; var imgprev17="";
var imgdet18=""; var typePDF18=""; var imgprev18="";
var imgdet19=""; var typePDF19=""; var imgprev19="";
var cant_docs=0; var cant_docs_capt=0; var cant_docs_capt_cont=0;

var ext1=""; var ext2=""; var ext3=""; var ext4=""; var ext5="";
var imgdetext1=""; var typePDFext1=""; var imgprevext1="";
var imgdetext2=""; var typePDFext2=""; var imgprevext2="";
var imgdetext3=""; var typePDFext3=""; var imgprevext3="";
var imgdetext4=""; var typePDFext4=""; var imgprevext4="";
var imgdetext5=""; var typePDFext5=""; var imgprevext5="";

$(function () {
  $("input[class*='form-check-input']").attr('disabled',true);

  if($("#Ch1").is(':checked')){
    presenApo = 1;
  }

  $("#mas_docs_cli").on("click", function(){
    verificaExtras();
  });

  /*$("input[id*='Chimg12']").attr('checked',false);
  $("input[id*='Chimg13']").attr('checked',false);
  $("input[id*='Chimg15']").attr('checked',false);*/
  verificaFotos();
  $('#form_docs_cliente').validate({
      submitHandler: function (form) {

        return false; // required to block normal submit for ajax
       },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
  });

  /*$('.img').change( function() {
    verificaFotos();
    validaCheck();
  });*/
  $.ajax({ // 
    type: "POST",
    url: base_url+"Clientes_cliente/verificaFoto",
    data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" validado", "id_opera":$("#idopera").val()},
    async: false,
    success: function (result) {
      var data= JSON.parse(result);
      $.each(data, function (key, dato){
        if(dato.validado!="0"){
          validado=1;
          $("input[id*='validado']").attr('checked',true);
        }
      });
    }
  });
  $('#validado').on('click', function() {
    if($('#validado').is(':checked')){
      val = 1;
      title = "¿Desea Validar estos documentos?";
    }else{
      val = 0;
      title = "¿Desea marcar como no validos estos documentos?";
    }
    swal({
        title: title,
        text: "Se cambiará el estatus",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type:'POST',
          url: base_url+'Clientes_cliente/validar',
          data:{ id: $("#id").val(),val:val },
          success:function(data){
              swal("Éxito", "Cambio realizado correctamente", "success");
              /*setTimeout(function(){ 
                location.href= base_url+'Clientes_cliente';
              }, 1500);*/
              //setTimeout(function () {  history.back() }, 1500);
          }
        }); //cierra ajax
      }else{
        $('input[id*="validado"]').prop("checked", true).trigger("change");
      }
    });
    
  });
  setTimeout(function(){ 
    validaCheck();
  }, 1500);

  verificaVigencia();
});

function verificaVigencia(){
  $.ajax({ // 
     type: "POST",
     url: base_url+"Clientes_cliente/verificaVigencia",
     data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val()},
     async: false,
     success: function (result) {
        if(result>0){
          swal("¡Advertencia!", "Expendiente con 3 meses o mas, verifique y valide nuevamente", "error");
          $('input[id*="validado"]').prop("checked", false);
        }
     }
  });
}

var tama_perm = false;
$('.img').change( function() {
  filezise = this.files[0].size;                     
  if(filezise > 4194304) { //=2 megas // 512000 bytes = 500 Kb -- se cambia a 4mb
    //console.log($('.img')[0].files[0].size);
    $(this).val('');
    swal("Error!", "El archivo supera el límite de peso permitido (4 mb)", "error");
    return false;
  }else { //ok
    tama_perm = true; 
    var archivo = this.value;
    var name = this.id;
    var col="";
    //console.log("archivo: "+archivo);
    //console.log("name: "+this.id);
    if(name=="img") col="formato_pb"; if(name=="imgcc") col="formato_cc";if(name=="imgio") col="ine"; if(name=="imgcif") col="cif";
    if(name=="imgcurp") col="curp"; if(name=="imgcd") col="comprobante_dom";if(name=="imgcdb") col="formato_duen_bene"; 
    if(name=="cons_elp") col="const_est_legal"; if(name=="carta_poder") col="carta_poder"; if(name=="cons_elp2") col="const_est_legal2";
    if(name=="imgcdal") col="comprobante_dom_apod"; if(name=="esc_ints") col="esc_ints"; if(name=="poder_not_fac_serv") col="poder_not_fac_serv";
    if(name=="poder_not") col="poder_not"; if(name=="imgineal") col="ine_apo"; if(name=="imgacre_legal") col="const_est_legal3";
    if(name=="poder_not2") col="poder_not2"; if(name=="cuestionario_pep") col="cuest_pep";
    if(name=="cuestionario_amp") col="cuest_amp";

    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
    permitida = false;
    if($('#'+name)[0].files.length > 0) {
      for (var i = 0; i < extensiones_permitidas.length; i++) {
         if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
         }
      }  
      if(permitida==true && tama_perm==true){
        //console.log("tamaño permitido");
        var inputFileImage = document.getElementById(name);
        var file = inputFileImage.files[0];
        var data = new FormData();
        //id_docs = $("#id").val();
        data.append('foto',file);
        data.append('iddoc',id_docs);
        $.ajax({
          url:base_url+'Clientes_cliente/cargafiles/'+$("#id").val()+"/"+$("#id_clientec").val()+'/'+$("#idperfilamiento").val()+"/"+col+"/"+$("#idopera").val(),
          type:'POST',
          contentType:false,
          data:data,
          processData:false,
          cache:false,
          success: function(data) {
            var array = $.parseJSON(data);
            //console.log(array.ok);
            //verificaFotos();          
            if (array.ok=true) {
              id_docs = array.id;
              //console.log("id_docs: "+id_docs);
              //console.log("array id: "+array.id);
              swal("Éxito", "Imagen cargada Correctamente", "success");
              $("#id").val(array.id);
            }else{
              swal("Error!", "No Se encuentra el archivo", "error");
            }
            if(array.id==0) {
                swal("Éxito", "Imagen cargada Correctamente", "success");
                $("#id").val(array.id);
                setTimeout(function(){ 
                  location.href= base_url+'Clientes_cliente';
                }, 1500);
            }
            cant_docs_capt++;
            validaCheck(); //cometado de pureba
            //graficaBarra();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
          }
        });
      }else if(permitida==false){
        swal("Error!", "Tipo de archivo no permitido", "error");
      } 
    }else{
      swal("Error!", "No se a selecionado un archivo", "error");
    } 
  }
});


$('.imgExt').change( function() {
  filezise = this.files[0].size;                     
  if(filezise > 2548000) { // 1mb
    //console.log($('.img')[0].files[0].size);
    $(this).val('');
    swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
  }else { //ok
    tama_perm = true; 
    var archivo = this.value;
    var name = this.id;
    var col=name;
    console.log("archivo: "+archivo);
    console.log("name: "+this.id);

    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
    permitida = false;
    if($('#'+name)[0].files.length > 0) {
      for (var i = 0; i < extensiones_permitidas.length; i++) {
         if (extensiones_permitidas[i] == extension) {
         permitida = true;
         break;
         }
      }  
      if(permitida==true && tama_perm==true){
        //console.log("tamaño permitido");
          var inputFileImage = document.getElementById(name);
          var file = inputFileImage.files[0];
          var data = new FormData();
          //id_docs = $("#id").val();
          data.append('foto',file);
          //data.append('iddoc',id_docs);
          $.ajax({
              url:base_url+'Clientes_cliente/cargafilesExtra/'+$("#id_extra").val()+"/"+$("#id_clientec").val()+'/'+$("#idperfilamiento").val()+"/"+col+"/"+$("#idopera").val(),
              type:'POST',
              contentType:false,
              data:data,
              processData:false,
              cache:false,
              success: function(data) {
                var array = $.parseJSON(data);
                if (array.ok=true) {
                  //id_docs = array.id;
                  //console.log("id_docs: "+id_docs);
                  swal("Éxito", "Imagen cargada Correctamente", "success");
                  $("#id_extra").val(array.id);
                }else{
                  swal("Error!", "No Se encuentra el archivo", "error");
                }
                if(array.id==0) {
                    swal("Éxito", "Imagen cargada Correctamente", "success");
                    $("#id_extra").val(array.id);
                    setTimeout(function(){ 
                      location.href= base_url+'Clientes_cliente';
                    }, 1500);
                }

              },
              error: function(jqXHR, textStatus, errorThrown) {
                  var data = JSON.parse(jqXHR.responseText);
              }
          });
        }else if(permitida==false){
            swal("Error!", "Tipo de archivo no permitido", "error");
        } 
    }else{
      swal("Error!", "No se a selecionado un archivo", "error");
    } 
  }
});

function verificaFotos(){
  var tipo_cliente = $('#tipo_cliente').val();
  /*if(tipo_cliente==1 || tipo_cliente==2 || tipo_cliente==3 || tipo_cliente==4 || tipo_cliente==5 || tipo_cliente==6){
    $.ajax({
      type: "POST",
      url: base_url+"Clientes_cliente/verificaFoto",
      data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"formato_pb"},
      async: false, 
      success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.formato_pb!=""){
              //console.log("nombre de foto: "+dato.formato_pb);
              $("input[id*='Chimg1']").attr('checked',true);
              img = dato.formato_pb;
            }
          });
      }
    });
    $("#img").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+base_url+'uploads/formato_perb/'+img+'" width="50px" alt="Sin Dato">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","pdf"],
        initialPreview: [
        // IMAGE RAW MARKUP
        '<img src="'+base_url+'uploads/formato_perb/'+img+'" class="kv-preview-data file-preview-image" >',
        
        ],
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type: "image", url: base_url+"Clientes_cliente/file_delete/"+$('#id').val()+"/formato_pb", caption: img, key: 1},
        ]
    });
  }*/
  if(tipo_cliente==1 || tipo_cliente==2 || tipo_cliente==3 || tipo_cliente==4 || tipo_cliente==5 || tipo_cliente==6){ //conoce a tu cliente
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"formato_cc", "id_opera":$("#idopera").val() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.formato_cc!=""){
              $("input[id*='Chimg2']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              img2 = dato.formato_cc;
              iddoc = $("#id").val();
              imgprev2 = base_url+"uploads/formato_cc/"+img2;
              imgdet2 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/formato_cc", caption: img2, key:1};
              ext = img2.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp2 = base_url+"uploads/formato_cc/"+img2; 
                typePDF2 = "false";  
              }else{
                imgp2 = base_url+"uploads/formato_cc/"+img2;
                imgdet2 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/formato_cc", caption: img2, key:1};
                typePDF2 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#imgcc").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev2+'',    
      ],
      initialPreviewAsData: typePDF2,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet2
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==1 || tipo_cliente==2){ //ine
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"ine", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.ine!=""){
              img3 = dato.ine;
              $("input[id*='Chimg3']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev3 = base_url+"uploads/ine/"+img3;
              imgdet3 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/ine", caption: img3, key:2};
              ext = img3.split('.');
              //console.log("img3: "+img3);
              //console.log("ext imgio: "+ext);
              if(ext[1].toLowerCase()!="pdf"){
                imgp3 = base_url+"uploads/ine/"+img3; 
                typePDF3 = "false";  
              }else{
                imgp3 = base_url+"uploads/ine/"+img3;
                imgdet3 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/ine", caption: img3, key:2};
                typePDF3 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#imgio").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
      initialPreview: [
        ''+imgprev3+'',    
      ],
      initialPreviewAsData: typePDF3,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet3
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==1 || tipo_cliente==3 || tipo_cliente==4 || tipo_cliente==5 || tipo_cliente==6){ //cif
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"cif", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.cif!=""){
              img4 = dato.cif;
              $("input[id*='Chimg4']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev4 = base_url+"uploads/cif/"+img4;
              imgdet4 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cif", caption: img4, key:3};
              ext = img4.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp4 = base_url+"uploads/cif/"+img4; 
                typePDF4 = "false";  
              }else{
                imgp4 = base_url+"uploads/cif/"+img4;
                imgdet4 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cif", caption: img4, key:3};
                typePDF4 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#imgcif").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev4+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
      initialPreview: [
        ''+imgprev4+'',    
      ],
      initialPreviewAsData: typePDF4,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet4
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==1){ //curp 
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"curp", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.curp!=""){
              img5 = dato.curp;
              $("input[id*='Chimg5']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev5 = base_url+"uploads/curp/"+img5;
              imgdet5 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/curp", caption: img5, key:4};
              ext = img5.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp5 = base_url+"uploads/curp/"+img5; 
                typePDF5 = "false";  
              }else{
                imgp5 = base_url+"uploads/curp/"+img5;
                imgdet5 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/curp", caption: img5, key:4};
                typePDF5 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#imgcurp").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev5+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev5+'',    
      ],
      initialPreviewAsData: typePDF5,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet5
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==1 || tipo_cliente==2 || tipo_cliente==3 || tipo_cliente==4 || tipo_cliente==5){ //comprobante de domicilio
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"comprobante_dom", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.comprobante_dom!=""){
              img6 = dato.comprobante_dom;
              $("input[id*='Chimg6']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev6 = base_url+"uploads/compro_dom/"+img6;
              imgdet6 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/comprobante_dom", caption: img6, key:5};
              ext = img6.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp6 = base_url+"uploads/compro_dom/"+img6; 
                typePDF6 = "false";  
              }else{
                imgp6 = base_url+"uploads/compro_dom/"+img6;
                imgdet6 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/comprobante_dom", caption: img6, key:5};
                typePDF6 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }//6
    });
    $("#imgcd").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev6+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev6+'',    
      ],
      initialPreviewAsData: typePDF6,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet6
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  //este if esta como =  8 para no cargar, ya ningun cliente lleva ese formato
  if(/*tipo_cliente==1 && presenApo==1 || tipo_cliente==2 && presenApo==1 || tipo_cliente==3 || tipo_cliente==5*/ tipo_cliente==8){ //dueño beneficiaio
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"formato_duen_bene", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.formato_duen_bene!=""){
              img7 = dato.formato_duen_bene;
              $("input[id*='Chimg7']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev7 = base_url+"uploads/compro_due_ben/"+img7;
              imgdet7 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/formato_duen_bene", caption: img7, key:6};
              ext = img7.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp7 = base_url+"uploads/compro_due_ben/"+img7; 
                typePDF7 = "false";  
              }else{
                imgp7 = base_url+"uploads/compro_due_ben/"+img7;
                imgde7 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/formato_duen_bene", caption: img7, key:6};
                typePDF7 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });//img 7
    $("#imgcdb").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev7+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev7+'',    
      ],
      initialPreviewAsData: typePDF7,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet7
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==2){//constancia de est. legal
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"const_est_legal", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.const_est_legal!=""){
              img8 = dato.const_est_legal;
              $("input[id*='Chimg8']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev8 = base_url+"uploads/constancia_legal/"+img8;
              imgdet8 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/const_est_legal", caption: img8, key:7};
              ext = img8.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp8 = base_url+"uploads/constancia_legal/"+img8; 
                typePDF8 = "false";  
              }else{
                imgp8 = base_url+"uploads/constancia_legal/"+img8;
                imgdet8 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/const_est_legal", caption: img8, key:7};
                typePDF8 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#cons_elp").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev8+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev8+'',    
      ],
      initialPreviewAsData: typePDF8,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet8
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==1 && presenApo==1 || tipo_cliente==2 && presenApo==1){ //carta poder
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"carta_poder", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.carta_poder!=""){
              img9 = dato.carta_poder;
              $("input[id*='Chimg9']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev9 = base_url+"uploads/carta_poder/"+img9;
              imgdet9 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/carta_poder", caption: img9, key:8};
              ext = img9.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp9 = base_url+"uploads/carta_poder/"+img9; 
                typePDF9 = "false";  
              }else{
                imgp9 = base_url+"uploads/carta_poder/"+img9;
                imgdet9 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/carta_poder", caption: img9, key:8};
                typePDF9 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#carta_poder").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev9+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev9+'',    
      ],
      initialPreviewAsData: typePDF9,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet9
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==4){ //constancia de estancia legal 2
    $.ajax({ // 10
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"const_est_legal2", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.const_est_legal2!=""){
              img10 = dato.const_est_legal2;
              $("input[id*='Chimg10']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev10 = base_url+"uploads/constancia_legal/"+img10;
              imgdet10 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/const_est_legal2", caption: img10, key:9};
              ext = img10.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp10 = base_url+"uploads/constancia_legal/"+img10; 
                typePDF10 = "false";  
              }else{
                imgp10 = base_url+"uploads/constancia_legal/"+img10;
                imgdet10 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/const_est_legal2", caption: img10, key:9};
                typePDF10 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#cons_elp2").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev10+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev10+'',    
      ],
      initialPreviewAsData: typePDF10,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet10
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==1 && presenApo==1 || tipo_cliente==2 && presenApo==1){ //falta verificar si presentó apoderado legal, de ser el caso verificar su imagen
    $.ajax({ //comprobante dom. apoderado legal
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"comprobante_dom_apod", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.comprobante_dom_apod!=""){
              img11 = dato.comprobante_dom_apod;
              $("input[id*='Chimg11']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev11 = base_url+"uploads/compro_dom/"+img11;
              imgdet11 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/comprobante_dom_apod", caption: img11, key:10};
              ext = img11.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp11 = base_url+"uploads/compro_dom/"+img11; 
                typePDF11 = "false";  
              }else{
                imgp11 = base_url+"uploads/compro_dom/"+img11;
                imgdet11 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/comprobante_dom_apod", caption: img11, key:10};
                typePDF11 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#imgcdal").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev11+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev11+'',    
      ],
      initialPreviewAsData: typePDF11,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet11
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==3 || tipo_cliente==4 || tipo_cliente==6){ //Escritura constitutiva o instrumento público
    $.ajax({ // 12
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" esc_ints", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.esc_ints!=""){
              img12 = dato.esc_ints;
              $("input[id*='Chimg12']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev12 = base_url+"uploads/escrituras/"+img12;
              imgdet12 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/esc_ints", caption: img12, key:11};
              ext = img12.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp12 = base_url+"uploads/escrituras/"+img12; 
                typePDF12 = "false";  
              }else{
                imgp12 = base_url+"uploads/escrituras/"+img12;
                imgdet12 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/esc_ints", caption: img12, key:11};
                typePDF12 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#esc_ints").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev12+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev12+'',    
      ],
      initialPreviewAsData: typePDF12,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet12
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==3){ //Poderes Notariales o documento para comprobar las facultades del(os) servidor(es) público(s)
    $.ajax({ // 13
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" poder_not_fac_serv", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.poder_not_fac_serv!=""){
              img13 = dato.poder_not_fac_serv;
              $("input[id*='Chimg13']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev13 = base_url+"uploads/poder_not/"+img13;
              imgdet13 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/poder_not_fac_serv", caption: img13, key:12};
              ext = img13.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp13 = base_url+"uploads/poder_not/"+img13; 
                typePDF13 = "false";  
              }else{
                imgp13 = base_url+"uploads/poder_not/"+img13;
                imgdet13 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/poder_not_fac_serv", caption: img13, key:12};
                typePDF13 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#poder_not_fac_serv").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev13+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev13+'',    
      ],
      initialPreviewAsData: typePDF13,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet13
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==6){ // poderes notariales
    $.ajax({ // 14
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" poder_not", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.poder_not!=""){
              img14 = dato.poder_not;
              $("input[id*='Chimg14']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev14 = base_url+"uploads/poder_not/"+img14;
              imgdet14 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/poder_not", caption: img14, key:13};
              ext = img14.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp14 = base_url+"uploads/poder_not/"+img14; 
                typePDF14 = "false";  
              }else{
                imgp = base_url+"uploads/poder_not/"+img14;
                imgdet14 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/poder_not", caption: img14, key:13};
                typePDF14 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#poder_not").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev14+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev14+'',    
      ],
      initialPreviewAsData: typePDF14,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet14
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==1 && presenApo==1 || tipo_cliente==2 && presenApo==1 || tipo_cliente==3 || tipo_cliente==4 || tipo_cliente==5 || tipo_cliente==6){//ine de apoderado
    //console.log("img15 a verificar: "+tipo_cliente);
    $.ajax({ // 15
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" ine_apo", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.ine_apo!=""){
              img15 = dato.ine_apo;
              //console.log("img15 desde verifica foto: "+img15);
              $("input[id*='Chimg15']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev15 = base_url+"uploads/ine/"+img15;
              imgdet15 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/ine_apo", caption: img15, key:14};
              ext = img15.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp15 = base_url+"uploads/ine/"+img15; 
                typePDF15 = "false";  
              }else{
                imgp15 = base_url+"uploads/ine/"+img15;
                imgdet15 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/ine_apo", caption: img15, key:14};
                typePDF15 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              //console.log("15");
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#imgineal").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev15+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev15+'',    
      ],
      initialPreviewAsData: typePDF15,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet15
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  if(tipo_cliente==5){ //Documento que acredite su legal existencia: para tipo cliente 5
    $.ajax({ // 16
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" const_est_legal3", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.const_est_legal3!=""){
              img16 = dato.const_est_legal3;
              $("input[id*='Chimg16']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev16 = base_url+"uploads/constancia_legal/"+img16;
              imgdet16 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/const_est_legal3", caption: img16, key:15};
              ext = img16.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp16 = base_url+"uploads/constancia_legal/"+img16; 
                typePDF16 = "false";  
              }else{
                imgp16 = base_url+"uploads/constancia_legal/"+img16;
                imgdet16 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/const_est_legal3", caption: img16, key:15};
                typePDF16 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                console.log("llamo a grafica");
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#imgacre_legal").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev16+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev16+'',    
      ],
      initialPreviewAsData: typePDF16,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet16
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
    $.ajax({ // 17 -- poderes notariales
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" poder_not2", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.poder_not2!=""){
              img17 = dato.poder_not2;
              $("input[id*='Chimg17']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev17 = base_url+"uploads/poder_not/"+img17;
              imgdet17 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/poder_not2", caption: img17, key:16};
              ext = img17.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp17= base_url+"uploads/poder_not/"+img17; 
                typePDF17 = "false";  
              }else{
                imgp17 = base_url+"uploads/poder_not/"+img17;
                imgdet17 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/poder_not2", caption: img17, key:16};
                typePDF17 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#poder_not2").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev17+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev17+'',    
      ],
      initialPreviewAsData: typePDF17,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet17
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  var idtipo_cliente= $("#idtipo_cliente").val();
  var clasificacion= $("#clasificacion").val();
  var es_pep= $("#es_pep").val();
  var id_actividad= $("#id_actividad").val();
  var emb_cons_org_inter= $("#emb_cons_org_inter").val();
  if(idtipo_cliente==5 || idtipo_cliente==1 && clasificacion>0 && clasificacion<=7 
    || idtipo_cliente==1 && clasificacion>=9 && clasificacion<=15 && id_actividad!=15 && id_actividad!=16 
    || idtipo_cliente==1 && clasificacion>9 && clasificacion<=15 && id_actividad==15 || 
        idtipo_cliente==1 && clasificacion>9 && clasificacion<=15 && id_actividad==16
    || emb_cons_org_inter==1 || es_pep>0){

    $.ajax({ // 18
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" cuest_pep", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.cuest_pep!="" && dato.cuest_pep!="0"){
              img18 = dato.cuest_pep;
              $("input[id*='Chimg18']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev18 = base_url+"uploads/cuestionario_pep/"+img18;
              imgdet18 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cuest_pep", caption: img18, key:18};
              ext = img18.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp18 = base_url+"uploads/cuestionario_pep/"+img18; 
                typePDF18 = "false";  
              }else{
                imgp18 = base_url+"uploads/cuestionario_pep/"+img18;
                imgdet18 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cuest_pep", caption: img18, key:18};
                typePDF18 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
            }
          });
       }
    });
    $("#cuestionario_pep").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev18+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev18+'',    
      ],
      initialPreviewAsData: typePDF18,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet18
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
    /*$.ajax({ // 19
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" cuest_amp"},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.cuest_amp!="" && dato.cuest_amp!="0"){
              img19 = dato.cuest_amp;
              $("input[id*='Chimg19']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev19 = base_url+"uploads/cuestionario_amp/"+img19;
              imgdet19 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cuest_amp", caption: img19, key:19};
              ext = img19.split('.');
              if(ext[1]!="pdf"){
                imgp19= base_url+"uploads/cuestionario_amp/"+img19; 
                typePDF19 = "false";  
              }else{
                imgp19 = base_url+"uploads/cuestionario_amp/"+img19;
                imgdet19 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cuest_amp", caption: img19, key:19};
                typePDF19 = "true";
              }
              if($("#grado").val()<=2.0){
                cant_docs_capt++;
              }
              cant_docs_capt_cont++;
            }
          });
       }
    });
    $("#cuestionario_amp").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev19+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev19+'',    
      ],
      initialPreviewAsData: typePDF19,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet19
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });*/
  }

  if($("#grado").val()>2.0){
    $.ajax({ // 19
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":" cuest_amp", "id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.cuest_amp!="" && dato.cuest_amp!="0"){
              img19 = dato.cuest_amp;
              $("input[id*='Chimg19']").attr('checked',true);
              iddoc = $("#id").val();
              imgprev19 = base_url+"uploads/cuestionario_amp/"+img19;
              imgdet19 = {type:"image", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cuest_amp", caption: img19, key:19};
              ext = img19.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp19= base_url+"uploads/cuestionario_amp/"+img19; 
                typePDF19 = "false";  
              }else{
                imgp19 = base_url+"uploads/cuestionario_amp/"+img19;
                imgdet19 = {type:"pdf", url: base_url+"Clientes_cliente/file_delete/"+iddoc+"/cuest_amp", caption: img19, key:19};
                typePDF19 = "true";
              }
              cant_docs_capt++;
              cant_docs_capt_cont++;
              /*if(cant_docs_capt_cont==cant_docs){
                graficaBarra(); //agregar baderas por tipo de cliente para llamar a esta funcion
              }*/
            }
          });
       }
    });
    $("#cuestionario_amp").fileinput({
      overwriteInitial: true,
      maxFileSize: 4096, //4mb
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev19+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev19+'',    
      ],
      initialPreviewAsData: typePDF19,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet19
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }
  //console.log("cant_docs_capt: "+cant_docs_capt);
  //console.log("cant_docs_capt_cont: "+cant_docs_capt_cont);
}
var bandera_ext=false;

function verificaExtras(){
  /** ****** EXTRA 1 ************ */
  if(bandera_ext==false){
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFotoExtra",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"extra","col2":"descrip","id_opera":$("#idopera").val() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra!=""){
              $("input[id*='Chext1']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext1 = dato.extra;
              iddoc = $("#id_extra").val();
              imgprevext1 = base_url+"uploads/extras/"+ext1;
              imgdetext1 = {type:"image", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra", caption: '', key:16};
              ext = ext1.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext1 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext1 = {type:"pdf", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra", caption: '', key:16};
                typePDFext1 = "true";
              }
              $("#descrip").val(dato.descrip);
            }
          });
       }
    });
    $("#extra").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext1+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext1+'',    
      ],
      initialPreviewAsData: typePDFext1,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext1
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
    /** ****** EXTRA 2 ************ */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFotoExtra",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"extra2","col2":"descrip2","id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra2!=""){
              $("input[id*='Chext2']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext2 = dato.extra2;
              iddoc = $("#id_extra").val();
              imgprevext2 = base_url+"uploads/extras/"+ext2;
              imgdetext2 = {type:"image", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra2", caption: ext2, key:17};
              ext = ext2.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext2 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext2 = {type:"pdf", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra2", caption: ext2, key:17};
                typePDFext2 = "true";
              }
              $("#descrip2").val(dato.descrip2);
            }
          });
       }
    });
    $("#extra2").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext2+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext2+'',    
      ],
      initialPreviewAsData: typePDFext2,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext2
      ]
    });
    /** ****** EXTRA 3 ************ */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFotoExtra",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"extra3","col2":"descrip3","id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra3!=""){
              $("input[id*='Chext3']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext3 = dato.extra3;
              iddoc = $("#id_extra").val();
              imgprevext3 = base_url+"uploads/extras/"+ext3;
              imgdetext3 = {type:"image", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra3", caption: ext3, key:18};
              ext = ext3.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext3 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext3 = {type:"pdf", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra3", caption: ext3, key:18};
                typePDFext3 = "true";
              }
              $("#descrip3").val(dato.descrip3);
            }
          });
       }
    });
    $("#extra3").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext3+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext3+'',    
      ],
      initialPreviewAsData: typePDFext3,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext3
      ]
    });  
    /** ****** EXTRA 4 ************ */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFotoExtra",
       data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"extra4","col2":"descrip4","id_opera":$("#idopera").val()},
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra4!=""){
              $("input[id*='Chext4']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext4 = dato.extra4;
              iddoc = $("#id_extra").val();
              imgprevext4 = base_url+"uploads/extras/"+ext4;
              imgdetext4 = {type:"image", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra4", caption: ext4, key:19};
              ext = ext4.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext4 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext4 = {type:"pdf", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra4", caption: ext4, key:19};
                typePDFext4 = "true";
              }
              $("#descrip4").val(dato.descrip4);
            }
          });
       }
    });
    $("#extra4").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext4+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext4+'',    
      ],
      initialPreviewAsData: typePDFext4,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext4
      ]
    });
    /** ****** EXTRA 5 ************ */
    $.ajax({
      type: "POST",
      url: base_url+"Clientes_cliente/verificaFotoExtra",
      data: { "idcc": $("#id_clientec").val(),"idper": $("#idperfilamiento").val(),"col":"extra5","col2":"descrip5","id_opera":$("#idopera").val()},
      async: false,
      success: function (result) {
        var data= JSON.parse(result);
        $.each(data, function (key, dato){
          if(dato.extra5!=""){
            $("input[id*='Chext5']").attr('checked',true);
            //console.log("nombre de foto cc: "+dato.formato_cc);
            ext5 = dato.extra5;
            iddoc = $("#id_extra").val();
            imgprevext5 = base_url+"uploads/extras/"+ext5;
            imgdetext5 = {type:"image", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra5", caption: ext5, key:20};
            ext = ext5.split('.');
            if(ext[1].toLowerCase()!="pdf"){
              //imgpe = base_url+"uploads/extras/"+ext1; 
              typePDFext5 = "false";  
            }else{
             // imgpe = base_url+"uploads/extras/"+ext1;
              imgdetext5 = {type:"pdf", url: base_url+"Clientes_cliente/file_deleteExtra/"+iddoc+"/extra5", caption: ext5, key:20};
              typePDFext5 = "true";
            }
            $("#descrip5").val(dato.descrip5);
          }
        });
       }
    });
    $("#extra5").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext5+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext5+'',    
      ],
      initialPreviewAsData: typePDFext5,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext5
      ]
    });
  }
  bandera_ext=true;
}

//Falta validar los posibles caminos 
var cant_docs_apo = 0;
var total_por_cap; 
var cant_docs_pep=2;
function validaCheck(){
  var tipo_cliente = $('#tipo_cliente').val();
  //console.log(tipo_cliente);
  if(tipo_cliente==1){
    if(img3==null){
        chk_valid=true;
    }else{ //ine      cif        curp     compro_dom  formato_duen_bene  carta_poder quedan pendientes las del apoderado
      if(img2=='' || img3=='' || img4=='' || img5=='' || img6=='' /*|| img9==''*/){
        chk_valid=true; //no se puede hacer check  
      }if($("#Ch1").is(':checked')){
        cant_docs_apo=3;
        if($("#Ch1").is(':checked') && img9!="" && $("#Ch1").is(':checked') && img11!="" && $("#Ch1").is(':checked') && img15!=""){
          chk_valid=false; //puede validar
        }
      }  
    }
    cant_docs=5+cant_docs_apo;     
  }
  else if(tipo_cliente==2){
    if(img2==null){
      chk_valid=true;
    }else{//formato_cc                                 const_est_legal       comprobante_dom_apod   ine_apo
       if(img2=='' || img3=='' || img6=='' || img8=='' /*|| img7=='' || img9=='' || img11=='' || img15==''*/){
          chk_valid=true;
       }if($("#Ch1").is(':checked')){
          cant_docs_apo=3;
          if($("#Ch1").is(':checked') && img9!="" && $("#Ch1").is(':checked') && img11!="" && $("#Ch1").is(':checked') && img15!=""){
            chk_valid=false;
          }
       }  
    }
    cant_docs=3+cant_docs_apo;      
  }else if(tipo_cliente==3){
    if(img2==null){
      chk_valid=true;
    }else{                                           //esc_ints poder_not_fac_serv
      if(img2=='' || img4=='' || img6=='' /*|| img7=='' || img12=='' || img13=='' || img15==''*/){
        chk_valid=true;
      }if($("#Ch1").is(':checked')){
        cant_docs_apo=3;
        if($("#Ch1").is(':checked') && img12!="" && $("#Ch1").is(':checked') && img13!="" && $("#Ch1").is(':checked') && img15!=""){
          chk_valid=false;
        }
      }  
    }
    cant_docs=3+cant_docs_apo;        
  }else if(tipo_cliente==4){
    if(img2==null){
      chk_valid=true;
    }/*else{                   //const_est_legal2
      if(img2=='' || img4=='' || img6==''){
        chk_valid=true;
      }if($("#Ch1").is(':checked')){
        cant_docs_apo=3;
        if($("#Ch1").is(':checked') && img12!="" && $("#Ch1").is(':checked') && img10!="" && $("#Ch1").is(':checked') && img15!=""){
          chk_valid=false;
        }
      }  
    }*/
    else{                
      if(img2=='' || img4=='' || img6=='' || img12!="" || img10!="" || img15!=""){
        chk_valid=true;
      }
      cant_docs_apo=3;
      if(img12!="" && img10!="" && img15!=""){
        chk_valid=false;
      } 
    }
    cant_docs=3+cant_docs_apo;      
  }else if(tipo_cliente==5){
    if(img2==null){
      chk_valid=true;
    }else{
       if(img2=='' || img4=='' || img6==''/* || img7=='' || img15==''*/){
           chk_valid=true;
       }if($("#Ch1").is(':checked')){
          cant_docs_apo=3;
          if($("#Ch1").is(':checked') && img15!="" && $("#Ch1").is(':checked') && img16!="" && $("#Ch1").is(':checked') && img17!=""){
            chk_valid=false;  
          }
       }  
    }
    cant_docs=3+cant_docs_apo;      
  }else if(tipo_cliente==6){
    /*console.log("img2: "+img2);
    console.log("img4: "+img4);
    console.log("img15: "+img15);*/
    if(img2==null){
      chk_valid=true;
    }else{                       //esc_ints   poder_not
       if(img2=='' || img4=='' /*|| img12=='' || img14=='' || img15==''*/){
           chk_valid=true;
           //console.log("tipo 6 sin docs de cliente: ");
       }if($("#Ch1").is(':checked')){ //verificar el if de abajo no entra a contar si tiene docs de apoderado
          cant_docs_apo=3;
          if($("#Ch1").is(':checked') && img12!="" && $("#Ch1").is(':checked') && img14!="" && $("#Ch1").is(':checked') && img15!=""){
            chk_valid=false;
            //console.log("cant_docs_apo: "+cant_docs_apo);
          }
       }  
    }
    cant_docs=2+cant_docs_apo;      
  }

  idtipo_cliente= $("#idtipo_cliente").val();
  clasificacion= $("#clasificacion").val();
  es_pep= $("#es_pep").val();
  emb_cons_org_inter= $("#emb_cons_org_inter").val();

  var id_actividad= $("#id_actividad").val();
  if(idtipo_cliente==5 || idtipo_cliente==1 && clasificacion>0 && clasificacion<=7 
    || idtipo_cliente==1 && clasificacion>=9 && clasificacion<=15 && id_actividad!=15 && id_actividad!=16 
    || idtipo_cliente==1 && clasificacion>9 && clasificacion<=15 && id_actividad==15 || 
        idtipo_cliente==1 && clasificacion>9 && clasificacion<=15 && id_actividad==16
    || emb_cons_org_inter==1 || es_pep>0){
    //cant_docs=cant_docs+cant_docs_pep;    
    cant_docs=cant_docs+1;    
  }
  if($("#grado").val()>2.0){
    cant_docs=cant_docs+1;   
  }

  //$("input[id*='validado']").attr('disabled',chk_valid);
  /*console.log("valida");

  console.log("img2: "+img2);
  console.log("img3: "+img3);
  console.log("img4: "+img4);
  console.log("img5: "+img5);
  console.log("img6: "+img6);*/

  /*if(chk_valid==true){
    //console.log("chk_valid: "+chk_valid);
    $(".valid").hide(); //porque no le mostró check a dany?? verficar pero dejarlo descomentado para prueba y error
    //console.log('oculto');
    //$("input[id*='validado']").attr('disabled',true); //verificar esta parte, sigue fallando a dany
    $("#validado").prop("disabled", false);
  }
  if(chk_valid==false ){
    //console.log("chk_valid: "+chk_valid);
    $(".valid").show();
    //$("#validado").prop("checked", true);
    $("#validado").prop("disabled", false);
    //$("input[id*='validado']").prop('disabled',chk_valid);
  }*/
  total_por_cap = cant_docs - cant_docs_capt;
  /*console.log("total por cap: "+total_por_cap);
  console.log("cant_docs: "+cant_docs);
  console.log("cant_docs_capt: "+cant_docs_capt);*/
  if(total_por_cap==0){
    $(".valid").show();
    //$("#validado").prop("checked", true);
    $("#validado").prop("disabled", false);
  }
  if(total_por_cap>0){
    $(".valid").hide(); //porque no le mostró check a dany?? verficar pero dejarlo descomentado para prueba y error
    //console.log('oculto');
    //$("input[id*='validado']").attr('disabled',true); //verificar esta parte, sigue fallando a dany
    $("#validado").prop("disabled", true);
  }
  graficaBarra();
}

function add_file(id,idc,idp,col,img,name){
  //console.log("add file");
  var archivo = img;
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
  /*extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
  extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
  permitida = false;
  if($('#'+name)[0].files.length > 0) {
    for (var i = 0; i < extensiones_permitidas.length; i++) {
       if (extensiones_permitidas[i] == extension) {
       permitida = true;
       break;
       }
    }  
    if(permitida==true && tama_perm==true){
      //console.log("tamaño permitido");
        var inputFileImage = document.getElementById(name);
        var file = inputFileImage.files[0];
        var data = new FormData();
        data.append('foto',file);
        $.ajax({
            url:base_url+'Clientes_cliente/cargafiles/'+id+"/"+idc+'/'+idp+"/"+col,
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
                var array = $.parseJSON(data);
                    //console.log(array.ok);
                    if (array.ok=true) {
                        swal("Éxito", "Guardado Correctamente", "success");
                    }else{
                        swal("Error!", "No Se encuentra el archivo", "error");
                    }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var data = JSON.parse(jqXHR.responseText);
            }
        });
      }else if(permitida==false){
          swal("Error!", "Tipo de archivo no permitido", "error");
      } 
  }else{
    swal("Error!", "No se a selecionado un archivo", "error");
  } */
          
}

function btn_alert_guardado() {
  var desc = $("#descrip").val();
  var desc2 = $("#descrip2").val();
  var desc3 = $("#descrip3").val();
  var desc4 = $("#descrip4").val();
  var desc5 = $("#descrip5").val();

  $.ajax({
   type: "POST",
   url: base_url+"Clientes_cliente/submit_extras",
   data: $("#form_docs_extra").serialize()+"&id="+$("#id_extra").val()+"&descrip="+desc+"&descrip2="+desc2+"&descrip3="+desc3+"&descrip4="+desc4+"&descrip5="+desc5+"&id_perfilamiento="+$("#idperfilamiento").val()+'&id_clientec='+$("#id_clientec").val(),
   beforeSend: function(){
      $("#btn_submit").attr("disabled",true);
   },
   success: function (result) {
      //console.log(result);
      $("#btn_submit").attr("disabled",false);
      //setTimeout(function () { window.location.href = base_url+"Clientes_cliente" }, 1500);
   }
  });
  if($("#validado").is(":checked")==true)
    swal("Éxito", "Expendiente Guardado Correctamente", "success");
  else
    swal("Éxito", "Expediente Guardado Correctamente, pendiente por validar", "success");
  //window.location.href = base_url+"Clientes_cliente";
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    setTimeout(function () { window.location.href = base_url+"Operaciones/procesoInicial/"+$("#idopera").val(); }, 1500);
}

function inicio_cliente(){
  window.history.back();
  //location.href= base_url+'Clientes_cliente';
  //history.back();
}

function graficaBarra(){
  /*console.log("cant capturados: "+cant_docs_capt);
  console.log("cant docs: "+cant_docs);*/
  //console.log("cant docs apoderado: "+cant_docs_apo);

  cant_docs_capt = parseFloat(cant_docs_capt);
  total_por_cap; 

  /*total_por = ((cant_docs *10) / cant_docs)*10;
  total_por_cap = cant_docs_capt * 10; */
  total_por_cap = cant_docs - cant_docs_capt;
  /*if(cant_docs==cant_docs_capt  && cant_docs_capt==0){
    total_por_cap = 100;
  }*/
  /*if(cant_docs_capt==1 && cant_docs<=6){
    total_por_cap = 85;
  }else if(cant_docs_capt==1 && cant_docs>6){
    total_por_cap = 80;
  }else{
    total_por_cap = total_por_cap * 10;
  }*/
  if(cant_docs==cant_docs_capt  && cant_docs_capt>0){
    total_cap = 100;
  }else if(cant_docs_capt>5){
    total_cap_pre = (100 * cant_docs_capt)/cant_docs;
    //console.log("total_cap_pre: "+total_cap_pre);
    total_cap = total_cap_pre / cant_docs;
    total_cap = total_cap*10;
    total_cap = parseFloat(total_cap).toFixed(2);
  }else{
    total_cap_pre = (100 * cant_docs_capt)/cant_docs;
    //console.log("total_cap_pre: "+total_cap_pre);
    total_cap = parseFloat(total_cap_pre).toFixed(2);
  }
  
  //total_cap = 100 - total_por_cap;
  /*console.log("total_por_cap: "+total_por_cap);
  console.log("total_cap: "+total_cap);*/
  
  if(cant_docs_capt==0){
    //console.log("0 capturados");
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 1%" aria-valuenow="1" aria-valuemin="100" aria-valuemax="1"></div><p style="font-size:17px"><b>0%</b></p>')
  }

  if(total_cap<=30 && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=45  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=60  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=75  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=90  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_por_cap==0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>100%</b></h1>')
  }
}

function formatoPersonasBloquedas(){
  window.open(base_url+"Clientes_cliente/formatoPesonasBloqueadas/"+$('#id_clientec').val()+"/"+$('#tipo_cliente').val()+"/"+$('#idperfilamiento').val()+"/"+$("#idopera").val(), "Formato PEP's", "width=1280, height=720"); //780x612
}

function tamano_ayuda(){
  $('#modal_carga_ayuda').modal();
}