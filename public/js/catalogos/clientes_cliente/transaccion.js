var base_url = $('#base_url').val();

$(document).ready(function() {
    get_beneficiarios();
    getpais(1);
    getpais(2);
    getpais(3)
    getpais2(1);
    pais_aux('t',12);
    get_pais_aux('t',12);
});

function get_beneficiarios(){
    var idc= $('#idciente').val();
    var idp= $('#idperfilamiento_cliente').val()
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Clientes_cliente/get_beneficiarios_select",
        data:{idc:idc,idp:idp},
        success: function (data){
            $('.select_beneficiario').html(data);
        }
    });
}

function validaBenef(val){
  console.log("valida benef");
  if(val!=""){
    $("#sig_si").attr('disabled',false);
    $("#otro_beneficiario").attr('disabled',true);
    $('input[id*="otro_beneficiario"]').prop("checked", false).trigger("change");
  }else{
    $("#sig_si").attr('disabled',true);
    $("#otro_beneficiario").attr('disabled',false);
  }

}
function click_beneficiario(){
  if($('#genero_1').is(':checked')){
       $('.text_select_beneficiario').css('display','block'); 
       $('.btn_siguiente').css('display','block');
       $('.btn_siguiente2').css('display','none');
  }
	if($('#genero_2').is(':checked')){
       $('.text_select_beneficiario').css('display','none');
       $('.btn_siguiente').css('display','none');
       $('.btn_siguiente2').css('display','block');
       $(".beneficiario_text").hide("slow");
       $(".text_select_beneficiario").hide("slow");
       $("#form_beneficiario_moral").hide("slow");
  }
}
function check_beneficiario(){
  if($('#otro_beneficiario').is(':checked')){
    $('.beneficiario_text').show(); 
  }else{
    $('.beneficiario_text').hide(); 
    $('.text_persona_fisica').hide(); 
    $('.text_persona_moral').hide(); 
    $('.text_persona_fideicomiso').hide(); 
    $('.residenciaEx').css('display','none'); 
    $('#especifique').css('display','none'); 
    $('.text_persona_moral').css('display','none'); 
  }
}
function check_tipo_persona(){
  if($('#tipo_persona option:selected').val()==1){
    $('.text_persona_fisica').show(1000);
    $('.text_persona_moral').hide(1000); 
    $('.text_persona_fideicomiso').hide(1000); 
  }else if($('#tipo_persona option:selected').val()==2){
    $('.text_persona_fisica').hide(1000); 
    $('.text_persona_moral').show(1000); 
    $('.text_persona_fideicomiso').hide(1000);
  }else if($('#tipo_persona option:selected').val()==3){
    $('.text_persona_moral').hide(1000); 
    $('.text_persona_fisica').hide(1000); 
    $('.text_persona_fideicomiso').show(1000); 
  }

  $("#sig_si").attr("disabled",false);
}
/*
function check_tipo_persona(){
  if($('#tipo_persona').is(':checked')){
    $('.text_persona_fisica').show(1000); 
  }else{
    $('.text_persona_fisica').hide(1000); 
  }
  if($('#tipo_persona2').is(':checked')){
    $('.text_persona_moral').show(1000); 
  }else{
    $('.text_persona_moral').hide(1000); 
  }
  $("#sig_si").attr("disabled",false);
}
*/
function lugar_recidencia_btn_paso3() {
  if($('#lugar_recidencia').is(':checked')){
      $('.residenciaEx').css('display','block');
  }else{
    $('.residenciaEx').css('display','none'); 
  }
}
function otro_beneficiario_siguiente(){
  var aux=0; var tp; var tp2;
  var rela_jur = "";
  var id_bene_moral;
  var id_trans_bene;

  if($('#beneficiario_ option:selected').val()!=""){
    var parte=$('#beneficiario_ option:selected').val().split('-');
    tipo=parte[1];
    idbene=parte[0];
    if(tipo=="fisica") tipo=1;
    if(tipo=="moralfisica" || tipo=="moralmoral") tipo=2;
    if(tipo=="fideicomiso") tipo=3;
    //if(tipo==1){
      $.ajax({
          type: "POST",
          url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_transaccion",
          data:{id_tipo_perfil:$('#idtipo_cliente').val(),
                id_perfilamiento:$('#idperfilamiento_cliente').val(),
                id_actividad:$('#idactividad').val(),
                tipo_persona:tipo,
                dueno_beneficiario:$('#idciente').val(),
                id_beneficiario: idbene,
                id_cli_opera: 0
              }, 
          success: function (data){
            //console.log('guardado bene Transaccion');
            var array = $.parseJSON(data);
            redirectAnexo(array.id,array.id_opera);
          }
      });
    /*}
    if(tipo==2){
    }*/
  }
  /// ES para validar los campos que esten llenos dependiendo el tipo de persona
  if($('#otro_beneficiario').is(':checked')){
  var form_register;
  if($('#tipo_persona option:selected').val()==1){
    form_register = $('#form_beneficiario_fisica');
  }
  if($('#tipo_persona option:selected').val()==2){
    form_register = $('#form_beneficiario_moral');
  }
  if($('#tipo_persona option:selected').val()==3){
    form_register = $('#form_beneficiario_fideicomiso');
  }
  var error_register = $('.alert-danger', form_register);
  var success_register = $('.alert-success', form_register);
  var $validator1;
  if($('#tipo_persona option:selected').val()==1){
    $validator1=form_register.validate({
      errorElement: 'div', //default input error message container
      errorClass: 'vd_red', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
          nombre:{
            required: true
          },
          apellido_paterno:{
            required: true
          },
          apellido_materno:{
            required: true
          }
      }
    });
  }
  if($('#tipo_persona option:selected').val()==2){
   $validator1=form_register.validate({
      errorElement: 'div', //default input error message container
      errorClass: 'vd_red', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
          razon_social:{
            required: true
          },
          fecha_constitucion:{
            required: true
          },
          rfc:{
            required: true,
            rfc: true
          }
      }
    });
  }
  if($('#tipo_persona option:selected').val()==3){
    $validator1=form_register.validate({
      errorElement: 'div', //default input error message container
      errorClass: 'vd_red', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
          razon_social:{
            required: true
          },
          rfc:{
            required: true,
            rfc: true
          },
          referencia:{
            required: true
          }
      }
    });
  }
  var $valid = form_register.valid();
    if($valid) {
        if($('#tipo_persona option:selected').val()==1){
          aux=1; 
        }
        if($('#tipo_persona option:selected').val()==2){
          aux=2;
        }
        if($('#tipo_persona option:selected').val()==3){
          aux=3;
        }
        /*
        if($('#tipo_persona_mfs1').is(':checked'))
          tp=1; //a
        if($('#tipo_persona_mms1').is(':checked'))
          tp=2; //b
        if($('#tipo_persona_mfs2').is(':checked'))
          tp2=1; //c 
        if($('#tipo_persona_mms2').is(':checked'))
          tp2=2; //d

        if($('#fisicab').is(':checked')) //socio 1
          tp=1; //persona fisica
        if($('#moralb').is(':checked')) // socio 1
          tp=2; // persona moral
        if($('#fisica2b').is(':checked')) // socio 2
          tp2=1; // persona fisica 
        if($('#moral2b').is(':checked')) // socio 2
          tp2=2; // persona moral
        */
        if(aux==1){ //if de persona fisica
          $.ajax({
              type: "POST",
              url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_transaccion",
              data:{id_tipo_perfil:$('#idtipo_cliente').val(),
                    id_perfilamiento:$('#idperfilamiento_cliente').val(),
                    id_actividad:$('#idactividad').val(),
                    tipo_persona:aux,
                    dueno_beneficiario:$('#idciente').val(),
                    id_cli_opera: 0}, //aqui deberia ser id beneficiario tabla (fisica o moral)
              success: function (data){
                var array = $.parseJSON(data);
                datos_fisica = $('#form_beneficiario_fisica').serialize()+'&id_beneficiario_fisica='+array.id+'&id_clientec='+$('#idciente').val()+'&idtipo_cliente='+$('#idperfilamiento_cliente').val();
                var id_trans_bene = array.id;
                console.log('id trans bene');
                $.ajax({
                    type: "POST",
                    url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_fisica",
                    data: datos_fisica,
                    success: function (data){
                      var id_benef = data;
                      swal("Éxito", "Guardado Correctamente", "success");
                      window.location.href = base_url+"index.php/Clientes_cliente/docs_beneficiario/"+id_benef+"/"+id_trans_bene+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/"+$('#idciente').val();
                      /*$('#idciente').val()+"/"+$('#idtipo_cliente').val()+"/"+$('#idperfilamiento_cliente').val()+"/"+
                      $.ajax({ //edita el id del cliente beneficiario  
                        type: "POST",
                        url: base_url+"index.php/Clientes_cliente/editarBeneTransac",
                        data: {id_benef:id_benef, id_trans_bene:id_trans_bene},
                        success: function (datos){
                            console.log(datos); 
                            
                        }
                    });*/
                    /*
                      var idact=$('#idactividad').val();
                      if(idact==1)anexo="anexo1";if(idact==2)anexo="anexo2a";if(idact==3)anexo="anexo2b";if(idact==4)anexo="anexo2c";if(idact==5)anexo="anexo3";
                      if(idact==6)anexo="anexo4";if(idact==7)anexo="anexo5a";if(idact==8)anexo="anexo5b";if(idact==9)anexo="anexo6";if(idact==10)anexo="anexo7";
                      if(idact==11)anexo="anexo8";if(idact==12)anexo="anexo9";if(idact==13)anexo="anexo10"; if(idact==14)anexo="anexo11";if(idact==15)anexo="anexo12a";
                      if(idact==16)anexo="anexo12b";if(idact==17)anexo="anexo13";if(idact==18)anexo="anexo14";if(idact==19)anexo="anexo15";if(idact==20)anexo="anexo16";          //id de transaccion anexo
                      window.location.href = base_url+"index.php/Transaccion/"+anexo+"/"+$('#idciente').val()+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/0/"+id_trans_bene;
                    */
                    // Datos de la URL id_cliente_cliente//tipo_cliente//idperfilamiento//

                    }
                });
              }
          });
        }//termina if de persona fisica
        if(aux==2){ //if de persona moral
          $.ajax({
              type: "POST",
              url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_transaccion",
              data:{id_tipo_perfil:$('#idtipo_cliente').val(),
                    id_perfilamiento:$('#idperfilamiento_cliente').val(),
                    id_actividad:$('#idactividad').val(),
                    tipo_persona:aux,
                    dueno_beneficiario:$('#idciente').val(),
                    id_cli_opera: 0}, //aqui deberia ser id beneficiario tabla (fisica o moral)
              success: function (data){
                var array = $.parseJSON(data);
                datos_moral = 'id_bene_trans='+array.id+'&tipo_cliente='+$('#idtipo_cliente').val()+'&id_perfilamiento='+$('#idperfilamiento_cliente').val()+'&identificacion_beneficiario=m'+'&tipo_persona='+2;
                var id_trans_bene = array.id;
                console.log('id trans bene');
                $.ajax({
                    type: "POST",
                    url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_moral",
                    data: datos_moral,
                    success: function (data){
                      datos_moral2 = $('#form_beneficiario_moral').serialize()+'&id_bene_moral='+data;
                      $.ajax({
                        type: "POST",
                        url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_moral2",
                        data: datos_moral2,
                        success: function (data){
                          var id_benef = data;
                          swal("Éxito", "Guardado Correctamente", "success");
                          window.location.href = base_url+"index.php/Clientes_cliente/docs_beneficiario/"+id_benef+"/"+id_trans_bene+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/"+$('#idciente').val();
                        }
                      });

                    }
                });
              }
          });
        }//termina if de persona mora
        if(aux==3){ //if de persona fideicomiso
          $.ajax({
              type: "POST",
              url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_transaccion",
              data:{id_tipo_perfil:$('#idtipo_cliente').val(),
                    id_perfilamiento:$('#idperfilamiento_cliente').val(),
                    id_actividad:$('#idactividad').val(),
                    tipo_persona:aux,
                    dueno_beneficiario:$('#idciente').val(),
                    id_cli_opera: 0}, //aqui deberia ser id beneficiario tabla (fisica o moral)
              success: function (data){
                var array = $.parseJSON(data);
                datos_moral = 'id_bene_trans='+array.id+'&tipo_cliente='+$('#idtipo_cliente').val()+'&id_perfilamiento='+$('#idperfilamiento_cliente').val()+'&identificacion_beneficiario=f'+'&tipo_persona='+3;
                var id_trans_bene = array.id;
                console.log('id trans bene');
                $.ajax({
                    type: "POST",
                    url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_moral",
                    data: datos_moral,
                    success: function (data){
                      datos_moral2 = $('#form_beneficiario_fideicomiso').serialize()+'&id_bene_moral='+data;
                      $.ajax({
                        type: "POST",
                        url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_fideicomiso",
                        data: datos_moral2,
                        success: function (data){
                          var id_benef = data;
                          swal("Éxito", "Guardado Correctamente", "success");
                          window.location.href = base_url+"index.php/Clientes_cliente/docs_beneficiario/"+id_benef+"/"+id_trans_bene+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/"+$('#idciente').val();
                        }
                      });

                    }
                });
              }
          });
        }//termina if de persona fideicomiso
        /*
        if(aux==2){ //if de persona moral
          if($("#accionaria_socio_a").is(':checked')) inciso="a"; if($("#accionaria_socio_b").is(':checked')) { inciso="b"; rela_jur=$("#relacion_juridica").val(); }
          if($("#accionaria_socio_c").is(':checked')) inciso="c"; if($("#accionaria_socio_d").is(':checked')) inciso="d"; 

          if(inciso=="a" || inciso=="b"){
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Clientes_cliente/add_transac_bm",
              data:{id_tipo_perfil:$('#idtipo_cliente').val(),
                    id_perfilamiento:$('#idperfilamiento_cliente').val(),
                    id_actividad:$('#idactividad').val(),
                    tipo_persona:aux,
                    dueno_beneficiario:$('#idciente').val(),
                    tipo_inciso: inciso,
                    tipo_p: tp
                  },
              statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra la ruta", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
              },
              success: function (data){
                //var id_bene_moral = data;
                var array = $.parseJSON(data);
                id_bene_moral = array.idbm;
                id_trans_bene = array.id;

                //console.log(id_bene_moral);
                //console.log(id_trans_bene);

                if(tp==1){ //datos para el socio 1 a) FISICA
                  datos_moral=$('#form_bene_moral_fisica_s1').serialize(); var gene="";
                  if($('#generom1').is(':checked')) gene="m"; if($('#generof1').is(':checked')) gene="f";
                  tabla = "beneficiario_moral_fisica";
                  //if($("#nombre").val()!=""){  
                    $.ajax({ //datos para el socio 1
                      type: "POST",
                      url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                      data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje1").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                      statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra la ruta", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                      },
                      success: function (data){
                        //swal("Éxito", "Socio 1 guardado correctamente", "success");
                        //console.log("Socio fisico 1 guardado correctamente");
                      },
                    });
                  //}
                }//if tp
                if(tp==2){ //datos para el socio 1 a) MORAL
                  datos_moralf = $('#form_bene_moral_moral_s1').serialize(); var gene="";
                  tabla = "beneficiario_moral_moral";
                  $.ajax({ //datos para ingresar el socio 1 moral 
                    type: "POST",
                    url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                    data: datos_moralf+'&id_bene_moral='+id_bene_moral+'&porcentaje='+$("#porcentaje1").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                    statusCode:{
                      404: function(data){
                          swal("Error!", "No Se encuentra la ruta", "error");
                      },
                      500: function(){
                          swal("Error!", "500", "error");
                      }
                    },
                    success: function (data){
                      datos_moral = $('#form_bene_moral_fisica_s12').serialize(); //socio fisico 1
                      if($('#generom2').is(':checked')) gene="m"; if($('#generof2').is(':checked')) gene="f";
                      tabla = "beneficiario_moral_fisica";
                      $.ajax({ //datos para el socio 1
                        type: "POST",
                        url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                        data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje2").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                        statusCode:{
                          404: function(data){
                              swal("Error!", "No Se encuentra la ruta", "error");
                          },
                          500: function(){
                              swal("Error!", "500", "error");
                          }
                        },
                        success: function (data){
                          datos_moral = $('#form_bene_fisica_fisica_s12_2').serialize(); //socio fisico 2
                          if($('#generom3').is(':checked')) gene="m"; if($('#generof3').is(':checked')) gene="f";
                          tabla = "beneficiario_moral_fisica";
                          $.ajax({ //datos para el socio 1
                            type: "POST",
                            url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                            data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje3").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                            statusCode:{
                              404: function(data){
                                  swal("Error!", "No Se encuentra la ruta", "error");
                              },
                              500: function(){
                                  swal("Error!", "500", "error");
                              }
                            },
                            success: function (data){
                              swal("Éxito", "Guardado correctamente", "success");
                            }
                          });

                        }
                      });
                    }//succes principal
                  });
                }//if tp=2

                $.ajax({
                  type: "POST",
                  url: base_url+"index.php/Clientes_cliente/add_transac_bm",
                  data:{id_tipo_perfil:$('#idtipo_cliente').val(),
                        id_perfilamiento:$('#idperfilamiento_cliente').val(),
                        id_actividad:$('#idactividad').val(),
                        tipo_persona:aux,
                        dueno_beneficiario:$('#idciente').val(), //aqui deberia ser id beneficiario tabla (fisica o moral)
                        tipo_inciso: inciso,
                        tipo_p: tp2
                      },
                  statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra la ruta", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                  },
                  success: function (data){
                    //var id_bene_moral = data; 
                    var array = $.parseJSON(data);
                    id_bene_moral = array.idbm;
                    id_trans_bene = array.id;
                    var gene="";
                    if(tp2==1){ //datos para el socio 1 a) FISICA
                      datos_moral = $('#form_bene_moral_fisica_s2').serialize();
                      if($('#generom4').is(':checked')) gene="m"; if($('#generof4').is(':checked')) gene="f";
                      tabla = "beneficiario_moral_fisica";
                      $.ajax({ //datos para el socio 1
                        type: "POST",
                        url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                        data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje4").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                        statusCode:{
                          404: function(data){
                              swal("Error!", "No Se encuentra la ruta", "error");
                          },
                          500: function(){
                              swal("Error!", "500", "error");
                          }
                        },
                        success: function (data){
                          swal("Éxito", "Socio guardado correctamente", "success");
                          //console.log("Socio fisico 2 guardado correctamente");
                      
                          window.location.href = base_url+"index.php/Clientes_cliente/docs_beneficiario/"+id_bene_moral+"/"+id_trans_bene+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/"+$('#idciente').val();
                        }
                      });//ajax
                    }//if tp2
                    if(tp2==2){ //datos para el socio 1 a) MORAL
                      datos_moralf = $('#form_bene_moral_moral_s1').serialize(); var gene="";
                      tabla = "beneficiario_moral_moral";
                      $.ajax({ //datos para ingresar el socio 1 moral 
                        type: "POST",
                        url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                        data: datos_moralf+'&id_bene_moral='+id_bene_moral+'&porcentaje='+$("#porcentaje4").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                        statusCode:{
                          404: function(data){
                              swal("Error!", "No Se encuentra la ruta", "error");
                          },
                          500: function(){
                              swal("Error!", "500", "error");
                          }
                        },
                        success: function (data){
                          datos_moral = $('#form_bene_moral_fisica_s22').serialize(); //socio fisico 1
                          if($('#generom5').is(':checked')) gene="m"; if($('#generof5').is(':checked')) gene="f";
                          tabla = "beneficiario_moral_fisica";
                          $.ajax({ //datos para el socio 1
                            type: "POST",
                            url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                            data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje5").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                            statusCode:{
                              404: function(data){
                                  swal("Error!", "No Se encuentra la ruta", "error");
                              },
                              500: function(){
                                  swal("Error!", "500", "error");
                              }
                            },
                            success: function (data){
                              datos_moral = $('#form_bene_fisica_fisica_s22_2').serialize(); //socio fisico 2
                              if($('#generom6').is(':checked')) gene="m"; if($('#generof6').is(':checked')) gene="f";
                              tabla = "beneficiario_moral_fisica";
                              $.ajax({ //datos para el socio 1
                                type: "POST",
                                url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
                                data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje6").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
                                statusCode:{
                                  404: function(data){
                                      swal("Error!", "No Se encuentra la ruta", "error");
                                  },
                                  500: function(){
                                      swal("Error!", "500", "error");
                                  }
                                },
                                success: function (data){
                                  swal("Éxito", "Guardado correctamente", "success");
                                  setTimeout(function(){ 
                                    redirectAnexo(id_trans_bene);
                                  }, 1500);
                                }
                              });

                            }
                          });
                        }//succes principal
                      });
                    }//if tp2=2
                  }//success
                });//ajax segundo
                
              } //success primero
            });
          }//tipo a o b
          if(inciso=="c" || inciso=="d"){
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Clientes_cliente/add_transac_bm",
              data:{id_tipo_perfil:$('#idtipo_cliente').val(),
                      id_perfilamiento:$('#idperfilamiento_cliente').val(),
                      id_actividad:$('#idactividad').val(),
                      tipo_persona:aux,
                      dueno_beneficiario:$('#idciente').val(),
                      tipo_inciso: inciso,
                      tipo_p: "0"
                    },
              statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra la ruta", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
              },
              success: function (data){
                  var array = $.parseJSON(data);
                    id_bene_moral = array.idbm;
                    id_trans_bene = array.id;
                  var aux_t;
                  var datos;
                  if($('#accionaria_socio_c').is(':checked')){
                    aux_t = 'c';
                      if($('#persona_fisica_c').is(':checked')){
                        datos = $('#beneficiario_moral_f').serialize()+'&tipo_persona='+1+'&tipo_c_d='+aux_t+'&id_bene_moral='+id_bene_moral;
                    } 
                    if($('#persona_m_f_c').is(':checked')){
                        datos = $('#beneficiario_moral_m').serialize()+'&tipo_persona='+2+'&tipo_c_d='+aux_t+'&id_bene_moral='+id_bene_moral;
                    } 
                  } 
                  if($('#accionaria_socio_d').is(':checked')){
                      aux_t = 'd';
                      datos = $('#beneficiario_moral_d').serialize()+'&tipo_c_d='+aux_t+'&id_bene_moral='+id_bene_moral;
                  }   
                  $.ajax({ //datos para el socio 1
                      type: "POST",
                      url: base_url+"Clientes_cliente/guardar_persona_c_d",
                      data: datos,
                      success: function (data){
                        swal("Éxito", "Guardado correctamente", "success");
                        setTimeout(function(){ 
                          //redirectAnexo(id_trans_bene);
                          window.location.href = base_url+"index.php/Clientes_cliente/docs_beneficiario/"+id_bene_moral+"/"+id_trans_bene+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/"+$('#idciente').val();
                        }, 1500);
                      }
                  });
                }
            });
          }

        }//termina if de persona moral
        */
    }
  }
} 

function redirectAnexo(id_trans_bene,idopera){
  var idact=$('#idactividad').val();
  if(idact==1)anexo="anexo1";if(idact==2)anexo="anexo2a";if(idact==3)anexo="anexo2b";if(idact==4)anexo="anexo2c";if(idact==5)anexo="anexo3";
  if(idact==6)anexo="anexo4";if(idact==7)anexo="anexo5a";if(idact==8)anexo="anexo5b";if(idact==9)anexo="anexo6";if(idact==10)anexo="anexo7";
  if(idact==11)anexo="anexo8";if(idact==12)anexo="anexo9";if(idact==13)anexo="anexo10"; if(idact==14)anexo="anexo11";if(idact==15)anexo="anexo12a";
  if(idact==16)anexo="anexo12b";if(idact==17)anexo="anexo13";if(idact==18)anexo="anexo14";if(idact==19)anexo="anexo15";if(idact==20)anexo="anexo16";           //id de transaccion anexo
  window.location.href = base_url+"index.php/Transaccion/"+anexo+"/0/"+$('#idciente').val()+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/0/"+id_trans_bene+"/"+idopera;
}

function otro_beneficiario_siguiente2(){
  var idact=$('#idactividad').val();
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Clientes_cliente/guardar_beneficiario_transaccion",
      data:{id_tipo_perfil:$('#idtipo_cliente').val(),
        id_perfilamiento:$('#idperfilamiento_cliente').val(),
        id_actividad:$('#idactividad').val(),
        tipo_persona:"0",
        dueno_beneficiario:"0",
        id_cli_opera: 0
      },
      success: function (data){
        var array = $.parseJSON(data);
        swal("Éxito", "Guardado Correctamente", "success");
        var idact=$('#idactividad').val();
        if(idact==1)anexo="anexo1";if(idact==2)anexo="anexo2a";if(idact==3)anexo="anexo2b";if(idact==4)anexo="anexo2c";if(idact==5)anexo="anexo3";
        if(idact==6)anexo="anexo4";if(idact==7)anexo="anexo5a";if(idact==8)anexo="anexo5b";if(idact==9)anexo="anexo6";if(idact==10)anexo="anexo7";
        if(idact==11)anexo="anexo8";if(idact==12)anexo="anexo9";if(idact==13)anexo="anexo10"; if(idact==14)anexo="anexo11";if(idact==15)anexo="anexo12a";
        if(idact==16)anexo="anexo12b";if(idact==17)anexo="anexo13";if(idact==18)anexo="anexo14";if(idact==19)anexo="anexo15";if(idact==20)anexo="anexo16";
        window.location.href = base_url+"index.php/Transaccion/"+anexo+"/0/"+$('#idciente').val()+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/0/"+array.id+"/"+array.id_opera;
      }
  });
}

function getpais(id){
  $('.pais'+id).select2({
      width: 255,
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Clientes_cliente/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
function getpais2(id){

    $('.pais'+id).select2({
        width: 255,
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Clientes_cliente/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var pais = $('.pais'+id+' option:selected').val(); 
      if(pais=='MX'){
       //<!-------->
       //console.log(pais); 
        $.ajax({
          type:'POST',
          data: {id:id},
          url: base_url+'Clientes_cliente/get_estadoMoral',
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
            //console.log(id);
            $('.span_div_'+id).html(data);
            //console.log(data);
            //$('.estado'+id+' option[value=""]').attr("selected", "selected");
          }  
        });
       //<!-------->   
      }else{
        $('.span_div_'+id).html('<input class="form-control" type="text" name="estado" id="estado">'); 
      }
      
    });
}
function get_pais_aux(text,num){

    $('.idpais_'+text+'_'+num).select2({
        width: 255,
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Clientes_cliente/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var pais = $('.idpais_'+text+'_'+num+' option:selected').val(); 
      if(pais=='MX'){
      //<!-------->
        $.ajax({
          type:'POST',
          data: {text_t:text,id_t:num},
          url: base_url+'Clientes_cliente/get_estados',
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
            $('.span_div_'+text+'_'+num).html(data);
          }  
        });
       //<!-------->   
      }else{
        $('.span_div_'+text+'_'+num).html('<input class="form-control" type="text" name="estado" id="estado">'); 
      }
      
    });
}
// Verificar si es mexico se seleciona los estados   idpais_t_12 span_div_t_12
function pais_aux(text,num){
  var pais = $('.idpais_'+text+'_'+num+' option:selected').val();       
  if(pais=='MX'){
   //<!-------->
    $.ajax({
      type:'POST',
      data: {text_t:text,id_t:num},
      url: base_url+'Clientes_cliente/get_estados',
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        $('.span_div_'+text+'_'+num).html(data);
      }  
    });
   //<!-------->   
  }else{
    $('.span_div_'+text+'_'+num).html('<input class="form-control" type="text" name="estado" id="estado">'); 
  }
}

/*
function otro_beneficiario_siguiente(){
  var idact=$('#idactividad').val();
  if(idact==1)anexo="anexo1";if(idact==2)anexo="anexo2a";if(idact==3)anexo="anexo2b";if(idact==4)anexo="anexo2c";if(idact==5)anexo="anexo3";
    if(idact==6)anexo="anexo4";if(idact==7)anexo="anexo5a";if(idact==8)anexo="anexo5b";if(idact==9)anexo="anexo6";if(idact==10)anexo="anexo7";
    if(idact==11)anexo="anexo8";if(idact==12)anexo="anexo9";if(idact==13)anexo="anexo10"; if(idact==14)anexo="anexo11";if(idact==15)anexo="anexo12a";
    if(idact==16)anexo="anexo12b";if(idact==17)anexo="anexo13";if(idact==18)anexo="anexo14";if(idact==19)anexo="anexo15";if(idact==20)anexo="anexo16";
    window.location.href = base_url+"index.php/Transaccion/"+anexo+"/"+$('#idciente').val()+"/"+$('#idactividad').val()+"/"+$('#idperfilamiento_cliente').val()+"/0";
}
*/