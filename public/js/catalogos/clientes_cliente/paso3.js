$(document).ready(function(){


	$('#accionaria_socio_a').click(function(event) {
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"a" },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      }
	    });
	});
	$('#accionaria_socio_b').click(function(event) {
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"b" },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      }
	    });
	});
	$('#accionaria_socio_c').click(function(event) {
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"c" },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      }
	    });
	});
	$('#accionaria_socio_d').click(function(event) {
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"d" },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      }
	    });
	});

  $('#accionaria_socio_a').click(function(event) {
    $(".accion_socio_a").show("slow");
    $(".accion_socio_b").hide("slow");
    $(".accion_socio_c").hide("slow");
    $(".accion_socio_d").hide("slow");
  });

  $('#accionaria_socio_b').click(function(event) {
    $(".accion_socio_b").show("slow");
    $(".accion_socio_a").hide("slow");
    $(".accion_socio_c").hide("slow");
    $(".accion_socio_d").hide("slow");
  });
  $('#accionaria_socio_c').click(function(event) {
    $(".accion_socio_c").show("slow");
    $(".accion_socio_a").hide("slow");
    $(".accion_socio_b").hide("slow");
    $(".accion_socio_d").hide("slow");
  });
  $('#accionaria_socio_d').click(function(event) {
    $(".accion_socio_d").show("slow");
    $(".accion_socio_a").hide("slow");
    $(".accion_socio_b").hide("slow");
    $(".accion_socio_c").hide("slow");
  });
  $('#tipo_persona_mfs1').click(function(event) {
    $(".pers_fisica_s1").show("slow");
    $(".pers_moral_s1").hide("slow");
  });
  $('#tipo_persona_mms1').click(function(event) {
    $(".pers_fisica_s1").hide("slow");
    $(".pers_moral_s1").show("slow");
  });
  $('#tipo_persona_mfs2').click(function(event) {
    $(".pers_fisica_s2").show("slow");
    $(".pers_moral_s2").hide("slow");
  });
  $('#tipo_persona_mms2').click(function(event) {
    $(".pers_fisica_s2").hide("slow");
    $(".pers_moral_s2").show("slow");
  });

  //////////////////////// B ///////////////
  if($('#fisicab').is(':checked')){
  	$('#pers_fisicab_s1').css('display','block');
  }
  if($('#fisica2b').is(':checked')){
  	$('#pers_fisicab_s2').css('display','block');
  }
  $('#fisicab').click(function(event) {
    $(".pers_fisicab_s1").show("slow");
    $(".pers_moralb_s1").hide("slow");
  });
  $('#moralb').click(function(event) {
    $(".pers_fisicab_s1").hide("slow");
    $(".pers_moralb_s1").show("slow");
  });
  $('#fisica2b').click(function(event) {
    $(".pers_fisicab_s2").show("slow");
    $(".pers_moralb_s2").hide("slow");
  });
  $('#moral2b').click(function(event) {
    $(".pers_fisicab_s2").hide("slow");
    $(".pers_moralb_s2").show("slow");
  });
	$('#lugar_r_sociob').click(function(event) { //persona fisica
	  	if($('#lugar_r_sociob').is(':checked')){
	      $('.residencia_extranjerob').css('display','block');
		}else{
		  $('.residencia_extranjerob').css('display','none');	
		}
	});
	$('#lugar_r_socio1_b').click(function(event) { //persona moral socio 1
	  	if($('#lugar_r_socio1_b').is(':checked')){
	      $('.r_extranjero_socio_accion_b').css('display','block');
		}else{
		  $('.r_extranjero_socio_accion_b').css('display','none');	
		}
	});
	$('#lugar_r_socio2_b').click(function(event) { //persona moral socio 2
	  	if($('#lugar_r_socio2_b').is(':checked')){
	      $('.r_extranjero_socio2_accion_b').css('display','block');
		}else{
		  $('.r_extranjero_socio2_accion_b').css('display','none');	
		}
	});

	$('#lugar_r_socio2fb').click(function(event) { //persona fisica
	  	if($('#lugar_r_socio2fb').is(':checked')){
	      $('.residencia_extranjero2b').css('display','block');
		}else{
		  $('.residencia_extranjero2b').css('display','none');	
		}
	});
	$('#lugar_r_socio2_mb').click(function(event) { //persona moral socio 1
	  	if($('#lugar_r_socio2_mb').is(':checked')){
	      $('.r_extranjero_socio2_accion_2b').css('display','block');
		}else{
		  $('.r_extranjero_socio2_accion_2b').css('display','none');	
		}
	});
	$('#lugar_r_socio2_m2b').click(function(event) { //persona moral socio 2
	  	if($('#lugar_r_socio2_m2b').is(':checked')){
	      $('.r_extranjero_socio2_accion2_2b').css('display','block');
		}else{
		  $('.r_extranjero_socio2_accion2_2b').css('display','none');	
		}
	});


   ////////////////////////// C ////////////////
   $('#persona_fisica_c').click(function(event) {
    $("#pf_c").show("slow");
    $("#pmfd_c").hide("slow");
  });
  $('#persona_m_f_c').click(function(event) {
    $("#pf_c").hide("slow");
    $("#pmfd_c").show("slow");
  });

});


function especifique_btnpaso3() {
	if($('#check_direccion_a').is(':checked')){
      $('#especifique').css('display','block');
	}else{
	  $('#especifique').css('display','none');	
	}
}
function lugar_recidencia_btn_paso3() {
	if($('#lugar_recidencia').is(':checked')){
      $('.residenciaEx').css('display','block');
	}else{
	  $('.residenciaEx').css('display','none');	
	}
}
function lugar_recidencia_btn_socio() {
	if($('#lugar_recidencia_socio').is(':checked')){
      $('.residencia_extranjero').css('display','block');
	}else{
	  $('.residencia_extranjero').css('display','none');	
	}
}
function lugar_recidencia_btn_socio2() {
	if($('#lugar_recidencia_socio2').is(':checked')){
      $('.residencia_extranjero2').css('display','block');
	}else{
	  $('.residencia_extranjero2').css('display','none');	
	}
}
function accionaria_socioa_a_btn() {
	if($('#accionaria_socioa_a').is(':checked')){
      $('.accion_socio_a').css('display','block');
	}else{
	  $('.accion_socio_a').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_2_accion_a() {
	if($('#lugar_recidencia_socio_2_a').is(':checked')){
      $('.residencia_extranjero_socio2_accion_a').css('display','block');
	}else{
	  $('.residencia_extranjero_socio2_accion_a').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_2_2_accion_a() {
	if($('#lugar_recidencia_socio_2_2_a').is(':checked')){
      $('.residencia_extranjero_socio2_2_accion_a').css('display','block');
	}else{
	  $('.residencia_extranjero_socio2_2_accion_a').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_accion_a_socio2() {
	if($('#lugar_recidencia_socio2_accion_a').is(':checked')){
      $('.residencia_extranjero_accion_a_socio2').css('display','block');
	}else{
	  $('.residencia_extranjero_accion_a_socio2').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_accion_a_socio2_2() {
	if($('#lugar_recidencia_socio2_accion_a2').is(':checked')){
      $('.residencia_extranjero_accion_a_socio2_2').css('display','block');
	}else{
	  $('.residencia_extranjero_accion_a_socio2_2').css('display','none');	
	}
}
//////////////// b //////////////////////
function accionaria_socioa_b_btn() {
	if($('#lugar_recidencia_b').is(':checked')){
      $('.accion_socio_b').css('display','block');
	}else{
	  $('.accion_socio_b').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_b() {
	if($('#lugar_recidencia_socio_b').is(':checked')){
      $('.residencia_extranjero_b').css('display','block');
	}else{
	  $('.residencia_extranjero_b').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_2_accion_b() {
	if($('#lugar_recidencia_socio_2_b').is(':checked')){
      $('.residencia_extranjero_socio2_accion_b').css('display','block');
	}else{
	  $('.residencia_extranjero_socio2_accion_b').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_accion_b_socio2() {
	if($('#lugar_recidencia_socio2_accion_b').is(':checked')){
      $('.residencia_extranjero_accion_b_socio2').css('display','block');
	}else{
	  $('.residencia_extranjero_accion_b_socio2').css('display','none');	
	}
}