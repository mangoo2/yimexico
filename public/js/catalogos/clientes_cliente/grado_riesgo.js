
$(document).ready(function(){
	mostraroptionclasificacion();
	$("#btn_submit").on("click",function(){
		guardar();
	});

	if($("#id").val()>0){
		$("#txt_title").append(": Consultar calificación");
        setTimeout(function(){ 
            limpiarForm2();
        }, 1500);
        
    }
	else{
		$("#txt_title").append(": Asignar calificación");
	}

  	$("#actividad_monto").on("change",function(){
  		calculaGrado();
  	});
  	$("#idtipo_cliente").on("change",function(){
  		limpiarForm();
  		mostraroptionclasificacion();
        selectAutoTipo();
  		calculaGrado();
  	});
  	$("#detalle_tipo_cli").on("change",function(){
  		mostraroptionclasificacion();
  		calculaGrado();
  	});
  	$("#clasificacion").on("change",function(){
      mostraroptionclasificacion();
      preguntasPEP(); //agregado reciente. agregar a demas calcs
  		calculaGrado();
  	});
  	$("#edad").on("change",function(){
  		calculaGrado();
  	});
  	$("#nacionalidad").on("change",function(){
  		calculaGrado();
  	});
  	$("#estado").on("change",function(){
  		calculaGrado();
  	});
  	$("#estructura").on("change",function(){
  		calculaGrado();
  	});
  	$("#historico").on("change",function(){
  		calculaGrado();
  	});
  	$("#paises").on("change",function(){
  		calculaGrado();
  	});
  	$("#transaccionalidad").on("change",function(){
  		calculaGrado();
  	});
  	$("#transaccionalidad_forma").on("change",function(){
  		calculaGrado();
  	});
  	$(".trans_pep").on("change",function(){
  		calculaGrado();
  	});
  	$("#c_d_agencia").on("change",function(){
  		calculaGrado();
  	});
  	$("#c_d_notaria").on("change",function(){
  		calculaGrado();
  	});
  	$("#c_d_cliente_usuario").on("change",function(){
  		calculaGrado();
  	});

  	//preguntasPEP();
  	$(".preg_pep_most").on("change", function(){
  		preguntasPEP();
  	});

  	setTimeout(function(){ 
      preguntasPEP();
    }, 1000);
    setTimeout(function(){ 
      calculaGrado();
    }, 1500);
  	
    $("#actividad_monto").select2({ theme: 'bootstrap4', width:'resolve' });
    $("#transaccionalidad_forma").select2({ theme: 'bootstrap4', width:'resolve' });
    //limpiarForm();

});

function selectAutoTipo(){
  if($("#idtipo_cliente option:selected").val()==6){
    $("#detalle_tipo_cli option:selected").val(7);
    $(".fides_tc").attr("selected",true);
  }
  else if($("#idtipo_cliente option:selected").val()==8){
    $("#detalle_tipo_cli option:selected").val(12);
    $(".emb").attr("selected",true);
  }else{
    $("#detalle_tipo_cli option:selected").val(0);
    $(".nda").attr("selected",true);
  }
}

function guardar(){
	$('#form_grado').removeData('validator');
	var form_register = $('#form_grado');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);
	//console.log("guardo y valido el cliente tipo: "+$("#idtipo_cliente option:selected").val());
	//console.log("form_register: "+form_register);
	if($("#idtipo_cliente option:selected").val()==1){
    var class_select=$("#clasificacion option:selected").val();
    if(class_select>=1 && class_select<=8 && $("#idact").val()==15 || class_select>=1 && class_select<=8 && $("#idact").val()==16 || class_select>=9 && class_select<=13 && $("#idact").val()!=15 && $("#idact").val()!=16){
        var $validator1=form_register.validate({ //para peps
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "input[type='text']:hidden",
                rules: {
                    clasificacion:{
                      required: true
                    },
                    transaccionalidad:{
                      required: true
                    },
                    transaccionalidad_forma:{
                      required: true
                    },
                    /*c_d_notaria:{
                      required: true
                    },*/
                    c_d_cliente_usuario:{
                      required: true
                    },
                    preg1:{
                      required: true
                    },
                    preg4:{
                      required: true
                    },
                    preg5:{
                      required: true
                    },
                    preg9:{
                      required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
        });

        if($("#preg5 option:selected").val()==2){
            //console.log("2do if de fisica en no lo pagará el cliente directamente: ");
            $('#form_grado').removeData('validator');
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                //ignore: "input[type='text']:hidden",
                rules: {
                    clasificacion:{
                      required: true
                    },
                    transaccionalidad:{
                      required: true
                    },
                    transaccionalidad_forma:{
                      required: true
                    },
                    /*c_d_notaria:{
                      required: true
                    },*/
                    c_d_cliente_usuario:{
                      required: true
                    },
                    preg1:{
                      required: true
                    },
                    preg4:{
                      required: true
                    },
                    preg5:{
                      required: true
                    },
                    preg9:{
                      required: true
                    },

                    preg6:{
                      required: true
                    },
                    preg7:{
                      required: true
                    },
                    preg8:{
                      required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
        }
    }
    if(class_select=="" || class_select==9 && $("#idact").val()==15 || class_select==9 && $("#idact").val()==16 || class_select==14 && $("#idact").val()!=15 || class_select==14 && $("#idact").val()!=16){
      var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "input[type='text']:hidden",
            rules: {
                clasificacion:{
                  required: true
                },
                transaccionalidad:{
                  required: true
                },
                transaccionalidad_forma:{
                  required: true
                },
                /*c_d_notaria:{
                  required: true
                },*/
                c_d_cliente_usuario:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
      });
    }
	}else if($("#idtipo_cliente option:selected").val()==2){
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "input[type='text']:hidden",
            rules: {
                estructura:{
                  required: true
                },
                transaccionalidad:{
                  required: true
                },
                transaccionalidad_forma:{
                  required: true
                },
                /*c_d_notaria:{
                  required: true
                },*/
                c_d_cliente_usuario:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
	}
	else if($("#idtipo_cliente option:selected").val()==3 || $("#idtipo_cliente option:selected").val()==4 || $("#idtipo_cliente option:selected").val()==6 || $("#idtipo_cliente option:selected").val()==7){
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "input[type='text']:hidden",
            rules: {
                detalle_tipo_cli:{
                  required: true
                },
                transaccionalidad:{
                  required: true
                },
                transaccionalidad_forma:{
                  required: true
                },
                /*c_d_notaria:{
                  required: true
                },*/
                c_d_cliente_usuario:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
	}else if($("#idtipo_cliente option:selected").val()==5){
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "input[type='text']:hidden",
            rules: {
                clasificacion:{ required: true },
                preg1:{ required: true },
                //preg6:{ required: true },
                preg4:{ required: true },
                preg5:{ required: true },
                preg9:{ required: true },
                
                //c_d_notaria:{ required: true },
                c_d_cliente_usuario: { required: true }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        if($("#preg5 option:selected").val()==2){
            //console.log("2do if de fisica en no lo pagará el cliente directamente: ");
            $('#form_grado').removeData('validator');
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                //ignore: "input[type='text']:hidden",
                rules: {
                    clasificacion:{ required: true },
                    preg1:{ required: true },
                    //preg6:{ required: true },
                    preg4:{ required: true },
                    preg5:{ required: true },
                    preg9:{ required: true },
                    //c_d_notaria:{ required: true },
                    c_d_cliente_usuario: { required: true },

                    preg6:{
                      required: true
                    },
                    preg7:{
                      required: true
                    },
                    preg8:{
                      required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
        }
	}
	var valid = form_register.valid();
    if(valid) {
    	var datos = form_register.serialize();
    	$.ajax({
             type: "POST",
             url: base_url+"Clientes_cliente/guardar_grado",
             data: datos+"&grado="+$("#grado_final").val()+"&id_operacion="+$("#idopera").val(),
             beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
             },
             success: function (result) {
                //console.log(result);
                $("#btn_submit").attr("disabled",false);
                swal("Éxito!", "Se han guardado los datos correctamente", "success");
                //setTimeout(function () { window.location.href = base_url+"Operaciones/proceso_operacion"+"/"+$("#idopera").val()+"/"+$("#idperfilamiento").val()+"/"+$("#idclientec").val() }, 1500);
                setTimeout(function () { 
                	var gf_aux=$("#grado_final").val();
                	var idact=$("#idact").val();
                	//history.back();
                	if($("#id_union").val()>0){
                		window.location.href = base_url+"Operaciones/gradoRiesgo"+"/"+$("#idopera").val()+"/1/"+$("#idperfilamiento").val(); 
                	}else{
                		//window.location.href = base_url+"Operaciones/procesoInicial"+"/"+$("#idopera").val(); 
                		window.location.href = base_url+"Operaciones/gradoRiesgo"+"/"+$("#idopera").val()+"/1/"+$("#idperfilamiento").val();
                	}
                	//mandar a grado riesgo tambien el id_perfilamiento para redireccionar en ese contralador a documentos o a cuestionario ampliado dependiendo el caso
                }, 1500);
             }
        });
    }
}

/*function preguntasPEP(){
	if($("#preg1 option:selected").val()==1){ //preg20 conteedor
		$("#preg21").show("slow");
		//$("#preg33").hide("slow");
	}else{ //no
		$("#preg21").hide("slow");
		//$("#preg33").show("slow");
	}

	if($("#preg3 option:selected").val()==1){ //en efectivo
		$("#preg25").hide("slow");
	}else if($("#preg3 option:selected").val()==2 || $("#preg3 option:selected").val()==3){ //en transferencia
		$("#preg25").show("slow");
	}

	if($("#preg3 option:selected").val()==2){ //osea 22 del formulario
		$("#preg26").show("slow");
		$("#preg27").show("slow");
	}else{
		$("#preg26").hide("slow");
		$("#preg27").hide("slow");
	}
  if($("#preg3 option:selected").val()==2 || $("#preg3 option:selected").val()==3){ //osea 22 del formulario
    $("#preg25").show("slow");
  }else{
    $("#preg25").hide("slow");
  }

	if($("#preg6 option:selected").val()==2){ //
		$("#preg29").show("slow");
		$("#preg30").show("slow");
	}else{
		$("#preg29").hide("slow");
		$("#preg30").hide("slow");
	}
}*/
function preguntasPEP(){
  if($("#preg1 option:selected").val()==3){ //preg22 conteedor
    $("#preg23").show("slow");
    $("#preg24").show("slow");
  }else{ //no
    $("#preg23").hide("slow");
    $("#preg24").hide("slow");
  }

  if($("#preg5 option:selected").val()==2){ //no lo liquida directamente el cliente
    $("#preg27").show("slow");
    $("#preg28").show("slow");
    $("#preg29").show("slow");
  }else{ //si lo liquida el cliente
    $("#preg27").hide("slow");
    $("#preg28").hide("slow");
    $("#preg29").hide("slow");
  }

    if($("#idact").val()==15 || $("#idact").val()==16){
        if($("#preg5 option:selected").val()==1){ 
            $("#preg26_1").show("slow");
        }else{ //no
            $("#preg26_1").hide("slow");
        } 
    }
}

function calculaGrado(){
	var clasificacion=0;
	monto=0; historico=0;
	/*if($("#idact").val()==11){
		monto = $("#actividad_monto").val();
	}*/
	if($("#idact").val()==11 || $("#idact").val()==12 || $("#idact").val()==15 || $("#idact").val()==16){ //blindaje, notarios 12a,12b
		monto = $("#actividad_monto option:selected").data("nivelriesgo");
	}
	tipo = $("#idtipo_cliente option:selected").val();
	//detalle_tipo = $("#detalle_tipo_cli").val();
	detalle_tipo = $("#detalle_tipo_cli option:selected").data("nivelriesgo");
	edad = $("#edad").val();
	nacionalidad = $("#nacionalidad option:selected").val();
	profesion = $("#profesion option:selected").val();
	estructura = $("#estructura option:selected").val();
	if($("#idact").val()==11 || $("#idact").val()==12 || $("#idact").val()==15 || $("#idact").val()==16){
		historico = $("#historico option:selected").val();
	}
	paises = $("#paises option:selected").val();
	transac = $("#transaccionalidad option:selected").val();


  if(tipo==1 || tipo==5)
    $("#name_edad").html("Edad");
  else
    $("#name_edad").html("Constitución");

	calc_monto=0;
	calc_clasificacion=0;
	calc_edad=0;
	calc_nac=0;
	calc_hist=0;
	calc_prof=0;
	calc_estru=0;
	calc_tipo=0;
	calc_pais=0;
	calc_trans=0;
	calc_transp1=0;
	calc_transp2=0;

	/*if($("#idact").val()==11){ //vehiculos
		if(monto==1){ //Productos y Servicios
			calc_monto=1;
		}else if(monto==2){ //Productos y Servicios
			calc_monto=2;
		}else if(monto==3){ //Productos y Servicios
			calc_monto=3;
		}
	}
	if($("#idact").val()==15 || $("#idact").val()==16){
		if(monto==1){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==2){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==3){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==4){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==5){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==6){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==7){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==8){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==9){ //Productos y Servicios
			calc_monto=1;
		}else if(monto==10){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==11){ //Productos y Servicios
			calc_monto=3;
		}else if(monto==12){ //Productos y Servicios
			calc_monto=1;
		}
	}*/

	calc_monto=$("#actividad_monto option:selected").data("nivelriesgo");
	if(tipo==1){ //Persona Física Nacionalidad Mexicana y Extranjera
		/*if(edad==1){
			calc_edad=1;
		}else if(edad==2){
			calc_edad=2;
		}
		else if(edad==3){
			calc_edad=3;
		}
		else if(edad==4){
			calc_edad=0;
		}*/
		calc_edad = $("#edad option:selected").data("nivelriesgo");
		calc_clasificacion=$("#clasificacion option:selected").data("nivelriesgo"); //tipo de cliente

	}
	
	if(tipo==1){ //Persona Física Nacionalidad Mexicana y Extranjera
		calc_nac=0;
		if(nacionalidad==1){
			calc_nac=1;
        }
		else if(nacionalidad==2){
            if($("input[name*='id_actividad']").val()==11){
			    calc_nac=2.5; //cambio nueva calc para anexo8 vehiculos -- 2da version de ajuste 18-04-22
            }else{
                calc_nac=2.5;  
            }
        }
	}
	
	if(tipo==1 && $("#idact").val()==11){ //Persona Física Nacionalidad Mexicana y Extranjera -- anexo8 avehiculos
		if(historico==1)
			calc_hist=1;
		else if(historico==2)
			calc_hist=2.5; //cambios de ajuste para la ultima version
		else if(historico==3)
			calc_hist=4; //cambios de ajuste para la ultima version
	}
    if(tipo==1 && $("#idact").val()==12){ //Persona Física Nacionalidad Mexicana y Extranjera -- anexo9 blindaje
        if(historico==1)
            calc_hist=1;
        else if(historico==2)
            calc_hist=2.5;
        else if(historico==3)
            calc_hist=4;
    }
	if(tipo==1 && $("#idact").val()==15 || tipo==1 && $("#idact").val()==16){ //Persona Física Nacionalidad Mexicana y Extranjera -- 12a 12b 

		if(historico==1)
			calc_hist=1;
		else if(historico==2)
			calc_hist=2.5;
		else if(historico==3)
			calc_hist=4; //ajustes ultima version global
	}
	
	if(tipo==1){ //Persona Física Nacionalidad Mexicana y Extranjera
		/*if(profesion==1){
			calc_prof=1;
		}
		else if(profesion==2){
			calc_prof=2;
		}
		else if(profesion==3){
			calc_prof=3;
		}*/
		calc_prof = $('#profesion option:selected').data('nivelriesgo');
		$("#prof_pmm").hide();
	}
	
	if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
		/*if(edad==5)
			calc_edad=1;
		else if(edad==6)
			calc_edad=2;
		else if(edad==7)
			calc_edad=3;*/

		calc_edad = $("#edad option:selected").data("nivelriesgo");
		//calc_clasificacion=1 //tipo de cliente
	}
	
	if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
		if(nacionalidad==1){
			calc_nac=1;
        }
		else if(nacionalidad==2){
			if($("input[name*='id_actividad']").val()==11){
                calc_nac=3; //cambio nueva calc para anexo8 vehiculos
            }else{
                calc_nac=3;  
            }
        }
	}
	
	if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
		if(estructura==1)
			calc_estru=3;
		else if(estructura==2)
			calc_estru=2;
		else if(estructura==3)
			calc_estru=1;
	}
	
	if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
		/*if(profesion==2)
			calc_prof=1;
		else if(profesion==1)
			calc_prof=3;*/
		$("#prof_pfme").hide();
		calc_prof = $('#profesion option:selected').data('nivelriesgo');
	}
	
	if(tipo==2 && $("#idact").val()==11){ //Persona Moral Nacionalidad Mexicana y Extranjera - vehiculos
		if(historico==1)
			calc_hist=1;
		else if(historico==2)
			calc_hist=2.5;
		else if(historico==3)
			calc_hist=4;
	}
    if(tipo==2 && $("#idact").val()==12){ //Persona Moral Nacionalidad Mexicana y Extranjera - blindaje
        if(historico==1)
            calc_hist=1;
        else if(historico==2) //cambios de ajustes ultima calculadora general
            calc_hist=2.5;
        else if(historico==3)
            calc_hist=4;
    }
	if(tipo==2 && $("#idact").val()==15 || tipo==2 && $("#idact").val()==16){ //Persona Moral Nacionalidad Mexicana y Extranjera -- 12a 12b
		if(historico==1)
			calc_hist=1;
		else if(historico==2)
			calc_hist=2.5;
		else if(historico==3)
			calc_hist=4;
	}
	if(tipo==5 && $("#idact").val()==11){ //Personas Políticamente Expuestas nacionales e internacionales
		if(historico==1)
			calc_hist=1;
		else if(historico==2)
			calc_hist=3;
		else if(historico==3)
			calc_hist=4;
	}
    if(tipo==5 && $("#idact").val()==12){ //Personas Políticamente Expuestas nacionales e internacionales
        if(historico==1)
            calc_hist=1;
        else if(historico==2)
            calc_hist=3;
        else if(historico==3)
            calc_hist=4;
    }
	if(tipo==5 && $("#idact").val()==15 || tipo==5 && $("#idact").val()==16){ //Personas Políticamente Expuestas nacionales e internacionales
		if(historico==1)
			calc_hist=1;
		else if(historico==2)
			calc_hist=3;
		else if(historico==3)
			calc_hist=4;
	}

	if(tipo==3){ //Entidades Gubernamentales Federales, Estatales y Municipales	
		//calc_tipo=3;
		calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo"); //tipo de cliente
	}

	if(tipo==4){ //Entidades Financieras Bancarias y no Bancarias	
		calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo"); //tipo de cliente
	}
  if(tipo==8){ //Embajadas, Consulados u Organismos Internacionales 
    calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo"); //tipo de cliente
  }

	if(tipo==5){ //Personas Políticamente Expuestas nacionales e internacionales
		if(nacionalidad==1)
			calc_nac=2.5;
		else if(nacionalidad==2)
			calc_nac=3;

		//calc_tipo=3;

		calc_edad = $("#edad option:selected").data("nivelriesgo");
		calc_clasificacion=$("#clasificacion option:selected").data("nivelriesgo");
	}

	if(tipo==6 || tipo==7){
		calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo");
	}

	if(tipo==5){ //Asociaciones Sin Fines de Lucro
		//calc_tipo=3;
	}

	if(paises==1){ //Países y áreas geográficas
		calc_pais=1;
	}else if(paises==2){ //Países y áreas geográficas
		calc_pais=2;
	}else if(paises==3){ //Países y áreas geográficas
		calc_pais=3;
	}

  var idtc= $("#idtipo_cliente option:selected").val();
  var clasi = parseInt($("#clasificacion option:selected").val());
  var calc_trans=0; var pre_calc_trans=0;
  //console.log("clasi: "+clasi);
	if(idtc=="1" && clasi==9 && $("#idact").val()==15 || idtc=="1" && clasi==9 && $("#idact").val()==16
    || idtc=="1" && clasi==16 && $("#idact").val()!=15 && $("#idact").val()!=16 || idtc=="2" || idtc=="3" || idtc=="4" || idtc=="6" || idtc=="7" || idtc=="8"){ //distinto a pep
		calc_transp1 = $("#transaccionalidad option:selected").data("nivelriesgo");	
		calc_transp2 = $("#transaccionalidad_forma option:selected").data("nivelriesgo");
        //console.log("calc_transp1: "+calc_transp1);
        //console.log("calc_transp2: "+calc_transp2);
		calc_trans = calc_transp1+calc_transp2;
		calc_trans = calc_trans/2;
        //console.log("calc_trans: "+calc_trans);
        //console.log("entro a no pep calculo: ");
	}else if(idtc=="5" || idtc=="1" && clasi>=1 && clasi<=8 && $("#idact").val()==15 || idtc=="1" && clasi>=1 && clasi<=8 && $("#idact").val()==16
    || idtc=="1" && clasi>=9 && clasi<=15 && $("#idact").val()!=15 && $("#idact").val()!=16){ //clientes PEP
		//console.log("entro a pep calculo: ");
    //var num_preg=parseFloat($('#padre_trans_pep div:visible').length);
		//var n_p_aux=0;
        val_p1=$("#preg1 option:selected").data("nivelriesgo");
        val_p4=$("#preg4 option:selected").data("nivelriesgo");
		val_p5=$("#preg5 option:selected").data("nivelriesgo");		
        val_p9=$("#preg9 option:selected").data("nivelriesgo");
        //val_p10=$("#preg10 option:selected").data("nivelriesgo");
		
		//calc_trans=pre_calc_trans/num_preg;
        if($("#idact").val()=='15' || $("#idact").val()=='16'){
		    if($('#preg5').val()=="1"){
                //console.log("divide a 5: ");
                pre_calc_trans=val_p1+val_p4+val_p5+val_p9;
                calc_trans=parseFloat(pre_calc_trans/4).toFixed(2);
            }
            else{
                //console.log("divide a 4: ");
                pre_calc_trans=val_p1+val_p4+val_p5+val_p9;
                calc_trans=parseFloat(pre_calc_trans/4).toFixed(2);
            }
        }
        else{
            //console.log("no soy notarios, divido a 4: ");
            pre_calc_trans=val_p1+val_p4+val_p5+val_p9;
            calc_trans=parseFloat(pre_calc_trans/4).toFixed(2);
        }

		//console.log("pre_calc_trans: "+pre_calc_trans);
		//console.log("n_p_aux: "+n_p_aux);
	}	
	
	//console.log("calc_trans: "+calc_trans);
	var canaldistri = 0;
	var canaldistricu=0
	var canaltotal=0
	if($("#idact").val()==15 || $("#idact").val()==16){ //notarios
		//canaldistri = $('#c_d_notaria option:selected').data('nivelriesgo');
		canaldistricu = $('#c_d_cliente_usuario option:selected').data('nivelriesgocu');
		canaltotal=canaldistri+canaldistricu;
		//canaltotal=canaltotal/2;
	}
	if($("#idact").val()==11 || $("#idact").val()==12){ //vehiculos
		//canaldistri = $('#c_d_agencia option:selected').data('nivelriesgo'); //VERIFICAR SI TAMBIEN SE QUITA DE AUTOS
		canaldistricu = $('#c_d_cliente_usuario option:selected').data('nivelriesgocu');
		canaltotal=canaldistri+canaldistricu;
		//canaltotal=canaltotal/2;
	}

	var calc_clasificacion_fin=0;
	if($("#idtipo_cliente option:selected").val()==1 || $("#idtipo_cliente option:selected").val()==2 || $("#idtipo_cliente option:selected").val()==5)
	{
		if($("#idtipo_cliente option:selected").val()==1){
			calc_clasificacion_fin=parseFloat(calc_clasificacion)+parseFloat(calc_edad)+parseFloat(calc_nac)+parseFloat(calc_hist)+parseFloat(calc_prof);
      //console.log("calc_clasificacion_fin: "+calc_clasificacion_fin);
      /*console.log("calc_edad: "+calc_edad);
      console.log("calc_nac: "+calc_nac);
      console.log("calc_estru: "+calc_estru);
      console.log("calc_hist: "+calc_hist);
      console.log("calc_prof: "+calc_prof);*/
			var div_num=5;
		}
		if($("#idtipo_cliente option:selected").val()==2){
      /*console.log("calc_edad: "+calc_edad);
      console.log("calc_nac: "+calc_nac);
      console.log("calc_estru: "+calc_estru);
      console.log("calc_hist: "+calc_hist);
      console.log("calc_prof: "+calc_prof);*/
			calc_clasificacion_fin=parseFloat(calc_edad)+parseFloat(calc_nac)+parseFloat(calc_estru)+parseFloat(calc_hist)+parseFloat(calc_prof);
			var div_num=5;
		}
		if($("#idtipo_cliente option:selected").val()==5){
			calc_clasificacion_fin=parseFloat(calc_clasificacion)+parseFloat(calc_edad)+parseFloat(calc_nac)+parseFloat(calc_hist);
			var div_num=4;
          /*console.log("calc_edad: "+calc_edad);
          console.log("calc_nac: "+calc_nac);
          console.log("calc_hist: "+calc_hist);
          console.log("calc_clasificacion: "+calc_clasificacion);*/
		}
		/*console.log("calc_clasificacion: "+calc_clasificacion);
		console.log("calc_edad: "+calc_edad);
		console.log("calc_nac: "+calc_nac);
		console.log("calc_hist: "+calc_hist);
		console.log("calc_prof: "+calc_prof);
		console.log("calc_estru: "+calc_estru);
		console.log("div_num: "+div_num);*/
		//if(calc_clasificacion>0)
		calc_clasificacion_fin = calc_clasificacion_fin/div_num;
	}else{
		calc_clasificacion_fin=calc_clasificacion;
	}

	/*console.log("calc_monto: "+calc_monto);
	//console.log("calc_tipo: "+calc_tipo);
	console.log("calc_pais: "+calc_pais);
	console.log("calc_trans: "+calc_trans);
	console.log("canaltotal: "+canaltotal);
	console.log("calc_clasificacion_fin: "+calc_clasificacion_fin);*/

    /*if($("input[name*='id_actividad']").val()!=11){
    	porc_ps=.25; //productos y servicios
    	porc_tipo=.25; //tipo d cliente
    	porc_paises=.10;
    	porc_trans=.25;
    	porc_c_distribusion=.15;
    }else{*/
        porc_ps=.10; //productos y servicios
        porc_tipo=.375; //tipo d cliente
        porc_paises=.05;
        porc_trans=.375;
        porc_c_distribusion=.10;
    //}

	puntaje1 = porc_ps*calc_monto; //productos y servicios
	puntaje2 = porc_tipo*calc_clasificacion_fin; //calsificacion o detalle de cliente. creo acá se debe sumar todo lo de clasifica, edad, nacionalidad, etc dependiente del tipo de cliente
	puntaje3 = porc_paises*calc_pais; //paises y areas geograficas
	puntaje4 = porc_trans*calc_trans; //transaccionalidad
	puntaje5 = porc_c_distribusion*canaltotal; //canales de distribuicion sobre agencia y sobre cliente

    /*console.log("puntaje1: "+puntaje1);
    console.log("puntaje2: "+puntaje2);
    console.log("puntaje3: "+puntaje3);
    console.log("puntaje4:" +puntaje4);
    console.log("puntaje5: "+puntaje5);*/

	var total = (puntaje1+puntaje2+puntaje3+puntaje4+puntaje5);
	total = total.toFixed(2);
	//total = total*1;
	//console.log("puntaje final: "+total);
	var val_barra=0;
	//if(total<=1.93){ //bajo
	if(total<=1.79){ //bajo
		val_barra=parseFloat(50);
		val_barra = parseFloat(val_barra)+parseFloat(total);
		//console.log("val barra: "+val_barra);
		//console.log("1.99");
		/*$("#cont_gdo1").html('<input readonly name="grado" id="grado_final" style="text-align:center; font-size: 30px; background-color:#4AD079" type="text" value="'+total+'">\
			<span style="font-size: 30px; color:#4AD079">BAJO</span>');*/
		$("#cont_gdo1").html('<div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar"\
		 style="width: '+val_barra+'%" aria-valuenow="1" aria-valuemin="'+total+'" aria-valuemax="'+total+'"></div><p style="font-size:29px; color:#4AD079"><b>'+total+'</b></p><br>');
		$("#txt_grado").html('<input name="grado" id="grado_final" type="hidden" value="'+total+'"><span style="font-size: 30px; color:#4AD079">BAJO</span>');
	}
	//else if(total>1.93 && total<=2.29){ //medio
	else if(total>=1.8 && total<=2.0){ //medio
		val_barra=parseFloat(70);
		val_barra = parseFloat(val_barra)+parseFloat(total);
		//console.log("val barra: "+val_barra);
		//console.log(">2.0");
		/*$("#cont_gdo1").html('<input readonly name="grado" id="grado_final" style="text-align:center; font-size: 30px; background-color:#FFC000" type="text" value="'+total+'">\
			<span style="font-size: 30px; color:#FFC000">MEDIO</span>');*/
		$("#cont_gdo1").html('<div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar"\
		 style="width: '+val_barra+'%" aria-valuenow="1" aria-valuemin="'+total+'" aria-valuemax="'+total+'"></div><p style="font-size:29px; color:#FFC000"><b>'+total+'</b></p><br>');
		$("#txt_grado").html('<input name="grado" id="grado_final" type="hidden" value="'+total+'"><span style="font-size: 30px; color:#FFC000">MEDIO</span>');
	}
	//else if(total>=2.30){ //alto
	else if(total>2.0){ //alto
		val_barra=parseFloat(90);
		val_barra = parseFloat(val_barra)+parseFloat(total);
		//console.log("val barra: "+val_barra);
		//console.log(">2.51");
		/*$("#cont_gdo1").html('<input readonly name="grado" id="grado_final" style="text-align:center; font-size: 30px; background-color:#D04A4A" type="text" value="'+total+'">\
			<span style="font-size: 30px; color:#D04A4A">ALTO</span>');*/
		$("#cont_gdo1").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar"\
		 style="width: '+val_barra+'%" aria-valuenow="1" aria-valuemin="'+total+'" aria-valuemax="'+total+'"></div><p style="font-size:29px; color:#D04A4A"><b>'+total+'</b></p><br>');
		$("#txt_grado").html('<input name="grado" id="grado_final" type="hidden" value="'+total+'"><span style="font-size: 30px; color:#D04A4A">ALTO</span>');
	}

}

/*function calculaGrado(){
	giro = $("#giro").val();
	pago = $("#forma_pago").val();
	grado_edo = $("#grado_edo").val();
	tipo = $("#tipo_cliente").val();

	if(tipo=="1" || tipo=="2" || tipo=="3")
		tipo=2;
	if(tipo=="4")
		tipo=1;

	porc_giro=.30;
	porc_form_p=.30;
	porc_edo=.10;
	porc_tipo=.30;

	puntaje1 = porc_giro*giro;
	puntaje2 = porc_form_p*pago;
	puntaje3 = porc_edo*grado_edo;
	puntaje4 = porc_tipo*tipo;

	var total = (puntaje1+puntaje2+puntaje3+puntaje4);
	total = parseFloat(total).toFixed(2);
	//console.log("puntaje final: "+total);
	if(total<=1.30){ //bajo
		$("#cont_gdo1").html('<input readonly name="grado" id="grado_final" style="font-size: 30px; background-color:#4AD079" type="text" value="'+total+'">\
			<span style="font-size: 30px; color:#4AD079">BAJO</span>');
	}
	if(total>1.30){ //alto
		$("#cont_gdo1").html('<input readonly name="grado" id="grado_final" style="font-size: 30px; background-color:#D04A4A" type="text" value="'+total+'">\
			<span style="font-size: 30px; color:#D04A4A">ALTO</span>');
	}
}*/

function limpiarForm2(){
    var idtipo_cliente=$("#idtipo_cliente option:selected").val();
    //$("#clasificacion").val('0');
    if(idtipo_cliente==1){
        $('#cont_constitucion').show('slow');
        $('#cont_nacional').show('slow');   
        $('#cont_historico').show('slow');
        $('#cont_profe').show('slow');
        $('#cont_est_acc').hide('slow')
    }else if(idtipo_cliente==2){
        $('#cont_constitucion').show('slow');
        $('#cont_nacional').show('slow');
        $('#cont_est_acc').show('slow');
        $('#cont_profe').show('slow');  
        $('#cont_historico').show('slow');
    }else if(idtipo_cliente==3 || idtipo_cliente==4 || idtipo_cliente==6 || idtipo_cliente==7 || idtipo_cliente==8){
        $('#cont_constitucion').hide('slow');
        $('#cont_nacional').hide('slow');   
        $('#cont_historico').hide('slow');
        $('#cont_profe').hide('slow');
        $('#cont_est_acc').hide('slow')
    }
    else if(idtipo_cliente==5){
        $('#cont_constitucion').show('slow');
        $('#cont_nacional').show('slow');   
        $('#cont_historico').show('slow');
        $('#cont_profe').hide('slow');
        $('#cont_est_acc').hide('slow')
    }
    //selectAutoTipo();
}


function limpiarForm(){
	var idtipo_cliente=$("#idtipo_cliente option:selected").val();
	$("#clasificacion").val('0');
	if(idtipo_cliente==1){
		$("#detalle_tipo_cli").val("0");
		$('#cont_constitucion').show('slow');
		$('#cont_nacional').show('slow');	
		$('#cont_historico').show('slow');
		$('#cont_profe').show('slow');
		$('#cont_est_acc').hide('slow')
	}else if(idtipo_cliente==2){
		$("#detalle_tipo_cli").val("0");
		$('#cont_constitucion').show('slow');
		$('#cont_nacional').show('slow');
		$('#cont_est_acc').show('slow');
		$('#cont_profe').show('slow');	
		$('#cont_historico').show('slow');
	}else if(idtipo_cliente==3 || idtipo_cliente==4 || idtipo_cliente==6 || idtipo_cliente==7 || idtipo_cliente==8){
		$("#detalle_tipo_cli").val("0");
		$('#cont_constitucion').hide('slow');
		$('#cont_nacional').hide('slow');	
		$('#cont_historico').hide('slow');
		$('#cont_profe').hide('slow');
		$('#cont_est_acc').hide('slow')
	}
	else if(idtipo_cliente==5){
		$("#detalle_tipo_cli").val("0");
		$('#cont_constitucion').show('slow');
		$('#cont_nacional').show('slow');	
		$('#cont_historico').show('slow');
		$('#cont_profe').hide('slow');
		$('#cont_est_acc').hide('slow')
	}
}

function mostraroptionclasificacion(){
	var idtipo_cliente=$("#idtipo_cliente option:selected").val();
	$('.clasificacion_5').hide('slow');
	$('.clasificacion_1').hide('slow');
	$('.transaccionalidacliente_pep').hide('slow');
	setTimeout(function(){ 
		
		if(idtipo_cliente!=5 /*|| idtipo_cliente==1 && $("#clasificacion").val()==1 || idtipo_cliente==1 && $("#clasificacion").val()==9*/){
			$('.transaccionalidacliente_pep').hide('slow');
			$('#trans_no_pep').show('slow'); //falta ocultar la no_pep2
			$('#trans_no_pep2').show('slow');
		}
		if(idtipo_cliente==2){
			$('#cont_est_acc').show('slow');
		}else{
			$('#cont_est_acc').hide('slow');
		}
		if(idtipo_cliente==3 || idtipo_cliente==4 || idtipo_cliente==6 || idtipo_cliente==7 || idtipo_cliente==8){
			$('#cont_det_tipo').show('slow');	
		}else{
			$('#cont_det_tipo').hide('slow');	
		}

		if(idtipo_cliente==3){
			$('.ent_gub').show();
			$('.ent_finan').hide();
			$('.fides_tc').hide();
			$('.asocia_sin').hide();
            $('.emb').hide();
    	}else if(idtipo_cliente==4){
			$('.ent_finan').show();
			$('.ent_gub').hide();
			$('.fides_tc').hide();
			$('.asocia_sin').hide();
            $('.emb').hide();
		}else if(idtipo_cliente==6){
			$('.fides_tc').show();
			$('.ent_finan').hide();
			$('.ent_gub').hide();
			$('.asocia_sin').hide();
            $('.emb').hide();
		}else if(idtipo_cliente==7){
			$('.asocia_sin').show();
			$('.fides_tc').hide();
			$('.ent_finan').hide();
			$('.ent_gub').hide();
            $('.emb').hide();
    	}else if(idtipo_cliente==8){
          $('.emb').show();
          $('.asocia_sin').hide();
          $('.fides_tc').hide();
          $('.ent_finan').hide();
          $('.ent_gub').hide();
        }


	  	if(idtipo_cliente==1 || idtipo_cliente==5){
	  		$('.clasificacionform').show('slow');
	  		//setTimeout(function(){
		  	if(idtipo_cliente==1){
                var clas_sel = $("#clasificacion option:selected").val();
      			if($("#idact").val()==15 || $("#idact").val()==16){ //actividad - notarios
      				$('.clasificacion_1').show('slow');
      				$('.clasificacion_5').hide('slow');
      				$('.clasificacion_1_de_11').hide('slow');
      			}else if($("#idact").val()==11 || $("#idact").val()==12){ //actividad autos, BLINDAJE
      				$('.clasificacion_1_de_11').show('slow');
      				$('.clasificacion_1').hide('slow');
      				$('.clasificacion_5').hide('slow');
      			}
                if(idtipo_cliente==5){
                    $('.clasificacion_5').show('slow');
                }else{
                   $('.clasificacion_5').hide('slow'); 
                }
                //console.log("clas_sel: "+clas_sel);
                if(clas_sel>=1 && clas_sel<=8 && $("#idact").val()==15 || clas_sel>=1 && clas_sel<=8 && $("#idact").val()==16
                  || clas_sel>=9 && clas_sel<=13 && $("#idact").val()!=15 && $("#idact").val()!=16){
                  $('.transaccionalidacliente_pep').show('slow');
                  $('#trans_no_pep').hide('slow');
                  $('#trans_no_pep2').hide('slow');
                  preguntasPEP();
                  //console.log("muestro transaccionalidad pep");
                }else if(clas_sel==9 && $("#idact").val()==15 || clas_sel==9 && $("#idact").val()==16
                  || clas_sel==14 && $("#idact").val()!=15 || clas_sel==14 && $("#idact").val()!=16){
                  $('.transaccionalidacliente_pep').hide('slow');
                  //console.log("oculto transaccionalidad pep");
                  $('#trans_no_pep').show('slow');
                  $('#trans_no_pep2').show('slow');
                }
		  			
		  			
	  		}
	  		if(idtipo_cliente==5){ //pep
	  			$('.clasificacion_1_de_11').hide('slow');
	  			$('.clasificacion_1').hide('slow');
	  			$('.clasificacion_5').show('slow');
	  			$('.transaccionalidacliente_pep').show('slow');
	  			$('#trans_no_pep').hide('slow');
	  			$('#trans_no_pep2').hide('slow');
	  			preguntasPEP();
	  		}else{

            }
	  		//}, 800);
	  	}else{
	  		$('.clasificacionform').hide('slow');
	  	}
	}, 800);
	
}