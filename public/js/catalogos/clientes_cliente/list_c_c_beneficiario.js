var base_url = $('#base_url').val();
var idbfi = $('.idbfi').text();
var id_perf = $('#id_perf').val();
var tabla;
$(document).ready(function($) {
    /// Tabla de clientes
    //tabla_load_cliente();
    loadTable();

    /*$('#table_beneficiario').on('click', 'button.editar', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        console.log("editar: "+data.tabla);
        if(data.tabla=="f")
            window.location.href = base_url+"Clientes_c_beneficiario/addBene/"+data.id+"/"+data.idtipo_cliente;
        if(data.tabla=="m")
            window.location.href = base_url+"Clientes_c_beneficiario/addBene/"+data.id+"/"+data.id_perfilamiento;
    });*/

    /*$('#table_beneficiario').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        console.log("eliminar: "+data.tabla);
        eliminar(data.id,data.tabla);
    });*/

    $("#agregarBene").on('click',function(){
        $("#modal_tipo_bene").modal();
    }); 

});
function inicio_cliente(){
  location.href= base_url+'Clientes_cliente';
  //history.back();
}
function editar(id,tabla){
    if(tabla==1)
        window.location.href = base_url+"Clientes_c_beneficiario/addBene/"+id+"/"+$("#idc").val()+"/"+id_perf+"/"+$("#tipoc").val();
    if(tabla==2)
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/"+id+"/"+$("#idc").val()+"/"+id_perf+"/"+$("#tipoc").val();
    if(tabla==3)
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/"+id+"/"+$("#idc").val()+"/"+id_perf+"/"+$("#tipoc").val();
}

function documentos(id,tipo){
    window.location.href = base_url+"Clientes_c_beneficiario/docs_beneficiario/"+id+"/"+tipo+"/"+id_perf+"/"+$("#idc").val()+"/0";
}

function aceptarAdd(){
    if($("#fisica").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBene/0/"+$("#idc").val()+"/"+id_perf+"/"+$("#tipoc").val();
    if($("#moral").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/0/"+$("#idc").val()+"/"+id_perf+"/"+$("#tipoc").val();
    if($("#fideicomiso").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/0/"+$("#idc").val()+"/"+id_perf+"/"+$("#tipoc").val();
}

function loadTable(){
    $.ajax({
          beforeSend: function(){
            //$("#table_beneficiario").html("Consultando detalle...");
          },
          url: base_url+"Clientes_c_beneficiario/records_benes",
          type: 'POST',
          data: {  id_perf: id_perf/*, idc: $("#idc").val()*/ },
          success: function(x){
            $("#table_beneficiario").html(x);
            //console.log(x);
            tabla = $("#table_beneficiario").DataTable({
                destroy:true
            });
           },
          error: function(jqXHR,estado,error){
            $("#table_beneficiario").html('Hubo un error: '+estado+' '+error);
          }
    });
}

function eliminar(id,tablaBen,cont){
    title = "¿Desea eliminar este registro?";
    swal({
          title: title,
          text: "Se eliminará el Beneficiario del listado!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Clientes_c_beneficiario/eliminarBene',
            data:{ id: id, tablaBen: tablaBen },
            success:function(data){
                //tabla.ajax.reload();
                $('.rowt_'+cont).remove();
                swal("Éxito", "Beneficiario eliminado correctamente", "success");
            }
          }); //cierra ajax
       }
    });
}
function expedientes(idbm,id,tipo){
  window.location.href = base_url+"Clientes_c_beneficiario/archivo/"+idbm+"/"+id+"/"+tipo;
}

/*function tabla_load_cliente(){
    tabla=$("#table_beneficiario").DataTable({
        "bProcessing": true,
        //"serverSide": true,
        "ajax": {
           "url": base_url+"Clientes_c_beneficiario/records_benes",
           type: "post",
           "dataSrc": "",
           data:{
              id_perf: id_perf
            },
            error: function(){
               $("#table_beneficiario").css("display","none");
            }
        },
        "columns": [
            //{"data": "id"},
            {"data": null,
                "render": function ( data, type, row, meta ) {
                var html= row.nombre+' '+row.apellido_paterno +' '+row.apellido_materno;        
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ) {
                var html='';      
                    if(row.tipo_persona==1){
                        html='<a class="btn btn-secondary btn-sm" title="Documentos"  style="color: white;" href="'+base_url+'Clientes_cliente/docs_beneficiario/'+row.idf+'/'+row.id+'/'+row.id_actividad+'/'+row.id_perfilamiento+'/'+row.id_cliente+'"><i class="fa fa-file-o"></i></a>';
                    }else if(row.tipo_persona==2){
                             html='<a class="btn btn-secondary btn-sm" title="Documentos"  style="color: white;" href="'+base_url+'Clientes_cliente/docs_beneficiario/'+row.idm+'/'+row.id+'/'+row.id_actividad+'/'+row.id_perfilamiento+'/'+row.id_cliente+'"><i class="fa fa-file-o"></i></a>';
                    }
                //var html='<a class="btn btn-secondary btn-sm" title="Documentos"  style="color: white;" href="'+base_url+'Clientes_cliente/docs_beneficiario/id_benef_m/id_trans_bene/idactividad/idperfilamiento_cliente/idciente"><i class="fa fa-file-o"></i></a>';        
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ) {
                var html='<a class="btn btn-dark btn-sm" title="Editar" style="color: white;" href="'+base_url+'Clientes_c_beneficiario/add/'+row.idcliente+'"><i class="fa fa-edit"></i></a>\
                          <button type="button" class="btn btn-info btn-sm" title="Cancelar" onclick="modaleliminar('+row.idcliente+')"><i class="fa fa-trash"></i></button>';        
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
    });
}*/