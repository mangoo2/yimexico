$(document).ready(function(){
	if($('#accionaria_socio_a').is(':checked')){
	  	clicka();
	  	$(".accion_socio_a").show("slow");
	}
	if($('#accionaria_socio_b').is(':checked')){
	  	clickb();
	  	$(".accion_socio_b").show("slow");
	}
	if($('#accionaria_socio_c').is(':checked')){
		clickc();
		$(".accion_socio_c").show("slow");
	}
	if($('#accionaria_socio_d').is(':checked')){
	  	clickd();
	  	$(".accion_socio_d").show("slow");
	}

	function clicka(){
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"a", "edita": 1, "idbene": $("#idbene").val() },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      	$("#btn_save").attr('disabled',false);
	      }
	    });
	}
	function clickb(){
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"b", "edita": 1, "idbene": $("#idbene").val() },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      	$("#btn_save").attr('disabled',false);
	      }
	    });
	}
	function clickc(){
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"c", "edita": 1, "idbene": $("#idbene").val() },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      	$("#btn_save").attr('disabled',false);
	      }
	    });
	}
	function clickd(){
		$.ajax({
	      type: "POST",
	      url: base_url+"Clientes_cliente/cargarForm",
	      data: { "inciso":"d", "edita": 1, "idbene": $("#idbene").val() },
	      async: false, 
	      success: function (result) {
	      	//console.log('carga form A');
	      	$("#contForm").html(result);
	      	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
	      	$("#btn_save").attr('disabled',false);
	      }
	    });
	}
	
	$('#accionaria_socio_a').click(function(event) {
	    $(".accion_socio_a").show("slow");
	    $(".accion_socio_b").hide("slow");
	    $(".accion_socio_c").hide("slow");
	    $(".accion_socio_d").hide("slow");
	});
	$('#accionaria_socio_b').click(function(event) {
	    $(".accion_socio_b").show("slow");
	    $(".accion_socio_a").hide("slow");
	    $(".accion_socio_c").hide("slow");
	    $(".accion_socio_d").hide("slow");
  	});
	$('#accionaria_socio_c').click(function(event) {
	    $(".accion_socio_c").show("slow");
	    $(".accion_socio_a").hide("slow");
	    $(".accion_socio_b").hide("slow");
	    $(".accion_socio_d").hide("slow");
	});
	$('#accionaria_socio_d').click(function(event) {
	    $(".accion_socio_d").show("slow");
	    $(".accion_socio_a").hide("slow");
	    $(".accion_socio_b").hide("slow");
	    $(".accion_socio_c").hide("slow");
	});
	$('#tipo_persona_mfs1').click(function(event) {
	    $(".pers_fisica_s1").show("slow");
	    $(".pers_moral_s1").hide("slow");
	});
	$('#tipo_persona_mms1').click(function(event) {
	    $(".pers_fisica_s1").hide("slow");
	    $(".pers_moral_s1").show("slow");
	});
	$('#tipo_persona_mfs2').click(function(event) {
	    $(".pers_fisica_s2").show("slow");
	    $(".pers_moral_s2").hide("slow");
	});
	$('#tipo_persona_mms2').click(function(event) {
	    $(".pers_fisica_s2").hide("slow");
	    $(".pers_moral_s2").show("slow");
	});

	  //////////////////////// B ///////////////
	if($('#fisicab').is(':checked')){
	  	$('#pers_fisicab_s1').css('display','block');
	}
	if($('#fisica2b').is(':checked')){
	  	$('#pers_fisicab_s2').css('display','block');
	}
	$('#fisicab').click(function(event) {
	    $(".pers_fisicab_s1").show("slow");
	    $(".pers_moralb_s1").hide("slow");
	});
	$('#moralb').click(function(event) {
	    $(".pers_fisicab_s1").hide("slow");
	    $(".pers_moralb_s1").show("slow");
	});
	$('#fisica2b').click(function(event) {
	    $(".pers_fisicab_s2").show("slow");
	    $(".pers_moralb_s2").hide("slow");
	});
	$('#moral2b').click(function(event) {
	    $(".pers_fisicab_s2").hide("slow");
	    $(".pers_moralb_s2").show("slow");
	});
	$('#lugar_r_sociob').click(function(event) { //persona fisica
	  	if($('#lugar_r_sociob').is(':checked')){
	      $('.residencia_extranjerob').css('display','block');
		}else{
		  $('.residencia_extranjerob').css('display','none');	
		}
	});
	$('#lugar_r_socio1_b').click(function(event) { //persona moral socio 1
	  	if($('#lugar_r_socio1_b').is(':checked')){
	      $('.r_extranjero_socio_accion_b').css('display','block');
		}else{
		  $('.r_extranjero_socio_accion_b').css('display','none');	
		}
	});
	$('#lugar_r_socio2_b').click(function(event) { //persona moral socio 2
	  	if($('#lugar_r_socio2_b').is(':checked')){
	      $('.r_extranjero_socio2_accion_b').css('display','block');
		}else{
		  $('.r_extranjero_socio2_accion_b').css('display','none');	
		}
	});

	$('#lugar_r_socio2fb').click(function(event) { //persona fisica
	  	if($('#lugar_r_socio2fb').is(':checked')){
	      $('.residencia_extranjero2b').css('display','block');
		}else{
		  $('.residencia_extranjero2b').css('display','none');	
		}
	});
	$('#lugar_r_socio2_mb').click(function(event) { //persona moral socio 1
	  	if($('#lugar_r_socio2_mb').is(':checked')){
	      $('.r_extranjero_socio2_accion_2b').css('display','block');
		}else{
		  $('.r_extranjero_socio2_accion_2b').css('display','none');	
		}
	});
	$('#lugar_r_socio2_m2b').click(function(event) { //persona moral socio 2
	  	if($('#lugar_r_socio2_m2b').is(':checked')){
	      $('.r_extranjero_socio2_accion2_2b').css('display','block');
		}else{
		  $('.r_extranjero_socio2_accion2_2b').css('display','none');	
		}
	});


   ////////////////////////// C ////////////////
   	$('#persona_fisica_c').click(function(event) {
	    $("#pf_c").show("slow");
	    $("#pmfd_c").hide("slow");
  	});
  	$('#persona_m_f_c').click(function(event) {
	    $("#pf_c").hide("slow");
	    $("#pmfd_c").show("slow");
  	});
 
    
    if($("#accionaria_socio_a").is(':checked')) {  
        $("#btn_save").attr('disabled',false);
    } 
    else if($("#accionaria_socio_b").is(':checked')) {  
        $("#btn_save").attr('disabled',false);
    } 
    else if($("#accionaria_socio_c").is(':checked')) {  
        $("#btn_save").attr('disabled',false);
    } 
    else if($("#accionaria_socio_d").is(':checked')) {  
        $("#btn_save").attr('disabled',false);
    }else {  
        $("#btn_save").attr('disabled',true); 
    }  


  	/* ************* PARA CARGAR EL FORMULARIO *************** */
  	
  	var aux=2; //persona moral
  	$("#btn_save").on('click', function(){
		if($('#tipo_persona_mfs1').is(':checked'))
		    tp=1; //a
		if($('#tipo_persona_mms1').is(':checked'))
		    tp=2; //b
		if($('#tipo_persona_mfs2').is(':checked'))
		    tp2=1; //c 
		if($('#tipo_persona_mms2').is(':checked'))
		    tp2=2; //d

		if($('#fisicab').is(':checked')) //socio 1
		    tp=1; //persona fisica
		if($('#moralb').is(':checked')) // socio 1
		    tp=2; // persona moral
		if($('#fisica2b').is(':checked')) // socio 2
		    tp2=1; // persona fisica 
		if($('#moral2b').is(':checked')) // socio 2
		    tp2=2; // persona moral

	  	if(aux==2){ //if de persona moral
		    if($("#accionaria_socio_a").is(':checked')) inciso="a"; if($("#accionaria_socio_b").is(':checked')) { inciso="b"; rela_jur=$("#relacion_juridica").val(); }
		    if($("#accionaria_socio_c").is(':checked')) inciso="c"; if($("#accionaria_socio_d").is(':checked')) inciso="d"; 

		    if(inciso=="a" || inciso=="b"){
		      	$.ajax({
		        	type: "POST",
		        	url: base_url+"Clientes_c_beneficiario/add_benem",
		        	data:{
		        	id_tipo_perfil:$('#tipoc').val(),
		            id_perfilamiento:$('#idp').val(),
		            tipo_persona:aux,
		            dueno_beneficiario:$('#idc').val(),
		            tipo_inciso: inciso,
		            tipo_p: tp
		        	},
			        statusCode:{
			          404: function(data){
			              swal("Error!", "No Se encuentra la ruta", "error");
			          },
			          500: function(){
			              swal("Error!", "500", "error");
			          }
			        },
		           success: function (data){
		         	//var id_bene_moral = data;
		          	var array = $.parseJSON(data);
		          	id_bene_moral = array.idbm;

			        if(tp==1){ //datos para el socio 1 a) FISICA
			            datos_moral=$('#form_bene_moral_fisica_s1').serialize(); var gene="";
			            if($('#generom1').is(':checked')) gene="m"; if($('#generof1').is(':checked')) gene="f";
			            tabla = "beneficiario_moral_fisica";
			            //if($("#nombre").val()!=""){  
			              $.ajax({ //datos para el socio 1
			                type: "POST",
			                url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
			                data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje1").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
			                statusCode:{
			                  404: function(data){
			                      swal("Error!", "No Se encuentra la ruta", "error");
			                  },
			                  500: function(){
			                      swal("Error!", "500", "error");
			                  }
			                },
			                success: function (data){
			                  	//swal("Éxito", "Socio 1 guardado correctamente", "success");
			                 	console.log("Socio fisico 1 guardado correctamente");
			                },
			              });
			            //}
			        }//if tp
			        if(tp==2){ //datos para el socio 1 a) MORAL
		            	datos_moralf = $('#form_bene_moral_moral_s1').serialize(); var gene="";
		            	tabla = "beneficiario_moral_moral";
		            	$.ajax({ //datos para ingresar el socio 1 moral 
		              		type: "POST",
		              		url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
		              		data: datos_moralf+'&id_bene_moral='+id_bene_moral+'&porcentaje='+$("#porcentaje1").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
		              		statusCode:{
			                	404: function(data){
			                    	swal("Error!", "No Se encuentra la ruta", "error");
			                	},
				                500: function(){
				                    swal("Error!", "500", "error");
				                }
		              		},
		              		success: function (data){
				                datos_moral = $('#form_bene_moral_fisica_s12').serialize(); //socio fisico 1
				                if($('#generom2').is(':checked')) gene="m"; if($('#generof2').is(':checked')) gene="f";
				                tabla = "beneficiario_moral_fisica";
				                $.ajax({ //datos para el socio 1
				                  	type: "POST",
				                  	url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
				                  	data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje2").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
				                  	statusCode:{
					                    404: function(data){
					                        swal("Error!", "No Se encuentra la ruta", "error");
					                    },
					                    500: function(){
					                        swal("Error!", "500", "error");
					                    }
					            	},
					                success: function (data){
					                    datos_moral = $('#form_bene_fisica_fisica_s12_2').serialize(); //socio fisico 2
					                    if($('#generom3').is(':checked')) gene="m"; if($('#generof3').is(':checked')) gene="f";
					                    tabla = "beneficiario_moral_fisica";
					                    $.ajax({ //datos para el socio 1
					                      type: "POST",
					                      url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
					                      data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje3").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
					                      statusCode:{
					                        404: function(data){
					                            swal("Error!", "No Se encuentra la ruta", "error");
					                        },
					                        500: function(){
					                            swal("Error!", "500", "error");
					                        }
					                      },
					                      success: function (data){
					                        swal("Éxito", "Guardado correctamente", "success");
					                      }
					                    });

					                }
		                		});
		              		}//succes principal
		            	});
		          	}//if tp=2

		          	$.ajax({
			            type: "POST",
			            url: base_url+"Clientes_c_beneficiario/add_benem",
			            data:{
			            	id_tipo_perfil:$('#tipoc').val(),
			                id_perfilamiento:$('#idp').val(),
			                tipo_persona:aux,
			                dueno_beneficiario:$('#idc').val(), //aqui deberia ser id beneficiario tabla (fisica o moral)
			                tipo_inciso: inciso,
			                tipo_p: tp2
			            },
			            statusCode:{
				            404: function(data){
				                swal("Error!", "No Se encuentra la ruta", "error");
				            },
				            500: function(){
				                swal("Error!", "500", "error");
				            }
			            },
		            	success: function (data){
			              	//var id_bene_moral = data; 
				            var array = $.parseJSON(data);
				            id_bene_moral = array.idbm;

				            var gene="";
				            if(tp2==1){ //datos para el socio 1 a) FISICA
				                datos_moral = $('#form_bene_moral_fisica_s2').serialize();
				                if($('#generom4').is(':checked')) gene="m"; if($('#generof4').is(':checked')) gene="f";
				            	    tabla = "beneficiario_moral_fisica";
				                	$.ajax({ //datos para el socio 1
				                  		type: "POST",
				                  		url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
				                  		data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje4").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
				                  		statusCode:{
					                    	404: function(data){
					                        	swal("Error!", "No Se encuentra la ruta", "error");
					                    	},
					                    	500: function(){
					                        	swal("Error!", "500", "error");
					                    	}
				                  		},
					                  	success: function (data){
					                    	swal("Éxito", "Socio guardado correctamente", "success");
					                    	window.history.back();
				                  		}
			            			});//ajax
		              		}//if tp1
				            if(tp2==2){ //datos para el socio 1 a) MORAL
				                datos_moralf = $('#form_bene_moral_moral_s1').serialize(); var gene="";
				                tabla = "beneficiario_moral_moral";
				                $.ajax({ //datos para ingresar el socio 1 moral 
				                  	type: "POST",
				                  	url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
				                  	data: datos_moralf+'&id_bene_moral='+id_bene_moral+'&porcentaje='+$("#porcentaje4").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
				                  	statusCode:{
				                    	404: function(data){
				                        	swal("Error!", "No Se encuentra la ruta", "error");
				                    	},
					                    500: function(){
					                        swal("Error!", "500", "error");
					                    }
				                  	},
				                  	success: function (data){
				                    	datos_moral = $('#form_bene_moral_fisica_s22').serialize(); //socio fisico 1
				                    	if($('#generom5').is(':checked')) gene="m"; if($('#generof5').is(':checked')) gene="f";
				                    	tabla = "beneficiario_moral_fisica";
				                    	$.ajax({ //datos para el socio 1
				                      		type: "POST",
				                      		url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
				                      		data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje5").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
				                      		statusCode:{
						                        404: function(data){
						                            swal("Error!", "No Se encuentra la ruta", "error");
						                        },
						                        500: function(){
						                            swal("Error!", "500", "error");
						                        }
				                      		},
					                      	success: function (data){
					                        	datos_moral = $('#form_bene_fisica_fisica_s22_2').serialize(); //socio fisico 2
					                        	if($('#generom6').is(':checked')) gene="m"; if($('#generof6').is(':checked')) gene="f";
					                        	tabla = "beneficiario_moral_fisica";
					                        	$.ajax({ //datos para el socio 1
					                         		type: "POST",
					                          		url: base_url+"index.php/Clientes_cliente/guardar_bene_moral_fisica",
					                          		data: datos_moral+'&id_bene_moral='+id_bene_moral+'&genero='+gene+'&porcentaje='+$("#porcentaje6").val()+'&relacion_juridica='+rela_jur+'&tabla='+tabla,
					                          		statusCode:{
							                            404: function(data){
							                                swal("Error!", "No Se encuentra la ruta", "error");
							                            },
							                            500: function(){
							                                swal("Error!", "500", "error");
							                            }
							                        },
					                          		success: function (data){
							                            swal("Éxito", "Guardado correctamente", "success");
							                            setTimeout(function(){ 
							                            	window.history.back();
							                            }, 2500);
					                          		}
					                        	});

					                     	}
				                    	});
				                  	}//succes principal
				                });
				            }//if tp2=2
		            }//success
		          });//ajax segundo
		          
		        } //success primero
		      });
		    }//tipo a o b
		    if(inciso=="c" || inciso=="d"){
		      	$.ajax({
		        	type: "POST",
		        	url: base_url+"index.php/Clientes_c_beneficiario/add_benem",
		        	data:{
		        		id_tipo_perfil:$('#tipoc').val(),
		                id_perfilamiento:$('#idp').val(),
		                tipo_persona:aux,
		                dueno_beneficiario:$('#idc').val(),
		                tipo_inciso: inciso,
		                tipo_p: "0"
		            },
			        statusCode:{
			          404: function(data){
			              swal("Error!", "No Se encuentra la ruta", "error");
			          },
			          500: function(){
			              swal("Error!", "500", "error");
			          }
			        },
		        	success: function (data){
			            var array = $.parseJSON(data);
			            id_bene_moral = array.idbm;
			            id_trans_bene = array.id;
			            var aux_t;
			            var datos;
			            if($('#accionaria_socio_c').is(':checked')){
			              	aux_t = 'c';
			                if($('#persona_fisica_c').is(':checked')){
			                 	datos = $('#beneficiario_moral_f').serialize()+'&tipo_persona='+1+'&tipo_c_d='+aux_t+'&id_bene_moral='+id_bene_moral;
			              	} 
			              	if($('#persona_m_f_c').is(':checked')){
			                 	datos = $('#beneficiario_moral_m').serialize()+'&tipo_persona='+2+'&tipo_c_d='+aux_t+'&id_bene_moral='+id_bene_moral;
			              	} 
			            } 
			            if($('#accionaria_socio_d').is(':checked')){
			                aux_t = 'd';
			                datos = $('#beneficiario_moral_d').serialize()+'&tipo_c_d='+aux_t+'&id_bene_moral='+id_bene_moral;
		            	}   
			            $.ajax({ //datos para el socio 1
			                type: "POST",
			                url: base_url+"Clientes_cliente/guardar_persona_c_d",
			                data: datos,
			                success: function (data){
			                  swal("Éxito", "Guardado correctamente", "success");
			                  setTimeout(function(){ 
			                  	window.history.back();
			                  }, 2500);
			                }
			            });
		        	}
		    	});
			}// termina c_d

	  	}//termina if de persona moral
  	});
	
});

function especifique_btnpaso3() {
	if($('#check_direccion_a').is(':checked')){
      $('#especifique').css('display','block');
	}else{
	  $('#especifique').css('display','none');	
	}
}
function lugar_recidencia_btn_paso3() {
	if($('#lugar_recidencia').is(':checked')){
      $('.residenciaEx').css('display','block');
	}else{
	  $('.residenciaEx').css('display','none');	
	}
}
function lugar_recidencia_btn_socio() {
	if($('#lugar_recidencia_socio').is(':checked')){
      $('.residencia_extranjero').css('display','block');
	}else{
	  $('.residencia_extranjero').css('display','none');	
	}
}
function lugar_recidencia_btn_socio2() {
	if($('#lugar_recidencia_socio2').is(':checked')){
      $('.residencia_extranjero2').css('display','block');
	}else{
	  $('.residencia_extranjero2').css('display','none');	
	}
}
function accionaria_socioa_a_btn() {
	if($('#accionaria_socioa_a').is(':checked')){
      $('.accion_socio_a').css('display','block');
	}else{
	  $('.accion_socio_a').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_2_accion_a() {
	if($('#lugar_recidencia_socio_2_a').is(':checked')){
      $('.residencia_extranjero_socio2_accion_a').css('display','block');
	}else{
	  $('.residencia_extranjero_socio2_accion_a').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_2_2_accion_a() {
	if($('#lugar_recidencia_socio_2_2_a').is(':checked')){
      $('.residencia_extranjero_socio2_2_accion_a').css('display','block');
	}else{
	  $('.residencia_extranjero_socio2_2_accion_a').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_accion_a_socio2() {
	if($('#lugar_recidencia_socio2_accion_a').is(':checked')){
      $('.residencia_extranjero_accion_a_socio2').css('display','block');
	}else{
	  $('.residencia_extranjero_accion_a_socio2').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_accion_a_socio2_2() {
	if($('#lugar_recidencia_socio2_accion_a2').is(':checked')){
      $('.residencia_extranjero_accion_a_socio2_2').css('display','block');
	}else{
	  $('.residencia_extranjero_accion_a_socio2_2').css('display','none');	
	}
}
//////////////// b //////////////////////
function accionaria_socioa_b_btn() {
	if($('#lugar_recidencia_b').is(':checked')){
      $('.accion_socio_b').css('display','block');
	}else{
	  $('.accion_socio_b').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_b() {
	if($('#lugar_recidencia_socio_b').is(':checked')){
      $('.residencia_extranjero_b').css('display','block');
	}else{
	  $('.residencia_extranjero_b').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_2_accion_b() {
	if($('#lugar_recidencia_socio_2_b').is(':checked')){
      $('.residencia_extranjero_socio2_accion_b').css('display','block');
	}else{
	  $('.residencia_extranjero_socio2_accion_b').css('display','none');	
	}
}
function lugar_recidencia_btn_socio_accion_b_socio2() {
	if($('#lugar_recidencia_socio2_accion_b').is(':checked')){
      $('.residencia_extranjero_accion_b_socio2').css('display','block');
	}else{
	  $('.residencia_extranjero_accion_b_socio2').css('display','none');	
	}
}

function getpais(id){
  $('.pais'+id).select2({
      width: 255,
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Clientes_cliente/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
function getpais2(id){

    $('.pais'+id).select2({
        width: 255,
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Clientes_cliente/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var pais = $('.pais'+id+' option:selected').val(); 
      if(pais=='MX'){
       //<!-------->
       //console.log(pais); 
        $.ajax({
          type:'POST',
          data: {id:id},
          url: base_url+'Clientes_cliente/get_estadoMoral',
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
            //console.log(id);
            $('.span_div_'+id).html(data);
            //console.log(data);
            //$('.estado'+id+' option[value=""]').attr("selected", "selected");
          }  
        });
       //<!-------->   
      }else{
        $('.span_div_'+id).html('<input class="form-control" type="text" name="estado" id="estado">'); 
      }
      
    });
}