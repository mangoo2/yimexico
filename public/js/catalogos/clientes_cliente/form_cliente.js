var base_url = $('#base_url').val();
$(document).ready(function($) {
  
  if($('#tipo1').is(':checked')){
    $("#cont_pf").show("slow");
  }
  $('#tipo1').click(function(event) {
    $("#cont_pf").show("slow");
    $("#cont_pm").hide("slow");
    $("#cont_fc").hide("slow");
  });
  $('#tipo2').click(function(event) {
    $("#cont_pf").hide("slow");
    $("#cont_pm").show("slow");
    $("#cont_fc").hide("slow");
  });
  $('#tipo3').click(function(event) {
    $("#cont_pf").hide("slow");
    $("#cont_pm").hide("slow");
    $("#cont_fc").show("slow");
  });

  $('#form_clientec').validate({
      rules: {
          calle: "required",
          colonia: "required",
          cp: "required",
          cel: "required",
          rfc_fisica:{
            rfc: true
          },
          rfc:{
            rfc: true
          },
      },
      messages: {
          calle: "Campo requerido",
          colonia: "Campo requerido",
          cp: "Campo requerido",
          cel: "Campo requerido",
      },
      submitHandler: function (form) {
         $.ajax({
             type: "POST",
             url: base_url+"index.php/Clientes_cliente/submit",
             data: $(form).serialize(),
             beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
             },
             success: function (result) {
                console.log(result);
                $("#btn_submit").attr("disabled",false);
                swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                setTimeout(function () { window.location.href = base_url+"index.php/Clientes_cliente/" }, 1500);
             }
         });
         return false; // required to block normal submit for ajax
       },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
        }
  });


});

