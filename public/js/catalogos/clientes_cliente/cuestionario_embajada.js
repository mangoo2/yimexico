$(document).ready(function(){
    $('#btn_submit').click(function(event){
        guardar_form();
    });
});

function guardar_form(){
    var form_register = $('#form_cuestionario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules:{
          motivo_transac: "required"
        },
        errorPlacement: function(error, element) {
          if ( element.is(":radio") ) {
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var valid = $("#form_cuestionario").valid();
    if(valid){
      //modal_imprimir();
      guardar_form_cuestionario2();
    }
}

function modal_imprimir(){
    $('#modalimprimir').modal();
}
function guardar_form_cuestionario2(){
  $('#btn_submit').attr('disabled',true);
    var datos = $('#form_cuestionario').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Clientes_cliente/add_cuestionario_embajada',
        data: datos,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Actualizado Correctamente", "success");
            idcc = $("#idclientec").val();
            idp = $("#idperfilamiento").val();
            tipo = $("#tipoc").val();
            ido = $("#idopera").val();
            idcgr = $("#idcgr").val();
            window.open(base_url+'Clientes_cliente/generarPDF_CAE/'+idcc+"/"+idp+"/"+tipo+"/"+ido+"/"+idcgr, '_blank');
            setTimeout(function () { 
                location.href= base_url+'Operaciones/gradoRiesgo/'+$("#idopera").val()+"/2/"+$("#idperfilamiento").val();
            }, 2000);
        }
    });
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}

function imprimir_formato(){
    var pre=$('input:radio[name=imprimir]:checked').val();
    if(pre==1){
        $('#modalimprimir').modal('hide');
        $('.btn_guardar_text').css('display','none');
        $('.texto_firma').css('display','block');
        $('#break_emple').css('display','block');
        setTimeout(function () { 
            window.print();
        }, 500);
        setTimeout(function () { 
            $('.btn_guardar_text').css('display','block');
            $('.texto_firma').css('display','none');
            $('#break_emple').css('display','none');
        }, 1000);
    }else{
        $('#modalimprimir').modal('hide');
        guardar_form_cuestionario2();
    }
}

function imprimir_for(){
    /*$('.btn_guardar_text').css('display','none');
    $('.firma').css('display','block');
    $('#break_emple').css('display','block');
    setTimeout(function () { 
        window.print();
    }, 500);
    setTimeout(function () { 
        $('.btn_guardar_text').css('display','block');
        $('.firma').css('display','none');
        $('#break_emple').css('display','none');
    }, 1000);*/
    idcc = $("#idclientec").val();
    idp = $("#idperfilamiento").val();
    tipo = $("#tipoc").val();
    ido = $("#idopera").val();
    idcgr = $("#idcgr").val();
    window.open(base_url+'Clientes_cliente/generarPDF_CAE/'+idcc+"/"+idp+"/"+tipo+"/"+ido+"/"+idcgr, '_blank');
}

function regresar_view(){
    window.history.back();
}