$(document).ready(function(){

    $('.ingreso_extran').click(function(event){
        ingreso_extran();
    });
    $('.relacion_extran').click(function(event){
        relacion_extran();
    });
    $('.sucursales').click(function(event){
        sucursales();
    });
    $('.sucursal_otro_pais').click(function(event){
        sucursal_otro_pais();
    });
    $('.empresa_grupo').click(function(event){
        empresa_grupo();
    });

    $('#btn_submit').click(function(event){
        guardar_form();
    });
    
    if($("#id").val()>0){
        setTimeout(function () { 
            ingreso_extran();
            relacion_extran();
            sucursales();
            sucursal_otro_pais();
            empresa_grupo();
        }, 1000);
    }
});

function ingreso_extran(){
    var pre=$('input:radio[name=ingreso_extran]:checked').val();  //si
    if(pre==1){
        $("#div_pais_ext").show();
    }else{
        $("#div_pais_ext").hide();
    }
}
function relacion_extran(){
    var pre2=$('input:radio[name=relacion_extran]:checked').val();  //si
    if(pre2==1){
        $("#div_relacion").show();
    }else{
        $("#div_relacion").hide();
    }
}
function sucursales(){
    var pre3=$('input:radio[name=sucursales]:checked').val();  //si
    if(pre3==1){
        $(".div_sucurs").show();
    }else{
        $(".div_sucurs").hide();
    }
}
function sucursal_otro_pais(){
    var pre3=$('input:radio[name=sucursal_otro_pais]:checked').val();  //si
    if(pre3==1){
        $(".div_sucurs_otro").show();
    }else{
        $(".div_sucurs_otro").hide();
    }
}
function empresa_grupo(){
    var pre4=$('input:radio[name=empresa_grupo]:checked').val();
    if(pre4==1){
        $(".div_cta_emp").show();
    }else{
        $(".div_cta_emp").hide();   
    }
}

function guardar_form(){
    var form_register = $('#form_cuestionario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules:{
          actividad: "required",
          fuente_ingreso: "required",
          porcentaje: "required",
          //otras_fuentes: "required",
          monto_ventas_mes: "required",
          ingreso_extran: "required",
          relacion_extran: "required",
          efectivo: "required",
          transfer_otro: "required",
          transfer_inter: "required",
          tarjeta: "required",
          cheques: "required",
          sucursales: "required",
          sucursal_otro_pais: "required",
          num_emplea: "required",
          num_cli: "required",
          num_prov: "required",
          empresa_grupo: "required",
          //cuantas_empre: "required",
          consejo_admin: "required",
          motivo_transac: "required"
        },
        errorPlacement: function(error, element) {
          if ( element.is(":radio") ) {
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var valid = $("#form_cuestionario").valid();
    if(valid){
      //modal_imprimir();
      guardar_form_cuestionario2();
    }
}

function modal_imprimir(){
    $('#modalimprimir').modal();
}
function guardar_form_cuestionario2(){
  $('#btn_submit').attr('disabled',true);
    var datos = $('#form_cuestionario').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Clientes_cliente/add_cuestionario_moral',
        data: datos,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Actualizado Correctamente", "success");
            idcc = $("#idclientec").val();
            idp = $("#idperfilamiento").val();
            tipo = $("#tipoc").val();
            ido = $("#idopera").val();
            idcgr = $("#idcgr").val();
            window.open(base_url+'Clientes_cliente/generarPDF_CAPM/'+idcc+"/"+idp+"/"+tipo+"/"+ido+"/"+idcgr, '_blank');
            setTimeout(function () { 
                location.href= base_url+'Operaciones/gradoRiesgo/'+$("#idopera").val()+"/2/"+$("#idperfilamiento").val();
            }, 2000);
        }
    });
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}

function imprimir_formato(){
    var pre=$('input:radio[name=imprimir]:checked').val();
    if(pre==1){
        $('#modalimprimir').modal('hide');
        $('.btn_guardar_text').css('display','none');
        $('.texto_firma').css('display','block');
        $('#break_emple').css('display','block');
        setTimeout(function () { 
            window.print();
        }, 500);
        setTimeout(function () { 
            $('.btn_guardar_text').css('display','block');
            $('.texto_firma').css('display','none');
            $('#break_emple').css('display','none');
        }, 1000);
    }else{
        $('#modalimprimir').modal('hide');
        guardar_form_cuestionario2();
    }
}

function imprimir_for(){
    /*$('.btn_guardar_text').css('display','none');
    $('.firma').css('display','block');
    $('#break_emple').css('display','block');
    setTimeout(function () { 
        window.print();
    }, 500);
    setTimeout(function () { 
        $('.btn_guardar_text').css('display','block');
        $('.firma').css('display','none');
        $('#break_emple').css('display','none');
    }, 1000);*/
    idcc = $("#idclientec").val();
    idp = $("#idperfilamiento").val();
    tipo = $("#tipoc").val();
    ido = $("#idopera").val();
    idcgr = $("#idcgr").val();
    window.open(base_url+'Clientes_cliente/generarPDF_CAPM/'+idcc+"/"+idp+"/"+tipo+"/"+ido+"/"+idcgr, '_blank');
}

function regresar_view(){
    window.history.back();
}