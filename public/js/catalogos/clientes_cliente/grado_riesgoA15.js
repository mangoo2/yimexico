$(document).ready(function(){
  mostraroptionclasificacion();
  /*var tipo = $("#tipoc").val();
  var idp = $("#idperfilamiento").val();
  var idcc = $("#idclientec").val();*/
  $("#btn_submit").on("click",function(){
    guardar();
  });

    /*$("#forma_pago").on("change",function(){
      if($("#forma_pago").val()!="" && $("#tipo_cliente").val()!=""){
        calculaGrado();
      }else{
        $("#cont_gdo1").html(''); 
      }
    });
    $("#tipo_cliente").on("change",function(){
      if($("#forma_pago").val()!="" && $("#tipo_cliente").val()!=""){
        calculaGrado();
      }else{
        $("#cont_gdo1").html(''); 
      }
    });
    if($("#forma_pago").val()!="" && $("#tipo_cliente").val()!=""){
    calculaGrado();
  }*/
    if($("#id").val()>0){
        $("#txt_title").append(": Consultar calificación");
        setTimeout(function(){ 
            limpiarForm2();
        }, 1500);
    }
    else{
        $("#txt_title").append(": Asignar calificación");
    }
    mostraroptionclasificacion();
    $("#actividad_monto").on("change",function(){
      calculaGrado();
    });
    $("#idtipo_cliente").on("change",function(){
      limpiarForm();
      mostraroptionclasificacion();
      selectAutoTipo();
      calculaGrado();
    });
    $("#detalle_tipo_cli").on("change",function(){
      mostraroptionclasificacion();
      calculaGrado();
    });
    $("#clasificacion").on("change",function(){
      calculaGrado();
      mostraroptionclasificacion();
    });
    $("#edad").on("change",function(){
      calculaGrado();
    });
    $("#nacionalidad").on("change",function(){
      calculaGrado();
    });
    $("#estado").on("change",function(){
      calculaGrado();
    });
    $("#estructura").on("change",function(){
      calculaGrado();
    });
    $("#historico").on("change",function(){
      calculaGrado();
    });
    $("#paises").on("change",function(){
      calculaGrado();
    });
    $("#transaccionalidad").on("change",function(){
      calculaGrado();
    });
    $("#transaccionalidad_forma").on("change",function(){
      calculaGrado();
    });
    $(".trans_pep").on("change",function(){
      calculaGrado();
    });
    $("#c_d_agencia").on("change",function(){
      calculaGrado();
    });
    $("#c_d_notaria").on("change",function(){
      calculaGrado();
    });
    $("#c_d_cliente_usuario").on("change",function(){
      calculaGrado();
    });

    //preguntasPEP();
    $(".preg_pep_most").on("change", function(){
      preguntasPEP();
    });

    setTimeout(function(){ 
      preguntasPEP();
    }, 1000);
    setTimeout(function(){ 
      calculaGrado();
    }, 1500);
    
    $("#actividad_monto").select2({ theme: 'bootstrap4', width:'resolve' });
    $("#transaccionalidad_forma").select2({ theme: 'bootstrap4', width:'resolve' });
});

function selectAutoTipo(){
  if($("#idtipo_cliente option:selected").val()==6){
    $("#detalle_tipo_cli option:selected").val(7);
    $(".fides_tc").attr("selected",true);
  }
  else if($("#idtipo_cliente option:selected").val()==8){
    $("#detalle_tipo_cli option:selected").val(12);
    $(".emb").attr("selected",true);
  }else{
    $("#detalle_tipo_cli option:selected").val(0);
    $(".nda").attr("selected",true);
  }
}

function guardar(){
  $('#form_grado').removeData('validator');
  var form_register = $('#form_grado');
  var error_register = $('.alert-danger', form_register);
  var success_register = $('.alert-success', form_register);
  //console.log("guardo y valido el cliente tipo: "+$("#idtipo_cliente option:selected").val());
  //console.log("form_register: "+form_register);
    if($("#idtipo_cliente option:selected").val()==1){ //falta agregar validacion para fisicos PEP
        var class_select=$("#clasificacion option:selected").val();
        if(class_select>=1 && class_select<8){ //pep
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "input[type='text']:hidden",
                rules: {
                    clasificacion:{
                      required: true
                    },
                    transaccionalidad:{
                      required: true
                    },
                    transaccionalidad_forma:{
                      required: true
                    },
                    /*c_d_notaria:{
                      required: true
                    },*/
                    c_d_cliente_usuario:{
                      required: true
                    },
                    preg1:{
                      required: true
                    },
                    preg4:{
                      required: true
                    },
                    preg5:{
                      required: true
                    },
                    preg9:{
                      required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            if($("#preg5 option:selected").val()==2){
                //console.log("2do if de fisica en no lo pagará el cliente directamente: ");
                $('#form_grado').removeData('validator');
                var $validator1=form_register.validate({
                    errorElement: 'div', //default input error message container
                    errorClass: 'vd_red', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    //ignore: "input[type='text']:hidden",
                    rules: {
                        clasificacion:{
                          required: true
                        },
                        transaccionalidad:{
                          required: true
                        },
                        transaccionalidad_forma:{
                          required: true
                        },
                        /*c_d_notaria:{
                          required: true
                        },*/
                        c_d_cliente_usuario:{
                          required: true
                        },
                        preg1:{
                          required: true
                        },
                        preg4:{
                          required: true
                        },
                        preg5:{
                          required: true
                        },
                        preg9:{
                          required: true
                        },

                        preg6:{
                          required: true
                        },
                        preg7:{
                          required: true
                        },
                        preg8:{
                          required: true
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                            element.parent().append(error);
                        } else if (element.parent().hasClass("vd_input-wrapper")){
                            error.insertAfter(element.parent());
                        }else {
                            error.insertAfter(element);
                        }
                    }, 
                    
                    invalidHandler: function (event, validator) { //display error alert on form submit              
                            success_register.fadeOut(500);
                            error_register.fadeIn(500);
                            scrollTo(form_register,-100);

                    },

                    highlight: function (element) { // hightlight error inputs
                
                        $(element).addClass('vd_bd-red');
                        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                    },

                    unhighlight: function (element) { // revert the change dony by hightlight
                        $(element)
                            .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label, element) {
                        label
                            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                        $(element).removeClass('vd_bd-red');
                    }
                });
            }
        }
        if(class_select=="" || class_select==8){ //no pep
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "input[type='text']:hidden",
                rules: {
                    clasificacion:{
                      required: true
                    },
                    transaccionalidad:{
                      required: true
                    },
                    transaccionalidad_forma:{
                      required: true
                    },
                    /*c_d_notaria:{
                      required: true
                    },*/
                    c_d_cliente_usuario:{
                      required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
        }
    }else if($("#idtipo_cliente option:selected").val()==2){
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "input[type='text']:hidden",
            rules: {
                estructura:{
                  required: true
                },
                transaccionalidad:{
                  required: true
                },
                transaccionalidad_forma:{
                  required: true
                },
                /*c_d_notaria:{
                  required: true
                },*/
                c_d_cliente_usuario:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
    }
    else if($("#idtipo_cliente option:selected").val()==3 || $("#idtipo_cliente option:selected").val()==4 || $("#idtipo_cliente option:selected").val()==6 || $("#idtipo_cliente option:selected").val()==7){
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "input[type='text']:hidden",
            rules: {
                detalle_tipo_cli:{
                  required: true
                },
                transaccionalidad:{
                  required: true
                },
                transaccionalidad_forma:{
                  required: true
                },
                /*c_d_notaria:{
                  required: true
                },*/
                c_d_cliente_usuario:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
    }else if($("#idtipo_cliente option:selected").val()==5){
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "input[type='text']:hidden",
            rules: {
                clasificacion:{ required: true },
                preg1:{ required: true },
                //preg6:{ required: true },
                preg4:{ required: true },
                preg5:{ required: true },
                preg9:{ required: true },
                //c_d_notaria:{ required: true },
                c_d_cliente_usuari: { required: true }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        if($("#preg5 option:selected").val()==2){
            //console.log("2do if de fisica en no lo pagará el cliente directamente: ");
            $('#form_grado').removeData('validator');
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                //ignore: "input[type='text']:hidden",
                rules: {
                    clasificacion:{ required: true },
                    preg1:{ required: true },
                    //preg6:{ required: true },
                    preg4:{ required: true },
                    preg5:{ required: true },
                    preg9:{ required: true },
                    //c_d_notaria:{ required: true },
                    c_d_cliente_usuario: { required: true },

                    preg6:{
                      required: true
                    },
                    preg7:{
                      required: true
                    },
                    preg8:{
                      required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
        }
    }
  var valid = form_register.valid();
    if(valid) {
      var datos = form_register.serialize();
      $.ajax({
             type: "POST",
             url: base_url+"Clientes_cliente/guardar_grado",
             data: datos+"&grado="+$("#grado_final").val()+"&id_operacion="+$("#idopera").val(),
             beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
             },
             success: function (result) {
                console.log(result);
                $("#btn_submit").attr("disabled",false);
                swal("Éxito!", "Se han guardado los datos correctamente", "success");
                //setTimeout(function () { window.location.href = base_url+"Operaciones/proceso_operacion"+"/"+$("#idopera").val()+"/"+$("#idperfilamiento").val()+"/"+$("#idclientec").val() }, 1500);
                setTimeout(function () { 
                  var gf_aux=$("#grado_final").val();
                  var idact=$("#idact").val();
                  //history.back();
                  if($("#id_union").val()>0){
                    window.location.href = base_url+"Operaciones/gradoRiesgo"+"/"+$("#idopera").val()+"/1/"+$("#idperfilamiento").val(); 
                  }else{
                    //window.location.href = base_url+"Operaciones/procesoInicial"+"/"+$("#idopera").val(); 
                    window.location.href = base_url+"Operaciones/gradoRiesgo"+"/"+$("#idopera").val()+"/1/"+$("#idperfilamiento").val();
                  }
                  //mandar a grado riesgo tambien el id_perfilamiento para redireccionar en ese contralador a documentos o a cuestionario ampliado dependiendo el caso
                }, 1500);
             }
        });
    }
}

function preguntasPEP(){
  if($("#preg1 option:selected").val()==3){ //preg22 conteedor
    $("#preg23").show("slow");
    $("#preg24").show("slow");
  }else{ //no
    $("#preg23").hide("slow");
    $("#preg24").hide("slow");
  }

  if($("#preg5 option:selected").val()==2){ //no lo liquida directamente el cliente
    $("#preg27").show("slow");
    $("#preg28").show("slow");
    $("#preg29").show("slow");
  }else{ //si lo liquida el cliente
    $("#preg27").hide("slow");
    $("#preg28").hide("slow");
    $("#preg29").hide("slow");
  }
  /*if($("#preg5 option:selected").val()==1){ 
    $("#preg26_1").show("slow");
  }else{ //no
    $("#preg26_1").hide("slow");
  } */
}

function calculaGrado(){
  var clasificacion=0;
  monto=0; historico=0;

  monto = $("#actividad_monto option:selected").data("nivelriesgo");
  tipo = $("#idtipo_cliente option:selected").val();
  //detalle_tipo = $("#detalle_tipo_cli").val();
  detalle_tipo = $("#detalle_tipo_cli option:selected").data("nivelriesgo");
  edad = $("#edad").val();
  nacionalidad = $("#nacionalidad option:selected").val();
  profesion = $("#profesion option:selected").val();
  estructura = $("#estructura option:selected").val();
  historico = $("#historico option:selected").val();
  paises = $("#paises option:selected").val();
  transac = $("#transaccionalidad option:selected").val();

  calc_monto=0;
  calc_clasificacion=0;
  calc_edad=0;
  calc_nac=0;
  calc_hist=0;
  calc_prof=0;
  calc_estru=0;
  calc_tipo=0;
  calc_pais=0;
  calc_trans=0;
  calc_transp1=0;
  calc_transp2=0;

  if(tipo==1 || tipo==5)
    $("#name_edad").html("Edad");
  else
    $("#name_edad").html("Constitución");

  calc_monto=$("#actividad_monto option:selected").data("nivelriesgo");

  if(tipo==1){ //Persona Física Nacionalidad Mexicana y Extranjera
    calc_edad=$("#edad option:selected").data("nivelriesgo");
    calc_clasificacion=$("#clasificacion option:selected").data("nivelriesgo"); //tipo de cliente
  }
  
  if(tipo==1){ //Persona Física Nacionalidad Mexicana y Extranjera
    calc_nac=0;
    if(nacionalidad==1)
      calc_nac=1;
    else if(nacionalidad==2)
      calc_nac=2.5;
  }
  
    if(tipo==1 || tipo==2){
       calc_hist=$('#historico option:selected').data('nivelriesgo');
    }
    else if(tipo==5){
        if(historico==1)
            calc_hist=1;
        if(historico==2)
            calc_hist=3;
        if(historico==3)
            calc_hist=4;
    }
  
  if(tipo==1){ //Persona Física Nacionalidad Mexicana y Extranjera
    calc_prof = $('#profesion option:selected').data('nivelriesgo');
    $("#prof_pmm").hide();
  }
  
  if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
    calc_edad=$("#edad option:selected").data("nivelriesgo");
    //calc_clasificacion=1 //tipo de cliente
  }
  
  if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
    if(nacionalidad==1)
      calc_nac=1;
    else if(nacionalidad==2)
      calc_nac=3;
  }
  
  if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
    calc_estru=$('#estructura option:selected').data('nivelriesgo');
  }
  
  if(tipo==2){ //Persona Moral Nacionalidad Mexicana y Extranjera
    $("#prof_pfme").hide();
    calc_prof = $('#profesion option:selected').data('nivelriesgo');
  }

  if(tipo==3){ //Entidades Gubernamentales Federales, Estatales y Municipales 
    calc_tipo=3;
    calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo"); //tipo de cliente
  }

  if(tipo==4){ //Entidades Financieras Bancarias y no Bancarias 
    
    calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo"); //tipo de cliente
  }

  if(tipo==5){ //Personas Políticamente Expuestas nacionales e internacionales
    if(nacionalidad==1)
      calc_nac=2.5;
    else if(nacionalidad==2)
      calc_nac=3;

    calc_clasificacion=$("#clasificacion option:selected").data("nivelriesgo");
  }

  if(tipo==6 || tipo==7){
    calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo");
  }

  if(tipo==8){ //Embajadas, Consulados u Organismos Internacionales 
    calc_clasificacion=$("#detalle_tipo_cli option:selected").data("nivelriesgo"); //tipo de cliente
  }

  if(tipo==5){ //Asociaciones Sin Fines de Lucro
    calc_tipo=3;
    calc_edad=$("#edad option:selected").data("nivelriesgo");
  }

  if(paises==1){ //Países y áreas geográficas
    calc_pais=1;
  }else if(paises==2){ //Países y áreas geográficas
    calc_pais=2;
  }else if(paises==3){ //Países y áreas geográficas
    calc_pais=3;
  }

  var idtc=$("#idtipo_cliente option:selected").val();
  var clasi = $("#clasificacion option:selected").val();
  if(idtc=="1"&& clasi==8 || idtc=="2" || idtc=="3" || idtc=="4" || idtc=="6" || idtc=="7" || idtc=="8"){ //distinto a pep
    calc_transp1 = $("#transaccionalidad option:selected").data("nivelriesgo"); 
    calc_transp2 = $("#transaccionalidad_forma option:selected").data("nivelriesgo");
    calc_trans = calc_transp1+calc_transp2;
    calc_trans = calc_trans/2;
  }else if(idtc=="5" || idtc=="1" && clasi>=1 && clasi<8){ //clientes PEP
    var num_preg=parseFloat($('#padre_trans_pep div:visible').length);
    var n_p_aux=0;
    val_p1=$("#preg1 option:selected").data("nivelriesgo");
    val_p4=$("#preg4 option:selected").data("nivelriesgo");
    val_p5=$("#preg5 option:selected").data("nivelriesgo");   
    val_p9=$("#preg9 option:selected").data("nivelriesgo");
    //val_p10=$("#preg10 option:selected").data("nivelriesgo");
    pre_calc_trans=val_p1+val_p4+val_p5+val_p9;
    //calc_trans=pre_calc_trans/num_preg;
    calc_trans=pre_calc_trans/4;
    //console.log("pre_calc_trans: "+pre_calc_trans);
    //console.log("n_p_aux: "+n_p_aux);
  }
  //console.log("calc_trans: "+calc_trans);


  var canaldistri = 0;
  var canaldistricu=0
  var canaltotal=0

  //canaldistri = $('#c_d_agencia option:selected').data('nivelriesgo');
  canaldistricu = $('#c_d_cliente_usuario option:selected').data('nivelriesgocu');
  canaltotal=canaldistri+canaldistricu;
  //canaltotal=canaltotal/2;

  var calc_clasificacion_fin=0;
  if($("#idtipo_cliente option:selected").val()==1 || $("#idtipo_cliente option:selected").val()==2 || $("#idtipo_cliente option:selected").val()==5)
  {
    if($("#idtipo_cliente option:selected").val()==1){
      calc_clasificacion_fin=parseFloat(calc_clasificacion)+parseFloat(calc_edad)+parseFloat(calc_nac)+parseFloat(calc_hist)+parseFloat(calc_prof);
      //console.log("calc_clasificacion_fin: "+calc_clasificacion_fin);
      var div_num=5;
    }
    if($("#idtipo_cliente option:selected").val()==2){
      calc_clasificacion_fin=parseFloat(calc_edad)+parseFloat(calc_nac)+parseFloat(calc_estru)+parseFloat(calc_hist)+parseFloat(calc_prof);
      var div_num=5;
    }
    if($("#idtipo_cliente option:selected").val()==5){
      calc_clasificacion_fin=parseFloat(calc_clasificacion)+parseFloat(calc_edad)+parseFloat(calc_nac)+parseFloat(calc_hist);
      var div_num=4;
    }
    //console.log("calc_clasificacion: "+calc_clasificacion);
    //console.log("div_num: "+div_num);
    calc_clasificacion_fin = calc_clasificacion_fin/div_num;
  }else{
    calc_clasificacion_fin=calc_clasificacion;
  }
  
  /*console.log("calc_monto: "+calc_monto);
  console.log("calc_tipo: "+calc_tipo);
  console.log("calc_pais: "+calc_pais);
  console.log("calc_trans: "+calc_trans);
  console.log("canaltotal: "+canaltotal);*/

  /*porc_ps=.30;
  porc_tipo=.25;
  porc_paises=.10;
  porc_trans=.25;
  porc_c_distribusion=.10;*/

    porc_ps=.10; //productos y servicios
    porc_tipo=.375; //tipo d cliente
    porc_paises=.05;
    porc_trans=.375;
    porc_c_distribusion=.10;

  puntaje1 = porc_ps*calc_monto;
  puntaje2 = porc_tipo*calc_clasificacion_fin;
  puntaje3 = porc_paises*calc_pais;
  puntaje4 = porc_trans*calc_trans;
  puntaje5 = porc_c_distribusion*canaltotal;

  var total = (puntaje1+puntaje2+puntaje3+puntaje4+puntaje5);
  total = parseFloat(total).toFixed(2);
  //total = total*1;
  //console.log("puntaje final: "+total);
  var val_barra=0;
  if(total<=1.79){ //bajo
    val_barra=parseFloat(50);
    val_barra = parseFloat(val_barra)+parseFloat(total);
    $("#cont_gdo1").html('<div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar"\
     style="width: '+val_barra+'%" aria-valuenow="1" aria-valuemin="'+total+'" aria-valuemax="'+total+'"></div><p style="font-size:29px; color:#4AD079"><b>'+total+'</b></p><br>');
    $("#txt_grado").html('<input name="grado" id="grado_final" type="hidden" value="'+total+'"><span style="font-size: 30px; color:#4AD079">BAJO</span>');
  }
  //else if(total>1.93 && total<=2.29){ //medio
  else if(total>=1.8 && total<=2.0){ //medio
    val_barra=parseFloat(70);
    val_barra = parseFloat(val_barra)+parseFloat(total);
    $("#cont_gdo1").html('<div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar"\
     style="width: '+val_barra+'%" aria-valuenow="1" aria-valuemin="'+total+'" aria-valuemax="'+total+'"></div><p style="font-size:29px; color:#FFC000"><b>'+total+'</b></p><br>');
    $("#txt_grado").html('<input name="grado" id="grado_final" type="hidden" value="'+total+'"><span style="font-size: 30px; color:#FFC000">MEDIO</span>');
  }
  //else if(total>=2.30){ //alto
  else if(total>2.0){ //alto
    val_barra=parseFloat(90);
    val_barra = parseFloat(val_barra)+parseFloat(total);
    $("#cont_gdo1").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar"\
     style="width: '+val_barra+'%" aria-valuenow="1" aria-valuemin="'+total+'" aria-valuemax="'+total+'"></div><p style="font-size:29px; color:#D04A4A"><b>'+total+'</b></p><br>');
    $("#txt_grado").html('<input name="grado" id="grado_final" type="hidden" value="'+total+'"><span style="font-size: 30px; color:#D04A4A">ALTO</span>');
  }

}

function limpiarForm(){
  var idtipo_cliente=$("#idtipo_cliente option:selected").val();
  if(idtipo_cliente==1){
    $("#detalle_tipo_cli").val("0");
    $('#cont_constitucion').show('slow');
    $('#cont_nacional').show('slow'); 
    $('#cont_historico').show('slow');
    $('#cont_profe').show('slow');
    $('#cont_est_acc').hide('slow')
  }else if(idtipo_cliente==2){
    $("#detalle_tipo_cli").val("0");
    $('#cont_constitucion').show('slow');
    $('#cont_nacional').show('slow');
    $('#cont_est_acc').show('slow');
    $('#cont_profe').show('slow');  
    $('#cont_historico').show('slow');
  }else if(idtipo_cliente==3 || idtipo_cliente==4 || idtipo_cliente==6 || idtipo_cliente==7 || idtipo_cliente==8){
    $("#detalle_tipo_cli").val("0");
    $('#cont_constitucion').hide('slow');
    $('#cont_nacional').hide('slow'); 
    $('#cont_historico').hide('slow');
    $('#cont_profe').hide('slow');
    $('#cont_est_acc').hide('slow')
  }
  else if(idtipo_cliente==5){
    $("#detalle_tipo_cli").val("0");
    $('#cont_constitucion').show('slow');
    $('#cont_nacional').show('slow'); 
    $('#cont_historico').show('slow');
    $('#cont_profe').hide('slow');
    $('#cont_est_acc').hide('slow')
  }
}

function limpiarForm2(){
    var idtipo_cliente=$("#idtipo_cliente option:selected").val();
    //$("#clasificacion").val('0');
    if(idtipo_cliente==1){
        $('#cont_constitucion').show('slow');
        $('#cont_nacional').show('slow');   
        $('#cont_historico').show('slow');
        $('#cont_profe').show('slow');
        $('#cont_est_acc').hide('slow')
    }else if(idtipo_cliente==2){
        $('#cont_constitucion').show('slow');
        $('#cont_nacional').show('slow');
        $('#cont_est_acc').show('slow');
        $('#cont_profe').show('slow');  
        $('#cont_historico').show('slow');
    }else if(idtipo_cliente==3 || idtipo_cliente==4 || idtipo_cliente==6 || idtipo_cliente==7 || idtipo_cliente==8){
        $('#cont_constitucion').hide('slow');
        $('#cont_nacional').hide('slow');   
        $('#cont_historico').hide('slow');
        $('#cont_profe').hide('slow');
        $('#cont_est_acc').hide('slow')
    }
    else if(idtipo_cliente==5){
        $('#cont_constitucion').show('slow');
        $('#cont_nacional').show('slow');   
        $('#cont_historico').show('slow');
        $('#cont_profe').hide('slow');
        $('#cont_est_acc').hide('slow')
    }
    //selectAutoTipo();
}

function mostraroptionclasificacion(){
  var idtipo_cliente=$("#idtipo_cliente option:selected").val();
  $('.clasificacion_5').hide('slow');
  $('.clasificacion_1').hide('slow');
  $('.transaccionalidacliente_pep').hide('slow');
  setTimeout(function(){ 
    
    if(idtipo_cliente!=5 || idtipo_cliente==1 && $("#clasificacion").val()==8){
      $('.transaccionalidacliente_pep').hide('slow');
      $('#trans_no_pep').show('slow');
      $('#trans_no_pep2').show('slow');
    }
    if(idtipo_cliente==2){
      $('#cont_est_acc').show('slow');
    }else{
      $('#cont_est_acc').hide('slow');
    }
    if(idtipo_cliente==3 || idtipo_cliente==4 || idtipo_cliente==6 || idtipo_cliente==7 || idtipo_cliente==8){
      $('#cont_det_tipo').show('slow'); 
    }else{
      $('#cont_det_tipo').hide('slow'); 
    }

    if(idtipo_cliente==3){
      $('.ent_gub').show();
      $('.ent_finan').hide();
      $('.fides_tc').hide();
      $('.asocia_sin').hide();
      $('.emb').hide();
    }else if(idtipo_cliente==4){
      $('.ent_finan').show();
      $('.ent_gub').hide();
      $('.fides_tc').hide();
      $('.asocia_sin').hide();
      $('.emb').hide();
    }else if(idtipo_cliente==6){
      $('.fides_tc').show();
      $('.ent_finan').hide();
      $('.ent_gub').hide();
      $('.asocia_sin').hide();
      $('.emb').hide();
    }else if(idtipo_cliente==7){
      $('.asocia_sin').show();
      $('.fides_tc').hide();
      $('.ent_finan').hide();
      $('.ent_gub').hide();
      $('.emb').hide();
    }else if(idtipo_cliente==8){
      $('.emb').show();
      $('.asocia_sin').hide();
      $('.fides_tc').hide();
      $('.ent_finan').hide();
      $('.ent_gub').hide();
    }


      if(idtipo_cliente==1 || idtipo_cliente==5){
        $('.clasificacionform').show('slow');
        //setTimeout(function(){
          if(idtipo_cliente==1){
            $('.clasificacion_1').show('slow');
            $('.clasificacion_5').hide('slow');
            
            if($("#clasificacion").val()>=1 && $("#clasificacion").val()<8){
              $('.transaccionalidacliente_pep').show('slow');
            }else{
              $('.transaccionalidacliente_pep').hide('slow');
            }
            if($("#clasificacion").val()==8){
                $('#trans_no_pep').show('slow');
                $('#trans_no_pep2').show('slow');
            }else{
                $('#trans_no_pep').hide('slow');
                $('#trans_no_pep2').hide('slow'); 
            }
            preguntasPEP();
          }
          if(idtipo_cliente==5){ //pep
            $('.clasificacion_1_de_11').hide('slow');
            $('.clasificacion_1').hide('slow');
            $('.clasificacion_5').show('slow');
            $('.transaccionalidacliente_pep').show('slow');
            $('#trans_no_pep').hide('slow');
            $('#trans_no_pep2').hide('slow');
            preguntasPEP();
          }
        //}, 800);
      }else{
        $('.clasificacionform').hide('slow');
      }
  }, 800);
  
}