var base_url = $('#base_url').val();
var id_docs = 0;// Esta bariable se va ocupar para almacenar el id del registro en l atabla docs_beneficiario 
var img1=""; var img2=""; var img3=""; var img4=""; var img5=""; var img6=""; var imgcc="";
var tipo = $('.tipo_persona').text();
var imgdet=""; var typePDF=""; var imgprev="";
var imgdetcc=""; var typePDFcc=""; var imgprevcc="";
var imgdet2=""; var typePDF2=""; var imgprev2="";
var imgdet3=""; var typePDF3=""; var imgprev3="";
var imgdet4=""; var typePDF4=""; var imgprev4="";
var imgdet5=""; var typePDF5=""; var imgprev5="";
var imgdet6=""; var typePDF6=""; var imgprev6="";
var imgdet7=""; var typePDF7=""; var imgprev7="";
var imgdet8=""; var typePDF8=""; var imgprev8="";
var iddocbene=""; var imgp=""; var imgp2=""; var imgp3=""; var imgp4=""; var imgp5=""; var imgp6=""; var imgpcc=""; 
var imgp7=""; var imgp8="";
var id_extra;
var cant_docs=0; var cant_docs_capt=0;

$(document).ready(function($) {
  //////
  graficaBarra();
  //////

  /*$.ajax({
     type: "POST",
     url: base_url+"Clientes_c_beneficiario/verificaFoto",
     data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"formato_pb" },
     async: false,
     success: function (result) {
        var data= JSON.parse(result);
        $.each(data, function (key, dato){
          if(dato.formato_pb!=""){
            img1 = dato.formato_pb;
            iddocbene = $("#id").val();
            $("input[id*='archt_1']").attr('checked',true);
            imgprev = base_url+"uploads_benes/formato_perb/"+img1;
            imgdet = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/formato_pb", caption: img1, key:1};
            ext = img1.split('.');
            if(ext[1]!="pdf"){
              imgp = base_url+"uploads_benes/formato_perb/"+img1; 
              typePDF = "false";  
            }else{
              imgp = base_url+"uploads_benes/formato_perb/"+img1;
              imgdet = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/formato_pb", caption: img1, key:1};
              typePDF = "true";
            }

          }
        });
     }
  });
  $("#arch_1").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf"],
      initialPreview: [
        ''+imgp+'',    
      ],
      initialPreviewAsData: typePDF,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
  });*/

  if(tipo==1 || tipo==2 || tipo==3){
    $.ajax({ //formato cc
      type: "POST",
      url: base_url+"Clientes_c_beneficiario/verificaFoto",
      data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"formato_cc" },
      async: false,
      success: function (result) {
        var data= JSON.parse(result);
        $.each(data, function (key, dato){
          if(dato.formato_cc!=""){
            imgcc = dato.formato_cc;
            //console.log("imgcc: "+imgcc);
            $("input[id*='Chimg2']").attr('checked',true);
            iddocbene = $("#id").val();
            imgprevcc = base_url+"uploads_benes/formato_cc/"+imgcc;
            imgdetcc = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/formato_cc", caption: imgcc, key:7};
            ext = imgcc.split('.');
            //console.log("ext[1]: "+ext[1]);
            if(ext[1].toLowerCase()!="pdf"){
              imgpcc = base_url+"uploads_benes/formato_cc/"+imgcc; 
              typePDFcc = "false";  
            }else{
              imgp2 = base_url+"uploads_benes/formato_cc/"+imgcc;
              imgdetcc = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/formato_cc", caption: imgcc, key:7};
              typePDFcc = "true";
            }
            cant_docs_capt++;
          }
        });
      }
    });
    $("#imgcc").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgpcc+'',    
      ],
      initialPreviewAsData: typePDFcc,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetcc
      ],
    }).on("filedeleted",function (event, files, extra){
        //location.reload();
        $(".file-default-preview").html("");
    }).on("filedeleted", function(event,key,jqXHR, data){
        //location.reload();
        $(".file-default-preview").html("");
    });
  }

  if(tipo==1){
    $.ajax({ //ine
      type: "POST",
      url: base_url+"Clientes_c_beneficiario/verificaFoto",
      data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"ine" },
      async: false,
      success: function (result) {
        var data= JSON.parse(result);
        $.each(data, function (key, dato){
          if(dato.ine!=""){
            img2 = dato.ine;
            //console.log("img2: "+img2);
            $("input[id*='archt_3']").attr('checked',true);
            iddocbene = $("#id").val();
            imgprev2 = base_url+"uploads_benes/ine/"+img2;
            imgdet2 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/ine", caption: img2, key:2};
            ext = img2.split('.');
            //console.log("ext_1_ "+ext);
            if(ext[1].toLowerCase()!="pdf"){
              imgp2 = base_url+"uploads_benes/ine/"+img2; 
              typePDF2 = "false";  
            }else{
              imgp2 = base_url+"uploads_benes/ine/"+img2;
              imgdet2 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/ine", caption: img2, key:2};
              typePDF2 = "true";
            }
            cant_docs_capt++;
          }
        });
      }
    });
    $("#arch_3").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev2+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
      initialPreview: [
        ''+imgp2+'',    
      ],
      initialPreviewAsData: typePDF2,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet2
      ]
          
    });
  }

  if(tipo==1 || tipo==2 || tipo==3){
    $.ajax({ //cif
      type: "POST",
      url: base_url+"Clientes_c_beneficiario/verificaFoto",
      data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"cif" },
      async: false,
      success: function (result) {
        var data= JSON.parse(result);
        $.each(data, function (key, dato){
          if(dato.cif!=""){
            img3 = dato.cif;
            $("input[id*='archt_4']").attr('checked',true);
            iddocbene = $("#id").val();
            imgprev3 = base_url+"uploads_benes/cif/"+img3;
            imgdet3 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/cif", caption: img3, key:3};
            ext = img3.split('.');
            if(ext[1].toLowerCase()!="pdf"){
              imgp3 = base_url+"uploads_benes/cif/"+img3; 
              typePDF3 = "false";  
            }else{
              imgp3 = base_url+"uploads_benes/cif/"+img3;
              imgdet3 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/cif", caption: img3, key:3};
              typePDF3 = "true";
            }
            cant_docs_capt++;
          }
        });
      }
    });
    $("#arch_4").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprev3+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
      initialPreview: [
        ''+imgprev3+'',    
      ],
      initialPreviewAsData: typePDF3,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet3
      ]
    });
  }

  if(tipo==1){
    $.ajax({ //curp
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFoto",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"curp" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.curp!=""){
              img4 = dato.curp;
              $("input[id*='archt_5']").attr('checked',true);
              iddocbene = $("#id").val();
              imgprev4 = base_url+"uploads_benes/curp/"+img4;
              imgdet4 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/curp", caption: img4, key:4};
              ext = img4.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp4 = base_url+"uploads_benes/curp/"+img4; 
                typePDF4 = "false";  
              }else{
                imgp4 = base_url+"uploads_benes/curp/"+img4;
                imgdet4 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/curp", caption: img4, key:4};
                typePDF4 = "true";
              }
              cant_docs_capt++;
            }
          });
       }
    });
    $("#arch_5").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: ""+imgprev4+"",
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
        initialPreview: [
          ''+imgprev4+'',    
        ],
        initialPreviewAsData: typePDF4,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
          imgdet4
        ]
    });
  }

  if(tipo==1 || tipo==2){
    $.ajax({ //comprobante 
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFoto",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"comprobante_dom" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.comprobante_dom!=""){
              img5 = dato.comprobante_dom;
              $("input[id*='archt_6']").attr('checked',true);
              iddocbene = $("#id").val();
              imgprev5 = base_url+"uploads_benes/compro_dom/"+img5;
              imgdet5 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/comprobante_dom", caption: img5, key:5};
              ext = img5.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp5 = base_url+"uploads_benes/compro_dom/"+img5; 
                typePDF5 = "false";  
              }else{
                imgp5 = base_url+"uploads_benes/compro_dom/"+img5;
                imgdet5 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/comprobante_dom", caption: img5, key:5};
                typePDF5 = "true";
              }
              cant_docs_capt++;
            }
          });
       }
    });
    $("#arch_6").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: ""+imgprev5+"",
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
        initialPreview: [
          ''+imgprev5+'',    
        ],
        initialPreviewAsData: typePDF5,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
          imgdet5
        ]
    });
  }

  if(tipo==2 || tipo==3){
    $.ajax({ //ine apoderado 
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFoto",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"imgineal" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.imgineal!=""){
              img6 = dato.imgineal;
              $("input[id*='Chimg15']").attr('checked',true);
              iddocbene = $("#id").val();
              imgprev6 = base_url+"uploads_benes/ine_apoderado/"+img6;
              imgdet6 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/imgineal", caption: img6, key:10};
              ext = img6.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp6 = base_url+"uploads_benes/ine_apoderado/"+img6; 
                typePDF6 = "false";  
              }else{
                imgp6 = base_url+"uploads_benes/ine_apoderado/"+img6;
                imgdet6 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/imgineal", caption: img6, key:10};
                typePDF6 = "true";
              }
              cant_docs_capt++;
            }
          });
       }
    });
    $("#imgineal").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: ""+imgprev6+"",
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
        initialPreview: [
          ''+imgprev6+'',    
        ],
        initialPreviewAsData: typePDF6,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
          imgdet6
        ]
    });
    $.ajax({ //escritura 
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFoto",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"esc_ints" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.esc_ints!=""){
              img7 = dato.esc_ints;
              $("input[id*='Chimg12']").attr('checked',true);
              iddocbene = $("#id").val();
              imgprev7 = base_url+"uploads_benes/escrituras/"+img7;
              imgdet7 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/esc_ints", caption: img7, key:10};
              ext = img7.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp7 = base_url+"uploads_benes/escrituras/"+img7; 
                typePDF7 = "false";  
              }else{
                imgp7 = base_url+"uploads_benes/escrituras/"+img7;
                imgdet7 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/esc_ints", caption: img7, key:10};
                typePDF7 = "true";
              }
              cant_docs_capt++;
            }
          });
       }
    });
    $("#esc_ints").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: ""+imgprev7+"",
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
        initialPreview: [
          ''+imgprev7+'',    
        ],
        initialPreviewAsData: typePDF7,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
          imgdet7
        ]
    });
    $.ajax({ //poderes notariales 
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFoto",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"poder_not_fac_serv" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.poder_not_fac_serv!=""){
              img8 = dato.poder_not_fac_serv;
              $("input[id*='Chimg13']").attr('checked',true);
              iddocbene = $("#id").val();
              imgprev8 = base_url+"uploads_benes/poder_notarial/"+img8;
              imgdet8 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/poder_not_fac_serv", caption: img8, key:10};
              ext = img8.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                imgp8 = base_url+"uploads_benes/poder_notarial/"+img8; 
                typePDF8 = "false";  
              }else{
                imgp8 = base_url+"uploads_benes/poder_notarial/"+img8;
                imgdet8 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddocbene+"/poder_not_fac_serv", caption: img8, key:10};
                typePDF8 = "true";
              }
              cant_docs_capt++;
            }
          });
       }
    });
    $("#poder_not_fac_serv").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: ""+imgprev8+"",
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png","pdf", "jpeg"],
        initialPreview: [
          ''+imgprev8+'',    
        ],
        initialPreviewAsData: typePDF8,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
          imgdet8
        ]
    });
  }

  $("#mas_docs_bene").on("click", function(){
    verificaExtras();
  });

  $("input[class*='form-check-input']").attr('disabled',true);
    
 	var tama_perm = false;
 	$('.img').change( function() {
  	filezise = this.files[0].size;                     
  	if(filezise > 2548000) { // 512000 bytes = 500 Kb
	    //console.log($('.img')[0].files[0].size);
	    $(this).val('');
	    swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
		}else { //ok
	    tama_perm = true; 
	    var archivo = this.value;
	    var name = this.id;
	    var col="";
	    //console.log("archivo: "+archivo);
	    //console.log("name: "+this.id);
	    if(name=="arch_1") col="formato_pb"; if(name=="arch_3") col="ine";if(name=="arch_4") col="cif"; if(name=="arch_5") col="curp";
	    if(name=="arch_6") col="comprobante_dom"; if(name=="imgcc") col="formato_cc"; if(name=="imgineal") col="imgineal";
      if(name=="esc_ints") col="esc_ints"; if(name=="poder_not_fac_serv") col="poder_not_fac_serv";

	    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
	    permitida = false;
	    if($('#'+name)[0].files.length > 0) {
      	for (var i = 0; i < extensiones_permitidas.length; i++) {
        	if (extensiones_permitidas[i] == extension) {
         		permitida = true;
         	break;
        	}
      	}  
      	if(permitida==true && tama_perm==true){
        	//console.log("tamaño permitido");
        	var inputFileImage = document.getElementById(name);
        	var file = inputFileImage.files[0];
        	var data = new FormData();
        	//id_docs = $("#id").val();
        	data.append('foto',file);
        	data.append('iddoc',id_docs);
        	$.ajax({
            url:base_url+'Clientes_c_beneficiario/cargafiles_beneficiario/'+$("#id").val()+"/"+$(".id_bene").text()+'/'+$(".idcientec").text()+"/"+col+"/"+tipo+"/"+$('.idopera').text(),
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
              var array = $.parseJSON(data);
              //console.log(array.ok);
              if (array.ok=true) {
                id_docs = array.id;
                //console.log("id_docs: "+id_docs);
                swal("Éxito", "Imagen cargada Correctamente", "success");
                $("#id").val(array.id);
              }else{
                swal("Error!", "No Se encuentra el archivo", "error");
              }
              if(array.id==0) {
                  swal("Éxito", "Imagen cargada Correctamente", "success");
                  $("#id").val(array.id);
                  setTimeout(function(){ 
                  	window.history.back();
                  }, 1500);
              }
              cant_docs_capt++;
              validaCheck();
              graficaBarra();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var data = JSON.parse(jqXHR.responseText);
            }
        	});
        }else if(permitida==false){
            swal("Error!", "Tipo de archivo no permitido", "error");
        } 
	    }else{
	      swal("Error!", "No se a selecionado un archivo", "error");
	    } 
		}
  });

  $.ajax({ // 
    type: "POST",
    url: base_url+"Clientes_c_beneficiario/verificaFoto",
    data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"validado" },
    async: false,
    success: function (result) {
      //console.log("validado: "+result);
      var data= JSON.parse(result);
      $.each(data, function (key, dato){
        if(dato.validado==1){
          //console.log("validado: "+dato.validado);
          //$("#validado").prop('checked',true);
          //$('input[id*="validado"]').prop("checked", true);
          $("input[id*='validado']").attr('checked',true);
        }
      });
    }
  });

  $('#validado').on('click', function() {
    if($('#validado').is(':checked')){
      val = 1;
      title = "¿Desea Validar estos documentos?";
    }else{
      val = 0;
      title = "¿Desea marcar como no validos estos documentos?";
    }
    swal({
        title: title,
        text: "Se cambiará el estatus",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type:'POST',
          url: base_url+'Clientes_c_beneficiario/validar',
          data:{ id: $(".id_benef_m").text(),tipob: $(".tipo_persona").text(), val:val },
          success:function(data){
              swal("Éxito", "Cambio realizado correctamente", "success");
              /*setTimeout(function(){ 
                location.href= base_url+'Clientes_cliente';
              }, 1500);*/
              //setTimeout(function () {  history.back() }, 1500);
          }
        }); //cierra ajax
      }else{
        $('input[id*="validado"]').attr("checked", true).trigger("change");
      }
    });
  });

  $('.imgExt').change( function() {
    filezise = this.files[0].size;                     
    if(filezise > 2548000) { // 1mb
      //console.log($('.img')[0].files[0].size);
      $(this).val('');
      swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
    }else { //ok
      tama_perm = true; 
      var archivo = this.value;
      var name = this.id;
      var col=name;
      //console.log("archivo: "+archivo);
      //console.log("name: "+this.id);

      extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
      extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
      permitida = false;
      if($('#'+name)[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true && tama_perm==true){
          //console.log("tamaño permitido");
          var inputFileImage = document.getElementById(name);
          var file = inputFileImage.files[0];
          var data = new FormData();
          //id_docs = $("#id").val();
          data.append('foto',file);
          //data.append('iddoc',id_docs);
          $.ajax({
            url:base_url+'Clientes_c_beneficiario/cargafilesExtra/'+$("#id_extra").val()+"/"+$(".id_benef_m").text()+'/'+$(".idcientec").text()+"/"+col+"/"+tipo+"/"+$('.idopera').text(),
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
              var array = $.parseJSON(data);
              if (array.ok=true) {
                //id_docs = array.id;
                //console.log("id_docs: "+array.id);
                $("#id_extra").val(array.id);
                swal("Éxito", "Imagen cargada Correctamente", "success");
                //$("#id_extra").val(array.id);
              }else{
                swal("Error!", "No Se encuentra el archivo", "error");
              }
              if(array.id==0) {
                  swal("Éxito", "Imagen cargada Correctamente", "success");
                  $("#id_extra").val(array.id);
                  setTimeout(function(){ 
                    location.href= base_url+'Clientes_cliente';
                  }, 1500);
              }
              //id_extra = array.id;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var data = JSON.parse(jqXHR.responseText);
            }
          });
        }else if(permitida==false){
            swal("Error!", "Tipo de archivo no permitido", "error");
        } 
      }else{
        swal("Error!", "No se a selecionado un archivo", "error");
      } 
    }
  });
  validaCheck();

});	

var bandera_ext=false;
var ext1=""; var ext2=""; var ext3=""; var ext4=""; var ext5="";
var imgdetext1=""; var typePDFext1=""; var imgprevext1="";
var imgdetext2=""; var typePDFext2=""; var imgprevext2="";
var imgdetext3=""; var typePDFext3=""; var imgprevext3="";
var imgdetext4=""; var typePDFext4=""; var imgprevext4="";
var imgdetext5=""; var typePDFext5=""; var imgprevext5="";

function verificaExtras(){
  /** ****** EXTRA 1 ************ */
  if(bandera_ext==false){
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFotoExtra",
       data: { "id": $("#id_extra").val(), "idb": $(".id_benef_m").text(), "tipo": tipo,"col":"extra","col2":"descrip", "id_opera":$('.idopera').text() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra!=""){
              $("input[id*='Chext1']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext1 = dato.extra;
              iddoc = $("#id_extra").val();
              imgprevext1 = base_url+"uploads_benes/extras/"+ext1;
              imgdetext1 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra", caption: ext1, key:16};
              ext = ext1.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext1 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext1 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra", caption: ext1, key:16};
                typePDFext1 = "true";
              }
              $("#descrip").val(dato.descrip);
            }
          });
       }
    });
    $("#extra").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext1+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext1+'',    
      ],
      initialPreviewAsData: typePDFext1,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext1
      ]
    });
    /** ****** EXTRA 2 ************ */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFotoExtra",
       data: { "id": $("#id_extra").val(), "idb": $(".id_benef_m").text(), "tipo": tipo,"col":"extra2","col2":"descrip2", "id_opera":$('.idopera').text() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra2!=""){
              $("input[id*='Chext2']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext2 = dato.extra2;
              iddoc = $("#id_extra").val();
              imgprevext2 = base_url+"uploads_benes/extras/"+ext2;
              imgdetext2 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra2", caption: ext2, key:17};
              ext = ext2.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext2 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext2 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra2", caption: ext2, key:17};
                typePDFext2 = "true";
              }
              $("#descrip2").val(dato.descrip2);
            }
          });
       }
    });
    $("#extra2").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext2+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext2+'',    
      ],
      initialPreviewAsData: typePDFext2,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext2
      ]
    });
    /** ****** EXTRA 3 ************ */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFotoExtra",
       data: { "id": $("#id_extra").val(), "idb": $(".id_benef_m").text(), "tipo": tipo,"col":"extra3","col2":"descrip3", "id_opera":$('.idopera').text() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra3!=""){
              $("input[id*='Chext3']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext3 = dato.extra3;
              iddoc = $("#id_extra").val();
              imgprevext3 = base_url+"uploads_benes/extras/"+ext3;
              imgdetext3 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra3", caption: ext3, key:18};
              ext = ext3.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext3 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext3 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra3", caption: ext3, key:18};
                typePDFext3 = "true";
              }
              $("#descrip3").val(dato.descrip3);
            }
          });
       }
    });
    $("#extra3").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext3+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext3+'',    
      ],
      initialPreviewAsData: typePDFext3,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext3
      ]
    });  
    /** ****** EXTRA 4 ************ */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFotoExtra",
       data: { "id": $("#id_extra").val(), "idb": $(".id_benef_m").text(), "tipo": tipo,"col":"extra4","col2":"descrip4", "id_opera":$('.idopera').text() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra4!=""){
              $("input[id*='Chext4']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext4 = dato.extra4;
              iddoc = $("#id_extra").val();
              imgprevext4 = base_url+"uploads_benes/extras/"+ext4;
              imgdetext4 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra4", caption: ext4, key:19};
              ext = ext4.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext4 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext4 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra4", caption: ext4, key:19};
                typePDFext4 = "true";
              }
              $("#descrip4").val(dato.descrip4);
            }
          });
       }
    });
    $("#extra4").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext4+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext4+'',    
      ],
      initialPreviewAsData: typePDFext4,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext4
      ]
    });
    /** ****** EXTRA 5 ************ */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFotoExtra",
       data: { "id": $("#id_extra").val(), "idb": $(".id_benef_m").text(), "tipo": tipo,"col":"extra5","col2":"descrip5", "id_opera":$('.idopera').text() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.extra5!=""){
              $("input[id*='Chext5']").attr('checked',true);
              //console.log("nombre de foto cc: "+dato.formato_cc);
              ext5 = dato.extra5;
              iddoc = $("#id_extra").val();
              imgprevext5 = base_url+"uploads_benes/extras/"+ext5;
              imgdetext5 = {type:"image", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra5", caption: ext5, key:20};
              ext = ext5.split('.');
              if(ext[1].toLowerCase()!="pdf"){
                //imgpe = base_url+"uploads/extras/"+ext1; 
                typePDFext5 = "false";  
              }else{
               // imgpe = base_url+"uploads/extras/"+ext1;
                imgdetext5 = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_deleteExtra/"+iddoc+"/extra5", caption: ext5, key:20};
                typePDFext5 = "true";
              }
              $("#descrip5").val(dato.descrip5);
            }
          });
       }
    });
    $("#extra5").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: ""+imgprevext5+"",
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "gif","pdf", "jpeg"],
      initialPreview: [
        ''+imgprevext5+'',    
      ],
      initialPreviewAsData: typePDFext5,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdetext5
      ]
    });
  }
  bandera_ext=true;
}

var chk_valid;
var cant_docs_apo = 0;
var total_por_cap;
function validaCheck(){
  if(tipo==1){
    cant_docs=5;     
  }
  if(tipo==2){
    cant_docs=6;     
  }
  if(tipo==3){
    cant_docs=5;     
  }

  total_por_cap = cant_docs - cant_docs_capt;
  console.log("total por cap: "+total_por_cap);
  if(total_por_cap==0){
    $(".valid").show();
    $("#validado").prop("disabled", false);
  }
  if(total_por_cap>0){
    $(".valid").hide(); //porque no le mostró check a dany?? verficar pero dejarlo descomentado para prueba y error
    $("#validado").prop("disabled", true);
  }
  graficaBarra();
}

function guardar_documentos(){
  var desc = $("#descrip").val();
  var desc2 = $("#descrip2").val();
  var desc3 = $("#descrip3").val();
  var desc4 = $("#descrip4").val();
  var desc5 = $("#descrip5").val();
	
  $.ajax({
   type: "POST",
   url: base_url+"Clientes_c_beneficiario/submit_extras",
   data: $("#form_docs_extra").serialize()+"&id="+$("#id_extra").val()+"&descrip="+desc+"&descrip2="+desc2+"&descrip3="+desc3+"&descrip4="+desc4+"&descrip5="+desc5+"&id_beneficiario="+$(".id_benef_m").text()+'&id_clientec='+$(".idcientec").text()+"&tipo_bene="+$("#tipo_persona").text(),
   beforeSend: function(){
      $("#btn_submit").attr("disabled",true);
   },
   success: function (result) {
      console.log(result);
      $("#btn_submit").attr("disabled",false);
      //setTimeout(function () { window.location.href = base_url+"Clientes_cliente" }, 1500);
   }
  });
  swal("Éxito", "Guardado Correctamente", "success");
 	//window.location.href = base_url+"Clientes_cliente"; //regresar a la pagina ant con jquery  
 	//window.history.back();
  //setTimeout(function () {  history.back() }, 1500);
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 2000);
  else
    setTimeout(function () { window.location.href = base_url+"Operaciones"; }, 2000);
}

function graficaBarra(){
  cant_docs_capt = parseFloat(cant_docs_capt);
  //total_por_cap; 
  total_por_cap = cant_docs - cant_docs_capt;

  if(cant_docs==cant_docs_capt  && cant_docs_capt>0){
    total_cap = 100;
  }else if(cant_docs_capt>5){
    total_cap_pre = (100 * cant_docs_capt)/cant_docs;
    //console.log("total_cap_pre: "+total_cap_pre);
    total_cap = total_cap_pre / cant_docs;
    total_cap = total_cap*10;
    total_cap = parseFloat(total_cap).toFixed(2);
  }else{
    total_cap_pre = (100 * cant_docs_capt)/cant_docs;
    //console.log("total_cap_pre: "+total_cap_pre);
    total_cap = parseFloat(total_cap_pre).toFixed(2);
  }
  
  if(cant_docs_capt==0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 1%" aria-valuenow="1" aria-valuemin="100" aria-valuemax="1"></div><p style="font-size:17px"><b>0%</b></p>')
  }
  if(total_cap<=30 && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=45  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=60  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=75  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_cap<=90  && cant_docs_capt>0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+total_cap+'%" aria-valuenow="'+total_cap+'" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>'+total_cap+'%</b></h1>')
  }
  else if(total_por_cap==0){
    $("#cont_barra").html('<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div><h1 style="font-size:17px"><b>100%</b></h1>')
  }
}
function inicio_cliente(){
  window.history.back();
}
function formatoPersonasBloquedas(){
  window.open(base_url+"Clientes_c_beneficiario/formatoPesonasBloqueadas2/"+$('.id_bene').text()+"/"+$('.tipo_persona').text()+"/"+$('.idopera').text(), "Prefactura", "width=780, height=612"); 
}

function tamano_ayuda(){
  $('#modal_carga_ayuda').modal();
}