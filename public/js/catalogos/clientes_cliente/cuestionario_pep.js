$(document).ready(function() {
	ingreso_proviene();
  otra_fuente_ingreso();
  recibe_ingreso_extranjero();
});

function guardar_cuestionario_pep(){
    var form_register = $('#form_cuestionario_pep');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        
        rules: {
          actividad_conforme_alta_fiscal:"required",
          indicar_ultimo_anio_pago_figura_publica:"required",
          dependencia_entidad_laboral:"required",
          ingresos_brutos_mensuales_totales:"required",
          tiene_capacidad_acceso_mover_recursos_gorbierno:"required",
          actividad_econominca_ocupacion_esposo:"required",
          actividad_economica_ocupacion_hijos:"required",
          explicar_razon_cual_operacion_trasanccion:"required",
        },
        
        errorPlacement: function(error, element) {
          if ( element.is(":radio") ) {
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_cuestionario_pep").valid();
    if(valid){
      //modal_imprimir();
      guardar_cuestionario_pep2();
    }
}
function modal_imprimir(){
    $('#modalimprimir').modal();
}

function guardar_cuestionario_pep2(){
  var datos = $('#form_cuestionario_pep').serialize();
    $.ajax({
      type:'POST',
      url: base_url+'Clientes_cliente/add_cuestionario_pep',
      data: datos,
      statusCode:{
        404: function(data){
            swal("Error!", "No Se encuentra el archivo", "error");
        },
        500: function(){
            swal("Error!", "500", "error");
        }
      },
      success:function(data){
          swal("Éxito", "Guardado Correctamente", "success");
          setTimeout(function () { 
            var idopera = $('#idopera').val();
            location.href= base_url+'Operaciones/gradoRiesgo/'+idopera+"/2"+"/"+$("#id_perfilamiento").val();
          }, 1000);
          
      }
    });
}


function ingreso_proviene(){
  var pre=$('input:radio[name=ingreso_proviene_fuente_gobierno]:checked').val();
  if(pre==1){
    $('.texto_fuente_ingreso').css('display','block');
  }else{
    $('.texto_fuente_ingreso').css('display','none');
  }
}

function otra_fuente_ingreso(){
  var pre=$('input:radio[name=fuente_ingreso]:checked').val();
  if(pre==1){
    $('.text_fuente_ingreso').css('display','block');
  }else{
    $('.text_fuente_ingreso').css('display','none');
  }
}

function recibe_ingreso_extranjero(){
  var pre=$('input:radio[name=ingreso_extranjero]:checked').val();
  if(pre==1){
    $('.text_de_que_pais').css('display','block');
  }else{
    $('.text_de_que_pais').css('display','none');
  }
}


function imprimir_formato(){
    var pre=$('input:radio[name=imprimir]:checked').val();
    if(pre==1){
        $('#modalimprimir').modal('hide');
        $('.btn_guardar_text').css('display','none');
        $('.texto_firma').css('display','block');
        setTimeout(function () { 
            window.print();
        }, 500);
        setTimeout(function () { 
            $('.btn_guardar_text').css('display','block');
            $('.texto_firma').css('display','none');
        }, 1000);
    }else{
        $('#modalimprimir').modal('hide');
        guardar_cuestionario_pep2();
    }
}

function imprimir_for(){
    $('.btn_guardar_text').css('display','none');
    $('.texto_firma').css('display','block');
    setTimeout(function () { 
        window.print();
    }, 500);
    setTimeout(function () { 
        $('.btn_guardar_text').css('display','block');
        $('.texto_firma').css('display','none');
    }, 1000);
}

function regresar_view(){
    window.history.back();
}