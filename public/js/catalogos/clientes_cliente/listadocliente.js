var base_url = $('#base_url').val();
var perf = $('.perfil_v').text();
$(document).ready(function($) {
    
    $("#tipo_lista").on("change", function(){
        load();
    });
    load();

    $("#idtipo_cliente").on("change", function(){
      if(this.value!=""){
        $("#btn_down").attr("disabled",false);
        $("#cargar_docs_det").attr("disabled",false);
      }
    }); 
    $("#btn_down").on("click", function(){
      idtc = $("#idtipo_cliente option:selected").val();
      window.open(base_url+'Clientes_cliente/generarMalla/'+idtc, '_blank');
    }); 
    $("#inputFile").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      //uploadUrl: base_url+"OrdenesMan/guardar_malla",
      //defaultPreviewContent: '<img src="'+base_url+'uploads/clientes/'+img1+'" width="50px" alt="Sin Dato">',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["xls", "xlsx", "csv"],
    });

    $("#cargar_docs_det").on("click",function(){
      if(permitida==true && tama_perm==true){
        cargarFile();
      }
    });

    $('.cargar_docs_det').change( function() {
      filezise = this.files[0].size;        
      //console.log($('.img')[0].files[0].size);        
      if(filezise > 512000) { // 512000 bytes = 500 Kb
        //console.log($('.img')[0].files[0].size);
        this.val('');
        swal("Error!", "El archivo supera el límite de peso permitido (1 mb)", "error");
      }else { //ok
        tama_perm = true; 
        var archivo = this.value;
        var name = "inputFile";

        extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
        extensiones_permitidas = new Array(".csv");
        permitida = false;
        if($('#inputFile')[0].files.length > 0) {
          for (var i = 0; i < extensiones_permitidas.length; i++) {
            if (extensiones_permitidas[i] == extension) {
              permitida = true;
              break;
            }
          }  
          /*if(permitida==true && tama_perm==true){
            cargarFile();
          }*/
        }
      }
    });

});

function cargarFile(){
  var archivo = $('input[name=inputFile]').val();
  var extension = $('#inputFile').val().split(".").pop().toLowerCase();
  var Formulario = document.getElementById('frmSubirCSV');
  var dataForm = new FormData(Formulario);

  $.blockUI({ 
    message: '<div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status" id="cont_load">\
        <span class="sr-only">Loading...</span>\
      </div><br>Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
    css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff'
    } 
  });

  $.ajax({
    type:'POST',
    url: base_url+'Clientes_cliente/cargaMasivaDoc/'+$("#idtipo_cliente option:selected").val(),
    data: dataForm,
    cache: false,
    contentType: false,
    processData: false,
    success:function(result){
      //console.log(result);
      if(result>0){
        $.unblockUI();
        //$('#modal_carga_doc').modal('hide');
        load();
        swal("Éxito!", "Se ha cargado los datos correctamente", "success");
        //limpiar fileinput
        $('#inputFile').fileinput('reset');
      }else{
        swal("Error!", "Hubo un problema al cargar datos, actualice he intente de nuevo", "error");
      }
    }
  });
}

var id_union=0;
function load(){
    tabla=$("#table_clientec").DataTable({
        "bProcessing": true,
        //"serverSide": true,
        destroy:true,
        "ajax": {
           "url": base_url+"Clientes_cliente/datatable_records",
           type: "post",
           data: { tipo: $("#tipo_lista option:selected").val() }
        },
        "columnDefs": [
            { "visible": false, "targets": 0 }
        ],
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var html_cliente='';
                    if(row.tipo_cli=="0"){
                        if(row.idtipo_cliente==1){
                           html_cliente=row.idtipo_cliente_p_f_m;
                        }else if(row.idtipo_cliente==2){
                           html_cliente=row.idtipo_cliente_p_f_e;
                        }else if(row.idtipo_cliente==3){
                           html_cliente=row.idtipo_cliente_p_m_m_e;
                        }else if(row.idtipo_cliente==4){
                           html_cliente=row.idtipo_cliente_p_m_m_d;
                        }else if(row.idtipo_cliente==5){
                           html_cliente=row.idtipo_cliente_e_c_o_i;
                        }else if(row.idtipo_cliente==6){
                           html_cliente=row.idtipo_cliente_f;
                        }
                    }else{
                        html_cliente=row.id_cliente_tipo;  
                    }
                    return html_cliente;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var html_cliente='';
                    
                        if(row.tipo_cli=="0"){
                            if(row.idtipo_cliente==1){
                               html_cliente=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                            }else if(row.idtipo_cliente==2){
                                //if(row.nombre!=null){
                                    html_cliente=row.nombre2+' '+row.apellido_paterno2+' '+row.apellido_materno2;
                                //}
                            }else if(row.idtipo_cliente==3){
                               html_cliente=row.razon_social;
                            }else if(row.idtipo_cliente==4){
                               html_cliente=row.nombre_persona;
                            }else if(row.idtipo_cliente==5){
                               html_cliente=row.denominacion2;
                            }else if(row.idtipo_cliente==6){
                               html_cliente=row.denominacion;
                            }
                        }else{
                            /*if(id_union!=row.id_union){
                                if(row.idtipo_cliente==1){
                                   html_cliente+=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                                }else if(row.idtipo_cliente==2){
                                    //if(row.nombre!=null){
                                        html_cliente+=row.nombre2+' '+row.apellido_paterno2+' '+row.apellido_materno2;
                                    //}
                                }else if(row.idtipo_cliente==3){
                                   html_cliente+=row.razon_social;
                                }else if(row.idtipo_cliente==4){
                                   html_cliente+=row.nombre_persona;
                                }else if(row.idtipo_cliente==5){
                                   html_cliente+=row.denominacion2;
                                }else if(row.idtipo_cliente==6){
                                   html_cliente+=row.denominacion;
                                }
                            }
                            id_union=row.id_union;*/
                            html_cliente=row.cliente;
                        }
                    return html_cliente;
                }
            }, 
            //{"data": 'ultimo_reg'}, //fecha de ultima transaccion
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var id_actividad=row.ultimo_activ;
                    var id_transaccion=row.ultimo_id;
                    //console.log("id_actividad: "+id_actividad);
                    //console.log("id_transaccion: "+id_transaccion);
                    if(id_actividad>0){
                        return detalleFecha(id_actividad,id_transaccion,1);
                    }else{
                        return "";
                    }
                }
            },
            /*{"data": null,
                "render": function ( data, type, row, meta ){
                var btn_doc_s;
                var btn_doc_d;
                var btn_color;
            
                if(row.validado==1){
                    btn_doc_d='';
                    btn_color='212b4c';
                }else{
                    btn_doc_d='disabled'; 
                    btn_color='999';
                }
                btn_doc_s='<button type="button" '+btn_doc_d+' class="btn btn-sm transacnva" style="background-color: #'+btn_color+'; color:white; font-size:18px; border-radius: 11px;" title="Paso 2: Nueva Transacción">Transacción <i class="fa fa-usd"></i></button>';                 
                return btn_doc_s;
                }
            },*/
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var btn_doc="";
                    if(row.tipo_cli=="0"){
                        btn_doc='<button type="button" class="btn docs" style="color: #212b4c;" title="Expediente Actual de Docs."> <i class="fa fa-files-o" style="font-size: 22px;"></i></button>'; 
                    }
                    btn_doc+='<button type="button" class="btn benes" style="color: #212b4c;" title="Dueño(s) Beneficiario(s) "> <i class="fa fa-users" style="font-size: 22px;"></i></button>'; 
                    if(row.tipo_cli=="0"){
                        btn_doc+='<button type="button" class="btn archivo" style="color: #212b4c;" title="Expediente total de Docs."> <i class="fa fa-archive" style="font-size: 22px;"></i></button>'; 
                    }
                    var html=btn_doc;
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                if(perf==7){
                    html='<button type="button" class="btn btn-sm visualizar" style="background-color: #00af79; color:white" title="Visualizar"><i class="fa fa-eye"></i></button>';
                }else{
                    var resultado=row.resultado;
                    //console.log("idtipo_cliente: "+row.idtipo_cliente);
                  html+='<!--<button type="button" class="btn btn-sm anexo8xml" style="color: #fff; background-color: #0072ff;" title="Historial de xml"><i class="fa fa-file"></i></button>-->\
                        <button type="button" class="btn historico" style="color: #212b4c;" title="Historial de Transacciones"><i class="fa fa-clock-o" style="font-size: 22px;"></i></button>\
                        <button type="button" class="btn editar" style="color: #212b4c;" title="Editar cliente"><i class="fa fa-edit" style="font-size: 22px;"></i></button>';
                  if(resultado!=null && resultado.length>78 && $("#tipo_lista option:selected").val()==1 ||
                      row.act_precand==1 || row.familiar_1_2_3_grado==1 || row.socio_candidato==1 || row.asesor_candidato==1 || row.responsable_campania==1 
                    || row.empresa_apoderado==1 || row.empresa_apoderado_accionista==1 || row.empresa_precandidato==1 || row.empresa_resp_campania==1
                    || row.idtipo_cliente==5){ //cliente PEP
                    html+='<button type="button" class="btn cuestionario" style="color: #212b4c;" title="Cuestionario PEP"><i class="fa fa-book" style="font-size: 22px;"></i></button>';
                  }      
                    //html+='<button type="button" class="btn cuest_ampliado" style="color: #212b4c;" title="Cuestionario Ampliado"><i class="fa fa-book" style="font-size: 22px;"></i></button>';
                  if(perf!=10){
                      var tipo_aux_t=$("#tipo_lista option:selected").val();
                      var union=0;
                      if(tipo_aux_t==1){
                        union=0;
                      }else{
                        union=row.id_union;
                      }
                      html+='<button type="button" class="btn" style="color: #212b4c;" title="Eliminar" onclick="modaleliminar('+row.idperfilamientop+','+union+','+$("#tipo_lista option:selected").val()+')"><i class="fa fa-trash" style="font-size: 22px;"></i></button>';      
                  }
                }
                
                return html;
                }
            },
        ],
        "order": [[ 1, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });


    /*$('#table_clientec').on('click', 'button.cuest_ampliado', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        if(data.idcc1!=0){
            idcc=data.idcc1;
        }
        if(data.idcc2!=0){
            idcc=data.idcc2;
        }
        if(data.idcc3!=0){
            idcc=data.idcc3;
        }
        if(data.idcc4!=0){
            idcc=data.idcc4;
        }
        if(data.idcc5!=0){
            idcc=data.idcc5;
        }
        if(data.idcc6!=0){
            idcc=data.idcc6;
        }

        if(data.idtipo_cliente==1 || data.idtipo_cliente==2)
          window.location.href = base_url+"Clientes_cliente/cuestionarioAmpliado/"+idcc+"/"+data.idperfilamientop+"/"+data.idtipo_cliente;
        else if(data.idtipo_cliente==3)
          window.location.href = base_url+"Clientes_cliente/cuestionarioAmpliadoMoral/"+data.idperfilamientop+"/"+data.idtipo_cliente;
        else if(data.idtipo_cliente==6)
          window.location.href = base_url+"Clientes_cliente/cuestionarioAmpliadoFide/"+data.idperfilamientop+"/"+data.idtipo_cliente;
        else if(data.idtipo_cliente==4 || data.idtipo_cliente==5)
          window.location.href = base_url+"Clientes_cliente/cuestionarioAmpliadoEmbajada/"+data.idperfilamientop+"/"+data.idtipo_cliente;

    });*/

    $('#table_clientec').on('click', 'button.cuestionario', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        if(data.idtipo_cliente==1 || data.idtipo_cliente==2)
          window.location.href = base_url+"Perfilamiento/formulariopep/"+data.idperfilamientop+"/"+data.idtipo_cliente;
        else
          window.location.href = base_url+"Perfilamiento/formulariopep_moral/"+data.idperfilamientop+"/"+data.idtipo_cliente;
    });

    $('#table_clientec').on('click', 'button.anexo8xml', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        modal_xml(data.idperfilamientop);
        // +"/"+data.idtipo_cliente;
    });
    $('#table_clientec').on('click', 'button.editar', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        if(data.tipo_cli=="0"){
            window.location.href = base_url+"Perfilamiento/editar/"+data.idperfilamientop+"/"+data.idtipo_cliente;
        }else{
            window.location.href = base_url+"Clientes_cliente/union/"+data.id_union;
        }
    });
    $('#table_clientec').on('click', 'button.visualizar', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        if(data.tipo_cli=="0"){
            window.location.href = base_url+"Perfilamiento/vizualizar/"+data.idperfilamientop+"/"+data.idtipo_cliente;
        }
    });
    $('#table_clientec').on('click', 'button.benes', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        if(data.idcc1!=0){
            idcc=data.idcc1;
        }
        if(data.idcc2!=0){
            idcc=data.idcc2;
        }
        if(data.idcc3!=0){
            idcc=data.idcc3;
        }
        if(data.idcc4!=0){
            idcc=data.idcc4;
        }
        if(data.idcc5!=0){
            idcc=data.idcc5;
        }
        if(data.idcc6!=0){
            idcc=data.idcc6;
        }
        if(data.tipo_cli=="0"){
            window.location.href = base_url+"Clientes_c_beneficiario/beneficiarios/"+data.idperfilamientop+"/"+data.idtipo_cliente+"/"+idcc+"/0";
        }else{
           window.location.href = base_url+"Clientes_c_beneficiario/beneficiariosUnion/"+data.id_union; 
        }
    });

    $('#table_clientec').on('click', 'button.transacnva', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();
        //window.location.href = base_url+"Clientes_cliente/registros_transaccion/"+data.idcc;
        /*if(data.idcc1!=0){
            idcc=data.idcc1;
        }
        if(data.idcc2!=0){
            idcc=data.idcc2;
        }
        if(data.idcc3!=0){
            idcc=data.idcc3;
        }
        if(data.idcc4!=0){
            idcc=data.idcc4;
        }
        if(data.idcc5!=0){
            idcc=data.idcc5;
        }
        if(data.idcc6!=0){
            idcc=data.idcc6;
        }
        console.log(idcc);
        modalActivi(idcc,data.idperfilamientop);*/
        window.location.href = base_url+"Operaciones";
    });
    $('#table_clientec').on('click', 'button.historico', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();   
        //console.log("nombre: "+data.nombre);
        var name;
        if(data.idtipo_cliente==1){
           name=data.nombre+' '+data.apellido_paterno+' '+data.apellido_materno;
        }else if(data.idtipo_cliente==2){
           name=data.nombre+' '+data.apellido_paterno+' '+data.apellido_materno;
        }else if(data.idtipo_cliente==3){
           name=data.razon_social;
        }else if(data.idtipo_cliente==4){
           name=data.nombre_persona;
        }else if(data.idtipo_cliente==5 || data.idtipo_cliente==6){
           name=data.denominacion;
        }
        if(data.tipo_cli!="0"){
          name=data.cliente; 
        }
        //console.log("cliente: "+data.cliente)
        $("#nameCliente").text("Cliente: "+name);
        if(data.idcc1!=0){
            idcc=data.idcc1;
        }
        if(data.idcc2!=0){
            idcc=data.idcc2;
        }
        if(data.idcc3!=0){
            idcc=data.idcc3;
        }
        if(data.idcc4!=0){
            idcc=data.idcc4;
        }
        if(data.idcc5!=0){
            idcc=data.idcc5;
        }
        if(data.idcc6!=0){
            idcc=data.idcc6;
        }
        //console.log(idcc);
        detalles(data.idperfilamientop,idcc,data.id_union);
    }); 

    $('#table_clientec').on('click', 'button.docs', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();// el idcc es el id dependiendo de la tabla que depende del id del tipo de cliente   
        if(data.idcc1!=0){
            idcc=data.idcc1;
        }
        if(data.idcc2!=0){
            idcc=data.idcc2;
        }
        if(data.idcc3!=0){
            idcc=data.idcc3;
        }
        if(data.idcc4!=0){
            idcc=data.idcc4;
        }
        if(data.idcc5!=0){
            idcc=data.idcc5;
        }
        if(data.idcc6!=0){
            idcc=data.idcc6;
        }
        //console.log(idcc);
        window.location.href = base_url+"Clientes_cliente/docs_cliente/"+idcc+"/"+data.idtipo_cliente+"/"+data.idperfilamientop;
    });

    $('#table_clientec').on('click', 'button.archivo', function () {
        var tr = $(this).closest('tr');
        var data = tabla.row(tr).data();// el idcc es el id dependiendo de la tabla que depende del id del tipo de cliente   
        if(data.idcc1!=0){
            idcc=data.idcc1;
        }
        if(data.idcc2!=0){
            idcc=data.idcc2;
        }
        if(data.idcc3!=0){
            idcc=data.idcc3;
        }
        if(data.idcc4!=0){
            idcc=data.idcc4;
        }
        if(data.idcc5!=0){
            idcc=data.idcc5;
        }
        if(data.idcc6!=0){
            idcc=data.idcc6;
        }
        //console.log(idcc);
        window.location.href = base_url+"Clientes_cliente/archivos_cliente/"+idcc+"/"+data.idtipo_cliente+"/"+data.idperfilamientop;
    }); 
}

function modalActivi(idc,idp) {
    console.log(idp)
    $('#transacción_actividades').modal();
    $('#idclientec').val(idc);
    $('#idperfila').val(idp);
    actividades_clientes(idc,idp);
}
var tipo_aux=0;
var union_aux=0;
function modaleliminar(id,union,tipo) {
    $('#modal_eliminar').modal();
    $('#idcliente').val(id);
    tipo_aux=tipo;
    union_aux=union;
}
function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes_cliente/updateregistro',
        data:{id:$('#idcliente').val(),union:union_aux,tipo:tipo_aux},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            load();
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                
            }, 1500);
        }
    });     
}

function modaleliminar_union(id) {
    $('#modal_eliminar').modal();
    $('#idcliente').val(id);
}

function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"-"+parte[1]+"-"+parte[0];
}
function modalactividad(id) {
    $('#modal_actividad').modal();
    $('.tabla_actividad').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/get_actividades_cliente',
        data:{id:id},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.tabla_actividad').html(data);
        }
    }); 
}

function aceptarAct(){
    var idc = $('#idclientec').val(); //id del cliente ciente
    var idp = $('#idperfila').val();   //id del perfilamiento
    var idact = $('#selec_activ').val();  //id de la actividad
                                                                             
    window.location.href = base_url+"Clientes_cliente/registros_transaccion/"+idact+"/"+idc+"/"+idp;

    /*if(idact==1)anexo="anexo1";if(idact==2)anexo="anexo2a";if(idact==3)anexo="anexo2b";if(idact==4)anexo="anexo2c";if(idact==5)anexo="anexo3";
    if(idact==6)anexo="anexo4";if(idact==7)anexo="anexo5a";if(idact==8)anexo="anexo5b";if(idact==9)anexo="anexo6";if(idact==10)anexo="anexo7";
    if(idact==11)anexo="anexo8";if(idact==12)anexo="anexo9";if(idact==13)anexo="anexo10"; if(idact==14)anexo="anexo11";if(idact==15)anexo="anexo12a";
    if(idact==16)anexo="anexo12b";if(idact==17)anexo="anexo13";if(idact==18)anexo="anexo14";if(idact==19)anexo="anexo15";if(idact==20)anexo="anexo16";
    window.location.href = base_url+"Transaccion/"+anexo+"/"+idc+"/"+idp;*/
}   

function detalles(idp,idc,id_union){
    /*console.log("funcion detalle");
    console.log("idp: "+idp);
    console.log("idc: "+idc);*/
    $("#detalles").modal();
    table2=$('#detalles_trans').DataTable({
        responsive: true,
        destroy: true,
        "order": [[ 0, "desc" ]],
        //"scrollY": "150px",
        "ajax": {
                "url": base_url+"Transaccion/datatable_Transf",
                type: "POST",
                "dataSrc": "",
                data:{
                  idp:idp, idc:idc,tipo:$("#tipo_lista").val(),id_union:id_union
                },
            },
            "columns": [
                {"data": "idopera_opera"},
                {"data": "transaccion"},
                //{"data": "fecha_reg_opera"},
                {"data": null,
                    "render": function ( data, type, row, meta ) {
                        var id_actividad=row.id_actividad;
                        var id_transaccion=row.id_transaccion;

                        return detalleFecha(id_actividad,id_transaccion,1);
                    }
                },
                {"data": null,
                    "render": function ( data, type, row, meta ) {
                        return detalleFecha(row.id_actividad,row.id_transaccion,2);
                    }
                },
                /*{
                    "data": null,
                    "defaultContent": "<button title='Ver Transacción' type='button' class='btn btn-danger detalle'><i class='mdi mdi-eye'></i></button>\
                    <button title='Ver Dueños Beneficiarios' type='button' class='btn btn-danger duenos'><i class='fa fa-users'></i></button>\
                    <button title='Ver Resultados de busqueda Persona Bloqueada' type='button' class='btn btn-danger busqueda'><i class='fa fa-user'></i></button>\
                    <button title='Ver Calculadora de Grado de Riesgo' type='button' class='btn btn-danger calculadora'><i class='fa fa-calculator'></i></button>"
                }*/
                {"data": null,
                  "render": function ( data, type, row, meta ) {
                    var id_operacion = row.id_operacion;
                    var anexo = row.transaccion;
                    var ane = anexo.split('-');
                    var ruta = ane[0];
                    rr = ruta.replace(" ","");
                    rr = rr.toLowerCase();
                    rr = rr.replace(/ /g, "");
                    rr2 = rr.toString();
                    rr2= "'"+rr2+"'";
                    var id_clientec= row.id_clientec;
                    var id_actividad=row.id_actividad;
                    var id_perfilamiento=row.id_perfilamiento;
                    var id_transaccion=row.id_transaccion;
                    var idtransbene=row.idtransbene;
                    var idopera=row.idopera_opera;
                    //console.log("id_operacion: "+id_operacion);
                    //console.log("idopera: "+idopera);
                    
                    var html="<button title='Ver Dueños Beneficiarios' type='button' class='btn btn-danger' onclick='duenos("+idopera+")'><i class='fa fa-users'></i></button>\
                    <button title='Ver Resultados de busqueda Persona Bloqueada' type='button' class='btn btn-danger' onclick='busqueda("+idopera+")'><i class='fa fa-search'></i></button>\
                    <button title='Ver Calculadora de Grado de Riesgo' type='button' class='btn btn-danger' onclick='calculadora("+idopera+")'><i class='fa fa-calculator'></i></button>\
                    <button title='Ver Expediente de Cliente(s)' type='button' class='btn btn-danger' onclick='expCliente("+idopera+")'><i class='fa fa-files-o'></i></button>\
                <button title='Ver Expediente de Dueño(s) Beneficio(s)' type='button' class='btn btn-danger' onclick='expBene("+idopera+")'><i class='fa fa-files-o'></i></button>";
                    html+='<button title="Ver Transacción" type="button" class="btn btn-danger" onclick="detalleTransac('+rr2+','+id_clientec+','+id_actividad+','+id_perfilamiento+','+id_transaccion+','+idtransbene+','+idopera+')"><i class="mdi mdi-eye"></i></button>';
                    return html;
                  }
                }
            ],
        //language: languageTables
    });   
    /*$('#detalles_trans').on('click', 'button.detalle', function(){
      var tr = $(this).closest('tr');
      var data2 = table2.row(tr).data();
      var anexo = data2.transaccion;
      var ane = anexo.split('-');
      var ruta = ane[0];
      rr = ruta.replace(" ","");
      rr = rr.toLowerCase();
      rr = rr.replace(/ /g, "");
      //console.log(rr);
     // window.location.href = base_url+"Transaccion/"+rr+"/1/"+data.id_clientec+"/"+data.id_actividad+"/"+data.id_perfilamiento+"/"+data.id_transaccion+"/"+data.idtransbene+"/"+data.idopera;
      var cont=0;
      cont++;
      //console.log("cont: "+cont);
      if(cont==1){
        window.open(base_url+"Transaccion/"+rr+"/1/"+data2.id_clientec+"/"+data2.id_actividad+"/"+data2.id_perfilamiento+"/"+data2.id_transaccion+"/"+data2.idtransbene+"/"+data2.idopera, '_blank');
      }
    });*/
    /*$('#detalles_trans').on('click', 'button.duenos', function(){
      var tr = $(this).closest('tr');
      var data2 = table2.row(tr).data();
      var id_operacion = data2.id_operacion;
      var cont=0;
      //window.location.href = base_url+"Operaciones/benesOperacion/"+id_operacion;
      cont++;
      //console.log("cont: "+cont);
      if(cont==1){
        window.open(base_url+"Operaciones/benesOperacion/"+id_operacion, '_blank');
      }
    });*/
    /*$('#detalles_trans').on('click', 'button.busqueda', function(){
      var tr = $(this).closest('tr');
      var data2 = table2.row(tr).data();
      var id_operacion = data2.id_operacion;
      //window.location.href = base_url+"Operaciones/resultadosBusqueda/"+id_operacion;
      var cont=0;
      cont++;
      //console.log("cont: "+cont);
      if(cont==1){
        window.open(base_url+"Operaciones/resultadosBusqueda/"+id_operacion, '_blank');
      }
    });*/
    /*$('#detalles_trans').on('click', 'button.calculadora', function(){
      var tr = $(this).closest('tr');
      var data2 = table2.row(tr).data();
      var id_operacion = data2.id_operacion;
      //window.location.href = base_url+"Operaciones/gradoRiesgo/"+id_operacion;
      var cont=0;
      cont++;
      //console.log("cont: "+cont);
      if(cont==1){
        window.open(base_url+"Operaciones/gradoRiesgo/"+id_operacion, '_blank');
      }
    });*/
}
function getNameMes(mes){
    if(mes==1){
      mes_t = 'Enero';
    }else if(mes==2){
        mes_t = 'Febrero';
    }else if(mes==3){
        mes_t = 'Marzo';
    }else if(mes==4){
        mes_t = 'Abril';
    }else if(mes==5){
        mes_t = 'Mayo';
    }else if(mes==6){
        mes_t = 'Junio';
    }else if(mes==7){
        mes_t = 'Julio';
    }else if(mes==8){
        mes_t = 'Agosto';
    }else if(mes==9){
        mes_t = 'Septiembre';
    }else if(mes==10){
        mes_t = 'Octubre';
    }else if(mes==11){
        mes_t = 'Noviembre';
    }else if(mes==12){
        mes_t = 'Diciembre';
    }
    return mes_t;
}

function detalleFecha(id_actividad,id_transaccion,tipo){
    var valor="";
    $.ajax({
        type: "POST",
        url: base_url+"Clientes_cliente/detalleFechaOpera",
        data:{act:id_actividad,id:id_transaccion},
        async:false,
        success: function (data){
            var array = $.parseJSON(data);
            if(tipo==1){
                valor=array.fecha_operacion;
            }else{
                valor=getNameMes(array.mes_acuse);
            }
            //console.log("valor: "+valor);
        }
    });
    //console.log("valor: "+valor);
    return valor;
}
function detalleTransac(rr,id_clientec,id_actividad,id_perfilamiento,id_transaccion,idtransbene,idopera){
  window.open(base_url+"Transaccion/"+rr+"/1/"+id_clientec+"/"+id_actividad+"/"+id_perfilamiento+"/"+id_transaccion+"/"+idtransbene+"/"+idopera, '_blank');
}
function duenos(id_operacion){
  window.open(base_url+"Operaciones/clientesOperacionBenes/"+id_operacion, '_blank');
}
function busqueda(id_operacion){
  window.open(base_url+"Operaciones/resultadosBusqueda/"+id_operacion, '_blank');
}
function calculadora(id_operacion){
  window.open(base_url+"Operaciones/gradoRiesgo/"+id_operacion, '_blank');
}
function expCliente(id_operacion){
  window.open(base_url+"Operaciones/documentosCliente/"+id_operacion, '_blank');
}
function expBene(id_operacion){
  window.open(base_url+"Operaciones/documentosBenes/"+id_operacion, '_blank');
}

function actividades_clientes(id,idp){
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Clientes_cliente/link_actividades_clientes",
        data:{id:id,idp:idp},
        success: function (data){
            $('.transaccion_acti').html(data);
        }
    });
}
function modal_xml(idp){//idperfilamiento
    $('#modal_xml').modal();
    /*$.ajax({
        type: "POST",
        url: base_url+"index.php/Clientes_cliente/tabla_xml",
        data:{id:idp},
        success: function (data){
            $('.table_xml').html(data);
        }
    });*/
    tableXML=$('#bita_xml').DataTable({
        //responsive: true,
        destroy: true,
        "order": [[ 0, "desc" ]],
        //"scrollY": "150px",
        "ajax": {
                "url": base_url+"Clientes_cliente/tabla_xml",
                type: "POST",
                "dataSrc": "",
                data:{
                  idp:idp
                },
            },
            "columns": [
                {"data": "id_transaccion"},
                {"data": "actividad"},
                {"data": "fecha_reg"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='btn btn-danger descarga'><i class='mdi mdi-download'></i></button>"
                }
            ],
        //language: languageTables
    }); 
    $('#bita_xml').on('click', 'button.descarga', function(){
        //console.log("descargar");
        var tr = $(this).closest('tr');
        var data = tableXML.row(tr).data();
        if(data.id_actividad==11)
            window.location.href = base_url+"Transaccion/anexo8_xml/"+data.id_transaccion+"/"+data.id_perfilamiento;
    });
}
function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}
function regresar_view(){
    window.history.back();
}