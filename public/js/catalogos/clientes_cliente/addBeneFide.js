$(document).ready(function(){

	var idp = $("#idp").val();
	var idt = $("#tipoc").val();
	var idc = $("#idc").val();
  var ido = $("#idopera").val();

  $("#nombre_ejec_imp").text($(".nav-profile-name").text());
  var form_register = $('#form_fide');
  var error_register = $('.alert-danger', form_register);
  var success_register = $('.alert-success', form_register);
	$('#form_fide').validate({
    errorElement: 'div', //default input error message container
      errorClass: 'vd_red', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
	      rules: {
          razon_social: "required",
          rfc: "required",
          referencia: "required",
          rfc:{
            rfc: true
          },
	      },
	      messages: {
	          razon_social: "Campo requerido",
	          //rfc: "Campo requerido",
	          referencia: "Campo requerido"
	      },
	      submitHandler: function (form) {
          $.ajax({
              type: "POST",
              url: base_url+"Clientes_c_beneficiario/add_benem",
              data:{
                id: $('#id').val(),
                id_tipo_perfil:$('#tipoc').val(),
                id_perfilamiento:$('#idp').val(),
                dueno_beneficiario:$('#idc').val(),
                tipo_inciso: "f",
                tipo_p: 3 //fideicomiso
              },
              statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra la ruta", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
              },
               success: function (data){
                var array = $.parseJSON(data);
                id_bene_moral = array.idbm;
      	         $.ajax({
      	             type: "POST",
      	             url: base_url+"Clientes_c_beneficiario/guardar_beneficiario_fidei",
      	             data: $(form).serialize()+'&id_bene_moral='+id_bene_moral,
      	             beforeSend: function(){
      	                $("#btn_submit").attr("disabled",true);
      	             },
      	             success: function (result) {
      	                console.log(result);
      	                $("#btn_submit").attr("disabled",false);
      	                swal("Éxito!", "Se han realizado los cambios correctamente", "success");
      	                //setTimeout(function () { window.location.href = base_url+"Clientes_c_beneficiario/beneficiarios/"+idp+"/"+idt+"/"+idc }, 1500);
                        
                        //////////////////////////
                        id = result;
                        idc = $("#idc").val();
                        idp = $("#idp").val();
                        tipoc = $("#tipoc").val();
                        idopera = $("#idopera").val();
                        idcc = $("#idcc").val();
                        id_union = $("#id_union").val();
                        window.open(base_url+'Clientes_c_beneficiario/generarPDF_BeneFide/'+id+"/"+idc+"/"+idp+"/"+tipoc+"/"+idopera+"/"+idcc+"/"+id_union);
                        /////////////////////
                        
                        if(ido==0){
                          setTimeout(function() { history.back() }, 2500);
                        }else{
                          setTimeout(function() { window.location.href = base_url+"Operaciones/procesoInicial/"+ido }, 2500); 
                        }
      	             }
      	         });
               }
          });
	      return false; // required to block normal submit for ajax
	     },
	      errorPlacement: function(label, element) {
	        label.addClass('mt-2 text-danger');
	        label.insertAfter(element);
	      },
	      highlight: function(element, errorClass) {
	        $(element).parent().addClass('has-danger')
	        $(element).addClass('form-control-danger')
	      },
        success: function (label, element) {
          label
              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
          $(element).removeClass('vd_bd-red');
        }
  	});
});

function imprimir_formato1(){
  //$('.firma_tipo_cliente').css('display','block');
  ////////////////////////////
  //$("#nombre_cli_imp").text($("#razon_social").val());
  ///////////////////////////
  /*setTimeout(function(){ 
    $('.firma_tipo_cliente').css('display','none');
  }, 500);
  window.print();*/

  /*id = $("#id").val();
  idc = $("#idc").val();
  idp = $("#idp").val();
  tipoc = $("#tipoc").val();
  idopera = $("#idopera").val();
  idcc = $("#idcc").val();
  id_union = $("#id_union").val();
  window.open(base_url+'Clientes_c_beneficiario/generarPDF_BeneFide/'+id+"/"+idc+"/"+idp+"/"+tipoc+"/"+idopera+"/"+idcc+"/"+id_union);*/
  $('#modal_fecha').modal('show');
}

function imprimir_formato(){
  fechaf = $('#fecha_f').val();
  id = $("#id").val();
  idc = $("#idc").val();
  idp = $("#idp").val();
  tipoc = $("#tipoc").val();
  idopera = $("#idopera").val();
  idcc = $("#idcc").val();
  id_union = $("#id_union").val();
  window.open(base_url+'Clientes_c_beneficiario/generarPDF_BeneFide/'+id+"/"+idc+"/"+idp+"/"+tipoc+"/"+idopera+"/"+idcc+"/"+id_union+"/"+fechaf);
}

function rfcValido(rfc, aceptarGenerico = true) {
    //const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
    const re = /^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){2,3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))$/;
    var   validado = rfc.match(re);

    if (!validado)  //Coincide con el formato general del regex?
        return false;

    //Separar el dígito verificador del resto del RFC
    const digitoVerificador = validado.pop(),
          rfcSinDigito      = validado.slice(1).join(''),
          len               = rfcSinDigito.length,

    //Obtener el digito esperado
          diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
          indice            = len + 1;
    var   suma,
          digitoEsperado;

    if (len == 12) suma = 0
    else suma = 481; //Ajuste para persona moral

    for(var i=0; i<len; i++)
        suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
    digitoEsperado = 11 - suma % 11;
    if (digitoEsperado == 11) digitoEsperado = 0;
    else if (digitoEsperado == 10) digitoEsperado = "A";

    //El dígito verificador coincide con el esperado?
    // o es un RFC Genérico (ventas a público general)?
    if ((digitoVerificador != digitoEsperado)
     && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
        return false;
    else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
        return false;
    return rfcSinDigito + digitoVerificador;
}

function validarInput(input) {
    var rfc         = input.value.trim().toUpperCase(),
        resultado   = document.getElementById("resultado"),
        valido;
    
    if($(".rfc").val()!=""){
      $("#resultado").show();
      var rfcCorrecto = rfcValido(rfc);   // ⬅️ Acá se comprueba
    
      if (rfcCorrecto) {
        valido = "Válido";
        resultado.classList.add("ok");
      } else {
        valido = "No válido"
        resultado.classList.remove("ok");
      }
      resultado.innerText ="Formato: " + valido;
    }else{
      $("#resultado").hide();
    }
}
 function c_beneficiarios(){
      window.history.back();
  }