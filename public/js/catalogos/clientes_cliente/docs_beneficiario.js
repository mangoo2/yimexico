var base_url = $('#base_url').val();
var id_docs = 0;// Esta bariable se va ocupar para almacenar el id del registro en l atabla docs_beneficiario 
var img1=""; var img2=""; var img3=""; var img4=""; var img5=""; var img6="";
var tipo = $('.tipo_persona').text();
$(document).ready(function($) {
   /*   
    $(".img").fileinput({
   	    showCancel: false,
        showCaption: false,
        showUpload: false,
    });
    
  */
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto_e",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"formato_pb" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.formato_pb!="0" || dato.formato_pb!=""){
              img1 = dato.formato_pb;
              $("input[id*='archt_1']").attr('checked',true);
            }
          });
       }
    });
    $("#arch_1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+base_url+'uploads_benes/formato_perb/'+img1+'" width="50px" alt="Sin Dato">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","pdf"],
        initialPreview: [
        // IMAGE RAW MARKUP
        '<img src="'+base_url+'uploads_benes/formato_perb/'+img1+'" class="kv-preview-data file-preview-image" >',
        
        ],
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type: "image", url: base_url+"Clientes_cliente/file_delete_c/"+$('#id').val()+"/formato_pb", caption: img1, key: 1},
        ]
    });
    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto_e",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"ine" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.ine!="0" || dato.ine!=""){
              img2 = dato.ine;
              $("input[id*='archt_3']").attr('checked',true);
            }
          });
       }
    });
    $("#arch_3").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+base_url+'uploads_benes/ine/'+img2+'" width="50px" alt="Sin Dato">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","pdf"],
        initialPreview: [
        // IMAGE RAW MARKUP
        '<img src="'+base_url+'uploads_benes/ine/'+img2+'" class="kv-preview-data file-preview-image" >',
        
        ],
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type: "image", url: base_url+"Clientes_cliente/file_delete_c/"+$('#id').val()+"/ine", caption: img2, key: 1},
        ]
    });

    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto_e",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"cif" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.cif!="0" || dato.cif!=""){
              img3 = dato.cif;
              $("input[id*='archt_4']").attr('checked',true);
            }
          });
       }
    });
    $("#arch_4").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+base_url+'uploads_benes/cif/'+img3+'" width="50px" alt="Sin Dato">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","pdf"],
        initialPreview: [
        // IMAGE RAW MARKUP
        '<img src="'+base_url+'uploads_benes/cif/'+img3+'" class="kv-preview-data file-preview-image" >',
        
        ],
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type: "image", url: base_url+"Clientes_cliente/file_delete_c/"+$('#id').val()+"/cif", caption: img3, key: 1},
        ]
    });

    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto_e",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"curp" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.curp!="0" || dato.curp!=""){
              img4 = dato.curp;
              $("input[id*='archt_5']").attr('checked',true);
            }
          });
       }
    });
    $("#arch_5").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+base_url+'uploads_benes/curp/'+img4+'" width="50px" alt="Sin Dato">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","pdf"],
        initialPreview: [
        // IMAGE RAW MARKUP
        '<img src="'+base_url+'uploads_benes/curp/'+img4+'" class="kv-preview-data file-preview-image" >',
        
        ],
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type: "image", url: base_url+"Clientes_cliente/file_delete_c/"+$('#id').val()+"/curp", caption: img4, key: 1},
        ]
    });

    $.ajax({
       type: "POST",
       url: base_url+"Clientes_cliente/verificaFoto_e",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"comprobante_dom" },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.comprobante_dom!="0" || dato.comprobante_dom!=""){
              img5 = dato.comprobante_dom;
              $("input[id*='archt_6']").attr('checked',true);
            }
          });
       }
    });
    $("#arch_6").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+base_url+'uploads_benes/compro_dom/'+img5+'" width="50px" alt="Sin Dato">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","pdf"],
        initialPreview: [
        // IMAGE RAW MARKUP
        '<img src="'+base_url+'uploads_benes/compro_dom/'+img5+'" class="kv-preview-data file-preview-image" >',
        
        ],
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type: "image", url: base_url+"Clientes_cliente/file_delete_c/"+$('#id').val()+"/compro_dom", caption: img5, key: 1},
        ]
    });

   	$("input[class*='form-check-input']").attr('disabled',true);
    var tama_perm = false;
   	$('.img').change( function() {
	  	filezise = this.files[0].size;                     
	  	if(filezise > 2548000) { // 512000 bytes = 500 Kb
		    //console.log($('.img')[0].files[0].size);
		    $(this).val('');
		    swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
		}else { //ok
		    tama_perm = true; 
		    var archivo = this.value;
		    var name = this.id;
		    var col="";
		    //console.log("archivo: "+archivo);
		    //console.log("name: "+this.id);
		    if(name=="arch_1") col="formato_pb"; if(name=="arch_3") col="ine";if(name=="arch_4") col="cif"; if(name=="arch_5") col="curp";
		    if(name=="arch_6") col="comprobante_dom";

		    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
		    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".//");
		    permitida = false;
		    if($('#'+name)[0].files.length > 0) {
		      	for (var i = 0; i < extensiones_permitidas.length; i++) {
		        	if (extensiones_permitidas[i] == extension) {
		         		permitida = true;
		         	break;
		        	}
		      	}  
		      	if(permitida==true && tama_perm==true){
		        	//console.log("tamaño permitido");
		          	var inputFileImage = document.getElementById(name);
		          	var file = inputFileImage.files[0];
		          	var data = new FormData();
		          	//id_docs = $("#id").val();
		          	data.append('foto',file);
		          	data.append('iddoc',id_docs);
		          	$.ajax({
		              url:base_url+'Clientes_cliente/cargafiles_beneficiario_e/'+$("#id").val()+"/"+$(".id_benef_m").text()+'/'+$(".idcientec").text()+"/"+col+"/"+tipo,
		              type:'POST',
		              contentType:false,
		              data:data,
		              processData:false,
		              cache:false,
		              success: function(data) {
		                var array = $.parseJSON(data);
		                //console.log(array.ok);
		                if (array.ok=true) {
		                  id_docs = array.id;
		                  //console.log("id_docs: "+id_docs);
		                  swal("Éxito", "Imagen cargada Correctamente", "success");
		                }else{
		                  swal("Error!", "No Se encuentra el archivo", "error");
		                }
		                if(array.id==0) {
		                    swal("Éxito", "Imagen cargada Correctamente", "success");
		                    setTimeout(function(){ 
		                    	window.history.back();
		                    }, 1500);
		                }

		              },
		              error: function(jqXHR, textStatus, errorThrown) {
		                  var data = JSON.parse(jqXHR.responseText);
		              }
		          	});
		        }else if(permitida==false){
		            swal("Error!", "Tipo de archivo no permitido", "error");
		        } 
		    }else{
		      swal("Error!", "No se a selecionado un archivo", "error");
		    } 
		}

    $.ajax({ // 
       type: "POST",
       url: base_url+"Clientes_c_beneficiario/verificaFoto",
       data: { "id": $("#id").val(), "idben": $(".id_benef_m").text(), "tipo":tipo, "col":"validado" },
       async: false,
       success: function (result) {
        console.log("validado: "+result);
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.validado!="0"){
              $("input[id*='validado']").attr('checked',true);
            }
          });
       }
    });

    $('#validado').on('click', function() {
      if($('#validado').is(':checked')){
        val = 1;
        title = "¿Desea Validar estos documentos?";
      }else{
        val = 0;
        title = "¿Desea marcar como no validos estos documentos?";
      }
      swal({
          title: title,
          text: "Se cambiará el estatus",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Clientes_cliente/validar',
            data:{ id: $("#id").val(),val:val },
            success:function(data){
                swal("Éxito", "Cambio realizado correctamente", "success");
                setTimeout(function(){ 
                  location.href= base_url+'Clientes_cliente';
                }, 1500);
            }
          }); //cierra ajax
        }else{
          $('input[id*="validado"]').prop("checked", true).trigger("change");
        }
      });
    });


	});
});	


var chk_valid;

function validaCheck(){
  console.log("valida");
  console.log("img1: "+img1);
  console.log("img2: "+img2);
  console.log("img3: "+img3);
  console.log("img4: "+img4);
  console.log("img5: "+img5);

  if(img1=='' || img2=='' || img3=='' || img4=='' || img5==''){
     chk_valid=true; //no se puede hacer check
  }else{
     chk_valid=false;
  }  

  //$("input[id*='validado']").attr('disabled',chk_valid);
  if(chk_valid==true){
    console.log("chk_valid: "+chk_valid);
    $(".valid").hide(); //porque no le mostró check a dany?? verficar pero dejarlo descomentado para prueba y error
    //console.log('oculto');
    //$("input[id*='validado']").attr('disabled',true); //verificar esta parte, sigue fallando a dany
    $("#validado").prop("disabled", false);
  }
  if(chk_valid==false){
    console.log("chk_valid: "+chk_valid);
    $(".valid").show();
    $("#validado").prop("checked", chk_valid);
    $("input[id*='validado']").prop('disabled',false);
    //$("#validado").prop("disabled", false);
    //$("input[id*='validado']").prop('disabled',chk_valid);
  }

}

function guardar_img(id){
	/*
	  filezise = $('#arch_'+id)[0].files[0].size;     
	  console.log(filezise);                
	  if(filezise > 512000) { // 512000 bytes = 500 Kb arch_1 
	    $('#arch_'+id).val('');
	    swal("Error!", "El archivo supera el límite de peso permitido (500 kb)", "error");
	  }else { //ok
	    tama_perm = true; 
	    var archivo = $('#arch_'+id).val();
	    var col="";
        
	    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
	    permitida = false;
	    if($('#arch_'+id)[0].files.length > 0) {
	      for (var i = 0; i < extensiones_permitidas.length; i++) {
	         if (extensiones_permitidas[i] == extension) {
	         permitida = true;
	         break;
	         }
	      }  
	      if(permitida==true && tama_perm==true){
	        //console.log("tamaño permitido");
	          var inputFileImage = document.getElementById('arch_'+id);
	          var file = inputFileImage.files[0];
	          var data = new FormData();
	          data.append('foto',file);
	          data.append('variable',id);
              if($('#archt_'+id).is(':checked')){
                data.append('variablet','on');
              }else{
              	data.append('variablet','');
              }
	          data.append('id_beneficiario',$('.id_benef_m').text());
	          data.append('id_Beneficiario_transaccion',$('.id_trans_bene').text());
	          data.append('tipo_persona',$('.tipo_persona').text());
	          data.append('id',id_docs);
	          $.ajax({
	              url:base_url+'Clientes_cliente/cargafiles_beneficiario',
	              type:'POST',
	              contentType:false,
	              data:data,
	              processData:false,
	              cache:false,
	              success: function(data) {
	                swal("Éxito", "Imagen cargada Correctamente", "success");
	                //  swal("Error!", "No Se encuentra el archivo", "error");
                    id_docs = data;
	              }
	          });
	        }else if(permitida==false){
	            swal("Error!", "Tipo de archivo no permitido", "error");
	        } 

	    }else{
	      swal("Error!", "No se a selecionado un archivo", "error");
	    } 
	    
	  }
	 */ 
}

function guadar_checkec(id){
	var data = new FormData();
	data.append('variable',id);
	if($('#archt_'+id).is(':checked')){
	    data.append('variablet','on');
	}else{
	  	data.append('variablet','');
	}
	data.append('id_beneficiario',$('.id_benef_m').text());
	data.append('id_Beneficiario_transaccion',$('.id_trans_bene').text());
	data.append('tipo_persona',$('.tipo_persona').text());
	data.append('id',id_docs);
	$.ajax({
        url:base_url+'Clientes_cliente/beneficiario_checked_f',
        type:'POST',
        contentType:false,
        data:data,
        processData:false,
        cache:false,
        success: function(data){
          id_docs = data;
      }
	});
}

function guardar_documentos(){
    var idact=$('.id_actividad').text();
    if(idact==1)anexo="anexo1";if(idact==2)anexo="anexo2a";if(idact==3)anexo="anexo2b";if(idact==4)anexo="anexo2c";if(idact==5)anexo="anexo3";
    if(idact==6)anexo="anexo4";if(idact==7)anexo="anexo5a";if(idact==8)anexo="anexo5b";if(idact==9)anexo="anexo6";if(idact==10)anexo="anexo7";
    if(idact==11)anexo="anexo8";if(idact==12)anexo="anexo9";if(idact==13)anexo="anexo10"; if(idact==14)anexo="anexo11";if(idact==15)anexo="anexo12a";
    if(idact==16)anexo="anexo12b";if(idact==17)anexo="anexo13";if(idact==18)anexo="anexo14";if(idact==19)anexo="anexo15";if(idact==20)anexo="anexo16";          //id de transaccion anexo
    window.location.href = base_url+"index.php/Transaccion/"+anexo+"/0/"+$('.idcientec').text()+"/"+$('.id_actividad').text()+"/"+$('.idperfilamiento_cliente').text()+"/"+0+"/"+$('.id_trans_bene').text();  
}

function tamano_ayuda(){
  $('#modal_carga_ayuda').modal();
}