var base_url = $('#base_url').val();
var idopera = $('.idopera').text();

var img=""; var id_docs = 0;
var imgdet=""; var typePDF=""; var imgprev="";
var total_por_cap=1;
$(function () {
  $("input[class*='form-check-input']").attr('disabled',true);

  verificaFotos();
  $('#form_doc_sin').validate({
    submitHandler: function (form) {
      return false; // required to block normal submit for ajax
     },
    errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    }
  });

  $(".kv-file-remove").on("click",function(){
    $("#sin_bene").fileinput("refresh");
  });

  $('#validado').on('click', function() {
    if($('#validado').is(':checked')){
      val = 1;
      title = "¿Desea Validar estos documentos?";
    }else{
      val = 0;
      title = "¿Desea marcar como no validos estos documentos?";
    }
    swal({
        title: title,
        text: "Se cambiará el estatus",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type:'POST',
          url: base_url+'Clientes_cliente/validar_sin_bene',
          data:{ id: $("#id").val(),val:val },
          success:function(data){
              swal("Éxito", "Cambio realizado correctamente", "success");
              /*setTimeout(function(){ 
                location.href= base_url+'Clientes_cliente';
              }, 1500);*/
              //setTimeout(function () {  history.back() }, 1500);
          }
        }); //cierra ajax
      }else{
        $('input[id*="validado"]').prop("checked", true).trigger("change");
      }
    });
    
  });

});

var tama_perm = false;
$('.img').change( function() {
  filezise = this.files[0].size;                     
  if(filezise > 2548000) { // 512000 bytes = 500 Kb
    //console.log($('.img')[0].files[0].size);
    $(this).val('');
    swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
  }else { //ok
    tama_perm = true; 
    var archivo = this.value;
    var name = this.id;
    var col="";
    //console.log("archivo: "+archivo);
    //console.log("name: "+this.id);
    if(name=="sin_bene") col="acuse_sin_doc";

    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
    permitida = false;
    if($('#'+name)[0].files.length > 0) {
      for (var i = 0; i < extensiones_permitidas.length; i++) {
        if (extensiones_permitidas[i] == extension) {
          permitida = true;
          break;
        }
      }  
      if(permitida==true && tama_perm==true){
        //console.log("tamaño permitido");
        var inputFileImage = document.getElementById(name);
        var file = inputFileImage.files[0];
        var data = new FormData();
        //id_docs = $("#id").val();
        data.append('foto',file);
        data.append('iddoc',id_docs);
        $.ajax({
          url:base_url+'Clientes_c_beneficiario/carga_archivo_sin/'+$("#id").val()+"/"+$(".idclientec").text()+'/'+$(".idperfilamiento").text()+"/"+col+"/"+idopera,
          type:'POST',
          contentType:false,
          data:data,
          processData:false,
          cache:false,
          success: function(data) {
            var array = $.parseJSON(data);
            //console.log(array.ok);
            if (array.ok=true) {
              id_docs = array.id;
              total_por_cap--;
              verificaVerValid();
              //console.log("id_docs: "+id_docs);
              swal("Éxito", "Imagen cargada Correctamente", "success");
              $("#id").val(array.id);
            }else{
              swal("Error!", "No Se encuentra el archivo", "error");
            }
            if(array.id==0) {
              swal("Éxito", "Imagen cargada Correctamente", "success");
              $("#id").val(array.id);
              setTimeout(function(){ 
                location.href= base_url+'Clientes_cliente';
              }, 1500);
            }

          },
          error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
          }
        });
      }else if(permitida==false){
        swal("Error!", "Tipo de archivo no permitido", "error");
      } 
    }else{
      swal("Error!", "No se a selecionado un archivo", "error");
    } 
  }
});

function verificaFotos(){
  $.ajax({
     type: "POST",
     url: base_url+"Clientes_c_beneficiario/verificaAcuseBeneSin",
     data: { "id": $("#id").val(),"idopera": $(".idopera").text() },
     async: false,
     success: function (result) {
      //console.log("result: "+result);
      //var data= JSON.parse(result);
      var array = $.parseJSON(result);
      //console.log("array length: "+array.length);
      //$.each(data, function (key, dato){
        //console.log("acuse_sin_doc: "+array.acuse_sin_doc);
        if(array.acuse_sin_doc!="" && array.acuse_sin_doc!=undefined){
          total_por_cap=0;
          $("input[id*='archt_1']").attr('checked',true);
          //console.log("nombre de foto cc: "+dato.formato_cc);
          img = array.acuse_sin_doc;
          //console.log("img: "+img);
          iddoc = $("#id").val();
          imgprev = base_url+"uploads/doc_sin_bene/"+img;
          if(img!=undefined){
            imgdet = {type:"image", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddoc+"/acuse_sin_doc", caption: img, key:1};
            ext = img.split('.');
            if(ext[1].toLowerCase()!="pdf"){
              imgp = base_url+"uploads/doc_sin_bene/"+img; 
              typePDF = "false";  
            }else{
              imgp = base_url+"uploads/doc_sin_bene/"+img;
              imgdet = {type:"pdf", url: base_url+"Clientes_c_beneficiario/file_delete/"+iddoc+"/acuse_sin_doc", caption: img, key:1};
              typePDF = "true";
            }
          }
        }if(array.validado=="1"){
          $("#validado").prop("checked", true);
        }
      //});
      if(total_por_cap==0){
        $(".valid").show();
        $("#validado").prop("disabled", false);
      }
      if(total_por_cap>0){
        $(".valid").hide();
        $("#validado").prop("disabled", true);
      }
    }
  });
  $("#sin_bene").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: ""+imgprev+"",
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif","pdf","jpeg"],
    initialPreview: [
      ''+imgprev+'',    
    ],
    initialPreviewAsData: typePDF,
    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
    initialPreviewConfig: [
      imgdet
    ]
  });

}

function verificaVerValid(){
  if(total_por_cap==0){
    $(".valid").show();
    $("#validado").prop("disabled", false);
  }
  if(total_por_cap>0){
    $(".valid").hide();
    $("#validado").prop("disabled", true);
  }
}

function guarda_acuse() {
  swal("Éxito", "Guardado Correctamente", "success");
  //setTimeout(function () {  history.back() }, 1500);
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    setTimeout(function () { window.location.href = base_url+"Operaciones/procesoInicial/"+idopera; }, 1500);
}

function tamano_ayuda(){
  $('#modal_carga_ayuda').modal();
}

function inicio_cliente(){
  window.history.back();
}