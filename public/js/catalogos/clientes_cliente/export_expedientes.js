var base_url = $('#base_url').val();
$(document).ready(function(){
  $(".down_expcli").on("click",function(){
    $("#modal_btdc").modal();
    $(".btn_export").attr("onclick","descargaZipCli()")
    /*window.open(base_url+'Clientes_cliente/exportarBitacoraDocs/0','_blank'); 

    setTimeout(function(){ 
      deleteZipCli();
    }, 8500); */
  });

  $(".down_expdb").on("click",function(){
    $("#modal_btdc").modal();
    $(".btn_export").attr("onclick","descargaZipDB()")

    /*window.open(base_url+'Clientes_c_beneficiario/exportarBitacoraDocs/0/0','_blank');
    setTimeout(function(){ 
      deleteZipDB();
    }, 8500); */
  });

  var idc= $('#idcliente').val();
  var idp= 0;
  $('.select_beneficiario').html("");
  $.ajax({
    type: "POST",
    url: base_url+"Clientes_cliente/get_clientes_select",
    data:{idc:idc,idp:idp},
    success: function (data){
      $('.select_clientes').html(data);
      $('#id_cliente_select').select2({ width: '100%', sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)) });
    }
  });

});

function descargaZipCli(){
  $("#modal_btdc").modal("hide");
  var id_cli = $("#id_cliente_select option:selected").val();
  var anio = $("#anio_opera option:selected").val();
  console.log("descarga zip cliente");
  window.open(base_url+'Clientes_cliente/exportarBitacoraDocs/0/'+id_cli+'/'+anio,'_blank'); 
  setTimeout(function(){ 
    deleteZipCli();
  }, 8500); 
}

function descargaZipDB() {
  $("#modal_btdc").modal("hide");
  var id_cli = $("#id_cliente_select option:selected").val();
  var anio = $("#anio_opera option:selected").val();
  console.log("descarga zip db");
  window.open(base_url+'Clientes_c_beneficiario/exportarBitacoraDocs/0/0/'+id_cli+'/'+anio,'_blank'); 
  setTimeout(function(){ 
    deleteZipCli();
  }, 8500);
}

function get_beneficiarios(){
  var dato_cli = $("#id_cliente_select option:selected").val();
  id_cli = dato_cli.split("-");
  id_cliente_tipo = id_cli[0];
  idperfilamientop = id_cli[1];
  idtipo_cliente = id_cli[2];
  id_union = id_cli[3];

  $('.select_clientes').html("");
  $.ajax({
    type: "POST",
    url: base_url+"Clientes_cliente/get_beneficiarios_select",
    data:{ idc:id_cliente_tipo, idp:idperfilamientop, id_union:id_union, id_opera:0 },
    success: function (data){
      //console.log(data);
      $('.select_beneficiario').html(data);
      if($('#beneficiario_ option').length<=1){
        $("#a_benes_funt").hide();
      }
    }
  });
}

function deleteZipCli(){
  $.ajax({
    type: "POST",
    url: base_url+"Clientes_cliente/deleteZipCli",
    async: false, 
    success: function (result) {
      //console.log('eliminado');
    }
  });
}

function deleteZipDB(){
  $.ajax({
    type: "POST",
    url: base_url+"Clientes_c_beneficiario/deleteZip",
    async: false, 
    success: function (result) {
      //console.log('eliminado');
    }
  });
}
