$(function () {

    $('#form_cliente').validate({
        submitHandler: function (form) {
            if ($("#table_uniones tbody > tr").length>=2) {
                var DATA  = [];
                var TABLA   = $("#table_uniones tbody > tr");
                TABLA.each(function(){         
                    item = {};
                    item ["id"] = $(this).find("input[id*='id_union']").val();
                    item ["id_union_cli"] = $(this).find("input[id*='id_union_cli']").val();
                    item ["ncli"] = $(this).find("input[id*='ncli']").val();
                    DATA.push(item);
                });
                INFO  = new FormData();
                aInfo   = JSON.stringify(DATA);
                INFO.append('data', aInfo);
                $.ajax({
                    data: INFO,
                    type: 'POST',
                    url : base_url+'Clientes_cliente/add_cli_union',
                    processData: false, 
                    contentType: false,
                    async: false,
                    statusCode:{
                        404: function(data){
                            //toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            //toastr.error('Error', '500');
                        }
                    },
                    success: function(data){
                        swal("Éxito!", "Clientes unidos correctamente", "success");
                        setTimeout(function() { 
                            if($("#desdeopera").val()==0)
                                window.location.href = base_url+"Clientes_cliente";
                            else
                                window.location.href = base_url+"Operaciones";
                        }, 1500);
                    }
                }); 
            }else{
                swal("Error!", "Se deben agregar minimo 2 clientes a la lista de mancomunado", "warning");
            }
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });

   
});

$('#add_cli').click(function(event) {
    var id_pc=$('#cliente option:selected').val();
    var cliente=$('#cliente option:selected').text();
    var id_union=$('#id_union').val();
    if (id_pc!='') {
        var repetido=0;
        var TABLA   = $("#table_uniones tbody > tr");
        TABLA.each(function(){         
            var ncli  = $(this).find("input[id*='ncli']").val();
            if (ncli==id_pc) {
                repetido=1;
            }
            
        });
        if (repetido==0) {
            $('#cliente').val(null).trigger('change');
            var cli_union = '<tr class="clienteselectd_'+id_pc+'">';
              cli_union += '<td class="colcli1">';
                cli_union += '<input type="hidden" name="id_union" id="id_union" value="'+id_union+'">';
                cli_union += '<input type="hidden" name="id_union_cli" id="id_union_cli" value="0">';
                cli_union += '<input type="hidden" name="ncli" id="ncli" value="'+id_pc+'">';
                cli_union += cliente;
              cli_union += '</td>';
               cli_union += '<td class="colpro4">';
                cli_union += "<button type='button' id='"+id_pc+"' class='btn btn_ayuda' onclick='deleteC(this.id)'><i class='fa fa-minus'></i></button>";
              cli_union += '</td>';
            cli_union += '</tr>';
            $('#tbody_uniones').append(cli_union);

        }else{
            //toastr.error('Error!', 'Materia ya agregada, intente con otra');
            swal("Error!", "Cliente ya agregado, intente con otro", "warning");
            $('#cliente').val(null).trigger('change');
        }
        
    }                   
});

function deleteC(id){
    //console.log("id: "+id);
    swal({
        title: "¿Desea eliminar este cliente de la unión?",
        text: "Se eliminará del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: true
    }, function () {
        $('.clienteselectd_'+id+'').remove();
    });
}

function deleteCliente(id){
    swal({
        title: "¿Desea eliminar este cliente de la unión?",
        text: "Se eliminará del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: true
    }, function () {
        $.ajax({
                 type: "POST",
                 url: base_url+"Clientes_cliente/deleteCliUnion/"+id,
                 async: false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function() {
                        toastr.error('Error', '500');
                    }
                },
                success: function(data) {
                    $('.clienteselectd_'+id).remove();
                }
            });
    });
}
