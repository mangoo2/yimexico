var base_url = $('#base_url').val();
var id_union = $('#id_union').val();
var tabla;
$(document).ready(function($) {
    loadTable();

    $("#agregarBene").on('click',function(){
        $("#modal_tipo_bene").modal();
    }); 

});
function inicio_cliente(){
  location.href= base_url+'Clientes_cliente';
  //history.back();
}
function editar(id,tabla){
    if(tabla==1)
        window.location.href = base_url+"Clientes_c_beneficiario/addBene/"+id+"/0/0/0/0/0/"+id_union;
    if(tabla==2)
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/"+id+"/0/0/0/0/0/"+id_union;
    if(tabla==3)
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/"+id+"/0/0/0/0/0/"+id_union;
}

function documentos(id,tipo){
    window.location.href = base_url+"Clientes_c_beneficiario/docs_beneficiario/"+id+"/"+tipo+"/0/0/0";
}

function aceptarAdd(){
    if($("#fisica").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBene/0/0/0/0/0/0/"+id_union;
    if($("#moral").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/0/0/0/0/0/0/"+id_union;
    if($("#fideicomiso").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/0/0/0/0/0/0/"+id_union;
}

function loadTable(){
  $.ajax({
    url: base_url+"Clientes_c_beneficiario/records_benes_union",
    type: 'POST',
    data: {  id_union:id_union  },
    success: function(x){
      $("#table_beneficiario").html(x);
      //console.log(x);
      tabla = $("#table_beneficiario").DataTable({
          destroy:true
      });
     },
    error: function(jqXHR,estado,error){
      $("#table_beneficiario").html('Hubo un error: '+estado+' '+error);
    }
  });
}

function eliminar(id,tablaBen,cont,id_union,id_auni,tipo_bene){
    title = "¿Desea eliminar este registro?";
    swal({
          title: title,
          text: "Se eliminará el Beneficiario del listado!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Clientes_c_beneficiario/eliminarBene',
            data:{ id:id,
                   tablaBen:tablaBen,
                   id_union:id_union,
                   id_auni:id_auni,
                   tipo_bene:tipo_bene},
            success:function(data){
                //tabla.ajax.reload();
                $('.rowt_'+cont).remove();
                swal("Éxito", "Beneficiario eliminado correctamente", "success");
            }
          }); //cierra ajax
       }
    });
}
