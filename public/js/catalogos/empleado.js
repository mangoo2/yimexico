var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
    $(".select2").select2();
    $("#userSession").hide();
    getpais();
    //====================== Form
        var form_register = $('#form_empleado');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                idpuesto:{
                  required: true
                },
                id:{
                  required: true
                },
                nombre:{
                  required: true
                },  
                Email:{
                  required: true
                }, 
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        $('.guardarregistro').click(function(event) {
            var $valid = $("#form_empleado").valid();
            if($valid) {
                var pass1=$('#contrasena').val();
                var pass2=$('#contrasenav').val();
                if(pass1!=pass2){
                  $('.text_validar').html('Por favor, introduzca el mismo valor de nuevo.');
                }else{
                    var verificauser_num=$('#verificar_user_num').val();
                    var n_s=parseFloat(verificauser_num);
                    if(n_s==1){
                       $('.user_validar').html('El usuario ya existe.');
                    }else{
                        var datos = form_register.serialize();
                        $.ajax({
                            type:'POST',
                            url: base_url+'Personal/registro',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){
                                var array = $.parseJSON(data);
                                if (array.status==1) {
                                    swal("Éxito", "Guardado Correctamente", "success");
                                }else if (array.status==2) {
                                    swal("Éxito", "Actualizado Correctamente", "success");
                                }
                                setTimeout(function(){ location.href= base_url+'Personal';
                                }, 2000);
                                // Guardar usuario
                                var idpro=array.id;
                                var datosuser = form_register.serialize()+'&idpersona='+idpro;
                                $.ajax({
                                    type:'POST',
                                    url: base_url+'Personal/registrouser',
                                    data: datosuser,
                                    statusCode:{
                                        404: function(data){
                                            swal("Error!", "No Se encuentra el archivo", "error");
                                        },
                                        500: function(){
                                            swal("Error!", "500", "error");
                                        }
                                    },
                                    success:function(data){           
                                    }
                                });
                            }
                        });
        
                    }
                }
            }
        });
//=============================Data table ===================================//
    load();
    setTimeout(function() { 
        if ($('#Usuario').data('usuario')=='') {
            $('#Usuario').val('');
            $('#contrasena').val('')
        }
    }, 1000);
    
});
function load(){
  //table_tipo.destroy();
  table=$("#table").DataTable({
    "bProcessing": true,
    "serverSide": true,
    "ajax": {
       "url": base_url+"Personal/getlistregistro",
       type: "post",
       "data": function(d){
        // d.bodegaId = $('#bodegaId option:selected').val()
        },
        error: function(){
           $("#table").css("display","none");
        }
    },
    "columns": [
        {"data": "id"},
        {"data": "nombre"},
        {"data": "telefono"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var cm = "'";
            var html='<div class="btn-group-vertical" role="group" aria-label="Basic example">\
                          <div class="btn-group">\
                            <button type="button" class="btn btn-outline-danger btn-icon-text dropdown-toggle" data-toggle="dropdown">Acciones</button>\
                            <div class="dropdown-menu">\
                              <a class="dropdown-item" style="cursor: pointer;" ><i class="fa fa-clock-o"></i> Ver listado de actividad</a>\
                              <a class="dropdown-item" href="'+base_url+'Personal/add/'+row.personalId+'"><i class="fa fa-edit"></i> Editar</a>\
                              <a class="dropdown-item" style="cursor: pointer;" onclick="modal_eliminar('+row.personalId+','+cm+row.nombre+cm+')"><i class="fa fa-trash-o"></i> Cancelar</a>\
                            </div>\
                          </div>\
                      </div>';        
            return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}
function modal_eliminar(id,texto) {
    $('#password').val('');
    $('#modaleliminar').modal();
    $('#titulo_texto').html(texto);
    $('#personalId').val(id);
}
function eliminar() {
    $.ajax({
        type:'POST',
        url: base_url+'Personal/verificapass',
        data:{pass:$('#password').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                $('#modaleliminar').modal('hide');
                swal("Éxito", "Eliminado Correctamente", "success");
                setTimeout(function() { 
                    table.ajax.reload(); 
                }, 1000);
                $.ajax({
                    type:'POST',
                    url: base_url+'Personal/updateregistro',
                    data:{id:$('#personalId').val()},
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                    }
                });     
            }else{
                swal("Error!", "Contraseña Incorrecta", "error");
            }
        }
    }); 
}

$("#type").change(function(){
    console.log("cambio");
    console.log($("#type").val());
    if ($("#type").val()=="1"){
        $("#userSession").hide();
    }else {
        $("#userSession").show();
    }
});

function modal_pass() {
    $('#modalpass').modal();
    $('#pass').val('');
}
function generarpass() {
    var longitud=10;
    var cart="abcdefghijklmnopqrstuvwxyzABCDEFJHIJKLMNOPQRCTUVWXYZ2346789";
    var pass="";
    for(i=0; i<longitud; i++) pass += cart.charAt(Math.floor(Math.random()*cart.length));
    $('#pass').val(pass);    
}
function addpass() {
    var pass=$('#pass').val();   
    $('#contrasena').val(pass);
    $('#contrasenav').val(pass);
    var pass1=$('#contrasena').val();
    var pass2=$('#contrasenav').val();
    if(pass1!=pass2){
      $('.text_validar').html('Por favor, introduzca el mismo valor de nuevo.');
    }else{
      $('.text_validar').html('');
    }
}
function verificarpass(){
    var pass1=$('#contrasena').val();
    var pass2=$('#contrasenav').val();
    if(pass1!=pass2){
      $('.text_validar').html('Por favor, introduzca el mismo valor de nuevo.');
    }else{
      $('.text_validar').html('');
    }
}
function verificauser() {
    $.ajax({
        type:'POST',
        url: base_url+'Personal/verificauser',
        data:{user:$('#Usuario').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                $('.user_validar').html('El usuario ya existe.');
                $('#verificar_user_num').val(1);
            }else{
                $('.user_validar').html('');
                $('#verificar_user_num').val(0);
            }
        }
    }); 
}
function modalpuesto() {
    $('#modalpuesto').modal();
    $('#puesto').val('');
}
function nuevopuesto() {
    $.ajax({
        type:'POST',
        url: base_url+'Personal/registropuesto',
        data:{puesto:$('#puesto').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Guardado Correctamente", "success");
            $('#idpuesto').html(data);
        }
    });    
}
function getpais(){
    $('.idpais').select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Clientes/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}