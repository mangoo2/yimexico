var base_url = $('#base_url').val();
var Clon=1;
var arr=[];
var lim_efec_band=false;

var anio = $("#anio").val();
var band_umbral=false; var band_divisas=false;
var valor_divi;
var addtp=0; var addtp2=0;
var addtpD=0; var addtp2D=0;
var valor_uma=0; var valor_uma2=0;
var valor_umaD=0; var valor_umaD2=0;
  
var addtpC=0; var addtp2C=0;
var addtpDC=0; var addtp2DC=0;
var valor_umaC=0; var valor_umaC2=0;
var valor_umaDC=0; var valor_umaDC2=0;
var umas_anexo=0;
var j=0; varj_aux=0;
var suma_efect=0;
var validar_xml;
var band_limite_config=false;
var cont_pos=0;
var pos_ant=0;

$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
	ComboAnio();
  //tipo_modeda(1);
  if($('#mes_aux').val()==''){
    $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
  }else{
    $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
  }
  $('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");

  var aux_pga=$('#aux_pga').val();
  if(aux_pga==1){
    var id_ax1=$('#id_ax1').val();
    tabla_liquidacion_anexo1(id_ax1,$("#aviso").val());
    setTimeout(function () { 
      InitialDelete(1);
      $("#form-principal").empty();
    }, 1500);
  }else{
    agregar_liqui();
  } 
  confirmarPagosMonto();
  //verificarConfigLimite();
  $("select[name*='instrumento_monetario']").on("change", function(){
    //console.log("cambia instrumento_monetario");
    confirmarPagosMonto();
  });
  $("select[name*='moneda']").on("change", function(){
    console.log("cambia moneda");
    confirmarPagosMonto();
  });
  $("select[name*='monedale']").on("change", function(){
    confirmarPagosMonto();
  });
  $("input[name*='monto_operacion']").on("change", function(){
    confirmarPagosMonto();
  });
  $("input[name*='valor_bien']").on("change", function(){
    confirmarPagosMonto();
  });

  $("#fecha_operacion").on("change",function(){
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });


  $("#monto_opera").val(convertMoneda($("#monto_opera").val()));
  verificarConfigLimite();
  setTimeout(function () { 
    confirmarPagosMonto();
  }, 2000);

  $("#agregar_liquida").on("click",function(){
    //console.log("btn agregar_liquida");
    //console.log("Clon: "+Clon);
    fin = $(document).height();
    window.scroll({
      top: fin-1500,
      left: 100,
      behavior: 'smooth'
    });
    $('#form-'+Clon+'').before("<h3>Nueva Liquidación</h3>");
  });

  if(status==0 && $("#aviso").val()>0)
    verificaFechas();


});

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}
function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function Delete(T){
  var aviso_tipo=$("#aviso").val();
  ELE=$("#form-"+T);
  console.log(T);
  console.log(ELE);
  id=ELE[0][13].value;
  if(id!=""){
    SetDelete(id,aviso_tipo);
  }
  $("#cln-"+T).remove();
  if (Clon>T){
    console.log("Clon es mayor");
    $(".formita").each(function(){
      Eleval=$(this);
      console.log($("cln-"+T));
      console.log(Eleval[0].id);
      iden=Eleval[0].id;
      num=iden.split("-");
      if (num[1]>T){
        N=num[1];
        Eleval.attr('id',"form-"+(N-1));
        $("#cln-"+N).attr('id',"cln-"+(N-1));
        $("#InfoInmueble-"+N).attr('id',"InfoInmueble-"+(N-1));
        $("#TipoOtro-"+N).attr('id',"TipoOtro-"+(N-1));
        $("#TipoOtro-"+N).attr('id',"TipoOtro-"+(N-1));
        $("#But-"+N).attr('onclick',"removerliquidacion("+(N-1)+")");
      }
    });
  }
  Clon=Clon-1;
  SumElements();
  swal("Éxito", "Pago eliminado correctamente", "success");
  confirmarPagosMonto();
}

function InitialDelete(T){
  var aviso_tipo=$("#aviso").val();
  ELE=$("#form-"+T);
  console.log(T);
  console.log(ELE);
  id=ELE[0][14].value;
  $("#cln-"+T).remove();
  if (Clon>T){
    console.log("Clon es mayor");
    $(".formita").each(function(){
      Eleval=$(this);
      console.log($("cln-"+T));
      console.log(Eleval[0].id);
      iden=Eleval[0].id;
      num=iden.split("-");
      if (num[1]>T){
        N=num[1];
        Eleval.attr('id',"form-"+(N-1));
        $("#cln-"+N).attr('id',"cln-"+(N-1));
        $("#InfoInmueble-"+N).attr('id',"InfoInmueble-"+(N-1));
        $("#TipoOtro-"+N).attr('id',"TipoOtro-"+(N-1));
        $("#TipoOtro-"+N).attr('id',"TipoOtro-"+(N-1));
        $("#But-"+N).attr('onclick',"removerliquidacion("+(N-1)+")");
      }
    });
  }
  Clon=Clon-1;
}

function Clonar(info){
  Clon=Clon+1;
  clone = $('#form-1').clone();
  p1i='<div id="cln-'+Clon+'"><form id="form-'+Clon+'" class="formita"><div class="row"><div class="col-md-12 form-group"><div class="form-check form-check-success">\
  <label class="form-check-label">';
  p1f='Liquidación numeraria<i class="input-helper"></i></label></div></div>';
  p2i='<div class="col-md-3 form-group"><label>Fecha de pago</label>';
  p2f='</div>';
  p3i='<div class="col-md-6 form-group"><label>Instrumento monetario con el que se realizó la operación o acto:</label>';
  p3f='</div>';
  p4i='<div class="col-md-3 form-group"><label>Tipo de moneda o divisa de la operación o acto</label>';
  p4f='</div>';
  p5i='<div class="col-md-4 form-group"><label>Monto de la operación o acto</label>';
  p5f='</div><div class="col-md-3"><br><h3 class="monto_divisa" style="color: red"></h3></div></div>';
  p6i=' <div class="row"><div class="col-md-12 form-group"><div class="form-check form-check-success">\
  <label class="form-check-label">';
  p6f='Liquidación en especie<i class="input-helper"></i></label></div></div>';
  p7i='<div class="col-md-4 form-group"><label>Valor del bien de liquidación:</label>';
  p7f='</div>';
  p8i=' <div class="col-md-4 form-group"><label>Tipo de moneda o divisa en la que esta expresado el valor del bien de liquidación:</label>';
  p8f='</div>';
  p9i=' <div class="col-md-4 form-group"><label>Tipo de bien de la liquidación</label>';
  p9f='</div></div>';
  p10i='<div id="InfoInmueble-'+Clon+'" style="display: none"><div class="row"><div class="col-md-12"> <label>Datos del inmueble</label>\
  </div><div class="col-md-4 form-group"><label>Tipo de inmueble</label>';
  p10f='</div>';
  p11i='<div class="col-md-4 form-group"><label>Código postal de la ubicación del inmueble:</label>'
  p11f='</div>';
  p12i='<div class="col-md-4 form-group"> <label>Folio real del inmueble o antecedentes registrales:</label>';
  p12f='</div></div></div>';
  p13i='<div id="TipoOtro-'+Clon+'" style="display: none"><div class="row"><div class="col-md-12 form-group"><label>Descripción</label>';
  p13f='</div></div></div>';
  p14c='<div class="row">\
          <div class="col-md-10"></div>\
          <div class="col-md-2">\
            <button type="button" id="But-'+Clon+'" class="btn gradient_nepal2" style="color:white" onclick="removerliquidacion('+Clon+')"><i class="fa fa-trash-o"></i> Quitar liquidación</button>\
  </div></div><hr class="subsubtitle">';
  //fin bases
  if (info==0){
    p1m=clone[0][0].outerHTML;
    p2m=clone[0][1].outerHTML;
    p3m=clone[0][2].outerHTML;
    p4m=clone[0][3].outerHTML;
    p5m=clone[0][4].outerHTML;
    p6m=clone[0][5].outerHTML;
    p7m=clone[0][6].outerHTML; 
    p8m=clone[0][7].outerHTML;
    p9m=clone[0][8].outerHTML;
    p10m=clone[0][9].outerHTML;
    p11m=clone[0][10].outerHTML;
    p12m=clone[0][11].outerHTML;
    p13m=clone[0][12].outerHTML;
    p14f=clone[0][13].outerHTML;
    p15f=clone[0][14].outerHTML;
    console.log(clone[0][14]);

  }else{
    p1m=clone[0][0];
    p2m=clone[0][1];
    p3m=clone[0][2];
    p4m=clone[0][3];
    p5m=clone[0][4];
    p6m=clone[0][5];
    p7m=clone[0][6];
    p8m=clone[0][7];
    p9m=clone[0][8];
    p10m=clone[0][9];
    p10m=clone[0][9];
    p11m=clone[0][10];
    p12m=clone[0][11]
    p13m=clone[0][12];
    p14f=clone[0][13];
    p15f=clone[0][14];

    if(info.tipo_liquidacion==1){
      console.log("num");
      p1m.setAttribute('checked',true);
      p2m.setAttribute('value',info.fecha_pago);
      vasel=(info.instrumento_monetario-1);
      p3m[vasel].setAttribute('selected',true);
      console.log("info.moneda: "+info.moneda);
      //vase=(info.moneda-1);
      vase=(info.moneda);
      console.log("vase: "+vase);
      p4m[vase].setAttribute('selected',true);
      p5m.setAttribute('value',info.monto_operacion);
      p15f.setAttribute('value',info.id);
    }else{
    }

    if(info.tipo_liquidacion==2){
      p6m.setAttribute('checked',true);
      p7m.setAttribute('value',info.valor_bien);
      vase8=(info.moneda-1);
      p8m[vase8].setAttribute('selected',true); 
      if (info.inmueble==1){
        p10i='<div id="InfoInmueble-'+Clon+'" style="display: block"><div class="row"><div class="col-md-12"> <label>Datos del inmueble</label>\
    </div><div class="col-md-4 form-group"><label>Tipo de inmueble</label>';
      vase9=0;
      p9m[vase9].setAttribute('selected',true);
      }else if (info.otro==1){
        p13i='<div id="TipoOtro-'+Clon+'" style="display: block"><div class="row"><div class="col-md-12 form-group"><label>Descripción</label>';
        vase9=8;
        p9m[vase9].setAttribute('selected',true);
      }else{
        vase9=(info.bien_liquidacion-1);
        p9m[vase9].setAttribute('selected',true);
      }
      if(info.tipo_inmueble==99){
        vase10=18;
        p10m[vase10].setAttribute('selected',true);
      }else {
        vase10=(info.tipo_inmueble-1);
        p10m[vase10].setAttribute('selected',true);
      }
      p11m.setAttribute('value',info.codigo_postal);
      p12m.setAttribute('value',info.folio_real);
      p13m.setAttribute('value',info.descripcion_bien_liquidacion);
      p15f.setAttribute('value',info.id);
    }else{
    }
    p14f.setAttribute('value',info.id);
    p1m=p1m.outerHTML;
    p2m=p2m.outerHTML;
    p3m=p3m.outerHTML;
    p4m=p4m.outerHTML;
    p5m=p5m.outerHTML;
    p6m=p6m.outerHTML;
    p7m=p7m.outerHTML; 
    p8m=p8m.outerHTML;
    p9m=p9m.outerHTML;
    p10m=p10m.outerHTML;
    p11m=p11m.outerHTML;
    p12m=p12m.outerHTML;
    p13m=p13m.outerHTML;
    p14f=p14f.outerHTML;
    p15f=p15f.outerHTML;
    console.log(p15f);

  } 
  npf=p1i+p1m+p1f+p2i+p2m+p2f+p3i+p3m+p3f+p4i+p4m+p4f+p5i+p5m+p5f+p6i+p6m+p6f+p7i+p7m+p7f+p8i+p8m+p8f+p9i+p9m+p9f+p10i+p10m+p10f+p11i+p11m+p11f+p12i+p12m+p12f+p13i+p13m+p13f+p14c+p15f+"</form></div>";
  $("#cloadas").append(npf);
  if(info==0){
    element=$('#form-'+Clon+'')[0];
    element[1].value="";
    element[2].value="1";
    element[3].value="0";
    element[4].value="$0.00";
    element[5].value="";
    element[7].value="";
    element[8].value="";
    element[9].value="";
    element[10].value="";
    element[11].value="";
    element[12].value="";
    element[13].value="";
    element[14].value="0";
  }
  SumElements();
}
 
function EnabledSelctN(T){
  frm=T.form;
  frm[1].readOnly= false;
  frm[2].disabled= false;
  frm[3].disabled= false;
  frm[4].readOnly= false;

  frm[6].readOnly= true;
  frm[7].disabled= true;
  frm[8].disabled= true;
}

function EnabledSelctE(T){
  frm=T.form;
  frm[1].readOnly= true;
  frm[2].disabled= true;
  frm[3].disabled= true;
  frm[4].readOnly= true;

  frm[6].readOnly= false;
  frm[7].disabled= false;
  frm[8].disabled= false;

}

function TipoBien(T){
  val=T.value;
  obnf=T.form.id;
  cont=obnf.split("-");
  n=cont[1]
  if (val==1){
    $("#InfoInmueble-"+n).show();
  }else {
    $("#InfoInmueble-"+n).hide();
  }
  if (val==99){
    $("#TipoOtro-"+n).show();
  }else {
    $("#TipoOtro-"+n).hide();
  }
}


function SetNformat(T){
  ValorT=T.value;
  ttle = replaceAll(ValorT, ",", "" );
  val = replaceAll(ttle, "$", "" );
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(val);
  T.value=monto;
  SumElements();
}

function SumElements(){
  /*suma=0;
  efectivo=0;
  $(".sumont").each(function(){
      Eleval=$(this).val();
      if (Eleval!=""){
        ttle = replaceAll(Eleval, ",", "" );
        ttle = replaceAll(ttle, "$", "" );        
        suma=suma+parseFloat(ttle);
      }
    });
  console.log(Clon);
  for (var i = 1; i <= Clon; i++){
    valsel=$("#form-"+i)[0][2].value;
    if (valsel==1){
      valoefe=$("#form-"+i)[0][4].value;
      valorinst=$("#form-"+i)[0][2].value;
      //console.log("valor inst: "+valorinst);
      if (valoefe!=""){
        valsc=replaceAll(valoefe, ",", "" );
        valss=replaceAll(valsc, "$", "" );
        efectivo=efectivo+parseFloat(valss);
      }
    }
  }
  //$("#total_liquida_efect").val(convertMoneda(efectivo));
  ttle = replaceAll($("#monto_opera").val(), ",", "" );
  val = replaceAll(ttle, "$", "" );

  //$("#total_liquida").val(convertMoneda(suma));
  if (suma==val){
    $('.validacion_cantidad').html('');
  }else{
     $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.');
  }
  verificarLimite();
  confirmarPagosMonto();*/
}

function verificarLimite(){

  efectivo = replaceAll($("#total_liquida_efect").val(), ",", "" );
  efectivo = replaceAll(efectivo, "$", "" );
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var array= JSON.parse(result);
      var limite = parseFloat(array[0].limite_efectivo).toFixed(2);
      var na = array[0].na;
      //console.log("limite: "+limite);
      //console.log("na: "+na);

      if(limite>0 && na==0){
        var anio = $("#anio").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Umas/getUmasAnio',
          data: { anio: anio},
          success: function(data2){
            valor_uma=data2;
            valor_uma = parseFloat(valor_uma);
            //console.log("valor de umas año: "+valor_uma);
            var uma_efect_anexo = efectivo / valor_uma;
            if(uma_efect_anexo>limite){
              lim_efec_band=true;
              swal("¡Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
              verificarConfigLimite(1);
              $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
            }else{
              lim_efec_band=false;
              verificarConfigLimite(0);
              $('.limite_efect_msj').html('');
            }
          }
        });
      }//if na
      
      //recorrerFechasUmasPagos($("#id").val(),0);
    }
  });
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function convertMoneda(num){
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
  return monto;
}

function agregar_liqui(){
  //addliquidacion(0,'','','',0,'');   
}

function tabla_liquidacion_anexo1(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_liquidacion_anexo1",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  Clonar(element);
                });
            }else{
                agregar_liqui();
            }   
        }
    });
}
function agregartipo_moneda(id,tipo_moneda){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_moneda",
      data:{t_pago:tipo_moneda},
      success: function (data) {
        $('.tipo_moneda_select_'+id).html(data);
      }
    });
}

function replaceAll( text="0.0", busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

function removerliquidacion(T){
  if (Clon==1){
    swal({
        title: "!ERROR¡",
        text: "No es posible eliminar",
        type: "error",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    });
  }else{
    title = "¿Desea eliminar este registro?";
    swal({
        title: title,
        text: "Se eliminará el pago!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    }, function (isConfirm) {
      if (isConfirm) {
        Delete(T);
      }
    });
  }
}
function SetDelete(id,aviso) {
  $.ajax({
    type:'POST',
    url: base_url+'Transaccion/eliminar_pago_anexo1',
    data:{
      'id':id,
      'aviso':aviso
    },
    success:function(data){
    }
  });
}

function validarF(T){
  fech=T.value;
  var fecha_operacion=$('#fecha_operacion').val();
  if(fecha_operacion==fech){ 
  }else{
    swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
  confirmarPagosMonto();
}

function registrar(){
  var form_register = $('#form_anexo1');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
                referencia:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_anexo1").valid();
        var band_aviso = 0;
        if(valid) {
          
          monto_opera = $("#monto_opera").val();

          if($("#aviso").val()>0){
            name_table = "anexo1_aviso";
          }else{
            name_table = "anexo1";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }

          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo1',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              success:function(data){
                var array = JSON.parse(data);
                var id=array.id; 
                var monto=parseFloat(array.monto); 
                var id_clientec_transac=array.id_clientec_transac; 
                $("input[name*='id_aviso']").val(id);
                SendLiqui(id,id_clientec_transac);
                //Anexo 9 es de $139,442.00
              }  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }  
}

function SendLiqui(id,idct){
  for (var i = 1; i <= Clon;i++){
    info=$("#form-"+i).serialize();
    $.ajax({
        type: 'POST',
        url : base_url+'index.php/Transaccion/inset_pago_anexo1',
        async: false,
        data: info+"&idanexo1="+id+"&aviso="+$("#aviso").val(),
        success: function(data2){
        }
    });
  } 
  //RevSiguiente(id);
  recorrerFechasUmasPagos(id,1,idct);
}

function umasAnio(anio){
  var valor_uma=0;
  $.ajax({
    type: 'POST',
    url : base_url+'Umas/getUmasAnio',
    data: { anio: anio},
    success: function(data2){
      valor_uma=data2;
      var valor_uma=parseFloat(valor_uma);
      console.log("valor uma en function umasAnio: "+valor_uma);
      return valor_uma;
    },error: function(jqXHR) {
      return 0;
    }
  });
  
}

function recorrerFechasUmasPagos(id,conf,idct){
  /* obtener umas - año acuse */
  var id_reg = id;
  var act = new Date();
  var mes_act = act.getMonth();
  var anio_act = act.getFullYear();
  var umas_anexo=0;
  
  //console.log("mes actual: "+mes_act);
  //console.log("anio_act: "+anio_act);

  /*$.ajax({
    type: 'POST',
    url : base_url+'Umas/getUmasAnio', //se debe consultar las umas por año de pago 
    data: { anio: anio},
    success: function(data2){
      valor_uma=data2;
      console.log("valor de umas año: "+valor_uma); */
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      $("#btn_submit").attr("disabled",true);
      //var array = $.parseJSON(result);
      var array= JSON.parse(result);
      var aviso = parseFloat(array[0].aviso).toFixed(2);
      var siempre_avi = array[0].siempre_avi;
      var siempre_iden=array[0].siempre_iden;
      var identifica=array[0].identificacion;

      //var TABLA = $(".card-body > #form-1");
      var TABLA = $("#form-principal > #form-1");              
      TABLA.each(function(){        
        // pesos de Liquidación numeraria 
        //console.log("radio checked numeraria: "+$(this).find("input[type=radio]radio[id*='tln-1']").is(":checked"));
        if($(this).find("select[name*='moneda'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){ //en pesos
          console.log("pesos de Liquidación numeraria ");
          var monto_operacion = $(this).find("input[name*='monto_operacion']").val();
          var fecha_pago = $(this).find("input[name*='fecha_pago']").val();
          if(monto_operacion.indexOf('$') != -1){
            tot_mo = replaceAll(monto_operacion, ",", "" );
            tot_mo = replaceAll(tot_mo, "$", "" );
          }else{
            tot_mo=monto_operacion;
          }
          tot_mo = parseFloat(tot_mo);
          var vstotal1 = tot_mo;
          
          let date = new Date(fecha_pago);
          var mes_pago = date.getMonth();
          mes_pago = parseInt(mes_pago);
          mes_pago = mes_pago +1;
        
          var anio_pago = date.getFullYear();
          /*console.log("mes de pago: "+mes_pago);
          console.log("anio_pago: "+anio_pago);*/
          
          //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
            addtp += Number(vstotal1);
          //}
          
          valor_uma=0;
          //if(addtp>0){
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              data: { anio: fecha_pago},
              async: false,
              success: function(data2){
                valor_uma=data2;
                var valor_uma=parseFloat(valor_uma); 
                console.log("valor uma en function umasAnio: "+valor_uma);
                umas_anexo += addtp/valor_uma;
                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                console.log("valor1 de umas_anexo: "+umas_anexo);
              }
            });
          //}
          
        }
        // pesos de Liquidación en especie 
        if($(this).find("select[id*='monedale'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
          console.log("pesos de Liquidación en especie ");
          var valor_bien = $(this).find("input[name*='valor_bien']").val();
          if(valor_bien.indexOf('$') != -1){
            tot_vb = replaceAll(valor_bien, ",", "" );
            tot_vb = replaceAll(tot_vb, "$", "" );
          }else{
            tot_vb=valor_bien;
          }
          var vstotal2 = tot_vb;
        
          /*let date = new Date(fecha_pago);
          var mes_pago = date.getMonth();
          mes_pago = parseInt(mes_pago);
          mes_pago = mes_pago +1;
        
          var anio_pago = date.getFullYear();
          if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
            addtp2 += Number(vstotal2);
          }*/

          addtp2 += Number(vstotal2);
          valor_uma=0;
          //if(addtp2>0){
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              data: { anio: $("#anio").val()},
              async: false,
              success: function(data2){
                valor_uma=data2;
                var valor_uma=parseFloat(valor_uma); 
                console.log("valor uma en function umasAnio: "+valor_uma);
                umas_anexo += addtp2/valor_uma;
                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                console.log("valor2 de umas_anexo: "+umas_anexo);
              }
            });
          //}
        }
        if($(this).find("select[name*='moneda'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){
          //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
          console.log("moneda != pesos de Liquidación numeraria ");
          var monto_operacion = $(this).find("input[name*='monto_operacion']").val();
          var fecha_pago = $(this).find("input[name*='fecha_pago']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { tipo: $(this).find("select[name*='moneda'] option:selected").val(), fecha:$(this).find("input[name*='fecha_pago']").val()},
            success: function(result_divi){
              valor_divi=result_divi;
              valor_divi = parseFloat(valor_divi);
              if(monto_operacion.indexOf('$') != -1){
                tot_moD = replaceAll(monto_operacion, ",", "" );
                tot_moD = replaceAll(tot_moD, "$", "" );
              }else{
                tot_moD = monto_operacion;
              }
              tot_moD = parseFloat(tot_moD);
              var vstotal1D = tot_moD*valor_divi;
              
              let date = new Date(fecha_pago);
              var mes_pago = date.getMonth();
              mes_pago = parseInt(mes_pago);
              mes_pago = mes_pago +1;
            
              var anio_pago = date.getFullYear();
              /*console.log("mes de pago: "+mes_pago);
              console.log("anio_pago: "+anio_pago);*/
              
              //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                addtpD += Number(vstotal1D);
              //}

              //addtpD += Number(vstotal1D);
              valor_uma=0;
              //if(addtpD>0){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  data: { anio: fecha_pago},
                  async: false,
                  success: function(data2){
                    valor_uma=data2;
                    var valor_uma=parseFloat(valor_uma); 
                    console.log("valor uma en function umasAnio: "+valor_uma);
                    umas_anexo += addtpD/valor_uma;
                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                    console.log("valor3 de umas_anexo: "+umas_anexo);
                  }
                });
              //}
              /*valor_uma = umasAnio($(this).find("input[name*='fecha_pago']").val());
              umas_anexo += addtpD/valor_uma;
              umas_anexo = parseFloat(umas_anexo).toFixed(2);
              console.log("valor3 de umas_anexo: "+umas_anexo);*/
            }
          });
          $(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);s
        }
        if($(this).find("select[id*='monedale'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
          //para sacar cant en pesos por tipo de divisa en liquidacion en especia
          console.log("moneda != pesos de Liquidación en especie ");
          var valor_bien = $(this).find("input[name*='valor_bien']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { tipo: $(this).find("select[id*='monedale'] option:selected").val(), fecha:$("#fecha_operacion").val()},
            success: function(result_divi){
              valor_divi=result_divi;
              valor_divi = parseFloat(valor_divi);
              if(valor_bien.indexOf('$') != -1){
                tot_vbD = replaceAll(valor_bien, ",", "" );
                tot_vbD = replaceAll(tot_vbD, "$", "" );
              }else{
                tot_vbD=valor_bien;
              }
              tot_vbD = parseFloat(tot_vbD);
              var vstotal2D = tot_vbD*valor_divi;
              /*let date = new Date(fecha_pago);
              var mes_pago = date.getMonth();
              mes_pago = parseInt(mes_pago);
              mes_pago = mes_pago +1;
            
              var anio_pago = date.getFullYear();*/
              /*console.log("mes de pago: "+mes_pago);
                console.log("anio_pago: "+anio_pago);*/
              
              /*if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                addtp2D += Number(vstotal2D);
              }*/

              addtp2D += Number(vstotal2D);
              valor_uma=0;
              //if(addtp2D>0){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  data: { anio: $("#anio").val()},
                  async: false,
                  success: function(data2){
                    valor_uma=data2;
                    var valor_uma=parseFloat(valor_uma); 
                    console.log("valor uma en function umasAnio: "+valor_uma);
                    umas_anexo += addtp2D/valor_uma;
                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                    console.log("valor4 de umas_anexo: "+umas_anexo);
                  }
                });
              //}
              /*valor_uma = umasAnio($("#anio").val());
              umas_anexo += addtp2D/valor_uma;
              umas_anexo = parseFloat(umas_anexo).toFixed(2);
              console.log("valor4 de umas_anexo: "+umas_anexo);*/
            }
          });
          $(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
        }
        if(Clon<=1){
          band_divisas=true;
        }
        varj_aux++;
        console.log("varj_aux: "+varj_aux);
      });

      for (j = 1; j <=Clon; j++){
        /*console.log("clon: "+Clon);
        console.log("j: "+j);*/
        var TABLAC = $("#cln-"+j+" > #form-"+j+"");              
        TABLAC.each(function(){        
          // pesos de Liquidación numeraria 
          //console.log("radio checked numeraria: "+$(this).find("input[type=radio]radio[id*='tln-1']").is(":checked"));
          if($(this).find("select[name*='moneda'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){ //en pesos
            console.log("pesos de Liquidación numeraria Clon");
            var monto_operacion = $(this).find("input[name*='monto_operacion']").val();
            fecha_pago = $(this).find("input[name*='fecha_pago']").val();
            if(monto_operacion.indexOf('$') != -1){
              tot_moC = replaceAll(monto_operacion, ",", "" );
              tot_moC = replaceAll(tot_moC, "$", "" );
            }else{
              tot_moC = monto_operacion;
            }
            tot_moC = parseFloat(tot_moC);
            var vstotal1C = tot_moC;
            
            let date = new Date(fecha_pago);
            var mes_pago = date.getMonth();
            mes_pago = parseInt(mes_pago);
            mes_pago = mes_pago +1;
            var anio_pago = date.getFullYear();
          
            //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
              addtpC += Number(vstotal1C);
            //}

            varj_aux++;
            valor_uma=0;
            //if(addtpC>0){
              $.ajax({
                type: 'POST',
                url : base_url+'Umas/getUmasAnio',
                data: { anio: fecha_pago},
                async: false,
                success: function(data2){
                  valor_uma=data2;
                  var valor_uma=parseFloat(valor_uma); 
                  console.log("valor5 uma en function umasAnio: "+valor_uma);
                  umas_anexo += addtpC/valor_uma;
                  //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                  console.log("valor5 de umas_anexo: "+umas_anexo);
                }
              });
            //}
            /*valor_uma = umasAnio($(this).find("input[name*='fecha_pago']").val());
            umas_anexo += addtpC/valor_uma;
            umas_anexo = parseFloat(umas_anexo).toFixed(2);
            console.log("valor5 de umas_anexo: "+umas_anexo);*/
            //console.log("varj_aux: "+varj_aux);
          }
          // pesos de Liquidación en especie 
          if($(this).find("select[id*='monedale'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
            console.log("pesos de Liquidación en especie Clon");
            var valor_bien = $(this).find("input[name*='valor_bien']").val();
            if(valor_bien.indexOf("$") != -1){
              tot_vbC = replaceAll(valor_bien, ",", "" );
              tot_vbC = replaceAll(tot_vbC, "$", "" );
            }else{
              tot_vbC = valor_bien; 
            }
            tot_vbC = parseFloat(tot_vbC);
            var vstotal2C = tot_vbC;
            /*let date = new Date(fecha_pago);
            var mes_pago = date.getMonth();
            mes_pago = parseInt(mes_pago);
            mes_pago = mes_pago +1;
            var anio_pago = date.getFullYear();
            
            if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
              addtp2C += Number(vstotal2C);
            }*/

            addtp2C += Number(vstotal2C);
            varj_aux++;
            valor_uma=0;
            //if(addtp2C>0){
              $.ajax({
                type: 'POST',
                url : base_url+'Umas/getUmasAnio',
                data: { anio: $("#anio").val()},
                async: false,
                success: function(data2){
                  valor_uma=data2;
                  var valor_uma=parseFloat(valor_uma); 
                  console.log("valor6 uma en function umasAnio: "+valor_uma);
                  //umas_anexo = Number(addtp2C);
                  umas_anexo += addtp2C/valor_uma;
                  //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                  console.log("valor6 de umas_anexo: "+umas_anexo);
                }
              });
            //}
            /*valor_uma = umasAnio($("#anio").val());
            umas_anexo += addtp2C/valor_uma;
            umas_anexo = parseFloat(umas_anexo).toFixed(2);
            console.log("valor6 de umas_anexo: "+umas_anexo);*/
            //console.log("varj_aux: "+varj_aux);
          }
          if($(this).find("select[name*='moneda'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){
            //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
            //console.log("moneda != pesos de Liquidación numeraria Clon");
            //console.log("monto operacion: "+$(this).find("input[name*='monto_operacion']").val());
            var monto_operacion = $(this).find("input[name*='monto_operacion']").val();
            var fecha_pago = $(this).find("input[name*='fecha_pago']").val();
            $.ajax({
              type: 'POST',
              url : base_url+'Divisas/getDivisaTipo',
              async: false,
              data: { tipo: $(this).find("select[name*='moneda'] option:selected").val(), fecha:$(this).find("input[name*='fecha_pago']").val()},
              success: function(result_divi){
                valor_divi=result_divi;
                valor_divi = parseFloat(valor_divi);
                console.log("valor_divi: "+valor_divi);
                console.log("monto_operacion: "+monto_operacion);
                if(monto_operacion.indexOf('$') != -1){
                  console.log("monto_operacion contiene $: "+monto_operacion);
                  tot_moDC = replaceAll(monto_operacion, ",", "" );
                  tot_moDC = replaceAll(tot_moDC, "$", "" );
                }else{
                  //console.log("monto_operacion no contiene $: "+monto_operacion);
                  tot_moDC = monto_operacion;
                }
                tot_moDC = parseFloat(tot_moDC);
                var vstotal1DC = tot_moDC*valor_divi;
     
                let date = new Date(fecha_pago);
                var mes_pago = date.getMonth();
                mes_pago = parseInt(mes_pago);
                mes_pago = mes_pago +1;
              
                var anio_pago = date.getFullYear();
                /*console.log("mes de pago: "+mes_pago);
                console.log("anio_pago: "+anio_pago);*/
                
                //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                  addtpDC += Number(vstotal1DC);
                //}

                //addtpDC += Number(vstotal1DC);
                //console.log("valor de addtpDC dentro de if: "+addtpDC);
                band_divisas=true;
                varj_aux++;
                valor_uma=0;
                
                //console.log("fecha_pago7: "+fecha_pago);
                //if(addtpDC>0){
                  $.ajax({
                    type: 'POST',
                    url : base_url+'Umas/getUmasAnio',
                    data: { anio: fecha_pago},
                    async: false,
                    success: function(data2){
                      valor_uma=data2;
                      var valor_uma=parseFloat(valor_uma); 
                      console.log("valor7 uma en function umasAnio: "+valor_uma);
                      //umas_anexo = Number(addtpDC);
                      umas_anexo += addtpDC/valor_uma;
                      //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                      //console.log("valor7 de umas_anexo: "+umas_anexo);
                    }
                  });
                //}
                /*valor_uma = umasAnio($(this).find("input[name*='fecha_pago']").val());
                umas_anexo += addtpDC/valor_uma;
                umas_anexo = parseFloat(umas_anexo).toFixed(2);
                console.log("valor7 de umas_anexo: "+umas_anexo);*/
                //console.log("varj_aux: "+varj_aux);
              }
            });
            $(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
          }
          if($(this).find("select[id*='monedale'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
            //para sacar cant en pesos por tipo de divisa en liquidacion en especia
            console.log("moneda != pesos de Liquidación en especie Clon");
            var valor_bien = $(this).find("input[name*='valor_bien']").val();
            $.ajax({
              type: 'POST',
              url : base_url+'Divisas/getDivisaTipo',
              async: false,
              data: { tipo: $(this).find("select[id*='monedale'] option:selected").val(), fecha:$("#fecha_operacion").val()},
              success: function(result_divi){
                valor_divi=result_divi;
                valor_divi = parseFloat(valor_divi);
                console.log("valor_divi: "+valor_divi);
                console.log("valor_bien: "+valor_bien);
                if(valor_bien.indexOf('$') != -1){
                  tot_vbDC = replaceAll(valor_bien, ",", "" );
                  tot_vbDC = replaceAll(tot_vbDC, "$", "" );
                }else{
                  tot_vbDC = valor_bien;
                }
                tot_vbDC = parseFloat(tot_vbDC);
                var vstotal2DC = tot_vbDC*valor_divi;

                /*let date = new Date(fecha_pago);
                var mes_pago = date.getMonth();
                mes_pago = parseInt(mes_pago);
                mes_pago = mes_pago +1;
                var anio_pago = date.getFullYear();
                if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                  addtp2DC += Number(vstotal2DC);
                }*/
                addtp2DC += Number(vstotal2DC);
                //console.log("valor de addtpDC dentro de if: "+addtp2DC);
                band_divisas=true;
                varj_aux++;
                //console.log("varj_aux: "+varj_aux);
                valor_uma=0;
                //if(addtpDC>0){
                  $.ajax({
                    type: 'POST',
                    url : base_url+'Umas/getUmasAnio',
                    data: { anio: $("#anio").val()},
                    async: false,
                    success: function(data2){
                      valor_uma=data2;
                      var valor_uma=parseFloat(valor_uma); 
                      console.log("valor8 uma en function umasAnio: "+valor_uma);
                      
                      umas_anexo += addtp2DC/valor_uma;
                      //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                      console.log("valor8 de umas_anexo: "+umas_anexo);
                    }
                  });
                //}

                /*valor_uma = umasAnio($("#anio").val());
                umas_anexo += addtp2DC/valor_uma;
                umas_anexo = parseFloat(umas_anexo).toFixed(2);
                console.log("valor8 de umas_anexo: "+umas_anexo);*/
              }
            });
             $(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
          }
        });
      } //for
      var time = Clon*1000;
      setTimeout(function () { 
        //console.log("clon: "+Clon);
        //console.log("j: "+j);
        //console.log("varj_aux: "+varj_aux);

        if(lim_efec_band==true){
          swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
          $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
        }else{
          lim_efec_band=false;
          $('.limite_efect_msj').html('');
        }
        //console.log("valor de band_divisas: "+band_divisas);
        //console.log("varj_aux final: "+varj_aux);
        umas_anexo = parseFloat(umas_anexo).toFixed(2);
        //console.log("valor de umas_anexo final: "+umas_anexo);
        //if(varj_aux==Clon /*&& band_divisas==true*/ /*&& lim_efec_band==false*/){
          addtpF = addtp + addtp2 + addtpD + addtp2D +addtpC + addtp2C + addtpDC + addtp2DC;

          /*console.log("valor de addtp: "+addtp);
          console.log("valor de addtp2: "+addtp2);
          console.log("valor de addtpD: "+addtpD);
          console.log("valor de addtp2: "+addtp2D);
          console.log("valor de addtpC: "+addtpC);
          console.log("valor de addtp2C: "+addtp2C);
          console.log("valor de addtpDC: "+addtpDC);
          console.log("valor de addtp2DC: "+addtp2DC);*/
          addtpF = parseFloat(addtpF).toFixed(2);
          /*console.log("valor de addtpF: "+addtpF);
          console.log("monto final: "+monto);*/
          umas_anexo = parseFloat(umas_anexo).toFixed(2);
          //console.log("valor de umas_anexo final: "+umas_anexo);

          //$("#total_liquida_efect").val(convertMoneda(efectivo));
          //$("#total_liquida").val(convertMoneda(addtpF));

          /*$.ajax({
            type: 'POST',
            url : base_url+'Umbrales/getUmbral',
            data: {anexo: $("#id_actividad").val()},
            success: function(result){
              //var array = $.parseJSON(result);
              var array= JSON.parse(result);
              var aviso = parseFloat(array[0].aviso).toFixed(2); */
              umas_anexo = umas_anexo*1;
              aviso = aviso*1;
              //console.log("aviso anexo: "+aviso);
              if(siempre_avi=="1"){
                band_umbral=true;
                $.ajax({ //cambiar pago a acusado
                  type: 'POST',
                  url : base_url+'Operaciones/acusaPago2',
                  async: false,
                  data: { id:id, act:$("#id_actividad").val()},
                  success: function(result_divi){

                  }
                });
              }else{
                /*if(umas_anexo>=aviso){
                  band_umbral=true;
                }*/
                if(umas_anexo>=aviso){
                  band_umbral=true;
                  $.ajax({ //cambiar pago a acusado
                    type: 'POST',
                    url : base_url+'Operaciones/acusaPago2',
                    async: false,
                    data: { id:id_reg, act:1},
                    success: function(result_divi){

                    }
                  });
                }
              }
              //traer pagos de otras transacciones pero del mismo anexo sin acusar
              id_acusado=0;
              if(umas_anexo<aviso){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Operaciones/getOpera',
                  async: false,
                  data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act: 1, id_union:$("#id_union").val(), id_anexo:id_reg, id_perfilamiento: $("#id_perfilamiento").val() },
                  success: function(result){
                    var data= $.parseJSON(result);
                    var datos = data.data;
                    
                    datos.forEach(function(element) {
                      var addtpHF=0; var umas_anexoH=0;
                      console.log("element.monto_operacion: "+element.monto_operacion);
                      if(element.moneda!=0){ //divisa
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Divisas/getDivisaTipo',
                          async: false,
                          data: { tipo: element.moneda, fecha: element.fecha_pago },
                          success: function(result_divi){
                            valor_divi=parseFloat(result_divi);
                            tot_moDH = parseFloat(element.monto_operacion);
                            //console.log("valor2 de tot_moD: "+tot_moD);
                            var totalHD = tot_moDH*valor_divi;
                            totalHD = totalHD.toFixed(2);
                            addtpHF += Number(totalHD);

                            $.ajax({
                              type: 'POST',
                              url : base_url+'Umas/getUmasAnio',
                              async: false,
                              data: { anio: element.fecha_pago},
                              success: function(data2){
                                valor_uma=parseFloat(data2).toFixed(2);
                                //console.log("valor de uma en historico: "+valor_uma);
                                precalc= addtpHF/valor_uma;
                                if(precalc>=identifica){
                                  umas_anexoH += parseFloat(addtpHF/valor_uma);
                                  //console.log("si es mayor o igual al valor de identificacion");
                                }
                                
                              }
                            });

                          }
                        });
                      }//if de divisa
                      else{
                        tot_moH = parseFloat(element.monto_operacion);
                        totalHD = tot_moH.toFixed(2);
                        addtpHF += Number(totalHD); 

                        $.ajax({
                          type: 'POST',
                          url : base_url+'Umas/getUmasAnio',
                          async: false,
                          data: { anio: element.fecha_pago},
                          success: function(data2){
                            valor_uma=parseFloat(data2).toFixed(2);
                            //console.log("valor de uma en historico: "+valor_uma);
                            precalc= addtpHF/valor_uma;
                            if(precalc>=identifica){
                              umas_anexoH += parseFloat(addtpHF/valor_uma);
                              console.log("valor de umas_anexoH en ajax: "+umas_anexoH);
                            }
                          }
                        });
                      }
                      addtpHF=parseFloat(addtpHF).toFixed(2);
                      umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                      umas_anexoH=umas_anexoH*1;
                      
                      if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){
                      //if(identifica<=umas_anexoH && identifica!="" && identifica>0){ 
                        //console.log("identifica menor o igual que historico: "+identifica);
                        if(umas_anexoH>=identifica)
                          umas_anexoTot_historico = umas_anexo + umas_anexoH;
                        else
                          umas_anexoTot_historico = umas_anexoH;

                        //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                        if(umas_anexoTot_historico>=aviso){
                          //band_umbral=true;
                          $.ajax({ //cambiar pago a acusado
                            type: 'POST',
                            url : base_url+'Operaciones/acusaPago',
                            async: false,
                            data: { id: element.id, act:1, id_anexo:id_reg, aviso:$("#aviso").val(), pago_ayuda:1},
                            success: function(result2){
                              id_acusado=result2;
                              //console.log("id_acusado: "+id_acusado);
                            }
                          });
                        }
                      }
                      /*else{
                        umas_anexoTot_historico = umas_anexo;
                      }*/
                      console.log("addtpHF: "+addtpHF);
                      console.log("identifica: "+identifica);
                      console.log("umas_anexoH: "+umas_anexoH);
                      console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                      if(umas_anexoTot_historico>=aviso){
                        band_umbral=true;
                      }
                    });//foreach
                  }//success de  get opera
                });
              }

              //console.log("band_umbral: "+band_umbral);
              if (addtpF==monto){ //hay diferentes divisas y no pueden ser los mismos montos siempre
                validar_xml=1;
              }else{
                validar_xml=0;
              }
              //validar_xml=1;
              //console.log("conf: "+conf);
              setTimeout(function () { 
                if(band_umbral==true && conf==1){ //aqui valida la operacion de umas
                  RevSiguiente(id_reg,idct);
                }else if(band_umbral==false && conf==1){
                  RevSiguiente(id_reg,idct);
                }
            }, 1500); //esperar a la sumatoria

            //}//result
          //});//ajax
          
        //}   //if j-clon
      }, time);
    
      

    }//success
   }); 
}

function RevSiguiente(id,idct){
  var idrecibo = id;
  mon=$("#monto_opera").val();
  mon1=replaceAll(mon, ",", "" );
  monto=replaceAll(mon1, "$", "" );
  rsum=$("#monto_opera").val();
  rsum1=replaceAll(rsum, ",", "" );
  suma=replaceAll(rsum1, "$", "" );

  if (addtpF==monto){ //hay diferentes divisas y no pueden ser los mismos montos siempre
    validar_xml=1;
  }else{
    validar_xml=0;
  }
  //validar_xml=1;
  if(band_umbral==true && band_umbral!=undefined){ //aqui valida la operacion de umas
  //if(monto>=56038){ 
    console.log("idrecibo: "+idrecibo);
    if(validar_xml==1 && !isNaN(umas_anexo)){
      if($("#aviso").val()>0){
        window.location.href = base_url+"Transaccion/anexo1_xml/"+idrecibo+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
      }else{
        window.location.href = base_url+"Transaccion/anexo1_xml/"+idrecibo+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
      }
      swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
      /*if(history.back()!=undefined){
        setTimeout(function () { history.back() }, 1500);
      }*/
      if($("#aviso").val()>0){
        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
      }else{
        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
      } 
    }else{
      swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
      /*if(history.back()!=undefined){
        setTimeout(function () { history.back() }, 1500);
      }*/
      if($("#aviso").val()>0){
        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
      }else{
        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
      }
    }
  }else{
    swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
    /*if(history.back()!=undefined){
        setTimeout(function () { history.back() }, 1500);
      }*/
    if($("#aviso").val()>0){
      setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
    }else{
      setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
    }
  }

  /******************* */
  
  
  
  // body... 
}


function confirmarPagosMonto(){
  //console.log('excuting');
  suma_no_efect=0;
  suma_efect=0;
  //var TABLA = $(".card-body > #form-1"); 
  //var TABLA = $("#cln-1 > #form-1"); 
  var TABLA = $("#form-principal > #form-1");              
  TABLA.each(function(){        
    // pesos de Liquidación numeraria 
    if($(this).find("select[name*='moneda'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){ //en pesos
      if($(this).find("select[name*='instrumento_monetario'] option:selected").val()=="1"){
        var monto = $(this).find("input[name*='monto_operacion']").val();
        if(monto.indexOf('$') != -1){
          montoe = replaceAll(monto, ",", "" );
          montoe = replaceAll(montoe, "$", "" );
        }else{
          montoe=monto;
        }
        montoe = parseFloat(montoe);
        suma_efect += Number(montoe);
      }  
      else{
        var monto = $(this).find("input[name*='monto_operacion']").val();
        if(monto.indexOf('$') != -1){
          montone = replaceAll(monto, ",", "" );
          montone = replaceAll(montone, "$", "" );
        }else{
          montone=monto;
        }
        montone = parseFloat(montone);
        suma_no_efect += Number(montone);
      }  
    }
    // pesos de Liquidación en especie 
    if($(this).find("select[id*='monedale'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
      var monto = $(this).find("input[name*='valor_bien']").val();
      if(monto.indexOf('$') != -1){
        montone = replaceAll(monto, ",", "" );
        montone = replaceAll(montone, "$", "" );
      }else{
        montone=monto;
      }
      montone = parseFloat(montone);
      suma_no_efect += Number(montone);
    }
    if($(this).find("select[name*='moneda'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){
      //para sacar cant en pesos por tipo de divisa en liquidacion numeraria
      monto_opera = $(this).find("input[name*='monto_operacion']").val();
      var inst= $(this).find("select[name*='instrumento_monetario'] option:selected").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[name*='moneda'] option:selected").val(),fecha:$(this).find("input[name*='fecha_pago']").val()},
        success: function(result_divi){
          valor_divi=result_divi;
          valor_divi = parseFloat(valor_divi);
          console.log("monto valor_divi: "+valor_divi);
          if(inst=="1"){
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = replaceAll(monto_opera, ",", "" );
              tot_moDE = replaceAll(tot_moDE, "$", "" );
            }else{
              tot_moDE = monto_opera;
            }
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }else{
            if(monto_opera.indexOf('$') != -1){
              montone = replaceAll(monto_opera, ",", "" );
              montone = replaceAll(montone, "$", "" );
            }else{
              montone=monto_opera;
            }
            montone = parseFloat(montone);
            var montone = montone*valor_divi;
            suma_no_efect += Number(montone);
          }
        }
      });  
       $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
        
    }
    if($(this).find("select[id*='monedale'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
      //para sacar cant en pesos por tipo de divisa en liquidacion en especia
      monto_opera = $(this).find("input[name*='valor_bien']").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[name*='moneda'] option:selected").val(),fecha:$("#fecha_operacion").val() },
        success: function(result_divi){
          valor_divi=result_divi;
          valor_divi = parseFloat(valor_divi);
          if(monto_opera.indexOf('$') != -1){
            tot_moDE = replaceAll(monto_opera, ",", "" );
            tot_moDE = replaceAll(tot_moDE, "$", "" );
          }else{
            tot_moDE = monto_opera;
          }
          tot_moDE = parseFloat(tot_moDE);
          var preno_efect = tot_moDE*valor_divi;
          suma_no_efect += Number(preno_efect);
          }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
    }

  });

  for (j = 1; j <=Clon; j++){
    //console.log("clon: "+Clon);
    //console.log("j: "+j);
    var TABLAC = $("#cln-"+j+" > #form-"+j+"");              
    TABLAC.each(function(){        
      // pesos de Liquidación numeraria 
      //console.log("radio checked numeraria: "+$(this).find("input[type=radio]radio[id*='tln-1']").is(":checked"));
      if($(this).find("select[name*='moneda'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){ //en pesos
        if($(this).find("select[name*='instrumento_monetario'] option:selected").val()=="1"){
          var monto = $(this).find("input[name*='monto_operacion']").val();
          if(monto.indexOf('$') != -1){
            montoe = replaceAll(monto, ",", "" );
            montoe = replaceAll(montoe, "$", "" );
          }else{
            montoe=monto;
          }
          montoe = parseFloat(montoe);
          suma_efect += Number(montoe);  
        }else{
          var monto = $(this).find("input[name*='monto_operacion']").val();
          if(monto.indexOf('$') != -1){
            montone = replaceAll(monto, ",", "" );
            montone = replaceAll(montone, "$", "" );
          }else{
            montone=monto;
          }
          montone = parseFloat(montone);
          suma_no_efect += Number(montone);
        }  
      }
      // pesos de Liquidación en especie 
      if($(this).find("select[id*='monedale'] option:selected").val()=="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
        var monto = $(this).find("input[name*='valor_bien']").val();
        if(monto.indexOf('$') != -1){
          montone = replaceAll(monto, ",", "" );
          montone = replaceAll(montone, "$", "" );
        }else{
          montone=monto;
        }
        montone = parseFloat(montone);
        suma_no_efect += Number(montone);
      }
      if($(this).find("select[name*='moneda'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tln-1']").is(":checked")==true){
        monto_opera = $(this).find("input[name*='monto_operacion']").val();
        var inst= $(this).find("select[name*='instrumento_monetario'] option:selected").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $(this).find("select[name*='moneda'] option:selected").val(),fecha:$(this).find("input[name*='fecha_pago']").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            valor_divi = parseFloat(valor_divi);
            //console.log("monto valor_divi: "+valor_divi);
            //console.log(" instrumento_monetario clone: "+inst);
            if(inst=="1" && monto_opera>0){
              //console.log(" monto_opera: "+monto_opera);
              if(monto_opera.indexOf('$') != -1){
                tot_moDE = replaceAll(monto_opera, ",", "" );
                tot_moDE = replaceAll(tot_moDE, "$", "" );
              }else{
                tot_moDE = monto_opera;
              }
              tot_moDE = parseFloat(tot_moDE);
              var preefect = tot_moDE*valor_divi;
              suma_efect += Number(preefect);
            }else if(inst!="1" && monto_opera>0){
              //console.log(" monto_opera: "+monto_opera);
              if(monto_opera.indexOf('$') != -1){
                montone = replaceAll(monto_opera, ",", "" );
                montone = replaceAll(montone, "$", "" );
              }else{
                montone=monto_opera;
              }
              montone = parseFloat(montone);
              //console.log(" valor_divi: "+valor_divi);
              //console.log(" montone: "+montone);
              var montone = montone*valor_divi;
              suma_no_efect += Number(montone);
            }
          }
        });
        $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);

      }
      if($(this).find("select[id*='monedale'] option:selected").val()!="0" && $(this).find("input[type=radio][id*='tle']").is(":checked")==true){
        monto_opera = $(this).find("input[name*='valor_bien']").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $(this).find("select[name*='moneda'] option:selected").val(),fecha:$("#fecha_operacion").val() },
          success: function(result_divi){
            valor_divi=result_divi;
            valor_divi = parseFloat(valor_divi);
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = replaceAll(monto_opera, ",", "" );
              tot_moDE = replaceAll(tot_moDE, "$", "" );
            }else{
              tot_moDE = monto_opera;
            }
            tot_moDE = parseFloat(tot_moDE);
            var preno_efect = tot_moDE*valor_divi;
            suma_no_efect += Number(preno_efect);
            }
        });
        $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
      }
    });
  } //for
  //console.log("suma_efect: "+suma_efect);
  //console.log("suma_no_efect: "+suma_no_efect);
  setTimeout(function () { 
    $("#total_liquida_efect").val(convertMoneda(suma_efect));
    $("#total_liquida").val(convertMoneda(parseFloat(suma_no_efect+suma_efect).toFixed(2)));
    verificarLimite();
  }, 1500);
}

function regresar(){
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}


function btn_referencia_ayuda(){
  $("#modal_referenciam_ayuda").modal('show');
}
function btn_factura_ayuda(){
  $("#modal_no_factura_ayuda").modal('show');
}

// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}