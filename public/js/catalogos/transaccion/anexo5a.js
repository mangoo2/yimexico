var base_url = $('#base_url').val();
var valCurp=false;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var validar_xml;
var siempre_avi;
var total_fin=0; var total=0;
$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
  getpais('m');
  getpais('f');
  tipo_modeda(1);
  tipo_modeda(123);
  ComboAnio();
  select_comptraparte();
  if($('#mes_aux').val()==''){
	  $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
	}else{
	  $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
	}
	$('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");
  var aux_pga=$('#aux_pga').val();
	if(aux_pga==1){
      var id_ax5a=$('#id_ax5a').val();
      tabla_liquidacion_anexo5a(id_ax5a,$("#aviso").val());
  }else{
      agregar_liqui();
  }
  select_contraparte();
  select_instrumento();
  $("#monto_opera").on("change", function(){
    //console.log("cambio monto opera");
    confirmarPagosMonto();
  });
  verificarLimite();
  verificarConfigLimite();

  /* setTimeout(function () { 
    confirmarPagosMonto(); //se llama despues de crear todas las liquidaciones en la funcion de tabla_liquidacion_anexo5a
  }, 5500); */

  $("#fecha_operacion").on("change",function(){
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  if(status==0 && $("#aviso").val()>0){
    verificaFechas();
  }
});

function changeImg(id,id_pago,aux_cont){ //id de input, id del pago asignado, contador
  //console.log("id_pago: "+id_pago);
  //console.log("aux_cont: "+aux_cont);

  const fi = document.getElementById(id);
  //id_aux=$("#"+id+"").data("idaux");
  num_aux=$("#"+id+"").data("num_aux");
  //console.log("id_aux: "+id_aux);
  //console.log("num_aux: "+num_aux);
  filezise = fi.files[0].size;                     
  if(filezise > 2548000) { // 1mb
    //console.log($('.img')[0].files[0].size);
    $("#"+id+"").val('');
    swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
  }else { //ok
    tama_perm = true; 
    var archivo = $("#"+id+"").val();
    var name = id;
    var col=name;
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".pdf",".jpeg",".png",".jpg",".webp");
    permitida = false;
    if($('#'+name)[0].files.length > 0) {
      for (var i = 0; i < extensiones_permitidas.length; i++) {
         if (extensiones_permitidas[i] == extension) {
         permitida = true;
         break;
         }
      }  
      if(permitida==true && tama_perm==true){
        //console.log("tamaño permitido");
        var inputFileImage = document.getElementById(id);
        var file = inputFileImage.files[0];
        var data = new FormData();
        //id_docs = $("#id").val();
        data.append('compro',file);
        
        if(aux_cont==0){ //es nuevo
          id_pago2=0;
        }else{
          id_pago2 = id_pago;
        }
        $.ajax({
          url:base_url+'Transaccion/cargafiles/'+$("#id_ax5a").val()+"/"+id_pago2+'/pagos_transac_anexo5a/'+num_aux,
          type:'POST',
          contentType:false,
          data:data,
          processData:false,
          cache:false,
          success: function(data) {
            var array = $.parseJSON(data);
            //console.log("id_tercero: "+array.id_tercero);
            if (array.ok==true && array.id_tercero>0) {
              //$("#name_comprobante_"+array.id_tercero+"").val(array.name);
              if(num_aux==1)
                $("#name_comprobante_"+array.id_tercero+"").val(array.name);
              else
                $("#name_comprobante"+num_aux+"_"+array.id_tercero+"").val(array.name);
              swal("Éxito", "Comprobante cargado correctamente", "success");
            }else if(array.ok==true && array.id_tercero==0) {
              if(num_aux==1)
                $("#name_comprobante_"+id_pago+"").val(array.name);
              else
                $("#name_comprobante"+num_aux+"_"+id_pago+"").val(array.name);

              swal("Éxito", "Comprobante cargado correctamente", "success");
              //$(".id_terceros_"+array.id_tercero+"")
            }else if(array.ok!=true) {
              swal("Error!", "Intente nuevamente", "error");
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
          }
        });
      }else if(permitida==false){
        swal("Error!", "Tipo de archivo no permitido", "error");
      } 
    }else{
      //swal("Error!", "No se a selecionado un archivo", "error");
    } 
  }
  /******************************************************** */
}

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}
function tipo_modeda(id){
  totot1 = replaceAll($('.monto_'+id).val(), ",", "" );
  totot2 = replaceAll(totot1, "$", "" );
  var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totot2);
  $('.monto_'+id).val(totalg);
  //
  suma_tablaliquidacion();
}
function select_contraparte(){
	if($('#contraparte1').is(':checked')){
        $('.fisica_p').css('display','block');
        $('.moral_p').css('display','none');
        $('.fideicomiso_p').css('display','none');
	}
	if($('#contraparte2').is(':checked')){
        $('.fisica_p').css('display','none');
        $('.moral_p').css('display','block');
        $('.fideicomiso_p').css('display','none');
	}
	if($('#contraparte3').is(':checked')){
        $('.fisica_p').css('display','none');
        $('.moral_p').css('display','none');
        $('.fideicomiso_p').css('display','block');
	}
}
function select_instrumento(){
	if($('#instrumento1').is(':checked')){
        $('.instrumento_publico').css('display','block');
        $('.instrumento_privado').css('display','none');
	}
	if($('#instrumento2').is(':checked')){
        $('.instrumento_publico').css('display','none');
        $('.instrumento_privado').css('display','block');
	}
}
function agregar_liqui(){
  addliquidacion(0,'','','','','','','','');   
}
var aux_liqui=0;
function addliquidacion(id,fecha_pago,forma_pago,instrum_notario,tipo_moneda,monto,comprobante,comprobante2,comprobante3){
    var aviso_tipo=$("#aviso").val();
    var fecha_max=$('#fecha_max').val();
    var fp1=''; var fp2=''; var fp3=''; var fp4=''; var fp5='';
    if(id>0){
      aux_id=id;
      aux_cont=1;
    }
    else{
      aux_id=aux_liqui;
      aux_cont=0;
    }

    if(forma_pago==1){ fp1='selected'; }
    else if(forma_pago==2){fp2='selected';}
    else if(forma_pago==3){fp3='selected';}
    else if(forma_pago==4){fp4='selected';}
    else if(forma_pago==5){fp5='selected';}
    ///////////////////////////////////////
    var html='<div class="row row_div_'+aux_liqui+'">\
                  <input type="hidden" class="id_pago_'+id+'" id="id_a" value="'+id+'">\
                  <div class="col-md-3 form-group">\
                    <label>Fecha de pago</label>\
                    <input onkeydown="return false" class="form-control fecha_pago_a_'+aux_liqui+'" max="'+fecha_max+'" type="date" id="fecha_pago_a" value="'+fecha_pago+'" onchange="validar_fecha_opera('+aux_liqui+')">\
                  </div>\
                  <div class="col-md-3 form-group">\
                    <label>Forma de pago</label>\
                    <select id="forma_pago_a" class="form-control">\
                      <option value="1" '+fp1+'>Contado</option>\
                      <option value="2" '+fp2+'>Diferido o en parcialidades</option>\
                      <option value="3" '+fp3+'>Dación en pago</option>\
                      <option value="4" '+fp4+'>Préstamo o crédito</option>\
                      <option value="5" '+fp5+'>Permuta</option>\
                    </select>\
                  </div>\
                  <div class="col-md-6 form-group">\
                    <label>Instrumento monetario con el que se realizó la operación o acto</label>\
                    <div class="instrumento_mone_select_'+aux_liqui+'"></div>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Tipo de moneda o divisa de la operacion o acto</label>\
                    <div class="tipo_moneda_select_'+aux_liqui+'"></div>\
                  </div>\
                  <div class="col-md-3 form-group">\
                    <label>Monto de la operación o acto sin IVA ni accesorios</label>\
                    <input class="form-control monto_a_'+aux_liqui+'" type="text" id="monto_a" value="'+monto+'" onchange="cambio_moneda_liqui('+aux_liqui+'),confirmarPagosMonto()">\
                  </div>\
                  <div class="row">\
                    <div class="col-md-10 form-group">\
                      <div>\
                        <label style="color: transparent;">agregar liquidación</label>';
                      //if(id>0){
                        html+='<button type="button" class="btn gradient_nepal2" onclick="removerliquidacion('+aux_liqui+','+id+','+aviso_tipo+')"><i class="fa fa-trash-o"></i> Quitar liquidación</button>';
                      //}
                        
                html+='</div>\
                    </div>\
                  </div><div class="col-md-2"><br><h3 class="monto_divisa" style="color: red;"></h3></div>\
                  <div class="col-md-4 form-group"><h4>Comprobante de Liquidación 1:</h4>\
                    <input name="name_comprobante" id="name_comprobante_'+aux_id+'" type="hidden" value="'+comprobante+'">\
                    <div class="col-md-11">\
                      <input onchange="changeImg(this.id,'+aux_id+','+aux_cont+')" width="50px" height="80px" data-num_aux="1" data-idaux="'+aux_id+'" class="img compro_nvo" id="comprobante_'+aux_id+'" type="file">\
                    </div>\
                  </div>\
                  <div class="col-md-4 form-group"><h4>Comprobante de Liquidación 2:</h4>\
                    <input name="name_comprobante2" id="name_comprobante2_'+aux_id+'" type="hidden" value="'+comprobante2+'">\
                    <div class="col-md-11">\
                      <input onchange="changeImg(this.id,'+aux_id+','+aux_cont+')" width="50px" height="80px" data-num_aux="2" data-idaux="'+aux_id+'" class="img compro_nvo" id="comprobante2_'+aux_id+'" type="file">\
                    </div>\
                  </div>\
                  <div class="col-md-4 form-group"><h4>Comprobante de Liquidación 3:</h4>\
                    <input name="name_comprobante3" id="name_comprobante3_'+aux_id+'" type="hidden" value="'+comprobante3+'">\
                    <div class="col-md-11">\
                      <input onchange="changeImg(this.id,'+aux_id+','+aux_cont+')" width="50px" height="80px" data-num_aux="3" data-idaux="'+aux_id+'" class="img compro_nvo" id="comprobante3_'+aux_id+'" type="file">\
                    </div>\
                  </div>';
                  if(aux_liqui==0){
                    html+='<div class="col-md-10 form-group"><button type="button" id="agregar_liquida" class="btn gradient_nepal2" onclick="agregar_liqui()"><i class="fa fa-plus"></i> Agregar Liquidación</button></div>';
                  }
                html+='<div class="col-md-12">\
                    <hr class="subsubtitle">\
                  </div>\
              </div>';
    $('.liquidacion_pago_text').append(html);
    agregartipo_moneda(aux_liqui,tipo_moneda);
    agregarinstrumento_mone(aux_liqui,instrum_notario);
    cambio_moneda_liqui(aux_liqui);
    name="comprobante_"+aux_id;
    name2="comprobante2_"+aux_id;
    name3="comprobante3_"+aux_id;
    fileInputLiquida(aux_id,comprobante,name,1);
    fileInputLiquida(aux_id,comprobante2,name2,2);
    fileInputLiquida(aux_id,comprobante3,name3,3);
    /* setTimeout(function () {  
      suma_tablaliquidacion();
    }, 2000);  */
    aux_liqui++;
}
function cambio_moneda_liqui(aux){
  var mon=$('.monto_a_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_a_"+aux+"").val(monto);
  suma_tablaliquidacion();
}
function agregarinstrumento_mone(id,instrum_notario){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_instrumento_notorio",
      data:{instru:instrum_notario},
      success: function (data) {
        $('.instrumento_mone_select_'+id).html(data);
      }
    });
}
function ValidCurp(T){
  let ex=/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
  valor=T.value;
  valid=valor.match(ex);
  if (valid){
    //console.log("valido");
  }else{
    swal("¡Error!", "CURP No valido", "error");
  }
}
function ValidRfc(T){
  let ex=/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;
  valor=T.value;
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      //console.log("valido");
    }else{
      swal("¡Error!", "RFC No valido", "error");
    }
  }
}

function agregartipo_moneda(id,tipo_moneda){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_moneda",
      data:{t_pago:tipo_moneda},
      success: function (data) {
        $('.tipo_moneda_select_'+id).html(data);
      }
    });
   suma_tablaliquidacion();
}
function removerliquidacion(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el pago!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_pago_anexo5a',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.row_div_'+aux).remove();
              confirmarPagosMonto();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.row_div_'+aux).remove();
  }
}

function tabla_liquidacion_anexo5a(id,aviso){
  var cont_liqui=0; var time_call = 1000;
  $.ajax({
    type:'POST',
    url: base_url+"Transaccion/get_liquidacion_anexo5a",
    data: {id:id,aviso:aviso},
    async:false,
    success: function (response){
      var array = $.parseJSON(response);
      //console.log("length: "+ array.length);
      if(array.length>0) {
        array.forEach(function(element) {
          addliquidacion(element.id,element.fecha_pago,element.forma_pago,element.instrum_notario,element.tipo_moneda,element.monto,element.comprobante,element.comprobante2,element.comprobante3);
          cont_liqui++;
        });
      }else{
        agregar_liqui();
      }
      //console.log("cont_liqui: "+cont_liqui);
      if(array.length==cont_liqui){        
        time_call = cont_liqui * 100;
        setTimeout(function () { 
          confirmarPagosMonto(); //se llama despues de crear todos los dom de liquidaciones 
        }, time_call);
        //console.log("time_call: "+time_call);
      } 
    }
  });
}

function validar_fecha_opera(aux){
  var fecha_pago_liqui=$('.fecha_pago_a_'+aux).val();
  var fecha_operacion=$('#fecha_operacion').val();
  if(fecha_pago_liqui==fecha_operacion){ 
  }else{
    swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
  confirmarPagosMonto();
}
function MonFor(value){
  var mon=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(value);
  //console.log(mon);
  value=mon;
}
function suma_tablaliquidacion(){
  /*var min = true;
  var addtp = 0;
  var mte = 0;
  var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
    TABLAP.each(function(){         
        totalmonto = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        var vstotal = totalmonto;
        addtp += Number(vstotal);
        
        if($(this).find("select[id*='instrum_notario_a'] option:selected").val()==1){
          totalmontoe = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
          totalmontoe = replaceAll(totalmontoe, "$", "" );
          var vstotale = totalmontoe;
          mte += Number(vstotale);
        } 
    });
  //Monto total de la liquidación
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  //Monto total en efectivo
  ttle = replaceAll(mte, ",", "" );
  ttle = replaceAll(ttle, "$", "" );
  var monto_ttle=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(ttle);
  $('#total_liquida_efect').val(monto_ttle);
  
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  
  var aux_mo=0;
  var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
  totalmontomo = replaceAll(totalmontomo, "$", "" );
  var vstotalmo = totalmontomo;
  aux_mo = Number(vstotalmo);
  if(aux_mo==addtp){
    validar_xml=1;
    $('.validacion_cantidad').html('');
  }else{
    validar_xml=0;
    $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
  }*/
}
function verificarLimite(){
  efectivo = replaceAll($("#total_liquida_efect").val(), ",", "" );
  efectivo = replaceAll(efectivo, "$", "" );
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var array= JSON.parse(result);
      var limite = parseFloat(array[0].limite_efectivo).toFixed(2);
      var na = array[0].na;
      //console.log("limite: "+limite);
      //console.log("na: "+na);

      if(limite>0 && na==0){
        var anio = $("#anio").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Umas/getUmasAnio',
          data: { anio: anio},
          success: function(data2){
            valor_uma=data2;
            valor_uma = parseFloat(valor_uma);
            //console.log("valor de umas año: "+valor_uma);
            var uma_efect_anexo = efectivo / valor_uma;
            if(uma_efect_anexo>limite){
              lim_efec_band=true;
              swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
              verificarConfigLimite(1);
              $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
            }else{
              lim_efec_band=false;
              verificarConfigLimite(0);
              $('.limite_efect_msj').html('');
            }
          }
        });
      }//if na
    }
  });
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function registrar(){
  var form_register = $('#form_anexo5a');
  var error_register = $('.alert-danger', form_register);
  var success_register = $('.alert-success', form_register);
  if($('#instrumento1').is(':checked')){
    var $validator1=form_register.validate({
      errorElement: 'div', //default input error message container
      errorClass: 'vd_red', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
          fecha_operacion:{
            required: true
          },
          /*monto_opera:{
            required: true
          },*/
          referencia:{
            required: true
          },
          numero_instrumento_publico:{
            maxlength: 20
          },
          notario_instrumento_publico:{
            maxlength: 8
          },
          valor_pactado:{
            required:true
          }
      },
      errorPlacement: function(error, element) {
          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
      }, 
      
      invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(500);
              error_register.fadeIn(500);
              scrollTo(form_register,-100);

      },

      highlight: function (element) { // hightlight error inputs
  
          $(element).addClass('vd_bd-red');
          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

      },

      unhighlight: function (element) { // revert the change dony by hightlight
          $(element)
              .closest('.control-group').removeClass('error'); // set error class to the control group
      },

      success: function (label, element) {
          label
              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
          $(element).removeClass('vd_bd-red');
      }
    }); 
  }else{  
    var $validator1=form_register.validate({
      errorElement: 'div', //default input error message container
      errorClass: 'vd_red', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
          fecha_operacion:{
            required: true
          },
          /*monto_opera:{
            required: true
          },*/
          referencia:{
            required: true
          },
      },
      errorPlacement: function(error, element) {
          if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
              element.parent().append(error);
          } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
          }else {
              error.insertAfter(element);
          }
      }, 
      
      invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(500);
              error_register.fadeIn(500);
              scrollTo(form_register,-100);

      },

      highlight: function (element) { // hightlight error inputs
  
          $(element).addClass('vd_bd-red');
          $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

      },

      unhighlight: function (element) { // revert the change dony by hightlight
          $(element)
              .closest('.control-group').removeClass('error'); // set error class to the control group
      },

      success: function (label, element) {
          label
              .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
              .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
          $(element).removeClass('vd_bd-red');
      }
    });
  }
  //////////Registro///////////
  var valid = $("#form_anexo5a").valid();
  var band_aviso = 0;
  if(valid) {
    monto_opera=0;
    /*monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
    monto_opera = replaceAll(monto_oper, "$", "" );*/
    valor_avaluo_catastra = replaceAll($("#valor_avaluo_catastral").val(), ",", "" );
    valor_avaluo_catastral = replaceAll(valor_avaluo_catastra, "$", "" );
    
    tot_liqui = replaceAll($("input[id*='total_liquida']").val(),"$", "");
    tot_liqui = replaceAll(tot_liqui,",", "");
    tot_liqui = parseFloat(tot_liqui);

    if($("#aviso").val()>0){
      name_table = "anexo5a_aviso";
    }else{
      name_table = "anexo5a";
    }

    if($("#aviso").val()>0){
      if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
        band_aviso=1;
      }
    }

    var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera+"&valor_avaluo_catastral="+valor_avaluo_catastral+"&tot_liqui="+tot_liqui;
    if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
      $("#btn_submit").attr("disabled",true);
      var band_fecha=0;
      /*var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
      TABLAP.each(function(){  
        if($(this).find("input[id*='fecha_pago_a']").val()=="")
          band_fecha++;
      });*/
      
      $(".liquidacion_pago .liquidacion_pago_text > div input[id*='fecha_pago_a']").filter(function() {
          return $(this).val() === ""; // Verifica si el valor es vacío
      }).each(function() {
          band_fecha++; // Incrementa solo cuando encuentras un elemento vacío
      });

      if(band_fecha==0){
        $.blockUI({ 
          message: '<div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status" id="cont_load">\
              <span class="sr-only">Guardando...</span>\
            </div><br>Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
          css: { 
              border: 'none', 
              padding: '15px', 
              backgroundColor: '#000', 
              '-webkit-border-radius': '10px', 
              '-moz-border-radius': '10px', 
              opacity: .5, 
              color: '#fff'
          } 
        });
        $.ajax({
          type:'POST',
          url: base_url+'Transaccion/registro_anexo5a',
          data:datos_anexo,
          //async:false,
          statusCode:{
            404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
              swal("Error!", "500", "error");
            }
          },
          beforeSend: function(){
            $("#btn_submit").attr("disabled",true);
          },
          success:function(data){
            var array = $.parseJSON(data);
            var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
            //////////////////////////////////////////////////
            var DATAP= [];
            var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
            TABLAP.each(function(){         
              item = {};
              item ["idanexo5a"]=id;
              item ["aviso"]=$("#aviso").val();
              item ["id"]=$(this).find("input[id*='id_a']").val();
              item ["fecha_pago"]=$(this).find("input[id*='fecha_pago_a']").val();
              item ["forma_pago"]=$(this).find("select[id*='forma_pago_a']").val();
              item ["instrum_notario"]=$(this).find("select[id*='instrum_notario_a'] option:selected").val();
              item ["tipo_moneda"]=$(this).find("select[id*='tipo_moneda_a'] option:selected").val();
              montop = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
              montop = replaceAll(montop, "$", "" );
              item ["monto"]=montop;
              item ["comprobante"]=$(this).find("input[name*='name_comprobante']").val();
              item ["comprobante2"]=$(this).find("input[name*='name_comprobante2']").val();
              item ["comprobante3"]=$(this).find("input[name*='name_comprobante3']").val();
              DATAP.push(item);
            });
            INFOP  = new FormData();
            aInfop   = JSON.stringify(DATAP);
            INFOP.append('data', aInfop);
            //console.log("aInfop: "+aInfop);
            $.ajax({
              data: INFOP,
              type: 'POST',
              url : base_url+'index.php/Transaccion/inset_pago_anexo5a',
              processData: false, 
              contentType: false,
              async: false,
              statusCode:{
                404: function(data2){
                  //toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                  //toastr.error('Error', '500');
                }
              },
              success: function(data2){
                ///////////
                $("input[name*='id_aviso']").val(id);
                var anio = $("#anio").val();
                var valor_uma; var band_umbral=false;
                var addtp=0; var addtpD=0;
                var act = new Date();
                var mes_act = act.getMonth();
                var anio_act = act.getFullYear();
                var umas_anexo=0;
                //var addtpHF=0; var umas_anexoH=0; 
                var umas_anexoTot_historico=0; let umas_anexoTot_historicoFinal=0;
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umbrales/getUmbral',
                  data: {anexo: $("#id_actividad").val()},
                  async:false,
                  success: function(result){
                    //var array = $.parseJSON(result);
                    var array= JSON.parse(result);
                    var aviso = parseFloat(array[0].aviso).toFixed(2);
                    var siempre_avi=array[0].siempre_avi;
                    var siempre_iden=array[0].siempre_iden;
                    var identifica=array[0].identificacion;
                    //console.log("identifica pricipal: "+identifica);
                    cont=0;
                    var TABLA = $(".liquidacion_pago .liquidacion_pago_text > div");              
                    TABLA.each(function(){        
                      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
                        //console.log("pesos de Liquidación ");
                        var monto_operacion = $(this).find("input[id*='monto_a']").val();
                        var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
                        if(monto_operacion.indexOf('$') != -1 || monto_operacion.indexOf(',') != -1){
                          tot_mo = replaceAll(monto_operacion, ",", "" );
                          tot_mo = replaceAll(tot_mo, "$", "" );
                        }else{
                          tot_mo=monto_operacion;
                        }
                        tot_mo = parseFloat(tot_mo);
                        var vstotal1 = tot_mo;
                        /*let date = new Date(fecha_pago);
                        var mes_pago = date.getMonth();
                        mes_pago = parseInt(mes_pago);
                        mes_pago = mes_pago +1;
                        var anio_pago = date.getFullYear();*/
                        //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                          addtp = Number(vstotal1);
                        //}
                        //addtp += Number(vstotal1);
                        //if(addtp>0){
                          $.ajax({
                            type: 'POST',
                            url : base_url+'Umas/getUmasAnio',
                            data: { anio: fecha_pago},
                            async: false,
                            success: function(data2){
                              valor_uma=data2;
                              valor_uma = parseFloat(valor_uma);
                              //console.log("valor uma en function umasAnio: "+valor_uma);
                              umas_anexo += addtp/valor_uma;
                              //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                              //console.log("valor1 de umas_anexo: "+umas_anexo);
                            }
                          });
                        //}
                      }
                      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){
                        //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
                        //console.log("moneda != pesos de Liquidación");
                        var monto_operacion = $(this).find("input[id*='monto_a']").val();
                        var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Divisas/getDivisaTipo',
                          async: false,
                          data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(), fecha:$(this).find("input[id*='fecha_pago_a']").val()},
                          success: function(result_divi){
                            valor_divi=result_divi;
                            valor_divi = parseFloat(valor_divi);
                            $(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
                            //console.log("monto operacion: "+monto_operacion);
                            //console.log("valor_divi: "+valor_divi);
                            if(monto_operacion.indexOf('$') != -1){
                              tot_moD = replaceAll(monto_operacion, ",", "" );
                              tot_moD = replaceAll(tot_moD, "$", "" );
                            }else{
                              tot_moD = monto_operacion;
                            }
                            tot_moD = parseFloat(tot_moD);
                            //console.log("valor2 de tot_moD: "+tot_moD);
                            var vstotal1D = tot_moD*valor_divi;
                            //console.log("valor2 de vstotal1D: "+vstotal1D);
                            /*let date = new Date(fecha_pago);
                            var mes_pago = date.getMonth();
                            mes_pago = parseInt(mes_pago);
                            mes_pago = mes_pago +1;
                            var anio_pago = date.getFullYear();*/
                            //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                              addtpD += Number(vstotal1D);
                            //}
                            //addtpD += Number(vstotal1D);
                            //console.log("valor2 de addtpD: "+addtpD);
                            //if(addtp>0){
                              $.ajax({
                                type: 'POST',
                                url : base_url+'Umas/getUmasAnio',
                                data: { anio: fecha_pago},
                                async: false,
                                success: function(data2){
                                  valor_uma=data2;
                                  valor_uma = parseFloat(valor_uma);
                                  //console.log("valor uma en function umasAnio: "+valor_uma);
                                  umas_anexo += addtpD/valor_uma;

                                  //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                  //console.log("valor2 de umas_anexo: "+umas_anexo);
                                }
                              });
                            //}
                          }
                        });
                      }
                      cont++;
                    });
  
                    var time = cont*750; //cont = cantidad de liquidaciones activas
                    setTimeout(function () { 

                      if(lim_efec_band==true){
                        swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
                        $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
                      }else{
                        lim_efec_band=false;
                        $('.limite_efect_msj').html('');
                      }

                      umas_anexo = parseFloat(umas_anexo).toFixed(2);
                      //console.log("valor de umas_anexo final: "+umas_anexo);
                      //console.log("valor de umas aviso: "+aviso);
                      addtpF = addtp + addtpD;
                      //console.log("valor de addtp: "+addtp);
                      //console.log("valor de addtpD: "+addtpD);
                      total_fin = Number(total);
                      //total_fin = Number(addtpF);
                      umas_anexo = umas_anexo*1;
                      aviso = aviso*1;
                      //console.log("aviso: "+aviso);
                      if(siempre_avi=="1"){ //no aplica para el 5a
                        band_umbral=true;
                        $.ajax({ //cambiar pago a acusado
                          type: 'POST',
                          url : base_url+'Operaciones/acusaPago2',
                          async: false,
                          data: { id:id, act:$("#id_actividad").val()},
                          success: function(result_divi){

                          }
                        });
                      }else{
                        if(umas_anexo>=aviso){
                          val_pact = replaceAll($("input[name*='valor_pactado']").val(),"$", "");
                          val_pact = replaceAll(val_pact,",", "");
                          val_pact = parseFloat(val_pact);
                          //console.log("val_pact: "+val_pact)
                          if(total_fin==val_pact){
                            band_umbral=true;
                            $.ajax({ //cambiar pago a acusado
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPago2',
                              async: false,
                              data: { id:id, act:$("#id_actividad").val()},
                              success: function(result_divi){

                              }
                            });
                          }
                        }
                      }
                      //traer pagos de otras transacciones pero del mismo anexo sin acusar
                      id_acusado=0;
                      if(umas_anexo<aviso){
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Operaciones/getOpera',
                          async: false,
                          data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id, id_perfilamiento: $("#id_perfilamiento").val() },
                          success: function(result){
                            var data= $.parseJSON(result);
                            var datos = data.data;
                            var id_element = [];
                            datos.forEach(function(element) {
                              var addtpHF=0; var umas_anexoH=0;
                              //console.log("tipo de moneda pagos historico: "+element.tipo_moneda);
                              if(element.tipo_moneda!=0){ //divisa
                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Divisas/getDivisaTipo',
                                  async: false,
                                  data: { tipo: element.tipo_moneda, fecha: element.fecha_pago },
                                  success: function(result_divi){
                                    valor_divi=parseFloat(result_divi);
                                    tot_moDH = parseFloat(element.monto);
                                    //console.log("valor2 de tot_moD: "+tot_moD);
                                    var totalHD = tot_moDH*valor_divi;
                                    totalHD = totalHD.toFixed(2);
                                    addtpHF += Number(totalHD);
                                    //console.log("valor de addtpHF divisa: "+addtpHF);

                                    $.ajax({
                                      type: 'POST',
                                      url : base_url+'Umas/getUmasAnio',
                                      async: false,
                                      data: { anio: element.fecha_pago},
                                      success: function(data2){
                                        valor_uma=parseFloat(data2).toFixed(2);
                                        //console.log("valor de uma en historico: "+valor_uma);
                                        umas_anexoH += parseFloat(addtpHF/valor_uma);
                                        //console.log("valor de umas_anexoH divisa: "+umas_anexoH);
                                      }
                                    });
                                  }
                                });
                              }//if de divisa
                              else{
                                //console.log("tipo de moneda pagos historico pesos: "+element.tipo_moneda);
                                tot_moH = parseFloat(element.monto).toFixed(2);
                                addtpHF += Number(tot_moH);
                                //console.log("valor de addtpHF: "+addtpHF); 
                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Umas/getUmasAnio',
                                  async: false,
                                  data: { anio: element.fecha_pago},
                                  success: function(data2){
                                    valor_uma=parseFloat(data2).toFixed(2);
                                    //console.log("valor de uma en historico: "+valor_uma);
                                    umas_anexoH += parseFloat(addtpHF/valor_uma);
                                    //console.log("valor de umas_anexoH: "+umas_anexoH);
                                  }
                                });
                              }
                              addtpHF=parseFloat(addtpHF).toFixed(2);
                              umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                              umas_anexoH=umas_anexoH*1;
                              //console.log("identifica: "+identifica);
                              var opera_comph=0; var cont_hist=0; var band_comple=0; var id_ant=0;
                            
                              if(identifica<=umas_anexoH || siempre_iden=="1" /* || identifica>umas_anexo */){ 
                                /*if(umas_anexoH>=identifica)
                                  umas_anexoTot_historico += umas_anexo + umas_anexoH;
                                else
                                  umas_anexoTot_historico = umas_anexo;*/

                                if(umas_anexoH>=identifica || siempre_iden=="1"){
                                  umas_anexoTot_historico += umas_anexoH;
                                }else{
                                  umas_anexoTot_historico = umas_anexoH;
                                }
                                //umas_anexoTot_historico = umas_anexo + umas_anexoH; //comentado desde inicios
                                umas_anexoTot_historicoFinal = umas_anexoTot_historico+umas_anexo;
                                //console.log("umas_anexoTot_historicoFinal: "+umas_anexoTot_historicoFinal);
                                if(umas_anexoTot_historico<aviso && band_comple==0){
                                  id_ant = element.id;
                                }

                                //console.log("element.id de consulta operaciones historico: "+element.id);
                                item = {}; 
                                item ["id_desa"]=element.id;
                                id_element.push(item);
                                INFO  = new FormData();
                                aInfo = JSON.stringify(id_element);
                                INFO.append('data', aInfo);

                                id_acusado=0;
                                if(umas_anexoTot_historicoFinal>=aviso){
                                  val_pact = replaceAll($("input[name*='valor_pactado']").val(),"$", "");
                                  val_pact = replaceAll(val_pact,",", "");
                                  val_pact = parseFloat(val_pact);
                                  //console.log("val_pact en umas_anexoTot_historicoFinal: "+val_pact);
                                  //console.log("aInfo para acuse: "+aInfo);
                                  if(total_fin==val_pact){

                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPago',
                                      async: false,
                                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1},
                                      success: function(result2){
                                        id_acusado=result2;
                                      }
                                    });

                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoActividad5aArray',
                                      async: false,
                                      data: "data="+aInfo+"&act"+$("#id_actividad").val()+"&id_anexo="+id+"&aviso="+$("#aviso").val()+"&pago_ayuda=1"+"&id_ant="+id_ant,
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado de array: "+id_acusado);
                                      }
                                    });
                                  }   
                                }
                              }

                              //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                              //umas_anexoTot_historicoFinal = umas_anexoTot_historico+umas_anexo;
                              //console.log("umas_anexoTot_historicoFinal: "+umas_anexoTot_historicoFinal);

                              if(umas_anexoTot_historicoFinal>=aviso){
                                band_comple=1;
                                band_umbral=true;
                              }
                            });//foreach
                          }//success de  get opera
                        });
                      }

                      //console.log("band_umbral: "+band_umbral);
                      if (addtpF==monto){ //hay diferentes divisas y no pueden ser los mismos montos siempre
                        validar_xml=1;
                      }else{
                        validar_xml=0;
                      }
                      val_pact = replaceAll($("input[name*='valor_pactado']").val(),"$", "");
                      val_pact = replaceAll(val_pact,",", "");
                      val_pact = parseFloat(val_pact);
                      //console.log("valor_uma: "+valor_uma);
                      //console.log("val_pact: "+val_pact);
                      //console.log("total_fin: "+total_fin)
                      $.unblockUI();

                      setTimeout(function () {
                        if(band_umbral==true && band_umbral!=undefined && !isNaN(umas_anexo) && valor_uma>0 && total_fin==val_pact){
                          //if(validar_xml==1){
                            if($("#aviso").val()>0){
                              window.location.href = base_url+"Transaccion/anexo5a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                            }else{
                              window.location.href = base_url+"Transaccion/anexo5a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                            }
                            swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                            /*if(history.back()!=undefined){
                              setTimeout(function () { history.back() }, 1500);
                            }*/
                            if($("#aviso").val()>0){
                              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                            }else{
                              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                            }
                          /*}else{
                            swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");

                            if($("#aviso").val()>0){
                              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                            }else{
                              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                            }
                          }*/
                        }
                        else{
                          swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                          /*if(history.back()!=undefined){
                            setTimeout(function () { history.back() }, 1500);
                          }*/
                          if($("#aviso").val()>0){
                            setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                          }else{
                            setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                          }
                        }
                      }, 1500); //esperar a la sumatoria
                      ///////////

                    }, time);
                  
                  }//success  
                });  //cierra ajax de umbral de anexo
              } //success de insert pagos de anexo5
            }); 
            
            
          } //success del ajax registro de anexo5
        }); //ajax registro_anexo5a 
      } // if band_fecha
      else{
        swal("Error!", "Ingrese fecha(s) de pago", "error");
        $("#btn_submit").attr("disabled",false);
      }
    }//band aviso
    else{
      swal("Error!", "Los campos de Aviso son obligatorios", "error");
    }           
  }  else{
    $.unblockUI();
  } 
}

function fileInputLiquida(id=0,compro,name,aux){
  //console.log("compro: "+compro);
  if(aux==1)
    name_col="comprobante";
  else if(aux==2)
    name_col="comprobante2";
  else
    name_col="comprobante3";
  if(id>0){
    imgprev2="";
    imgdet2="";
    imgp2="";
    typePDF2="";
    //compro = $("#name_comprobante_"+id+"").val();
    //console.log("compro: "+compro);
    //name="comprobante_"+id+"";
    if(compro!=""){
      imgprev2 = base_url+"uploads/comprobantes_anexo5a/"+compro;
      imgdet2 = {type:"image", url: base_url+"Transaccion/file_delete/"+id+"/pagos_transac_anexo5a/"+name_col, caption: compro, key:id};
      ext = compro.split('.');
      if(ext[1].toLowerCase()!="pdf"){
        imgp2 = base_url+"uploads/comprobantes_anexo5a/"+compro; 
        typePDF2 = "false";  
      }else{
        imgp2 = base_url+"uploads/comprobantes_anexo5a/"+compro;
        imgdet2 = {type:"pdf", url: base_url+"Transaccion/file_delete/"+id+"/pagos_transac_anexo5a/"+name_col, caption: compro, key:id};
        typePDF2 = "true";
      }
    }

    $("#"+name+"").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "webp"],
      initialPreview: [
        ''+imgprev2+'',    
      ],
      initialPreviewAsData: typePDF2,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet2
      ],
    });
  }
  else{
    name="compro_nvo";
    $("."+name+"").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: '',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "jpeg","pdf", "webp"],
      initialPreview: [
      ],
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        {type: "image", caption:"", key: 1},
      ]
    });
  }
  
}

async function confirmarPagosMonto() {
  var suma_no_efect = 0;
  var suma_efect = 0;
  var contta = 0;
  var valor_divi = 0;
  var montoe = 0;
  var montone = 0;

  var TABLA = $(".liquidacion_pago .liquidacion_pago_text > div");
  for (let i = 0; i < TABLA.length; i++) {
    var div = $(TABLA[i]);
    var tipo_moneda_sel = div.find("select[id*='tipo_moneda_a']");
    var tipo_moneda_sel_value = tipo_moneda_sel.length > 0 ? tipo_moneda_sel.val() : "0"; // Valor por defecto "0" si no se encuentra

    //console.log("tipo_moneda_a: " + tipo_moneda_sel_value);
    // Si es en pesos
    if (tipo_moneda_sel_value == "0") {
      //console.log("pesos");
      div.find(".monto_divisa").hide();

      var monto = div.find("input[id*='monto_a']").val();
      if (div.find("select[id*='instrum_notario_a'] option:selected").val() == "1") { // Efectivo
        montoe = limpiarMonto(monto);
        suma_efect += montoe;
      } else { // No es efectivo
        montone = limpiarMonto(monto);
        suma_no_efect += montone;
      }

    } else { // Si no es en pesos, es una divisa
      //console.log("¡Divisa!");
      var monto_opera = div.find("input[id*='monto_a']").val();
      var inst = div.find("select[id*='instrum_notario_a'] option:selected").val();
      try {
        valor_divi = await obtenerValorDivisa(tipo_moneda_sel_value, div.find("input[id*='fecha_pago_a']").val());
        valor_divi = parseFloat(valor_divi);
        //console.log("Valor de divisa: " + valor_divi);
        if (inst == "1") { // Efectivo
          monto_opera = limpiarMonto(monto_opera);
          var preefect = monto_opera * valor_divi;
          suma_efect += preefect;
        } else { // No efectivo
          monto_opera = limpiarMonto(monto_opera);
          var montone = monto_opera * valor_divi;
          suma_no_efect += montone;
        }
      } catch (error) {
        //console.error("Error al obtener el valor de la divisa: ", error);
      }
      div.find(".monto_divisa").html('Valor de divisa: $' + valor_divi);
    }
    contta++;
  }

  var time_out = contta * 25;
  //console.log("time_out: " + time_out);

  setTimeout(function () {
    total = parseFloat(suma_no_efect + suma_efect).toFixed(2);
    console.log("total: " + total);
    $("#total_liquida_efect").val(convertMoneda(suma_efect));
    $("#total_liquida").val(convertMoneda(total));

    verificarLimite();
  }, time_out);
}

// Función para obtener el valor de la divisa
async function obtenerValorDivisa(tipoMoneda, fecha) {
  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      url: base_url + 'Divisas/getDivisaTipo',
      data: { tipo: tipoMoneda, fecha: fecha },
      success: function(result_divi) {
        resolve(result_divi);
      },
      error: function(err) {
        reject(err);
      }
    });
  });
}

// Función para limpiar el monto (eliminar comas y simbolo de dólar)
function limpiarMonto(monto) {
  if (monto.indexOf('$') !== -1) {
    monto = replaceAll(monto, ",", "");
    monto = replaceAll(monto, "$", "");
  }
  return parseFloat(monto);
}

function convertMoneda(num){
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
  return monto;
}

function regresar(){
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}
function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
function getpais(text){
  //console.log("get pais2");
    $('.idpais_'+text).select2({
        width: '300px',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Transaccion/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
function select_comptraparte(){
  var of=$('#figura_so option:selected').val();
  if(of==3){
    $('.contraparte_txt').css('display','block');
  }else{
    $('.contraparte_txt').css('display','none');
  }
}

////////////////////// Operaciones
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
/// < / >
// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}
// < / > // 

function cambiaCP(id){
  cp = $("#"+id+"").val();  
  if(cp!="" && id=="cp"){
    col = "colonia";
    var itemc = [];
    $('#colonia').select2({
      placeholder: {
          id: 0,
          text: 'Buscar colonia:'
      },
      width: '100%',
      allowClear: true,
      data: itemc
    });
    autoComplete("colonia");
  }
}
function autoComplete(idcol){
  if(idcol=="colonia"){
    cp = $("#codigo_postal").val(); 
    col = $('#colonia').val();  
  }

  $('#'+idcol+'').select2({
      width: 'resolve',
      minimumInputLength: 0,
      minimumResultsForSearch: 10,
      placeholder: 'Seleccione una colonia:',
      ajax: {
          url: base_url+'Perfilamiento/getDatosCPSelect',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  cp: cp, 
                  col: col ,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.colonia,
                      text: element.colonia,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}