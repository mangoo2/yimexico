var base_url = $('#base_url').val();
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var validar_xml;
var siempre_avi;
$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
    tipo_modeda(1);
    selecionar_ganrantica();
    setTimeout(function () {  
       tipo_bine_otros();
    }, 1000);
    ComboAnio();
    $('#tipo_bien option[value="'+$('#tipo_bien_aux').val()+'"]').attr("selected", "selected");
    $('#instrumento_monetario option[value="'+$('#instrumento_monetario_aux').val()+'"]').attr("selected", "selected");
    // Tipo persona
    var ctp=$('#checked_tipo_persona').val();
    tipo_persona(ctp);
    ///// N liquidaciones
    var aux_pga=$('#aux_pga').val();
    if(aux_pga==1){
      var id_ax4=$('#id_ax4').val();
      tabla_liquidacion_anexo4(id_ax4,$("#aviso").val());
    }else{
      agregar_liqui();
    } 

  $("#monto_opera").on("change", function(){
    //console.log("cambio monto opera");
    confirmarPagosMonto();
  });
  verificarLimite();
  verificarConfigLimite();
  setTimeout(function () { 
    confirmarPagosMonto();
  }, 1500);
  $("#fecha_operacion").on("change",function(){
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();
});	

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
function tipo_modeda(id){
  totot1 = replaceAll($('.monto_opera_'+id).val(), ",", "" );
  totot2 = replaceAll(totot1, "$", "" );
  var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totot2);
  $('.monto_opera_'+id).val(totalg);
  //
  suma_tablaliquidacion();
}
function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function ValidCurp(T){
  let ex=/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
  valor=T.value;
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "CURP No valido", "error");
    }
  }
}

function ValidRfc(T){
  let ex=/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;
  valor=T.value;
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "RFC No valido", "error");
    }
  }
}

function tipo_persona(id){	
	if($('#tipo'+id).is(':checked')){
        $('.text_tipo'+id).css('display','block');
        if(id==1){
        	$('.text_tipo2').css('display','none');
            $('.text_tipo3').css('display','none');
        }
		else if(id==2){
			$('.text_tipo1').css('display','none');
            $('.text_tipo3').css('display','none');
		}
		else if(id==3){
			$('.text_tipo1').css('display','none');
            $('.text_tipo2').css('display','none');
		}
	}
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function registrar(){
        var form_register = $('#form_anexo4');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        if($('#tipo1').is(':checked')){
          var $validator1=form_register.validate({
              errorElement: 'div', //default input error message container
              errorClass: 'vd_red', // default input error message class
              focusInvalid: false, // do not focus the last invalid input
              ignore: "",
              rules: {
                  fecha_operacion:{
                    required: true
                  },
                  /*monto_opera:{
                    required: true
                  },*/
                  referencia:{
                    required: true
                  },
                  nombre_f:{
                    required: true
                  },
                  app_paternof:{
                    required: true
                  },
                  app_maternof:{
                    required: true
                  },
                  fecha_nacimientof:{
                    required: true
                  },
                  rfc_f:{
                    required: true
                  },
                  curpf:{
                    required: true
                  },
              },
              errorPlacement: function(error, element) {
                  if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                      element.parent().append(error);
                  } else if (element.parent().hasClass("vd_input-wrapper")){
                      error.insertAfter(element.parent());
                  }else {
                      error.insertAfter(element);
                  }
              }, 
              
              invalidHandler: function (event, validator) { //display error alert on form submit              
                      success_register.fadeOut(500);
                      error_register.fadeIn(500);
                      scrollTo(form_register,-100);

              },

              highlight: function (element) { // hightlight error inputs
          
                  $(element).addClass('vd_bd-red');
                  $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

              },

              unhighlight: function (element) { // revert the change dony by hightlight
                  $(element)
                      .closest('.control-group').removeClass('error'); // set error class to the control group
              },

              success: function (label, element) {
                  label
                      .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                      .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                  $(element).removeClass('vd_bd-red');
              }
          });
        }
        if($('#tipo2').is(':checked')){
          var $validator1=form_register.validate({
              errorElement: 'div', //default input error message container
              errorClass: 'vd_red', // default input error message class
              focusInvalid: false, // do not focus the last invalid input
              ignore: "",
              rules: {
                  fecha_operacion:{
                    required: true
                  },
                  /*monto_opera:{
                    required: true
                  },*/
                  referencia:{
                    required: true
                  },
                  razon_socialm:{
                    required: true
                  },
                  fecha_constitucionm:{
                    required: true
                  },
                  rfc_m:{
                    required: true
                  }
              },
              errorPlacement: function(error, element) {
                  if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                      element.parent().append(error);
                  } else if (element.parent().hasClass("vd_input-wrapper")){
                      error.insertAfter(element.parent());
                  }else {
                      error.insertAfter(element);
                  }
              }, 
              
              invalidHandler: function (event, validator) { //display error alert on form submit              
                      success_register.fadeOut(500);
                      error_register.fadeIn(500);
                      scrollTo(form_register,-100);

              },

              highlight: function (element) { // hightlight error inputs
          
                  $(element).addClass('vd_bd-red');
                  $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

              },

              unhighlight: function (element) { // revert the change dony by hightlight
                  $(element)
                      .closest('.control-group').removeClass('error'); // set error class to the control group
              },

              success: function (label, element) {
                  label
                      .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                      .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                  $(element).removeClass('vd_bd-red');
              }
          });
        }
        if($('#tipo3').is(':checked')){
          var $validator1=form_register.validate({
              errorElement: 'div', //default input error message container
              errorClass: 'vd_red', // default input error message class
              focusInvalid: false, // do not focus the last invalid input
              ignore: "",
              rules: {
                  fecha_operacion:{
                    required: true
                  },
                  /*monto_opera:{
                    required: true
                  },*/
                  referencia:{
                    required: true
                  },
                  /*num_factura:{
                    required: true
                  },*/
                  razon_socialfi:{
                    required: true
                  },
                  rfc_fi:{
                    required: true
                  },
                  referencia_fi:{
                    required: true
                  }
              },
              errorPlacement: function(error, element) {
                  if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                      element.parent().append(error);
                  } else if (element.parent().hasClass("vd_input-wrapper")){
                      error.insertAfter(element.parent());
                  }else {
                      error.insertAfter(element);
                  }
              }, 
              
              invalidHandler: function (event, validator) { //display error alert on form submit              
                      success_register.fadeOut(500);
                      error_register.fadeIn(500);
                      scrollTo(form_register,-100);

              },

              highlight: function (element) { // hightlight error inputs
          
                  $(element).addClass('vd_bd-red');
                  $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

              },

              unhighlight: function (element) { // revert the change dony by hightlight
                  $(element)
                      .closest('.control-group').removeClass('error'); // set error class to the control group
              },

              success: function (label, element) {
                  label
                      .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                      .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                  $(element).removeClass('vd_bd-red');
              }
          });
        }
 
        //////////Registro///////////
        var valid = $("#form_anexo4").valid();
        var band_aviso = 0;
        if(valid) {
          
          monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
          monto_opera = replaceAll(monto_oper, "$", "" );
          
          if($("#aviso").val()>0){
            name_table = "anexo4_aviso";
          }else{
            name_table = "anexo4";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }

          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;

          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo4',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = $.parseJSON(data);
                var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                //////////////////////////////////////////////////
                    var DATAP= [];
                    var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
                    TABLAP.each(function(){         
                          item = {};
                          item ["idanexo4"]=id;
                          item ["aviso"]=$("#aviso").val();
                          item ["id"]=$(this).find("input[id*='id_a']").val();
                          item ["fecha_pago"]=$(this).find("input[id*='fecha_disposicion_a']").val();
                          item ["instrum_notario"]=$(this).find("select[id*='instrum_notario_a'] option:selected").val();
                          item ["tipo_moneda"]=$(this).find("select[id*='tipo_moneda_a'] option:selected").val();
                          montop = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
                          montop = replaceAll(montop, "$", "" );
                          item ["monto"]=montop;
                          DATAP.push(item);
                    });
                    INFOP  = new FormData();
                    aInfop   = JSON.stringify(DATAP);
                    INFOP.append('data', aInfop);
                    $.ajax({
                        data: INFOP,
                        type: 'POST',
                        url : base_url+'index.php/Transaccion/inset_pago_anexo4',
                        processData: false, 
                        contentType: false,
                        async: false,
                        statusCode:{
                            404: function(data2){
                                //toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                //toastr.error('Error', '500');
                            }
                        },
                        success: function(data2){
                          
                        }
                    }); 
                    ////

                $("input[name*='id_aviso']").val(id);
                var anio = $("#anio").val();
                var valor_uma; var band_umbral=false;
                var addtp=0; var addtpD=0;
                var act = new Date();
                var mes_act = act.getMonth();
                var anio_act = act.getFullYear();
                var umas_anexo=0;
                //var addtpHF=0; var umas_anexoH=0; 
                var umas_anexoTot_historico=0;
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umbrales/getUmbral',
                  data: {anexo: $("#id_actividad").val()},
                  success: function(result){
                    //var array = $.parseJSON(result);
                    var array= JSON.parse(result);
                    var aviso = parseFloat(array[0].aviso).toFixed(2);
                    var siempre_avi=array[0].siempre_avi;
                    var siempre_iden=array[0].siempre_iden;
                    var identifica=array[0].identificacion;
                    /*console.log("siempre avi: "+siempre_avi);
                    console.log("siempre_iden: "+siempre_iden);
                    console.log("identifica: "+identifica);*/
                    cont=0;
                    var TABLA = $(".liquidacion_pago .liquidacion_pago_text > div");              
                    TABLA.each(function(){        
                      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
                        //console.log("pesos de Liquidación ");
                        var monto_operacion = $(this).find("input[id*='monto_a']").val();
                        var fecha_pago = $(this).find("input[id*='fecha_disposicion_a']").val();
                        if(monto_operacion.indexOf('$') != -1){
                          tot_mo = replaceAll(monto_operacion, ",", "" );
                          tot_mo = replaceAll(tot_mo, "$", "" );
                        }else{
                          tot_mo=monto_operacion;
                        }
                        tot_mo = parseFloat(tot_mo);
                        var vstotal1 = tot_mo;
                        let date = new Date(fecha_pago);
                        var mes_pago = date.getMonth();
                        mes_pago = parseInt(mes_pago);
                        mes_pago = mes_pago +1;
                        var anio_pago = date.getFullYear();
                        //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                          addtp += Number(vstotal1);
                        //}
                        //addtp += Number(vstotal1);
                        //if(addtp>0){
                          $.ajax({
                            type: 'POST',
                            url : base_url+'Umas/getUmasAnio',
                            data: { anio: fecha_pago},
                            async: false,
                            success: function(data2){
                              valor_uma=data2;
                              valor_uma = parseFloat(valor_uma);
                              //console.log("valor uma en function umasAnio: "+valor_uma);
                              umas_anexo += addtp/valor_uma;
                              //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                              //console.log("valor1 de umas_anexo: "+umas_anexo);
                            }
                          });
                        //}
                      }
                      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){
                        //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
                        //console.log("moneda != pesos de Liquidación");
                        var monto_operacion = $(this).find("input[id*='monto_a']").val();
                        var fecha_pago = $(this).find("input[id*='fecha_disposicion_a']").val();
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Divisas/getDivisaTipo',
                          async: false,
                          data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(), fecha:$(this).find("input[id*='fecha_disposicion_a']").val()},
                          success: function(result_divi){
                            valor_divi=result_divi;
                            valor_divi = parseFloat(valor_divi);
                            $(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
                            //console.log("monto operacion: "+monto_operacion);
                            //console.log("valor_divi: "+valor_divi);
                            if(monto_operacion.indexOf('$') != -1){
                              tot_moD = replaceAll(monto_operacion, ",", "" );
                              tot_moD = replaceAll(tot_moD, "$", "" );
                            }else{
                              tot_moD = monto_operacion;
                            }
                            tot_moD = parseFloat(tot_moD);
                            //console.log("valor2 de tot_moD: "+tot_moD);
                            var vstotal1D = tot_moD*valor_divi;
                            //console.log("valor2 de vstotal1D: "+vstotal1D);
                            let date = new Date(fecha_pago);
                            var mes_pago = date.getMonth();
                            mes_pago = parseInt(mes_pago);
                            mes_pago = mes_pago +1;
                            var anio_pago = date.getFullYear();
                            //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                              addtpD += Number(vstotal1D);
                            //}
                            //addtpD += Number(vstotal1D);
                            //console.log("valor2 de addtpD: "+addtpD);
                            //if(addtpD>0){
                              $.ajax({
                                type: 'POST',
                                url : base_url+'Umas/getUmasAnio',
                                data: { anio: fecha_pago},
                                async: false,
                                success: function(data2){
                                  valor_uma=data2;
                                  valor_uma = parseFloat(valor_uma);
                                  //console.log("valor uma en function umasAnio: "+valor_uma);
                                  umas_anexo += addtpD/valor_uma;
                                  //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                  //console.log("valor2 de umas_anexo: "+umas_anexo);
                                }
                              });
                            //}
                          }
                        });
                      }
                      cont++;
                    });

                    var time = cont*500;
                    setTimeout(function () { 
                      if(lim_efec_band==true){
                        swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
                        $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
                      }else{
                        lim_efec_band=false;
                        $('.limite_efect_msj').html('');
                      }
    
                      umas_anexo = parseFloat(umas_anexo).toFixed(2);
                      console.log("valor de umas_anexo final: "+umas_anexo);
                      addtpF = addtp + addtpD;
                      console.log("valor de addtpF: "+addtpF);

                      umas_anexo = umas_anexo*1;
                      aviso = aviso*1;
                      //console.log("aviso: "+aviso);
                      if(siempre_avi=="1"){
                        band_umbral=true;
                        $.ajax({ //cambiar pago a acusado
                          type: 'POST',
                          url : base_url+'Operaciones/acusaPago2',
                          async: false,
                          data: { id:id, act:$("#id_actividad").val()},
                          success: function(result_divi){

                          }
                        });
                      }else{
                        if(umas_anexo>=aviso){
                          band_umbral=true;
                          $.ajax({ //cambiar pago a acusado
                          type: 'POST',
                          url : base_url+'Operaciones/acusaPago2',
                          async: false,
                          data: { id:id, act:$("#id_actividad").val()},
                          success: function(result_divi){

                          }
                        });
                        }
                      }
                      //traer pagos de otras transacciones pero del mismo anexo sin acusar
                      id_acusado=0;
                      if(umas_anexo<aviso){
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Operaciones/getOpera',
                          async: false,
                          data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id, id_perfilamiento: $("#id_perfilamiento").val() },
                          success: function(result){
                            var data= $.parseJSON(result);
                            var datos = data.data;
                            datos.forEach(function(element) {
                              var addtpHF=0; var umas_anexoH=0;
                              if(element.tipo_moneda!=0){ //divisa
                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Divisas/getDivisaTipo',
                                  async: false,
                                  data: { tipo: element.tipo_moneda, fecha: element.fecha_disposicion },
                                  success: function(result_divi){
                                    valor_divi=parseFloat(result_divi);
                                    tot_moDH = parseFloat(element.monto_operacion);
                                    //console.log("valor2 de tot_moD: "+tot_moD);
                                    var totalHD = tot_moDH*valor_divi;
                                    totalHD = totalHD.toFixed(2);
                                    addtpHF += Number(totalHD);

                                    $.ajax({
                                      type: 'POST',
                                      url : base_url+'Umas/getUmasAnio',
                                      async: false,
                                      data: { anio: element.fecha_disposicion},
                                      success: function(data2){
                                        valor_uma=parseFloat(data2).toFixed(2);
                                        //console.log("valor de uma en historico: "+valor_uma);
                                        umas_anexoH += parseFloat(addtpHF/valor_uma);
                                      }
                                    });

                                  }
                                });
                              }//if de divisa
                              else{
                                tot_moH = parseFloat(element.monto_operacion);
                                totalHD = tot_moH.toFixed(2);
                                addtpHF += Number(totalHD); 

                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Umas/getUmasAnio',
                                  async: false,
                                  data: { anio: element.fecha_disposicion},
                                  success: function(data2){
                                    valor_uma=parseFloat(data2).toFixed(2);
                                    //console.log("valor de uma en historico: "+valor_uma);
                                    umas_anexoH += parseFloat(addtpHF/valor_uma);
                                  }
                                });
                              }
                              addtpHF=parseFloat(addtpHF).toFixed(2);
                              umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                              umas_anexoH=umas_anexoH*1;

                              if(identifica<=umas_anexoH || siempre_iden=="1" /* || identifica>umas_anexo */){ 
                                if(umas_anexoH>=identifica)
                                  umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                else
                                  umas_anexoTot_historico = umas_anexoH;

                                //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                if(umas_anexoTot_historico>=aviso){
                                  $.ajax({ //cambiar pago a acusado
                                    type: 'POST',
                                    url : base_url+'Operaciones/acusaPago',
                                    async: false,
                                    data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1},
                                    success: function(result2){
                                      id_acusado=result2;
                                    }
                                  });
                                }
                              }

                              //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                              if(umas_anexoTot_historico>=aviso){
                                band_umbral=true;
                              }
                            });//foreach
                          }//success de  get opera
                        });
                      }

                      //console.log("band_umbral: "+band_umbral);
                      if (addtpF==monto){ //hay diferentes divisas y no pueden ser los mismos montos siempre
                        validar_xml=1;
                      }else{
                        validar_xml=0;
                      }


                      //console.log("monto operacion: "+monto);
                      //Anexo 4. Umbral: $139,442.00
                      setTimeout(function () { 
                        if(band_umbral==true && band_umbral!=undefined && !isNaN(umas_anexo)){
                          //if(validar_xml==1){ // se comenta por comentaro ultimo en chat de mara, no lleva factura
                            if($("#aviso").val()>0){
                              window.location.href = base_url+"Transaccion/anexo4_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                            }else{
                              window.location.href = base_url+"Transaccion/anexo4_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                            }
                            
                            swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                            /*if(history.back()!=undefined){
                              setTimeout(function () { history.back() }, 1500);
                            }*/
                            if($("#aviso").val()>0){
                              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                            }else{
                              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                            }
                          //}

                        }
                        else{
                          swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                          /*if(history.back()!=undefined){
                            setTimeout(function () { history.back() }, 1500);
                          }*/
                          if($("#aviso").val()>0){
                            setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                          }else{
                            setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                          }
                        }
                      }, 1500); //esperar a la sumatoria
                      //
                    }, time);
                    
                  }//success  
                });  //ajax
                
              }  
            }); 
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          } 
        }  
}
function regresar(){
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}
function agregar_liqui(){
  addliquidacion(0,'','','','');   
}
var aux_liqui=0;
function addliquidacion(id,fecha_pago,instrum_notario,tipo_moneda,monto){
    var aviso_tipo=$("#aviso").val();
    var fecha_max=$('#fecha_max').val();
    ///////////////////////////////////////
    var in1=''; var in2=''; var in3=''; var in4=''; var in5=''; var in6=''; var in7=''; var in8=''; var in9=''; 
    var in10=''; var in11=''; var in12=''; var in13=''; var in14=''; var in15=''; var in16=''; var in17='';
    if(instrum_notario==1){ in1='selected'; }
    else if(instrum_notario==2){in2='selected';}
    else if(instrum_notario==3){in3='selected';}
    else if(instrum_notario==4){in4='selected';}
    else if(instrum_notario==5){in5='selected';}
    else if(instrum_notario==6){in6='selected';}
    else if(instrum_notario==7){in7='selected';}
    else if(instrum_notario==8){in8='selected';}
    else if(instrum_notario==9){in9='selected';}
    else if(instrum_notario==10){in10='selected';}
    else if(instrum_notario==11){in11='selected';}
    else if(instrum_notario==12){in12='selected';}
    else if(instrum_notario==13){in13='selected';}
    else if(instrum_notario==14){in14='selected';}
    else if(instrum_notario==15){in15='selected';}
    else if(instrum_notario==16){in16='selected';}
    else if(instrum_notario==17){in17='selected';}

    var html='<div class="row row_div_'+aux_liqui+'">\
                  <input type="hidden" id="id_a" value="'+id+'">\
                  <div class="col-md-3 form-group">\
                    <label>Fecha de disposición</label>\
                    <input onkeydown="return false" class="form-control fecha_pago_a_'+aux_liqui+'" max="'+fecha_max+'" type="date" id="fecha_disposicion_a" value="'+fecha_pago+'" onchange="validar_fecha_opera('+aux_liqui+')">\
                  </div>\
                  <div class="col-md-5 form-group">\
                    <label>Instrumento monetario con el que se realizó la operación o acto</label>\
                    <select id="instrum_notario_a" class="form-control" onchange="confirmarPagosMonto()">\
                      <option value="1" '+in1+'>Efectivo</option>\
                      <option value="2" '+in2+'>Tarjeta de Crédito</option>\
                      <option value="3" '+in3+'>Tarjeta de Debito</option>\
                      <option value="4" '+in4+'>Tarjeta de Prepago</option>\
                      <option value="5" '+in5+'>Cheque Nominativo</option>\
                      <option value="6" '+in6+'>Cheque de Caja</option>\
                      <option value="7" '+in7+'>Cheques de Viajero</option>\
                      <option value="8" '+in8+'>Transferencia Interbancaria</option>\
                      <option value="9" '+in9+'>Transferencia Misma Institución</option>\
                      <option value="10" '+in10+'>Transferencia Internacional</option>\
                      <option value="11" '+in11+'>Orden de Pago</option>\
                      <option value="12" '+in12+'>Giro</option>\
                      <option value="13" '+in13+'>Oro o Platino Amonedados</option>\
                      <option value="14" '+in14+'>Plata Amonedada</option>\
                      <option value="15" '+in15+'>Metales Preciosos</option>\
                      <option value="16" '+in16+'>Activos Virtuales</option>\
                      <option value="17" '+in17+'>Otros</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Tipo de moneda o divisa de la operación</label>\
                    <div class="tipo_moneda_select_'+aux_liqui+'"></div>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Monto de la operación o acto sin IVA ni accesorios</label>\
                    <input class="form-control monto_a_'+aux_liqui+'" type="text" id="monto_a" value="'+monto+'" onchange="cambio_moneda_liqui('+aux_liqui+'),confirmarPagosMonto()">\
                  </div>\
                  <div class="row">\
                    <div class="col-md-10 form-group">\
                      <div>\
                        <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_liqui==0){
                      html+=''; 
                        }else{
                      html+='<button type="button" class="btn gradient_nepal2" onclick="removerliquidacion('+aux_liqui+','+id+','+aviso_tipo+')"><i class="fa fa-trash-o"></i> Quitar operación</button>';
                        }
                        
                html+='</div>\
                    </div>\
                  </div><div class="col-md-2"><br><h3 class="monto_divisa" style="color: red;"></h3></div>\
                  <div class="col-md-12">\
                    <hr class="subsubtitle">\
                  </div>\
              </div>';
    $('.liquidacion_pago_text').append(html);
    agregartipo_moneda(aux_liqui,tipo_moneda);
    cambio_moneda_liqui(aux_liqui);
    aux_liqui++;
}
function agregartipo_moneda(id,tipo_moneda){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_moneda",
      data:{t_pago:tipo_moneda},
      success: function (data) {
        $('.tipo_moneda_select_'+id).html(data);
      }
    });
}
function removerliquidacion(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el pago!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_pago_anexo15',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.row_div_'+aux).remove();
              confirmarPagosMonto();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.row_div_'+aux).remove();
  }
}
function tabla_liquidacion_anexo4(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_liquidacion_anexo4",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  addliquidacion(element.id,element.fecha_disposicion,element.instrumento_monetario,element.tipo_moneda,element.monto_operacion);
                });
            }else{
                agregar_liqui();
            }   
        }
    });
}
function cambio_moneda_liqui(aux){
  var mon=$('.monto_a_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_a_"+aux+"").val(monto);
  suma_tablaliquidacion();
}
function suma_tablaliquidacion(){
  /*var min = true;
  var addtp = 0;
  var mte = 0;
  var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
    TABLAP.each(function(){         
        totalmonto = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        var vstotal = totalmonto;
        addtp += Number(vstotal);
        
        if($(this).find("select[id*='instrum_notario_a'] option:selected").val()==1){
          totalmontoe = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
          totalmontoe = replaceAll(totalmontoe, "$", "" );
          var vstotale = totalmontoe;
          mte += Number(vstotale);
        } 
    });
  //Monto total de la liquidación
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  //Monto total en efectivo
  ttle = replaceAll(mte, ",", "" );
  ttle = replaceAll(ttle, "$", "" );
  var monto_ttle=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(ttle);
  $('#total_liquida_efect').val(monto_ttle);
  
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  
  var aux_mo=0;
  var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
  totalmontomo = replaceAll(totalmontomo, "$", "" );
  var vstotalmo = totalmontomo;
  aux_mo = Number(vstotalmo);
  if(aux_mo==addtp){
    validar_xml=1;
    $('.validacion_cantidad').html('');
  }else{
    validar_xml=0;
    $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
  }*/
}

function verificarLimite(){
  efectivo = replaceAll($("#total_liquida_efect").val(), ",", "" );
  efectivo = replaceAll(efectivo, "$", "" );
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var array= JSON.parse(result);
      var limite = parseFloat(array[0].limite_efectivo).toFixed(2);
      var na = array[0].na;
      //console.log("limite: "+limite);
      //console.log("na: "+na);

      if(limite>0 && na==0){
        var anio = $("#anio").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Umas/getUmasAnio',
          data: { anio: anio},
          success: function(data2){
            valor_uma=data2;
            valor_uma = parseFloat(valor_uma);
            //console.log("valor de umas año: "+valor_uma);
            var uma_efect_anexo = efectivo / valor_uma;
            if(uma_efect_anexo>limite){
              lim_efec_band=true;
              swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
              $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
            }else{
              lim_efec_band=false;
              $('.limite_efect_msj').html('');
            }
          }
        });
      }//if na
    }
  });
}

function confirmarPagosMonto(){ //para sumar el efectivo nadamas
  suma_no_efect=0;
  suma_efect=0;
  contta=0;
  valor_divi=0;
  montoe=0;
  montone=0;
  var TABLA = $(".liquidacion_pago .liquidacion_pago_text > div");   
  TABLA.each(function(){        
    // pesos de Liquidación 
    if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
      //console.log("pesos");
      if($(this).find("select[id*='instrum_notario_a'] option:selected").val()=="1"){
        var monto = $(this).find("input[id*='monto_a']").val();
        //console.log("monto: "+$(this).find("input[id*='monto_a']").val());
        if(monto.indexOf('$') != -1){
          montoe = replaceAll(monto, ",", "" );
          montoe = replaceAll(montoe, "$", "" );
        }else{
          montoe=monto;
        }
        montoe = parseFloat(montoe);
        suma_efect += Number(montoe);

      }  
      else{
        var monto = $(this).find("input[id*='monto_a']").val();
        if(monto.indexOf('$') != -1){
          montone = replaceAll(monto, ",", "" );
          montone = replaceAll(montone, "$", "" );
        }else{
          montone=monto;
        }
        montone = parseFloat(montone);
        suma_no_efect += Number(montone);
      }
      /*console.log("suma_nefect pesos: "+suma_efect);
      console.log("suma_no_efect pesos: "+suma_no_efect);*/
    }
    if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){ //!= de pesos
      //console.log("!pesos");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      monto_opera = $(this).find("input[id*='monto_a']").val();
      var inst = $(this).find("select[id*='instrum_notario_a'] option:selected").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(),fecha:$(this).find("input[id*='fecha_disposicion_a']").val()},
        success: function(result_divi){
          valor_divi=result_divi;
          valor_divi = parseFloat(valor_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = replaceAll(monto_opera, ",", "" );
              tot_moDE = replaceAll(tot_moDE, "$", "" );
            }else{
              tot_moDE = monto_opera;
            }
            tot_moDE = parseFloat(tot_moDE);
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }else{
            if(monto_opera.indexOf('$') != -1){
              montone = replaceAll(monto_opera, ",", "" );
              montone = replaceAll(montone, "$", "" );
            }else{
              montone=monto_opera;
            }
            montone = parseFloat(montone);
            var montone = montone*valor_divi;
            suma_no_efect += Number(montone);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
    }
    contta++;
  });
  
  setTimeout(function () { 
    total = parseFloat(suma_no_efect+suma_efect).toFixed(2);
    var aux_mo=0;
    var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
    totalmontomo = replaceAll(totalmontomo, "$", "" );
    var vstotalmo = totalmontomo;
    aux_mo = Number(vstotalmo);
    if(aux_mo==total){
      validar_xml=1;
      $('.validacion_cantidad').html('');
    }else{
      validar_xml=0;
      //$('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
    }
    $("#total_liquida_efect").val(convertMoneda(suma_efect));
    $("#total_liquida").val(convertMoneda(total));
    verificarLimite();
  }, 1500);
}

function convertMoneda(num){
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
  return monto;
}

function validar_fecha_opera(aux){
  var fecha_pago_liqui=$('.fecha_pago_a_'+aux).val();
  var fecha_operacion=$('#fecha_operacion').val();
  if(fecha_pago_liqui==fecha_operacion){ 
  }else{
    //swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
  confirmarPagosMonto();
}
function selecionar_ganrantica(){
  var t_p = $('#tipo_operacion option:selected').val();
  if(t_p==401){
    $('.datos_garante').css('display','none');
    $('.datos_garante2').css('display','none');
  }else if(t_p==402){
    $('.datos_garante').css('display','block');
    $('.datos_garante2').css('display','block');
  }
}    
function tipo_bine_otros(){
  var t_p = $('#tipo_bien option:selected').val();
  if(t_p==99){
    $('.tipo_bien_text').css('display','none');
    $('.tipo_bien_text_otros').css('display','block');
  }else{
    $('.tipo_bien_text').css('display','block');
    $('.tipo_bien_text_otros').css('display','none');
  }
}
function numero_decimal(){
  var av_r= $('#valor_referencia').val();
  var pv= parseFloat(av_r);
  var conDecimal = pv.toFixed(2);
  $('#valor_referencia').val(conDecimal);
}

////////////////////// Operaciones
function btn_fecha_opera(){
  $('#modal_fecha_opera').modal();
}
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
/// < / >
// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}
// < / > // 