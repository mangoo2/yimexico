var validar_xml=0;
var valCurp=false;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var siempre_avi;
var valor_uma=0;
$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
  $('#desglo_det').click(function(event) {

    $("#det_client").show("slow");
    $("#desglo_det").removeClass("waves-effect").addClass("btn-danger").attr("onclick","minus()").find("i").removeClass("mdi mdi-plus-circle").addClass('mdi mdi-minus-circle');
    $("#desglo_det").attr("id",'oculte_cli');
  });

  validator=$("#TiBi").val();
  if (validator==1){
    $("#traslado_efect").show("slow");
    $("#traslado_otros").hide("slow");
  }else{
    $("#traslado_efect").hide("slow");
    $("#traslado_otros").show("slow");
  }
//--------mostrarQuitar------//
  $('#efectivo_inst').click(function(event) {
    $("#traslado_efect").show("slow");
    $("#traslado_otros").hide("slow");
  });
  $('#otros').click(function(event) {
    $("#traslado_efect").hide("slow");
    $("#traslado_otros").show("slow");
  });
//--------mostrarQuitarEnd------//
  $('#ent_nac').click(function(event) {
    $("#cont_cust").show("slow");
    $("#cont_cust_int").hide("slow");
  });

  $('#ent_inter').click(function(event) {
    $("#cont_cust").hide("slow");
    $("#cont_cust_int").show("slow");
  });
  ComboAnio();
  AllNformat();
  OcultarDes();
  valorUma();
  $("#fecha_operacion").on("change",function(){
    valorUma();
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });

  if(status==0 && $("#aviso").val()>0)
    verificaFechas();
  
});

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
function btn_descripcion_ayuda(){
  $('#modal_descripcion_ayuda').modal();
}


function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc);
  }
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function AllNformat(){
    $(".Nformat").each(function(){
      mon=$(this).val();
      mon1=replaceAll(mon, ",", "" );
      mon2=replaceAll(mon1, "$", "" );
      monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mon2);
      $(this).val(monto);
    });
}

$(".Nformat").blur(function(){
  mon=$(this).val();
  mon1=replaceAll(mon, ",", "" );
  mon2=replaceAll(mon1, "$", "" );
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mon2);
  $(this).val(monto);
});

function plus(){
    $("#det_client").show("slow");
    $("#desglo_det").removeClass("waves-effect").addClass("btn-danger").attr("onclick","minus()").find("i").removeClass("mdi mdi-plus-circle").addClass('mdi mdi-minus-circle');
    $("#desglo_det").attr("id",'oculte_cli')
}
function minus(){
  $("#det_client").hide("slow"); 
  $("#oculte_cli").removeClass("btn-danger").addClass("waves-effect").attr("onclick","plus()").find("i").removeClass("mdi mdi-minus-circle").addClass('mdi mdi-plus-circle');
  $("#oculte_cli").attr("id",'desglo_det');
}
function tipo_cliente_select() {
  	var tipo = $('#perfilCli option:selected').val();
    if(tipo==1){
      $(".texto_tipo_cliente").html("Persona fisica mexicana o extranjera con condición de estancia temporal o permanente");
      $(".paso_1_2_3_texto").html("Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).");
      $(".paso2_hoja_1_2_datos_generales").show("slow");
      $(".paso_2_hoja_1_2_3").show("slow");
      $(".paso2_1").show("slow");
      $(".paso2_2").hide(1000);
      $(".paso_2_2_hoja_telefono").hide(1000);
      $(".paso_2_1_hoja_presenta_apoderado_legal").show("slow");
      $(".paso2_hoja_3_datos_generales").hide(1000);
      $(".paso2_3_hoja3").hide(1000);
      $(".paso2_hoja_4_datos_generales").hide(1000);
      $(".paso2_hoja_5_datos_generales").hide(1000);
      $(".paso2_hoja_6_datos_generales").hide(1000);
    }else if(tipo==2){
      $(".texto_tipo_cliente").html("Persona fisica extranjera con condición de estancia de visitante");
      $(".paso_1_2_3_texto").html("Nombre - los apellido completos que corresponda y nombre(s).");
      $(".paso2_hoja_1_2_datos_generales").show("slow");
      $(".paso_2_hoja_1_2_3").show("slow");
      $(".paso_2_2_hoja_telefono").show("slow");
      $(".paso_2_1_hoja_presenta_apoderado_legal").hide(1000);
      $(".paso2_hoja_3_datos_generales").hide(1000);
      $(".paso2_3_hoja3").hide(1000);
      $(".paso2_hoja_4_datos_generales").hide(1000);
      $(".paso2_hoja_5_datos_generales").hide(1000);
      $(".paso2_hoja_6_datos_generales").hide(1000);
    }else if(tipo==3){
      $(".texto_tipo_cliente").html("Persona moral mexicana y extranjera, persona moral de derecho público");	
      $(".paso_1_2_3_texto").html("Nombre - apellido paterno, materno y nombre(s) / En caso de ser extranjero los apellidos completos que corresponda y nombre(s).");
      $(".paso2_hoja_1_2_datos_generales").hide(1000);
      $(".paso_2_hoja_1_2_3").show("slow");
      $(".paso_2_1_text").hide(1000);
      $(".paso_2_2_text").hide(1000);
      $(".paso_2_2_hoja_telefono").hide(1000);
      $(".paso2_hoja_3_datos_generales").show("slow");
      $(".paso2_3_hoja3").show("slow");
      $(".paso2_hoja_4_datos_generales").hide(1000);
      $(".paso2_hoja_5_datos_generales").hide(1000);
      $(".paso2_hoja_6_datos_generales").hide(1000);
    }else if(tipo==4){
      $(".texto_tipo_cliente").html('Persona moral mexicana de derecho público detallados en el anexo 7 bis A <button type="button" class="btn btn-outline-secondary btn-rounded btn-icon" onclick="paso2_hoja4_btn_texto_ayuda()">\
                          ?\
                        </button>');	
      $(".paso_1_2_3_texto").html("");
      $(".paso2_hoja_1_2_datos_generales").hide(1000);
      $(".paso_2_hoja_1_2_3").hide(1000);
      $(".paso2_1").hide(1000);
      $(".paso2_2").hide(1000);
      $(".paso_2_2_hoja_telefono").hide(1000);
      $(".paso_2_1_hoja_presenta_apoderado_legal").hide(1000);
      $(".paso2_hoja_3_datos_generales").hide(1000);
      $(".paso2_3_hoja3").hide(1000);
      $(".paso2_hoja_4_datos_generales").show("slow");
      $(".paso2_hoja_5_datos_generales").hide(1000);
      $(".paso2_hoja_6_datos_generales").hide(1000);
    }else if(tipo==5){
      $(".texto_tipo_cliente").html("Embajadas, consulados u organismos internacionales");	
      $(".paso_1_2_3_texto").html("");
      $(".paso2_hoja_1_2_datos_generales").hide(1000);
      $(".paso_2_hoja_1_2_3").hide(1000);
      $(".paso2_1").hide(1000);
      $(".paso2_2").hide(1000);
      $(".paso_2_2_hoja_telefono").hide(1000);
      $(".paso_2_1_hoja_presenta_apoderado_legal").hide(1000);
      $(".paso2_hoja_3_datos_generales").hide(1000);
      $(".paso2_3_hoja3").hide(1000);
      $(".paso2_hoja_4_datos_generales").hide(1000);
      $(".paso2_hoja_5_datos_generales").show("slow");
      $(".paso2_hoja_6_datos_generales").hide(1000);
    }else if(tipo==6){
      $(".texto_tipo_cliente").html("Fideicomisos");		
      $(".paso_1_2_3_texto").html("");
      $(".paso2_hoja_1_2_datos_generales").hide(1000);
      $(".paso_2_hoja_1_2_3").hide(1000);
      $(".paso2_1").hide(1000);
      $(".paso2_2").hide(1000);
      $(".paso_2_2_hoja_telefono").hide(1000);
      $(".paso_2_1_hoja_presenta_apoderado_legal").hide(1000);
      $(".paso2_hoja_3_datos_generales").hide(1000);
      $(".paso2_3_hoja3").hide(1000);
      $(".paso2_hoja_4_datos_generales").hide(1000);
      $(".paso2_hoja_5_datos_generales").hide(1000);
      $(".paso2_hoja_6_datos_generales").show("slow");
    } 
    if(tipo>=1){
      $(".paso2_datos_generales").show("slow");
      $(".btn_paso2").show("slow");
        setTimeout(function(){ 
	    	getpais();
	    }, 3000);
    }else{
      $(".paso_1_y_2_texto").html("");
      $(".paso2_hoja_1_2_datos_generales").hide();
      $(".paso_2_hoja_1_2_3").hide();
      $(".paso2_1").hide();
      $(".paso2_2").hide();
      $(".paso_2_2_hoja_telefono").hide();
      $(".paso_2_1_hoja_presenta_apoderado_legal").hide();
      $(".paso2_hoja_3_datos_generales").hide();
      $(".paso2_3_hoja3").hide();
      $(".paso2_hoja_4_datos_generales").hide();
    }
}

function ShowOPC(){
  opc=$('input:radio[name=EDPOA]:checked').val();
  console.log(opc);
  switch (opc) {
    case "PF":
    $('#DPOVFisica').show();
    $('#DPOVMoral').hide();
    $('#DPOVFideicomiso').hide();
    break;
    case "PM":
    $('#DPOVFisica').hide();
    $('#DPOVMoral').show();
    $('#DPOVFideicomiso').hide();
    break;
    case "F":
    $('#DPOVFisica').hide();
    $('#DPOVMoral').hide();
    $('#DPOVFideicomiso').show();
    break;
    default:
    alert("Algo salio Extrañamente mal");
    break;
  }
}

function showPerson(){
  opc=$('input:radio[name=destinatario]:checked').val();
  if(opc=="NO"){
    $('#Conten-Person').show();
  }else{
    $('#Conten-Person').hide();
  }
}

function OcultarDes(){
  $('#Conten-Person').hide();
  $('#DPOVFisica').hide();
  $('#DPOVMoral').hide();
  $('#DPOVFideicomiso').hide();
}

function registrar(){
  var form_register = $('#form_anexo10');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
                referencia:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_anexo10").valid();
        var band_aviso = 0;
        if(valid) {
          
          monto_opera = $("#monto_opera").val();

          if($("#aviso").val()>0){
            name_table = "anexo10_aviso";
          }else{
            name_table = "anexo10";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }

          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'TransaccionAn10/registro_anexo10',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = JSON.parse(data);
                var id=array.id; 
                var monto=parseFloat(array.monto); 
                var id_clientec_transac=array.id_clientec_transac; 
                $("input[name*='id_aviso']").val(id);
                SenComp(id);
                //Anexo 9 es de $139,442.00
              }  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }  
}

function SenComp(id){
  info=$("#complete").serialize();
  $.ajax({
        type: 'POST',
        url : base_url+'index.php/TransaccionAn10/inset_pago_anexo10',
        async: false,
        data: info+"&idanexo10="+id+"&id="+$("#idLi").val()+"&aviso="+$("#aviso").val()+"&fecha_operacion="+$("#fecha_operacion").val(),
        success: function(data2){
        }
    });
    RevSiguiente(id);
}

function valorUma(){
  $.ajax({
    type: 'POST',
    url : base_url+'Umas/getUmasAnio',
    data: { anio: $("#fecha_operacion").val()},
    success: function(data2){
      //console.log("data2 de umas: "+data2);
      valor_uma=data2;
      var valor_uma=parseFloat(valor_uma);
      $("#valor_uma").val(valor_uma);
    }
  });
}

function RevSiguiente(id){
  mon=$("#monto_opera").val();
  mon1=replaceAll(mon, ",", "" );
  monto=replaceAll(mon1, "$", "" );
  rsum=$("#monto_opera").val();
  rsum1=replaceAll(rsum, ",", "" );
  suma=replaceAll(rsum1, "$", "" );

  var anio = $("#anio").val();
  var band_umbral=false;
  var addtp=0; var addtpD=0;
  var umas_anexo=0;
  //var addtpHF=0; var umas_anexoH=0; 
  var umas_anexoTot_historico=0;
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      //var array = $.parseJSON(result);
      var array= JSON.parse(result);
      var aviso = parseFloat(array[0].aviso).toFixed(2);
      var siempre_avi=array[0].siempre_avi;
      var siempre_iden=array[0].siempre_iden;
      var identifica=array[0].identificacion;

      valor_uma=$("#valor_uma").val();
      valor_uma =  parseFloat(valor_uma).toFixed(2);
      /*console.log("monto: "+monto);
      console.log("valor_uma: "+valor_uma);*/
      umas_anexo=monto/valor_uma;
      umas_anexo = parseFloat(umas_anexo).toFixed(2);
      /*console.log("valor de umas_anexo final: "+umas_anexo);
      console.log("valor de umas aviso: "+aviso);*/

      umas_anexo = umas_anexo*1;
      aviso = aviso*1;
      //console.log("aviso: "+aviso);
      if(siempre_avi=="1"){
        band_umbral=true;
        $.ajax({ //cambiar pago a acusado
          type: 'POST',
          url : base_url+'Operaciones/acusaPago2',
          async: false,
          data: { id:id, act:$("#id_actividad").val()},
          success: function(result_divi){

          }
        });
      }else{
        if(umas_anexo>=aviso){
          band_umbral=true;
          $.ajax({ //cambiar pago a acusado
            type: 'POST',
            url : base_url+'Operaciones/acusaPago2',
            async: false,
            data: { id:id, act:$("#id_actividad").val()},
            success: function(result_divi){

            }
          });
        }
      }
      id_acusado=0;
      if(umas_anexo<aviso){
        $.ajax({
          type: 'POST',
          url : base_url+'Operaciones/getOpera',
          async: false,
          data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id,id_perfilamiento: $("#id_perfilamiento").val() },
          success: function(result){
            var data= $.parseJSON(result);
            var datos = data.data;
            datos.forEach(function(element) {
              var addtpHF=0; var umas_anexoH=0;
              if(element.moneda!=0){ //divisa
                $.ajax({
                  type: 'POST',
                  url : base_url+'Divisas/getDivisaTipo',
                  async: false,
                  data: { tipo: element.moneda, fecha: element.fecha_recepcion },
                  success: function(result_divi){
                    valor_divi=parseFloat(result_divi);
                    tot_moDH = parseFloat(element.monto_operacion);
                    //console.log("valor2 de tot_moD: "+tot_moD);
                    var totalHD = tot_moDH*valor_divi;
                    totalHD = totalHD.toFixed(2);
                    addtpHF += Number(totalHD);

                    $.ajax({
                      type: 'POST',
                      url : base_url+'Umas/getUmasAnio',
                      async: false,
                      data: { anio: element.fecha_recepcion},
                      success: function(data2){
                        valor_uma=parseFloat(data2).toFixed(2);
                        //console.log("valor de uma en historico: "+valor_uma);
                        precalc= addtpHF/valor_uma;
                        if(precalc>=identifica){
                          umas_anexoH += parseFloat(addtpHF/valor_uma);
                        }
                      }
                    });

                  }
                });
              }//if de divisa
              else{
                tot_moH = parseFloat(element.monto_operacion);
                totalHD = tot_moH.toFixed(2);
                addtpHF += Number(totalHD); 

                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  async: false,
                  data: { anio: element.fecha_recepcion},
                  success: function(data2){
                    valor_uma=parseFloat(data2).toFixed(2);
                    //console.log("valor de uma en historico: "+valor_uma);
                    precalc= addtpHF/valor_uma;
                      if(precalc>=identifica){
                        umas_anexoH += parseFloat(addtpHF/valor_uma);
                      }
                  }
                });
              }
              addtpHF=parseFloat(addtpHF).toFixed(2);
              umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
              umas_anexoH=umas_anexoH*1;
              //console.log("valor de umas_anexoH: "+umas_anexoH);
              if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){ 
              //if(identifica<=umas_anexoH && identifica!="" && identifica>0){
                if(umas_anexoH>=identifica)
                  umas_anexoTot_historico = umas_anexo + umas_anexoH;
                else
                  umas_anexoTot_historico = umas_anexoH;

                //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                if(umas_anexoTot_historico>=aviso){
                  $.ajax({ //cambiar pago a acusado
                    type: 'POST',
                    url : base_url+'Operaciones/acusaPago',
                    async: false,
                    data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1},
                    success: function(result2){
                      id_acusado=result2;
                      //console.log("acusado: "+id_acusado);
                    }
                  });
                }
              }
              /*else{
                umas_anexoTot_historico = umas_anexo;
              }*/

              //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
              if(umas_anexoTot_historico>=aviso){
                band_umbral=true;
              }
            });//foreach
          }//success de  get opera
        });
      }

      //console.log("band_umbral: "+band_umbral);
      if (suma==monto){
        validar_xml=1;
      }else{
        validar_xml=0;
      }
      //console.log("validar_xml: "+validar_xml);
      setTimeout(function () {  
        if(band_umbral==true && band_umbral!=undefined && !isNaN(umas_anexo)){
          if($("#aviso").val()>0){
            window.location.href = base_url+"TransaccionAn10/anexo10_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
          }else{
            window.location.href = base_url+"TransaccionAn10/anexo10_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
          }
          swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
          /*if(history.back()!=undefined){
            setTimeout(function () { history.back() }, 1500);
          }*/
          if($("#aviso").val()>0){
            setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
          }else{
            setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
          }
        }else{
          swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
          /*if(history.back()!=undefined){
            setTimeout(function () { history.back() }, 1500);
          }*/
          if($("#aviso").val()>0){
            setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
          }else{
            setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
          }
        }
      }, 1500); //esperar a la sumatoria
    }//success  
  });  //ajax
      // body... 
}


function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}


function cambiaCP(){
    var itemc = [];
    $('#colonia').select2({
      placeholder: {
          id: 0,
          text: 'Buscar colonia:'
      },
      width: '100%',
      allowClear: true,
      data: itemc
    });
    autoComplete("colonia");
}
function autoComplete(idcol){
  cp = $("#cp").val(); 
  col = $('#colonia').val();  
  $('#colonia').select2({
      width: 'resolve',
      minimumInputLength: 0,
      minimumResultsForSearch: 10,
      placeholder: 'Seleccione una colonia:',
      ajax: {
          url: base_url+'Perfilamiento/getDatosCPSelect',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  cp: cp, 
                  col: col ,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.colonia,
                      text: element.colonia,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}

function regresar(){
   //history.back();
   if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}

// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}