var base_url = $('#base_url').val();
var validar_xml=0;
var valCurp=false;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var siempre_avi;
$(document).ready(function(){
  var status = $('.status').text(); 
  $("#regresa").on("click", function() {
    //history.back();
    if(history.back()!=undefined)
      setTimeout(function () { history.back() }, 1500);
    else
      window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
  });
  var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#monto_opera').val());
  $('#monto_opera').val(totalg);
  
  //verificarConfigLimite();
  setTimeout(function () { 
    confirmarPagosMonto();
    //verificarLimite();
  }, 1500);
  
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false); 
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500);  

  $("#instrum_notario").on("change",function(){
    //verificarConfigLimite();
  });
  $("#monto").on('change',function(){
    //console.log("cambia monto");
    /*var min = true;
    var addtp = 0; i=0;
    $(".monto").each(function() {
        totalmonto = replaceAll($(this).val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        //totalmonto = Math.trunc(totalmonto);
        var vstotal = totalmonto;
        addtp += Number(vstotal);
        //addtp_min = $(".porcentaje_b");

    });
    monto_opera = replaceAll($('#monto_opera').val(), ",", "" );
    monto_operan = replaceAll(monto_opera, "$", "" );
    var monto_operantt = monto_operan;
    var monto_ope = Number(monto_operantt);

    total_liquida = replaceAll($('#total_liquida').val(), ",", "" );
    total_liquida = replaceAll(total_liquida, "$", "" );
    total_liquida = parseFloat(total_liquida);

    if(monto_ope==total_liquida){
       validar_xml=1;
       $('.validacion_cantidad').html('');
    }else{
       validar_xml=0;
       $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
    }*/
    //var comparara_ddtp = addtp;
    //var comparara_monto_opera= $('#monto_opera').val();
    //alert(comparara_monto_opera);
    //var monto = parseFloat($("#monto").val()).toFixed(2);
    totalmontov = replaceAll($("#monto").val(), ",", "" );
    totalmontov = replaceAll(totalmontov, "$", "" );
    //totalmontov = Math.trunc(totalmontov);

    var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalmontov);
    //console.log("monto para cambiar formato a moneda: "+monto);
    $("#monto").val(monto);
    //console.log("addtp: "+addtp);
    
    /*var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(addtp);
    $("#total_liquida").val(totalg);
    tipoPagoNvo();*/
  });

  $("#form_anexo8").keypress(function(e) {
    if (e.which == 13) {
        return false;
    }
  });
  $("#forma_pago").keypress(function(e) {
    if (e.which == 13) {
        return false;
    }
  });

  verificaTama();

  $('#vehiculo').click(function(event) {
    $("#cont_terre").show("slow");
    $("#cont_mari").hide("slow");
    $("#cont_aereo").hide("slow");
  });
  $('#vehiculo_terr').click(function(event) {
    $("#cont_terre").hide("slow");
    $("#cont_mari").show("slow");
    $("#cont_aereo").hide("slow");
  });
  $('#vehiculo_aereo').click(function(event) {
    $("#cont_terre").hide("slow");
    $("#cont_mari").hide("slow");
    $("#cont_aereo").show("slow");
  });

  if($("#id").val()>0){
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/records_table",
      data:  { "tabla": "anexo_8", "id": $("#id").val() },
      success: function (data) {
        var json = $.parseJSON(data);
        if(json[0].tipo_vehiculo==1){
          $("#cont_terre").show("slow");
        }
        if(json[0].tipo_vehiculo==2){
          $("#cont_mari").show("slow");
        }
        if(json[0].tipo_vehiculo==3){
          $("#cont_aereo").show("slow");
        }
      }
    });
    validarFechasPago();
  }

  if($("#idtransbene").val()>0){ //comprobar de otra manera, y agregar a morales
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/verificarBene",
      data: { "idtb": $("#idtransbene").val() },
      success: function (data) {
        //console.log(data);
        var json = $.parseJSON(data);
        //console.log(json.data[0].bene_moral);
        if(json.tipo_persona==1){
          $("#cont_benef").html("<h4>Beneficiario:</h4><br><br>Nombre: "+json.data.nombre +"<br>Apellido Paterno: "+json.data.apellido_paterno+" <br>Apellido Materno: "+json.data.apellido_materno);
        }
        if(json.tipo_persona==2){
          //console.log('persona moral');
          $("#cont_benef").html("<h4>Beneficiario:</h4><br><br>Nombre: "+json.data[0].bene_moral);
        }
      }
    });
  }

  $('#btn_submit').click(function(event){
      var form_register = $('#form_anexo8');
      var error_register = $('.alert-danger', form_register);
      var success_register = $('.alert-success', form_register);
      if($("#vehiculo").is(":checked")==true){
        $('#form_anexo8').removeData('validator');
        var rules_2="";
        if($("#num_vehit").val()=="" && $("input['name=num_reg_pubt']").val()=="" && $("input['name=matriculat']").val()==""){
          rules_2="num_vehit:{\
            required: true\
          },\
          num_reg_pubt:{\
            required: true\
          },\
          matriculat:{\
            required: true\
          }";
        }
        if($("#num_vehit").val()!="" && $("input[name*='num_reg_pubt']").val()=="" && $("input[name*='matriculat']").val()==""){
          rules_2="num_vehit:{\
            required: true\
          },";
        }
        if($("#num_vehit").val()=="" && $("input[name*='num_reg_pubt']").val()!="" && $("input[name*='matriculat']").val()==""){
          rules_2="num_reg_pubt:{\
            required: true\
          },";
        }
        if($("#num_vehit").val()=="" && $("input[name*='num_reg_pubt']").val()=="" && $("input[name*='matriculat']").val()!=""){
          rules_2="matriculat:{\
            required: true\
          },";
        }

        var $validator1=form_register.validate({ 
          errorElement: 'div', //default input error message container
          errorClass: 'vd_red', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          ignore: "",
          rules: {
              fecha_opera:{
                required: true
              },
              referencia:{
                required: true
              },
              monto_opera:{
                required: true
              },
              anio_acuse:{
                required: true
              },
              mes_acuse:{
                required: true
              },
              num_factura:{
                required: true
              },
              tipo_opera:{
                required: true
              },
              tipo_vehiculo:{
                required: true
              },
              marcat:{
                required: true
              },
              modelot:{
                required: true
              },
              aniot:{
                required: true
              },
              /*num_vehit:{
                required: true
              },
              num_reg_pubt:{
                required: true
              },
              matriculat:{
                required: true
              },*/
              //rules_2,
              blindajet:{
                required: true
              }
          },
          errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                  element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                  error.insertAfter(element.parent());
              }else {
                  error.insertAfter(element);
              }
          }, 
          
          invalidHandler: function (event, validator) { //display error alert on form submit              
                  success_register.fadeOut(500);
                  error_register.fadeIn(500);
                  scrollTo(form_register,-100);

          },

          highlight: function (element) { // hightlight error inputs
      
              $(element).addClass('vd_bd-red');
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

          },

          unhighlight: function (element) { // revert the change dony by hightlight
              $(element)
                  .closest('.control-group').removeClass('error'); // set error class to the control group
          },

          success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
          }
        });
      }
      else if($("#vehiculo_terr").is(":checked")==true){
        $('#form_anexo8').removeData('validator');
        var $validator1=form_register.validate({ //para peps
          errorElement: 'div', //default input error message container
          errorClass: 'vd_red', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          ignore: "",
          rules: {
              fecha_opera:{
                required: true
              },
              referencia:{
                required: true
              },
              monto_opera:{
                required: true
              },
              anio_acuse:{
                required: true
              },
              mes_acuse:{
                required: true
              },
              num_factura:{
                required: true
              },
              tipo_opera:{
                required: true
              },
              tipo_vehiculo:{
                required: true
              },
              marcam:{
                required: true
              },
              modelom:{
                required: true
              },
              aniom:{
                required: true
              },
              num_seriem:{
                required: true
              },
              banderam:{
                required: true
              },
              matriculam:{
                required: true
              },
              nivel_blindajem:{
                required: true
              }
          },
          errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                  element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                  error.insertAfter(element.parent());
              }else {
                  error.insertAfter(element);
              }
          }, 
          
          invalidHandler: function (event, validator) { //display error alert on form submit              
                  success_register.fadeOut(500);
                  error_register.fadeIn(500);
                  scrollTo(form_register,-100);

          },

          highlight: function (element) { // hightlight error inputs
      
              $(element).addClass('vd_bd-red');
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

          },

          unhighlight: function (element) { // revert the change dony by hightlight
              $(element)
                  .closest('.control-group').removeClass('error'); // set error class to the control group
          },

          success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
          }
        });
      }else if($("#vehiculo_aereo").is(":checked")==true){
        $('#form_anexo8').removeData('validator');
        var $validator1=form_register.validate({ //
          errorElement: 'div', //default input error message container
          errorClass: 'vd_red', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          ignore: "",
          rules: {
              fecha_opera:{
                required: true
              },
              referencia:{
                required: true
              },
              monto_opera:{
                required: true
              },
              anio_acuse:{
                required: true
              },
              mes_acuse:{
                required: true
              },
              num_factura:{
                required: true
              },
              tipo_opera:{
                required: true
              },
              tipo_vehiculo:{
                required: true
              },
              marcaa:{
                required: true
              },
              modeloa:{
                required: true
              },
              anioa:{
                required: true
              },
              num_seriea:{
                required: true
              },
              bandera_aerea:{
                required: true
              },
              matricula_aerea:{
                required: true
              },
              nivel_blindaje_a:{
                required: true
              }
          },
          errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                  element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                  error.insertAfter(element.parent());
              }else {
                  error.insertAfter(element);
              }
          }, 
          
          invalidHandler: function (event, validator) { //display error alert on form submit              
                  success_register.fadeOut(500);
                  error_register.fadeIn(500);
                  scrollTo(form_register,-100);

          },

          highlight: function (element) { // hightlight error inputs
      
              $(element).addClass('vd_bd-red');
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

          },

          unhighlight: function (element) { // revert the change dony by hightlight
              $(element)
                  .closest('.control-group').removeClass('error'); // set error class to the control group
          },

          success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
          }
        });
      }

      var valid = form_register.valid();
      if(valid) {
          if($("#fecha_opera").val()!="" && $("#tipo_opera").val()!=""){
            totot1 = replaceAll($("#monto_opera").val(), ",", "" );
            monto_opera = replaceAll(totot1, "$", "" );
            $.ajax({
                 type: "POST",
                 url: base_url+"index.php/Transaccion/submit",
                 data: $('#form_anexo8').serialize()+"&id_actividad="+$("#id_actividad").val()+"&monto="+$("#monto").val()+"&monto_opera="+monto_opera,
                 beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                 },
                 success: function (result) {
                    //console.log(result);
                    var array = $.parseJSON(result);
                    var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                    var total_monto=0;
                    $("#btn_submit").attr("disabled",false);
                    if ($(".cont_padre #form_pagos > .cont_hijo").length>0 && array.ok=='ok') {
                      //console.log("guarda pagos de liquidacion");
                      var DATA  = [];
                      var TABLA   = $(".cont_padre #form_pagos > .cont_hijo");                  
                      TABLA.each(function(){         
                          item = {};
                          item ["id"] = $(this).find("input[id*='id_pago']").val();
                          item ["fecha_pago"] = $(this).find("input[id*='fecha_pago']").val();
                          item ["forma_pago"] = $(this).find("select[id*='forma_pago'] option:selected").val();
                          item ["instrum_notario"] = $(this).find("select[id*='instrum_notario'] option:selected").val();
                          item ["tipo_moneda"] = $(this).find("select[id*='tipo_moneda'] option:selected").val();
                          //item ["monto"] = $(this).find("input[id*='monto']").val();
                          totot1 = replaceAll($(this).find("input[id*='monto']").val(), ",", "" );
                          monto = replaceAll(totot1, "$", "" );
                          //monto = Math.trunc(monto);
                          item ["monto"] = monto;
                          item ["id_anexo_8"] = id;
                          item ["idtransbene"] = $("#idtransbene").val();
                          item ["id_clientec_transac"] = id_clientec_transac;
                          item ["idperfilamiento_cliente"] = $("#idperfilamiento_cliente").val();
                          //if(monto>0 && $(this).find("input[id*='fecha_pago']").val()!="")
                            DATA.push(item);
                      });
                      var TABLA   = $(".cont_padre > .cont_hijo_clone");                  
                      TABLA.each(function(){         
                          item = {};
                          item ["id"] = $(this).find("input[id*='id_pago']").val();
                          item ["fecha_pago"] = $(this).find("input[id*='fecha_pago']").val();
                          item ["forma_pago"] = $(this).find("select[id*='forma_pago'] option:selected").val();
                          item ["instrum_notario"] = $(this).find("select[id*='instrum_notario'] option:selected").val();
                          item ["tipo_moneda"] = $(this).find("select[id*='tipo_moneda'] option:selected").val();
                          //item ["monto"] = $(this).find("input[id*='monto']").val();
                          totot1 = replaceAll($(this).find("input[id*='monto']").val(), ",", "" );
                          monto = replaceAll(totot1, "$", "" );
                          //monto = Math.trunc(monto);
                          item ["monto"] = monto;
                          item ["id_anexo_8"] = id;
                          item ["idtransbene"] = $("#idtransbene").val();
                          item ["id_clientec_transac"] = id_clientec_transac;
                          DATA.push(item);
                      });
                      var TABLA   = $(".cont_padre #form_pagos > .cont_hijoExist");                  
                      TABLA.each(function(){        
                          item = {};
                          item ["id"] = $(this).find("input[id*='id_pago']").val();
                          item ["fecha_pago"] = $(this).find("input[id*='fecha_pago']").val();
                          item ["forma_pago"] = $(this).find("select[id*='forma_pago'] option:selected").val();
                          item ["instrum_notario"] = $(this).find("select[id*='instrum_notario'] option:selected").val();
                          item ["tipo_moneda"] = $(this).find("select[id*='tipo_moneda'] option:selected").val();
                          //item ["monto"] = $(this).find("input[id*='monto']").val();
                          totot1 = replaceAll($(this).find("input[id*='monto']").val(), ",", "" );
                          monto = replaceAll(totot1, "$", "" );
                          //monto = Math.trunc(monto);
                          item ["monto"] = monto;
                          item ["id_anexo_8"] = id;
                          item ["idtransbene"] = $("#idtransbene").val();
                          item ["id_clientec_transac"] = id_clientec_transac;
                          DATA.push(item);
                      });
                      INFO  = new FormData();
                      aInfo   = JSON.stringify(DATA);
                      INFO.append('data', aInfo);
                      $.ajax({
                          data: INFO,
                          //data: INFO+"&idperfilamiento_cliente="+$("#idperfilamiento_cliente").val()
                          type: 'POST',
                          url : base_url+'index.php/Transaccion/submitPagos',
                          processData: false, 
                          contentType: false,
                          async: false,
                          statusCode:{
                              404: function(data2){
                                  //toastr.error('Error!', 'No Se encuentra el archivo');
                              },
                              500: function(){
                                  //toastr.error('Error', '500');
                              }
                          },
                          success: function(data2){
                            //console.log('monto en success que trae ctrl: '+data2);
                            total_monto = data2;
                          }
                      }); 
                    }else{
                        //console.log("sin datos de pago");
                    }

                    //console.log("total_monto: "+total_monto);
                    monto_opera = replaceAll($('#monto_opera').val(), ",", "" );
                    monto_operan = replaceAll(monto_opera, "$", "" );
                    var monto_operantt = monto_operan;
                    var monto_ope = Number(monto_operantt);

                    total_liquida = replaceAll($('#total_liquida').val(), ",", "" );
                    total_liquida = replaceAll(total_liquida, "$", "" );
                    total_liquida = parseFloat(total_liquida);
                    
                    if(monto_ope==total_liquida){
                       validar_xml=1;
                       $('.validacion_cantidad').html('');
                    }else{
                       validar_xml=0;
                       $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
                    }

                    ValidLimitByUmas(id);

                    //if(total_monto>=557770){
                    
                 }
             
            });
            //return false; // required to block normal submit for ajax
          }else{
            swal("Error!", "Ingrese fecha y/o tipo de operación", "warning");
          }
      }

    });
  ComboAnio();
  $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
  $("#monto_opera").on('change',function(){
    //console.log("cambia de monto");
    var monto_operacion = $("input[id*='monto_opera']").val();
    if(monto_operacion.indexOf('$') != -1){
      /*tot_mo = replaceAll(monto_operacion, ",", "" );
      tot_mo = replaceAll(tot_mo, "$", "" );*/
    }else{
      var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#monto_opera').val());
      $('#monto_opera').val(totalg);
    }
    
    confirmarPagosMonto();
  });

  /*verificarLimite();
  verificarConfigLimite();*/

  setTimeout(function () { 
    validaFormaPagoCalc(); //agregado para alerta
  }, 1000); 

  /*setTimeout(function () { 
    confirmarPagosMonto();
  }, 1500); */

  $("#fecha_opera").on("change",function(){
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();

  //validar fechas 
  $("#forma_pago").on("change",function(){
    validarFechasPago();
  });
  $("#instrum_notario").on("change",function(){
    validarFechasPago();
  });
  $("#tipo_moneda").on("change",function(){
    validarFechasPago();
  });
  $("input[name*='monto']").on("change",function(){
    validarFechasPago();
  });

});

function validarFechasPago(){
  /*var TABLAP = $("#cont_gral > div");                  
  TABLAP.each(function(){         
    fecha_pago_liquidas=$(this).find("input[id*='fecha_pago']").val();
    console.log("fecha_pago_liquidas: "+fecha_pago_liquidas);
    let result = moment(fecha_pago_liquidas, 'YYYY-MM-DD',true).isValid();
    if(fecha_pago_liquidas!="" && result == true){
      $("#btn_submit").attr("disabled",false);
    }else{
      $("#btn_submit").attr("disabled",true);
      swal("¡Alerta!", "Fecha de pago incorrecta, favor de ingresar una fecha valida", "error");
    }
  });*/
}

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

var idtipo_cliente=0;
var clasificacion=0;
var preg1=0;
var trans_forma=0;

function ValidLimitByUmas(id){
  idrecibe = id;
  var anio = $("#anio").val();
  var valor_uma; var band_umbral=false;
  var addtp=0; var addtpD=0; var addtp2=0; var addtpD2=0; var addtp3=0; var addtpD3=0; var umas_anexocom=0;
  var act = new Date();
  var mes_act = act.getMonth();
  var anio_act = act.getFullYear();
  var umas_anexoTot=0; var addtpF=0; var umas_anexo=0; var umas_anexo2=0; var umas_anexo3=0; var umas_anexo4=0; var umas_anexo5=0; var umas_anexo6=0;
  //var addtpHF=0; var umas_anexoH=0;
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    async: false,
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      //var array = $.parseJSON(result);
      var array= JSON.parse(result);
      var aviso = parseFloat(array[0].aviso).toFixed(2);
      var siempre_avi=array[0].siempre_avi;
      var siempre_iden=array[0].siempre_iden;
      var identifica=array[0].identificacion;
      //console.log("siempre avi: "+siempre_avi);
      cont=0;

      var TABLA = $(".form_pagos_padre > .cont_hijo");   
      TABLA.each(function(){    
        //console.log("recorre tabla de pagos cont_hijo");    
        // pesos de Liquidación 
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
          //console.log("pesos cont_hijo");
          var monto_operacion = $(this).find("input[id*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          if(monto_operacion.indexOf('$') != -1){
            tot_mo = replaceAll(monto_operacion, ",", "" );
            tot_mo = replaceAll(tot_mo, "$", "" );
          }else{
            tot_mo=monto_operacion;
          }
          tot_mo = parseFloat(tot_mo);
          var vstotal1 = tot_mo;
          /*let date = new Date(fecha_pago);
          var mes_pago = date.getMonth();
          mes_pago = parseInt(mes_pago);
          mes_pago = mes_pago +1;
          var anio_pago = date.getFullYear();*/
          //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
            addtp += Number(vstotal1);
          //}
          addtpF += addtp;
          console.log("valor1 addtp: "+addtp);
          //addtp += parseFloat(vstotal1);
          //if(addtp>0){
          if(fecha_pago!=""){
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              async: false,
              data: { anio: fecha_pago},
              success: function(data2){
                valor_uma=parseFloat(data2).toFixed(2);
                //console.log("valor uma en function umasAnio: "+valor_uma);
                umas_anexo = parseFloat(addtp/valor_uma);
                umas_anexocom = parseFloat(addtp/valor_uma);
                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                //console.log("valor1 de umas_anexo: "+umas_anexo);
                if(umas_anexocom>=identifica){
                  umas_anexoTot = parseFloat(umas_anexocom);
                }
                //umas_anexoTot += parseFloat(umas_anexo);
              }
            });
          //}
          }

        }
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0" && $(this).find("input[id*='fecha_pago']").val()!=""){ //!= de pesos
          console.log("!pesos cont_hijo");
          //para sacar cant en pesos por tipo de divisa en liquidacion 
          var monto_operacion = $(this).find("input[id*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(), fecha:$(this).find("input[id*='fecha_pago']").val()},
            success: function(result_divi){
              valor_divi=parseFloat(result_divi);
              //$(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
              //console.log("monto operacion: "+monto_operacion);
              //console.log("valor_divi: "+valor_divi);
              if(monto_operacion.indexOf('$') != -1){
                tot_moD = replaceAll(monto_operacion, ",", "" );
                tot_moD = replaceAll(tot_moD, "$", "" );
              }else{
                tot_moD = monto_operacion;
              }
              tot_moD = parseFloat(tot_moD);
              //console.log("valor2 de tot_moD: "+tot_moD);
              var vstotal1D = tot_moD*valor_divi;
              //console.log("valor2 de vstotal1D: "+vstotal1D);
              /*let date = new Date(fecha_pago);
              var mes_pago = date.getMonth();
              mes_pago = parseInt(mes_pago);
              mes_pago = mes_pago +1;
              var anio_pago = date.getFullYear();*/
              //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                addtpD += Number(vstotal1D);
              //}
              addtpF += addtpD;
              //addtpD += parseFloat(vstotal1D);
              //console.log("valor2 de addtpD: "+addtpD);
              //if(addtpD>0){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  async: false,
                  data: { anio: fecha_pago},
                  success: function(data2){
                    valor_uma=parseFloat(data2).toFixed(2);
                    //console.log("valor uma en function umasAnio: "+valor_uma);
                    umas_anexo2 = parseFloat(addtpD/valor_uma);
                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                    //console.log("valor2 de umas_anexo: "+umas_anexo);
                    if(umas_anexo2>=identifica){
                      umas_anexoTot = parseFloat(umas_anexo2);
                    }
                    //umas_anexoTot += parseFloat(umas_anexo2);
                  }
                });
              //}
            }
          });
        }
        contta++;
      });
      var TABLA2 = $(".cont_padre .form_pagos > .cont_hijoExist"); 
      TABLA2.each(function(){    
        //console.log("recorre tabla de pagos cont_hijoExist");    
        // pesos de Liquidación 
        //cont++;
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
          //console.log("pesos cont_hijoExist");
          var monto_operacion = $(this).find("input[id*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          var acusado = $(this).find("input[id*='acusado']").val();
          console.log("acusado: "+acusado);
          if(monto_operacion.indexOf('$') != -1){
            tot_mo = replaceAll(monto_operacion, ",", "" );
            tot_mo = replaceAll(tot_mo, "$", "" );
          }else{
            tot_mo=monto_operacion;
          }
          tot_mo = parseFloat(tot_mo).toFixed(2);
          var vstotal1 = tot_mo;
          console.log("valor2 tot_mo: "+tot_mo);
          console.log("valor2 vstotal1: "+vstotal1);
          /*let date = new Date(fecha_pago);
          var mes_pago = date.getMonth();
          mes_pago = parseInt(mes_pago);
          mes_pago = mes_pago +1;
          var anio_pago = date.getFullYear();*/
          //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
          if(acusado==0){ //no acusado
            addtp2 += Number(vstotal1);
          }
          //}
          addtpF += addtp2;
          //addtp += parseFloat(vstotal1);
          //addtp2 = parseFloat(addtp2).toFixed(2);
          console.log("valor2 addtp2: "+addtp2);
          //if(addtp>0){
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              async: false,
              data: { anio: fecha_pago},
              success: function(data2){
                valor_uma=parseFloat(data2).toFixed(2);
                console.log("valor3 uma en function umasAnio: "+valor_uma);
                umas_anexo3 = parseFloat(addtp2/valor_uma);
                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                //console.log("valor3 de umas_anexo: "+umas_anexo3);
                if(umas_anexo3>=identifica){
                  umas_anexoTot = parseFloat(umas_anexo3);
                }
                //umas_anexoTot += parseFloat(umas_anexo3);
                console.log("valor3 de umas_anexoTot : "+umas_anexoTot);
              }
            });
          //}
        }
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0" && $(this).find("input[id*='fecha_pago']").val()!=""){ //!= de pesos
          //console.log("!pesos cont_hijoExist");
          //para sacar cant en pesos por tipo de divisa en liquidacion 
          contta++;
          var monto_operacion = $(this).find("input[id*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          var acusado = $(this).find("input[id*='acusado']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(), fecha:$(this).find("input[id*='fecha_pago']").val()},
            success: function(result_divi){
              valor_divi=parseFloat(result_divi);
              //$(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
              //console.log("monto operacion: "+monto_operacion);
              //console.log("valor_divi: "+valor_divi);
              if(monto_operacion.indexOf('$') != -1){
                tot_moD = replaceAll(monto_operacion, ",", "" );
                tot_moD = replaceAll(tot_moD, "$", "" );
              }else{
                tot_moD = monto_operacion;
              }
              tot_moD = parseFloat(tot_moD);
              //console.log("valor2 de tot_moD: "+tot_moD);
              var vstotal1D = tot_moD*valor_divi;
              //console.log("valor2 de vstotal1D: "+vstotal1D);
              /*let date = new Date(fecha_pago);
              var mes_pago = date.getMonth();
              mes_pago = parseInt(mes_pago);
              mes_pago = mes_pago +1;
              var anio_pago = date.getFullYear();*/
              //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
              if(acusado==0){ //no acusado
                addtpD2 += Number(vstotal1D);
              }
              addtpF += addtpD2;
              //}
              //addtpD += parseFloat(vstotal1D);
              console.log("valor2 de addtpD: "+addtpD2);
              //if(addtpD>0){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  async: false,
                  data: { anio: fecha_pago},
                  success: function(data2){
                    valor_uma=parseFloat(data2).toFixed(2);
                    //console.log("valor uma en function umasAnio: "+valor_uma);
                    umas_anexo4 = parseFloat(addtpD2/valor_uma);
                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                    //console.log("valor4 de umas_anexo: "+umas_anexo4);
                    if(umas_anexo4>=identifica){
                      umas_anexoTot = parseFloat(umas_anexo4);
                    }
                    //umas_anexoTot += parseFloat(umas_anexo4);
                  }
                });
              //}
            }
          });
        }
        
      });
      var TABLA3 = $(".cont_padre > .cont_hijo_clone"); 
      TABLA3.each(function(){    
        //console.log("recorre tabla de pagos cont_hijo_clone");    
        // pesos de Liquidación 
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
          var monto_operacion = $(this).find("input[id*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          if(monto_operacion.indexOf('$') != -1){
            tot_mo = replaceAll(monto_operacion, ",", "" );
            tot_mo = replaceAll(tot_mo, "$", "" );
          }else{
            tot_mo=monto_operacion;
          }
          tot_mo = parseFloat(tot_mo);
          var vstotal1 = tot_mo;
          /*let date = new Date(fecha_pago);
          var mes_pago = date.getMonth();
          mes_pago = parseInt(mes_pago);
          mes_pago = mes_pago +1;
          var anio_pago = date.getFullYear();*/
          //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
            addtp3 += Number(vstotal1);
          //}
          addtpF += addtp3;
          //addtp += parseFloat(vstotal1);
          //if(addtp>0){
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              async: false,
              data: { anio: fecha_pago},
              success: function(data2){
                valor_uma=parseFloat(data2).toFixed(2);
                //console.log("valor uma en function umasAnio: "+valor_uma);
                umas_anexo5 = parseFloat(addtp3/valor_uma);
                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                console.log("valor5 de umas_anexo: "+umas_anexo5);
                if(umas_anexo5>=identifica){
                  umas_anexoTot = parseFloat(umas_anexo5);
                }
                //umas_anexoTot += parseFloat(umas_anexo5);
              }
            });
          //}
        }
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0" && $(this).find("input[id*='fecha_pago']").val()!=""){ //!= de pesos
          //console.log("!pesos cont_hijo_clone");
          //para sacar cant en pesos por tipo de divisa en liquidacion 
          var monto_operacion = $(this).find("input[id*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(), fecha:$(this).find("input[id*='fecha_pago']").val()},
            success: function(result_divi){
              valor_divi=parseFloat(result_divi);
              //$(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
              //console.log("monto operacion: "+monto_operacion);
              //console.log("valor_divi: "+valor_divi);
              if(monto_operacion.indexOf('$') != -1){
                tot_moD = replaceAll(monto_operacion, ",", "" );
                tot_moD = replaceAll(tot_moD, "$", "" );
              }else{
                tot_moD = monto_operacion;
              }
              tot_moD = parseFloat(tot_moD);
              //console.log("valor2 de tot_moD: "+tot_moD);
              var vstotal1D = tot_moD*valor_divi;
              //console.log("valor2 de vstotal1D: "+vstotal1D);
              let date = new Date(fecha_pago);
              var mes_pago = date.getMonth();
              mes_pago = parseInt(mes_pago);
              mes_pago = mes_pago +1;
              var anio_pago = date.getFullYear();
              //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                addtpD3 += Number(vstotal1D);
              //}
              addtpF += addtpD3;
              //addtpD += parseFloat(vstotal1D);
              //console.log("valor2 de addtpD: "+addtpD);
              //if(addtpD>0){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  async: false,
                  data: { anio: fecha_pago},
                  success: function(data2){
                    valor_uma=parseFloat(data2).toFixed(2);
                    //console.log("valor uma en function umasAnio: "+valor_uma);
                    umas_anexo6 = parseFloat(addtpD3/valor_uma);
                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                    console.log("valor6 de umas_anexo: "+umas_anexo6);
                    if(umas_anexo6>=identifica){
                      umas_anexoTot = parseFloat(umas_anexo6);
                    }
                    //umas_anexoTot += parseFloat(umas_anexo6);
                  }
                });
              //}
            }
          });
        }
        contta++;
      });

      var time = cont*1500;
      setTimeout(function () { 
        if(lim_efec_band==true){
          swal("Alerta!", "El efectivo de la transaccion es mayor al limite permitido", "error");
          $('.limite_efect_msj').html('El efectivo(UMAS) de la transaccion es mayor al limite permitido.');
        }else{
          lim_efec_band=false;
          $('.limite_efect_msj').html('');
        }
        /*console.log("valor de umas_anexo: "+umas_anexo);
        console.log("valor de umas_anexo2: "+umas_anexo2);
        console.log("valor de umas_anexo3: "+umas_anexo3);
        console.log("valor de umas_anex4o: "+umas_anexo4);
        console.log("valor de umas_ane5xo: "+umas_anexo5);
        console.log("valor de umas_an6exo: "+umas_anexo6);*/

        //umas_anexoTot = parseFloat(umas_anexo)+parseFloat(umas_anexo2)+parseFloat(umas_anexo3)+parseFloat(umas_anexo4)+parseFloat(umas_anexo5)+parseFloat(umas_anexo6);
        umas_anexoTot = parseFloat(umas_anexoTot).toFixed(2);
        console.log("valor (umas_anexoTot) de umas_anexo final: "+umas_anexoTot);
        //console.log("valor de umas aviso: "+aviso);
        //addtpF = parseFloat(addtp) + parseFloat(addtpD) + parseFloat(addtp2) + parseFloat(addtpD2) + parseFloat(addtp3) + parseFloat(addtpD3);
        //console.log("valor de addtpF: "+addtpF);

        montoc = replaceAll(monto, "$", "" );
        montoc = replaceAll(montoc, ",", "" );
        montoc = parseFloat(montoc);
        //console.log("valor de montoc: "+montoc);
        //console.log("valor de umas_anexoTot: "+umas_anexoTot);

        umas_anexoTot = umas_anexoTot*1;
        aviso = parseFloat(aviso)*1;
        console.log("aviso: "+aviso);
        if(siempre_avi=="1"){
          band_umbral=true;
          $.ajax({ //cambiar pago a acusado
            type: 'POST',
            url : base_url+'Operaciones/acusaPago2',
            async: false,
            data: { id:id, act:$("#id_actividad").val()},
            success: function(result_divi){

            }
          });
        }else{
          if(umas_anexoTot>=aviso){
            band_umbral=true;
            $.ajax({ //cambiar pago a acusado
              type: 'POST',
              url : base_url+'Operaciones/acusaPago2',
              async: false,
              data: { id:idrecibe, act:11},
              success: function(result_divi){

              }
            });
          }
        }
        //traer pagos de otras transacciones pero del mismo anexo sin acusar
        id_acusado=0;
        umas_anexoTot_historico=0;
        if(umas_anexoTot<aviso){
          console.log("entra a if para traer pagos de historico: "+umas_anexoTot);
          $.ajax({
            type: 'POST',
            url : base_url+'Operaciones/getOpera',
            async: false,
            data: { aviso:0, id_opera: $("input[name*='idopera']").val(), id_act: 11, id_union:$("#id_union").val(), id_anexo:idrecibe, id_perfilamiento: $("#idperfilamiento_cliente").val() },
            success: function(result){
              var data= $.parseJSON(result);
              var datos = data.data;
              datos.forEach(function(element) {
                var addtpHF=0; var umas_anexoH=0;
                if(element.tipo_moneda!=0){ //divisa
                  $.ajax({
                    type: 'POST',
                    url : base_url+'Divisas/getDivisaTipo',
                    async: false,
                    data: { tipo: element.tipo_moneda, fecha: element.fecha_pago },
                    success: function(result_divi){
                      valor_divi=parseFloat(result_divi);
                      tot_moDH = parseFloat(element.monto);
                      //console.log("valor2 de tot_moD: "+tot_moD);
                      var totalHD = tot_moDH*valor_divi;
                      totalHD = totalHD.toFixed(2);
                      addtpHF += Number(totalHD);

                      $.ajax({
                        type: 'POST',
                        url : base_url+'Umas/getUmasAnio',
                        async: false,
                        data: { anio: element.fecha_pago},
                        success: function(data2){
                          valor_uma=parseFloat(data2).toFixed(2);
                          //console.log("valor de uma en historico: "+valor_uma);
                          precalc= addtpHF/valor_uma;
                          if(precalc>=identifica){
                            umas_anexoH += parseFloat(addtpHF/valor_uma);
                            //console.log("si es mayor o igual al valor de identificacion");
                          }else{
                            //console.log("es menor al valor de identificacion"); 
                          }
                        }
                      });

                    }
                  });
                }//if de divisa
                else{
                  tot_moH = parseFloat(element.monto);
                  totalHD = tot_moH.toFixed(2);
                  addtpHF += Number(totalHD); 

                  $.ajax({
                    type: 'POST',
                    url : base_url+'Umas/getUmasAnio',
                    async: false,
                    data: { anio: element.fecha_pago},
                    success: function(data2){
                      valor_uma=parseFloat(data2).toFixed(2);
                      //console.log("valor de uma en historico: "+valor_uma);
                      precalc= addtpHF/valor_uma;
                      if(precalc>=identifica){
                        umas_anexoH += parseFloat(addtpHF/valor_uma);
                        //console.log("si es mayor o igual al valor de identificacion");
                      }else{
                        //console.log("es menor al valor de identificacion");
                      }
                    }
                  });
                }
                addtpHF=parseFloat(addtpHF).toFixed(2);
                umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                umas_anexoH=umas_anexoH*1;
                console.log("addtpHF: "+addtpHF);
                console.log("identifica: "+identifica);
                console.log("umas_anexoTot: "+umas_anexoTot); //total de la transaccion actual
                console.log("umas_anexoH: "+umas_anexoH); //total de los pagos que pueden ayudar - historico

                if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){
                  //console.log("identifica menor o igual que historico: "+identifica);
                  if(umas_anexoH>=identifica)
                    umas_anexoTot_historico = umas_anexoTot + umas_anexoH;
                  else
                    umas_anexoTot_historico = umas_anexoTot;

                  //umas_anexoTot_historico = umas_anexoTot + umas_anexoH;
                  console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                  if(umas_anexoTot_historico>=aviso){
                    //band_umbral=true;
                    $.ajax({ //cambiar pago a acusado
                      type: 'POST',
                      url : base_url+'Operaciones/acusaPago',
                      async: false,
                      data: { id: element.id, act:11, id_anexo:idrecibe, aviso:0, pago_ayuda:1},
                      success: function(result2){
                        id_acusado=result2;
                        console.log("acusado: "+id_acusado);
                      }
                    });
                  }
                }
                /*else{
                  umas_anexoTot_historico = umas_anexoTot;
                }*/
                console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                if(umas_anexoTot_historico>=aviso){
                  band_umbral=true;
                }
              });//foreach
            }//success de  get opera
          });
        }
        console.log("band_umbral: "+band_umbral);

        /*if (addtpF==montoc){
          validar_xml=1;
        }else{
          validar_xml=0;
        }*/
      }, time);
      console.log("validar_xml: "+validar_xml);
      setTimeout(function () { 
          if(band_umbral==true && band_umbral!=undefined){
            if(validar_xml==1 && band_umbral==true && band_umbral===true && !isNaN(umas_anexoTot)){
              if($("#aviso").val()>0){
                window.location.href = base_url+"Transaccion/anexo8_xml/"+idrecibe+"/"+$('#idperfilamiento_cliente').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
              }else{
                window.location.href = base_url+"Transaccion/anexo8_xml/"+idrecibe+"/"+$('#idperfilamiento_cliente').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
              }
              
              swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
              /*if(history.back()!=undefined){
                setTimeout(function () { history.back() }, 1500);
              }*/
              if($("#aviso").val()>0){
                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
              }else{
                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
              }
            }else{
              swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
              /*if(history.back()!=undefined){
                setTimeout(function () { history.back() }, 1500);
              }*/
              if($("#aviso").val()>0){
                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
              }else{
                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
              }
            }
          }
          else{
            swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
            /*if(history.back()!=undefined){
              setTimeout(function () { history.back() }, 1500);
            }*/
            if($("#aviso").val()>0){
              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
            }else{
              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
            }
          }
      }, 2500); //esperar a la sumatoria

    }//success  
  });  //ajax
}
function validaFormaPagoCalc(){ //agregado para alerta
  $.ajax({
    type: 'POST',
    url : base_url+'Alertas/getGradoTrans',
    async: false,
    data: { ido: $("input[name*='idopera']").val(), ida:$("#id_actividad").val() },
    success: function(result){
      var data= $.parseJSON(result);
      data.forEach(function(element) {
        //console.log("transac forma: "+element.transaccionalidad_forma);
        idtipo_cliente=element.idtipo_cliente;
        clasificacion=element.clasificacion;
        preg1=element.preg1;
        trans_forma=element.transaccionalidad_forma;
      });      
    }
  });
}

function msjsAlerta(instrum_comp){
  /* ************ agregado para alertas **************** */
  /*console.log("idtipo_cliente: "+idtipo_cliente);
  console.log("clasificacion: "+clasificacion);
  console.log("preg1: "+preg1);
  console.log("trans_forma: "+trans_forma);
  console.log("instrum_comp: "+instrum_comp);*/
  if(idtipo_cliente==1 && clasificacion==16 || idtipo_cliente!=5){ //clientes No PEP
    if(trans_forma==1){ //lo que declaró en la calculadora
      if(instrum_comp!="10" && instrum_comp!="11" && instrum_comp!="12"){
        swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción", "warning");
      }
    }
    if(trans_forma==2){ //lo que declaró en la calculadora
      if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9"){
        //console.log("instrum_comp 2: "+instrum_comp);
        swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción", "warning");
      }
    }
    if(trans_forma==3){ //lo que declaró en la calculadora
      if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9" && instrum_comp!="10" && instrum_comp!="11"){
        swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción", "warning");
      }
    }
    if(trans_forma==4){ //lo que declaró en la calculadora
      if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9"){
        swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción", "warning");
      }
    }
    if(trans_forma==5 && instrum_comp==1){ //lo que declaró en la calculadora -- ajuste nvo en efectivo vs efectivo
      swal("Alerta", "Toda vez que el instrumento de pago es efectivo, se recomienda validar la congruencia de la información y documentación presentada y los montos en efectivo a recibir", "warning");
    }
    if(trans_forma==5 && instrum_comp!=1){ //lo que declaró en la calculadora - ajuste nvo
      swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es) de la transacción", "warning");
    }
  }
  /* ******************************CLIENTES PEP**************************************** */
  if(idtipo_cliente==1 && clasificacion!=16 || idtipo_cliente==5){ //clientes PEP
    if(preg1==1 && instrum_comp==1){ //lo que declaró en la calculadora - El vehículo se liquidará total o parcialmente en efectivo - ajuste nvo -efectivo vs efectivo
      swal("Alerta", "Toda vez que el instrumento de pago es efectivo, se recomienda validar la congruencia de la información y documentación presentada y los montos en efectivo a recibir", "warning");
    }
    if(preg1==1 && instrum_comp!=1){ //lo que declaró en la calculadora - El vehículo se liquidará total o parcialmente en efectivo - ajuste nvo
      swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es) de la transacción", "warning");
    }
    if(preg1==2){ //lo que declaró en la calculadora - El vehículo se liquidará con transferencia bancaria nacional
      if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9"){
        swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción", "warning");
      }
    }
    if(preg1==3){ //lo que declaró en la calculadora - El vehículo se liquidará con transferencia bancaria internacional
      if(instrum_comp!="10" && instrum_comp!="11" && instrum_comp!="12"){
        swal("Alerta", "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción", "warning");
      }
    }
  }
  /* *****************************************************/
}

function confirmarPagosMonto(){ //para sumar el efectivo nadamas
  suma_no_efect=0;
  suma_efect=0;
  suma_no_efect2=0;
  suma_efect2=0;
  suma_no_efect3=0;
  suma_efect3=0;
  contta=0;
  valor_divi=0;
  montoe=0;
  montone=0;
  cont=0;
  var band_alerta=false; var band_alerta_exist=false; var band_alerta_clone=false;
  var TABLA = $(".form_pagos_padre > .cont_hijo");   
  TABLA.each(function(){    
    /* ************ agregado para alertas **************** */
    instrum_comp=$(this).find("select[id*='instrum_notario'] option:selected").val();
    if($(this).find("input[id*='fecha_pago']").val()!="")
      msjsAlerta(instrum_comp);
    /* *****************************************************/

    //console.log("recorre tabla de pagos cont_hijo");    
    // pesos de Liquidación 
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
      //console.log("pesos cont_hijo");
      if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){ //en efectivo
        var monto = $(this).find("input[name*='monto']").val();
        if(monto.indexOf('$') != -1){
          montoe = replaceAll(monto, ",", "" );
          montoe = replaceAll(montoe, "$", "" );
        }else{
          montoe=parseFloat(monto);
        }
        montoe = parseFloat(montoe);
        suma_efect += parseFloat(montoe);

      }  
      else{
        var monto = $(this).find("input[name*='monto']").val();
        if(monto.indexOf('$') != -1){
          montone = replaceAll(monto, ",", "" );
          montone = replaceAll(montone, "$", "" );
        }else{
          montone=parseFloat(monto);
        }
        montone = parseFloat(montone);
        suma_no_efect += parseFloat(montone);
      }
      /*console.log("suma_nefect pesos: "+suma_efect);
      console.log("suma_no_efect pesos: "+suma_no_efect);*/
    }
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){ //!= de pesos
      //console.log("!pesos cont_hijo");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      monto_opera = $(this).find("input[name*='monto']").val();
      //console.log("monto (de divisa) capturada: "+monto_opera);
      var inst = $(this).find("select[id*='instrum_notario'] option:selected").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago']").val()},
        success: function(result_divi){
          valor_divi=parseFloat(result_divi);
          console.log(" valor_divi:" +valor_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = replaceAll(monto_opera, ",", "" );
              tot_moDE = replaceAll(tot_moDE, "$", "" );
            }else{
              tot_moDE = parseFloat(monto_opera);
            }
            tot_moDE = parseFloat(tot_moDE);
            console.log("tot_moDE hijo:" +tot_moDE);
            var preefect = tot_moDE*valor_divi;
            console.log("preefect hijo:" +preefect);
            suma_efect += parseFloat(preefect);
            console.log("suma_efect hijo:" +suma_efect);
          }else{
            if(monto_opera.indexOf('$') != -1){
              montone = replaceAll(monto_opera, ",", "" );
              montone = replaceAll(montone, "$", "" );
            }else{
              montone=parseFloat(monto_opera);
            }
            var montone = montone*valor_divi;
            suma_no_efect += parseFloat(montone);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
    }
    contta++;
  });
  var TABLA2 = $(".cont_padre .form_pagos > .cont_hijoExist"); 
  TABLA2.each(function(){    
    /* ************ agregado para alertas **************** */
    instrum_comp=$(this).find("select[id*='instrum_notario'] option:selected").val();
    if($(this).find("input[id*='fecha_pago']").val()!="")
      msjsAlerta(instrum_comp);
    /* *****************************************************/
    //console.log("recorre tabla de pagos cont_hijoExist");    
    // pesos de Liquidación 
    //cont++;
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
      //console.log("pesos cont_hijoExist");
      if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){ //en efectivo
        var monto2 = $(this).find("input[name*='monto']").val();
        console.log("monto pesos exist: "+$(this).find("input[name*='monto']").val());
        if(monto2.indexOf('$') != -1){
          montoe2 = replaceAll(monto2, ",", "" );
          montoe2 = replaceAll(montoe2, "$", "" );
        }else{
          montoe2=parseFloat(monto2);
        }
        suma_efect2 += parseFloat(montoe2);
        console.log("suma_efect2 pesos:" +suma_efect2);
        //console.log("contta:" +contta);
      }  
      else{
        var monto2 = $(this).find("input[name*='monto']").val();
        if(monto2.indexOf('$') != -1){
          montone2 = replaceAll(monto2, ",", "" );
          montone2 = replaceAll(montone2, "$", "" );
        }else{
          montone2=parseFloat(monto2);
        }
        suma_no_efect2 += parseFloat(montone2);
      }
      /*console.log("suma_nefect pesos: "+suma_efect);
      console.log("suma_no_efect pesos: "+suma_no_efect);*/
    }
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){ //!= de pesos
      //console.log("!pesos cont_hijoExist");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      contta++;
      console.log("contta: "+contta);
      monto_opera2 = $(this).find("input[name*='monto']").val();
      console.log("monto divisas exist: "+monto_opera2);
      var inst = $(this).find("select[id*='instrum_notario'] option:selected").val(); //es para saber si son efectivo o no

      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago']").val()},
        success: function(result_divi){
          valor_divi=parseFloat(result_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera2.indexOf('$') != -1){
              tot_moDE2 = replaceAll(monto_opera2, ",", "" );
              tot_moDE2 = replaceAll(tot_moDE2, "$", "" );
            }else{
              tot_moDE2 = parseFloat(monto_opera2);
            }
            var preefect2 = tot_moDE2*valor_divi;
            suma_efect2 += parseFloat(preefect2);
            console.log("suma_efect2 divisa:" +suma_efect2);
          }else{
            if(monto_opera2.indexOf('$') != -1){
              montone2 = replaceAll(monto_opera2, ",", "" );
              montone2 = replaceAll(montone2, "$", "" );
            }else{
              montone2=parseFloat(monto_opera2);
            }
            var montone2 = montone2*valor_divi;
            suma_no_efect2 += parseFloat(montone2);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);

    }
    
  });
  var TABLA3 = $(".cont_padre > .cont_hijo_clone"); 
  TABLA3.each(function(){    
    /* ************ agregado para alertas **************** */
    instrum_comp=$(this).find("select[id*='instrum_notario'] option:selected").val();
    if($(this).find("input[id*='fecha_pago']").val()!="")
      msjsAlerta(instrum_comp);
    /* *****************************************************/
    //console.log("recorre tabla de pagos cont_hijo_clone");    
    // pesos de Liquidación 
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
      //console.log("pesos cont_hijo_clone");
      if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){ //en efectivo
        var monto3 = $(this).find("input[name*='monto']").val();
        //console.log("monto: "+$(this).find("input[name*='monto']").val());
        if(monto3.indexOf('$') != -1){
          montoe3 = replaceAll(monto3, ",", "" );
          montoe3 = replaceAll(montoe3, "$", "" );
        }else{
          montoe3=parseFloat(monto3);
        }
        suma_efect3 += parseFloat(montoe3);

      }  
      else{
        var monto3 = $(this).find("input[name*='monto']").val();
        if(monto3.indexOf('$') != -1){
          montone3 = replaceAll(monto3, ",", "" );
          montone3 = replaceAll(montone3, "$", "" );
        }else{
          montone3=parseFloat(monto3);
        }
        suma_no_efect3 += parseFloat(montone3);
      }
      /*console.log("suma_nefect pesos: "+suma_efect);
      console.log("suma_no_efect pesos: "+suma_no_efect);*/
    }
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){ //!= de pesos
      //console.log("!pesos cont_hijo_clone");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      monto_opera3 = $(this).find("input[name*='monto']").val();
      //console.log("monto: "+monto_opera);
      var inst = $(this).find("select[id*='instrum_notario'] option:selected").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago']").val()},
        success: function(result_divi){
          valor_divi=parseFloat(result_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera3.indexOf('$') != -1){
              tot_moDE3 = replaceAll(monto_opera3, ",", "" );
              tot_moDE3 = replaceAll(tot_moDE3, "$", "" );
            }else{
              tot_moDE3 = parseFloat(monto_opera3);
            }
            var preefect3 = parseFloat(tot_moDE3)*valor_divi;
            suma_efect3 += parseFloat(preefect3);
          }else{
            if(monto_opera3.indexOf('$') != -1){
              montone3 = replaceAll(monto_opera3, ",", "" );
              montone3 = replaceAll(montone3, "$", "" );
            }else{
              montone3=parseFloat(monto_opera3);
            }
            var montone3 = parseFloat(montone3)*valor_divi;
            suma_no_efect3 += parseFloat(montone3);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
    }
    contta++;
  });
  
  setTimeout(function () { 
    total = parseFloat(suma_no_efect+suma_efect+suma_no_efect2+suma_efect2+suma_no_efect3+suma_efect3).toFixed(2);
    var aux_mo=0;
    var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
    totalmontomo = replaceAll(totalmontomo, "$", "" );
    var vstotalmo = parseFloat(totalmontomo);
    aux_mo = parseFloat(vstotalmo);
    if(aux_mo==total){
      validar_xml=1;
      $('.validacion_cantidad').html('');
    }else{
      validar_xml=0;
      $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
    }
    /*console.log("suma_no_efect:" +suma_no_efect);
    console.log("suma_efect:" +suma_efect);
    console.log("suma_no_efect2:" +suma_no_efect2);
    console.log("suma_efect2:" +suma_efect2);
    console.log("suma_no_efect3:" +suma_no_efect3);
    console.log("suma_efect3:" +suma_efect3);
    console.log("total:" +total);*/
    $("#total_liquida_efect").val(convertMoneda(suma_efect+suma_efect2+suma_efect3));
    $("#total_liquida").val(convertMoneda(total));
    verificarLimite();
  }, 1500);
}

function convertMoneda(num){
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
  return monto;
}

function ComboAnio(){
    var d = new Date();
    var n = d.getFullYear();
    var select = document.getElementById("anio");
    for(var i = n; i >= 2000; i--) {
        var opc = document.createElement("option");
        opc.text = i;
        opc.value = i;
        select.add(opc)
    }
}

var cont_liqui = 0;
function add_liquida(){
  
  //console.log("add liquida");
  $row=$(".cont_hijo").clone().attr('class','row cont_hijo_clone');
  $row.find("button").removeClass("bg-teal").removeClass("waves-effect").addClass("btn-danger").attr("onclick","remove_liquida($(this))").find("i").attr('class','fa fa-trash-o');
  $row.find('button').text("Quitar liquidación");
  $row.find("input").val("");
  $row.find(".monto_divisa").text("");
  //$row.find("select").val("");
  //$row.find("#fecha_pago").attr('id',"fecha_add_"+cont_liqui+"").attr("onchange","validar_fecha(this.value,"+cont_liqui+")");
  $row.find("#fecha_pago").attr("onchange","validar_fecha(this.value,"+cont_liqui+"),confirmarPagosMonto()");
  //$row.find("#monto").attr('id',"monto_add_"+cont_liqui+"").attr("onchange","cambiaMonto(this.value,"+cont_liqui+")");
  $row.find("#monto").addClass("monto_"+cont_liqui+"").attr("onchange","formatoMontoExist(this.value,this.id,'"+cont_liqui+"'),confirmarPagosMonto()").val("$0.00");
  $row.find("#instrum_notario").addClass("instrum_notario_add").attr("onchange","confirmarPagosMonto()");
  if(cont_liqui==0){
    //console.log("cont_liqui =0: "+cont_liqui);
    $row.insertAfter(".form_pagos_clone").attr("id","id_cont_clone_"+cont_liqui+"");
  }else{
    //console.log("cont_liqui >0: "+cont_liqui);
    var cont_Ant = cont_liqui-1;
    $row.find("#id_cont_clone_"+cont_Ant+"");
    //console.log("id_cont_clone_: "+cont_Ant);
    $row.insertAfter("#id_cont_clone_"+cont_Ant+"").attr("id","id_cont_clone_"+cont_liqui+"");
  }
  cont_liqui++;
  //console.log("cont_liqui final: "+cont_liqui);
  validarFechasPago();
}

function formatoMontoExist(val,id,cont){
  console.log("cambia monto");
  console.log("val: "+val);
  console.log("id: "+id);
  /*nvo = replaceAll(val, ",", "" );
  nvo = replaceAll(nvo, "$", "" );*/
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(val);
  $("."+id+"_"+cont+"").val(monto);


  /*var min = true;
  var addtp = 0; i=0;
  $(".monto").each(function() {
      totalmonto = replaceAll($(this).val(), ",", "" );
      totalmonto = replaceAll(totalmonto, "$", "" );
      //totalmonto = Math.trunc(totalmonto);
      var vstotal = totalmonto;
      
      vstotal
      addtp += Number(vstotal);
      //addtp_min = $(".porcentaje_b");
  }); 

    var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(addtp);
    $("#total_liquida").val(totalg);
    tipoPago2();*/
}

function cambiaMonto(val,id){
  /*var min = true;
  var addtp = 0; i=0;
  $(".monto").each(function() {
      totalmonto = replaceAll($(this).val(), ",", "" );
      totalmonto = replaceAll(totalmonto, "$", "" );
      //totalmonto = Math.trunc(totalmonto);
      var vstotal = totalmonto;
      
      vstotal
      addtp += Number(vstotal);
      //addtp_min = $(".porcentaje_b");
  });
  var totalg = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(addtp);
  $("#total_liquida").val(totalg);

  var montoadd = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(val);
  $("#monto_add_"+id+"").val(montoadd);
  //console.log("montoadd: "+montoadd);
  tipoPagoNvo();
  validar_cantidad();*/
}
function validar_fecha(aux,id){
  var fecha_pago_liqui=aux;
  var fecha_operacion=$('#fecha_opera').val();
  if(fecha_pago_liqui==fecha_operacion){ 
  }else{
    swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
}
function tipoPagoNvo(){
  //console.log("tipoPagoNvo");
  /*var TABLA = $(".cont_padre #form_pagos > .cont_hijo");      
  var addtp=0;          
  TABLA.each(function(){         
    if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){
      //console.log("tipo de pago "+$(this).find("select[id*='instrum_notario'] option:selected").val());
      totalmonto = replaceAll($(this).find("input[name*='monto']").val(), ",", "" );
      totalmonto = replaceAll(totalmonto, "$", "" );
      //totalmonto = Math.trunc(totalmonto);
      var vstotal = totalmonto;
      vstotal
      addtp += Number(vstotal);
      //addtp_min = $(".porcentaje_b");
    }
  });
  var TABLA2 = $(".cont_padre #form_pagos > .cont_hijoExist ");      
  var addtp2=0;          
  TABLA2.each(function(){         
    if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){
      //console.log("tipo de pago "+$(this).find("select[id*='instrum_notario'] option:selected").val());
      totalmonto2 = replaceAll($(this).find("input[name*='monto']").val(), ",", "" );
      totalmonto2 = replaceAll(totalmonto2, "$", "" );
      //totalmonto2 = Math.trunc(totalmonto2);
      var vstotal2 = totalmonto2;
      vstotal2
      addtp2 += Number(vstotal2);
      //addtp_min = $(".porcentaje_b");
    }
  });
  var TABLA3 = $(".cont_padre > .cont_hijo_clone");      
  var addtp3=0;          
  TABLA3.each(function(){         
    if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){
      //console.log("tipo de pago "+$(this).find("select[id*='instrum_notario'] option:selected").val());
      totalmonto3 = replaceAll($(this).find("input[name*='monto']").val(), ",", "" );
      totalmonto3 = replaceAll(totalmonto3, "$", "" );
      //totalmonto3 = Math.trunc(totalmonto3);
      var vstotal3 = totalmonto3;
      vstotal3
      addtp3 += Number(vstotal3);
      //addtp_min = $(".porcentaje_b");
    }
  });
  //console.log("cont_hijo_clone: "+addtp3);
  var fin = addtp+addtp2+addtp3;
  var totale = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(fin);
  $("#total_liquida_efect").val(totale); */
}

function tipoPago2(){
  //console.log("tipopago2");
  /*var TABLA = $(".cont_padre #form_pagos > .cont_hijoExist");      
  var   addtp=0;          
  TABLA.each(function(){         
    if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){
      //console.log("tipo de pago "+$(this).find("select[id*='instrum_notario'] option:selected").val());
      totalmonto = replaceAll($(this).find("input[name*='monto']").val(), ",", "" );
      totalmonto = replaceAll(totalmonto, "$", "" );
      //totalmonto = Math.trunc(totalmonto);
      var vstotal = totalmonto;
      vstotal
      addtp += Number(vstotal);
      //addtp_min = $(".porcentaje_b");
    }

    var totale = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(addtp);
    $("#total_liquida_efect").val(totale);
  });*/
}

function tipoPago(id){
  /*totalmonto = replaceAll($("#total_liquida_efect").val(), ",", "" );
  totalmonto = replaceAll(totalmonto, "$", "" );
  //totalmonto = Math.trunc(totalmonto);
  monto_add = "#monto_add_"+id+"";
  monto = replaceAll($(monto_add).val(), ",", "" );
  monto = replaceAll(monto, "$", "" );
  //monto = Math.trunc(monto);
  if($(".instrum_notario_add").val()=="1"){
    var totale = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalmonto+monto);
    $("#total_liquida_efect").val(totale);
  }*/

}
function remove_liquida(i){
  i.closest(".cont_hijo_clone").remove();
  cont_liqui = cont_liqui-1;
}

function remove_liquidaE(i,id){
  title = "¿Desea eliminar este registro?";
  swal({
      title: title,
      text: "Se eliminará el pago!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Aceptar",
      closeOnConfirm: true
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        type:'POST',
        url: base_url+'Transaccion/eliminarPago',
        data:{ id: id, tipo:0},
        success:function(data){
          i.closest(".cont_hijoExist").remove();
          confirmarPagosMonto();
          swal("Éxito", "Pago eliminado correctamente", "success");
        }
      }); //cierra ajax
    }
  });
}

function tipo_moneda(id){
  /*var total = parseFloat($("#total_liquida").val()).toFixed(2);
 //console.log("montog: "+montog);
  monto = $(''+id+'').val();
  var totalg = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total+monto);
  $("#total_liquida").val(totalg);*/
}

function verificaTama(){
  //console.log("click en num  ");
  f=0;
  if($("#num_vehit").val().length<17){
    var tot = $("#num_vehit").val().length;
    var cadfalta="0"; var cadena="";
    var sub = 17 - $("#num_vehit").val().length;
    for(i=0; i<sub; i++){
      f++;
      cadena += cadfalta;
    }
    //console.log("cadena: "+cadena);
    $("#num_vehit").val(cadena+$("#num_vehit").val());
  }
}
function tipo_modeda(){
  /*const number = document.querySelector('#monto_opera');
  number.addEventListener('keyup', (e) => {
  const element = e.target;
  const value = element.value;
  element.value = formatNumber(value);
  });*/
  //totot1 = parseFloat($('#monto_opera').val());

  /*totot1 = replaceAll($('#monto_opera').val(), ",", "" );
  //console.log("toto1: "+totot1);
  totot2 = replaceAll(totot1, "$", "" );
  var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totot2);
  $('#monto_opera').val(totalg);
  validar_cantidad();*/
}
/*function tipo_modeda_monto(){
  //console.log("monto tipo_modeda_monto: "+$("#monto").val());
  const number = document.querySelector('#monto');
  number.addEventListener('keyup', (e) => {
  const element = e.target;
  const value = element.value;
  element.value = formatNumber(value);
  });
}*/
function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

function formatNumber (n) {
  n = String(n).replace(/\D/g, "");
  return n === '' ? n : Number(n).toLocaleString();
}
/*
function addCommas(nStr)
{
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}
*/
function validar_cantidad(){
  /*var min = true;
    var addtp = 0; i=0;
    $(".monto").each(function() {
        totalmonto = replaceAll($(this).val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        //totalmonto = Math.trunc(totalmonto);
        var vstotal = totalmonto;
        
        
        addtp += Number(vstotal);
        //addtp_min = $(".porcentaje_b");

    });
    //console.log("total monto");
    monto_opera = replaceAll($('#monto_opera').val(), ",", "" );
    monto_operan = replaceAll(monto_opera, "$", "" );
    var monto_operantt = monto_operan;
    var monto_ope = Number(monto_operantt);

    total_liquida = replaceAll($('#total_liquida').val(), ",", "" );
    total_liquida = replaceAll(total_liquida, "$", "" );
    total_liquida = parseFloat(total_liquida);
    console.log("total liquida: "+total_liquida);

    if(total_liquida==addtp){
       validar_xml=1;
       $('.validacion_cantidad').html('');
    }else{
       validar_xml=0;
       $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
    }
    //var comparara_ddtp = addtp;
    //var comparara_monto_opera= $('#monto_opera').val();
    //alert(comparara_monto_opera);
    //var monto = parseFloat($("#monto").val()).toFixed(2);
    totalmontov = replaceAll($("#monto").val(), ",", "" );
    totalmontov = replaceAll(totalmontov, "$", "" );
    //totalmontov = Math.trunc(totalmontov);

    var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalmontov);
    //console.log("monto: "+monto);
    $("#monto").val(monto);
    //console.log("addtp: "+addtp);
    
    var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(addtp);
    $("#total_liquida").val(totalg);
    tipoPagoNvo();*/
}

function verificarLimite(){
  efectivo = replaceAll($("#total_liquida_efect").val(), ",", "" );
  efectivo = replaceAll(efectivo, "$", "" );
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var array= JSON.parse(result);
      var limite = parseFloat(array[0].limite_efectivo).toFixed(2);
      var na = array[0].na;
      //console.log("limite: "+limite);
      //console.log("na: "+na);

      if(limite>0 && na==0){
        var anio = $("#anio").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Umas/getUmasAnio',
          data: { anio: anio},
          success: function(data2){
            valor_uma=data2;
            //console.log("valor de umas año: "+valor_uma);
            var uma_efect_anexo = efectivo / valor_uma;
            if(uma_efect_anexo>limite){
              lim_efec_band=true;
              swal("Error!", "El efectivo de la transaccion es mayor al limite permitido", "error");
             verificarConfigLimite(1);
              $('.limite_efect_msj').html('El efectivo(UMAS) de la transaccion es mayor al limite permitido.');
            }else{
              lim_efec_band=false;
              verificarConfigLimite(0);
              $('.limite_efect_msj').html('');
            }
          }
        });
      }//if na
    }
  });
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        setTimeout(function () { 
          swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
        }, 1500);        
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        
        setTimeout(function () { 
          swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
        }, 1500);
      }
    }
  });
}

function btn_numero_ayuda(){
  $('#modal_numero_ayuda').modal();
}
function btn_numero_ayuda2(){
  $('#modal_numero_ayuda2').modal();
}
function btn_matricula_ayuda(){
  $('#modal_matricula_ayuda').modal();
}

////////////////////// Operaciones
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}

/// < / >
