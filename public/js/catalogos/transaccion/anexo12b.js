var base_url = $('#base_url').val();
var id_aux = $('#id').val();
var tipo_actividad_aux=0;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var siempre_avi;
var valor_uma=0;
$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500);  
	ComboAnio();
  tipo_modeda(1);
  if($('#mes_aux').val()==''){
    $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
  }else{
    $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
  }
  $('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");

  //=====================================================
  //var aux_tipo_actividad = $('#aux_tipo_actividad').val();
  var aux_tipo_actividad = $('#tipo_actividad option:selected').val();
  otorgamiento_poder_select(aux_tipo_actividad); 
  //Actividades
  tipo_persona_opt(1);
  tipo_persona_opt(2);
  tipo_acto_opt();
  getpais('act1',1);
  getpais('act2',2);
  caracter_opt();
  domicilio_oficina_opt(1);
  tipo_persona_opt(21);
  tipo_persona_opt(22);
  tipo_autoridad_opt(2);
  domicilio_oficina_opt(2);
  tipo_autoridad_opt(3);
  domicilio_oficina_opt(3);
  tipo_persona_moralact_opt3();
  getpais('act3',3);
  tipo_autoridad_opt(4);
  domicilio_oficina_opt(4);
  getpais('act4',4);
  tipo_autoridad_opt(5);
  domicilio_oficina_opt(5);
  getpais('act5',5);
  tipo_persona_opt(5);
  tipo_persona_opt(52);
  datos_bien_mutuo_select(5);
  tipo_persona_opt(53);
  tipo_persona_opt(6);
  getpais('act6',6);

  var aux_cpm=$('#aux_cpm').val();
  if(aux_cpm==1){
    tabla_anexo12b_constitucion_personas_morales_socios(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios();
  } 
  var aux_dcv=$('#aux_dcv').val();
  if(aux_dcv==1){
    tabla_anexo12b_accionistas_vigentes(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_vigentes();
  }

  valorUma();
  $("#fecha_operacion").on("change",function(){
    valorUma();
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  validaGarantia();
  $("#tipo_garantiaact5").on("change",function(){
    validaGarantia();
  });
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();

});

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function validaGarantia(){
  if($("#tipo_garantiaact5 option:selected").val()==1){
    $("#cont_datos_garantia").hide();
  }else{
    $("#cont_datos_garantia").show();
  }
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function valorUma(){
  $.ajax({
    type: 'POST',
    url : base_url+'Umas/getUmasAnio',
    data: { anio: $("#fecha_operacion").val()},
    success: function(data2){
      valor_uma=data2;
      valor_uma=parseFloat(valor_uma);
      console.log("valor uma inicio: "+valor_uma);
    }
  });
}

function tipo_modeda(aux){
  var mon=$('.monto_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_"+aux+"").val(monto);
}
//==============Actividad 1===============
function tipo_acto_opt(){
  var opt = $('#tipo_actoact1 option:selected').val();
  if(opt==9){
    $('.tipo_actotxt').css('display','block');
  }else{
    $('.tipo_actotxt').css('display','none'); 
  }
}
function caracter_opt(){
  var opt = $('#caractervat1 option:selected').val();
  if(opt==9){
    $('.caractertxt').css('display','block');
  }else{
    $('.caractertxt').css('display','none'); 
  } 
}
function tipo_persona_opt(id){
  if($('#tipo_persona_'+id+'_1').is(':checked')){
    $('.tipo_persona_txt_'+id+'_1').css('display','block');
  }else{
    $('.tipo_persona_txt_'+id+'_1').css('display','none');
  }
  if($('#tipo_persona_'+id+'_2').is(':checked')){
    $('.tipo_persona_txt_'+id+'_2').css('display','block');
  }else{
    $('.tipo_persona_txt_'+id+'_2').css('display','none');
  }
  if($('#tipo_persona_'+id+'_3').is(':checked')){
    $('.tipo_persona_txt_'+id+'_3').css('display','block');
  }else{
    $('.tipo_persona_txt_'+id+'_3').css('display','none');
  }
}
function tipo_domicilio_opt(){
  if($('#tipo_domicilioact1').is(':checked')){
    $('.tipo_domiciliotxt1').css('display','block');
  }else{
    $('.tipo_domiciliotxt1').css('display','none'); 
  } 
  if($('#tipo_domicilioact2').is(':checked')){
    $('.tipo_domiciliotxt2').css('display','block');
  }else{
    $('.tipo_domiciliotxt2').css('display','none'); 
  } 
}
function domicilio_oficina_opt(id){
  if($('#domicilio_oficinaact'+id+'1').is(':checked')){
    $('.domicilio_oficina'+id+'txt1').css('display','block');
  }else{
    $('.domicilio_oficina'+id+'txt1').css('display','none'); 
  } 
  if($('#domicilio_oficinaact'+id+'2').is(':checked')){
    $('.domicilio_oficina'+id+'txt2').css('display','block');
  }else{
    $('.domicilio_oficina'+id+'txt2').css('display','none'); 
  } 
}

//=============================
function tipo_autoridad_opt(id){
  if($('#tipo_autoridadact'+id+'1').is(':checked')){
    $('.tipo_autoridadact'+id+'txt1').css('display','block');
  }else{
    $('.tipo_autoridadact'+id+'txt1').css('display','none'); 
  } 
  if($('#tipo_autoridadact'+id+'2').is(':checked')){
    $('.tipo_autoridadact'+id+'txt2').css('display','block');
  }else{
    $('.tipo_autoridadact'+id+'txt2').css('display','none'); 
  } 
}
function tipo_persona_moralact_opt3(){
  var opt = $('#tipo_persona_moralact3 option:selected').val();
  if(opt==99){
    $('.tipo_persona_moralacttxt3').css('display','block');
  }else{
    $('.tipo_persona_moralacttxt3').css('display','none'); 
  } 
}
//====== ACtividad 3=================================================================================================================
function agregar_accionistas_socios(){
  add_accionistas_socios(0,'','','','','','','','','','','','','','','','','','','');
}
var aux_accs=0;
function add_accionistas_socios(id,cargo_accionista,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //alert(pais);
  ///////////////////////////////////
  var cargo_accionista1=''; var cargo_accionista2=''; var cargo_accionista3=''; var cargo_accionista4=''; var cargo_accionista5='';
  if(cargo_accionista==1){
    cargo_accionista1='selected'; cargo_accionista2=''; cargo_accionista3=''; cargo_accionista4=''; cargo_accionista5='';
  }else if(cargo_accionista==2){
    cargo_accionista1=''; cargo_accionista2='selected'; cargo_accionista3=''; cargo_accionista4=''; cargo_accionista5='';
  }else if(cargo_accionista==3){
    cargo_accionista1=''; cargo_accionista2=''; cargo_accionista3='selected'; cargo_accionista4=''; cargo_accionista5='';
  }else if(cargo_accionista==4){
    cargo_accionista1=''; cargo_accionista2=''; cargo_accionista3=''; cargo_accionista4='selected'; cargo_accionista5='';
  }else if(cargo_accionista==5){
    cargo_accionista1=''; cargo_accionista2=''; cargo_accionista3=''; cargo_accionista4=''; cargo_accionista5='selected';
  }
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios_'+aux_accs+'">\
              <input class="form-control" type="hidden" id="id3" value="'+id+'">';
     html+='<div class="row">\
              <div class="col-md-6 form-group">\
                <label>Cargo del accionista dentro de la persona moral</label>\
                <select class="form-control" id="cargo_accionista3">\
                  <option value="1" '+cargo_accionista1+'>Administrador único</option>\
                  <option value="2" '+cargo_accionista2+'>Presidente del consejo de administración u órgano equivalente</option>\
                  <option value="3" '+cargo_accionista3+'>Otro miembro del consejo de administración u órgano equivalente</option>\
                  <option value="4" '+cargo_accionista4+'>Comisario o Miembro del consejo de vigilancia u órgano equivalente</option>\
                  <option value="5" '+cargo_accionista5+'>No aplica</option>\
                </select>\
              </div>\
            </div>\
            <h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona3" name="tipo_personas3_'+aux_accs+'" id="tipo_persona1_3_'+aux_accs+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(3,'+aux_accs+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona3" name="tipo_personas3_'+aux_accs+'" id="tipo_persona2_3_'+aux_accs+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(3,'+aux_accs+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona3" name="tipo_personas3_'+aux_accs+'" id="tipo_persona3_3_'+aux_accs+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(3,'+aux_accs+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_3_'+aux_accs+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre3" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno3" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno3" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento3" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc3" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp3" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad3 pais_3_'+aux_accs+'" id="pais_nacionalidad3_'+aux_accs+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_3_'+aux_accs+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm3" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm3" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm3" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm3 pais_3_'+aux_accs+'" id="pais_nacionalidad_accms_'+aux_accs+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_3_'+aux_accs+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf3" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf3" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof3" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o certificados de aportación</label>\
                  <input class="form-control" type="number" id="numero_accionesf3" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accs==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn gradient_nepal2" onclick="remover_anexo12b_constitucion_personas_morales_socios('+aux_accs+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios').append(html);
  getpais_socios(3,aux_accs); 
  tipo_personav_select(3,aux_accs);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_accs++;   
}
function tabla_anexo12b_constitucion_personas_morales_socios(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12b_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios(element.id,
                                        element.cargo_accionista,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios();
            }   
        }
    });
}
function remover_anexo12b_constitucion_personas_morales_socios(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12b_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios_'+aux).remove();
  }
}
function datos_bien_mutuo_select(id){
  if($('#datos_bien_mutuo1'+id).is(':checked')){
    $('.datos_inmuebletxt'+id).css('display','block');
  }else{
    $('.datos_inmuebletxt'+id).css('display','none');
  }
  if($('#datos_bien_mutuo2'+id).is(':checked')){
    $('.datos_otrotxt'+id).css('display','block');
  }else{
    $('.datos_otrotxt'+id).css('display','none');
  }
}
//========Actividad 4===============================================================================================================
function agregar_accionistas_vigentes(){
  add_accionistas_vigentes(0,'','','','','','','','','','','','','','','','','','');
}
var aux_acc4=0;
function add_accionistas_vigentes(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //alert(pais);
  ///////////////////////////////////
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_vigentes_'+aux_acc4+'">\
              <input class="form-control" type="hidden" id="id4" value="'+id+'">';
     html+='<h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_personas4_'+aux_acc4+'" id="tipo_persona1_4_'+aux_acc4+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(4,'+aux_acc4+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_personas4_'+aux_acc4+'" id="tipo_persona2_4_'+aux_acc4+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(4,'+aux_acc4+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_personas4_'+aux_acc4+'" id="tipo_persona3_4_'+aux_acc4+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(4,'+aux_acc4+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_4_'+aux_acc4+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre4" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno4" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno4" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento4" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc4" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp4" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad4 pais_4_'+aux_acc4+'" id="pais_nacionalidad4_'+aux_acc4+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_4_'+aux_acc4+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm4" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm4" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm4" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm4 pais_4_'+aux_acc4+'" id="pais_nacionalidad_accms_'+aux_acc4+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_4_'+aux_acc4+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf4" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf4" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof4" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o certificados de aportación</label>\
                  <input class="form-control" type="number" id="numero_accionesf4" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_acc4==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_vigentes()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn gradient_nepal2" onclick="remover_anexo12b_accionistas_vigentes('+aux_acc4+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_vigentes').append(html);
  getpais_socios(4,aux_acc4); 
  tipo_personav_select(4,aux_acc4);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_acc4++;   
}
function tabla_anexo12b_accionistas_vigentes(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12b_accionistas_vigentes",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_vigentes(element.id,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_vigentes();
            }   
        }
    });
}
function remover_anexo12b_accionistas_vigentes(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12b_accionistas_vigentes',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_vigentes_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_vigentes_'+aux).remove();
  }
}
//=======================================================================================================================//
function otorgamiento_poder_select(id){
  tipo_actividad_aux=id;
  /*if(id==1){
    $('#tipo_actividad1').prop('checked', true);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==2){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', true);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==3){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', true);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==4){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', true);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==5){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', true);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==6){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', true);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==7){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', true);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==8){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', true);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==9){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', true);$('#tipo_actividad10').prop('checked', false);
  }else if(id==10){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', true);
  }*/
  ///=========================================================================================
  //if($('#tipo_actividad1').is(':checked')){
  if($('#tipo_actividad option:selected').val()==1){
    $('.tipo_actividadtxt1').css('display','block');
  }else{
    $('.tipo_actividadtxt1').css('display','none');
  }
  //if($('#tipo_actividad2').is(':checked')){
  if($('#tipo_actividad option:selected').val()==2){
    $('.tipo_actividadtxt2').css('display','block');
  }else{
    $('.tipo_actividadtxt2').css('display','none');
  }
  //if($('#tipo_actividad3').is(':checked')){
  if($('#tipo_actividad option:selected').val()==3){
    $('.tipo_actividadtxt3').css('display','block');
  }else{
    $('.tipo_actividadtxt3').css('display','none');
  }
  //if($('#tipo_actividad4').is(':checked')){
  if($('#tipo_actividad option:selected').val()==4){
    $('.tipo_actividadtxt4').css('display','block');
  }else{
    $('.tipo_actividadtxt4').css('display','none');
  }
  //if($('#tipo_actividad5').is(':checked')){
  if($('#tipo_actividad option:selected').val()==5){
    $('.tipo_actividadtxt5').css('display','block');
  }else{
    $('.tipo_actividadtxt5').css('display','none');
  }
  //if($('#tipo_actividad6').is(':checked')){
  if($('#tipo_actividad option:selected').val()==6){
    $('.tipo_actividadtxt6').css('display','block');
  }else{
    $('.tipo_actividadtxt6').css('display','none');
  }
}
//=======================
function getpais(txt,id){
  //console.log("get pais");
  $('.pais_'+txt+'_'+id).select2({
      width: '350px',
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Transaccion/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
function registrar(){
  var form_register = $('#form_anexo,#form_anexo2,#form_anexo3,#form_anexo4,#form_anexo5,#form_anexo6,#form_anexo7');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                /*monto_opera:{
                  required: true
                },*/
                referencia:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_anexo").valid();
        var band_aviso = 0;
        if(valid) {
          
          monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
          monto_opera = replaceAll(monto_oper, "$", "" );
          
          if($("#aviso").val()>0){
            name_table = "anexo12b_aviso";
          }else{
            name_table = "anexo12b";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }
          $("#btn_submit").attr("disabled",true);
          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo12b',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = $.parseJSON(data);
                var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                //===================
                if(tipo_actividad_aux==3){
                  var form_act=$('#form_actividad'+tipo_actividad_aux+',#form_actividad31').serialize()+"&anexo12b="+id+"&aviso="+$("#aviso").val();  
                }else{
                  var form_act=$('#form_actividad'+tipo_actividad_aux).serialize()+"&anexo12b="+id+"&aviso="+$("#aviso").val();  
                }
                var id_acusado=0;
                $.ajax({
                  type:'POST',
                  url: base_url+'Transaccion/registro_anexo12b_actividad'+tipo_actividad_aux,
                  data:form_act,
                  statusCode:{
                      404: function(data){
                          swal("Error!", "No Se encuentra el archivo", "error");
                      },
                      500: function(){
                          swal("Error!", "500", "error");
                      }
                  },
                  success:function(data){
                  }
                });   
                if(tipo_actividad_aux==3){
                  var DATA3= [];
                  var TABLA3 = $(".row_accionistas_socios .accionistas_socios > div");                  
                  TABLA3.each(function(){         
                        item = {};
                        item ["idanexo12b"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id3']").val();
                        item ["cargo_accionista"]=$(this).find("select[id*='cargo_accionista3']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona3']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre3']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno3']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno3']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento3']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc3']").val();
                        item ["curp"]=$(this).find("input[id*='curp3']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad3']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm3']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm3']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm3']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm3']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf3']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf3']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof3']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf3']").val();
                        DATA3.push(item);
                  });
                  INFO3  = new FormData();
                  aInfo3   = JSON.stringify(DATA3);
                  INFO3.append('data', aInfo3);
                  $.ajax({
                      data: INFO3,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12b_constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                } 
                if(tipo_actividad_aux==4){
                  var DATA4= [];
                  var TABLA4 = $(".row_accionistas_vigentes .accionistas_vigentes > div");                  
                  TABLA4.each(function(){         
                        item = {};
                        item ["idanexo12b"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id4']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona4']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre4']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno4']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno4']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento4']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc4']").val();
                        item ["curp"]=$(this).find("input[id*='curp4']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad4']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm4']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm4']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm4']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm4']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf4']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf4']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof4']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf4']").val();
                        DATA4.push(item);
                  });
                  INFO4  = new FormData();
                  aInfo4   = JSON.stringify(DATA4);
                  INFO4.append('data', aInfo4);
                  $.ajax({
                      data: INFO4,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12b_accionistas_vigentes',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                } 
                var anio = $("#anio").val();
                var band_umbral=false;
                var addtp=0; var addtpD=0;
                var umas_anexo=0;
                if($("#tipo_actividad option:selected").val()==1)
                  act=1;
                else if($("#tipo_actividad option:selected").val()==2)
                  act=2;
                else if($("#tipo_actividad option:selected").val()==3 || $("#tipo_actividad option:selected").val()==4)
                  act=3;
                else if($("#tipo_actividad option:selected").val()==5)
                  act=4;
                else if($("#tipo_actividad option:selected").val()==6)
                  act=5;
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umbrales/getUmbralActividad',
                  data: {anexo: $("#id_actividad").val(), act:act},
                  success: function(result){
                    //var array = $.parseJSON(result);
                    var array= JSON.parse(result);
                    var aviso = parseFloat(array[0].aviso).toFixed(2);
                    var siempre_avi=array[0].siempre_avi;
                    var siempre_iden=array[0].siempre_iden;
                    var identifica=array[0].identificacion;
                    //monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
                    //monto_opera = replaceAll(monto_oper, "$", "" );
                    //if($("#tipo_actividad option:selected").val()==1){
                    monto_opera=0;
                    if($("#tipo_actividad option:selected").val()==1){
                      monto_opera = $("input[name*='valor_catastralact1']").val()
                    }
                    if($("#tipo_actividad option:selected").val()==6){
                      monto_opera = $("input[id*='valor_avaluorea']").val()
                    }
                    
                    monto_opera = parseFloat(monto_opera).toFixed(2);
                    valor_uma = valor_uma*1;
                    //console.log("aviso: "+aviso);
                    console.log("valor_uma: "+valor_uma);
                    //console.log("monto: "+monto_opera);
                    if(siempre_avi=="1"){
                      band_umbral=true;
                      $.ajax({ //cambiar pago a acusado
                        type: 'POST',
                        url : base_url+'Operaciones/acusaPago2',
                        async: false,
                        data: { id:id, act:$("#id_actividad").val()},
                        success: function(result_divi){

                        }
                      });
                    }//else if(siempre_avi!="1" && $("#tipo_actividad option:selected").val()==1 || siempre_avi!="1" && $("#tipo_actividad option:selected").val()==6){
                    else{
                      umas_anexo=monto_opera/valor_uma;
                      umas_anexo = parseFloat(umas_anexo).toFixed(2);
                      console.log("valor de monto_opera: "+monto_opera);
                      console.log("valor de umas_anexo final: "+umas_anexo);
                      console.log("valor de umas_anexo final: "+umas_anexo);
                      console.log("valor de umas aviso: "+aviso);
                      console.log("siempre_avi: "+siempre_avi);
                      umas_anexo = umas_anexo*1;
                      aviso = aviso*1;
                      if(umas_anexo>=aviso){
                        band_umbral=true;
                        $.ajax({ //cambiar pago a acusado
                          type: 'POST',
                          url : base_url+'Operaciones/acusaPago2',
                          async: false,
                          data: { id:id, act:$("#id_actividad").val()},
                          success: function(result_divi){

                          }
                        });
                      }else{
                        if(umas_anexo<aviso && $("#tipo_actividad option:selected").val()==1 || umas_anexo<aviso && $("#tipo_actividad option:selected").val()==6){
                        //*se debe crear una nueva ocnsulta de getOpera, ya ques es por anexo y por actividad interna
                        //console.log("If para buscar historico de mismo anexo misma act");
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Operaciones/getOperaActividad12b',
                          async: false,
                          data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id, tipo:$("#tipo_actividad option:selected").val(), id_perfilamiento: $("#id_perfilamiento").val() },
                          success: function(result){
                            //console.log("result: "+result);
                            var data= $.parseJSON(result);

                            var datos = data.data;
                            var opera_comph=0; var cont_hist=0; var band_comple=0; var id_ant=0;
                            var id_element = [];
                            datos.forEach(function(element) {
                              var addtpHF=0; var addtpHDF=0; var umas_anexoH=0;
                              if(element.tipo_actividad==1){
                                suma_acth=parseFloat(element.valor_catastralact1);
                                //umas_anexoH += parseFloat(suma_acth/valor_uma);
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma);
                                }
                                //console.log("umas_anexo de tipo actividad: "+umas_anexo);
                              }
                              umas_anexoH=umas_anexoH*1;
                              umas_anexo = umas_anexo*1;
                              item = {}; 
                              item ["id_desa"]=element.id;
                              id_element.push(item);
                              
                              INFO  = new FormData();
                              aInfo = JSON.stringify(id_element);
                              INFO.append('data', aInfo);
                              id_acusado=0;
                              if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){ 
                                if(cont_hist<1){
                                  umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                }else{
                                  umas_anexoTot_historico += umas_anexoH;
                                }
                                if(umas_anexoTot_historico<aviso && band_comple==0){
                                  id_ant = element.id;
                                }
                                //console.log("band_comple: "+band_comple);
                                if(umas_anexoTot_historico>=aviso){ //descomentar lo de abajo
                                  $.ajax({ //cambiar pago a acusado
                                    type: 'POST',
                                    url : base_url+'Operaciones/acusaPagoActividad12bArray',
                                    async: false,
                                    data: "data="+aInfo+"&act"+$("#id_actividad").val()+"&id_anexo="+id+"&aviso="+$("#aviso").val()+"&pago_ayuda=1"+"&id_ant="+id_ant,
                                    success: function(result2){
                                      id_acusado=result2;
                                      console.log("acusado: "+id_acusado);
                                    }
                                  }); 
                                }
                                cont_hist++;
                                
                              }
                              //console.log("band_comple final: "+band_comple);
                              //console.log("cont_hist: "+cont_hist);
                              console.log("umas_anexoTot_historico ultimo: "+umas_anexoTot_historico);
                              if(umas_anexoTot_historico>=aviso){
                                band_comple=1;
                                band_umbral=true;
                              }
                            });//foreach
            
                          }//success de  get opera
                        });

                      }
                      }
                    }
                    //console.log("band_umbral: "+band_umbral);

                    /*if (suma==monto){
                      validar_xml=1;
                    }else{
                      validar_xml=0;
                    }*/

                    //alert(tipo_actividad_aux);
                    setTimeout(function () { 
                      if($("#tipo_actividad option:selected").val()==2 || $("#tipo_actividad option:selected").val()==3 || $("#tipo_actividad option:selected").val()==4 || $("#tipo_actividad option:selected").val()==5){
                        if($("#aviso").val()>0){
                          window.location.href = base_url+"Transaccion/anexo12b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                        }else{
                          window.location.href = base_url+"Transaccion/anexo12b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                        }
                      }
                      if($("#tipo_actividad option:selected").val()==1 || $("#tipo_actividad option:selected").val()==6){
                        if(band_umbral==true){ // actividad 
                          if($("#aviso").val()>0){
                            window.location.href = base_url+"Transaccion/anexo12b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                          }else{
                            window.location.href = base_url+"Transaccion/anexo12b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                          }
                        }
                      }/*else{
                        if($("#aviso").val()>0 && band_umbral==true){
                          window.location.href = base_url+"Transaccion/anexo12b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"";
                        }else{
                          window.location.href = base_url+"Transaccion/anexo12b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"";
                        }
                      }*/
                      swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                      /*if(history.back()!=undefined){
                        setTimeout(function () { history.back() }, 1500);
                      }*/
                      if($("#aviso").val()>0){
                        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                      }else{
                        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                      }
                    }, 2500); //esperar a xml y sumatoria
                  }//success  
                });  //ajax  
              }  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }  
}
//============================
function tipo_personav_select(txt,id){
  if($('#tipo_persona1_'+txt+'_'+id).is(':checked')){
    $('.tipo_personatxt1_'+txt+'_'+id).css('display','block');
  }else{
    $('.tipo_personatxt1_'+txt+'_'+id).css('display','none');
  }
  if($('#tipo_persona2_'+txt+'_'+id).is(':checked')){
    $('.tipo_personatxt2_'+txt+'_'+id).css('display','block');
  }else{
    $('.tipo_personatxt2_'+txt+'_'+id).css('display','none');
  }
  if($('#tipo_persona3_'+txt+'_'+id).is(':checked')){
    $('.tipo_personatxt3_'+txt+'_'+id).css('display','block');
  }else{
    $('.tipo_personatxt3_'+txt+'_'+id).css('display','none');
  }
}
function getpais_socios(txt,id){
  //console.log("get pais");
  $('.pais_'+txt+'_'+id).select2({
      width: '350px',
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Transaccion/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
function regresar(){
   //history.back();
   if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}
function ValidCurp(T){
  let ex=/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
  valor=T.value;
  valor = valor.toUpperCase();
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "CURP No valido", "error");
    }
  }
}

function ValidRfc(T){
  let ex=/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;
  valor=T.value;
  valor = valor.toUpperCase();
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "RFC No valido", "error");
    }
  }
}
function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
function btn_organoJ_ayuda(){
  $('#modal_organoJ_ayuda').modal();
}
function btn_tipodeJ_ayuda(){
  $('#modal_tipodeJ_ayuda').modal();
}
function btn_materia_ayuda(){
  $('#modal_materia_ayuda').modal();
}
function btn_expediente_ayuda(){
  $('#modal_expediente_ayuda').modal();
}
function btn_TARCSO_ayuda(){
  $('#modal_TARCSO_ayuda').modal(); 
}
function btn_organoA_ayuda(){
  $('#modal_organoA_ayuda').modal(); 
}
function btn_CAAQSRA_ayuda(){
  $('#modal_CAAQSRA_ayuda').modal(); 
}
function btn_NIP_ayuda(){
  $('#modal_NIP_ayuda').modal(); 
}
function btn_NRIF_ayuda(){
  $('#modal_NRIF_ayuda').modal(); 
}
function btn_foliomer_ayuda(){
  $('#modal_foliomer_ayuda').modal(); 
}
function btn_folio_ayuda(){
  $('#modal_folio_ayuda').modal(); 
}
function btn_DCTGO_ayuda(){
  $('#modal_DCTGO_ayuda').modal(); 
}
function btn_NIPFE_ayuda(){
   $('#modal_NIPFE_ayuda').modal(); 
}
// http://localhost/yimexico/Transaccion/anexo12b/0/21/16/42/0/256/157

function cambiaCP(id){
  cp = $("#"+id+"").val();  
  if(cp!="" && id=="codigo_postalact1"){
    col = "coloniaact1";
  }
  else if(cp!="" && id=="codigo_postalnaact1"){
    col="colonianaact1";
  }
  else if(cp!="" && id=="codigo_postalexact1"){
    col="coloniaexact1";
  }

  else if(cp!="" && id=="codigo_postalact2"){
    col="coloniaact2";
  }
  else if(cp!="" && id=="codigo_postaleact2"){
    col="coloniaeact2";
  }
  else if(cp!="" && id=="codigo_postalact3"){
    col="coloniaact3";
  }
  else if(cp!="" && id=="codigo_postaleact3"){
    col="coloniaeact3";
  }

  else if(cp!="" && id=="codigo_postalact4"){
    col="coloniaact4";
  }
  else if(cp!="" && id=="codigo_postaleact4"){
    col="coloniaeact4";
  }

  else if(cp!="" && id=="codigo_postaleact5"){
    col="coloniaact5";
  }
  else if(cp!="" && id=="codigo_postalact5"){
    col="coloniaeact5";
  }
  var itemc = [];
  $("#"+col+"").select2({
    placeholder: {
      id: 0,
      text: 'Buscar colonia:'
    },
    width: '100%',
    allowClear: true,
    data: itemc
  });
  autoComplete(col);
}
function autoComplete(idcol){
  if(idcol=="coloniaact1"){
    cp = $("#codigo_postalact1").val(); 
    col = $('#coloniaact1').val();  
  }
  else if(idcol=="colonianaact1"){
    cp = $("#codigo_postalnaact1").val(); 
    col = $('#colonianaact1').val();  
  }
  else if(idcol=="coloniaexact1"){
    cp = $("#codigo_postalexact1").val(); 
    col = $('#coloniaexact1').val();  
  }
  else if(idcol=="coloniaact2"){
    cp = $("#codigo_postalact2").val(); 
    col = $('#coloniaact2').val();  
  }
  else if(idcol=="coloniaeact2"){
    cp = $("#codigo_postaleact2").val(); 
    col = $('#coloniaeact2').val();  
  }
  else if(idcol=="coloniaact3"){
    cp = $("#codigo_postalact3").val(); 
    col = $('#coloniaact3').val();  
  }
  else if(idcol=="coloniaeact3"){
    cp = $("#codigo_postaleact3").val(); 
    col = $('#coloniaeact3').val();  
  }

  else if(idcol=="coloniaact4"){
    cp = $("#codigo_postalact4").val(); 
    col = $('#coloniaact4').val();  
  }
  else if(idcol=="coloniaeact4"){
    cp = $("#codigo_postaleact4").val(); 
    col = $('#coloniaeact4').val();  
  }

  else if(idcol=="coloniaact5"){
    cp = $("#codigo_postaleact5").val(); 
    col = $('#coloniaact5').val();  
  }
  else if(idcol=="coloniaeact5"){
    cp = $("#codigo_postalact5").val(); 
    col = $('#coloniaeact5').val();  
  }
  /*console.log("cp: "+cp);
  console.log("col: "+col);*/

  $('#'+idcol+'').select2({
      width: 'resolve',
      minimumInputLength: 0,
      minimumResultsForSearch: 10,
      placeholder: 'Seleccione una colonia:',
      ajax: {
          url: base_url+'Perfilamiento/getDatosCPSelect',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  cp: cp, 
                  col: col ,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.colonia,
                      text: element.colonia,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}

// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}