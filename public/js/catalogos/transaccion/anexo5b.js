var base_url = $('#base_url').val();
var validar_xml=0;
var valCurp=false;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var siempre_avi;
$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
  $("#fisica").on("click",function(){ //funciones para -- Datos cuando la aportación es a través de socios 
    //if($("#tipo_aportacion2").is(':checked')){
    if($("#tipo_aportacion").val()==2){
      $("#cont_persfi").show();
      $("#cont_persmor").hide();
      $("#cont_fide").hide();
    }
  });
  $("#moral").on("click",function(){
    //if($("#tipo_aportacion2").is(':checked')){
    if($("#tipo_aportacion").val()==2){
      $("#cont_persmor").show();
      $("#cont_persfi").hide();
      $("#cont_fide").hide();
    }
  });
  $("#fideicomiso").on("click",function(){
    //if($("#tipo_aportacion2").is(':checked')){
    if($("#tipo_aportacion").val()==2){
      $("#cont_persmor").hide();
      $("#cont_persfi").hide();
      $("#cont_fide").show();
    }
  });

  $("#extrangero").on("click",function(){
    $("#cont_dir_ext").show();
    $("#cont_col_sel_ext").show();
    $("#cont_col_sel").hide();
    $("#colonia_dir").val('');
    /*$("#cont_col_sel").html('<label>Colonia:</label>\
            <input class="form-control" type="text" name="colonia_dir" value="">');*/
  });
  $("#nacional").on("click",function(){
    $("#cont_dir_ext").hide();
    $("#cont_col_sel").show();
    $("#cont_col_sel_ext").hide();
    $("#colonia_dir2").val('');
    /*$("#cont_col_sel").html('<label>Colonia:</label>\
            <select onclick="autoComplete(this.id)" class="form-control" name="colonia_dir" id="colonia_dir">\
              <option value=""></option>\
            </select>');*/
  });

  if($("#nacional").is(":checked")==true){
    $("#cont_dir_ext").hide();
    $("#cont_col_sel").show();
    $("#cont_col_sel_ext").hide();
    $("#colonia_dir2").val('');
    /*$("#cont_col_sel").html('<label>Colonia:</label>\
            <select onclick="autoComplete(this.id)" class="form-control" name="colonia_dir" id="colonia_dir">\
              <option value=""></option>\
            </select>');*/
  }

  if($("#extrangero").is(":checked")==true){
    $("#cont_dir_ext").show();  
    $("#cont_col_sel_ext").show();
    $("#cont_col_sel").hide();
    $("#colonia_dir").val('');
  }

  $("#aport_num").on("click",function(){
    $("#cont_apor_num").show();
    $("#cont_apor_esp").hide();
  });
  $("#aport_espe").on("click",function(){
    $("#cont_apor_num").hide();
    $("#cont_apor_esp").show();
  });
  $("#tipo_tercero").on("change",function(){
    if($("#tipo_tercero").val()=="99"){
      $("#cont_desc_otro").show();
    }else{
      $("#cont_desc_otro").hide();
    }
  });

  $("#fisca_ter").on("click",function(){
    //if($("#tipo_aportacion3").is(':checked')){ 
    if($("#tipo_aportacion").val()==3){
      $("#cont_persfi_ter").show();
      $("#cont_persmor_ter").hide();
      $("#cont_fide_ter").hide();
    }
  });
  $("#moral_ter").on("click",function(){
    //if($("#tipo_aportacion3").is(':checked')){ 
    if($("#tipo_aportacion").val()==3){
      $("#cont_persmor_ter").show();
      $("#cont_persfi_ter").hide();
      $("#cont_fide_ter").hide();
    }
  });
  $("#fide_ter").on("click",function(){
    //if($("#tipo_aportacion3").is(':checked')){ 
    if($("#tipo_aportacion").val()==3){
      $("#cont_persmor_ter").hide();
      $("#cont_persfi_ter").hide();
      $("#cont_fide_ter").show();
    }
  });

  $("#aport_num_ter").on("click",function(){
    //if($("#tipo_aportacion3").is(':checked')){ 
    if($("#tipo_aportacion").val()==3){
      $("#cont_apor_num_ter").show();
      $("#cont_apor_num_ter2").show();
      $("#cont_apor_esp_ter").hide();
    }
  });
  $("#aport_esp_ter").on("click",function(){
    //if($("#tipo_aportacion3").is(':checked')){
    if($("#tipo_aportacion").val()==3){
      $("#cont_apor_num_ter").hide();
      $("#cont_apor_num_ter2").hide();
      $("#cont_apor_esp_ter").show();
    }
  });

  //tipos de personas aportacion tipo 5 //
  $("#fisica_nof").on("click",function(){
    //if($("#tipo_aportacion5").is(':checked')){
    if($("#tipo_aportacion").val()==5){
      $("#cont_persfi_nof").show();
      $("#cont_persmor_nof").hide();
      $("#cont_fide_nof").hide();
    }
  });
  $("#moral_nof").on("click",function(){
    //if($("#tipo_aportacion5").is(':checked')){
    if($("#tipo_aportacion").val()==5){
      $("#cont_persmor_nof").show();
      $("#cont_persfi_nof").hide();
      $("#cont_fide_nof").hide();
    }
  });
  $("#fide_nof").on("click",function(){
    //if($("#tipo_aportacion5").is(':checked')){
    if($("#tipo_aportacion").val()==5){
      $("#cont_persmor_nof").hide();
      $("#cont_persfi_nof").hide();
      $("#cont_fide_nof").show();
    }
  });

   //tipos de personas aportacion tipo 2 //
   $("#fisica").on("click",function(){
    //if($("#tipo_aportacion2").is(':checked')){
    if($("#tipo_aportacion").val()==2){
      $("#cont_persfi").show();
      $("#cont_persmor").hide();
      $("#cont_fide").hide();
    }
  });
  $("#moral").on("click",function(){
    //if($("#tipo_aportacion2").is(':checked')){
    if($("#tipo_aportacion").val()==2){
      $("#cont_persmor").show();
      $("#cont_persfi").hide();
      $("#cont_fide").hide();
    }
  });
  $("#fideicomiso").on("click",function(){
    //if($("#tipo_aportacion2").is(':checked')){
    if($("#tipo_aportacion").val()==2){
      $("#cont_persmor").hide();
      $("#cont_persfi").hide();
      $("#cont_fide").show();
    }
  });
  //tipo dom aportacion tipo 2
  $("#extrangero").on("click",function(){
    //if($("#tipo_aportacion2").is(':checked')){
    if($("#tipo_aportacion").val()==2){
      $("#cont_dir_ext").show();
    }else{
      $("#cont_dir_ext").hide();
    }
  });

  $("#aportacion_fideicomiso").on("click",function(){
    if($("#aportacion_fideicomiso").is(':checked')){ 
      $("#cont_nom_insti").show();
    }else{
      $("#cont_nom_insti").hide();
    }
  });
  
  $("#tipo_aportacion").on("click",function(){
    /*$('#tipo_aportacion2').prop('checked', false).removeAttr('checked');
    $('#tipo_aportacion3').prop('checked', false).removeAttr('checked');
    $('#tipo_aportacion4').prop('checked', false).removeAttr('checked');
    $('#tipo_aportacion5').prop('checked', false).removeAttr('checked');
    $('#tipo_aportacion6').prop('checked', false).removeAttr('checked');*/
    select_aportacion();
    select_numerario_especie();
  });

  var cont=0;
  $(".monto").on('change',function(){
    id = this.id;
    //if(cont>0){
      var comp = $('#'+id+'').val().indexOf("$");
      if(comp==0){
        tot1 = replaceAll($('#'+id+'').val(), ",", "" );
        tot1 = replaceAll(totot1, "$", "" );
        var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#'+id+'').val());
        $('#'+id+'').val(totalg); 
      }else{
        var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#'+id+'').val());
        $('#'+id+'').val(totalg); 
      }
      /*if($("#monto_opera").val()==totalg){
        validar_xml=1;
        $('.validacion_cantidad').html('');
      }else{
        validar_xml=0;
        $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
      }*/
    /*}else{
      var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#'+id+'').val());
      $('#'+id+'').val(monto);    
    }
    cont++;*/
    validar_xml=1;
  });

  ComboAnio();
  $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");

  select_aportacion();
  select_numerario_especie();
  personas();

  setTimeout(function () { 
    //muestraTerceroTable();
  }, 2500);

  $("#tipo_moneda").on("change", function(){
    //console.log("cambio monto opera");
    confirmarPagosMonto();
  });
  //verificarLimite();
  verificarConfigLimite();
  setTimeout(function () { 
    confirmarPagosMonto();
  }, 1500);
  $("#fecha_operacion").on("change",function(){
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();

  //para traer datos del inmueble
  $("#inmuebles").on("change",function(){
    if($("#inmuebles").val()>0 || $("#inmuebles").val()!=""){
      //console.log("val inmuble: "+this.value);
      datosDirSelect(this.value)
    }
  });

  $('.img').change( function() {
    id_aux=$(this).data("idaux");
    filezise = this.files[0].size;  
    num_aux=$(this).data("num_aux");    
    //console.log("num_aux: "+$(this).data("num_aux"));               
    if(filezise > 2548000) { // 1mb
      //console.log($('.img')[0].files[0].size);

      $(this).val('');
      swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
    }else { //ok
      tama_perm = true; 
      var archivo = this.value;
      var name = this.id;
      var col=name;
      extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
      extensiones_permitidas = new Array(".pdf",".jpeg",".png",".jpg",".webp");
      permitida = false;
      if($('#'+name)[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true && tama_perm==true){
          //console.log("tamaño permitido");
          var inputFileImage = document.getElementById(name);
          var file = inputFileImage.files[0];
          var data = new FormData();
          //id_docs = $("#id").val();
          data.append('compro',file);
          id_tercero = id_aux;
          $.ajax({
            url:base_url+'Transaccion/cargafiles/'+$("#id_anexo").val()+"/"+id_tercero+'/terceros_5b/'+num_aux,
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
              var array = $.parseJSON(data);
              if (array.ok==true && array.id_tercero>0) {
                $("#name_comprobante_"+array.id_tercero+"").val(array.name);
                swal("Éxito", "Comprobante cargado correctamente", "success");
              }else if(array.ok==true && array.id_tercero==0) {
                if(num_aux==1)
                  $("#name_comprobante").val(array.name);
                else
                  $("#name_comprobante"+num_aux+"").val(array.name);

                swal("Éxito", "Comprobante cargado correctamente", "success");
                $(".id_terceros_"+array.id_tercero+"")
              }else if(array.ok!=true) {
                swal("Error!", "Intente nuevamente", "error");
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              var data = JSON.parse(jqXHR.responseText);
            }
          });
        }else if(permitida==false){
            swal("Error!", "Tipo de archivo no permitido", "error");
        } 
      }else{
        //swal("Error!", "No se a selecionado un archivo", "error");
      } 
    }
  });
});

function changeImg(name,cont){
  /* ****************************** */
  const fi = document.getElementsByName(name);
  id_aux=$("input[name*='"+name+"']").data("idaux");
  //id_aux=$("."+name+"").data("idaux");
  num_aux=$("input[name*='"+name+"']").data("num_aux");
  //num_aux=$("."+name+"").data("num_aux");
  //console.log("id_aux: "+id_aux);
  //console.log("num_aux: "+num_aux);
  filezise = $("input[name*='"+name+"']")[0].files[0].size;                     
  if(filezise > 2548000) { // 1mb
    //console.log($('.img')[0].files[0].size);
    $("input[name*='"+name+"']").val('');
    swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
  }else { //ok
    tama_perm = true; 
    var archivo = $("input[name*='"+name+"']").val();
    //var name = name;
    var col=name;
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".pdf",".jpeg",".png",".jpg",".webp");
    permitida = false;
    if($("input[name*='"+name+"']")[0].files.length > 0) {
      for (var i = 0; i < extensiones_permitidas.length; i++) {
         if (extensiones_permitidas[i] == extension) {
         permitida = true;
         break;
         }
      }  
      if(permitida==true && tama_perm==true){
        //console.log("tamaño permitido");
        var inputFileImage = document.getElementsByName(name);
        var file = inputFileImage[0].files[0];
        var data = new FormData();
        //id_docs = $("#id").val();
        data.append('compro',file);
        id_pago = id_aux;
        $.ajax({
          url:base_url+'Transaccion/cargafiles/'+$("#id_anexo").val()+"/"+id_tercero+'/terceros_5b/'+num_aux,
          type:'POST',
          contentType:false,
          data:data,
          processData:false,
          cache:false,
          success: function(data) {
            var array = $.parseJSON(data);
            //console.log("id_tercero: "+array.id_tercero);
            if (array.ok==true && array.id_tercero>0) {
              $("#name_comprobante_"+array.id_tercero+"").val(array.name);
              swal("Éxito", "Comprobante cargado correctamente", "success");
            }else if(array.ok==true && array.id_tercero==0) {
              $(".name_comprobante"+num_aux+"_"+cont+"").val(array.name);

              swal("Éxito", "Comprobante cargado correctamente", "success");
              //$(".id_terceros_"+array.id_tercero+"")
            }else if(array.ok!=true) {
              swal("Error!", "Intente nuevamente", "error");
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
          }
        });
      }else if(permitida==false){
        swal("Error!", "Tipo de archivo no permitido", "error");
      } 
    }else{
      //swal("Error!", "No se a selecionado un archivo", "error");
    } 
  }
  /******************************************************** */
}

function fileInputTerceros(id=0,compro,name,aux){
  //console.log("id: "+id);
  if(aux==1)
    name_col="comprobante";
  else if(aux==2)
    name_col="comprobante2";
  else
    name_col="comprobante3";

  if(id>0){
    imgprev2="";
    imgdet2="";
    imgp2="";
    typePDF2="";
    //compro = $("#name_comprobante_"+id+"").val();
    //console.log("compro: "+compro);
    //name="comprobante_"+id+"";
    if(compro!=""){
      imgprev2 = base_url+"uploads/comprobantes_anexo5b/"+compro;
      imgdet2 = {type:"image", url: base_url+"Transaccion/file_delete/"+id+"/terceros_5b/"+name_col, caption: compro, key:id};
      ext = compro.split('.');
      if(ext[1].toLowerCase()!="pdf"){
        imgp2 = base_url+"uploads/comprobantes_anexo5b/"+compro; 
        typePDF2 = "false";  
      }else{
        imgp2 = base_url+"uploads/comprobantes_anexo5b/"+compro;
        imgdet2 = {type:"pdf", url: base_url+"Transaccion/file_delete/"+id+"/terceros_5b/"+name_col, caption: compro, key:id};
        typePDF2 = "true";
      }
    }
    $("#"+name+"").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "pdf", "jpeg"],
      initialPreview: [
        ''+imgprev2+'',    
      ],
      initialPreviewAsData: typePDF2,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        imgdet2
      ],
    });
  }
  else{
    name="compro_nvo";
    $("."+name+"").fileinput({
      overwriteInitial: true,
      maxFileSize: 1500,
      showClose: false,
      showCaption: false,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: '',
      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["jpg", "png", "jpeg","pdf", "webp"],
      initialPreview: [
      ],
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        {type: "image", caption:"", key: 1},
      ]
    });
  }
  
}


function fileInputTercerosClone(name){
  //console.log("name en tercers clone: "+name);
  $("."+name+"").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "jpeg","pdf", "webp"],
    initialPreview: [
    ],
    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
    initialPreviewConfig: [
      {type: "image", caption:"", key: 1},
    ]
  });
}


function cambiaTipoTercero(val,id,clo){
  if(clo==0){
    if(val=="99"){
      $("#cont_desc_otro_"+id+"").show();
    }else{
      $("#cont_desc_otro_"+id+"").hide();
    }
  }else{
    if(val=="99"){
      $(".cont_desc_otro_"+id+"").show();
    }else{
      $(".cont_desc_otro_"+id+"").hide();
    }
  }
}

function cambiaPersonaTercero(tipo,id,clo){
  if(clo==0){
    if(tipo==1){
      $("#cont_persfi_ter_"+id+"").show();
      $("#cont_persmor_ter_"+id+"").hide();
      $("#cont_fide_ter_"+id+"").hide();
    }else if(tipo==2){
      $("#cont_persmor_ter_"+id+"").show();
      $("#cont_persfi_ter_"+id+"").hide();
      $("#cont_fide_ter_"+id+"").hide();
    }else if(tipo==3){
      $("#cont_persmor_ter_"+id+"").hide();
      $("#cont_persfi_ter_"+id+"").hide();
      $("#cont_fide_ter_"+id+"").show();
    }
  }else{
    if(tipo==1){
      $(".cont_persfi_ter_"+id+"").show();
      $(".cont_persmor_ter_"+id+"").hide();
      $(".cont_fide_ter_"+id+"").hide();
    }else if(tipo==2){
      $(".cont_persmor_ter_"+id+"").show();
      $(".cont_persfi_ter_"+id+"").hide();
      $(".cont_fide_ter_"+id+"").hide();
    }else if(tipo==3){
      $(".cont_persmor_ter_"+id+"").hide();
      $(".cont_persfi_ter_"+id+"").hide();
      $(".cont_fide_ter_"+id+"").show();
    }
  }
}

function cambiaPagoTercero(tipo,id,clo) {
  if(clo==0){
    if(tipo==1){
      $("#cont_apor_num_ter_"+id+"").show();
      $("#cont_apor_num_ter2_"+id+"").show();
      $("#cont_apor_esp_ter_"+id+"").hide();
    }else if(tipo==2){
      $("#cont_apor_num_ter_"+id+"").hide();
      $("#cont_apor_num_ter2_"+id+"").hide();
      $("#cont_apor_esp_ter_"+id+"").show();
    }
  }else{
    if(tipo==1){
      $(".cont_apor_num_ter_"+id+"").show();
      $(".cont_apor_num_ter2_"+id+"").show();
      $(".cont_apor_esp_ter_"+id+"").hide();
    }else if(tipo==2){
      $(".cont_apor_num_ter_"+id+"").hide();
      $(".cont_apor_num_ter2_"+id+"").hide();
      $(".cont_apor_esp_ter_"+id+"").show();
    }
  }
}

function datosDirSelect(id){
  $.ajax({
    type: "POST",
    url: base_url+"Clientes/data_record_inmuebleId5b",
    data: { "id": id },
    success: function (data) {
      var json = $.parseJSON(data);
      $("#cp").val(json[0].codigo_postal);
      $('#colonia').append('<option selected value="'+json[0].colonia+'" >'+json[0].colonia+ '</option');
      $("#calle").val(json[0].calle);
      $("#tipo_desarrollo").val(json[0].tipo_desarrollo);
      $("#descripcion_desarrollo").val(json[0].descripcion_desarrollo);
      var monto_desarrollo=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(json[0].monto_desarrollo);
      $("#monto_desarrollo").val(monto_desarrollo);
      $("#unidades_comercializadas").val(json[0].unidades_comercializadas);
      $("#costo_unidad").val(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(json[0].costo_unidad));
      if(json[0].otras_empresas=="SI")
        $("#otras_empresass").attr("checked",true);
      else if(json[0].otras_empresas=="NO")
        $("#otras_empresasn").attr("checked",true);
    }
  });
}

let cont_ter=0;
function agregarTercero(tis){
  cont_ter++;
  //console.log("cont_ter: "+cont_ter);
  //$row=$(".terceros_text").clone().removeClass("terceros_text").addClass("terceros_text_"+cont_ter+"");
  //$row=$(".terceros_text").clone().addClass("terceros_text_"+cont_ter+"");
  if($("#datos_tercero_principal").is(":visible")==true){
    $row=$("#datos_tercero_principal").clone().attr("id","datos_tercero_clone").addClass("datos_tercero_clone_"+cont_ter+"");
  }else{
    $row=$(".datos_tercero_clone1").clone().attr("id","datos_tercero_clone").addClass("datos_tercero_clone_"+cont_ter+"");
  }
  
  $row.find("#cont_tit_dt").show();

  $row.find("#tit_dt").html("Datos de Tercero agregado: ("+cont_ter+")");

  $row.find("#tipo_tercero").attr("onchange","cambiaTipoTercero(this.value,"+cont_ter+",1)");
  $row.find("#cont_desc_otro").addClass("cont_desc_otro_"+cont_ter+"");

  $row.find("#fisca_ter").attr("onchange","cambiaPersonaTercero(1,"+cont_ter+",1)");
  $row.find("#cont_persfi_ter").addClass("cont_persfi_ter_"+cont_ter+"");

  $row.find("#moral_ter").attr("onchange","cambiaPersonaTercero(2,"+cont_ter+",1)");
  $row.find("#cont_persmor_ter").addClass("cont_persmor_ter_"+cont_ter+"");

  $row.find("#fide_ter").attr("onchange","cambiaPersonaTercero(3,"+cont_ter+",1)");
  $row.find("#cont_fide_ter").addClass("cont_fide_ter_"+cont_ter+"");

  $row.find("#aport_num_ter").attr("onchange","cambiaPagoTercero(1,"+cont_ter+",1)");
  $row.find("#cont_apor_num_ter").addClass("cont_apor_num_ter_"+cont_ter+"");
  $row.find("#cont_apor_num_ter2").addClass("cont_apor_num_ter2_"+cont_ter+"");

  $row.find("#aport_esp_ter").attr("onchange","cambiaPagoTercero(2,"+cont_ter+",1)");
  $row.find("#cont_apor_esp_ter").addClass("cont_apor_esp_ter_"+cont_ter+"");

  //tipo de persona
  $row.find("#fisca_ter").attr("name","tipo_persona_clone"+cont_ter+"");
  $row.find("#moral_ter").attr("name","tipo_persona_clone"+cont_ter+"");
  $row.find("#fide_ter").attr("name","tipo_persona_clone"+cont_ter+"");

  //tipo de aportacion
  $row.find("#aport_num_ter").attr("name","aportacion_clone"+cont_ter+"");
  $row.find("#aport_esp_ter").attr("name","aportacion_clone"+cont_ter+"");

  $row.find("#cont_img_nvo1").html("");
  $row.find("#cont_img_nvo1").html('<h4>Comprobante 1:</h4>\
    <input class="name_comprobante1_'+cont_ter+'" name="name_comprobante" id="name_comprobante" type="hidden">\
    <div class="col-md-10">\
      <input name="name_nvo1_clone_'+cont_ter+'" onchange="changeImg(this.name,'+cont_ter+')" data-num_aux="1" width="50px" height="80px" data-idaux="0" class="img compro_nvo compro_nvo1" id="comprobante" type="file">\
    </div>');
  $row.find("#cont_img_nvo2").html("");
  $row.find("#cont_img_nvo2").html('<h4>Comprobante 2:</h4>\
    <input class="name_comprobante2_'+cont_ter+'" name="name_comprobante2" id="name_comprobante2" type="hidden">\
    <div class="col-md-10">\
      <input name="name_nvo2_clone_'+cont_ter+'" onchange="changeImg(this.name,'+cont_ter+')" data-num_aux="2" width="50px" height="80px" data-idaux="0" class="img compro_nvo compro_nvo2" id="comprobante2" type="file">\
    </div>');
  $row.find("#cont_img_nvo3").html("");
  $row.find("#cont_img_nvo3").html('<h4>Comprobante 3:</h4>\
    <input class="name_comprobante3_'+cont_ter+'" name="name_comprobante3" id="name_comprobante3" type="hidden">\
    <div class="col-md-10">\
      <input name="name_nvo3_clone_'+cont_ter+'" onchange="changeImg(this.name,'+cont_ter+')" data-num_aux="3" width="50px" height="80px" data-idaux="0" class="img compro_nvo compro_nvo3" id="comprobante3" type="file">\
    </div>');

  $row.find(".compro_nvo1").addClass("compro_nvo1_clone_"+cont_ter+"");
  $row.find(".compro_nvo2").addClass("compro_nvo2_clone_"+cont_ter+"");
  $row.find(".compro_nvo3").addClass("compro_nvo3_clone_"+cont_ter+"");
  /*$(".compro_nvo1_clone_"+cont_ter+"").fileinput("reset");
  $(".compro_nvo2_clone_"+cont_ter+"").fileinput("reset");
  $(".compro_nvo3_clone_"+cont_ter+"").fileinput("reset");*/

  $row.find("input").val("");  
  $row.find(".agregar_tercero").html("");
  $row.find("#cont_add_tro").hide();
  $row.find(".elimina_tercero").attr("onclick","remove_tercero($(this),"+cont_ter+")").html("<i class='fa fa-trash-o'></i> Eliminar Tercero");

  //console.log("cont_ter: "+cont_ter);
  if(cont_ter==1){
    if ($('#datos_tercero_principal').is(':visible')==true) {
      $row.insertAfter("#datos_tercero_principal");
    }else{
      //$row.insertAfter("#solo_bton");
      $row.insertAfter(".datos_tercero_clone1");
    }
    //$row.insertAfter("#table_servicios");
  }
  else{
    cont_ter_ant = cont_ter-1;
    if ($('#datos_tercero_principal').is(':visible')==true) {
      $row.insertAfter(".datos_tercero_clone_"+cont_ter_ant+"");
    }else{
      $row.insertAfter(".datos_tercero_clone1");
    }
  }

  name="compro_nvo1_clone_"+cont_ter+"";
  fileInputTercerosClone(name);
  name2="compro_nvo2_clone_"+cont_ter+"";
  fileInputTercerosClone(name2);
  name3="compro_nvo3_clone_"+cont_ter+"";
  fileInputTercerosClone(name3);

}

function remove_tercero(i,cont_ter_row){
  if(cont_ter_row>0){
    i.closest(".datos_tercero_clone_"+cont_ter_row+"").remove();
    cont_ter--;
  }else if(cont_ter_row==0){
    i.closest("#datos_tercero_principal").remove();
  }
  //console.log("cont_ter_row desde remove: "+cont_ter_row);
}

function eliminarTercero(id){
  swal({
    title: "¿Desea eliminar este registro?",
    text: "Se eliminará del formulario",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Eliminar",
    closeOnConfirm: true
  },function () {
    $.ajax({
      type: "POST",
      url: base_url+"Transaccion/elimina_tercero",
      data: { id: id },
      async: false,
      success: function(data) {
          $('#datos_tercero_'+id).remove();
      }
    });
  });
}

function guardarTerceros(id){
  var DATA  = [];
  var TABLA = $("#tabla_tercs tbody > tr"); 
  if(TABLA.length>0){
    TABLA.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[name*='id']").val();
        item ["num_aporta_tercero"] = $(this).find("input[name*='num_aporta_tercero']").val();
        item ["tipo_tercero"] = $(this).find("select[name*='tipo_tercero'] option:selected").val();
        item ["descrip_tercero"] = $(this).find("input[name*='descrip_tercero']").val();
        if($(this).find("input[id*='fisca_ter']").is(":checked")==true)
          item ["tipo_persona"] =1;
        else if($(this).find("input[id*='moral_ter']").is(":checked")==true)
          item ["tipo_persona"] =2;
        else if($(this).find("input[id*='fide_ter']").is(":checked")==true)
          item ["tipo_persona"] =3;
        item ["nombre"] = $(this).find("input[name*='nombre']").val();
        item ["app"] = $(this).find("input[name*='app']").val();
        item ["apm"] = $(this).find("input[name*='apm']").val();
        item ["fecha_nac"] = $(this).find("input[name*='fecha_nac']").val();
        item ["pais_nacionalidad"] = $(this).find("select[name*='pais_nacionalidad'] option:selected").val();
        item ["rfc"] = $(this).find("input[name*='rfc']").val();
        item ["curp"] = $(this).find("input[name*='curp']").val();
        item ["actividad_ocupa"] = $(this).find("select[name*='actividad_ocupa'] option:selected").val();
        item ["razon_social"] = $(this).find("input[name*='razon_social']").val();
        item ["fecha_consti"] = $(this).find("input[name*='fecha_consti']").val();
        item ["pais_nacion_moral"] = $(this).find("select[name*='pais_nacion_moral'] option:selected").val();
        item ["nombre_moral"] = $(this).find("input[name*='nombre_moral']").val();
        item ["app_moral"] = $(this).find("input[name*='app_moral']").val();
        item ["apm_moral"] = $(this).find("input[name*='apm_moral']").val();
        item ["fecha_nac_moral"] = $(this).find("input[name*='fecha_nac_moral']").val();
        item ["rfc_moral"] = $(this).find("input[name*='rfc_moral']").val();
        item ["curp_moral"] = $(this).find("input[name*='curp_moral']").val();
        item ["razon_social_fide"] = $(this).find("input[name*='razon_social_fide']").val();
        item ["rfc_fide"] = $(this).find("input[name*='rfc_fide']").val();
        item ["curp_fide"] = $(this).find("input[name*='curp_fide']").val();

        //item ["aportacion"] = $(this).find("input[name*='aportacion']").val();
        if($(this).find("input[id*='aport_num_ter']").is(":checked")==true){
          item ["aportacion"]=1;
        }else{
          item ["aportacion"]=2;
        }
        item ["instrum_notario"] = $(this).find("select[name*='instrum_notario'] option:selected").val();
        item ["tipo_moneda"] = $(this).find("select[name*='tipo_moneda'] option:selected").val();
        //item ["aport_fide"] = $(this).find("input[name*='aport_fide']").val();
        if($(this).find("input[name*='aport_fide']").is(":checked")==true) 
          item ["aport_fide"]="on";
        else
          item ["aport_fide"]="off";
        item ["nombre_insti"] = $(this).find("input[name*='nombre_insti']").val();
        item ["descrip_bien"] = $(this).find("input[name*='descrip_bien']").val();
        item ["monto_aporta"] = $(this).find("input[name*='monto_aporta']").val();
        item ["valor_pactado"] = $(this).find("input[name*='valor_pactado']").val();
        item ["comprobante"] = $(this).find("input[name*='name_comprobante']").val();
        item ["comprobante2"] = $(this).find("input[name*='name_comprobante2']").val();
        item ["comprobante3"] = $(this).find("input[name*='name_comprobante3']").val();

        item ["id_anexo"] = id;
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    /*console.log("INFO: "+INFO);
    console.log("aInfo: "+aInfo);
    console.log("DATA: "+DATA);*/
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url+'Transaccion/submitTerceros5b',
        processData: false, 
        contentType: false,
        async: false,
        success: function(data2){

        }
    });
  }
}

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function personas(){
  if($("#aportacion_fideicomiso").is(':checked')){ 
    $("#cont_nom_insti").show();
  }else{
    $("#cont_nom_insti").hide();
  }

  if($("#fisca").is(':checked')){ 
    $("#cont_persfi").show();
    $("#cont_persmor").hide();
    $("#cont_fide").hide();
  }
  if($("#moral").is(':checked')){ 
    $("#cont_persmor").show();
    $("#cont_persfi").hide();
    $("#cont_fide").hide();
  }
  if($("#fideicomiso").is(':checked')){ 
    $("#cont_persmor").hide();
    $("#cont_persfi").hide();
    $("#cont_fide").show();
  }

  if($("#nacional").is(':checked')){ 
    $("#cont_dir_ext").hide();
  }else{
    $("#cont_dir_ext").show();
  }


  if($("#fisca_ter").is(':checked')){ 
    $("#cont_persfi_ter").show();
    $("#cont_persmor_ter").hide();
    $("#cont_fide_ter").hide();
  }
  if($("#moral_ter").is(':checked')){ 
    $("#cont_persmor_ter").show();
    $("#cont_persfi_ter").hide();
    $("#cont_fide_ter").hide();
  }
  if($("#fide_ter").is(':checked')){ 
    $("#cont_persmor_ter").hide();
    $("#cont_persfi_ter").hide();
    $("#cont_fide_ter").show();
  }

  if($("#fisica_nof").is(':checked')){
    $("#cont_persfi_nof").show();
    $("#cont_persmor_nof").hide();
    $("#cont_fide_nof").hide();
  }

  if($("#moral_nof").is(':checked')){
    $("#cont_persmor_nof").show();
    $("#cont_persfi_nof").hide();
    $("#cont_fide_nof").hide();
  }

  if($("#fide_nof").is(':checked')){
    $("#cont_persmor_nof").hide();
    $("#cont_persfi_nof").hide();
    $("#cont_fide_nof").show();
  }

}

function ValidCurp(T){
  let ex=/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
  valor=T.value;
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "CURP No valido", "error");
    }
  }
}

function ValidRfc(T){
  let ex=/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;
  valor=T.value;
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "RFC No valido", "error");
    }
  }
}

function verificarLimite(){
  efectivo = $("#total_liquida_efect").val().replace("$", '');
  efectivo = efectivo.replace(",", '');
  //efectivo = $("#total_liquida_efect").val();
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var array= JSON.parse(result);
      var limite = parseFloat(array[0].limite_efectivo).toFixed(2);
      var na = array[0].na;
      //console.log("limite: "+limite);
      //console.log("na: "+na);

      if(limite>0 && na==0){
        var anio = $("#anio").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Umas/getUmasAnio',
          data: { anio: anio},
          success: function(data2){
            valor_uma=data2;
            valor_uma = parseFloat(valor_uma).toFixed(2);
            //console.log("valor de umas año: "+valor_uma);
            var uma_efect_anexo = efectivo / valor_uma;
            if(uma_efect_anexo>limite){
              lim_efec_band=true;
              swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
              verificarConfigLimite(1);
              $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
            }else{
              lim_efec_band=false;
              verificarConfigLimite(0);
              $('.limite_efect_msj').html('');
            }
          }
        });
      }//if na
    }
  });
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function cambiaDivisa(tipo_moneda){
  result_divi=0;
  $.ajax({
    type: 'POST',
    url : base_url+'Divisas/getDivisaTipo',
    async: false,
    data: { tipo: tipo_moneda, fecha: $("#fecha_aporta").val() },
    success: function(result_divi){
      valor_divi=parseFloat(result_divi).toFixed(2);
      result_divi=valor_divi;
    }
  });
  return result_divi;
}

function guardar(){
  var form_register = $('#form_anexo5b');
  var error_register = $('.alert-danger', form_register);
  var success_register = $('.alert-success', form_register);
  var $validator1=form_register.validate({
          errorElement: 'div', //default input error message container
          errorClass: 'vd_red', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          ignore: "",
          rules: {
            fecha_operacion:{
              required: true
            },
            referencia:{
              required: true,
              minlength: 1,
              maxlength: 14
            },
            /*monto_opera:{
              required: true
            },*/
            anio_acuse:{
              required: true
            },
            mes_acuse:{
              required: true
            },

            objeto_aviso_anterior:{
              required: true
            },
            modificacion:{
              required: true
            },
            registro_licencia:{
              required: true
            },             
            codigo_postal:{
              required: true
            },              
            colonia:{
              required: true
            },              
            calle:{
              required: true
            },              
            tipo_desarrollo:{
              required: true
            },    
            /*descripcion_desarrollo:{
              required: true
            }, */             
            monto_desarrollo:{
              required: true
            },           
            unidades_comercializadas:{
              required: true
            },             
            costo_unidad:{
              required: true
            },     
            otras_empresas:{
              required: true
            },
            fecha_aporta:{
              required: true
            },

            /*rfc_socio:{
              required: true,
              rfc: true
            },
            rfc:{
              required: true,
              rfc: true
            },
            rfc_moral:{
              required: true,
              rfc: true
            },
            rfc_fide:{
              required: true,
              rfc: true
            },*/
          },
          errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
              element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
            }else {
              error.insertAfter(element);
            }
          }, 
          invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register,-100);
          },
          highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
          },
          unhighlight: function (element) { // revert the change dony by hightlight
            $(element).closest('.control-group').removeClass('error'); // set error class to the control group
          },
          success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
          }
  });
  //////////Registro///////////
  var valid = form_register.valid();
  var band_aviso = 0;
  var colonia_inp="";
  var num_socios="";
  var aporta_num="";
  if(valid){
    //console.log("valid");
    name_table = "anexo5b";
    if($("#aviso").val()>0){
      name_table = "anexo5b_aviso";
    }

    if($("#aviso").val()>0){
      if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
        band_aviso=1;
      }   
    }
    if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
      var formulario = "";
      var aporta="";
      var validado_ext="";
      var valid_form_ext="";
      //if($("#tipo_aportacion1").is(':checked')){
      if($("#tipo_aportacion").val()==1){  
        formulario = $('#form_aporta1').serialize();
        aporta=1;
        if($("#aportacion_numerario_especie1").is(":checked")==true){
          aporta_num=1;
        }if($("#aportacion_numerario_especie2").is(":checked")==true){
          aporta_num=2;
        }

        valid_form_ext = form_register.valid();
      }
      //else if($("#tipo_aportacion2").is(':checked')){
      else if($("#tipo_aportacion").val()==2){  
        var form_register2 = $('#form_aporta2');
        var error_register2 = $('.alert-danger', form_register2);
        var success_register2 = $('.alert-success', form_register2);
        var $validator1=form_register2.validate({
          errorElement2: 'div', //default input error message container
          errorClass2: 'vd_red', // default input error message class
          focusInvalid2: false, // do not focus the last invalid input
          ignore: "",
          rules: {
            rfc_socio:{
              rfc: true
            },
            rfc:{
              rfc: true
            },
            /*curp:{
              curp: true
            },*/
            rfc_moral:{
              rfc: true
            },
            /*curp_moral:{
              curp: true
            },*/
            rfc_fide:{
              rfc: true
            },
            /*curp_fide:{
              curp: true
            },*/
          },
          errorPlacement: function(error, element2) {
            if (element2.parent().hasClass("vd_checkbox") || element2.parent().hasClass("vd_radio") || element2.is(":radio")){
              element2.parent().append(error);
            } else if (element2.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element2.parent());
            }else {
              error.insertAfter(element2);
            }
          }, 
          invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register2.fadeOut(200);
            error_register2.fadeIn(200);
            scrollTo(form_register2,-50);
          },
          highlight: function (element2) { // hightlight error inputs
            //$(element2).addClass('vd_bd-red');
            $(element2).addClass('vd_bd-red').css("color", "red");
            $(element2).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
          },
          unhighlight: function (element2) { // revert the change dony by hightlight
            $(element2).closest('.control-group').removeClass('error'); // set error class to the control group
          },
          success: function (label, element2) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element2).removeClass('vd_bd-red');
          }
        });
        var valid_form_ext = form_register2.valid();
        if(valid_form_ext){
          formulario = $('#form_aporta2').serialize();
          aporta=2;
          //validado_ext=1;
          if($("#extrangero").is(":checked")){
            colonia_inp = $("#colonia_dir2").val();
          }else if($("#nacional").is(":checked")){
            colonia_inp = $("#colonia_dir").val();
          }
          num_socios = $("#num_socios").val();
        }
      }
      //else if($("#tipo_aportacion3").is(':checked')){
      else if($("#tipo_aportacion").val()==3){ 
        //console.log("tipo_aportacion: 3"); 
        //$('#form_aporta3').removeData('validator');
        form_register = $('#form_aporta3');
        error_register = $('.alert-danger', form_register);
        success_register = $('.alert-success', form_register);
        $validator1=form_register.validate({
          errorElement: 'div', //default input error message container
          errorClass: 'vd_red', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          rules: {
            //comentados así, temporalmente el dia 05-10-2023
            rfc:{
              rfc: true
            },
            /*curp:{
              curp: true
            },*/
            rfc_moral:{
              rfc: true
            },
            /*curp_moral:{
              curp: true
            },*/
            rfc_fide:{
              //rfc: true
            },
            /*curp_fide:{
              curp: true
            },*/
            num_aporta_tercero:{
              required: true
            },
            tipo_tercero:{
              required: true
            },
            /*descrip_tercero:{
              required: true
            },*/
            tipo_persona:{
              required: true
            },
            aportacion:{
              required: true
            },
          },
          errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
              element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
            }else {
              error.insertAfter(element);
            }
          }, 
          invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(200);
            error_register.fadeIn(200);
            scrollTo(form_register,-50);
          },
          highlight: function (element) { // hightlight error inputs
            //$(element2).addClass('vd_bd-red');
            $(element).addClass('vd_bd-red').css("color", "red");
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
          },
          unhighlight: function (element) { // revert the change dony by hightlight
            $(element).closest('.control-group').removeClass('error'); // set error class to the control group
          },
          success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
          }
        });
        var valid_form_ext = form_register.valid();

        if($("#fisca_ter").is(":checked")==true){
          $('#form_aporta3').removeData('validator');
            $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
              rfc:{
                rfc: true
              },
              /*curp:{
                curp: true
              },*/
              curp:{
                required: true
              },
              nombre:{
                required: true
              },
              app:{
                required: true
              },
              apm:{
                required: true
              },
              fecha_nac:{
                required: true
              },
              pais_nacionalidad:{
                required: true
              },
              actividad_ocupa:{
                required: true
              }
            },
            errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
              }else {
                error.insertAfter(element);
              }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(200);
              error_register.fadeIn(200);
              scrollTo(form_register,-50);
            },
            highlight: function (element) { // hightlight error inputs
              //$(element2).addClass('vd_bd-red');
              $(element).addClass('vd_bd-red').css("color", "red");
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
              $(element).closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
            }
          });
        }
        var valid_form_pf = form_register.valid();
        ////////////////TERMINA VALID PERSONA FISICA//////////////////

        if($("#moral_ter").is(":checked")==true){
          $('#form_aporta3').removeData('validator');
            $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
              rfc_moral:{
                rfc: true
              },
              /*curp_moral:{
                curp: true
              },*/
              curp_moral:{
                required: true
              },
              razon_social:{
                required: true
              },
              fecha_consti:{
                required: true
              },
              pais_nacion_moral:{
                required: true
              },
              nombre_moral:{
                required: true
              },
              app_moral:{
                required: true
              },
              apm_moral:{
                required: true
              },
              fecha_nac_moral:{
                required: true
              }
            },
            errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
              }else {
                error.insertAfter(element);
              }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(200);
              error_register.fadeIn(200);
              scrollTo(form_register,-50);
            },
            highlight: function (element) { // hightlight error inputs
              //$(element2).addClass('vd_bd-red');
              $(element).addClass('vd_bd-red').css("color", "red");
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
              $(element).closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
            }
          });
        }
        var valid_form_pm = form_register.valid();
        ////////////////TERMINA VALID PERSONA MORAL//////////////////

        if($("#fide_ter").is(":checked")==true){
          $('#form_aporta3').removeData('validator');
          $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
              rfc_fide:{
                rfc: true
              },
              /*curp_fide:{
                curp: true
              },*/
              curp_fide:{
                required: true
              },
              razon_social_fide:{
                required: true
              },
              aportacion:{
                required: true
              },
              instrum_notario:{
                required: true
              },
              tipo_moneda:{
                required: true
              },
              /*aport_fide:{
                required: true
              },*/
              nombre_insti:{
                required: true
              },
              monto_aporta2:{
                required: true
              }
            },
            errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
              }else {
                error.insertAfter(element);
              }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(200);
              error_register.fadeIn(200);
              scrollTo(form_register,-50);
            },
            highlight: function (element) { // hightlight error inputs
              //$(element2).addClass('vd_bd-red');
              $(element).addClass('vd_bd-red').css("color", "red");
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
              $(element).closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
            }
          });
        }
        var valid_form_pfi = form_register.valid();
        ////////////////TERMINA VALID PERSONA FIDE//////////////////

        ///////////////////////////////////////////////////////////////
        if(valid_form_ext && valid_form_pf && $("#fisca_ter").is(":checked")==true || valid_form_ext && valid_form_pm && $("#moral_ter").is(":checked")==true || valid_form_ext && valid_form_pfi && $("#fide_ter").is(":checked")==true){
          formulario = $('#form_aporta3').serialize();
          aporta=3;
          console.log("aporta: "+aporta);
        }
      }
      //else if($("#tipo_aportacion4").is(':checked')){
      else if($("#tipo_aportacion").val()==4){  
        $('#form_aporta4').removeData('validator');
        form_register = $('#form_aporta4');
        error_register = $('.alert-danger', form_register);
        success_register = $('.alert-success', form_register);
        $validator1=form_register.validate({
          errorElement: 'div', //default input error message container
          errorClass: 'vd_red', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          rules: {
            tipo_inst:{
              required: true
            },
            insti_financiamiento:{
              required: true
            },
            tipo_credito:{
              required: true
            },
            monto_prestamo:{
              required: true
            },
            plazo:{
              required: true
            },
            tipo_moneda:{
              required: true
            }
          },
          errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
              element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
            }else {
              error.insertAfter(element);
            }
          }, 
          invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(200);
            error_register.fadeIn(200);
            scrollTo(form_register,-50);
          },
          highlight: function (element) { // hightlight error inputs
            //$(element2).addClass('vd_bd-red');
            $(element).addClass('vd_bd-red').css("color", "red");
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
          },
          unhighlight: function (element) { // revert the change dony by hightlight
            $(element).closest('.control-group').removeClass('error'); // set error class to the control group
          },
          success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
          }
        });
        var valid_form_ext = form_register.valid();
        if(valid_form_ext){
          //formulario = $('#form_aporta4').serialize();
          aporta=4;
          console.log("aporta: "+aporta);
        }
      }
      //else if($("#tipo_aportacion5").is(':checked')){
      else if($("#tipo_aportacion").val()==5){  
        $('#form_aporta5').removeData('validator');
        var form_register = $('#form_aporta5');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        if($("#fisica_nof").is(":checked")==true){
          $('#form_aporta5').removeData('validator');
          $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
              monto_prestamo:{
                required: true
              },
              plazo:{
                required: true
              },
              tipo_moneda:{
                required: true
              },
              tipo_persona:{
                required: true
              },
              nombre:{
                required: true
              },
              app:{
                required: true
              },
              apm:{
                required: true
              },
              fecha_nac:{
                required: true
              },
              pais_nacionalidad:{
                required: true
              },
              rfc:{
                rfc: true
              },
              actividad_ocupa:{
                required: true
              }
            },
            errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
              }else {
                error.insertAfter(element);
              }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(200);
              error_register.fadeIn(200);
              scrollTo(form_register,-50);
            },
            highlight: function (element) { // hightlight error inputs
              //$(element2).addClass('vd_bd-red');
              $(element).addClass('vd_bd-red').css("color", "red");
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
              $(element).closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
            }
          });
          var valid_form_pf = form_register.valid();
        }else{
          var valid_form_pf = false;
        }
        
        ////////////////TERMINA VALID PERSONA FISICA//////////////////
        var valid_form_ext = form_register.valid();
        if($("#moral_nof").is(":checked")==true){
          $('#form_aporta5').removeData('validator');
          $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
              rfc_moral:{
                rfc: true
              },
              curp_moral:{
                required: true
              },
              razon_social:{
                required: true
              },
              fecha_consti:{
                required: true
              },
              pais_nacion_moral:{
                required: true
              },
              nombre_moral:{
                required: true
              },
              app_moral:{
                required: true
              },
              apm_moral:{
                required: true
              },
              fecha_nac_moral:{
                required: true
              }
            },
            errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
              }else {
                error.insertAfter(element);
              }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(200);
              error_register.fadeIn(200);
              scrollTo(form_register,-50);
            },
            highlight: function (element) { // hightlight error inputs
              //$(element2).addClass('vd_bd-red');
              $(element).addClass('vd_bd-red').css("color", "red");
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
              $(element).closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
            }
          });
          var valid_form_pm = form_register.valid();
        }else{
          var valid_form_pm = false;
        }
        
        ////////////////TERMINA VALID PERSONA MORAL//////////////////

        if($("#fide_nof").is(":checked")==true){
          $('#form_aporta5').removeData('validator');
          $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
              rfc_fide:{
                rfc: true
              },
              curp_fide:{
                required: true
              },
              razon_social_fide:{
                required: true
              },
            },
            errorPlacement: function(error, element) {
              if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
              } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
              }else {
                error.insertAfter(element);
              }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(200);
              error_register.fadeIn(200);
              scrollTo(form_register,-50);
            },
            highlight: function (element) { // hightlight error inputs
              //$(element2).addClass('vd_bd-red');
              $(element).addClass('vd_bd-red').css("color", "red");
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
              $(element).closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
              label
                  .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
              $(element).removeClass('vd_bd-red');
            }
          });
          var valid_form_pfi = form_register.valid();
        }else{
          var valid_form_pfi = false;
        }
        
        ////////////////TERMINA VALID PERSONA FIDE//////////////////

        /*console.log("valid_form_ext: "+valid_form_ext);
        console.log("valid_form_pf: "+valid_form_pf);
        console.log("valid_form_pm: "+valid_form_pm);
        console.log("valid_form_pfi: "+valid_form_pfi);*/

        if(valid_form_ext==true && valid_form_pf==true && $("#fisica_nof").is(":checked")==true || valid_form_ext==true && valid_form_pm==true && $("#moral_nof").is(":checked")==true || valid_form_ext==true && valid_form_pfi==true && $("#fide_nof").is(":checked")==true){
          formulario = $('#form_aporta5').serialize()+"&monto_prestamo="+$("input[id*='monto_prestamo2']").val();
          aporta=5;
          console.log("aporta: "+aporta);
        }
      }
      //else if($("#tipo_aportacion6").is(':checked')){
      else if($("#tipo_aportacion").val()==6){ 
        $('#form_aporta6').removeData('validator');
        form_register = $('#form_aporta6');
        error_register = $('.alert-danger', form_register);
        success_register = $('.alert-success', form_register);
        $validator1=form_register.validate({
          errorElement: 'div', //default input error message container
          errorClass: 'vd_red', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          rules: {
            fecha_emision:{
              required: true
            },
            monto_estimado:{
              required: true
            },
            monto_recibido:{
              required: true
            }
          },
          errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
              element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
              error.insertAfter(element.parent());
            }else {
              error.insertAfter(element);
            }
          }, 
          invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(200);
            error_register.fadeIn(200);
            scrollTo(form_register,-50);
          },
          highlight: function (element) { // hightlight error inputs
            //$(element2).addClass('vd_bd-red');
            $(element).addClass('vd_bd-red').css("color", "red");
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
          },
          unhighlight: function (element) { // revert the change dony by hightlight
            $(element).closest('.control-group').removeClass('error'); // set error class to the control group
          },
          success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
          }
        });
        var valid_form_ext = form_register.valid();
        if(valid_form_ext){
          formulario = $('#form_aporta6').serialize();
          aporta=6;
          console.log("aporta: "+aporta);
        }
      }
      if(valid_form_ext && aporta!=""){
        let id_ane=$("#id_anexo").val();
        //falta mandar los tipo aportacions
        otras_empresas="";
        if($("#tipo_aportacion").val()==5){ 
          if($("#otras_empresass").is(":checked")==true){
            otras_empresas="SI";
          }else{
            otras_empresas="NO";
          }
        }
        $.ajax({
          type: "POST",
          url: base_url+"index.php/Transaccion/submit5b",
          data: $('#form_anexo5b').serialize()+formulario+"&id_anexo="+id_ane+"&tabla="+name_table+"&fecha_aporta="+$("#fecha_aporta").val()+"&tipo_aportacion="+aporta+"&colonia_dir="+colonia_inp+"&num_socios="+num_socios+"&aportacion_numerario_especie="+aporta_num+"&otras_empresas="+otras_empresas,
          async:false,
          statusCode:{
            404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
              swal("Error!", "500", "error");
            }
          },
          //agregar beforsend
          beforeSend: function(){
            $("#btn_submit").attr("disabled",true);
          },
          success: function (result) {
            //console.log(result);
            var array = $.parseJSON(result);
            var id=array.id; /*var monto=parseFloat(array.monto);*/ var id_clientec_transac=array.id_clientec_transac;
            var valor_uma=0;
            //var addtpHF=0; var umas_anexoH=0; 
            var umas_anexoTot_historico=0; let umas_anexoTot_historicoFinal=0;
            ///////////////////////////////////////
            if($("#tipo_aportacion").val()==3){
              guardarTerceros(id);
            }

            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              data: { anio: $("#fecha_aporta").val()},
              async:false,
              success: function(data2){
                valor_uma=data2;
                valor_uma = parseFloat(valor_uma).toFixed(2);
              }
            });

            $("input[name*='id_aviso']").val(id);
            //console.log("monto operacion: "+monto);
            var anio = $("#anio").val();
            var band_umbral=false;
            var addtp=0; var addtpD=0;
            var umas_anexo=0;
            $.ajax({
              type: 'POST',
              url : base_url+'Umbrales/getUmbral',
              data: {anexo: $("#id_actividad").val()},
              async:false,
              success: function(result){
                //var array = $.parseJSON(result);
                var array= JSON.parse(result);
                var aviso = parseFloat(array[0].aviso).toFixed(2);
                var siempre_avi=array[0].siempre_avi; //band 1
                var siempre_iden=array[0].siempre_iden; //band 1
                var identifica=array[0].identificacion; //montos

                if(lim_efec_band==true){
                  swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
                  $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
                }else{
                  lim_efec_band=false;
                  $('.limite_efect_msj').html('');
                }
                //console.log("valor de uma: "+valor_uma);
                let monto=0;

                if($("#tipo_aportacion option:selected").val()==1){ //Datos cuando la aportación es a través de recursos propios 
                  if($("#aportacion_numerario_especie1").is(":checked")){ //numeraria
                    if($("#tipo_moneda_1 option:selected").val()==0){ //pesos
                      monto = replaceAll($("#monto_aportacion").val(), ",", "" );
                      monto = replaceAll(monto, "$", "" );
                      monto = parseFloat(monto).toFixed(2);
                    }
                    else{
                      divi = cambiaDivisa($("#tipo_moneda_1 option:selected").val()); //divisa
                      monto = replaceAll($("#monto_aportacion").val(), ",", "" );
                      monto = replaceAll(monto, "$", "" );
                      monto = divi * parseFloat(monto).toFixed(2);
                    }
                  }else{ // en especie
                    monto = replaceAll($("#monto_estimado").val(), ",", "" );
                    monto = replaceAll(monto, "$", "" );
                    monto = parseFloat(monto).toFixed(2);
                  }
                }
                else if($("#tipo_aportacion option:selected").val()==2){ //Datos cuando la aportación es a través de socios 
                  if($("#aport_num").is(":checked")){ //numeraria
                    if($(".tipo_moneda_2 option:selected").val()==0){ //pesos
                      monto = replaceAll($("#monto_aporta").val(), ",", "" );
                      monto = replaceAll(monto, "$", "" );
                      monto = parseFloat(monto).toFixed(2);
                    }
                    else{
                      divi = cambiaDivisa($(".tipo_moneda_2 option:selected").val()); //divisa
                      monto = replaceAll($("#monto_aporta").val(), ",", "" );
                      monto = replaceAll(monto, "$", "" );
                      monto = divi * parseFloat(monto).toFixed(2);
                    }
                  }else{ //en especie
                    monto = replaceAll($("#monto_aporta").val(), ",", "" );
                    monto = replaceAll(monto, "$", "" );
                    monto = parseFloat(monto).toFixed(2);
                  }
                }
                else if($("#tipo_aportacion option:selected").val()==3){ //Terceros 
                  //console.log("datos de terceros");
                  let TABLA2 = $("#tabla_tercs tbody > tr");              
                  TABLA2.each(function(){ //entra a recorrido
                    //console.log("aport_num del tercero: "+$(this).find("input[id*='aport_num_ter']").is(":checked"));
                    //console.log("aport_num_ter: "+$(this).find("input[id*='aport_num_ter']").is(":checked"));
                    if($(this).find("input[id*='aport_num_ter']").is(":checked")==true){ //numeraria
                      if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
                        //console.log("pesos de Liquidación ");
                        var monto_operacion = $(this).find("input[id*='monto_aporta2']").val();
                        var fecha_pago = $("input[id*='fecha_aporta']").val();
                        //console.log("monto_operacion de pesos de Liquidación :"+monto_operacion);
                        if(monto_operacion.indexOf('$') != -1 || monto_operacion.indexOf(',') != -1){
                          tot_mo = replaceAll(monto_operacion, ",", "" );
                          tot_mo = replaceAll(tot_mo, "$", "" );
                        }else{
                          tot_mo=monto_operacion;
                        }
                        //console.log("tot_mo sin $ de pesos de Liquidación :"+tot_mo);
                        tot_mo = parseFloat(tot_mo).toFixed(2);
                        addtp += Number(tot_mo);
                        //console.log("addtp de pesos de Liquidación :"+addtp);
                        umas_anexo += addtp/valor_uma;
                        //console.log("umas_anexo de pesos de Liquidación :"+umas_anexo);
                      }
                      if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){
                        //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
                        console.log("moneda != pesos de Liquidación");
                        var monto_operacion = $(this).find("input[id*='monto_aporta2']").val();
                        var fecha_pago = $("input[id*='fecha_aporta']").val();
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Divisas/getDivisaTipo',
                          async: false,
                          data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(), fecha:$("input[id*='fecha_aporta']").val()},
                          success: function(result_divi){
                            valor_divi=parseFloat(result_divi);
                            if(monto_operacion.indexOf('$') != -1){
                              tot_moD = replaceAll(monto_operacion, ",", "" );
                              tot_moD = replaceAll(tot_moD, "$", "" );
                            }else{
                              tot_moD = monto_operacion;
                            }
                            tot_moD = parseFloat(tot_moD);
                            console.log("valor2 de tot_moD: "+tot_moD);
                            var vstotal1D = tot_moD*valor_divi;
                            console.log("valor2 de vstotal1D: "+vstotal1D);

                            addtpD += Number(vstotal1D);
                            umas_anexo += addtpD/valor_uma;
                          }
                        });
                      }
                    }else{ //en especie
                      monto = $(this).find("input[id*='monto_aporta2']").val();
                      //console.log("monto: "+$(this).find("input[id*='monto_aporta2']").val());
                      if(monto.indexOf('$') != -1 || monto.indexOf(',') != -1){
                        monto = replaceAll(monto, ",", "" );
                        monto = replaceAll(monto, "$", "" );
                      }
                      //console.log("monto sin $ en especie :"+monto);
                      monto = parseFloat(monto).toFixed(2);
                      umas_anexo += monto/valor_uma;
                      //console.log("en especie del tercero");
                      //console.log("monto del tercero: "+monto);
                      //console.log("umas_anexo del tercero: "+umas_anexo);
                    }
                  });
                }
                else if($("#tipo_aportacion option:selected").val()==4){ //Crédito o préstamo financiero  
                  if($("#tipo_moneda_4 option:selected").val()==0){ //pesos
                    monto = replaceAll($("input[id*='monto_prestamo']").val(), ",", "" );
                    monto = replaceAll(monto, "$", "" );
                    monto = parseFloat(monto).toFixed(2);
                  }
                  else{
                    divi = cambiaDivisa($("#tipo_moneda_4 option:selected").val()); //divisa
                    monto = replaceAll($("input[id*='monto_prestamo']").val(), ",", "" );
                    monto = replaceAll(monto, "$", "" );
                    monto = divi * parseFloat(monto).toFixed(2);
                  }
                }
                else if($("#tipo_aportacion option:selected").val()==5){ //Crédito o préstamo no financiero  
                  if($(".tipo_moneda_5 option:selected").val()==0){ //pesos
                    monto = replaceAll($("input[id*='monto_prestamo2']").val(), ",", "" );
                    monto = replaceAll(monto, "$", "" );
                    monto = parseFloat(monto).toFixed(2);
                    console.log("monto: "+monto);
                  }
                  else{
                    divi = cambiaDivisa($(".tipo_moneda_5 option:selected").val()); //divisa
                    monto = replaceAll($("input[id*='monto_prestamo2']").val(), ",", "" );
                    monto = replaceAll(monto, "$", "" );
                    monto = divi * parseFloat(monto).toFixed(2);
                  }
                }else if($("#tipo_aportacion option:selected").val()==6){ //Aportación a través de financiamiento bursátil (emisión de deuda capital) 
                  monto = replaceAll($("#monto_recibido").val(), ",", "" );
                  monto = replaceAll(monto, "$", "" );
                  monto = parseFloat(monto).toFixed(2);
                }
                if($("#tipo_aportacion option:selected").val()!=3){
                  umas_anexo = monto / valor_uma; 
                }
                umas_anexo = parseFloat(umas_anexo).toFixed(2);
                //console.log("valor de umas_anexo final: "+umas_anexo);
                //console.log("valor de umas aviso: "+aviso);

                umas_anexo = umas_anexo*1;
                aviso = aviso*1;
                //console.log("aviso: "+aviso);
                if(siempre_avi=="1"){
                  band_umbral=true;
                  $.ajax({ //cambiar pago a acusado
                    type: 'POST',
                    url : base_url+'Operaciones/acusaPago2',
                    async: false,
                    data: { id:id, act:$("#id_actividad").val()},
                    success: function(result_divi){

                    }
                  });
                }else{
                  if(umas_anexo>=aviso){
                    band_umbral=true;
                    $.ajax({ //cambiar pago a acusado
                    type: 'POST',
                    url : base_url+'Operaciones/acusaPago2',
                    async: false,
                    data: { id:id, act:$("#id_actividad").val()},
                    success: function(result_divi){

                    }
                  });
                  }
                }
                //traer pagos de otras transacciones pero del mismo anexo sin acusar
                id_acusado=0;
                var id_element = [];
                if(umas_anexo<aviso){
                  $.ajax({
                    type: 'POST',
                    url : base_url+'Operaciones/getOpera',
                    async: false,
                    data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id, id_perfilamiento: $("#id_perfilamiento").val() },
                    success: function(result){
                      var data= $.parseJSON(result);
                      var datos = data.data;
                      var tot_moDH=0; var tot_moH=0;
                      datos.forEach(function(element) {
                        var addtpHF=0; var umas_anexoH=0; let addtpH=0;
                        /*if(element.tipo_moneda!=0){ //divisa
                          $.ajax({
                            type: 'POST',
                            url : base_url+'Divisas/getDivisaTipo',
                            async: false,
                            data: { tipo: element.tipo_moneda, fecha: $("#fecha_aporta").val() },
                            success: function(result_divi){
                              valor_divi=parseFloat(result_divi);
                              if(element.monto_aportacion!="" && element.monto_aportacion>0)
                                tot_moDH = parseFloat(element.monto_aportacion);
                              else if(element.monto_estimado!="" && element.monto_estimado>0)
                                tot_moDH = parseFloat(element.monto_estimado);
                              else if(element.monto_aporta!="" && element.monto_aporta>0)
                                tot_moDH = parseFloat(element.monto_aporta);
                              else if(element.monto_prestamo!="" && element.monto_prestamo>0)
                                tot_moDH = parseFloat(element.monto_prestamo);
                              else if(element.monto_recibido!="" && element.monto_recibido>0)
                                tot_moDH = parseFloat(element.monto_recibido);

                              //console.log("valor2 de tot_moD: "+tot_moD);
                              console.log("valor_divi: "+valor_divi);
                              var totalHD = tot_moDH*valor_divi;
                              totalHD = totalHD.toFixed(2);
                              addtpHF += Number(totalHD);
                              console.log("addtpHF: "+addtpHF);

                              $.ajax({
                                type: 'POST',
                                url : base_url+'Umas/getUmasAnio',
                                async: false,
                                data: { anio: $("#fecha_aporta").val()},
                                success: function(data2){
                                  valor_uma=parseFloat(data2).toFixed(2);
                                  console.log("valor de uma en historico: "+valor_uma);
                                  umas_anexoH += parseFloat(addtpHF/valor_uma);
                                  console.log("resultado de operacion de umas_anexoH: "+umas_anexoH);
                                }
                              });

                            }
                          });
                        }//if de divisa
                        else{ //en pesos
                          if(element.monto_aportacion!="" && element.monto_aportacion>0)
                            tot_moH = parseFloat(element.monto_aportacion);
                          else if(element.monto_estimado!="" && element.monto_estimado>0)
                            tot_moH = parseFloat(element.monto_estimado);
                          else if(element.monto_aporta!="" && element.monto_aporta>0)
                            tot_moH = parseFloat(element.monto_aporta);
                          else if(element.monto_prestamo!="" && element.monto_prestamo>0)
                            tot_moH = parseFloat(element.monto_prestamo);
                          else if(element.monto_recibido!="" && element.monto_recibido>0)
                            tot_moH = parseFloat(element.monto_recibido);

                          totalHD = tot_moH.toFixed(2);
                          addtpHF += Number(totalHD); 

                          console.log("totalHD: "+totalHD);
                          $.ajax({
                            type: 'POST',
                            url : base_url+'Umas/getUmasAnio',
                            async: false,
                            data: { anio: $("#fecha_aporta").val() },
                            success: function(data2){
                              valor_uma=parseFloat(data2).toFixed(2);
                              console.log("valor de uma en historico: "+valor_uma);
                              umas_anexoH += parseFloat(addtpHF/valor_uma);
                            }
                          });
                        }
                        addtpHF=parseFloat(addtpHF).toFixed(2);
                        umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                        umas_anexoH=umas_anexoH*1;*/

                        if(element.tipo_aportacion==1){ //Datos cuando la aportación es a través de recursos propios 
                          if(element.aportacion_numerario_especie==1){ //numeraria
                            if(element.tipo_moneda_==0) //pesos
                              tot_moDH = element.monto_aportacion.toFixed(2);
                            else{
                              divi = cambiaDivisa(element.tipo_moneda_); //divisa
                              tot_moDH = divi * parseFloat(element.monto_estimado).toFixed(2);
                            }
                          }else{ // en especie
                            tot_moDH = parseFloat(element.monto_estimado).toFixed(2);
                          }
                        }
                        else if(element.tipo_aportacion==2){ //Datos cuando la aportación es a través de socios 
                          if(element.aport_num==1){ //numeraria
                            if(element.tipo_moneda==0) //pesos
                              tot_moDH = parseFloat(element.monto_aporta).toFixed(2);
                            else{
                              divi = cambiaDivisa(element.tipo_moneda); //divisa
                              tot_moDH = divi * parseFloat(element.monto_aporta).toFixed(2);
                            }
                          }else{ //en especie
                            tot_moDH = parseFloat(element.monto_aporta).toFixed(2);
                          }
                        }
                        else if(element.tipo_aportacion==3){ //Terceros 
                          //console.log("tipo de aportacion: "+element.tipo_aportacion);
                          //console.log("aportacion: "+element.aportacion_t);
                          if(element.aportacion_t==1){ //numeraria
                            if(element.tipo_moneda_t=="0"){ //en pesos
                              //console.log("pesos de Liquidación ");
                              var monto_operacion = element.monto_aporta_t;
                              var fecha_pago = element.fecha_aporta;
                              /*tot_moH=monto_operacion;
                              tot_moH = parseFloat(tot_moH);
                              var vstotal1H = tot_moH;*/
                              //console.log("valor de monto_operacion de historico: "+monto_operacion);
                              addtpH += Number(monto_operacion);

                              umas_anexoH += addtpH/valor_uma;
                              //console.log("valor de umas_anexoH: "+umas_anexoH);
                            }
                            if(element.tipo_moneda_t!="0"){
                              //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
                              //console.log("moneda != pesos de Liquidación");
                              var monto_operacion = element.monto_aporta_t;
                              //var fecha_pago = $("input[id*='fecha_aporta']").val();
                              var fecha_pago = element.fecha_aporta;
                              $.ajax({
                                type: 'POST',
                                url : base_url+'Divisas/getDivisaTipo',
                                async: false,
                                data: { tipo: element.tipo_moneda_t, fecha:$("input[id*='fecha_aporta']").val()},
                                success: function(result_divi){
                                  valor_divi=parseFloat(result_divi);

                                  if(monto_operacion.indexOf('$') != -1 || monto_operacion.indexOf(',') != -1){
                                    tot_moD = replaceAll(monto_operacion, ",", "" );
                                    tot_moD = replaceAll(tot_moD, "$", "" );
                                  }else{
                                    tot_moD = monto_operacion;
                                  }
                                  tot_moD = parseFloat(tot_moD);
                                  //console.log("valor2 de tot_moD: "+tot_moD);
                                  var vstotal1DH = tot_moD*valor_divi;
                                  //console.log("valor2 de vstotal1DH: "+vstotal1DH);

                                  addtpDH += Number(vstotal1DH);
                                  umas_anexoH += addtpDH/valor_uma;
                                  //console.log("valor2 de umas_anexoH de divisa: "+umas_anexoH);
                                }
                              });
                            }
                          }else{ //en especie
                            tot_moDH += parseFloat(element.monto_aporta_t).toFixed(2);
                            umas_anexoH += tot_moDH/valor_uma;
                            //console.log("valor2 de umas_anexoH de especie: "+umas_anexoH);
                          }
                          
                        }
                        else if(element.tipo_aportacion==4){ //Crédito o préstamo financiero  
                          if(element.tipo_moneda==0) //pesos
                            tot_moDH = parseFloat(element.monto_prestamo).toFixed(2);
                          else{
                            divi = cambiaDivisa(element.tipo_moneda); //divisa
                            tot_moDH = divi * parseFloat(element.monto_prestamo).toFixed(2);
                          }
                        }
                        else if(element.tipo_aportacion==5){ //Crédito o préstamo no financiero  
                          if(element.tipo_moneda==0) //pesos
                            tot_moDH = parseFloat(element.monto_prestamo).toFixed(2);
                          else{
                            divi = cambiaDivisa(element.tipo_moneda); //divisa
                            tot_moDH = divi * parseFloat(element.monto_prestamo).toFixed(2);
                          }
                        }else if(element.tipo_aportacion ==6){ //Aportación a través de financiamiento bursátil (emisión de deuda capital) 
                          tot_moDH = parseFloat(element.monto_recibido).toFixed(2);
                        }
                        if(element.tipo_aportacion!=3){
                          umas_anexoH = tot_moDH / valor_uma; 
                        }

                        umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                        umas_anexoH=umas_anexoH*1;

                        var opera_comph=0; var cont_hist=0; var band_comple=0; var id_ant=0;
                        
                        //if(identifica<=umas_anexoH || siempre_iden=="1"){
                        if(identifica<=umas_anexoH || siempre_iden=="1" /* || identifica>umas_anexo*/){ 

                          if(umas_anexoH>=identifica || siempre_iden=="1"){
                            //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                            umas_anexoTot_historico += umas_anexoH;
                          }
                          else{
                            umas_anexoTot_historico = umas_anexoH;
                          }

                          //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                          umas_anexoTot_historicoFinal = umas_anexoTot_historico+umas_anexo;
                          //console.log("umas_anexoTot_historicoFinal: "+umas_anexoTot_historicoFinal);

                          if(umas_anexoTot_historico<aviso && band_comple==0){
                            id_ant = element.id;
                          }

                          //console.log("id de pagos que ayudan, consulta operacion: "+element.id_t);

                          item = {}; 
                          item ["id_desa"]=element.id;
                          item ["id_desa_t"]=element.id_t;
                          id_element.push(item);
                          INFO  = new FormData();
                          aInfo = JSON.stringify(id_element);
                          INFO.append('data', aInfo);

                          //console.log("aInfo: "+aInfo);

                          id_acusado=0;
                          if(umas_anexoTot_historicoFinal>=aviso){
                            $.ajax({ //cambiar pago a acusado
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPago',
                              async: false,
                              data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1},
                              success: function(result2){
                                id_acusado=result2;
                              }
                            });
                            $.ajax({ //cambiar pago a acusado
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPagoActividad5bArray',
                              async: false,
                              data: "data="+aInfo+"&act"+$("#id_actividad").val()+"&id_anexo="+id+"&aviso="+$("#aviso").val()+"&pago_ayuda=1"+"&id_ant="+id_ant,
                              success: function(result2){
                                id_acusado=result2;
                                console.log("acusado: "+id_acusado);
                              }
                            });
                          }
                        }
                        /*else{
                          umas_anexoTot_historico = umas_anexo;
                        }*/
                        //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                        if(umas_anexoTot_historicoFinal>=aviso){
                          band_comple=1;
                          band_umbral=true;
                        }
                      });//foreach
                    }//success de  get opera
                  });
                }

                //console.log("band_umbral: "+band_umbral);
                setTimeout(function () {
                  if(band_umbral==true && band_umbral!=undefined && !isNaN(umas_anexo) && !isNaN(parseInt(umas_anexo))){ //697,212.00
                    //if(validar_xml==1){
                      if($("#aviso").val()>0){
                        window.location.href = base_url+"Transaccion/anexo5b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                      }else{
                        window.location.href = base_url+"Transaccion/anexo5b_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                      }
                      
                      swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                      /*if(history.back()!=undefined){
                        setTimeout(function () { history.back() }, 1500);
                      }*/
                      if($("#aviso").val()>0){
                        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                      }else{
                        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                      }
                    /*}else{
                      swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                      if($("#aviso").val()>0){
                        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                      }else{
                        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                      }  
                    }*/
                  }else{
                    swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                    /*if(history.back()!=undefined){
                      setTimeout(function () { history.back() }, 1500);
                    }*/
                    if($("#aviso").val()>0){
                      setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                    }else{
                      setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                    }
                  }
              }, 3500); //esperar a la sumatoria
          
              }//success  
            });  //ajax
          }
        });  
      }else{
        //swal("Error!", "Revise que los datos sean correctos", "error");
      }
    }//band aviso
    else{
      swal("Error!", "Los campos de Aviso son obligatorios", "error");
    }
  }//valid
}

function confirmarPagosMonto(){ //para sumar el efectivo nadamas
  suma_efect=0; let suma_esp=0;
  contta=0;
  valor_divi=0;
  montoe=0;
  montone=0;

  if($("#tipo_aportacion").val()==1){
    if($("#aportacion_numerario_especie1").is(":checked")==true){ //aportacio numeraria
      if($("select[name*='instrumento_monetario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()=="0"){ //en efectivo y en pesos
        var monto = $("input[id*='monto_aportacion']").val();
        if(monto.indexOf('$') != -1){
          montoe = monto.replace("$", '');
          montoe = montoe.replace(",", '');
        }else{
          montoe=monto;
        }
        montoe = parseFloat(montoe);
        suma_efect += Number(montoe);
      }if($("select[name*='instrumento_monetario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()!="0"){//efectivo y en otra moneda
        monto_opera = $("input[id*='monto_aportacion']").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $("select[name*='tipo_moneda'] option:selected").val(),fecha:$("#fecha_aporta").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            valor_divi = parseFloat(valor_divi);
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = monto_opera.replace("$", '');
              tot_moDE = tot_moDE.replace(",", '');
            }else{
              tot_moDE = monto_opera;
            }
            tot_moDE = parseFloat(tot_moDE);
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }
        });
      }  
    }
  }
  if($("#tipo_aportacion").val()==2){
    if($("#aport_num").is(":checked")==true){ //aportacio numeraria
      if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[class*='tipo_moneda_2'] option:selected").val()=="0"){ //en efectivo y en pesos
        var monto = $("#monto_aporta").val();
        //console.log("monto: "+monto);
        var comp = $("#monto_aporta").val().indexOf("$");
        //console.log("comp : "+comp);
        if(comp== -1){
          montoe = monto.replace("$", '');
          montoe = montoe.replace(",", '');
          //console.log("monto sin $: "+montoe);
        }else{
          montoe=monto;
        }

        /*if(monto.indexOf('$') != -1 && monto!=""){
          montoe = monto.replace("$", '');
          console.log("monto sin $: "+montoe);
          montoe = montoe.replace(",", '');
        }else{
          montoe=monto;
        }*/
        montoe = parseFloat(montoe);
        //console.log("montoe: "+montoe);
        if(montoe>0){
          suma_efect += Number(montoe);
        }
      }if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[class*='tipo_moneda_2'] option:selected").val()!="0"){//efectivo y en otra moneda
        //monto_opera = $("input[id*='monto_aporta']").val();
        monto_opera=$("#monto_aporta").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $("select[class*='tipo_moneda_2'] option:selected").val(),fecha:$("#fecha_aporta").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            valor_divi = parseFloat(valor_divi);

            var comp = $("#monto_aporta").val().indexOf("$");
            //console.log("comp : "+comp);
            if(comp== -1){
              tot_moDE = monto_opera.replace("$", '');
              tot_moDE = tot_moDE.replace(",", '');
              //console.log("monto sin $: "+montoe);
            }else{
              tot_moDE=monto_opera;
            }

            /*if(monto_opera.indexOf('$') != -1 && monto_opera!=""){
              tot_moDE = monto_opera.replace("$", '');
              tot_moDE = tot_moDE.replace(",", '');
            }else{
              tot_moDE = monto_opera;
            }*/
            tot_moDE = parseFloat(tot_moDE);
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }
        });
      }  
    }
  }
  console.log("suma_efect: "+suma_efect);
  //console.log("montoe: "+montoe);
  if($("#tipo_aportacion").val()==3){ //terceros
    var TABLA = $("#tabla_tercs tbody > tr"); 
    TABLA.each(function(){   
      if($(this).find("input[id*='aport_num_ter']").is(":checked")==true){ //aportacio numeraria      
        if($(this).find("select[name*='instrum_notario'] option:selected").val()=="1" && $(this).find("select[name*='tipo_moneda'] option:selected").val()=="0"){ //en efectivo y en pesos
          var monto = $(this).find("input[id*='monto_aporta2']").val();
          if(monto.indexOf('$') != -1 || monto.indexOf(',') != -1){
            montoe = monto.replace("$", '');
            montoe = montoe.replace(",", '');
          }else{
            montoe=monto;
          }
          montoe = parseFloat(montoe);
          suma_efect += Number(montoe);
          //suma_efect = parseFloat(suma_efect).toFixed(2);
          //console.log("terceros en pesos. suma_efect: "+suma_efect);
        }if($(this).find("select[name*='instrum_notario'] option:selected").val()=="1" && $(this).find("select[name*='tipo_moneda'] option:selected").val()!="0" && $(this).find("input[id*='monto_aporta2']").val()!=""){//efectivo y en otra moneda
          monto_opera = $(this).find("input[id*='monto_aporta2']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { tipo: $(this).find("select[name*='tipo_moneda'] option:selected").val(),fecha:$("#fecha_aporta").val()},
            success: function(result_divi){
              valor_divi=result_divi;
              valor_divi = parseFloat(valor_divi).toFixed(2);
              console.log("valor_divi de terceros: "+valor_divi);

              if(monto_opera.indexOf('$') != -1 || monto_opera.indexOf(',') != -1){
                tot_moDE = monto_opera.replace("$", '');
                tot_moDE = tot_moDE.replace(",", '');
              }else{
                tot_moDE = monto_opera;
              }
              tot_moDE = parseFloat(tot_moDE);
              var preefect = tot_moDE*valor_divi;
              suma_efect += Number(preefect);
              //suma_efect = parseFloat(suma_efect).toFixed(2);
              //console.log("terceros con divisa. suma_efect: "+suma_efect);
            }
          });
          
        }
      }else if($(this).find("input[id*='aport_esp_ter']").is(":checked")==true){ //en especie
        var monto_esp = $(this).find("input[id*='monto_aporta2']").val();
        if(monto_esp.indexOf('$') != -1 || monto_esp.indexOf(',') != -1){
          montoe = monto_esp.replace("$", '');
          montoe = montoe.replace(",", '');
        }else{
          montoe=monto_esp;
        }
        montoe = parseFloat(montoe);
        suma_esp += Number(montoe);
        //suma_esp = parseFloat(suma_esp).toFixed(2);
        //console.log("terceros. suma_esp: "+suma_esp);
      } 
      id_ter = $(this).find("input[id*='id_terceros']").val();
      compro = $("#name_comprobante_"+id_ter+"").val();
      name="comprobante_"+id_ter+"";
      fileInputTerceros($(this).find("input[id*='id_terceros']").val(),compro,name,1);
      
      compro2 = $("#name_comprobante2_"+id_ter+"").val();
      name2="comprobante2_"+id_ter+"";
      fileInputTerceros($(this).find("input[id*='id_terceros']").val(),compro2,name2,2);

      compro3 = $("#name_comprobante3_"+id_ter+"").val();
      name3="comprobante3_"+id_ter+"";
      fileInputTerceros($(this).find("input[id*='id_terceros']").val(),compro3,name3,3);
    });

    /*if($("#aport_num_ter").is(":checked")==true){ //aportacio numeraria
      if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()=="0"){ //en efectivo y en pesos
        var monto = $("input[id*='monto_aporta2']").val();
        if(monto.indexOf('$') != -1){
          montoe = monto.replace("$", '');
          montoe = montoe.replace(",", '');
        }else{
          montoe=monto;
        }
        montoe = parseFloat(montoe);
        suma_efect += Number(montoe);
      }if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()!="0"){//efectivo y en otra moneda
        monto_opera = $("input[id*='monto_aporta2']").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $("select[name*='tipo_moneda'] option:selected").val(),fecha:$("#fecha_aporta").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            valor_divi = parseFloat(valor_divi);
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = monto_opera.replace("$", '');
              tot_moDE = tot_moDE.replace(",", '');
            }else{
              tot_moDE = monto_opera;
            }
            tot_moDE = parseFloat(tot_moDE);
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
            //console.log("terceros con divisa. suma_efect: "+suma_efect);
          }
        });
      }  
    }*/
  }
  if($("#tipo_aportacion").val()==4){ //credito o prestamo financiero
    /*if($("#aport_num_ter").is(":checked")==true){ //aportacio numeraria
      if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()=="0"){ //en efectivo y en pesos
        var monto = $("input[id*='monto_prestamo']").val();
        if(monto.indexOf('$') != -1){
          montoe = monto.replace("$", '');
          montoe = montoe.replace(",", '');
        }else{
          montoe=monto;
        }
        suma_efect += Number(montoe);
      }if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()!="0"){//efectivo y en otra moneda
        monto_opera = $("input[id*='monto_prestamo']").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $("select[name*='tipo_moneda'] option:selected").val(),fecha:$("#fecha_operacion").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = monto_opera.replace("$", '');
              tot_moDE = tot_moDE.replace(",", '');
            }else{
              tot_moDE = monto_opera;
            }
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }
        });
      }  
    }*/
  }
  if($("#tipo_aportacion").val()==5){ //credito o prestamo no financiero
    /*if($("#aport_num_ter").is(":checked")==true){ //aportacio numeraria
      if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()=="0"){ //en efectivo y en pesos
        var monto = $("input[id*='monto_prestamo2']").val();
        if(monto.indexOf('$') != -1){
          montoe = monto.replace("$", '');
          montoe = montoe.replace(",", '');
        }else{
          montoe=monto;
        }
        suma_efect += Number(montoe);
      }if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()!="0"){//efectivo y en otra moneda
        monto_opera = $("input[id*='monto_prestamo2']").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $("select[name*='tipo_moneda'] option:selected").val(),fecha:$("#fecha_operacion").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = monto_opera.replace("$", '');
              tot_moDE = tot_moDE.replace(",", '');
            }else{
              tot_moDE = monto_opera;
            }
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }
        });
      }  
    }*/
  }
  if($("#tipo_aportacion").val()==6){ //aportacion atraves de financiamiento
    /*if($("#aport_num_ter").is(":checked")==true){ //aportacio numeraria
      if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()=="0"){ //en efectivo y en pesos
        var monto = $("input[id*='monto_prestamo2']").val();
        if(monto.indexOf('$') != -1){
          montoe = monto.replace("$", '');
          montoe = montoe.replace(",", '');
        }else{
          montoe=monto;
        }
        suma_efect += Number(montoe);
      }if($("select[name*='instrum_notario'] option:selected").val()=="1" && $("select[name*='tipo_moneda'] option:selected").val()!="0"){//efectivo y en otra moneda
        monto_opera = $("input[id*='monto_prestamo2']").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $("select[name*='tipo_moneda'] option:selected").val(),fecha:$("#fecha_operacion").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = monto_opera.replace("$", '');
              tot_moDE = tot_moDE.replace(",", '');
            }else{
              tot_moDE = monto_opera;
            }
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }
        });
      }  
    }*/
  }
  setTimeout(function () { 
    suma_efect = parseFloat(suma_efect).toFixed(2);
    suma_esp = parseFloat(suma_esp).toFixed(2);
    subtotal = Number(suma_efect);
    subtotal_esp = Number(suma_esp);
    total = subtotal+subtotal_esp;
    //console.log("total: "+total);
    var aux_mo=0;
    /*var totalmontomo = $('#monto_opera').val().replace("$", '');
    totalmontomo = totalmontomo.replace(",", '');

    var vstotalmo = totalmontomo;
    aux_mo = Number(vstotalmo);
    if(aux_mo==total){
      validar_xml=1;
      $('.validacion_cantidad').html('');
    }else{
      validar_xml=0;
      $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
    }*/
    validar_xml=1;
    $("#total_liquida_efect").val(convertMoneda(subtotal));
    $("#total_liquida").val(convertMoneda(total));
    verificarLimite();
  }, 1500);
}

function convertMoneda(num){
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
  return monto;
}

function muestraTerceroTable(){
  console.log("muestraTerceroTable: ");
  if($('#tipo_aportacion').val()=="3"){
    //if($("#id_anexo").val()==0){
      $('.terceros_text').css('display','block');
    //}
    if($("#id_anexo").val()!=0){
      //$('#datos_tercero_principal').remove();
      $('#solo_bton').css('display','block');
    }
  }else{
    $('.terceros_text').css('display','none');   
  }
}
  

function select_aportacion(){
  if($('#tipo_aportacion').val()==1){
    $('.recursos_propios').css('display','block');
  }else{
    $('.recursos_propios').css('display','none');   
  }
  if($('#tipo_aportacion').val()==2){
     $('.socios_text').css('display','block');
  }else{
    $('.socios_text').css('display','none');
    //$('.terceros_text').attr('checked',false);
  }
  if($('#tipo_aportacion').val()==3){
    //if($("#id_anexo").val()==0){
      $('.terceros_text').css('display','block');
    //}
    if($("#id_anexo").val()!=0){
      //$('#datos_tercero_principal').remove();
      $('#solo_bton').css('display','block');
    }
    fileInputTerceros();
  }else{
    $('.terceros_text').css('display','none');   
  }
  if($('#tipo_aportacion').val()==4){
    $('.credito_finan_text').css('display','block');
  }else{
    $('.credito_finan_text').css('display','none');   
  }
  if($('#tipo_aportacion').val()==5){
    $('.credito_nofinan_text').css('display','block');
  }else{
    $('.credito_nofinan_text').css('display','none');   
  }
  if($('#tipo_aportacion').val()==6){
    $('.finan_burs').css('display','block');
  }else{
    $('.finan_burs').css('display','none');   
  }

  /*if($('#tipo_aportacion1').is(':checked')){
    $('.recursos_propios').css('display','block');
  }else{
    $('.recursos_propios').css('display','none');   
  }
  if($('#tipo_aportacion2').is(':checked')){
     $('.socios_text').css('display','block');
  }else{
    $('.socios_text').css('display','none');
    //$('.terceros_text').attr('checked',false);
  }
  if($('#tipo_aportacion3').is(':checked')){
    $('.terceros_text').css('display','block');
  }else{
    $('.terceros_text').css('display','none');   
  }
  if($('#tipo_aportacion4').is(':checked')){
    $('.credito_finan_text').css('display','block');
  }else{
    $('.credito_finan_text').css('display','none');   
  }
  if($('#tipo_aportacion5').is(':checked')){
    $('.credito_nofinan_text').css('display','block');
  }else{
    $('.credito_nofinan_text').css('display','none');   
  }
  if($('#tipo_aportacion6').is(':checked')){
    $('.finan_burs').css('display','block');
  }else{
    $('.finan_burs').css('display','none');   
  }*/
} 
function select_numerario_especie(){
  if($('#aportacion_numerario_especie1').is(':checked') && $('#tipo_aportacion').val()==1 /*$('#tipo_aportacion1').is(':checked')*/){
    $('.aportacion_numerario').css('display','block');
  }else{
    $('.aportacion_numerario').css('display','none');   
  }
  if($('#aportacion_numerario_especie2').is(':checked') && $('#tipo_aportacion').val()==1 /*$('#tipo_aportacion1').is(':checked')*/){
    $('.aportacion_especie').css('display','block');
  }else{
    $('.aportacion_especie').css('display','none');   
  }

  if($('#aport_num').is(':checked') && $('#tipo_aportacion').val()==2 /*$('#tipo_aportacion2').is(':checked')*/){
    $('#cont_apor_num').css('display','block');
  }else{
    $('#cont_apor_num').css('display','none');   
  }
  if($('#aport_espe').is(':checked') && $('#tipo_aportacion').val()==2 /*$('#tipo_aportacion2').is(':checked')*/){
    $('#cont_apor_esp').css('display','block');
  }else{
    $('#cont_apor_esp').css('display','none');   
  }
  
  if($('#aport_num_ter').is(':checked') && $('#tipo_aportacion').val()==3 /*$('#tipo_aportacion3').is(':checked')*/){
    $('#cont_apor_num_ter').css('display','block');
    $('#cont_apor_num_ter2').css('display','block');
  }else{
    $('#cont_apor_num_ter').css('display','none');   
    $('#cont_apor_num_ter2').css('display','none');
  }
  if($('#aport_esp_ter').is(':checked') && $('#tipo_aportacion').val()==3 /*$('#tipo_aportacion3').is(':checked')*/){
    $('#cont_apor_esp_ter').css('display','block');
  }else{
    $('#cont_apor_esp_ter').css('display','none');   
  }

}

function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}

function btn_registro_licencia_ayuda(){
  $('#modal_licencia_ayuda').modal();
}
function btn_descripcion_ayuda(){
  $('#modal_descripcion_ayuda').modal();
}
function btn_nsocios_ayuda(){
  $('#modal_nsocios_ayuda').modal();
}
function btn_plazo_ayuda(){
  $('#modal_plazo_ayuda').modal();
}

function cambiaCP(id){
  cp = $("#"+id+"").val();  
  if(cp!="" && id=="cp"){
    col = "colonia";
    var itemc = [];
    $('#colonia').select2({
      placeholder: {
          id: 0,
          text: 'Buscar colonia:'
      },
      width: '100%',
      allowClear: true,
      data: itemc
    });
    autoComplete("colonia");
  }else if(cp!="" && id=="cp_nacional"){
    col = "colonia_dir";
    var itemc = [];
    $('#colonia_dir').select2({
      placeholder: {
          id: 0,
          text: 'Buscar colonia:'
      },
      width: '100%',
      allowClear: true,
      data: itemc
    });
    autoComplete("colonia_dir");
  }
  /*console.log("cp cambiaCP: "+cp);
  console.log("col cambiaCP: "+col);*/
}
function autoComplete(idcol){
  if(idcol=="colonia"){
    cp = $("#cp").val(); 
    col = $('#colonia').val();  
  }
  else if(idcol=="colonia_dir"){
    cp = $("#cp_nacional").val(); 
    col = $('#colonia_dir').val();  
  }
  /*console.log("cp: "+cp);
  console.log("col: "+col);*/

  $('#'+idcol+'').select2({
      width: 'resolve',
      minimumInputLength: 0,
      minimumResultsForSearch: 10,
      placeholder: 'Seleccione una colonia:',
      ajax: {
          url: base_url+'Perfilamiento/getDatosCPSelect',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  cp: cp, 
                  col: col ,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.colonia,
                      text: element.colonia,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function regresar(){
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}
// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}