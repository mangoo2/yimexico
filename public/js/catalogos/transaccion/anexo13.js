var base_url = $('#base_url').val();
var validar_xml=0;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var siempre_avi;
$(document).ready(function() {
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
	check_donativo();
	ComboAnio();
	tipo_modeda(1);
  if($('#mes_aux').val()==''){
    $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
  }else{
    $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
  }
  $('#instrumento_monetario option[value="'+$('#instrumento_monetario_aux').val()+'"]').attr("selected", "selected");
  $('#instrumento_monetario_d option[value="'+$('#instrumento_monetario_d_aux').val()+'"]').attr("selected", "selected");
  ////////////Donativo Numerico
  var aux_pga=$('#aux_pga').val();
  var id_ax13=$('#id_ax13').val();
  if(aux_pga==1){
    if($('#donativo_num').is(':checked')){
      tabla_liquidacion_anexo13_num(id_ax13,$("#aviso").val());
    }
    if($('#donativo_esp').is(':checked')){
      tabla_liquidacion_anexo13_esp(id_ax13,$("#aviso").val());
    }

  }else{
    if($('#donativo_num').is(':checked')){
    agregar_liqui_numerico();
    }
    if($('#donativo_esp').is(':checked')){
    agregar_liqui_especie();
    }
  } 
  if(id_ax13==0){
    agregar_liqui_numerico();
    agregar_liqui_especie();
  }
  $("#fecha_operacion").on("change",function(){
    valorUma();
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });

  verificarLimite();
  verificarConfigLimite();
  setTimeout(function () { 
    confirmarPagosMonto();
  }, 1500);
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();

  ///////// 
});

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}
function check_donativo(){
	if($('#donativo_num').is(':checked')){
        $('.row_donativo_numerico').css('display','block');
	}else{
        $('.row_donativo_numerico').css('display','none');
	}
	if($('#donativo_esp').is(':checked')){
        $('.row_donativo_especie').css('display','block');
	}else{
        $('.row_donativo_especie').css('display','none');
	}
}
function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
function tipo_modeda(id){
  totot1 = replaceAll($('.monto_'+id).val(), ",", "" );
  totot2 = replaceAll(totot1, "$", "" );
  var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totot2);
  $('.monto_'+id).val(totalg);
  //$("#total_liquida").val($('.monto_'+id).val());
}
function verificarLimite(){
  efectivo = replaceAll($("#total_liquida_efect").val(), ",", "" );
  efectivo = replaceAll(efectivo, "$", "" );
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var array= JSON.parse(result);
      var limite = parseFloat(array[0].limite_efectivo).toFixed(2);
      var na = array[0].na;
      //console.log("limite: "+limite);
      //console.log("na: "+na);

      if(limite>0 && na==0){
        var anio = $("#anio").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Umas/getUmasAnio',
          data: { anio: anio},
          success: function(data2){
            valor_uma=data2;
            var valor_uma=parseFloat(valor_uma);
            //console.log("valor de umas año: "+valor_uma);
            var uma_efect_anexo = efectivo / valor_uma;
            if(uma_efect_anexo>limite){
              lim_efec_band=true;
              swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
              verificarConfigLimite(1);
              $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
            }else{
              lim_efec_band=false;
              verificarConfigLimite(0);
              $('.limite_efect_msj').html('');
            }
          }
        });
      }//if na
    }
  });
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function registrar(){
  var form_register = $('#form_anexo13');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
                referencia:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_anexo13").valid();
        var band_aviso = 0;
        if(valid) {
          monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
          monto_opera = replaceAll(monto_oper, "$", "" );
          
          if($("#aviso").val()>0){
            name_table = "anexo13_aviso";
          }else{
            name_table = "anexo13";
          }
          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }

          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo13',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = $.parseJSON(data);
                var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                ///////// Pagos liquidacion
                if($('#donativo_num').is(':checked')){
                  var DATAP= [];
                  var TABLAP = $(".row_donativo_numerico .donativo_numerico > div");                  
                  TABLAP.each(function(){         
                        item = {};
                        item ["idanexo13"]=id;
                        item ["aviso"]=$("#aviso").val();
                        item ["id"]=$(this).find("input[id*='id_a']").val();
                        item ["fecha_pago"]=$(this).find("input[id*='fecha_pago_a']").val();
                        item ["instrum_notario"]=$(this).find("select[id*='instrum_notario_a'] option:selected").val();
                        item ["tipo_moneda"]=$(this).find("select[id*='tipo_moneda_a'] option:selected").val();
                        montop = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
                        montop = replaceAll(montop, "$", "" );
                        item ["monto"]=montop;
                        DATAP.push(item);
                  });
                  INFOP  = new FormData();
                  aInfop   = JSON.stringify(DATAP);
                  INFOP.append('data', aInfop);
                  $.ajax({
                      data: INFOP,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_pago_anexo13_num',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  });
                }
                ///////// Pagos especie
                if($('#donativo_esp').is(':checked')){
                  var DATAP= [];
                  var TABLAP = $(".row_donativo_especie .donativo_especie > div");                  
                  TABLAP.each(function(){         
                        item = {};
                        item ["idanexo13"]=id;
                        item ["aviso"]=$("#aviso").val();
                        item ["id"]=$(this).find("input[id*='id_e']").val();
                        item ["tipo_moneda"]=$(this).find("select[id*='tipo_moneda_e']").val();
                        item ["tipo_material"]=$(this).find("select[id*='tipo_material_d'] option:selected").val();
                        item ["tipo_inmueble"]=$(this).find("select[id*='tipo_inmueble_e'] option:selected").val();
                        item ["folio"]=$(this).find("input[id*='folio_real']").val();
                        item ["codigo_postal"]=$(this).find("input[id*='codigo_postal']").val();
                        item ["descripcion_bien_donado"]=$(this).find("input[id*='descripcion_bien_donado']").val();
                        montop = replaceAll($(this).find("input[id*='monto_operacion']").val(), ",", "" );
                        montop = replaceAll(montop, "$", "" );
                        item ["monto_operacion"]=montop;
                        DATAP.push(item);
                  });
                  INFOP  = new FormData();
                  aInfop   = JSON.stringify(DATAP);
                  INFOP.append('data', aInfop);
                  $.ajax({
                      data: INFOP,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_pago_anexo13_esp',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  });
                } 
                ///////////////////////////  
                $("input[name*='id_aviso']").val(id);
                var anio = $("#anio").val();
                var valor_uma; var band_umbral=false;
                var addtp=0; var addtpD=0;
                var act = new Date();
                var mes_act = act.getMonth();
                var anio_act = act.getFullYear();
                var umas_anexo=0;
                //var addtpHF=0; var umas_anexoH=0; 
                var umas_anexoTot_historico=0;
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umbrales/getUmbral',
                  data: {anexo: $("#id_actividad").val()},
                  success: function(result){
                    //var array = $.parseJSON(result);
                    var array= JSON.parse(result);
                    var aviso = parseFloat(array[0].aviso).toFixed(2);
                    var siempre_avi=array[0].siempre_avi;
                    var siempre_iden=array[0].siempre_iden;
                    var identifica=array[0].identificacion;
                    //console.log("siempre avi: "+siempre_avi);
                    cont=0;
                    if($('#donativo_num').is(':checked')){
                      var TABLA = $(".row_donativo_numerico .donativo_numerico > div");               
                      TABLA.each(function(){        
                        if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
                          //console.log("pesos de Liquidación ");
                          var monto_operacion = $(this).find("input[id*='monto_a']").val();
                          var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
                          if(monto_operacion.indexOf('$') != -1){
                            tot_mo = replaceAll(monto_operacion, ",", "" );
                            tot_mo = replaceAll(tot_mo, "$", "" );
                          }else{
                            tot_mo=monto_operacion;
                          }
                          tot_mo = parseFloat(tot_mo);
                          var vstotal1 = tot_mo;
                          
                          let date = new Date(fecha_pago);
                          var mes_pago = date.getMonth();
                          mes_pago = parseInt(mes_pago);
                          mes_pago = mes_pago +1;
                          var anio_pago = date.getFullYear();
                          //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                            addtp += Number(vstotal1);
                          //}
                          //addtp += Number(vstotal1);
                          //if(addtp>0){
                            $.ajax({
                              type: 'POST',
                              url : base_url+'Umas/getUmasAnio',
                              data: { anio: fecha_pago},
                              async: false,
                              success: function(data2){
                                valor_uma=data2;
                                var valor_uma=parseFloat(valor_uma);
                                //console.log("valor uma en function umasAnio: "+valor_uma);
                                umas_anexo += addtp/valor_uma;
                                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                //console.log("valor1 de umas_anexo: "+umas_anexo);
                              }
                            });
                          //}
                        }
                        if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){
                          //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
                          //console.log("moneda != pesos de Liquidación");
                          var monto_operacion = $(this).find("input[id*='monto_a']").val();
                          var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
                          $.ajax({
                            type: 'POST',
                            url : base_url+'Divisas/getDivisaTipo',
                            async: false,
                            data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(), fecha:$(this).find("input[id*='fecha_pago_a']").val()},
                            success: function(result_divi){
                              valor_divi=result_divi;
                              valor_divi = parseFloat(valor_divi);
                              $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
                              //console.log("monto operacion: "+monto_operacion);
                              //console.log("valor_divi: "+valor_divi);
                              if(monto_operacion.indexOf('$') != -1){
                                tot_moD = replaceAll(monto_operacion, ",", "" );
                                tot_moD = replaceAll(tot_moD, "$", "" );
                              }else{
                                tot_moD = monto_operacion;
                              }
                              tot_moD = parseFloat(tot_moD);
                              //console.log("valor2 de tot_moD: "+tot_moD);
                              var vstotal1D = tot_moD*valor_divi;
                              //console.log("valor2 de vstotal1D: "+vstotal1D);

                              let date = new Date(fecha_pago);
                              var mes_pago = date.getMonth();
                              mes_pago = parseInt(mes_pago);
                              mes_pago = mes_pago +1;
                              var anio_pago = date.getFullYear();
                              //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                                addtpD += Number(vstotal1D);
                              //}
                              //addtpD += Number(vstotal1D);
                              //console.log("valor2 de addtpD: "+addtpD);
                              //if(addtp>0){ 
                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Umas/getUmasAnio',
                                  data: { anio: fecha_pago},
                                  async: false,
                                  success: function(data2){
                                    valor_uma=data2;
                                    var valor_uma=parseFloat(valor_uma);
                                    
                                    //console.log("valor uma en function umasAnio: "+valor_uma);
                                    umas_anexo += addtpD/valor_uma;
                                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                    //console.log("valor2 de umas_anexo: "+umas_anexo);
                                  }
                                });
                              //}
                            }
                          });
                        }
                        cont++;
                      });
                    }
                    if($('#donativo_esp').is(':checked')){
                      var TABLAP = $(".row_donativo_especie .donativo_especie > div");               
                      TABLAP.each(function(){        
                        if($(this).find("select[id*='tipo_moneda_e'] option:selected").val()=="0"){ //en pesos
                          //console.log("pesos de Liquidación ");
                          var monto_operacion = $(this).find("input[id*='monto_operacion']").val();
                          var fecha_pago = $("input[id*='fecha_operacion']").val();
                          if(monto_operacion.indexOf('$') != -1){
                            tot_mo = replaceAll(monto_operacion, ",", "" );
                            tot_mo = replaceAll(tot_mo, "$", "" );
                          }else{
                            tot_mo=monto_operacion;
                          }
                          tot_mo = parseFloat(tot_mo);
                          var vstotal1 = tot_mo;
                          
                          /*let date = new Date(fecha_pago);
                          var mes_pago = date.getMonth();
                          mes_pago = parseInt(mes_pago);
                          mes_pago = mes_pago +1;
                          var anio_pago = date.getFullYear();
                          if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                            addtp += Number(vstotal1);
                          }*/

                          addtp += Number(vstotal1);
                          //if(addtp>0){ 
                            $.ajax({
                              type: 'POST',
                              url : base_url+'Umas/getUmasAnio',
                              data: { anio: fecha_pago},
                              async: false,
                              success: function(data2){
                                valor_uma=data2;
                                var valor_uma=parseFloat(valor_uma);
                                
                                //console.log("valor uma en function umasAnio: "+valor_uma);
                                umas_anexo += addtp/valor_uma;
                                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                //console.log("valor1 de umas_anexo: "+umas_anexo);
                              }
                            });
                          //}
                        }
                        if($(this).find("select[id*='tipo_moneda_e'] option:selected").val()!="0"){
                          //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
                          //console.log("moneda != pesos de Liquidación");
                          var monto_operacion = $(this).find("input[id*='monto_operacion']").val();
                          var fecha_pago = $("input[id*='fecha_operacion']").val();
                          $.ajax({
                            type: 'POST',
                            url : base_url+'Divisas/getDivisaTipo',
                            async: false,
                            data: { tipo: $(this).find("select[id*='tipo_moneda_e'] option:selected").val(), fecha:fecha_pago},
                            success: function(result_divi){
                              valor_divi=result_divi;
                              valor_divi = parseFloat(valor_divi);
                              $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
                              //console.log("monto operacion: "+monto_operacion);
                              //console.log("valor_divi: "+valor_divi);
                              if(monto_operacion.indexOf('$') != -1){
                                tot_moD = replaceAll(monto_operacion, ",", "" );
                                tot_moD = replaceAll(tot_moD, "$", "" );
                              }else{
                                tot_moD = monto_operacion;
                              }
                              tot_moD = parseFloat(tot_moD);
                              //console.log("valor2 de tot_moD: "+tot_moD);
                              var vstotal1D = tot_moD*valor_divi;
                              //console.log("valor2 de vstotal1D: "+vstotal1D);
                              /*let date = new Date(fecha_pago);
                              var mes_pago = date.getMonth();
                              mes_pago = parseInt(mes_pago);
                              mes_pago = mes_pago +1;
                              var anio_pago = date.getFullYear();
                              if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                                addtpD += Number(vstotal1D);
                              }*/
                              addtpD += Number(vstotal1D);
                              //console.log("valor2 de addtpD: "+addtpD);
                              //if(addtpD>0){ 
                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Umas/getUmasAnio',
                                  data: { anio: fecha_pago},
                                  async: false,
                                  success: function(data2){
                                    valor_uma=data2;
                                    var valor_uma=parseFloat(valor_uma);
                                    //console.log("valor uma en function umasAnio: "+valor_uma);
                                    umas_anexo += addtpD/valor_uma;
                                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                    //console.log("valor2 de umas_anexo: "+umas_anexo);
                                  }
                                });
                              //}
                            }
                          });
                        }
                        cont++;
                      });
                    }

                    var time = cont*500;
                    setTimeout(function () { 
                      if(lim_efec_band==true){
                        swal("!Alerta!", "El efectivo de la transacción es mayor al límite permitido", "error");
                        $('.limite_efect_msj').html('El efectivo(UMAS) de la transacción es mayor al límite permitido.');
                      }else{
                        lim_efec_band=false;
                        $('.limite_efect_msj').html('');
                      }
    
                      umas_anexo = parseFloat(umas_anexo).toFixed(2);
                      /*console.log("valor de umas_anexo final: "+umas_anexo);
                      console.log("valor de umas aviso: "+aviso);*/
                      addtpF = addtp + addtpD;
                      //console.log("valor de addtpF: "+addtpF);

                      umas_anexo = umas_anexo*1;
                      aviso = aviso*1;
                      //console.log("aviso: "+aviso);
                      if(siempre_avi=="1"){
                        band_umbral=true;
                        $.ajax({ //cambiar pago a acusado
                          type: 'POST',
                          url : base_url+'Operaciones/acusaPago2',
                          async: false,
                          data: { id:id, act:$("#id_actividad").val()},
                          success: function(result_divi){

                          }
                        });
                      }else{
                        if(umas_anexo>=aviso){
                          band_umbral=true;
                          $.ajax({ //cambiar pago a acusado
                          type: 'POST',
                          url : base_url+'Operaciones/acusaPago2',
                          async: false,
                          data: { id:id, act:$("#id_actividad").val()},
                          success: function(result_divi){

                          }
                        });
                        }
                      }
                      id_acusado=0;
                      if(umas_anexo<aviso){
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Operaciones/getOpera',
                          async: false,
                          data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id, id_perfilamiento: $("#id_perfilamiento").val() },
                          success: function(result){
                            var data= $.parseJSON(result);
                            var datos = data.data;
                            datos.forEach(function(element) {
                              var addtpHF=0; var umas_anexoH=0;
                              if(element.tipo_moneda!=0){ //divisa
                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Divisas/getDivisaTipo',
                                  async: false,
                                  data: { tipo: element.tipo_moneda, fecha: element.fecha_pago },
                                  success: function(result_divi){
                                    valor_divi=parseFloat(result_divi);
                                    tot_moDH = parseFloat(element.monto_operacion);
                                    //console.log("valor2 de tot_moD: "+tot_moD);
                                    var totalHD = tot_moDH*valor_divi;
                                    totalHD = totalHD.toFixed(2);
                                    addtpHF += Number(totalHD);

                                    $.ajax({
                                      type: 'POST',
                                      url : base_url+'Umas/getUmasAnio',
                                      async: false,
                                      data: { anio: element.fecha_pago},
                                      success: function(data2){
                                        valor_uma=parseFloat(data2).toFixed(2);
                                        //console.log("valor de uma en historico: "+valor_uma);
                                        //umas_anexoH += parseFloat(addtpHF/valor_uma);
                                        precalc= addtpHF/valor_uma;
                                        if(precalc>=identifica){
                                          umas_anexoH += parseFloat(addtpHF/valor_uma);
                                        }
                                      }
                                    });

                                  }
                                });
                              }//if de divisa
                              else{
                                tot_moH = parseFloat(element.monto_operacion);
                                totalHD = tot_moH.toFixed(2);
                                addtpHF += Number(totalHD); 

                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Umas/getUmasAnio',
                                  async: false,
                                  data: { anio: element.fecha_pago},
                                  success: function(data2){
                                    valor_uma=parseFloat(data2).toFixed(2);
                                    //console.log("valor de uma en historico: "+valor_uma);
                                    //umas_anexoH += parseFloat(addtpHF/valor_uma);
                                    precalc= addtpHF/valor_uma;
                                    if(precalc>=identifica){
                                      umas_anexoH += parseFloat(addtpHF/valor_uma);
                                    }
                                  }
                                });
                              }
                              addtpHF=parseFloat(addtpHF).toFixed(2);
                              umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                              umas_anexoH=umas_anexoH*1;
                              //console.log("valor de umas_anexoH: "+umas_anexoH);
                              if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){ 
                              //if(identifica<=umas_anexoH && identifica!="" && identifica>0){

                                if(umas_anexoH>=identifica)
                                  umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                else
                                  umas_anexoTot_historico = umas_anexoH;

                                //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                if(umas_anexoTot_historico>=aviso){
                                  $.ajax({ //cambiar pago a acusado
                                    type: 'POST',
                                    url : base_url+'Operaciones/acusaPago',
                                    async: false,
                                    data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1},
                                    success: function(result2){
                                      id_acusado=result2;
                                      //console.log("acusado: "+id_acusado);
                                    }
                                  });
                                }
                              }
                              /*else{
                                umas_anexoTot_historico = umas_anexo;
                              }*/

                              //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                              if(umas_anexoTot_historico>=aviso){
                                band_umbral=true;
                              }
                            });//foreach
                          }//success de  get opera
                        });
                      }

                      //console.log("band_umbral: "+band_umbral);
                      if (addtpF==monto){ //hay diferentes divisas y no pueden ser los mismos montos siempre
                        validar_xml=1;
                      }else{
                        validar_xml=0;
                      }
                      //console.log("validar_xml: "+validar_xml);
                      //Anexo 9 es de $139,442.00
                      setTimeout(function () {
                        if(band_umbral==true && band_umbral!=undefined){
                          if(validar_xml==1 && !isNaN(umas_anexo)){
                            if($("#aviso").val()>0){
                              window.location.href = base_url+"Transaccion/anexo13_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                            }else{
                              window.location.href = base_url+"Transaccion/anexo13_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                            }
                            
                            swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                            /*if(history.back()!=undefined){
                              setTimeout(function () { history.back() }, 1500);
                            }*/
                            if($("#aviso").val()>0){
                              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                            }else{
                              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                            }
                          }else{
                            swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                            /*if(history.back()!=undefined){
                              setTimeout(function () { history.back() }, 1500);
                            }*/
                            if($("#aviso").val()>0){
                              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                            }else{
                              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                            }
                          }
                        }
                        else{
                          swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                          /*if(history.back()!=undefined){
                            setTimeout(function () { history.back() }, 1500);
                          }*/
                          if($("#aviso").val()>0){
                            setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                          }else{
                            setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                          }
                        }
                      }, 1500); //esperar a la sumatoria

                    }, time);
                  }//success  
                });  //ajax

                
              }  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }  
}

function confirmarPagosMonto(){ //para sumar el efectivo nadamas
  suma_no_efect=0;
  suma_efect=0;
  contta=0;
  valor_divi=0;
  montoe=0;
  montone=0;
  suma_no_efect2=0;
  suma_efect2=0;
  contta2=0;
  valor_divi2=0;
  montoe2=0;
  montone2=0;

  if($('#donativo_num').is(':checked')){
    var TABLA = $(".row_donativo_numerico .donativo_numerico > div");   
    TABLA.each(function(){        
      // pesos de Liquidación 
      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
        //console.log("pesos");
        if($(this).find("select[id*='instrum_notario_a'] option:selected").val()=="1"){ //en efectivo
          var monto = $(this).find("input[id*='monto_a']").val();
          //console.log("monto: "+$(this).find("input[id*='monto_a']").val());
          if(monto.indexOf('$') != -1){
            montoe = replaceAll(monto, ",", "" );
            montoe = replaceAll(montoe, "$", "" );
          }else{
            montoe=monto;
          }
          montoe = parseFloat(montoe);
          suma_efect += Number(montoe);

        }  
        else{
          var monto = $(this).find("input[id*='monto_a']").val();
          if(monto.indexOf('$') != -1){
            montone = replaceAll(monto, ",", "" );
            montone = replaceAll(montone, "$", "" );
          }else{
            montone=monto;
          }
          montone = parseFloat(montone);
          suma_no_efect += Number(montone);
        }
      }
      
      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){ //!= de pesos
        console.log("!pesos");
        //para sacar cant en pesos por tipo de divisa en liquidacion 
        monto_opera = $(this).find("input[id*='monto_a']").val();
        console.log("monto opera: "+monto_opera);
        var inst = $(this).find("select[id*='instrum_notario_a'] option:selected").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago_a']").val()},
          success: function(result_divi){
            valor_divi=result_divi;
            valor_divi = parseFloat(valor_divi);
            console.log("inst: "+inst);
            if(inst=="1"){ //efectivo 
              if(monto_opera.indexOf('$') != -1){
                tot_moDE = replaceAll(monto_opera, ",", "" );
                tot_moDE = replaceAll(tot_moDE, "$", "" );
              }else{
                tot_moDE = monto_opera;
              }
              tot_moDE = parseFloat(tot_moDE);
              var preefect = tot_moDE*valor_divi;
              suma_efect += Number(preefect);
            }else{
              if(monto_opera.indexOf('$') != -1){
                montone = replaceAll(monto_opera, ",", "" );
                montone = replaceAll(montone, "$", "" );
              }else{
                montone=monto_opera;
              }
              montone = parseFloat(montone);
              var montone = montone*valor_divi;
              suma_no_efect += Number(montone);
            }  
          }
        });
        $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
      }
    });
  }
  if($('#donativo_esp').is(':checked')){
    var TABLA2 = $(".row_donativo_especie .donativo_especie > div");   
    TABLA2.each(function(){        
      // pesos de Liquidación 
      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
        //console.log("pesos");
        if($(this).find("select[id*='instrum_notario_a'] option:selected").val()=="1"){ //en efectivo
          var monto2 = $(this).find("input[id*='monto_a']").val();
          //console.log("monto: "+$(this).find("input[id*='monto_a']").val());
          if(monto2.indexOf('$') != -1){
            montoe2 = replaceAll(monto2, ",", "" );
            montoe2 = replaceAll(montoe2, "$", "" );
          }else{
            montoe2=monto2;
          }
          montoe2 = parseFloat(montoe2);
          suma_efect2 += Number(montoe2);
        }  
        else{
          var monto2 = $(this).find("input[id*='monto_a']").val();
          if(monto2.indexOf('$') != -1){
            montone2 = replaceAll(monto2, ",", "" );
            montone2 = replaceAll(montone2, "$", "" );
          }else{
            montone2=monto2;
          }
          montoe2 = parseFloat(montoe2);
          suma_no_efect2 += Number(montone2);
        }
      }
      
      if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){ //!= de pesos
        //console.log("!pesos");
        //para sacar cant en pesos por tipo de divisa en liquidacion 
        monto_opera2 = $(this).find("input[id*='monto_a']").val();
        var inst2 = $(this).find("select[id*='instrum_notario_a'] option:selected").val();
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago_a']").val()},
          success: function(result_divi){
            valor_divi2=result_divi;
            valor_divi2 = parseFloat(valor_divi2);
            if(inst2=="1"){ //efectivo 
              if(monto_opera2.indexOf('$') != -1){
                tot_moDE2 = replaceAll(monto_opera2, ",", "" );
                tot_moDE2 = replaceAll(tot_moDE2, "$", "" );
              }else{
                tot_moDE2 = monto_opera2;
              }
              tot_moDE2 = parseFloat(tot_moDE2);
              var preefect2 = tot_moDE2*valor_divi2;
              suma_efect2 += Number(preefect2);
            }else{
              if(monto_opera2.indexOf('$') != -1){
                montone2 = replaceAll(monto_opera2, ",", "" );
                montone2 = replaceAll(montone2, "$", "" );
              }else{
                montone2=monto_opera2;
              }
              montone2 = parseFloat(montone2);
              var montone2 = montone2*valor_divi2;
              suma_no_efect2 += Number(montone2);
            }  
          }
        });
        $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi2);
      }
    });
  }
  
  setTimeout(function () { 
    total = parseFloat(suma_no_efect+suma_efect+suma_no_efect2+suma_efect2).toFixed(2);
    var aux_mo=0;
    var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
    totalmontomo = replaceAll(totalmontomo, "$", "" );
    var vstotalmo = totalmontomo;
    aux_mo = Number(vstotalmo);
    if(aux_mo==total){
      validar_xml=1;
      $('.validacion_cantidad').html('');
    }else{
      validar_xml=0;
      $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
    }
    /*console.log("suma_no_efect:" +suma_no_efect);
    console.log("suma_efect:" +suma_efect);
    console.log("total:" +total);*/
    $("#total_liquida_efect").val(convertMoneda(suma_efect));
    $("#total_liquida").val(convertMoneda(total));
    verificarLimite();
  }, 1500);
}

function convertMoneda(num){
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
  return monto;
}

function regresar(){
   //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}
function agregar_liqui_numerico(){
  addliquidacion_numerico(0,'','','',0);   
}
var aux_liqui_num=0;
function addliquidacion_numerico(id,fecha_pago,instrum_notario,tipo_moneda,monto){
    var aviso_tipo=$("#aviso").val();
    var fecha_max=$('#fecha_max').val();
    ///////////////////////////////////////
    var in1=''; var in2=''; var in3=''; var in4=''; var in5=''; var in6=''; var in7=''; var in8=''; var in9=''; 
    var in10=''; var in11=''; var in12=''; var in13=''; var in14=''; var in15=''; var in16=''; var in17='';
    if(instrum_notario==1){ in1='selected'; }
    else if(instrum_notario==2){in2='selected';}
    else if(instrum_notario==3){in3='selected';}
    else if(instrum_notario==4){in4='selected';}
    else if(instrum_notario==5){in5='selected';}
    else if(instrum_notario==6){in6='selected';}
    else if(instrum_notario==7){in7='selected';}
    else if(instrum_notario==8){in8='selected';}
    else if(instrum_notario==9){in9='selected';}
    else if(instrum_notario==10){in10='selected';}
    else if(instrum_notario==11){in11='selected';}
    else if(instrum_notario==12){in12='selected';}
    else if(instrum_notario==13){in13='selected';}
    else if(instrum_notario==14){in14='selected';}
    else if(instrum_notario==15){in15='selected';}
    else if(instrum_notario==16){in16='selected';}
    else if(instrum_notario==17){in17='selected';}
    ///////////////////////////////////////
    var html='<div class="row row_div_'+aux_liqui_num+'">\
                  <input type="hidden" id="id_a" value="'+id+'">\
                  <div class="col-md-3 form-group">\
                    <label>Fecha de pago</label>\
                    <input onkeydown="return false" class="form-control fecha_pago_a_'+aux_liqui_num+'" max="'+fecha_max+'" type="date" id="fecha_pago_a" value="'+fecha_pago+'" onchange="validar_fecha_opera('+aux_liqui_num+'),confirmarPagosMonto()">\
                  </div>\
                  <div class="col-md-5 form-group">\
                    <label>Instrumento monetario con el que se realizó la operación o acto</label>\
                    <select id="instrum_notario_a" class="form-control" onchange="confirmarPagosMonto()">\
                      <option value="1" '+in1+'>Efectivo</option>\
                      <option value="2" '+in2+'>Tarjeta de Crédito</option>\
                      <option value="3" '+in3+'>Tarjeta de Debito</option>\
                      <option value="4" '+in4+'>Tarjeta de Prepago</option>\
                      <option value="5" '+in5+'>Cheque Nominativo</option>\
                      <option value="6" '+in6+'>Cheque de Caja</option>\
                      <option value="7" '+in7+'>Cheques de Viajero</option>\
                      <option value="8" '+in8+'>Transferencia Interbancaria</option>\
                      <option value="9" '+in9+'>Transferencia Misma Institución</option>\
                      <option value="10" '+in10+'>Transferencia Internacional</option>\
                      <option value="11" '+in11+'>Orden de Pago</option>\
                      <option value="12" '+in12+'>Giro</option>\
                      <option value="13" '+in13+'>Oro o Platino Amonedados</option>\
                      <option value="14" '+in14+'>Plata Amonedada</option>\
                      <option value="15" '+in15+'>Metales Preciosos</option>\
                      <option value="16" '+in16+'>Activos Virtuales</option>\
                      <option value="17" '+in17+'>Otros</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Tipo de moneda o divisa de la operación o acto</label>\
                    <div class="tipo_moneda_select_'+aux_liqui_num+'"></div>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Monto de la operación o acto sin IVA ni accesorios</label>\
                    <input class="form-control monto_a_'+aux_liqui_num+'" type="text" id="monto_a" value="'+monto+'" onchange="cambio_moneda_liqui('+aux_liqui_num+'),confirmarPagosMonto()">\
                  </div>\
                  <div class="row">\
                    <div class="col-md-10 form-group">\
                      <div>\
                        <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_liqui_num==0){
                      html+=''; 
                        }else{
                      html+='<button type="button" class="btn gradient_nepal2" onclick="removerliquidacion('+aux_liqui_num+','+id+','+aviso_tipo+')"><i class="fa fa-trash-o"></i> Quitar Liquidación</button>';
                        }
                        
                html+='</div>\
                    </div>\
                  </div><div class="col-md-2"><br><h3 class="monto_divisa" style="color: red;"></h3></div>\
                  <div class="col-md-12">\
                    <hr class="subsubtitle">\
                  </div>\
              </div>';
    $('.donativo_numerico').append(html);
    agregartipo_moneda(aux_liqui_num,tipo_moneda);
    cambio_moneda_liqui(aux_liqui_num);
    aux_liqui_num++;
}
function agregartipo_moneda(id,tipo_moneda){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_moneda",
      data:{t_pago:tipo_moneda},
      success: function (data) {
        $('.tipo_moneda_select_'+id).html(data);
      }
    });
}

function Validliqui(){
  sel=$('input:radio[name=donativo]:checked').val();
  switch (sel){
    case '1':
      agregar_liqui_numerico();
      break;
      case '2':
      agregar_liqui_especie();
      break;
    default:
      swal({
          title: "Error",
          text: "No ha seleccionado un tipo de liquidacion",
          type: "error",
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "OK",
          closeOnConfirm: true
        });
      break;
  }
}

function removerliquidacion(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el pago!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_pago_anexo15',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.row_div_'+aux).remove();
              confirmarPagosMonto();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.row_div_'+aux).remove();
  }
}
function validar_fecha_opera(aux){
  if($('#donativo_num').is(':checked')){
    var fecha_pago_liqui=$('.fecha_pago_a_'+aux).val();
  }
  if($('#donativo_esp').is(':checked')){
    var fecha_pago_liqui=$('.fecha_pago_a_'+aux).val();
  }
  var fecha_operacion=$('#fecha_operacion').val();
  if(fecha_pago_liqui==fecha_operacion){ 
  }else{
    swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
}
function tabla_liquidacion_anexo13_num(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_liquidacion_anexo13",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  addliquidacion_numerico(element.id,element.fecha_pago,element.instrumento_monetario,element.tipo_moneda,element.monto_operacion);
                });
            }else{
                agregar_liqui();
            }   
        }
    });
}
function agregar_liqui_especie(){
  add_liqui_especie(0,0,0,0,'','','','');   
}
var aux_liqui_esp=0;
function add_liqui_especie(id,monto,tipomoneda,tipo_material,tipo_inmueble,codigo_postal,folio,descripcion_bien_donado){
    var aviso_tipo=$("#aviso").val();
    //////////////////////////////////////
    var fp1=''; var fp2=''; var fp3=''; var fp4=''; var fp5=''; var fp6=''; var fp7=''; var fp8=''; var fp99='';
    if(tipo_material==1){ fp1='selected'; }
    else if(tipo_material==2){fp2='selected';}
    else if(tipo_material==3){fp3='selected';}
    else if(tipo_material==4){fp4='selected';}
    else if(tipo_material==5){fp5='selected';}
    else if(tipo_material==6){fp6='selected';}
    else if(tipo_material==7){fp7='selected';}
    else if(tipo_material==8){fp8='selected';}
    else if(tipo_material==99){fp99='selected';}
    /////////////////////////////////////

    /////////////////////////////////////
    var html='<div class="row_div_esp_'+aux_liqui_esp+'">\
                <input type="hidden" id="id_e" value="'+id+'">\
                <div class="row">\
                  <div class="col-md-3 form-group">\
                    <label>Monto del valor del donativo en especie</label>\
                    <input class="form-control monto_e_'+aux_liqui_esp+'" type="text" id="monto_operacion" value="'+monto+'" onchange="cambio_moneda_liqui('+aux_liqui_esp+'),limite_efect_msj()">\
                  </div>\
                  <div class="col-md-5 form-group">\
                    <label>Tipo de moneda o divisa en la que se expreso el valor del donativo</label>\
                    <div class="tipo_moneda_select_esp_'+aux_liqui_esp+'"></div>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Tipo del bien material del donativo</label>\
                    <select id="tipo_material_d" class="form-control tipo_material_d_'+aux_liqui_esp+'" onchange="datos_donativo_text('+aux_liqui_esp+')">\
                      <option value="1" '+fp1+'>Inmueble nacional</option>\
                      <option value="2" '+fp2+'>Vehículo terrestre</option>\
                      <option value="3" '+fp3+'>Vehículo aéreo</option>\
                      <option value="4" '+fp4+'>Vehículo marítimo</option>\
                      <option value="5" '+fp5+'>Priedras Preciosas</option>\
                      <option value="6" '+fp6+'>Metales Preciosos</option>\
                      <option value="7" '+fp7+'>Joyas o relojes</option>\
                      <option value="8" '+fp8+'>Obras de arte o antigüedades</option>\
                      <option value="99" '+fp99+'>Otro (Especificar)</option>\
                    </select>\
                  </div>\
                </div>\
                <div class="row t_datos_donativo_'+aux_liqui_esp+'">\
                  <div class="col-md-12">\
                    <h4>Datos del donativo</h4>\
                  </div>\
                </div>\
                <div class="text_donativo_'+aux_liqui_esp+'">\
                  <div class="row">  \
                    <div class="col-md-4 form-group">\
                      <label>Tipo de inmueble</label>\
                      <div class="tipo_inmueble_select_esp_'+aux_liqui_esp+'"></div>\
                    </div>\
                    <div class="col-md-4 form-group">\
                      <label>Codigo postal de la hubicacion del inmueble</label>\
                      <input class="form-control" type="text" id="codigo_postal" value="'+codigo_postal+'">\
                    </div>\
                    <div class="col-md-4 form-group">\
                      <label>folio real del inmueble o antecedentes registrales</label>\
                      <input class="form-control" type="text" id="folio_real" value="'+folio+'">\
                    </div>\
                  </div>\
                </div>\
                <div class="text_donativo_descripcion_'+aux_liqui_esp+'" style="display: none">\
                  <div class="row">  \
                    <div class="col-md-12 form-group">\
                      <label>Descripción del bien donado</label>\
                      <input class="form-control" type="text" id="descripcion_bien_donado" value="'+descripcion_bien_donado+'">\
                    </div>\
                  </div>  \
                </div>\
                <div class="row">\
                  <div class="col-md-10 form-group">\
                    <div>';
                      if(aux_liqui_esp==0){
                    html+=''; 
                      }else{
                    html+='<button type="button" class="btn gradient_nepal2" onclick="removerliquidacion_esp('+aux_liqui_esp+','+id+','+aviso_tipo+')"><i class="fa fa-trash-o"></i> Quitar Liquidación</button>';
                      }
              html+='</div>\
                  </div>\
                </div><div class="col-md-2"><br><h3 class="monto_divisa" style="color: red;"></h3></div>\
                <div class="row">\
                  <div class="col-md-12">\
                    <hr class="subsubtitle">\
                  </div>\
                </div>\
              </div>';
    $('.donativo_especie').append(html);
    agregartipo_moneda_esp(aux_liqui_esp,tipomoneda);
    agregartipo_inmueble_esp(aux_liqui_esp,tipomoneda);
    datos_donativo_text(aux_liqui_esp);
    cambio_moneda_liqui(aux_liqui_esp);
    aux_liqui_esp++;
}
function datos_donativo_text(id){
  var tipo_bien = $('.tipo_material_d_'+id+' option:selected').val();
  if(tipo_bien==1){
    $('.t_datos_donativo_'+id).css('display','block');
    $('.text_donativo_'+id).css('display','block');
    $('.text_donativo_descripcion_'+id).css('display','none');
  }else if(tipo_bien==99){
    $('.t_datos_donativo_'+id).css('display','block');
    $('.text_donativo_'+id).css('display','none');
    $('.text_donativo_descripcion_'+id).css('display','block');
  }else{
    $('.t_datos_donativo_'+id).css('display','none');
    $('.text_donativo_'+id).css('display','none');
    $('.text_donativo_descripcion_'+id).css('display','none');
  }
}
function tabla_liquidacion_anexo13_esp(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_liquidacion_anexo13_esp",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  add_liqui_especie(element.id,element.monto_operacion,element.tipo_moneda,element.tipo_material,element.tipo_inmueble,element.codigo_postal,element.folio,element.descripcion_bien_donado);
                });
            }else{
                add_liqui_especie();
            }   
        }
    });
}
function removerliquidacion_esp(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el pago!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_pago_anexo15',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.row_div_'+aux).remove();
              confirmarPagosMonto();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.row_div_esp_'+aux).remove();
  }
}
function cambio_moneda_liqui(aux){
  var mon=0;
  if($('#donativo_num').is(':checked')){
    mon=$('.monto_a_'+aux).val();
  }
  if($('#donativo_esp').is(':checked')){
    mon=$('.monto_e_'+aux).val();
  }
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  if($('#donativo_num').is(':checked')){
    $(".monto_a_"+aux+"").val(monto);
  }
  if($('#donativo_esp').is(':checked')){
    $(".monto_e_"+aux+"").val(monto);
  }
  suma_tablaliquidacion();
}
function suma_tablaliquidacion(){
  /*var min = true;
  var addtp = 0;
  var mte = 0;
  if($('#donativo_num').is(':checked')){
    var TABLAP = $(".row_donativo_numerico .donativo_numerico > div");                  
    TABLAP.each(function(){         
        totalmonto = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        var vstotal = totalmonto;
        addtp += Number(vstotal);
        
        if($(this).find("select[id*='instrum_notario_a'] option:selected").val()==1){
          totalmontoe = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
          totalmontoe = replaceAll(totalmontoe, "$", "" );
          var vstotale = totalmontoe;
          mte += Number(vstotale);
        } 
    });
  }
  if($('#donativo_esp').is(':checked')){
    var TABLAP = $(".row_donativo_especie .donativo_especie > div");                  
    TABLAP.each(function(){         
        totalmonto = replaceAll($(this).find("input[id*='monto_operacion']").val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        var vstotal = totalmonto;
        addtp += Number(vstotal);
    });
  }
  //Monto total de la liquidación
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  //Monto total en efectivo
  ttle = replaceAll(mte, ",", "" );
  ttle = replaceAll(ttle, "$", "" );
  var monto_ttle=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(ttle);
  $('#total_liquida_efect').val(monto_ttle);
  
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  
  var aux_mo=0;
  var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
  totalmontomo = replaceAll(totalmontomo, "$", "" );

  var vstotalmo = totalmontomo;
  aux_mo = Number(vstotalmo);
  if(aux_mo==addtp){
    validar_xml=1;
    $('.validacion_cantidad').html('');
  }else{
    validar_xml=0;
    $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
  }*/
}
function agregartipo_moneda_esp(id,tipo_moneda){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_moneda_esp",
      data:{t_pago:tipo_moneda},
      success: function (data) {
        $('.tipo_moneda_select_esp_'+id).html(data);
      }
    });
}
function agregartipo_inmueble_esp(id,tipo_inmueble){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/agregartipo_inmueble_esp",
      data:{t_inmueble:tipo_inmueble},
      success: function (data) {
        $('.tipo_inmueble_select_esp_'+id).html(data);
      }
    });
}

////////////////////// Operaciones
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
/// < / >
// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}
// < / > // 