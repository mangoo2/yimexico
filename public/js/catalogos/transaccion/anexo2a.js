var base_url = $('#base_url').val();
$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
	ComboAnio();
  tipo_modeda(1);
  if($('#mes_aux').val()==''){
    $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
  }else{
    $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
  }
  $('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");

  var aux_pga=$('#aux_pga').val();
  if(aux_pga==1){
    var id_aux=$('#id_aux').val();
    tabla_liquidacion_anexo(id_aux,$("#aviso").val());
  }else{
    agregar_liqui();
  } 
  verificarConfigLimite();
  $("#fecha_operacion").on("change",function(){
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();
  
});

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}
function tipo_modeda(aux){
  var mon=$('.monto_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_"+aux+"").val(monto);
  suma_tablaliquidacion();
}
function agregar_liqui(){
  addliquidacion(0,'','','',0);   
}
var aux_liqui_num=0;
function addliquidacion(id,tipo_tarjeta,numero_identificador,monto_gasto){
    var aviso_tipo=$("#aviso").val();
    var fecha_max=$('#fecha_max').val();
    ///////////////////////////////////////
    var in1=''; var in2='';
    if(tipo_tarjeta==1){ in1='selected'; }
    else if(tipo_tarjeta==2){in2='selected';}
    ///////////////////////////////////////
    var html='<div class="row row_div_'+aux_liqui_num+'">\
                  <input type="hidden" id="id_a" value="'+id+'">\
                  <div class="col-md-4 form-group">\
                    <label>Tipo de tarjeta</label>\
                    <select id="tipo_tarjeta_a" class="form-control">\
                      <option value="1" '+in1+'>Tarjeta de Servicio</option>\
                      <option value="2" '+in2+'>Tarjeta de Crédito</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Número de Tarjeta, Cuenta, Contrato o Identificador</label>\
                    <input maxlength="18" class="form-control" type="text" id="numero_identificador_a" value="'+numero_identificador+'">\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Monto Total del gasto acumulado en el periodo</label>\
                    <input class="form-control monto_a_'+aux_liqui_num+'" type="text" id="monto_gasto_a" value="'+monto_gasto+'" onchange="cambio_moneda_liqui('+aux_liqui_num+')">\
                  </div>\
              </div>';
    $('.liquidacion').append(html);
    cambio_moneda_liqui(aux_liqui_num);
    aux_liqui_num++;
}
function tabla_liquidacion_anexo(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_liquidacion_anexo2a",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  addliquidacion(element.id,element.tipo_tarjeta,element.numero_identificador,element.monto_gasto);
                });
            }else{
                agregar_liqui();
            }   
        }
    });
}

function removerliquidacion(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el pago!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_pago_anexo3',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.row_div_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.row_div_'+aux).remove();
  }
}

function cambio_moneda_liqui(aux){
  var mon=$('.monto_a_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_a_"+aux+"").val(monto);
  suma_tablaliquidacion();
}

function suma_tablaliquidacion(){
  var min = true;
  var addtp = 0;
  var mte = 0;
  var TABLAP = $(".row_liquidacion .liquidacion > div");                  
    TABLAP.each(function(){         
        totalmonto = replaceAll($(this).find("input[id*='monto_gasto_a']").val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        var vstotal = totalmonto;
        addtp += Number(vstotal);
    });
  //Monto total de la liquidación
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);

  var aux_mo=0;
  var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
  totalmontomo = replaceAll(totalmontomo, "$", "" );
  var vstotalmo = totalmontomo;
  aux_mo = Number(vstotalmo);


  var total_liquidado = replaceAll($('#total_liquida').val(), ",", "" );
  total_liquidado = replaceAll(total_liquidado, "$", "" );
  //total_liquidado = parseFloat(total_liquidado);

  /*aux_mo = parseFloat(aux_mo);
  addtp = parseFloat(addtp);*/

  //console.log("aux_mo: "+aux_mo);
  //console.log("total_liquidado: "+total_liquidado);
  //console.log("mtl: "+mtl);
  
  
  if(aux_mo==total_liquidado){
    validar_xml=1;
    $('.validacion_cantidad').html('');
  }else{
    validar_xml=0;
    $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
  }

}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

/*
function validar_fecha_opera(aux){
  var fecha_pago_liqui=$('.fecha_pago_a_'+aux).val();
  var fecha_operacion=$('#fecha_operacion').val();
  if(fecha_pago_liqui==fecha_operacion){ 
  }else{
    swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
}
*/

function registrar(){
  var form_register = $('#form_anexo');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
                referencia:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_anexo").valid();
        var band_aviso = 0;
        if(valid) {
          
          monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
          monto_opera = replaceAll(monto_oper, "$", "" );
          
          if($("#aviso").val()>0){
            name_table = "anexo2a_aviso";
          }else{
            name_table = "anexo2a";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }

          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo2a',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = $.parseJSON(data);
                var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                              //===================
                // por bien 
                var DATAP= [];
                    var TABLAP = $(".row_liquidacion .liquidacion > div");                  
                    TABLAP.each(function(){         
                          item = {};
                          item ["idanexo2a"]=id;
                          item ["aviso"]=$("#aviso").val();
                          item ["id"]=$(this).find("input[id*='id_a']").val();
                          item ["tipo_tarjeta"]=$(this).find("select[id*='tipo_tarjeta_a']").val();
                          item ["numero_identificador"]=$(this).find("input[id*='numero_identificador_a']").val();
                          montop = replaceAll($(this).find("input[id*='monto_gasto_a']").val(), ",", "" );
                          montop = replaceAll(montop, "$", "" );
                          item ["monto_gasto"]=montop;
                          DATAP.push(item);
                    });
                    INFOP  = new FormData();
                    aInfop   = JSON.stringify(DATAP);
                    INFOP.append('data', aInfop);
                    $.ajax({
                        data: INFOP,
                        type: 'POST',
                        url : base_url+'index.php/Transaccion/inset_pago_anexo2a',
                        processData: false, 
                        contentType: false,
                        async: false,
                        statusCode:{
                            404: function(data2){
                                //toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                //toastr.error('Error', '500');
                            }
                        },
                        success: function(data2){
                          
                        }
                    }); 

                //=================== 
              
                $("input[name*='id_aviso']").val(id);
                //Anexo 2A: $111,641.00
                var anio = $("#anio").val();
                var valor_uma; var band_umbral=false;
                /*var addtpHF=0; var umas_anexoH=0;*/
                id_acusado=0;
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  data: { anio: anio},
                  success: function(datauma){
                    valor_uma=datauma;
                    valor_uma = parseFloat(valor_uma);
                    $.ajax({
                      type: 'POST',
                      url : base_url+'Umbrales/getUmbral',
                      data: {anexo: $("#id_actividad").val()},
                      success: function(result){
                        //var array = $.parseJSON(result);
                        var array= JSON.parse(result);
                        var aviso = parseFloat(array[0].aviso).toFixed(2);
                        var siempre_avi = array[0].siempre_avi;
                        var siempre_iden=array[0].siempre_iden;
                        var identifica=array[0].identificacion;

                        //console.log("siempre avi: "+array[0].siempre_avi);
                        if(array[0].siempre_avi=="1"){
                          band_umbral=true;
                          $.ajax({ //cambiar pago a acusado
                            type: 'POST',
                            url : base_url+'Operaciones/acusaPago2',
                            async: false,
                            data: { id:id, act:$("#id_actividad").val()},
                            success: function(result_divi){

                            }
                          });
                        }else{
                          //console.log("valor de uma en calculo: "+valor_uma);
                          totalmonto = replaceAll($("input[id*='monto_gasto_a']").val(), ",", "" );
                          totalmonto = replaceAll(totalmonto, "$", "" );
                          preumas_anexo = totalmonto/valor_uma;
                          umas_anexo = parseFloat(preumas_anexo).toFixed(2);
                          //console.log("umas_anexo: "+umas_anexo);
                          console.log("aviso: "+aviso);

                          umas_anexo = umas_anexo*1;
                          aviso = aviso*1;
                          if(umas_anexo>=aviso){
                            //console.log("se cumple umbral: "+umas_anexo);
                            band_umbral=true;
                            $.ajax({ //cambiar pago a acusado
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPago2',
                              async: false,
                              data: { id:id, act:$("#id_actividad").val()},
                              success: function(result_divi){

                              }
                            });
                            
                          }else{//treerias el historico
                                                   
                            $.ajax({
                              type: 'POST',
                              url : base_url+'Operaciones/getOpera',
                              async: false,
                              data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act: 2, id_union:$("#id_union").val(), id_anexo:id, id_perfilamiento: $("#id_perfilamiento").val() },
                              success: function(result){
                                var data= $.parseJSON(result);
                                var datos = data.data;
                                datos.forEach(function(element) {
                                  var addtpHF=0; var umas_anexoH=0;
                                  tot_moH = parseFloat(element.monto_gasto);
                                  totalHD = tot_moH.toFixed(2);
                                  addtpHF += Number(totalHD); 

                                  $.ajax({
                                    type: 'POST',
                                    url : base_url+'Umas/getUmasAnio',
                                    async: false,
                                    data: { anio: element.fecha_reg},
                                    success: function(data2){
                                      valor_uma=parseFloat(data2).toFixed(2);
                                      //console.log("valor de uma en historico: "+valor_uma);
                                      precalc= addtpHF/valor_uma;
                                      if(precalc>=identifica){
                                        umas_anexoH += parseFloat(addtpHF/valor_uma);
                                      }
                                    }
                                  });
                                  
                                  addtpHF=parseFloat(addtpHF).toFixed(2);
                                  umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                                  umas_anexoH=umas_anexoH*1;
                                  console.log("addtpHF: "+addtpHF);
                                  console.log("identifica: "+identifica);
                                  console.log("umas_anexoH: "+umas_anexoH);

                                  if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){
                                    //console.log("identifica menor o igual que historico: "+identifica);
                                    if(umas_anexoH>=identifica)
                                      umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                    else
                                      umas_anexoTot_historico = umas_anexoH;

                                    //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                    if(umas_anexoTot_historico>=aviso){
                                      //band_umbral=true;
                                      $.ajax({ //cambiar pago a acusado
                                        type: 'POST',
                                        url : base_url+'Operaciones/acusaPago',
                                        async: false,
                                        data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1},
                                        success: function(result2){
                                          id_acusado=result2;
                                        }
                                      });
                                    }
                                  }
                                  /*else{
                                    umas_anexoTot_historico = umas_anexo;
                                  }*/
                                  console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                                  if(umas_anexoTot_historico>=aviso){
                                    band_umbral=true;
                                  }
                                });//foreach
                              }//success de  get opera
                            });
                            
                          }
                        }
                        //console.log("band_umbral: "+band_umbral);
                        setTimeout(function () { 
                          if(band_umbral==true && band_umbral!=undefined && !isNaN(umas_anexo)){
                          //if(monto>=111641){
                            if(validar_xml==1){
                              if($("#aviso").val()>0){
                                window.location.href = base_url+"Transaccion/anexo2a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                              }else{
                                window.location.href = base_url+"Transaccion/anexo2a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                              }
                              
                              swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                              /*if(history.back()!=undefined){
                                setTimeout(function () { history.back() }, 1500);
                              }*/
                              if($("#aviso").val()>0){
                                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                              }else{
                                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                              }
                            }else{
                              swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                              /*if(history.back()!=undefined){
                                setTimeout(function () { history.back() }, 1500);
                              }*/
                              if($("#aviso").val()>0){
                                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                              }else{
                                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                              }
                            }
                          }
                          else{
                            swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                            /*if(history.back()!=undefined){
                              setTimeout(function () { history.back() }, 1500);
                            }*/
                            if($("#aviso").val()>0){
                              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                            }else{
                              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                            }
                          }
                        }, 1500); //esperar a la sumatoria
                      }
                    }); 
                  }
                }); 

              }  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }  
}

function regresar(){
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
////////////////////// Operaciones
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
/// < / >
// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}
// < / > // 