var base_url = $('#base_url').val();
var act_num_aux=0;
var validar_xml=0;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var siempre_avi;
var valor_uma=0;
$(document).ready(function(){
  //$("input[class*='form-check-input']").attr('disabled',false);
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
	ComboAnio();
  tipo_modeda(1);
  if($('#mes_aux').val()==''){
    $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
  }else{
    $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
  }
  $('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");

  var aux_pga=$('#aux_pga').val();
  var id_aux=$('#id_aux').val();
  if(aux_pga==1 && $("#tipo_actividad option:selected").val()!="7"){
    tabla_liquidacion_anexo(id_aux,$("#aviso").val());
  }else if(aux_pga!=1 && $("#tipo_actividad option:selected").val()!="7"){
    agregar_liqui();
  } 
  // Tipo actividad 2 Constitución de personas Morales
  var aux_cpm=$('#aux_cpm').val();
  if(aux_cpm==1){
    tabla_anexo12a_constitucion_personas_morales_socios(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios();
  } 
  // Tipo actividad 3 Modificacion patrimonial por aumento de capital o por disminucion de capital
  var aux_mps=$('#aux_mps').val();
  if(aux_mps==1){
    tabla_anexo12a_modificacion_patrimonial_socios(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas();
  } 
  // Tipo actividad 4 Fusión
  var aux_fus=$('#aux_fus').val();
  if(aux_fus==1){
    tabla_anexo12a_fusion_socios_funcionantes(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios_funcionantes();
  } 
  // Tipo actividad 5 Escisión
  var aux_esc=$('#aux_esc').val();
  if(aux_esc==1){
    tabla_anexo12a_accionistas_socios_escision(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios_escision();
  }
  //Escindida
  var aux_esci=$('#aux_esci').val();
  if(aux_esci==1){
    tabla_anexo12a_accionistas_socios_escindida(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios_escindida();
  } 
  // Tipo actividad 7
  var aux_fide=$('#aux_fide').val();
  if(aux_fide==1){
    tabla_anexo12a_datos_fideicomitente(id_aux,$("#aviso").val());
  }else{
    agregar_datos_fideicomitente();
  }  
  var aux_fidei=$('#aux_fidei').val();
  if(aux_fidei==1){
    tabla_anexo12a_datos_fideicomisarios(id_aux,$("#aviso").val());
  }else{
    agregar_datos_fideicomisarios();
  } 
  //=====================================================================================================================//
  // tipo persona 1
  getpais('f',1);
  getpais('m',1);
  // tipo perosna 2 apoderados
  getpais('pf',1);
  getpais('pm',1);
  getpais('fc',1);
  // tipo persona 3
  getpais('mp',1);
  // Vigilante
  getpais('fv',1);
  getpais('mv',1);  
  //=====================================================================================================================//
  /// Actividad 1
  //var aux_tipo_actividad = $('#aux_tipo_actividad').val();
  var aux_tipo_actividad = $('#tipo_actividad option:selected').val();
  otorgamiento_poder_select(aux_tipo_actividad);
  tipo_persona_select();
  tipo_personap1_select();
  // Actividad 2
  tipo_personac_select();
  // Actividad 3
  getpais('fu',1);
  getpais('ffu',1);
  // Actividad 5
  getpais('esci',1);
  getpais('esce',1);
  // Actividad 6
  getpais('ven',1);
  tipo_persona6_select();
  getpais('com',1);
  getpais('comm',1);
  tipo_persona7_select();
  // Actividad 7
  tipo_fideicomiso_fide();
  //datos_tipo_patrimoniofi_select();
  // Actividad 8
  tipo_persona_selectces();
  getpais('ces',1);
  getpais('cesm',1);
  tipo_persona_selectce();
  getpais('ce',1);
  getpais('cem',1);
  // Actividad 9
  tipo_persona_selectort();
  getpais('ort',1);
  getpais('ortm',1);
  tipo_persona_selectdeu();
  getpais('deu',1);
  getpais('deum',1);
  datos_bien_mutuoort_select();
  tipo_persona_selectgar();
  tipo_persona_selectrea();
  // Tipo actividad 10
  getpais('rea',1);
  getpais('ream',1);
  ValidFusion();
  MostrarSelect();
  $("#DPME").hide();

  valorUma();
  $("#fecha_operacion").on("change",function(){
    valorUma();
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  $("#tipo_movimientofi").on("change",function(){
    verificarTipoMov();
  });

  $("#tipo_fideicomisofi").on("change",function(){
    verificarOtroFide();
  });
  $("#tipo_otorgamientoort").on("change",function(){
    verificarGaranDatos();
  });

  ValidEsicion();
  ValidEsicion1();
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();

  verificarTipoMov();
  verificarGaranDatos();
  setTimeout(function () { 
    verificarConfigLimite();
  }, 1000);
  
  $("#instrum_notario_a").on("change",function(){
    verificarConfigLimite();
    confirmarPagosMonto();
  });

  $("#monto_a").on("change",function(){
    confirmarPagosMonto();
  });

  setTimeout(function () { 
    confirmarPagosMonto();
  }, 4000);

  //validar fechas 
  $("#instrum_notario_a").on("change",function(){
    validarFechasPago();
  });
  $("#tipo_moneda_a").on("change",function(){
    validarFechasPago();
  });
  $("#monto_a").on("change",function(){
    validarFechasPago();
  });
  // ////////////////////////////////// //
  
});

function validarFechasPago(){
  var tipo_act_sel = $("#tipo_actividad option:selected").val();
  if(tipo_act_sel==6){
    var TABLAP = $(".row_liquidacion .liquidacion > div");                  
    TABLAP.each(function(){         
      fecha_pago_liquidas=$(this).find("input[id*='fecha_pago_a']").val();
      //console.log("fecha_pago_liquidas: "+fecha_pago_liquidas);
      let result = moment(fecha_pago_liquidas, 'YYYY-MM-DD',true).isValid();
      //console.log("fecha_pago: "+result);
      if(fecha_pago_liquidas!="" && result == true){
        $("#btn_submit").attr("disabled",false);
      }else{
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "Fecha de pago incorrecta, favor de ingresar una fecha valida", "error");
      }
    });
  }
}

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}
function verificarTipoMov(){
  //console.log($("#tipo_movimientofi option:selected").val());
  if($("#tipo_movimientofi option:selected").val()=="6")
    $("#cont_prin_ddlf").hide();
  else
    $("#cont_prin_ddlf").show();
}

function verificarOtroFide(){
  //console.log($("#tipo_fideicomisofi option:selected").val());
  if($("#tipo_fideicomisofi option:selected").val()=="4")
    $("#cont_ult_preg").hide();
  else
    $("#cont_ult_preg").show();
}
function verificarGaranDatos(){
  if($("#tipo_otorgamientoort option:selected").val()=="1")
    $("#cont_total_garantia").hide();
  else
    $("#cont_total_garantia").show();
}
function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}
var valor_uma=0;
function valorUma(){
  $.ajax({
    type: 'POST',
    url : base_url+'Umas/getUmasAnio',
    data: { anio: $("#fecha_operacion").val()},
    success: function(data2){
      valor_uma=data2;
      valor_uma=parseFloat(valor_uma);
      console.log("valo de uma inicial en proyecto: "+valor_uma); 
    }
  });
}
function tipo_modeda(aux){
  var mon=$('.monto_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_"+aux+"").val(monto);
  suma_tablaliquidacion();
}
//===================================================================================================//
$("#tipo_actividad").change(function(event) {
  MostrarSelect();
});
function MostrarSelect(){
  op=$("#tipo_actividad option:selected").val();
  console.log(op);
  switch (op) {
      case "1":
        $('.otorgamiento_podertxt').css('display','block');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "2":
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','block');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "3":
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','block');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "4":
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','block');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "5":
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','block');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "6":
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','block');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "7":
      console.log(7);
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','block');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "8":
      console.log(8);
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','block');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "9":
      console.log(9);
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','block');
        $('.realizacion_avaluostxt').css('display','none');
      break;
      case "10":
      console.log(10);
        $('.otorgamiento_podertxt').css('display','none');
        $('.constitucion_personas_moralestxt').css('display','none');
        $('.modificacion_patrimonialtxt').css('display','none');
        $('.fusiontxt').css('display','none');
        $('.escisiontxt').css('display','none');
        $('.compra_venta_accionestxt').css('display','none');
        $('.constitucion_modificacion_fideicomisotxt').css('display','none');
        $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
        $('.contrato_mutuo_creditotxt').css('display','none');
        $('.realizacion_avaluostxt').css('display','block');
      break;
    default:
      break;
  }
}
//===================================================================================================//
function otorgamiento_poder_select(id){
  act_num_aux=id;
  if(id==1){
    $('#tipo_actividad1').prop('checked', true);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==2){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', true);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==3){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', true);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==4){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', true);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==5){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', true);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==6){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', true);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==7){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', true);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==8){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', true);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', false);
  }else if(id==9){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', true);$('#tipo_actividad10').prop('checked', false);
  }else if(id==10){
    $('#tipo_actividad1').prop('checked', false);$('#tipo_actividad2').prop('checked', false);$('#tipo_actividad3').prop('checked', false);$('#tipo_actividad4').prop('checked', false);$('#tipo_actividad5').prop('checked', false);
    $('#tipo_actividad6').prop('checked', false);$('#tipo_actividad7').prop('checked', false);$('#tipo_actividad8').prop('checked', false);$('#tipo_actividad9').prop('checked', false);$('#tipo_actividad10').prop('checked', true);
  }

  if($('#tipo_actividad1').is(':checked')){
    $('.otorgamiento_podertxt').css('display','block');
  }else{
    $('.otorgamiento_podertxt').css('display','none');
  }
  if($('#tipo_actividad2').is(':checked')){
    $('.constitucion_personas_moralestxt').css('display','block');
  }else{
    $('.constitucion_personas_moralestxt').css('display','none');
  }
  if($('#tipo_actividad3').is(':checked')){
    $('.modificacion_patrimonialtxt').css('display','block');
  }else{
    $('.modificacion_patrimonialtxt').css('display','none');
  }
  if($('#tipo_actividad4').is(':checked')){
    $('.fusiontxt').css('display','block');
  }else{
    $('.fusiontxt').css('display','none');
  }
  if($('#tipo_actividad5').is(':checked')){
    $('.escisiontxt').css('display','block');
  }else{
    $('.escisiontxt').css('display','none');
  }
  if($('#tipo_actividad6').is(':checked')){
    $('.compra_venta_accionestxt').css('display','block');
  }else{
    $('.compra_venta_accionestxt').css('display','none');
  }
  if($('#tipo_actividad7').is(':checked')){
    $('.constitucion_modificacion_fideicomisotxt').css('display','block');
  }else{
    $('.constitucion_modificacion_fideicomisotxt').css('display','none');
  }
  if($('#tipo_actividad8').is(':checked')){
    $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','block');
  }else{
    $('.cesion_derechos_fideicomitente_fideicomisariotxt').css('display','none');
  }
  if($('#tipo_actividad9').is(':checked')){
    $('.contrato_mutuo_creditotxt').css('display','block');
  }else{
    $('.contrato_mutuo_creditotxt').css('display','none');
  }
  if($('#tipo_actividad10').is(':checked')){
    $('.realizacion_avaluostxt').css('display','block');
  }else{
    $('.realizacion_avaluostxt').css('display','none');
  }
}

function ValidCurp(T){
  
  let ex=/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
  valor=T.value;
  valor = valor.toUpperCase();
  console.log("valor: "+valor);
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "CURP No valido", "error");
    }
  }
}

function ValidRfc(T){
  let ex=/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;
  valor=T.value;
  valor = valor.toUpperCase();
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "RFC No valido", "error");
    }
  }
}

function tipo_persona_select(){
  if($('#tipo_persona1').is(':checked')){
    $('.persona_fisicatxt').css('display','block');
  }else{
    $('.persona_fisicatxt').css('display','none');
  }
  if($('#tipo_persona2').is(':checked')){
    $('.persona_moraltxt').css('display','block');
  }else{
    $('.persona_moraltxt').css('display','none');
  }
  if($('#tipo_persona3').is(':checked')){
    $('.fideicomisotxt').css('display','block');
  }else{
    $('.fideicomisotxt').css('display','none');
  }
}
function tipo_personap1_select(){
  if($('#tipo_personap1').is(':checked')){
    $('.persona_fisicaptxt').css('display','block');
  }else{
    $('.persona_fisicaptxt').css('display','none');
  }
  if($('#tipo_personap2').is(':checked')){
    $('.persona_moralptxt').css('display','block');
  }else{
    $('.persona_moralptxt').css('display','none');
  }
  if($('#tipo_personap3').is(':checked')){
    $('.fideicomisoptxt').css('display','block');
  }else{
    $('.fideicomisoptxt').css('display','none');
  }
}
//Tipo_actividad 2
function tipo_persona_moral_otros(){
  var tpmo=$('#tipo_persona_moral option:selected').val();
  if(tpmo==99){
    $('.tipo_persona_moral_otratxt').css('display','block'); 
  }else{
    $('.tipo_persona_moral_otratxt').css('display','none')
  }
}
function tipo_personac_select(){
  if($('#tipo_personac1').is(':checked')){
    $('.persona_fisicactxt').css('display','block');
  }else{
    $('.persona_fisicactxt').css('display','none');
  }
  if($('#tipo_personac2').is(':checked')){
    $('.persona_moralctxt').css('display','block');
  }else{
    $('.persona_moralctxt').css('display','none');
  }
  if($('#tipo_personac3').is(':checked')){
    $('.fideicomisoctxt').css('display','block');
  }else{
    $('.fideicomisoctxt').css('display','none');
  }
}
//Tipo_actividad 3
function tipo_personacc_select(){
  if($('#tipo_personacc1').is(':checked')){
    $('.persona_fisicacctxt').css('display','block');
  }else{
    $('.persona_fisicacctxt').css('display','none');
  }
  if($('#tipo_personacc2').is(':checked')){
    $('.persona_moralcctxt').css('display','block');
  }else{
    $('.persona_moralcctxt').css('display','none');
  }
  if($('#tipo_personacc3').is(':checked')){
    $('.fideicomisocctxt').css('display','block');
  }else{
    $('.fideicomisocctxt').css('display','none');
  }
}
//Tipo_actividad 6
function tipo_persona6_select(){
  if($('#tipo_persona61').is(':checked')){
    $('.persona_fisica6txt').css('display','block');
  }else{
    $('.persona_fisica6txt').css('display','none');
  }
  if($('#tipo_persona62').is(':checked')){
    $('.persona_moral6txt').css('display','block');
  }else{
    $('.persona_moral6txt').css('display','none');
  }
  if($('#tipo_persona63').is(':checked')){
    $('.fideicomiso6txt').css('display','block');
  }else{
    $('.fideicomiso6txt').css('display','none');
  }
}
//Tipo_actividad 6
function tipo_persona7_select(){
  if($('#tipo_persona71').is(':checked')){
    $('.persona_fisica7txt').css('display','block');
  }else{
    $('.persona_fisica7txt').css('display','none');
  }
  if($('#tipo_persona72').is(':checked')){
    $('.persona_moral7txt').css('display','block');
  }else{
    $('.persona_moral7txt').css('display','none');
  }
  if($('#tipo_persona73').is(':checked')){
    $('.fideicomiso7txt').css('display','block');
  }else{
    $('.fideicomiso7txt').css('display','none');
  }
}
function tipo_fideicomiso_fide(){
  var tipo_fideicomisofi = $('#tipo_fideicomisofi option:selected').val();
  if(tipo_fideicomisofi==4){
    $('.tipo_fideicomiso_cmftxt').css('display','block');
  }else{
    $('.tipo_fideicomiso_cmftxt').css('display','none');
  }
}
function ValidFusion(){
    valid=$('input:radio[name=fusionante_determinadasfu]:checked').val();
    if (valid=="SI"){
      $("#DPMF").show();
      $("#con_dato_accion_socio").show();
    }else{
      $("#DPMF").hide();
      $("#con_dato_accion_socio").hide();
    }
}

function ValidEsicion1(){
  //console.log('Click');
    valid=$('input:radio[name=escindidas_determinadase]:checked').val();
    if (valid=="SI"){
      $("#DPME").show();
      $("#cont_accion_moral_esc").show();
    }else{
      $("#DPME").hide();
      $("#cont_accion_moral_esc").hide();
    }
}

function ValidEsicion(){
  //console.log('Click');
    valid=$('input:radio[id=escindente_subsistee1]:checked').val();
    if (valid=="SI"){
      $("#DPME").show();
      $("#datos_accion_subs").show();
      //$("#escision_subsiste").show();
    }else{
      $("#DPME").hide();
      $("#datos_accion_subs").hide();
      //$("#escision_subsiste").hide();
    }
}

function ValidForamlizacion(){
    valid=$('input:radio[name=propietario_solicita]:checked').val();
    if (valid=="SI"){
      $("#DTPBOV").hide();
    }else{
      $("#DTPBOV").show();
    }
}

function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
function btn_descripcion_ayuda(){
  $('#modal_descripcion_ayuda').modal();
}
function btn_intrumentoPublico_ayuda(){
  $('#modal_intrumentoPublico_ayuda').modal();
}
function btn_foliomer_ayuda(){
 $('#modal_foliomer_ayuda').modal(); 
}
function btn_NIPFE_ayuda(){
  $('#modal_NIPFE_ayuda').modal();
}
function btn_NRIF_ayuda(){
  $('#modal_NRIF_ayuda').modal();
}
function btn_DTFCO_ayuda(){
  $('#modal_DTFCO_ayuda').modal();
}
function btn_descripcion_ayuda(){
  $('#modal_descripcion_ayuda').modal();
}
//Tipo_actividad 7
function datos_tipo_patrimoniofi_select(id){
  if($('#datos_tipo_patrimoniofi1_'+id).is(':checked')){
    $('.patrimonio_monetariotxt_'+id).css('display','block');
  }else{
    $('.patrimonio_monetariotxt_'+id).css('display','none');
  }
  if($('#datos_tipo_patrimoniofi2_'+id).is(':checked')){
    $('.patrimonio_inmuebletxt_'+id).css('display','block');
  }else{
    $('.patrimonio_inmuebletxt_'+id).css('display','none');
  }
  if($('#datos_tipo_patrimoniofi3_'+id).is(':checked')){
    $('.patrimonio_otro_bientxt_'+id).css('display','block');
  }else{
    $('.patrimonio_otro_bientxt_'+id).css('display','none');
  }
}
// Tipo actividad 8
function tipo_persona_selectces(){
  if($('#tipo_personaces1').is(':checked')){
    $('.persona_fisicacestxt').css('display','block');
  }else{
    $('.persona_fisicacestxt').css('display','none');
  }
  if($('#tipo_personaces2').is(':checked')){
    $('.persona_moralcestxt').css('display','block');
  }else{
    $('.persona_moralcestxt').css('display','none');
  }
  if($('#tipo_personaces3').is(':checked')){
    $('.fideicomisocestxt').css('display','block');
  }else{
    $('.fideicomisocestxt').css('display','none');
  }
}
function tipo_persona_selectce(){
  if($('#tipo_personace1').is(':checked')){
    $('.persona_fisicacetxt').css('display','block');
  }else{
    $('.persona_fisicacetxt').css('display','none');
  }
  if($('#tipo_personace2').is(':checked')){
    $('.persona_moralcetxt').css('display','block');
  }else{
    $('.persona_moralcetxt').css('display','none');
  }
  if($('#tipo_personace3').is(':checked')){
    $('.fideicomisocetxt').css('display','block');
  }else{
    $('.fideicomisocetxt').css('display','none');
  }
}
// Tipo actividad 9
function tipo_persona_selectort(){
  if($('#tipo_personaort1').is(':checked')){
    $('.persona_fisicaorttxt').css('display','block');
  }else{
    $('.persona_fisicaorttxt').css('display','none');
  }
  if($('#tipo_personaort2').is(':checked')){
    $('.persona_moralorttxt').css('display','block');
  }else{
    $('.persona_moralorttxt').css('display','none');
  }
  if($('#tipo_personaort3').is(':checked')){
    $('.fideicomisoorttxt').css('display','block');
  }else{
    $('.fideicomisoorttxt').css('display','none');
  }
}
function tipo_persona_selectdeu(){
  if($('#tipo_personadeu1').is(':checked')){
    $('.persona_fisicadeutxt').css('display','block');
  }else{
    $('.persona_fisicadeutxt').css('display','none');
  }
  if($('#tipo_personadeu2').is(':checked')){
    $('.persona_moraldeutxt').css('display','block');
  }else{
    $('.persona_moraldeutxt').css('display','none');
  }
  if($('#tipo_personadeu3').is(':checked')){
    $('.fideicomisodeutxt').css('display','block');
  }else{
    $('.fideicomisodeutxt').css('display','none');
  }
}
function datos_bien_mutuoort_select(){
  if($('#datos_bien_mutuoort1').is(':checked')){
    $('.datos_inmuebleorttxt').css('display','block');
  }else{
    $('.datos_inmuebleorttxt').css('display','none');
  }
  if($('#datos_bien_mutuoort2').is(':checked')){
    $('.datos_otroorttxt').css('display','block');
  }else{
    $('.datos_otroorttxt').css('display','none');
  }
}
function tipo_persona_selectgar(){
  if($('#tipo_personagar1').is(':checked')){
    $('.persona_fisicagartxt').css('display','block');
  }else{
    $('.persona_fisicagartxt').css('display','none');
  }
  if($('#tipo_personagar2').is(':checked')){
    $('.persona_moralgartxt').css('display','block');
  }else{
    $('.persona_moralgartxt').css('display','none');
  }
  if($('#tipo_personagar3').is(':checked')){
    $('.fideicomisogartxt').css('display','block');
  }else{
    $('.fideicomisogartxt').css('display','none');
  }
}
// Tipo actividad 10
function tipo_persona_selectrea(){
  if($('#tipo_personarea1').is(':checked')){
    $('.persona_fisicareatxt').css('display','block');
  }else{
    $('.persona_fisicareatxt').css('display','none');
  }
  if($('#tipo_personarea2').is(':checked')){
    $('.persona_moralreatxt').css('display','block');
  }else{
    $('.persona_moralreatxt').css('display','none');
  }
  if($('#tipo_personarea3').is(':checked')){
    $('.fideicomisoreatxt').css('display','block');
  }else{
    $('.fideicomisoreatxt').css('display','none');
  }
}
//================================
//Constitución de personas Morales
function agregar_accionistas_socios(){
  add_accionistas_socios(0,'','','','','','','','','','','','','','','','','','','');
}
var aux_accs=0;
function add_accionistas_socios(id,cargo_accionistaco,tipo_personas_acc,nombre_acc,apellido_paterno_acc,apellido_materno_acc,fecha_nacimiento_acc,rfc_acc,curp_acc,pais_nacionalidad_acc,pais_acc,denominacion_razonm4_acc,fecha_constitucionm_acc,rfcm_acc,pais_nacionalidadm_acc,paism_acc,denominacion_razonf_acc,rfcf_acc,identificador_fideicomiso_accf,numero_accionesf_acc){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //alert(nombre_acc);
  ///////////////////////////////////
  var cargo_accionistaco1=''; var cargo_accionistaco2=''; var cargo_accionistaco3=''; var cargo_accionistaco4=''; var cargo_accionistaco5='';
  if(cargo_accionistaco==1){
    cargo_accionistaco1='selected'; cargo_accionistaco2=''; cargo_accionistaco3=''; cargo_accionistaco4=''; cargo_accionistaco5='';
  }else if(cargo_accionistaco==2){
    cargo_accionistaco1=''; cargo_accionistaco2='selected'; cargo_accionistaco3=''; cargo_accionistaco4=''; cargo_accionistaco5='';
  }else if(cargo_accionistaco==3){
    cargo_accionistaco1=''; cargo_accionistaco2=''; cargo_accionistaco3='selected'; cargo_accionistaco4=''; cargo_accionistaco5='';
  }else if(cargo_accionistaco==4){
    cargo_accionistaco1=''; cargo_accionistaco2=''; cargo_accionistaco3=''; cargo_accionistaco4='selected'; cargo_accionistaco5='';
  }else if(cargo_accionistaco==5){
    cargo_accionistaco1=''; cargo_accionistaco2=''; cargo_accionistaco3=''; cargo_accionistaco4=''; cargo_accionistaco5='selected';
  }
  //////////////////////////////////
  var tipo_personas_acct1=''; var tipo_personas_acct2=''; var tipo_personas_acct3='';
  if(tipo_personas_acc==1){
    tipo_personas_acct1='checked'; tipo_personas_acct2=''; tipo_personas_acct3='';
  }else if(tipo_personas_acc==2){
    tipo_personas_acct1=''; tipo_personas_acct2='checked'; tipo_personas_acct3='';
  }else if(tipo_personas_acc==3){
    tipo_personas_acct1=''; tipo_personas_acct2=''; tipo_personas_acct3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios_'+aux_accs+'">\
              <input class="form-control" type="hidden" id="idaccc" value="'+id+'">';
     html+='<div class="row">\
              <div class="col-md-6 form-group">\
                <label>Cargo del accionista dentro de la persona moral</label>\
                <select class="form-control" id="cargo_accionistaco">\
                  <option value="1" '+cargo_accionistaco1+'>Administrador único</option>\
                  <option value="2" '+cargo_accionistaco2+'>Presidente del consejo de administración u órgano equivalente</option>\
                  <option value="3" '+cargo_accionistaco3+'>Otro miembro del consejo de administración u órgano equivalente</option>\
                  <option value="4" '+cargo_accionistaco4+'>Comisario o Miembro del consejo de vigilancia u órgano equivalente</option>\
                  <option value="5" '+cargo_accionistaco5+'>No aplica</option>\
                </select>\
              </div>\
            </div>\
            <h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_personac" name="tipo_personas_1_'+aux_accs+'" id="tipo_persona1_1_'+aux_accs+'" value="1" checked '+tipo_personas_acct1+' onclick="tipo_personav_select(1,'+aux_accs+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_personac" name="tipo_personas_1_'+aux_accs+'" id="tipo_persona2_1_'+aux_accs+'" value="2" '+tipo_personas_acct2+' onclick="tipo_personav_select(1,'+aux_accs+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_personac" name="tipo_personas_1_'+aux_accs+'" id="tipo_persona3_1_'+aux_accs+'" value="3" '+tipo_personas_acct3+' onclick="tipo_personav_select(1,'+aux_accs+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="persona_fisicavtxt_1_'+aux_accs+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombreco" value="'+nombre_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paternoco" value="'+apellido_paterno_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_maternoco" value="'+apellido_materno_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimientoco" value="'+fecha_nacimiento_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcco" value="'+rfc_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curpco" value="'+curp_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadco pais_accos_'+aux_accs+'" id="pais_nacionalidad_accs_'+aux_accs+'">';
                    if(pais_nacionalidad_acc!=''){
                html+='<option value="'+pais_nacionalidad_acc+'">'+pais_acc+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="persona_moralvtxt_1_'+aux_accs+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonmco" value="'+denominacion_razonm4_acc+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionmco" value="'+fecha_constitucionm_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcmco" value="'+rfcm_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadmco pais_accsomv_'+aux_accs+'" id="pais_nacionalidad_accms_'+aux_accs+'">';
                    if(pais_nacionalidadm_acc!=''){
                html+='<option value="'+pais_nacionalidadm_acc+'">'+paism_acc+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="fideicomisovtxt_1_'+aux_accs+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonfco" value="'+denominacion_razonf_acc+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcfco" value="'+rfcf_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisofco" value="'+identificador_fideicomiso_accf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesfco" value="'+numero_accionesf_acc+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accs==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn gradient_nepal2" onclick="remover_anexo12a_constitucion_personas_morales_socios('+aux_accs+','+id+','+aviso_tipo+')"><i class="fa fa-trash-o"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios').append(html);
  getpais_socios('pais_accos',aux_accs); 
  getpais_socios('pais_accsomv',aux_accs); 
  tipo_personav_select(1,aux_accs);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_accs++;   
}
//==========
function tabla_anexo12a_constitucion_personas_morales_socios(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12a_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios(element.id,
                                        element.cargo_accionistaco,
                                        element.tipo_personac,
                                        element.nombreco,
                                        element.apellido_paternoco,
                                        element.apellido_maternoco,
                                        element.fecha_nacimientoco,
                                        element.rfcco,
                                        element.curpco,
                                        element.pais_nacionalidadco,
                                        element.pais_acc,
                                        element.denominacion_razonmco,
                                        element.fecha_constitucionmco,
                                        element.rfcmco,
                                        element.pais_nacionalidadmco,
                                        element.paism_acc,
                                        element.denominacion_razonfco,
                                        element.rfcfco,
                                        element.identificador_fideicomisofco,
                                        element.numero_accionesfco);
                });
            }else{
                agregar_accionistas_socios();
            }   
        }
    });
}
function remover_anexo12a_constitucion_personas_morales_socios(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12a_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios_'+aux).remove();
  }
}
/*********************************/
//Tipo actividad 3
//================================
//Datos de los accionistas vigentes
function agregar_accionistas(){
  add_accionistas(0,'','','','','','','','','','','','','','','','','','');
}
var aux_acc=0;
function add_accionistas(id,tipo_personas_acc,nombre_acc,apellido_paterno_acc,apellido_materno_acc,fecha_nacimiento_acc,rfc_acc,curp_acc,pais_nacionalidad_acc,pais_acc,denominacion_razonm4_acc,fecha_constitucionm_acc,rfcm_acc,pais_nacionalidadm_acc,paism_acc,denominacion_razonf_acc,rfcf_acc,identificador_fideicomiso_accf,numero_accionesf_acc){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  var tipo_personas_acct1=''; var tipo_personas_acct2=''; var tipo_personas_acct3='';
  if(tipo_personas_acc==1){
    tipo_personas_acct1='checked'; tipo_personas_acct2=''; tipo_personas_acct3='';
  }else if(tipo_personas_acc==2){
    tipo_personas_acct1=''; tipo_personas_acct2='checked'; tipo_personas_acct3='';
  }else if(tipo_personas_acc==3){
    tipo_personas_acct1=''; tipo_personas_acct2=''; tipo_personas_acct3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="accionistas_socios_'+aux_acc+'">\
              <input class="form-control" type="hidden" id="idacc" value="'+id+'">';
        html+='<div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_personas_acc" name="tipo_personas_3_'+aux_acc+'" id="tipo_persona1_3_'+aux_acc+'" value="1" checked '+tipo_personas_acct1+' onclick="tipo_personav_select(3,'+aux_acc+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_personas_acc" name="tipo_personas_3_'+aux_acc+'" id="tipo_persona2_3_'+aux_acc+'" value="2" '+tipo_personas_acct2+' onclick="tipo_personav_select(3,'+aux_acc+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_personas_acc" name="tipo_personas_3_'+aux_acc+'" id="tipo_persona3_3_'+aux_acc+'" value="3" '+tipo_personas_acct3+' onclick="tipo_personav_select(3,'+aux_acc+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="persona_fisicavtxt_3_'+aux_acc+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre_acc" value="'+nombre_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno_acc" value="'+apellido_paterno_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno_acc" value="'+apellido_materno_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento_acc" value="'+fecha_nacimiento_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc_acc" value="'+rfc_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp_acc" value="'+curp_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad_acc pais_accs_'+aux_acc+'" id="pais_nacionalidad_acc_'+aux_acc+'">';
                    if(pais_nacionalidad_acc!=''){
                html+='<option value="'+pais_nacionalidad_acc+'">'+pais_acc+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="persona_moralvtxt_3_'+aux_acc+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm4_acc" value="'+denominacion_razonm4_acc+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm_acc" value="'+fecha_constitucionm_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm_acc" value="'+rfcm_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm_acc pais_accsmv_'+aux_acc+'" id="pais_nacionalidadm_acc'+aux_acc+'">';
                    if(pais_nacionalidadm_acc!=''){
                html+='<option value="'+pais_nacionalidadm_acc+'">'+paism_acc+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="fideicomisovtxt_3_'+aux_acc+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf_acc" value="'+denominacion_razonf_acc+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf_acc" value="'+rfcf_acc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomiso_accf" value="'+identificador_fideicomiso_accf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf_acc" value="'+numero_accionesf_acc+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_acc==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn gradient_nepal2" onclick="remover_anexo12a_modificacion_patrimonial_socios('+aux_acc+','+id+','+aviso_tipo+')"><i class="fa fa-trash-o"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div>';
   html+='</div>';
  $('.accionistas_vigentes').append(html);
  getpais_socios('pais_accs',aux_acc); 
  getpais_socios('pais_accsmv',aux_acc); 
  tipo_personav_select(3,aux_acc);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_acc++;   
}
//==========
function tabla_anexo12a_modificacion_patrimonial_socios(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12a_modificacion_patrimonial_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas(element.id,element.tipo_personas_acc,element.nombre_acc,element.apellido_paterno_acc,element.apellido_materno_acc,element.fecha_nacimiento_acc,element.rfc_acc,element.curp_acc,element.pais_nacionalidad_acc,
                                  element.pais_acc,element.denominacion_razonm4_acc,element.fecha_constitucionm_acc,element.rfcm_acc,element.pais_nacionalidadm_acc,element.paism_acc,element.denominacion_razonf_acc,element.rfcf_acc,element.identificador_fideicomiso_accf,element.numero_accionesf_acc);
                });
            }else{
                agregar_accionistas();
            }   
        }
    });
}
function remover_anexo12a_modificacion_patrimonial_socios(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12a_modificacion_patrimonial_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.accionistas_socios_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.accionistas_socios_'+aux).remove();
  }
}
//Tipo actividad 4
//===========================================================================
//Datos de los accionistas socios funcionantes
function agregar_accionistas_socios_funcionantes(){
  add_accionistas_socios_funcionantes(0,'','','','','','','','','','','','','','','','','','');
}
var aux_acc4=0;
function add_accionistas_socios_funcionantes(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="accionistas_socios4_'+aux_acc4+'">\
              <input class="form-control" type="hidden" id="id4" value="'+id+'">';
        html+='<div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_persona_4_'+aux_acc4+'" id="tipo_persona1_4_'+aux_acc4+'" value="1" checked '+tipo_persona1+' onclick="tipo_personav_select(4,'+aux_acc4+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_persona_4_'+aux_acc4+'" id="tipo_persona2_4_'+aux_acc4+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(4,'+aux_acc4+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_persona_4_'+aux_acc4+'" id="tipo_persona3_4_'+aux_acc4+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(4,'+aux_acc4+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="persona_fisicavtxt_4_'+aux_acc4+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre4" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno4" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno4" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento4" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc4" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp4" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad4 pais4_'+aux_acc4+'" id="pais_nacionalidad4_'+aux_acc4+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="persona_moralvtxt_4_'+aux_acc4+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm4" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm4" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm4" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm4 paism4_'+aux_acc4+'" id="pais_nacionalidadm4'+aux_acc4+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="fideicomisovtxt_4_'+aux_acc4+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf4" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf4" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof4" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf4" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_acc4==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios_funcionantes()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo12a_fusion_socios_funcionantes('+aux_acc4+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div>';
   html+='</div>';
  $('.accionistas_socios_fucionante').append(html);
  getpais_socios('pais4',aux_acc4); 
  getpais_socios('paism4',aux_acc4); 
  tipo_personav_select(4,aux_acc4);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_acc4++;   
}
function tabla_anexo12a_fusion_socios_funcionantes(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12a_fusion_socios_funcionantes",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios_funcionantes(element.id,element.tipo_persona,element.nombre,element.apellido_paterno,element.apellido_materno,element.fecha_nacimiento,element.rfc,element.curp,element.pais_nacionalidad,
                                  element.pais,element.denominacion_razonm,element.fecha_constitucionm,element.rfcm,element.pais_nacionalidadm,element.paism,element.denominacion_razonf,element.rfcf,element.identificador_fideicomisof,element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios_funcionantes();
            }   
        }
    });
}
function remover_anexo12a_fusion_socios_funcionantes(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12a_fusion_socios_funcionantes_aviso',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.accionistas_socios4_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.accionistas_socios4_'+aux).remove();
  }
}
//===========================================================================
//Tipo actividad 5
//================================
//Datos de los accionistas socios funcionantes
function agregar_accionistas_socios_escision(){
  add_accionistas_socios_escision(0,'','','','','','','','','','','','','','','','','','');
}
var aux_acc5=0;
function add_accionistas_socios_escision(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////

  var aviso_tipo=$("#aviso").val();
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="accionistas_socios5_'+aux_acc5+'">\
              <input class="form-control" type="hidden" id="id5" value="'+id+'">';
        html+='<div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona5" name="tipo_persona_5_'+aux_acc5+'" id="tipo_persona1_5_'+aux_acc5+'" value="1" checked '+tipo_persona1+' onclick="tipo_personav_select(5,'+aux_acc5+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona5" name="tipo_persona_5_'+aux_acc5+'" id="tipo_persona2_5_'+aux_acc5+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(5,'+aux_acc5+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona5" name="tipo_persona_5_'+aux_acc5+'" id="tipo_persona3_5_'+aux_acc5+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(5,'+aux_acc5+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="persona_fisicavtxt_5_'+aux_acc5+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre5" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno5" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno5" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento5" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc5" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp5" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad5 pais5_'+aux_acc5+'" id="pais_nacionalidad5_'+aux_acc5+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="persona_moralvtxt_5_'+aux_acc5+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-9 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm5" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm5" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm5" value="'+rfcm+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm5 paism5_'+aux_acc5+'" id="pais_nacionalidadm5'+aux_acc5+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="fideicomisovtxt_5_'+aux_acc5+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf5" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf5" value="'+rfcf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof5" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf5" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_acc5==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios_escision()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo12a_escision_socios_subsiste('+aux_acc5+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div>';
   html+='</div>';
  $('.accionistas_socios_escision').append(html);
  getpais_socios('pais5',aux_acc5); 
  getpais_socios('paism5',aux_acc5); 
  tipo_personav_select(5,aux_acc5);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_acc5++;   
}
function tabla_anexo12a_accionistas_socios_escision(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12a_escision_socios_subsiste",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios_escision(element.id,element.tipo_persona,element.nombre,element.apellido_paterno,element.apellido_materno,element.fecha_nacimiento,element.rfc,element.curp,element.pais_nacionalidad,
                                  element.pais,element.denominacion_razonm,element.fecha_constitucionm,element.rfcm,element.pais_nacionalidadm,element.paism,element.denominacion_razonf,element.rfcf,element.identificador_fideicomisof,element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios_escision();
            }   
        }
    });
}
function remover_anexo12a_escision_socios_subsiste(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12a_escision_socios_subsiste',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.accionistas_socios5_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.accionistas_socios5_'+aux).remove();
  }
}
//Tipo actividad 5 Escindida
//================================
//Datos de los accionistas socios funcionantes
function agregar_accionistas_socios_escindida(){
  add_accionistas_socios_escindida(0,'','','','','','','','','','','','','','','','','','');
}
var aux_acc6=0;
function add_accionistas_socios_escindida(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////

  var aviso_tipo=$("#aviso").val();
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="accionistas_socios6_'+aux_acc6+'">\
              <input class="form-control" type="hidden" id="id6" value="'+id+'">';
        html+='<div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona6" name="tipo_persona_6_'+aux_acc6+'" id="tipo_persona1_6_'+aux_acc6+'" value="1" checked '+tipo_persona1+' onclick="tipo_personav_select(6,'+aux_acc6+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona6" name="tipo_persona_6_'+aux_acc6+'" id="tipo_persona2_6_'+aux_acc6+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(6,'+aux_acc6+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona6" name="tipo_persona_6_'+aux_acc6+'" id="tipo_persona3_6_'+aux_acc6+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(6,'+aux_acc6+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="persona_fisicavtxt_6_'+aux_acc6+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre6" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno6" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno6" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento6" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc6" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp6" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad6 pais6_'+aux_acc6+'" id="pais_nacionalidad6_'+aux_acc6+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="persona_moralvtxt_6_'+aux_acc6+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-9 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm6" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm6" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm6" value="'+rfcm+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Clave país nacionalidad</label><br>\
                  <select class="form-control pais_nacionalidadm6 paism6_'+aux_acc6+'" id="pais_nacionalidadm6'+aux_acc6+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="fideicomisovtxt_6_'+aux_acc6+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf6" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf6" value="'+rfcf+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof6" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf6" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_acc6==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios_escindida()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo12a_escision_socios_escindida('+aux_acc6+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div>';
   html+='</div>';
  $('.accionistas_socios_escindida').append(html);
  getpais_socios('pais6',aux_acc6); 
  getpais_socios('paism6',aux_acc6); 
  tipo_personav_select(6,aux_acc6);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_acc6++;   
}
function remover_anexo12a_escision_socios_escindida(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12a_escision_socios_escindida',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.accionistas_socios6_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.accionistas_socios6_'+aux).remove();
  }
}
function tabla_anexo12a_accionistas_socios_escindida(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12a_escision_socios_escindida",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios_escindida(element.id,element.tipo_persona,element.nombre,element.apellido_paterno,element.apellido_materno,element.fecha_nacimiento,element.rfc,element.curp,element.pais_nacionalidad,
                                  element.pais,element.denominacion_razonm,element.fecha_constitucionm,element.rfcm,element.pais_nacionalidadm,element.paism,element.denominacion_razonf,element.rfcf,element.identificador_fideicomisof,element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios_escindida();
            }   
        }
    });
}
//Tipo actividad 7 Constitución o Modificación de Fideicomiso traslativo de Dominio o en garantía sobre inmuebles
//================================
//Datos de los fideicomitentes
function agregar_datos_fideicomitente(){
  add_datos_fideicomitente(0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
}
var aux_acc7=0;
function add_datos_fideicomitente(id,tipo_movimiento_fideicomitente,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,actividad_economica,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,giro_mercantil,denominacion_razonf,rfcf,identificador_fideicomisof,datos_tipo_patrimoniofi,monedafi,monto_operacionfi,tipo_inmueblefi,codigo_postalfi,folio_realfi,importe_garantiafi,descripcionfi,valor_bienfi){
  var aviso_tipo=$("#aviso").val();
  /////////////////////////////////////////////////////////////////////////////////
  var tipo_fide1=''; var tipo_fide2=''; var tipo_fide3='';
  if(tipo_movimiento_fideicomitente==1){
    tipo_fide1='selected'; tipo_fide2=''; tipo_fide3='';
  }else if(tipo_movimiento_fideicomitente==2){
    tipo_fide1=''; tipo_fide2='selected'; tipo_fide3='';
  }else if(tipo_movimiento_fideicomitente==3){
    tipo_fide1=''; tipo_fide2=''; tipo_fide3='selected';
  }
  /// 
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  var datos_tipo_patrimoniofi1=''; var datos_tipo_patrimoniofi2=''; var datos_tipo_patrimoniofi3='';
  if(datos_tipo_patrimoniofi==1){
    datos_tipo_patrimoniofi1='checked'; datos_tipo_patrimoniofi2=''; datos_tipo_patrimoniofi3='';
  }else if(datos_tipo_patrimoniofi==2){
    datos_tipo_patrimoniofi1=''; datos_tipo_patrimoniofi2='checked'; datos_tipo_patrimoniofi3='';
  }else if(datos_tipo_patrimoniofi==3){
    datos_tipo_patrimoniofi1=''; datos_tipo_patrimoniofi2=''; datos_tipo_patrimoniofi3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="accionistas_socios7_'+aux_acc7+'">\
              <input class="form-control" type="hidden" id="id7" value="'+id+'">';
        html+='<div class="row">\
                <div class="col-md-6 form-group">\
                  <label>Tipo de movimiento del fideicomitente</label>\
                  <select class="form-control" id="tipo_movimiento_fideicomitente7">\
                    <option value="1" '+tipo_fide1+'>Incorporación</option>\
                    <option value="2" '+tipo_fide2+'>Destitución</option>\
                    <option value="3" '+tipo_fide3+'>Sin modificación</option>\
                  </select>\
                </div>\
              </div>\
              <h5>Tipo de Persona</h5>';      
        html+='<div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona7" name="tipo_persona_7_'+aux_acc7+'" id="tipo_persona1_7_'+aux_acc7+'" value="1" checked '+tipo_persona1+' onclick="tipo_personav_select(7,'+aux_acc7+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona7" name="tipo_persona_7_'+aux_acc7+'" id="tipo_persona2_7_'+aux_acc7+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(7,'+aux_acc7+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona7" name="tipo_persona_7_'+aux_acc7+'" id="tipo_persona3_7_'+aux_acc7+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(7,'+aux_acc7+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="persona_fisicavtxt_7_'+aux_acc7+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre7" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno7" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno7" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento7" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc7" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp7" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad7 pais7_'+aux_acc7+'" id="pais_nacionalidad7_'+aux_acc7+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
                <div class="col-md-12 form-group">\
                  <div class="actividad_economicatxt7_'+aux_acc7+'"></div>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="persona_moralvtxt_7_'+aux_acc7+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-9 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm7" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm7" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-8 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm7" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label><br>\
                  <select class="form-control pais_nacionalidadm7 paism7_'+aux_acc7+'" id="pais_nacionalidadm7'+aux_acc7+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
                <div class="col-md-12 form-group">\
                  <div class="giro_mercantiltxt7_'+aux_acc7+'"></div>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="fideicomisovtxt_7_'+aux_acc7+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf7" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf7" value="'+rfcf+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof7" value="'+identificador_fideicomisof+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <h5>Datos del tipo de patrimonio de constitución o modificación</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input datos_tipo_patrimoniofi" name="datos_tipo_patrimoniofi_'+aux_acc7+'" value="1" id="datos_tipo_patrimoniofi1_'+aux_acc7+'" checked '+datos_tipo_patrimoniofi1+' onclick="datos_tipo_patrimoniofi_select('+aux_acc7+')">\
                   Monetario\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input datos_tipo_patrimoniofi" name="datos_tipo_patrimoniofi_'+aux_acc7+'" value="2" id="datos_tipo_patrimoniofi2_'+aux_acc7+'" '+datos_tipo_patrimoniofi2+' onclick="datos_tipo_patrimoniofi_select('+aux_acc7+')">\
                   Inmueble\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input datos_tipo_patrimoniofi" name="datos_tipo_patrimoniofi_'+aux_acc7+'" value="3" id="datos_tipo_patrimoniofi3_'+aux_acc7+'" '+datos_tipo_patrimoniofi3+' onclick="datos_tipo_patrimoniofi_select('+aux_acc7+')">\
                   Otro bien\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="patrimonio_monetariotxt_'+aux_acc7+'" style="display: none">\
              <div class="row">  \
                <div class="col-md-4 form-group">\
                  <label>Tipo de moneda o divisa del patrimonio</label>\
                  <div class="tipo_moneda_select_'+aux_acc7+'"></div>\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Monto del patrimonio</label>\
                  <input class="form-control" type="number" id="monto_operacionfi" maxlength="17" value="'+monto_operacionfi+'">\
                </div>\
              </div>\
            </div>\
            <div class="patrimonio_inmuebletxt_'+aux_acc7+'" style="display: none">\
              <div class="row">  \
                <div class="col-md-4 form-group">\
                  <label>Tipo de inmueble</label>\
                  <div class="tipo_inmueble_select_'+aux_acc7+'"></div>\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Código Postal de la ubicación del inmueble</label>\
                  <input class="form-control" type="text" id="codigo_postalfi" value="'+codigo_postalfi+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Folio real del inmueble o antecedentes registrales</label>\
                  <input class="form-control" type="text" id="folio_realfi" value="'+folio_realfi+'">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Importe que garantiza el inmueble que se aporta al Fideicomiso en Moneda Nacional</label>\
                  <input class="form-control" type="number" id="importe_garantiafi" minlength="4" maxlength="17" value="'+importe_garantiafi+'">\
                </div>\
              </div>\
            </div>\
            <div class="patrimonio_otro_bientxt_'+aux_acc7+'" style="display: none">\
              <div class="row">\
                <div class="col-md-12 form-group">\
                  <label>Descripción del bien</label>\
                  <input class="form-control" type="text" id="descripcionfi" value="'+descripcionfi+'">\
                </div>\
              </div>  \
              <div class="row">\
                <div class="col-md-6 form-group">\
                  <label>Valor del bien que se aporta al Fideicomiso en Moneda Nacional</label>\
                  <input class="form-control" type="number" id="valor_bienfi" minlength="4" maxlength="17" value="'+valor_bienfi+'">\
                </div>\
              </div>  \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_acc7==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_datos_fideicomitente()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo12a_datos_fideicomitente('+aux_acc7+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div>';
   html+='</div>';
  $('.datos_fideicomitente').append(html);
  getpais_socios('pais7',aux_acc7); 
  getpais_socios('paism7',aux_acc7); 
  tipo_personav_select(7,aux_acc7);
  agregar_actividad_economica(aux_acc7,7,actividad_economica);
  agregar_giro_mercantil(aux_acc7,7,giro_mercantil);
  datos_tipo_patrimoniofi_select(aux_acc7);
  agregartipo_monedaAct7(aux_acc7,monedafi,7);
  agregartipo_inmueble(aux_acc7,tipo_inmueblefi);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_acc7++;   
}
function tabla_anexo12a_datos_fideicomitente(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12a_datos_fideicomitente",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_datos_fideicomitente(element.id,element.tipo_movimiento_fideicomitente,element.tipo_persona,element.nombre,element.apellido_paterno,element.apellido_materno,element.fecha_nacimiento,element.rfc,element.curp,element.pais_nacionalidad,
                                  element.pais,element.actividad_economica,element.denominacion_razonm,element.fecha_constitucionm,element.rfcm,element.pais_nacionalidadm,element.paism,element.giro_mercantil,element.denominacion_razonf,element.rfcf,element.identificador_fideicomisof,
                                  element.datos_tipo_patrimoniofi,element.monedafi,element.monto_operacionfi,element.tipo_inmueblefi,element.codigo_postalfi,element.folio_realfi,element.importe_garantiafi,element.descripcionfi,element.valor_bienfi);
                });
            }else{
                agregar_datos_fideicomitente();
            }   
        }
    });
}
function remover_anexo12a_datos_fideicomitente(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12a_datos_fideicomitente',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.accionistas_socios7_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.accionistas_socios7_'+aux).remove();
  }
}
///
function agregar_datos_fideicomisarios(){
  add_datos_fideicomisarios(0,'','','','','','','','','','','','','','','','','','','','','');
}
var aux_acc8=0;
function add_datos_fideicomisarios(id,datos_fideicomisarios_determinados,tipo_movimiento_fideicomitente,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,actividad_economica,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,giro_mercantil,denominacion_razonf,rfcf,identificador_fideicomisof){
  var aviso_tipo=$("#aviso").val();
  /////////////////////////////////////////////////////////////////////////////////
  var datos_fi1=''; var datos_fi2='';
  if(datos_fideicomisarios_determinados=='SI'){
    datos_fi1='checked'; datos_fi2='';
  }if(datos_fideicomisarios_determinados=='NO'){
    datos_fi1=''; datos_fi2='checked';
  }
  //
  var tipo_fide1=''; var tipo_fide2=''; var tipo_fide3='';
  if(tipo_movimiento_fideicomitente==1){
    tipo_fide1='selected'; tipo_fide2=''; tipo_fide3='';
  }else if(tipo_movimiento_fideicomitente==2){
    tipo_fide1=''; tipo_fide2='selected'; tipo_fide3='';
  }else if(tipo_movimiento_fideicomitente==3){
    tipo_fide1=''; tipo_fide2=''; tipo_fide3='selected';
  }
  /// 
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="accionistas_socios8_'+aux_acc8+'">\
              <input class="form-control" type="hidden" id="id8" value="'+id+'">';
        html+='<div class="col-md-6 form-group">\
                    <label>Los datos del o los fideicomisarios se encuentran determinados</label>\
                    <div class="row">\
                      <div class="col-md-3 form-group">\
                        <div class="form-check form-check-success">\
                          <label class="form-check-label">\
                            <input type="radio" class="form-check-input datos_fideicomisarios_determinados8" name="datos_fideicomisarios_determinados8'+aux_acc8+'" value="SI" checked '+datos_fi1+'>\
                            SI \
                          </label>\
                        </div>\
                      </div>\
                      <div class="col-md-3 form-group">\
                        <div class="form-check form-check-success">\
                          <label class="form-check-label">\
                            <input type="radio" class="form-check-input datos_fideicomisarios_determinados8" name="datos_fideicomisarios_determinados8'+aux_acc8+'" value="NO" '+datos_fi2+'>\
                            NO\
                          </label>\
                        </div>\
                      </div>\
                    </div>\
                  </div>';  
        html+='<div class="row">\
                <div class="col-md-6 form-group">\
                  <label>Tipo de movimiento del fideicomitente</label>\
                  <select class="form-control" id="tipo_movimiento_fideicomitente8">\
                    <option value="1" '+tipo_fide1+'>Incorporación</option>\
                    <option value="2" '+tipo_fide2+'>Destitución</option>\
                    <option value="3" '+tipo_fide3+'>Sin modificación</option>\
                  </select>\
                </div>\
              </div><h5>Tipo de Persona</h5>';          
        html+='<div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona8" name="tipo_persona_8_'+aux_acc8+'" id="tipo_persona1_8_'+aux_acc8+'" value="1" checked '+tipo_persona1+' onclick="tipo_personav_select(8,'+aux_acc8+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona8" name="tipo_persona_8_'+aux_acc8+'" id="tipo_persona2_8_'+aux_acc8+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(8,'+aux_acc8+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona8" name="tipo_persona_8_'+aux_acc8+'" id="tipo_persona3_8_'+aux_acc8+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(8,'+aux_acc8+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="persona_fisicavtxt_8_'+aux_acc8+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre8" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno8" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno8" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento8" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc8" value="'+rfc+'" maxlength="13">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp8" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad8 pais8_'+aux_acc8+'" id="pais_nacionalidad8_'+aux_acc8+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
                <div class="col-md-12 form-group">\
                  <div class="actividad_economicatxt8_'+aux_acc8+'"></div>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="persona_moralvtxt_8_'+aux_acc8+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-9 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm8" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-3 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm8" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-8 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm8" value="'+rfcm+'" maxlength="13">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label><br>\
                  <select class="form-control pais_nacionalidadm8 paism8_'+aux_acc8+'" id="pais_nacionalidadm8'+aux_acc8+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
                <div class="col-md-12 form-group">\
                  <div class="giro_mercantiltxt8_'+aux_acc8+'"></div>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="fideicomisovtxt_8_'+aux_acc8+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-8 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf8" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf8" value="'+rfcf+'" maxlength="13">\
                </div>\
                <div class="col-md-6 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof8" value="'+identificador_fideicomisof+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_acc8==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_datos_fideicomisarios()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo12a_datos_fideicomisarios('+aux_acc8+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div>';
   html+='</div>';
  $('.datos_fideicomisarios').append(html);
  getpais_socios('pais8',aux_acc8); 
  getpais_socios('paism8',aux_acc8); 
  tipo_personav_select(8,aux_acc8);
  agregar_actividad_economica(aux_acc8,8,actividad_economica);
  agregar_giro_mercantil(aux_acc8,8,giro_mercantil);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_acc8++;   
}
function tabla_anexo12a_datos_fideicomisarios(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo12a_datos_fideicomisarios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_datos_fideicomisarios(element.id,element.datos_fideicomisarios_determinados,element.tipo_movimiento_fideicomitente,element.tipo_persona,element.nombre,element.apellido_paterno,element.apellido_materno,element.fecha_nacimiento,element.rfc,element.curp,element.pais_nacionalidad,
                                  element.pais,element.actividad_economica,element.denominacion_razonm,element.fecha_constitucionm,element.rfcm,element.pais_nacionalidadm,element.paism,element.giro_mercantil,element.denominacion_razonf,element.rfcf,element.identificador_fideicomisof);
                });
            }else{
                agregar_datos_fideicomisarios();
            }   
        }
    });
}
function remover_anexo12a_datos_fideicomisarios(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo12a_datos_fideicomisarios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.accionistas_socios8_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.accionistas_socios8_'+aux).remove();
  }
}
///////===========
function agregar_actividad_economica(aux_acc,tnum,actividad_economica){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_actividad_economica",
      data:{tnum:tnum,actividad_economica:actividad_economica},
      success: function (data) {
        $('.actividad_economicatxt'+tnum+'_'+aux_acc).html(data);
      }
    });
}
function agregar_giro_mercantil(aux_acc,tnum,giro_mercantil){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_giro_mercantil",
      data:{tnum:tnum,giro_mercantil:giro_mercantil},
      success: function (data) {
        $('.giro_mercantiltxt'+tnum+'_'+aux_acc).html(data);
      }
    });
}
//===========================================================================
function getpais_socios(txt,id){
  //console.log("get pais");
  $('.'+txt+'_'+id).select2({
      width: '350px',
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Transaccion/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
//==================================
//////////////////////////
// Datos de los accionistas 
function tipo_personav_select(txt,id){
  if($('#tipo_persona1_'+txt+'_'+id).is(':checked')){
    $('.persona_fisicavtxt_'+txt+'_'+id).css('display','block');
  }else{
    $('.persona_fisicavtxt_'+txt+'_'+id).css('display','none');
  }
  if($('#tipo_persona2_'+txt+'_'+id).is(':checked')){
    $('.persona_moralvtxt_'+txt+'_'+id).css('display','block');
  }else{
    $('.persona_moralvtxt_'+txt+'_'+id).css('display','none');
  }
  if($('#tipo_persona3_'+txt+'_'+id).is(':checked')){
    $('.fideicomisovtxt_'+txt+'_'+id).css('display','block');
  }else{
    $('.fideicomisovtxt_'+txt+'_'+id).css('display','none');
  }
}
//=======================
function getpais(txt,id){
  //console.log("get pais");
  $('.pais_'+txt+'_'+id).select2({
      width: '350px',
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Transaccion/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
//=======================================================================================================================//
function agregar_liqui(){
  addliquidacion(0,'','','',0);   
}
var aux_liqui_num=0;
function addliquidacion(id,fecha_pago,instrum_notario,tipo_moneda,monto){
    var aviso_tipo=$("#aviso").val();
    var fecha_max=$('#fecha_max').val();
    ///////////////////////////////////////
    var in1=''; var in2=''; var in3=''; var in4=''; var in5=''; var in6=''; var in7=''; var in8=''; var in9=''; 
    var in10=''; var in11=''; var in12=''; var in13=''; var in14=''; var in15=''; var in16=''; var in17='';
    if(instrum_notario==1){ in1='selected'; }
    else if(instrum_notario==2){in2='selected';}
    else if(instrum_notario==3){in3='selected';}
    else if(instrum_notario==4){in4='selected';}
    else if(instrum_notario==5){in5='selected';}
    else if(instrum_notario==6){in6='selected';}
    else if(instrum_notario==7){in7='selected';}
    else if(instrum_notario==8){in8='selected';}
    else if(instrum_notario==9){in9='selected';}
    else if(instrum_notario==10){in10='selected';}
    else if(instrum_notario==11){in11='selected';}
    else if(instrum_notario==12){in12='selected';}
    else if(instrum_notario==13){in13='selected';}
    else if(instrum_notario==14){in14='selected';}
    else if(instrum_notario==15){in15='selected';}
    else if(instrum_notario==16){in16='selected';}
    else if(instrum_notario==17){in17='selected';}
    ///////////////////////////////////////
    var html='<div class="row row_div_'+aux_liqui_num+'">\
                  <input type="hidden" id="id_a" value="'+id+'">\
                  <div class="col-md-3 form-group">\
                    <label>Fecha de pago</label>\
                    <input class="form-control fecha_pago_a_'+aux_liqui_num+'" max="'+fecha_max+'" type="date" id="fecha_pago_a" value="'+fecha_pago+'" onchange="validar_fecha_opera('+aux_liqui_num+')">\
                  </div>\
                  <div class="col-md-5 form-group">\
                    <label>Instrumento monetario con el que se realizó la operación o acto</label>\
                    <select id="instrum_notario_a" class="form-control" onchange="validarFechasPago();confirmarPagosMonto();">\
                      <option value="1" '+in1+'>Efectivo</option>\
                      <option value="2" '+in2+'>Tarjeta de Crédito</option>\
                      <option value="3" '+in3+'>Tarjeta de Debito</option>\
                      <option value="4" '+in4+'>Tarjeta de Prepago</option>\
                      <option value="5" '+in5+'>Cheque Nominativo</option>\
                      <option value="6" '+in6+'>Cheque de Caja</option>\
                      <option value="7" '+in7+'>Cheques de Viajero</option>\
                      <option value="8" '+in8+'>Transferencia Interbancaria</option>\
                      <option value="9" '+in9+'>Transferencia Misma Institución</option>\
                      <option value="10" '+in10+'>Transferencia Internacional</option>\
                      <option value="11" '+in11+'>Orden de Pago</option>\
                      <option value="12" '+in12+'>Giro</option>\
                      <option value="13" '+in13+'>Oro o Platino Amonedados</option>\
                      <option value="14" '+in14+'>Plata Amonedada</option>\
                      <option value="15" '+in15+'>Metales Preciosos</option>\
                      <option value="16" '+in16+'>Activos Virtuales</option>\
                      <option value="17" '+in17+'>Otros</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Tipo de moneda o divisa de la operación o acto</label>\
                    <div class="de_add_liquida tipo_moneda_select_6'+aux_liqui_num+'"></div>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Monto de la operación o acto sin IVA ni accesorios</label>\
                    <input class="form-control monto_a_'+aux_liqui_num+'" type="text" id="monto_a" value="'+monto+'" onchange="cambio_moneda_liqui('+aux_liqui_num+')">\
                  </div>\
                  <div class="row">\
                    <div class="col-md-10 form-group">\
                      <div>\
                        <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_liqui_num==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_liqui()"><i class="fa fa-plus"></i> Otra Liquidación</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="removerliquidacion('+aux_liqui_num+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar Liquidación</button>';
                        }
                        
                html+='</div>\
                    </div>\
                  </div>\
                  <div class="col-md-12">\
                    <hr class="subsubtitle">\
                  </div>\
              </div>';
    $('.liquidacion').append(html);
    agregartipo_moneda(aux_liqui_num,tipo_moneda,0);
    cambio_moneda_liqui(aux_liqui_num);
    aux_liqui_num++;
}
function tabla_liquidacion_anexo(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_liquidacion_anexo12a",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  addliquidacion(element.id,element.fecha_pago,element.instrumento_monetario,element.tipo_moneda,element.monto_operacion);
                });
            }else{
                agregar_liqui();
            }   
        }
    });
}
function agregartipo_moneda(id,tipo_moneda,act){
  console.log("act en tipo moneda: "+act);
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_moneda",
      data:{t_pago:tipo_moneda,act:act},
      success: function (data) {
        $('.tipo_moneda_select_6'+id).html(data);
      }
    });
}

function agregartipo_monedaAct7(id,tipo_moneda,act){
  console.log("act en tipo moneda: "+act);
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_monedaAct7",
      data:{t_pago:tipo_moneda,act:act},
      success: function (data) {
        $('.tipo_moneda_select_'+id).html(data);
      }
    });
}
function agregartipo_inmueble(id,tipo_inmueble){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_inmueble",
      data:{tipo_inmueble:tipo_inmueble},
      success: function (data) {
        $('.tipo_inmueble_select_'+id).html(data);
      }
    });
}
function removerliquidacion(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el pago!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_pago_anexo12a',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.row_div_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.row_div_'+aux).remove();
  }
}

function cambio_moneda_liqui(aux){
  var mon=$('.monto_a_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_a_"+aux+"").val(monto);
  //suma_tablaliquidacion();
  confirmarPagosMonto();
  if($("#tipo_actividad option:selected").val()==6){
    validarFechasPago();
  }
}
function suma_tablaliquidacion(){
  var min = true;
  var addtp = 0;
  var mte = 0;
  var TABLAP = $(".row_liquidacion .liquidacion > div");                  
    TABLAP.each(function(){         
        totalmonto = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        var vstotal = totalmonto;
        addtp += Number(vstotal);
        
        if($(this).find("select[id*='instrum_notario_a'] option:selected").val()==1){
          totalmontoe = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
          totalmontoe = replaceAll(totalmontoe, "$", "" );
          var vstotale = totalmontoe;
          mte += Number(vstotale);
        } 
    });
  //Monto total de la liquidación
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  //Monto total en efectivo
  ttle = replaceAll(mte, ",", "" );
  ttle = replaceAll(ttle, "$", "" );
  var monto_ttle=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(ttle);
  $('#total_liquida_efect').val(monto_ttle);
  
  mtl = replaceAll(addtp, ",", "" );
  mtl = replaceAll(mtl, "$", "" );
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mtl);
  $('#total_liquida').val(monto_mtl);
  
  var aux_mo=0;
  var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
  totalmontomo = replaceAll(totalmontomo, "$", "" );
  var vstotalmo = totalmontomo;
  aux_mo = Number(vstotalmo);


  var total_liquidado = replaceAll($('#total_liquida').val(), ",", "" );
  total_liquidado = replaceAll(total_liquidado, "$", "" );

  validar_xml=1;
  /*if(aux_mo==total_liquidado){
    validar_xml=1;
    $('.validacion_cantidad').html('');
  }else{
    validar_xml=0;
    $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
  }*/
  //confirmarPagosMonto();
}
function validar_fecha_opera(aux){
  var fecha_pago_liqui=$('.fecha_pago_a_'+aux).val();
  var fecha_operacion=$('#fecha_operacion').val();
  if(fecha_pago_liqui==fecha_operacion){ 
  }else{
    swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
  if($("#tipo_actividad option:selected").val()==6){
    validarFechasPago();
  }
}
//=======================================================================================================================//
function registrar(){
  var form_register = $('#form_anexo,#form_anexo2,#form_anexo3,#form_anexo4,#form_anexo5,#form_anexo6,#form_anexo7,#form_anexo8,#form_anexo9,#form_anexo10,#form_anexo11,#form_anexo12,#form_anexo13,#form_anexo14,#form_anexo15,#form_anexo16');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                /*monto_opera:{
                  required: true
                },*/
                referencia:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_anexo").valid();
        var band_aviso = 0;
        if(valid) {
          
          monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
          monto_opera = replaceAll(monto_oper, "$", "" );
          
          if($("#aviso").val()>0){
            name_table = "anexo12a_aviso";
          }else{
            name_table = "anexo12a";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }
          $("#btn_submit").attr("disabled",true);
          if($("#datos_tipo_patrimoniofi1_0").is(":checked")==true)
            var datos_tipo_patrimoniofi=1;
          else if($("#datos_tipo_patrimoniofi2_0").is(":checked")==true)
            var datos_tipo_patrimoniofi=2;
          else if($("#datos_tipo_patrimoniofi3_0").is(":checked")==true)
            var datos_tipo_patrimoniofi=3;
          var monto_operacionfi = $("#monto_operacionfi").val();
          var importe_garantiafi = $("#importe_garantiafi").val();
          var valor_bienfi = $("#valor_bienfi").val();
          var datos_anexo = form_register.serialize()+"&tipo_actividad="+$("#tipo_actividad").val()+"&tabla="+name_table+"&monto_opera="+monto_opera+"&datos_tipo_patrimoniofi="+datos_tipo_patrimoniofi+"&monto_operacionfi="+monto_operacionfi+"&importe_garantiafi="+importe_garantiafi+"&valor_bienfi="+valor_bienfi;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo12a',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = $.parseJSON(data);
                var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                //==========================================================================
                
                act_num_aux = $("#tipo_actividad option:selected").val();
                console.log("act_num_aux: "+act_num_aux);
                var band_umbral=false;
                var id_acusado=0;
                var suma_act=0;
                var umas_anexoAct6=0;
                //Tipo actividad 2
                if(act_num_aux==2){
                  var DATAPACCCS= [];
                  var TABLAPACCCS = $(".row_accionistas_socios .accionistas_socios > div");                  
                  TABLAPACCCS.each(function(){         
                        item = {};
                        item ["idanexo12a"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='idaccc']").val();
                        item ["cargo_accionistaco"]=$(this).find("select[id*='cargo_accionistaco']").val();
                        item ["tipo_personac"]=$(this).find("input:radio[class*='tipo_personac']:checked").val();
                        item ["nombreco"]=$(this).find("input[id*='nombreco']").val();
                        item ["apellido_paternoco"]=$(this).find("input[id*='apellido_paternoco']").val();
                        item ["apellido_maternoco"]=$(this).find("input[id*='apellido_maternoco']").val();
                        item ["fecha_nacimientoco"]=$(this).find("input[id*='fecha_nacimientoco']").val();

                        item ["rfcco"]=$(this).find("input[id*='rfcco']").val();
                        item ["curpco"]=$(this).find("input[id*='curpco']").val();
                        item ["pais_nacionalidadco"]=$(this).find("select[class*='pais_nacionalidadco']").val();

                        item ["denominacion_razonmco"]=$(this).find("input[id*='denominacion_razonmco']").val();
                        item ["fecha_constitucionmco"]=$(this).find("input[id*='fecha_constitucionmco']").val();
                        item ["rfcmco"]=$(this).find("input[id*='rfcmco']").val();
                        item ["pais_nacionalidadmco"]=$(this).find("select[class*='pais_nacionalidadmco']").val();

                        item ["denominacion_razonfco"]=$(this).find("input[id*='denominacion_razonfco']").val();
                        item ["rfcfco"]=$(this).find("input[id*='rfcfco']").val();
                        item ["identificador_fideicomisofco"]=$(this).find("input[id*='identificador_fideicomisofco']").val();
                        item ["numero_accionesfco"]=$(this).find("input[id*='numero_accionesfco']").val();
                        DATAPACCCS.push(item);
                  });
                  INFOPACCCS  = new FormData();
                  aInfopacccs   = JSON.stringify(DATAPACCCS);
                  INFOPACCCS.append('data', aInfopacccs);
                  $.ajax({
                      data: INFOPACCCS,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12a_constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                }else if(act_num_aux==3){
                //Tipo actividad 3
                  var DATAPACCS= [];
                  var TABLAPACCS = $(".row_accionistas_vigentes .accionistas_vigentes > div");                  
                  TABLAPACCS.each(function(){         
                        item = {};
                        item ["idanexo12a"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["idacc"]=$(this).find("input[id*='idacc']").val();
                        item ["tipo_personas_acc"]=$(this).find("input:radio[class*='tipo_personas_acc']:checked").val();
                        item ["nombre_acc"]=$(this).find("input[id*='nombre_acc']").val();
                        item ["apellido_paterno_acc"]=$(this).find("input[id*='apellido_paterno_acc']").val();
                        item ["apellido_materno_acc"]=$(this).find("input[id*='apellido_materno_acc']").val();
                        item ["fecha_nacimiento_acc"]=$(this).find("input[id*='fecha_nacimiento_acc']").val();

                        item ["rfc_acc"]=$(this).find("input[id*='rfc_acc']").val();
                        item ["curp_acc"]=$(this).find("input[id*='curp_acc']").val();
                        item ["pais_nacionalidad_acc"]=$(this).find("select[class*='pais_nacionalidad_acc']").val();

                        item ["denominacion_razonm4_acc"]=$(this).find("input[id*='denominacion_razonm4_acc']").val();
                        item ["fecha_constitucionm_acc"]=$(this).find("input[id*='fecha_constitucionm_acc']").val();
                        item ["rfcm_acc"]=$(this).find("input[id*='rfcm_acc']").val();
                        item ["pais_nacionalidadm_acc"]=$(this).find("select[class*='pais_nacionalidadm_acc']").val();

                        item ["denominacion_razonf_acc"]=$(this).find("input[id*='denominacion_razonf_acc']").val();
                        item ["rfcf_acc"]=$(this).find("input[id*='rfcf_acc']").val();
                        item ["identificador_fideicomiso_accf"]=$(this).find("input[id*='identificador_fideicomiso_accf']").val();
                        item ["numero_accionesf_acc"]=$(this).find("input[id*='numero_accionesf_acc']").val();
                        DATAPACCS.push(item);
                  });
                  INFOPACCS  = new FormData();
                  aInfopaccs   = JSON.stringify(DATAPACCS);
                  INFOPACCS.append('data', aInfopaccs);
                  $.ajax({
                      data: INFOPACCS,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12a_accionistas_vigentes',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                }else if(act_num_aux==4){
                //Tipo actividad 4
                  var DATAPACC4= [];
                  var TABLAPACC4 = $(".row_accionistas_socios_fucionante .accionistas_socios_fucionante > div");                  
                  TABLAPACC4.each(function(){         
                        item = {};
                        item ["idanexo12a"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id4']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona4']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre4']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno4']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno4']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento4']").val();
                        item ["rfc"]=$(this).find("input[id*='rfc4']").val();
                        item ["curp"]=$(this).find("input[id*='curp4']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad4']").val();
                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm4']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm4']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm4']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm4']").val();
                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf4']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf4']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof4']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf4']").val();
                        DATAPACC4.push(item);
                  });
                  INFOPACC4  = new FormData();
                  aInfopacc4   = JSON.stringify(DATAPACC4);
                  INFOPACC4.append('data', aInfopacc4);
                  $.ajax({
                      data: INFOPACC4,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12a_fusion_socios_funcionantes',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                }else if(act_num_aux==5){
                  //Tipo actividad 5 Subsiste
                  var DATAPACC5= [];
                  var TABLAPACC5 = $(".row_accionistas_socios_escision .accionistas_socios_escision > div");                  
                  TABLAPACC5.each(function(){         
                        item = {};
                        item ["idanexo12a"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id5']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona5']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre5']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno5']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno5']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento5']").val();
                        item ["rfc"]=$(this).find("input[id*='rfc5']").val();
                        item ["curp"]=$(this).find("input[id*='curp5']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad5']").val();
                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm5']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm5']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm5']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm5']").val();
                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf5']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf5']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof5']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf5']").val();
                        DATAPACC5.push(item);
                  });
                  INFOPACC5  = new FormData();
                  aInfopacc5   = JSON.stringify(DATAPACC5);
                  INFOPACC5.append('data', aInfopacc5);
                  $.ajax({
                      data: INFOPACC5,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12a_escision_socios_subsiste',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //Tipo actividad 5 Escindida 
                  var DATAPACC6= [];
                  var TABLAPACC6 = $(".row_accionistas_socios_escindida .accionistas_socios_escindida > div");                  
                  TABLAPACC6.each(function(){         
                        item = {};
                        item ["idanexo12a"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id6']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona6']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre6']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno6']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno6']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento6']").val();
                        item ["rfc"]=$(this).find("input[id*='rfc6']").val();
                        item ["curp"]=$(this).find("input[id*='curp6']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad6']").val();
                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm6']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm6']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm6']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm6']").val();
                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf6']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf6']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof6']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf6']").val();
                        DATAPACC6.push(item);
                  });
                  INFOPACC6  = new FormData();
                  aInfopacc6   = JSON.stringify(DATAPACC6);
                  INFOPACC6.append('data', aInfopacc6);
                  $.ajax({
                      data: INFOPACC6,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12a_escision_socios_escindida',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                }else if(act_num_aux==7){
                  //Tipo actividad 7 
                  var DATAPACC7= [];
                  var TABLAPACC7 = $(".row_datos_fideicomitente .datos_fideicomitente > div");                  
                  TABLAPACC7.each(function(){         
                        item = {};
                        item ["idanexo12a"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id7']").val();
                        item ["tipo_movimiento_fideicomitente"]=$(this).find("select[id*='tipo_movimiento_fideicomitente7']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona7']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre7']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno7']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno7']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento7']").val();
                        item ["rfc"]=$(this).find("input[id*='rfc7']").val();
                        item ["curp"]=$(this).find("input[id*='curp7']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad7']").val();
                        item ["actividad_economica"]=$(this).find("select[id*='actividad_economica7']").val();
                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm7']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm7']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm7']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm7']").val();
                        item ["giro_mercantil"]=$(this).find("select[id*='giro_mercantil7']").val();
                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf7']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf7']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof7']").val();
                        //item ["datos_tipo_patrimoniofi"]=$(this).find("input:radio[class*='datos_tipo_patrimoniofi']:checked").val();
                        if($("#datos_tipo_patrimoniofi1_0").is(":checked")==true)
                          item ["datos_tipo_patrimoniofi"]=1;
                        else if($("#datos_tipo_patrimoniofi2_0").is(":checked")==true)
                          item ["datos_tipo_patrimoniofi"]=2;
                        else if($("#datos_tipo_patrimoniofi3_0").is(":checked")==true)
                          item ["datos_tipo_patrimoniofi"]=3;
                        item ["monedafi"]=$(this).find("select[class*='tipo_moneda_a7']").val();
                        item ["monto_operacionfi"]=$(this).find("input[id*='monto_operacionfi']").val();
                        item ["tipo_inmueblefi"]=$(this).find("select[id*='tipo_inmueble_a']").val();
                        item ["codigo_postalfi"]=$(this).find("input[id*='codigo_postalfi']").val();
                        item ["folio_realfi"]=$(this).find("input[id*='folio_realfi']").val();
                        item ["importe_garantiafi"]=$(this).find("input[id*='importe_garantiafi']").val();
                        item ["descripcionfi"]=$(this).find("input[id*='descripcionfi']").val();
                        item ["valor_bienfi"]=$(this).find("input[id*='valor_bienfi']").val();
                        DATAPACC7.push(item);
                  });
                  INFOPACC7  = new FormData();
                  aInfopacc7   = JSON.stringify(DATAPACC7);
                  INFOPACC7.append('data', aInfopacc7);
                  $.ajax({
                      data: INFOPACC7,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12a_datos_fideicomitente',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //Tipo actividad 7  Datos_fideicomisarios
                  var DATAPACC8= [];
                  var TABLAPACC8 = $(".row_datos_fideicomisarios .datos_fideicomisarios > div");                  
                  TABLAPACC8.each(function(){         
                        item = {};
                        item ["idanexo12a"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id8']").val();
                        item ["datos_fideicomisarios_determinados"]=$(this).find("input:radio[class*='datos_fideicomisarios_determinados8']:checked").val();
                        item ["tipo_movimiento_fideicomitente"]=$(this).find("select[id*='tipo_movimiento_fideicomitente8']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona8']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre8']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno8']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno8']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento8']").val();
                        item ["rfc"]=$(this).find("input[id*='rfc8']").val();
                        item ["curp"]=$(this).find("input[id*='curp8']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad8']").val();
                        item ["actividad_economica"]=$(this).find("select[id*='actividad_economica8']").val();
                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm8']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm8']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm8']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm8']").val();
                        item ["giro_mercantil"]=$(this).find("select[id*='giro_mercantil8']").val();
                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf8']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf8']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof8']").val();
                        DATAPACC8.push(item);
                  });
                  INFOPACC8  = new FormData();
                  aInfopacc8   = JSON.stringify(DATAPACC8);
                  INFOPACC8.append('data', aInfopacc8);
                  $.ajax({
                      data: INFOPACC8,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo12a_datos_fideicomisarios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                //
                }else if(act_num_aux==6){
                  //=================== 
                  //==========================================================================
                  // por bien 
                    var DATAP= [];
                    var TABLAP = $(".row_liquidacion .liquidacion > div");                  
                    TABLAP.each(function(){         
                          item = {};
                          item ["idanexo12a"]=id;
                          item ["aviso"]=$("#aviso").val();
                          item ["id"]=$(this).find("input[id*='id_a']").val();
                          item ["fecha_pago"]=$(this).find("input[id*='fecha_pago_a']").val();
                          item ["instrumento_monetario"]=$(this).find("select[id*='instrum_notario_a']").val();
                          item ["tipo_moneda"]=$(this).find("select[id*='tipo_moneda_a']").val();
                          montop = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
                          montop = replaceAll(montop, "$", "" );
                          item ["monto_operacion"]=montop;
                          DATAP.push(item);
                    });
                    INFOP  = new FormData();
                    aInfop   = JSON.stringify(DATAP);
                    INFOP.append('data', aInfop);
                    $.ajax({
                        data: INFOP,
                        type: 'POST',
                        url : base_url+'index.php/Transaccion/inset_pago_anexo12a',
                        processData: false, 
                        contentType: false,
                        async: false,
                        statusCode:{
                            404: function(data2){
                                //toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                //toastr.error('Error', '500');
                            }
                        },
                        success: function(data2){
                          
                        }
                    }); 
                }
                $("input[name*='id_aviso']").val(id);
                //=================== 
                //esperar a que guarde socios y demas tablas del anexo
                setTimeout(function () {
                  if(act_num_aux==1 || act_num_aux==9){ //siempre se acusan y avisan
                    // modificar operacion como acusada
                    band_umbral=true;
                    $.ajax({ //cambiar pago a acusado
                      type: 'POST',
                      url : base_url+'Operaciones/acusaPago2',
                      async: false,
                      data: { id:id, act:$("#id_actividad").val()},
                      success: function(result_divi){

                      }
                    });
                    setTimeout(function () { 
                      if($("#aviso").val()>0){
                        window.location.href = base_url+"Transaccion/anexo12a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/0";
                      }else{
                        window.location.href = base_url+"Transaccion/anexo12a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/0";
                      } 
                      swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                      if($("#aviso").val()>0){
                        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                      }else{
                        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                      } 
                    }, 1500); //esperar al acuse
                  }else if(act_num_aux==2 || act_num_aux==3 || act_num_aux==4 || act_num_aux==5 || act_num_aux==6 || act_num_aux==7 || act_num_aux==8 || act_num_aux==10){
                    var monto_act=0;
                    var addtp=0; var addtpD=0;
                    var umas_anexo=0;
                    //var valor_uma=0;

                    /*$.ajax({
                      type: 'POST',
                      url : base_url+'Umas/getUmasAnio',
                      async: false,
                      data: { anio: $("#anio option:selected").val()},
                      success: function(data2){
                        valor_uma=parseFloat(data2).toFixed(2);
                        valor_uma = valor_uma*1;
                      }
                    });*/

                    /////////////////////////////////////////////////////
                    //setTimeout(function () { 
                      if(act_num_aux==2){
                        var capital_fijoco=$('#capital_fijoco').val(); 
                        var capital_variableco=$('#capital_variableco').val(); 
                        var cf=parseFloat(capital_fijoco);
                        var cv=parseFloat(capital_variableco);
                        suma_act=parseFloat(cf+cv);
                        //umas_anexo=suma_act;
                        console.log("suma actividad en act2: "+suma_act);
                      }else if(act_num_aux==3){ //Modificacion patrimonial por aumento de capital o por disminucion de capital
                        var final_capital_variablemo = $('#final_capital_variablemo').val(); 
                        var final_capital_fijomo= $("input[name*='final_capital_fijomo']").val(); 
                        var fc=parseFloat(final_capital_variablemo);
                        var fcf=parseFloat(final_capital_fijomo);

                        var inicial_capital_variablemo = $("input[name*='inicial_capital_variablemo']").val();  
                        var inicial_capital_fijomo= $("input[name*='inicial_capital_fijomo']").val(); 
                        var icv=parseFloat(inicial_capital_variablemo);
                        var ivf=parseFloat(inicial_capital_fijomo);

                        if($("select[name*='tipo_modificacion_capital_fijomo'] option:selected").val()==1){ //aumento
                          suma1=parseFloat(fcf-ivf);
                          suma2=parseFloat(fc-icv);
                          if(Math.sign(suma1)==-1){
                            var nega = (-1);
                            suma1 = suma1 * nega;
                          }
                          if(Math.sign(suma2)==-1){
                            var nega = (-1);
                            suma2 = suma2 * nega;
                          }
                          console.log("suma1: "+suma1);
                          console.log("suma2: "+suma2);
                          opera_comp=suma1+suma2;
                        }else if($("select[name*='tipo_modificacion_capital_fijomo'] option:selected").val()==2){ //disminucion
                          resta1=parseFloat(fcf-ivf);
                          resta2=parseFloat(fc-icv);
                          if(Math.sign(resta1)==-1){
                            var nega = (-1);
                            resta1 = resta1 * nega;
                          }
                          if(Math.sign(resta2)==-1){
                            var nega = (-1);
                            resta2 = resta2 * nega;
                          }
                          console.log("resta1: "+resta1);
                          console.log("resta2: "+resta2);
                          opera_comp=resta1+resta2;
                        }
                        opera_comp = opera_comp *1;
                        console.log("opera_comp: "+opera_comp);
                        //suma_act=parseFloat(fc+fcf);
                        suma_act=parseFloat(opera_comp);
                        console.log("suma actividad en act3: "+suma_act);
                        //umas_anexo=suma_act;
                      }else if(act_num_aux==4){ //fusion
                        var csf=0; var csv=0; 
                        var capital_fijoco=$('#capital_social_fijofu').val(); 
                        var capital_variableco=$('#capital_social_variablefu').val(); 
                        var cf=parseFloat(capital_fijoco);
                        var cv=parseFloat(capital_variableco);

                        if($("input[name*='fusionante_determinadasfu']").is(":checked")==true){
                          var capital_social_fijoffu=$("input[name*='capital_social_fijoffu']").val();
                          var capital_social_variableffu=$("input[name*='capital_social_variableffu']").val();
                          csf=parseFloat(capital_social_fijoffu);
                          csv=parseFloat(capital_social_variableffu);
                        }

                        suma1=parseFloat(cf+cv);
                        suma2=parseFloat(csf+csv);
                        if(Math.sign(suma1)==-1){
                          var nega = (-1);
                          suma1 = suma1 * nega;
                        }
                        if(Math.sign(suma2)==-1){
                          var nega = (-1);
                          suma2 = suma2 * nega;
                        }
                        opera_comp=suma1+suma2;

                        //suma_act=parseFloat(cf+cv);
                        suma_act=parseFloat(opera_comp);
                        console.log("suma actividad en act4: "+suma_act);
                        //umas_anexo=suma_act;
                      }else if(act_num_aux==5){ //escicion
                        var capital_social_fijoso=$("input[name*='capital_social_fijoe']").val();
                        var capital_social_variablee=$("input[name*='capital_social_variablee']").val();
                        csfe=parseFloat(capital_social_fijoso);
                        csve=parseFloat(capital_social_variablee);

                        var cf=0;
                        var cv=0;
                        if($("input[name*='escindidas_determinadase']").is(":checked")==true) {
                          var capital_fijome=$('#capital_social_fijome').val(); 
                          var capital_variableme=$('#capital_social_variableme').val(); 
                          cf=parseFloat(capital_fijome);
                          cv=parseFloat(capital_variableme); 
                        }
                        
                        suma1=parseFloat(cf+cv);
                        suma2=parseFloat(csfe+csve);
                        if(Math.sign(suma1)==-1){
                          var nega = (-1);
                          suma1 = suma1 * nega;
                        }
                        if(Math.sign(suma2)==-1){
                          var nega = (-1);
                          suma2 = suma2 * nega;
                        }
                        
                        opera_comp=suma1+suma2;

                        //suma_act=parseFloat(cf+cv);
                        suma_act=parseFloat(opera_comp);
                        console.log("suma actividad en act5: "+suma_act);
                        //umas_anexo=suma_act;
                      }else if(act_num_aux==6){
                        umas_anexo=0; umas_anexoD=0; addtpF=0;
                        addtp=0;
                        addtpD=0;
                        /*monto_act=monto;
                        if(monto_act>=697212){
                            if(validar_xml==1){
                              suma_act=monto_act;
                            }else{
                              suma_act=0;
                            }
                        }else{
                          suma_act=0;
                        }*/
                        var TABLA = $(".row_liquidacion .liquidacion > div");              
                        TABLA.each(function(){        
                          if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
                            console.log("pesos de Liquidación ");
                            var monto_operacion = $(this).find("input[id*='monto_a']").val();
                            var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
                            if(monto_operacion.indexOf('$') != -1){
                              tot_mo = replaceAll(monto_operacion, ",", "" );
                              tot_mo = replaceAll(tot_mo, "$", "" );
                              console.log("valor1 de monto_operacion: "+monto_operacion);
                            }else{
                              tot_mo=monto_operacion;
                            }
                            //tot_mo = Number(tot_mo);
                            tot_mo = parseFloat(tot_mo);
                            var vstotal1 = tot_mo;
                            console.log("valor1 de vstotal1: "+vstotal1);
                            
                            addtp += Number(vstotal1);
                            //addtp += Number(vstotal1);
                            console.log("valor1 de addtp: "+addtp);
                            //if(addtp>0){
                              $.ajax({
                                type: 'POST',
                                url : base_url+'Umas/getUmasAnio',
                                data: { anio: fecha_pago},
                                async: false,
                                success: function(data2){
                                  valor_uma=data2;
                                  valor_uma = parseFloat(valor_uma);
                                  console.log("valor uma en function umasAnio: "+valor_uma);
                                  umas_anexo = addtp/valor_uma;
                                  //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                  console.log("valor1 de umas_anexo: "+umas_anexo);
                                }
                              });
                            //}
                          }
                          if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){
                            //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
                            console.log("moneda != pesos de Liquidación");
                            var monto_operacion = $(this).find("input[id*='monto_a']").val();
                            var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
                            $.ajax({
                              type: 'POST',
                              url : base_url+'Divisas/getDivisaTipo',
                              async: false,
                              data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(), fecha:$(this).find("input[id*='fecha_pago_a']").val()},
                              success: function(result_divi){
                                valor_divi=result_divi;
                                valor_divi = parseFloat(valor_divi);
                                //$(this).find(".monto_divisa").html('Costo de divisa: $'+valor_divi);
                                //console.log("monto operacion: "+monto_operacion);
                                valor_divi = parseFloat(valor_divi);
                                console.log("valor_divi: "+valor_divi);
                                if(monto_operacion.indexOf('$') != -1){
                                  tot_moD = replaceAll(monto_operacion, ",", "" );
                                  tot_moD = replaceAll(tot_moD, "$", "" );
                                }else{
                                  tot_moD = monto_operacion;
                                }
                                tot_moD = parseFloat(tot_moD);
                                console.log("valor2 de tot_moD: "+tot_moD);
                                var vstotal1D = tot_moD*valor_divi;
                                //console.log("valor2 de vstotal1D: "+vstotal1D);
                                addtpD += Number(vstotal1D);
                                
                                //addtpD += Number(vstotal1D);
                                console.log("valor2 de addtpD: "+addtpD);
                                //if(addtpD>0){ 
                                  $.ajax({
                                    type: 'POST',
                                    url : base_url+'Umas/getUmasAnio',
                                    data: { anio: fecha_pago},
                                    async: false,
                                    success: function(data2){
                                      valor_uma=data2;
                                      valor_uma = parseFloat(valor_uma);
                                      console.log("valor uma en function umasAnio: "+valor_uma);
                                      umas_anexoD = addtpD/valor_uma;
                                      //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                                      console.log("valor2 de umas_anexoD: "+umas_anexoD);
                                    }
                                  });
                                //}
                              }
                            });
                          }
                        });
                        //setTimeout(function () { 
                          umas_anexo = parseFloat(umas_anexo).toFixed(2);
                          umas_anexoD = parseFloat(umas_anexoD).toFixed(2);
                          addtpF = addtp + addtpD;
                          umas_anexo = umas_anexo*1;
                          umas_anexoD = umas_anexoD*1;
                          suma_act=addtpF;

                          umas_anexoAct6 = umas_anexo + umas_anexoD;
                        //}, 500);
                        console.log("suma actividad en act6: "+suma_act);
                      }else if(act_num_aux==7){
                        if($("#datos_tipo_patrimoniofi1_0").is(":checked")==true){
                          //console.log("tipo_moneda_a7 : "+$(".tipo_moneda_a7 option:selected").val());
                          if($(".tipo_moneda_a7 option:selected").val()==0){ //pesos
                            var monto_operacionfi = $('#monto_operacionfi').val(); 
                            var fc=parseFloat(monto_operacionfi); 
                            suma_act=fc;
                          }
                          else if($(".tipo_moneda_a7 option:selected").val()!=0){
                            $.ajax({
                              type: 'POST',
                              url : base_url+'Divisas/getDivisaTipo',
                              async: false,
                              data: { tipo: $(".tipo_moneda_a7 option:selected").val(), fecha:$("#fecha_operacion").val()},
                              success: function(result_divi){
                                valor_divi=result_divi;
                                valor_divi = parseFloat(valor_divi);
                                console.log("valor_divi: "+valor_divi);
                                /*if(monto_operacionfi.indexOf('$') != -1){
                                  tot_moD = replaceAll(monto_operacionfi, ",", "" );
                                  tot_moD = replaceAll(tot_moD, "$", "" );
                                }else{
                                  tot_moD = monto_operacionfi;
                                }*/
                                tot_moD = $('#monto_operacionfi').val();
                                tot_moD = parseFloat(tot_moD);
                                console.log("valor2 de tot_moD: "+tot_moD);
                                var vstotal1D = tot_moD*valor_divi;
                                //console.log("valor2 de vstotal1D: "+vstotal1D);
                                addtpD += Number(vstotal1D);
                                
                                //addtpD += Number(vstotal1D);
                                console.log("valor2 de addtpD: "+addtpD);
                                var fc=parseFloat(addtpD); 
                                suma_act=fc;
                              }
                            });
                          }
                        }else if($("#datos_tipo_patrimoniofi2_0").is(":checked")==true){
                          var importe_garantiafi = $('#importe_garantiafi').val(); 
                          var fc=parseFloat(importe_garantiafi);
                          suma_act=fc;
                        }else if($("#datos_tipo_patrimoniofi3_0").is(":checked")==true){
                          var valor_bienfi = $('#valor_bienfi').val(); 
                          var fc=parseFloat(valor_bienfi);  
                          suma_act=fc;
                        }
                        
                        console.log("suma actividad en act7: "+suma_act);
                        //umas_anexo=suma_act;
                      }else if(act_num_aux==8){
                        var monto_cesionces = $('#monto_cesionces').val(); 
                        var fc=parseFloat(monto_cesionces);
                        suma_act=fc;
                        //umas_anexo=suma_act;
                        console.log("suma actividad en act8: "+suma_act);
                      }else if(act_num_aux==10){
                        var valor_avaluorea = $('#valor_avaluorea').val(); 
                        var fc=parseFloat(valor_avaluorea);
                        suma_act=fc;
                        //umas_anexo=suma_act;
                      }
                      /*if(monto==suma_act){
                        monto_act=suma_act;
                      }else{
                        monto_act=0;
                      }*/
                      if(Math.sign(suma_act)==-1){
                        var nega = (-1);
                        //umas_anexo = umas_anexo * nega;
                        //console.log("suma actividad negativo: "+suma_act);
                        suma_act = suma_act * nega;
                        //console.log("suma actividad a positivo: "+suma_act);
                      }
                      suma_act = parseFloat(suma_act).toFixed(2);
                      //console.log("suma_act: "+suma_act);
                      //console.log("valor de valor_uma: "+valor_uma);

                      if(act_num_aux!=6){
                        umas_anexo= parseFloat(suma_act/valor_uma);
                      }else{
                        umas_anexo = umas_anexoAct6;
                      }
                      console.log("valor de umas_anexo de operacion ejecutada: "+umas_anexo);
                    //}, 1500); //esperar a la sumatoria local
                  }
                }, 1000); //esperar el guardado de socios y demas tablas 
                ////////////////////////////////////////////////////
                if($("#tipo_actividad option:selected").val()==1) //
                  act=1;
                else if($("#tipo_actividad option:selected").val()==2 || $("#tipo_actividad option:selected").val()==3 || $("#tipo_actividad option:selected").val()==4 || $("#tipo_actividad option:selected").val()==5 || $("#tipo_actividad option:selected").val()==6)
                  act=2;
                else if($("#tipo_actividad option:selected").val()==7 || $("#tipo_actividad option:selected").val()==8)
                  act=3;
                else if($("#tipo_actividad option:selected").val()==9)
                  act=4;
                else if($("#tipo_actividad option:selected").val()==10)
                  act=5;

                var anio = $("#anio").val();
                //var addtpHF=0; var umas_anexoH=0; 
                var umas_anexoTot_historico=0;
                
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umbrales/getUmbralActividad',
                  data: {anexo: $("#id_actividad").val(), act:act},
                  async:false,
                  success: function(result){
                    //var array = $.parseJSON(result);
                    var array= JSON.parse(result);
                    var aviso = parseFloat(array[0].aviso).toFixed(2);
                    var siempre_avi=array[0].siempre_avi;
                    var siempre_iden=array[0].siempre_iden;
                    var identifica=array[0].identificacion;

                    monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
                    monto_opera = replaceAll(monto_oper, "$", "" );
                    monto_opera = parseFloat(monto_opera).toFixed(2);
                    setTimeout(function () { 
                      //console.log("aviso: "+aviso);
                      //console.log("valor_uma: "+valor_uma);
                      //console.log("monto: "+monto_opera);
                      console.log("suma de actividad en umbrales actividad: "+suma_act);
                      if(siempre_avi=="1"){
                        band_umbral=true;
                        if($("#tipo_actividad option:selected").val()!="6"){ //acusa e pago dentro de 12a
                          $.ajax({ //cambiar pago a acusado
                            type: 'POST',
                            url : base_url+'Operaciones/acusaPago2',
                            async: false,
                            data: { id:id, act:$("#id_actividad").val()},
                            success: function(result_divi){

                            }
                          });
                        }else{
                          /*if($("#tipo_actividad option:selected").val()!="6"){ //acusa e pago dentro de 12a
                            $.ajax({ //cambiar pago a acusado - las liquidaciones de la tabla pagos 12a
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPago2',
                              async: false,
                              data: { id:id, act:$("#id_actividad").val()},
                              success: function(result_divi){

                              }
                            });
                          }else{*/
                            $.ajax({ //cambiar pago a acusado - las liquidaciones de la tabla pagos 12a
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPago2Liquida',
                              async: false,
                              data: { id:id, act:$("#id_actividad").val()},
                              success: function(result_divi){

                              }
                            });
                          //}
                        }
                      }else{
                        //umas_anexo=monto_opera/valor_uma;
                        if(act_num_aux==6){
                          umas_anexo=umas_anexoAct6;
                        }
                        else{
                          umas_anexo=suma_act/valor_uma;
                        }

                        umas_anexo = parseFloat(umas_anexo).toFixed(2);
                        //console.log("valor de uma: "+valor_uma);
                        //console.log("valor de umas_anexo final: "+umas_anexo);
                        console.log("valor de umas aviso: "+aviso);
                        //console.log("siempre_avi: "+siempre_avi);
                        umas_anexo = umas_anexo*1;
                        aviso = aviso*1;
                        if(umas_anexo>=aviso){
                          band_umbral=true;
                          if($("#tipo_actividad option:selected").val()!="6"){ //acusa e pago dentro de 12a
                            $.ajax({ //cambiar pago a acusado
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPago2',
                              async: false,
                              data: { id:id, act:$("#id_actividad").val()},
                              success: function(result_divi){

                              }
                            });
                          }else{
                            $.ajax({ //cambiar pago a acusado - las liquidaciones de la tabla pagos 12a
                              type: 'POST',
                              url : base_url+'Operaciones/acusaPago2Liquida',
                              async: false,
                              data: { id:id, act:$("#id_actividad").val()},
                              success: function(result_divi){

                              }
                            });
                          }
                        }
                      }
                      id_acusado=0;
                      tipo_act = $("#tipo_actividad option:selected").val();
                      /*console.log("umas_anexo: "+umas_anexo);
                      console.log("aviso: "+aviso);
                      console.log("tipo_act: "+tipo_act);*/
                      if(umas_anexo<aviso && tipo_act!=1 || umas_anexo<aviso && tipo_act!=9){
                        //*se debe crear una nueva ocnsulta de getOpera, ya ques es por anexo y por actividad interna
                        //console.log("If para buscar historico de mismo anexo misma act");
                        $.ajax({
                          type: 'POST',
                          url : base_url+'Operaciones/getOperaActividad',
                          async: false,
                          data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id, tipo:$("#tipo_actividad option:selected").val(), id_perfilamiento: $("#id_perfilamiento").val() },
                          success: function(result){
                            var data= $.parseJSON(result);
                            var datos = data.data;
                            var opera_comph=0; var cont_hist=0; var band_comple=0; var id_ant=0;
                            var id_element = [];
                            datos.forEach(function(element) {
                              var addtpHF=0; var addtpHDF=0; var umas_anexoH=0;
                              var umas_anexoHD=0;
                              total_regs = element.total_regs;

                              //console.log("element.tipo_actividad: "+element.tipo_actividad);
                              //console.log("element.id: "+element.id);
                              if(element.tipo_actividad==2){
                                var capital_fijocoh=element.capital_fijoco; 
                                var capital_variablecoh=element.capital_variableco; 
                                var cfh=parseFloat(capital_fijocoh);
                                var cvh=parseFloat(capital_variablecoh);
                                suma_acth=parseFloat(cfh+cvh);
                                if(Math.sign(suma_acth)==-1){
                                  var nega = (-1);
                                  suma_acth = suma_acth * nega;
                                }
                                //umas_anexoH+=suma_acth;
                                //umas_anexoH += parseFloat(suma_acth/valor_uma);
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica || siempre_iden=="1"){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma);
                                }
                                //console.log("umas_anexo de tipo actividad: "+umas_anexo);
                              }else if(element.tipo_actividad==3){
                                var final_capital_variablemoh = element.final_capital_variablemo; 
                                var final_capital_fijomoh= element.final_capital_fijomo; 
                                var fch=parseFloat(final_capital_variablemoh);
                                var fcfh=parseFloat(final_capital_fijomoh);

                                var inicial_capital_variablemoh = element.inicial_capital_variablemo;  
                                var inicial_capital_fijomoh = element.inicial_capital_fijomo; 
                                var icvh=parseFloat(inicial_capital_variablemoh);
                                var ivfh=parseFloat(inicial_capital_fijomoh);
                                /*suma1h=parseFloat(fch+fcfh);
                                suma2h=parseFloat(icvh+ivfh);
                                opera_comph=suma1h-suma2h;*/

                                //console.log("id: "+element.id);
                                /*console.log("fch: "+fch);
                                console.log("fcfh: "+fcfh);
                                console.log("icvh: "+icvh);
                                console.log("ivfh: "+ivfh);*/

                                if(element.tipo_modificacion_capital_fijomo==1){
                                  suma1h=parseFloat(fcfh-ivfh);
                                  suma2h=parseFloat(fch-icvh);
                                  if(Math.sign(suma1h)==-1){
                                    var nega = (-1);
                                    suma1h = suma1h * nega;
                                  }
                                  if(Math.sign(suma2h)==-1){
                                    var nega = (-1);
                                    suma2h = suma2h * nega;
                                  }
                                  //console.log("suma1h: "+suma1h);
                                  //console.log("suma2h: "+suma2h);
                                  opera_comph=suma1h+suma2h;
                                }else if(element.tipo_modificacion_capital_fijomo==2){
                                  resta1h=parseFloat(fcfh-ivfh);
                                  resta2h=parseFloat(fch-icvh);
                                  if(Math.sign(resta1h)==-1){
                                    var nega = (-1);
                                    resta1h = resta1h * nega;
                                  }
                                  if(Math.sign(resta2h)==-1){
                                    var nega = (-1);
                                    resta2h = resta2h * nega;
                                  }
                                  //console.log("resta1h: "+resta1h);
                                  //console.log("resta2h: "+resta2h);
                                  opera_comph=resta1h+resta2h;
                                } 
                                //console.log("opera_comph: "+opera_comph);
                                //suma_act=parseFloat(fc+fcf);
                                opera_comph = opera_comph *1;
                                suma_acth=parseFloat(opera_comph);
                                if(Math.sign(suma_acth)==-1){
                                  var nega = (-1);
                                  suma_acth = suma_acth * nega;
                                }
                                console.log("suma_acth: "+suma_acth);
                                //umas_anexoH=suma_acth;
                                //umas_anexoH += parseFloat(suma_acth/valor_uma).toFixed(2);
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica || siempre_iden=="1"){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma).toFixed(2);
                                }
                              }else if(element.tipo_actividad==4){ //fusion
                                var capital_fijocoh=element.capital_social_fijofu; 
                                var capital_variablecoh=element.capital_social_variablefu; 
                                var cfh=parseFloat(capital_fijocoh);
                                var cvh=parseFloat(capital_variablecoh);

                                var capital_social_fijoffuh=element.capital_social_fijoffu;
                                var capital_social_variableffuh=element.capital_social_variableffu;
                                var csfh=parseFloat(capital_social_fijoffuh);
                                var csvh=parseFloat(capital_social_variableffuh);
                                suma1h=parseFloat(cfh+cvh);
                                suma2h=parseFloat(csfh+csvh);
                                if(Math.sign(suma1h)==-1){
                                  var nega = (-1);
                                  suma1h = suma1h * nega;
                                }
                                if(Math.sign(suma2h)==-1){
                                  var nega = (-1);
                                  suma2h = suma2h * nega;
                                }
                                
                                opera_comph=suma1h+suma2h;
                                suma_acth=parseFloat(opera_comph);
                                //umas_anexoH+=suma_acth;
                                if(Math.sign(suma_acth)==-1){
                                  var nega = (-1);
                                  suma_acth = suma_acth * nega;
                                }
                                //umas_anexoH += parseFloat(suma_acth/valor_uma);
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica || siempre_iden=="1"){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma);
                                }
                              }else if(element.tipo_actividad==5){
                                var capital_fijocoh=element.capital_social_fijome; 
                                var capital_variablecoh=element.capital_social_variableme; 
                                var cfh=parseFloat(capital_fijocoh);
                                var cvh=parseFloat(capital_variablecoh);

                                var capital_social_fijoe=element.capital_social_fijoe;
                                var capital_social_variablee=element.capital_social_variablee;
                                var csfeh=parseFloat(capital_social_fijoe);
                                var csveh=parseFloat(capital_social_variablee);

                                suma1h=parseFloat(cfh+cvh);
                                suma2h=parseFloat(csfeh+csveh);
                                if(Math.sign(suma1h)==-1){
                                  var nega = (-1);
                                  suma1h = suma1h * nega;
                                }
                                if(Math.sign(suma2h)==-1){
                                  var nega = (-1);
                                  suma2h = suma2h * nega;
                                }
                                opera_comph=suma1h+suma2h;
                                suma_acth=parseFloat(opera_comph);
                                //umas_anexoH=suma_acth;
                                if(Math.sign(suma_acth)==-1){
                                  var nega = (-1);
                                  suma_acth = suma_acth * nega;
                                }
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica || siempre_iden=="1"){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma);
                                }
                              }else if(element.tipo_actividad==6){
                                //umas_anexoH=0; addtpHF=0;
                                //addtp=0;
                                //addtpD=0;
                                $.ajax({
                                  type: 'POST',
                                  url : base_url+'Operaciones/getPagosA12a',
                                  async: false,
                                  data: { id:element.id },
                                  success: function(result_pagos){
                                    var data_pagos= $.parseJSON(result_pagos);
                                    var datospa6 = data_pagos.data;
                                    datospa6.forEach(function(elementpa6) {
                                      if(elementpa6.tipo_moneda!=0){ //divisa

                                        $.ajax({
                                          type: 'POST',
                                          url : base_url+'Divisas/getDivisaTipo',
                                          async: false,
                                          data: { tipo: elementpa6.tipo_moneda, fecha: elementpa6.fecha_pago },
                                          success: function(result_divi){
                                            valor_divi=parseFloat(result_divi);
                                            tot_moDH = parseFloat(elementpa6.monto_operacion);
                                            //console.log("valor2 de tot_moD: "+tot_moD);
                                            var totalHD = tot_moDH*valor_divi;
                                            totalHD = totalHD.toFixed(2);
                                            addtpHDF += Number(totalHD);

                                            $.ajax({
                                              type: 'POST',
                                              url : base_url+'Umas/getUmasAnio',
                                              async: false,
                                              data: { anio: elementpa6.fecha_pago},
                                              success: function(data2){
                                                valor_uma=parseFloat(data2).toFixed(2);
                                                //console.log("valor de uma en historico: "+valor_uma);
                                                //umas_anexoH += parseFloat(addtpHDF/valor_uma);
                                                precalc= addtpHDF/valor_uma;
                                                if(precalc>=identifica || siempre_iden=="1"){
                                                  umas_anexoH = parseFloat(addtpHDF/valor_uma);
                                                }
                                                console.log("valor de umas_anexoH: "+umas_anexoH);
                                              }
                                            });

                                          }
                                        });
                                      }//if de divisa
                                      else{
                                        tot_moH = parseFloat(elementpa6.monto_operacion);
                                        totalH = tot_moH.toFixed(2);
                                        addtpHF += Number(totalH); 

                                        $.ajax({
                                          type: 'POST',
                                          url : base_url+'Umas/getUmasAnio',
                                          async: false,
                                          data: { anio: elementpa6.fecha_pago},
                                          success: function(data2){
                                            valor_uma=parseFloat(data2).toFixed(2);
                                            //console.log("valor de uma en historico: "+valor_uma);
                                            //umas_anexoH += parseFloat(addtpHF/valor_uma);
                                            precalc= addtpHF/valor_uma;
                                            if(precalc>=identifica || siempre_iden=="1"){
                                              umas_anexoHD = parseFloat(addtpHF/valor_uma);
                                            }
                                            console.log("valor de umas_anexoHD: "+umas_anexoHD);
                                          }
                                        });
                                      } //else de pagos en pesos
                                      umas_anexoH=umas_anexoH + umas_anexoHD;
                                      console.log("valor de umas_anexoH final: "+umas_anexoH);
                                      console.log("valor de addtpHDF: "+addtpHDF);
                                      console.log("valor de addtpHF: "+addtpHF);
                                      console.log("valor de valor_uma: "+valor_uma);
                                    });
                                  }
                                });
                              }else if(element.tipo_actividad==7){
                                if(element.datos_tipo_patrimoniofi==1){
                                  //var monto_operacionfih = element.monto_operacionfi; 
                                  //var fch=parseFloat(monto_operacionfih);  
                                  console.log("element.tipo_moneda_a: "+element.monedafi);
                                  if(element.monedafi==0){
                                    var monto_operacionfih = element.monto_operacionfi; 
                                    var fch=parseFloat(monto_operacionfih); 
                                    suma_acth=fch;
                                  }
                                  else if(element.monedafi!=0){
                                    $.ajax({
                                      type: 'POST',
                                      url : base_url+'Divisas/getDivisaTipo',
                                      async: false,
                                      data: { tipo: element.monedafi, fecha:element.fecha_operacion},
                                      success: function(result_divi){
                                        valor_divi=result_divi;
                                        valor_divi = parseFloat(valor_divi);
                                        console.log("valor_divi: "+valor_divi);
                                        if(element.monto_operacionfi.indexOf('$') != -1){
                                          tot_moDH = replaceAll(element.monto_operacionfi, ",", "" );
                                          tot_moDH = replaceAll(tot_moDH, "$", "" );
                                        }else{
                                          tot_moDH = element.monto_operacionfi;
                                        }
                                        tot_moDH = parseFloat(tot_moDH);
                                        console.log("valor2 de tot_moD: "+tot_moDH);
                                        var vstotal1DH = tot_moDH*valor_divi;
                                        //console.log("valor2 de vstotal1D: "+vstotal1D);
                                        addtpHDF += Number(vstotal1DH);
                                        
                                        //addtpD += Number(vstotal1D);
                                        console.log("valor2 de addtpHDF: "+addtpHDF);
                                        var fch=parseFloat(addtpHDF); 
                                        suma_acth=fch;
                                      }
                                    });
                                  }
                                }else if(element.datos_tipo_patrimoniofi==2){
                                  var importe_garantiafih = element.importe_garantiafi; 
                                  var fch=parseFloat(importe_garantiafih);
                                  suma_acth=fch;
                                }else if(element.datos_tipo_patrimoniofi==3){
                                  var valor_bienfih = element.valor_bienfi; 
                                  var fch=parseFloat(valor_bienfih);  
                                  suma_acth=fch;
                                }
                                
                                //umas_anexoH=suma_acth; 
                                console.log("suma_acth: "+suma_acth);
                                if(Math.sign(suma_acth)==-1){
                                  var nega = (-1);
                                  suma_acth = suma_acth * nega;
                                }
                                //umas_anexoH += parseFloat(suma_acth/valor_uma);
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica || siempre_iden=="1"){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma);
                                }
                              }else if(element.tipo_actividad==8){
                                var monto_cesioncesH = element.monto_cesionces; 
                                var fch=parseFloat(monto_cesioncesH);
                                suma_acth=fch;
                                //umas_anexoH=suma_acth;
                                if(Math.sign(suma_acth)==-1){
                                  var nega = (-1);
                                  suma_acth = suma_acth * nega;
                                }
                                //umas_anexoH += parseFloat(suma_acth/valor_uma);
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica || siempre_iden=="1"){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma);
                                }
                              }else if(element.tipo_actividad==10){
                                var valor_avaluoreaH = element.valor_avaluorea; 
                                console.log("valor_avaluoreaH: "+valor_avaluoreaH);
                                var fch=parseFloat(valor_avaluoreaH);
                                suma_acth=fch;
                                //umas_anexoH=suma_acth;
                                if(Math.sign(suma_acth)==-1){
                                  var nega = (-1);
                                  suma_acth = suma_acth * nega;
                                }
                                //console.log("valor de suma_acth: "+suma_acth);
                                //umas_anexoH += parseFloat(suma_acth/valor_uma);
                                precalc= suma_acth/valor_uma;
                                if(precalc>=identifica || siempre_iden=="1"){
                                  umas_anexoH += parseFloat(suma_acth/valor_uma);
                                }
                                //console.log("valor de valor_uma: "+valor_uma);
                              }

                              //console.log("valor_uma: "+valor_uma);
                              
                              if(Math.sign(umas_anexoH)==-1){
                                var nega = (-1);
                                umas_anexoH = umas_anexoH * nega;
                                suma_acth = suma_acth * nega;
                              }
                              if(element.tipo_actividad!=6){
                                //umas_anexoH += parseFloat(suma_acth/valor_uma);
                              }
                              //addtpHF=parseFloat(addtpHF).toFixed(2);
                              
                              //umas_anexoH+=parseFloat(umas_anexoH).toFixed(2);
                              umas_anexoH=umas_anexoH*1;
                              umas_anexo = umas_anexo*1;
                              /*console.log("valor de umas_anexo: "+umas_anexo);
                              console.log("valor de umas_anexoH: "+umas_anexoH);
                              console.log("valor de identifica: "+identifica);
                              console.log("element.total_regs: "+element.total_regs);*/
                              
                              item = {}; 
                              item ["id_desa"]=element.id;
                              id_element.push(item);
                              
                              INFO  = new FormData();
                              aInfo = JSON.stringify(id_element);
                              INFO.append('data', aInfo);
                              //INFO.append('id_anexo', id);
                              //INFO.append('aviso', $("#aviso").val());
                              
                              if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){ 
                              //if(identifica<=umas_anexoH && identifica!="" && identifica>0){
                                
                                if(cont_hist<1){
                                  umas_anexoTot_historico = umas_anexo + umas_anexoH;
                                }else{
                                  umas_anexoTot_historico += umas_anexoH;
                                }
                                //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                                //console.log("aviso: "+aviso);

                                /*if(umas_anexoTot_historico<aviso && band_comple==0 || umas_anexoTot_historico>=aviso && band_comple==0){
                                  item ["id"]=element.id;
                                  id_element.push(item);
                                  console.log("guardo ids de historico: ");
                                  console.log("id a guardar : "+element.id);
                                }*/
                                //console.log("element.id: "+element.id);
                              
                                if(umas_anexoTot_historico<aviso && band_comple==0){
                                  id_ant = element.id;
                                }
                                //console.log("band_comple: "+band_comple);
                                if(umas_anexoTot_historico>=aviso /*&& cont_hist==element.total_regs*/ /* || umas_anexoTot_historico<aviso */){ //descomentar lo de abajo
                                  //aInfo = JSON.stringify(id_element);
                                  //console.log("aInfo: "+aInfo);
                                  //console.log("id_ant: "+id_ant);
                                  /*if($("#tipo_actividad option:selected").val()=="6" && band_comple==0){ //acusa pagos de liquidaciones de tabla pagos12a
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPago',
                                      async: false,
                                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1, id_ant:id_ant},
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado: "+id_acusado);
                                      }
                                    });
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoActividad12',
                                      async: false,
                                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1, id_ant:id_ant},
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado: "+id_acusado);
                                      }
                                    });
                                  }else if($("#tipo_actividad option:selected").val()!="6" && band_comple==0){
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoActividad12',
                                      async: false,
                                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1, id_ant:id_ant},
                                      success: function(result2){
                                        id_acusado=result2;
                                        console.log("acusado: "+id_acusado);
                                      }
                                    });
                                  }*/
                                  /*console.log("Info: "+INFO);
                                  console.log("aInfo: "+aInfo);
                                  
                                  console.log("cont_hist: "+cont_hist);
                                  console.log("element.total_regs: "+element.total_regs);*/
                                  if($("#tipo_actividad option:selected").val()=="6" /*&& cont_hist==element.total_regs*/){ //acusa pagos de liquidaciones de tabla pagos12a
                                    
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoArray',
                                      async: false,
                                      data: "data="+aInfo+"&act"+$("#id_actividad").val()+"&id_anexo="+id+"&aviso="+$("#aviso").val()+"&pago_ayuda=1"+"&id_ant="+id_ant,
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado: "+id_acusado);
                                      }
                                    });
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoActividad12Array',
                                      async: false,
                                      data: "data="+aInfo+"&act"+$("#id_actividad").val()+"&id_anexo="+id+"&aviso="+$("#aviso").val()+"&pago_ayuda=1"+"&id_ant="+id_ant,
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado: "+id_acusado);
                                      }
                                    });
                                  }else if($("#tipo_actividad option:selected").val()!="6" /*&& cont_hist==element.total_regs*/){
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoActividad12Array',
                                      async: false,
                                      data: "data="+aInfo+"&act"+$("#id_actividad").val()+"&id_anexo="+id+"&aviso="+$("#aviso").val()+"&pago_ayuda=1"+"&id_ant="+id_ant,
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado: "+id_acusado);
                                      }
                                    });
                                  }
                                  
                                }else if(umas_anexoTot_historico<aviso && element.total_regs>=1){ //debe acusar a todos despues de haber llegado al umbral
                                  /*if($("#tipo_actividad option:selected").val()=="6" && band_comple==0){ //acusa pagos de liquidaciones de tabla pagos12a
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPago',
                                      async: false,
                                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1, id_ant:id_ant},
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado: "+id_acusado);
                                      }
                                    });
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoActividad12',
                                      async: false,
                                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1, id_ant:id_ant},
                                      success: function(result2){
                                        id_acusado=result2;
                                        //console.log("acusado: "+id_acusado);
                                      }
                                    });
                                  }else if($("#tipo_actividad option:selected").val()!="6" && band_comple==0){
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaPagoActividad12',
                                      async: false,
                                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id, aviso:$("#aviso").val(), pago_ayuda:1, id_ant:id_ant},
                                      success: function(result2){
                                        id_acusado=result2;
                                        console.log("acusado: "+id_acusado);
                                      }
                                    });
                                  }*/
                                }
                                cont_hist++;
                                
                              }
                              //console.log("band_comple final: "+band_comple);
                              //console.log("cont_hist: "+cont_hist);
                              console.log("umas_anexoTot_historico ultimo: "+umas_anexoTot_historico);
                              if(umas_anexoTot_historico>=aviso){
                                band_comple=1;
                                band_umbral=true;
                              }else if(umas_anexoTot_historico<aviso && umas_anexoTot_historico!=NaN && cont_hist==element.total_regs){
                                /*if($("#tipo_actividad option:selected").val()=="6"){ //acusa pagos de liquidaciones de tabla pagos12a
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaNoPago',
                                      async: false,
                                      processData: false, 
                                      contentType: false,
                                      data: "data="+aInfo+"&id_anexo="+id+"&aviso="+$("#aviso").val(),
                                      success: function(result2){
                                        id_acusado=result2;
                                        console.log("desacusado: "+id_acusado);
                                      }
                                    });
                                    $.ajax({ //cambiar pago a acusado
                                      type: 'POST',
                                      url : base_url+'Operaciones/acusaNoPagoActividad12',
                                      async: false,
                                      processData: false, 
                                      contentType: false,
                                      data: "data="+aInfo+"&id_anexo="+id+"&aviso="+$("#aviso").val(),
                                      success: function(result2){
                                        id_acusado=result2;
                                        console.log("desacusado: "+id_acusado);
                                      }
                                    });
                                }else if($("#tipo_actividad option:selected").val()!="6"){
                                  $.ajax({ //cambiar pago a acusado
                                    type: 'POST',
                                    url : base_url+'Operaciones/acusaNoPagoActividad12',
                                    data: "data="+aInfo+"&id_anexo="+id+"&aviso="+$("#aviso").val(),
                                    success: function(result2){
                                      id_acusado=result2;
                                      console.log("desacusado: "+id_acusado);
                                    }
                                  });
                                }*/
                              }
                            });//foreach
            
                          }//success de  get opera
                        });

                      }
                      console.log("band_umbral: "+band_umbral);

                      /*if (suma==monto){
                        validar_xml=1;
                      }else{
                        validar_xml=0;
                      }*/
                    }, 3500);  // esperar datos de umbral y calculor de sumatoria y acuses de pago.
                    ///////////////////////////////////////////////////////////
                    //// CREACION DE XML Y REDIRECCIONAMIENTO FINAL ////
                    setTimeout(function () {  
                      if (band_umbral === undefined || band_umbral === NaN || umas_anexo===NaN || umas_anexo===undefined) {
                        console.log("calculo mal realizado");
                      }else{
                        umas_anexo = parseFloat(umas_anexo).toFixed(2);
                        aviso = parseFloat(aviso).toFixed(2);
                        umas_anexoTot_historico = parseFloat(umas_anexoTot_historico).toFixed(2);
                        umas_anexoTot_historico = umas_anexoTot_historico*1;
                        umas_anexo = umas_anexo*1;
                        aInfo = aviso*1;

                        /*console.log("umas_anexo con bandera umbral ! de error: "+umas_anexo);
                        console.log("aviso con bandera umbral ! de error: "+aviso);
                        console.log("band_umbral ! de error: "+band_umbral);*/
                        if(band_umbral==true && umas_anexo>=aviso && !isNaN(umas_anexo) || band_umbral==true && umas_anexoTot_historico>=aviso && !isNaN(umas_anexo)){
                          console.log("a generar xml: ");
                          if($("#aviso").val()>0){
                            window.location.href = base_url+"Transaccion/anexo12a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                          }else{
                            window.location.href = base_url+"Transaccion/anexo12a_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
                          } 
                          swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                          /*if(history.back()!=undefined){
                            setTimeout(function () { history.back() }, 1500);
                          }*/
                          if($("#aviso").val()>0){
                            setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                          }else{
                            setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                          } 
                        }else if(band_umbral==false || band_umbral==false && umas_anexo<aviso || band_umbral==false && umas_anexoTot_historico<aviso){
                          console.log("solo guardar: ");
                          swal("¡Éxito!", "Se han realizado los cambios correctamente", "success");
                          /*if(history.back()!=undefined){
                            setTimeout(function () { history.back() }, 1500);
                          }*/
                          if($("#aviso").val()>0){
                            setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                          }else{
                            setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                          } 
                        } 
                      }  
                    }, 4000); //esperar a la sumatoria
                  }//success  
                });  //ajax    
                  
              }//success  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }  
}

function regresar(){
   //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}

function confirmarPagosMonto(){ //para sumar el efectivo nadamas
  suma_no_efect=0;
  suma_efect=0;
  contta=0;
  valor_divi=0;
  montoe=0;
  montone=0;
  var TABLA = $(".row_liquidacion .liquidacion > div");   
  TABLA.each(function(){
    console.log("revisa pagos");
    // pesos de Liquidación 
    if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){ //en pesos
      console.log("pesos");
      //console.log("instrum_notario_a: "+$(this).find("select[id*='instrum_notario_a'] option:selected").val());
      if($(this).find("select[id*='instrum_notario_a'] option:selected").val()=="1"){ //en efectivo
        var monto = $(this).find("input[id*='monto_a']").val();
        console.log("monto: "+$(this).find("input[id*='monto_a']").val());
        if(monto.indexOf('$') != -1){
          montoe = replaceAll(monto, ",", "" );
          montoe = replaceAll(montoe, "$", "" );
        }else{
          montoe=monto;
        }
        montoe = parseFloat(montoe);
        suma_efect += Number(montoe);

      }  
      else{
        var monto = $(this).find("input[id*='monto_a']").val();
        if(monto.indexOf('$') != -1){
          montone = replaceAll(monto, ",", "" );
          montone = replaceAll(montone, "$", "" );
        }else{
          montone=monto;
        }
        montone = parseFloat(montone);
        suma_no_efect += Number(montone);
      }
      //console.log("suma_nefect pesos: "+suma_efect);
      //console.log("suma_no_efect pesos: "+suma_no_efect);
    }
    if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){ //!= de pesos
      //console.log("!pesos");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      monto_opera = $(this).find("input[id*='monto_a']").val();
      var inst = $(this).find("select[id*='instrum_notario_a'] option:selected").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago_a']").val()},
        success: function(result_divi){
          valor_divi=result_divi;
          valor_divi = parseFloat(valor_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = replaceAll(monto_opera, ",", "" );
              tot_moDE = replaceAll(tot_moDE, "$", "" );
            }else{
              tot_moDE = monto_opera;
            }
            tot_moDE = parseFloat(tot_moDE);
            var preefect = tot_moDE*valor_divi;
            suma_efect += Number(preefect);
          }else{
            if(monto_opera.indexOf('$') != -1){
              montone = replaceAll(monto_opera, ",", "" );
              montone = replaceAll(montone, "$", "" );
            }else{
              montone=monto_opera;
            }
            montone = parseFloat(montone);
            var montone = montone*valor_divi;
            suma_no_efect += Number(montone);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
    }
    contta++;
  });
  
  setTimeout(function () { 
    total = parseFloat(suma_no_efect+suma_efect).toFixed(2);

    /*console.log("suma_no_efect:" +suma_no_efect);
    console.log("suma_efect:" +suma_efect);
    console.log("total:" +total);*/
    $("#total_liquida_efect").val(convertMoneda(suma_efect));
    $("#total_liquida").val(convertMoneda(total));
  }, 1500);
  if($("#tipo_actividad option:selected").val()==6){
    validarFechasPago();
  }
}
function convertMoneda(num){
  monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
  return monto;
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

////////////////////// Operaciones
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}
/// < / >