var lim_efec_band=false;
$(document).ready(function($) {
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500);  
	ComboAnio();
	$(".dateTime").inputmask();
  AllNformat();
  ShowForm();
  $("#fecha_operacion").on("change",function(){
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });

  if(status==0 && $("#aviso").val()>0)
    verificaFechas();

});

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function AllNformat(){
    $(".Nformat").each(function(){
      mon=$(this).val();
      mon1=replaceAll(mon, ",", "" );
      mon2=replaceAll(mon1, "$", "" );
      monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mon2);
      $(this).val(monto);
    });
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc);
  }
}

$(".Nformat").blur(function(){
	mon=$(this).val();
	mon1=replaceAll(mon, ",", "" );
	mon2=replaceAll(mon1, "$", "" );
	monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mon2);
	$(this).val(monto);
});

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

$("#tipoOperacion").change(function(){
	ShowForm();
});

function ShowForm(){
  to=$("#tipoOperacion").val();
  switch(to){
    case "1":
    Element=document.getElementById("EL1");
    break;
    case "2":
    Element=document.getElementById("EL1");
    break;
    case "3":
    Element=document.getElementById("EL2");
    break;
    case "4":
    Element=document.getElementById("EL3");
    break;
    case "5":
    Element=document.getElementById("EL3");
    break;
    case "6":
    Element=document.getElementById("EL4");
    break;
    case "7":
    Element=document.getElementById("EL4");
    break;
    default:

    break;
  }
  E1=document.getElementById("EL1");
  E2=document.getElementById("EL2");
  E3=document.getElementById("EL3");
  E4=document.getElementById("EL4");
  E1.style.display="none";
  E2.style.display="none";
  E3.style.display="none";
  E4.style.display="none";
  Element.style.display="block";
}

$("#SAV1").change(function(event){
  val=$("#SAV1").val();
  if(val==999999){
    $("#NAC1").css("display", "block");
  }else{
    $("#NAC1").css("display", "none");
  }
});
$("#SAV2").change(function(event){
  val=$("#SAV2").val();
  if(val==999999){
    $("#NAC2").css("display", "block");
  }else{
    $("#NAC2").css("display", "none");
  }
});
$("#SAV3").change(function(event){
  val=$("#SAV3").val();
  if(val==999999){
    $("#NAC3").css("display", "block");
  }else{
    $("#NAC3").css("display", "none");
  }
});
$("#SAV4").change(function(event){
  val=$("#SAV4").val();
  if(val==999999){
    $("#NAC4").css("display", "block");
  }else{
    $("#NAC4").css("display", "none");
  }
});

$("#Fis").click(function(event){
  $("#Fisica").show();
  $("#Moral").hide();
});
$("#Mor").click(function(event){
  $("#Fisica").hide();
  $("#Moral").show();
});
$("#Nac").click(function(event){
  $("#Nacional").show();
  $("#Extranjera").hide();
});
$("#Ext").click(function(event){
  $("#Nacional").hide();
  $("#Extranjera").show();
});

function registrar(){
  var form_register = $('#form_anexo16');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
                referencia:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_anexo16").valid();
        var band_aviso = 0;
        if(valid) {
          
          monto_opera = $("#monto_opera").val();

          if($("#aviso").val()>0){
            name_table = "anexo16_aviso";
          }else{
            name_table = "anexo16";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }

          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo16',
              async: false,
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = JSON.parse(data);
                var id=array.id; 
                var monto=parseFloat(array.monto); 
                var id_clientec_transac=array.id_clientec_transac; 
                $("input[name*='id_aviso']").val(id);
                SenComp(id);
                //Anexo 9 es de $139,442.00
              }  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }  
}

function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
function btn_hashOperacion_ayuda(){
  $('#modal_hashOperacion_ayuda').modal();
}

function SenComp(id){
	to=$("#tipoOperacion").val();
	switch(to){
		case "1":
		info=$("#T1").serialize();
		break;
		case "2":
		info=$("#T1").serialize();
		break;
		case "3":
		info=$("#T2").serialize();
		break;
		case "4":
		info=$("#T3").serialize();
		break;
		case "5":
		info=$("#T3").serialize();
		break;
		case "6":
		info=$("#T4").serialize();
		break;
		case "7":
		info=$("#T4").serialize();
		break;
		default:
		break;
	}
	$.ajax({
        type: 'POST',
        url : base_url+'index.php/Transaccion/inset_pago_anexo16',
        async: false,
        data: info+"&idanexo16="+id+"&id="+$("#idLi").val()+"&aviso="+$("#aviso").val()+"&tipoOperacion="+to,
        success: function(data2){
        }
    });
    //RevSiguiente(id);
    ValidLimitByUmas(id)
}

function ValidLimitByUmas(id){
  var id_recibe = id;
  var anio = $("#anio").val();
  var valor_uma; var band_umbral=false;
  var addtp=0; var addtpD=0;
  /*var act = new Date();
  var mes_act = act.getMonth();
  var anio_act = act.getFullYear();*/
  var umas_anexo=0;
  //var addtpHF=0; var umas_anexoH=0; 
  var umas_anexoTot_historico=0;
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      //var array = $.parseJSON(result);
      var array= JSON.parse(result);
      var aviso = parseFloat(array[0].aviso).toFixed(2);
      var siempre_avi=array[0].siempre_avi;
      var siempre_iden=array[0].siempre_iden;
      var identifica=array[0].identificacion;
      //console.log("siempre avi: "+siempre_avi);
      cont=1;
      /*var TABLA = $(".row_liquidacion .liquidacion > div");              
      TABLA.each(function(){        
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){//en pesos
          //console.log("pesos de Liquidación ");
          var monto_operacion = $(this).find("input[name*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          if(monto_operacion.indexOf('$') != -1){
            tot_mo = replaceAll(monto_operacion, ",", "" );
            tot_mo = replaceAll(tot_mo, "$", "" );
          }else{
            tot_mo=monto_operacion;
          }
          tot_mo = parseFloat(tot_mo);
          var vstotal1 = tot_mo;
          //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
            addtp += Number(vstotal1);
          //}
          //addtp += parseFloat(vstotal1);
          //if(addtp>0){
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              async: false,
              data: { anio: fecha_pago},
              success: function(data2){
                valor_uma=parseFloat(data2);
                //console.log("valor uma en function umasAnio: "+valor_uma);
                umas_anexo += addtp/valor_uma;
                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                //console.log("valor1 de umas_anexo: "+umas_anexo);
              }
            });
          //}
        }
        if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){
          //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
          var monto_operacion = $(this).find("input[name*='monto']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { 
              tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(), 
              fecha:$(this).find("input[id*='fecha_pago']").val()
          },
            success: function(result_divi){
              valor_divi=parseFloat(result_divi);
              $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
              //console.log("monto operacion: "+monto_operacion);
              //console.log("valor_divi: "+valor_divi);
              if(monto_operacion.indexOf('$') != -1){
                tot_moD = replaceAll(monto_operacion, ",", "" );
                tot_moD = replaceAll(tot_moD, "$", "" );
              }else{
                tot_moD = monto_operacion;
              }
              tot_moD = parseFloat(tot_moD);
              //console.log("valor2 de tot_moD: "+tot_moD);
              var vstotal1D = tot_moD*valor_divi;
              //console.log("valor2 de vstotal1D: "+vstotal1D);
              //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                addtpD += Number(vstotal1D);
              //}
              //addtpD += parseFloat(vstotal1D);
              //console.log("valor2 de addtpD: "+addtpD);
              //if(addtpD>0){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  async: false,
                  data: { anio: fecha_pago},
                  success: function(data2){
                    valor_uma=parseFloat(data2);
                    //console.log("valor uma en function umasAnio: "+valor_uma);
                    umas_anexo += addtpD/valor_uma;
                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                    //console.log("valor2 de umas_anexo: "+umas_anexo);
                  }
                });
              //}
            }
          });
        }
        cont++;
      });*/
      to=$("#tipoOperacion").val();
      switch(to){
        case "1":
        monto_operacion=$("input[name*='monto_operacion']").val();
        fecha_operacion=$("input[name*='fecha_hora_operacion']").val(); 
        moneda_operacion=$("select[name*='moneda_operacion'] option:selected").val(); 
        break;
        case "2":
        monto_operacion=$("input[name*='monto_operacion']").val();
        fecha_operacion=$("input[name*='fecha_hora_operacion']").val(); 
        moneda_operacion=$("select[name*='moneda_operacion']").val(); 
        break;
        case "3":
        monto_operacion=$("input[name*='monto_operacion_mn']").val();
        fecha_operacion=$("input[name*='fecha_hora_operacion']").val(); 
        moneda_operacion=$("select[name*='moneda_operacion'] option:selected").val();
        break;
        case "4":
        monto_operacion=$("input[name*='monto_operacion_mn']").val();
        fecha_operacion=$("input[name*='fecha_hora_operacion']").val(); 
        moneda_operacion=0;
        break;
        case "5":
        monto_operacion=$("input[name*='monto_operacion_mn']").val();
        fecha_operacion=$("input[name*='fecha_hora_operacion']").val(); 
        moneda_operacion=0;
        break;
        case "6":
        monto_operacion=$("input[name*='monto_operacion']").val();
        fecha_operacion=$("input[name*='fecha_hora_operacion']").val(); 
        moneda_operacion=$("select[name*='moneda_operacion'] option:selected").val();;
        break;
        case "7":
        monto_operacion=$("input[name*='monto_operacion']").val();
        fecha_operacion=$("input[name*='fecha_hora_operacion']").val(); 
        moneda_operacion=$("select[name*='moneda_operacion'] option:selected").val();;
        break;
        default:
        break;
      }
      if(moneda_operacion=="0"){//en pesos
        if(monto_operacion.indexOf('$') != -1){
          tot_mo = replaceAll(monto_operacion, ",", "" );
          tot_mo = replaceAll(tot_mo, "$", "" );
        }else{
          tot_mo=monto_operacion;
        }
        tot_mo = parseFloat(tot_mo);
        var vstotal1 = tot_mo;
        addtp += Number(vstotal1);
        $.ajax({
          type: 'POST',
          url : base_url+'Umas/getUmasAnio',
          async: false,
          data: { anio: fecha_operacion},
          success: function(data2){
            valor_uma=parseFloat(data2);
            //console.log("valor uma en function umasAnio: "+valor_uma);
            umas_anexo += addtp/valor_uma;
            //umas_anexo = parseFloat(umas_anexo).toFixed(2);
            //console.log("valor1 de umas_anexo: "+umas_anexo);
          }
        });
      }
      console.log("moneda_operacion: "+moneda_operacion);
      console.log("monto_operacion: "+monto_operacion);
      if(moneda_operacion!="0"){
        $.ajax({
          type: 'POST',
          url : base_url+'Divisas/getDivisaTipo',
          async: false,
          data: { 
            tipo: moneda_operacion, 
            fecha:fecha_operacion
          },
          success: function(result_divi){
            valor_divi=parseFloat(result_divi);
            if(monto_operacion.indexOf('$') != -1){
              tot_moD = replaceAll(monto_operacion, ",", "" );
              tot_moD = replaceAll(tot_moD, "$", "" );
            }else{
              tot_moD = monto_operacion;
            }
            tot_moD = parseFloat(tot_moD);
            //console.log("valor2 de tot_moD: "+tot_moD);
            var vstotal1D = tot_moD*valor_divi;
            addtpD += Number(vstotal1D);
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              async: false,
              data: { anio: fecha_operacion},
              success: function(data2){
                valor_uma=parseFloat(data2);
                //console.log("valor uma en function umasAnio: "+valor_uma);
                umas_anexo += addtpD/valor_uma;
                //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                //console.log("valor2 de umas_anexo: "+umas_anexo);
              }
            });
          }
        });
      }

      var time = cont*500;
      setTimeout(function () { 
        if(lim_efec_band==true){
          swal("Alerta!", "El efectivo de la transaccion es mayor al limite permitido", "error");
          $('.limite_efect_msj').html('El efectivo(UMAS) de la transaccion es mayor al limite permitido.');
        }else{
          lim_efec_band=false;
          $('.limite_efect_msj').html('');
        }

        umas_anexo = parseFloat(umas_anexo).toFixed(2);
        console.log("valor de umas_anexo final: "+umas_anexo);
        console.log("valor de umas aviso: "+aviso);
        addtpF = parseFloat(addtp) + parseFloat(addtpD);
        console.log("valor de addtpF: "+addtpF);

        umas_anexo = umas_anexo*1;
        aviso = parseFloat(aviso)*1;
        //console.log("aviso: "+aviso);
        if(siempre_avi=="1"){
          band_umbral=true;
          $.ajax({ //cambiar pago a acusado
            type: 'POST',
            url : base_url+'Operaciones/acusaPago2',
            async: false,
            data: { id:id_recibe, act:$("#id_actividad").val()},
            success: function(result_divi){

            }
          });
        }else{
          if(umas_anexo>=aviso){
            band_umbral=true;
            $.ajax({ //cambiar pago a acusado
            type: 'POST',
            url : base_url+'Operaciones/acusaPago2',
            async: false,
            data: { id:id_recibe, act:$("#id_actividad").val()},
            success: function(result_divi){

            }
          });
          }
        }
        id_acusado=0;
        if(umas_anexo<aviso){
          $.ajax({
            type: 'POST',
            url : base_url+'Operaciones/getOpera',
            async: false,
            data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id_recibe, id_perfilamiento: $("#id_perfilamiento").val() },
            success: function(result){
              var data= $.parseJSON(result);
              var datos = data.data;
              datos.forEach(function(element) {
                var addtpHF=0; var umas_anexoH=0;
                if(element.moneda_operacion!=0){ //divisa
                  $.ajax({
                    type: 'POST',
                    url : base_url+'Divisas/getDivisaTipo',
                    async: false,
                    data: { tipo: element.moneda_operacion, fecha: element.fecha_hora_operacion },
                    success: function(result_divi){
                      valor_divi=parseFloat(result_divi);
                      if(element.monto_operacion!="" && element.monto_operacion>0)
                        tot_moDH = parseFloat(element.monto_operacion);
                      else if(element.monto_operacion_mn!="" && element.monto_operacion_mn>0)
                        tot_moDH = parseFloat(element.monto_operacion_mn);
                      else if(element.monto_operacion_mn_r!="" && element.monto_operacion_mn_r>0)
                        tot_moDH = parseFloat(element.monto_operacion_mn_r);
                      
                      //tot_moDH = parseFloat(element.monto);
                      //console.log("valor2 de tot_moD: "+tot_moD);
                      var totalHD = tot_moDH*valor_divi;
                      totalHD = totalHD.toFixed(2);
                      addtpHF += Number(totalHD);

                      $.ajax({
                        type: 'POST',
                        url : base_url+'Umas/getUmasAnio',
                        async: false,
                        data: { anio: element.fecha_hora_operacion},
                        success: function(data2){
                          valor_uma=parseFloat(data2).toFixed(2);
                          //console.log("valor de uma en historico: "+valor_uma);
                          //umas_anexoH += parseFloat(addtpHF/valor_uma);
                          precalc= addtpHF/valor_uma;
                          if(precalc>=identifica){
                            umas_anexoH += parseFloat(addtpHF/valor_uma);
                          }
                        }
                      });

                    }
                  });
                }//if de divisa
                else{
                  //tot_moH = parseFloat(element.monto);
                  if(element.monto_operacion!="" && element.monto_operacion>0)
                    tot_moH = parseFloat(element.monto_operacion);
                  else if(element.monto_operacion_mn!="" && element.monto_operacion_mn>0)
                    tot_moH = parseFloat(element.monto_operacion_mn);
                  else if(element.monto_operacion_mn_r!="" && element.monto_operacion_mn_r>0)
                    tot_moH = parseFloat(element.monto_operacion_mn_r);
                  totalHD = tot_moH.toFixed(2);
                  addtpHF += Number(totalHD); 

                  $.ajax({
                    type: 'POST',
                    url : base_url+'Umas/getUmasAnio',
                    async: false,
                    data: { anio: element.fecha_hora_operacion},
                    success: function(data2){
                      valor_uma=parseFloat(data2).toFixed(2);
                      //console.log("valor de uma en historico: "+valor_uma);
                      //umas_anexoH += parseFloat(addtpHF/valor_uma);
                      precalc= addtpHF/valor_uma;
                      if(precalc>=identifica){
                        umas_anexoH += parseFloat(addtpHF/valor_uma);
                      }
                    }
                  });
                }
                addtpHF=parseFloat(addtpHF).toFixed(2);
                umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                umas_anexoH=umas_anexoH*1;
                //console.log("valor de umas_anexoH: "+umas_anexoH);
                if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){ 
                  if(umas_anexoH>=identifica)
                    umas_anexoTot_historico = umas_anexo + umas_anexoH;
                  else
                    umas_anexoTot_historico = umas_anexoH;

                  //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                  if(umas_anexoTot_historico>=aviso){
                    $.ajax({ //cambiar pago a acusado
                      type: 'POST',
                      url : base_url+'Operaciones/acusaPago',
                      async: false,
                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id_recibe, aviso:$("#aviso").val(), pago_ayuda:1},
                      success: function(result2){
                        id_acusado=result2;
                        //console.log("acusado: "+id_acusado);
                      }
                    });
                  }
                }
                /*else{
                  umas_anexoTot_historico = umas_anexo;
                }*/
                //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                if(umas_anexoTot_historico>=aviso){
                  band_umbral=true;
                }
              });//foreach
            }//success de  get opera
          });
        }

        console.log("band_umbral: "+band_umbral);
        var montof = replaceAll($('#monto_opera').val(), "$", "" );
        montof = replaceAll(montof, ",", "" );
        var montof = parseFloat(montof);
        if (addtpF==montof){ //hay diferentes divisas y no pueden ser los mismos montos siempre
          validar_xml=1;
        }else{
          validar_xml=0;
        }
        console.log("validar_xml: "+validar_xml);
        setTimeout(function () {
          if(band_umbral==true && band_umbral!=undefined && !isNaN(umas_anexo)){
            if(validar_xml==1){
              if($("#aviso").val()>0){
                window.location.href = base_url+"Transaccion/anexo16_xml/"+id_recibe+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
              }else{
                window.location.href = base_url+"Transaccion/anexo16_xml/"+id_recibe+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
              }
              
              swal("Éxito!", "Se han realizado los cambios correctamente", "success");
              /*if(history.back()!=undefined){
                setTimeout(function () { history.back() }, 1500);
              }*/
              if($("#aviso").val()>0){
                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
              }else{
                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
              }
            }else{
              swal("Éxito!", "Se han realizado los cambios correctamente", "success");
              /*if(history.back()!=undefined){
                setTimeout(function () { history.back() }, 1500);
              }*/
              if($("#aviso").val()>0){
                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
              }else{
                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
              }
            }
          }
          else{
            swal("Éxito!", "Se han realizado los cambios correctamente", "success");
            /*if(history.back()!=undefined){
              setTimeout(function () { history.back() }, 1500);
            }*/
            if($("#aviso").val()>0){
              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
            }else{
              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
            }
          }
        }, 1500); //esperar a la sumatoria

      }, time);

    }//success  
  });  //ajax
}

function RevSiguiente(id){
  mon=$("#monto_opera").val();
  mon1=replaceAll(mon, ",", "" );
  monto=replaceAll(mon1, "$", "" );
  rsum=$("#monto_opera").val();
  rsum1=replaceAll(rsum, ",", "" );
  suma=replaceAll(rsum1, "$", "" );
  if (suma==monto){
    validar_xml=1;
  }else{
    validar_xml=0;
  }
  if(monto>=56038){
      if($("#aviso").val()>0){
        window.location.href = base_url+"Transaccion/anexo16_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0";
      }else{
        window.location.href = base_url+"Transaccion/anexo16_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0";
      }
      swal("Éxito!", "Se han realizado los cambios correctamente", "success");
      if($("#aviso").val()>0){
        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
      }else{
        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
      }
  }else{
    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
    if($("#aviso").val()>0){
      setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
    }else{
      setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
    }
  }
  // body... 
}

function regresar(){
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}

// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}