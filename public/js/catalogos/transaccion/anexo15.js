var base_url = $('#base_url').val();
var validar_xml=0;
var lim_efec_band=false;
$(document).ready(function(){
  var status = $('.status').text(); 
  var aux_pga=$('#aux_pga').val();

  if(aux_pga==1){
    var id_ax15=$('#id_ax15').val();
    tabla_liquidacion_anexo15(id_ax15,$("#aviso").val());
  }else{
    agregar_liqui();
  } 
  $("#regresa").on("click", function() {
    //history.back();
    if(history.back()!=undefined)
      setTimeout(function () { history.back() }, 1500);
    else
      window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
  });
  var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#monto').val());
  $('#monto').val(totalg);

  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($("#valor_inmueble").val());
  $("#valor_inmueble").val(monto);

  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
  /*
  $("#monto").on('change',function(){
    //console.log("cambia monto");
    var min = true;
    var addtp = 0; i=0;
    /*$(".monto").each(function() {
        totalmonto = replaceAll($(this).val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        totalmonto = Math.trunc(totalmonto);
        var vstotal = totalmonto;
        
        vstotal
        addtp += Number(vstotal);
    });
    totalmonto = replaceAll($("#monto").val(), ",", "" );
    totalmonto = replaceAll(totalmonto, "$", "" );
    //totalmonto = Math.trunc(totalmonto);
    var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalmonto);
    $("#monto").val(monto);
  });
*/
  $("#valor_inmueble").on('change',function(){
    //console.log("valor_inmueble: "+$("#valor_inmueble").val());
    totalmonto = replaceAll($("#valor_inmueble").val(), ",", "" );
    totalmonto = replaceAll(totalmonto, "$", "" );
    //totalmonto = Math.trunc(totalmonto);

    var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalmonto);
    $("#valor_inmueble").val(monto);
  });

  $("#form_anexo15").keypress(function(e) {
    if (e.which == 13) {
        return false;
    }
  });
  $("#forma_pago").keypress(function(e) {
    if (e.which == 13) {
        return false;
    }
  });

  /*if($("#idtransbene").val()>0){ //comprobar de otra manera, y agregar a morales
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/verificarBene",
      data: { "idtb": $("#idtransbene").val() },
      success: function (data) {
        //console.log(data);
        var json = $.parseJSON(data);
        //console.log(json.data[0].bene_moral);
        if(json.tipo_persona==1){
          $("#cont_benef").html("<h4>Beneficiario:</h4><br><br>Nombre: "+json.data.nombre +"<br>Apellido Paterno: "+json.data.apellido_paterno+" <br>Apellido Materno: "+json.data.apellido_materno);
        }
        if(json.tipo_persona==2){
          //console.log('persona moral');
          $("#cont_benef").html("<h4>Beneficiario:</h4><br><br>Nombre: "+json.data[0].bene_moral);
        }
      }
    });
  }*/
  /*
  $('#form_anexo15').validate({
      rules: {
          fecha_operacion: "required",
          fecha_arrenda: "required",
          fecha_fin_arrenda: "required",
          tipo_inmueble: "required",
          valor_inmueble: "required",
          calle: "required",
          no_ext: "required",
          colonia: "required",
          cp: "required",
          folio: "required",
          fecha_pago: "required",
          forma_pago: "required",
          instrum_notario: "required",
          tipo_moneda: "required",
          monto_opera: "required"
      },
      messages: {
          fecha_operacion: "Campo requerido",
          fecha_arrenda: "Campo requerido",
          fecha_fin_arrenda: "Campo requerido",
          tipo_inmueble: "Campo requerido",
          valor_inmueble: "Campo requerido",
          calle: "Campo requerido",
          no_ext: "Campo requerido",
          colonia: "Campo requerido",
          cp: "Campo requerido",
          folio: "Campo requerido",
          fecha_pago: "Campo requerido",
          forma_pago: "Campo requerido",
          instrum_notario: "Campo requerido",
          tipo_moneda: "Campo requerido",
          monto_opera: "Campo requerido"
      },
      submitHandler: function (form) {
          totot1 = replaceAll($("#monto").val(), ",", "" );
          monto_opera = replaceAll(totot1, "$", "" );
          //mnop = Math.trunc(monto_opera);

          valorm = replaceAll($("#valor_inmueble").val(), ",", "" );
          valorm = replaceAll(valorm, "$", "" );
          //valorm = Math.trunc(valorm);
          name_table = "anexo_15";
          if($("#aviso").val()>0)
            name_table = "anexo_15_aviso";
          $.ajax({
               type: "POST",
               url: base_url+"index.php/Transaccion/submit15",
               // data: $(form).serialize()+"&tipo_vehiculo="+vehi,
               data: $('#form_anexo15').serialize()+"&tabla="+name_table+"&valor_inmueble="+valorm+"&monto_opera="+monto_opera,
               beforeSend: function(){
                  $("#btn_submit").attr("disabled",true);
               },
               success: function (result) {
                  //console.log(result);
                  var array = $.parseJSON(result);
                  var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                  $("input[name*='id_aviso']").val(id);
                  console.log("monto operacion: "+monto);
                  if(validar_xml==1){
                    if($("#aviso").val()>0){
                      window.location.href = base_url+"Transaccion/anexo15_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0";
                    }else{
                      window.location.href = base_url+"Transaccion/anexo15_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0";
                    }
                    
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    if($("#aviso").val()>0){
                      setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                    }else{
                      setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                    }
                  }else{
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    if($("#aviso").val()>0){
                      setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                    }else{
                      setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                    }
                  }
                  //////////////////////////////////////////////////
                  var DATAP= [];
                  var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
                  TABLAP.each(function(){         
                        item = {};
                        item ["id_anexo_15"]=id;
                        item ["id"]=$(this).find("input[id*='id_a']").val();
                        item ["fecha_pago"]=$(this).find("input[id*='fecha_pago_a']").val();
                        item ["forma_pago"]=$(this).find("select[id*='forma_pago_a'] option:selected").val();
                        item ["instrum_notario"]=$(this).find("select[id*='instrum_notario_a'] option:selected").val();
                        item ["tipo_moneda"]=$(this).find("select[id*='tipo_moneda_a'] option:selected").val();
                        item ["monto"]=$(this).find("input[id*='monto_a']").val();
                        DATAP.push(item);
                  });
                  INFOP  = new FormData();
                  aInfop   = JSON.stringify(DATAP);
                  INFOP.append('data', aInfop);
                  $.ajax({
                      data: INFOP,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_pago_anexo15',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  /*window.location.href = base_url+"Transaccion/anexo15_xml/"+id+"/"+$('#id_perfilamiento').val();
                  swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                  setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
               }
           
          });
          return false; // required to block normal submit for ajax
      },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
  });
  */

  tipo_modeda();
  ComboAnio();
  setTimeout(function () { 
    suma_tablaliquidacion();
    anio_acuse=$("#anio_aux").val();
    $("#anio").val(anio_acuse);

  }, 1500);
  //
  $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");

  //para traer datos del inmueble an caso de seleccionar uno
  $("#inmuebles").on("change",function(){
    if($("#inmuebles").val()>0 || $("#inmuebles").val()!=""){
      //console.log("val inmuble: "+this.value);
      datosDirSelect(this.value)
    }
  });
  $("#fecha_operacion").on("change",function(){
    valorUma();
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();
  
});

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function datosDirSelect(id){
  $.ajax({
    type: "POST",
    url: base_url+"Clientes/data_record_inmuebleId",
    data: { "id": id },
    success: function (data) {
      var json = $.parseJSON(data);
      $("#tipo_inmueble").val(json[0].tipo_inmueble);
      var valorc=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(json[0].valor);
      $("#valor_inmueble").val(valorc);
      $("#calle").val(json[0].calle_avenida);
      $("#no_ext").val(json[0].num_ext);
      $("#no_int").val(json[0].num_int);
      //$("#colonia").val(json[0].colonia);
      //autoComplete();
      $('#colonia').append('<option selected value="'+json[0].colonia+'" >'+json[0].colonia+ '</option');
      $("#cp").val(json[0].cp);
      $("#folio").val(json[0].folio_inmueble);
    }
  });
}

function autoComplete(){
  cp = $("#cp").val(); 
  col = $('#colonia').val(); 
  $('#colonia').select2({
      width: 'resolve',
      minimumInputLength: 0,
      minimumResultsForSearch: 10,
      placeholder: 'Seleccione una colonia:',
      ajax: {
          url: base_url+'Perfilamiento/getDatosCPSelect',
          dataType: "json",
          data: function(params) {
            var query = {
              search: params.term,
              cp: cp, 
              col: col ,
              type: 'public'
            }
            return query;
          },
          processResults: function(data) {
            var itemscli = [];
            data.forEach(function(element) {
              itemscli.push({
                  id: element.colonia,
                  text: element.colonia,
              });
            });
            return {
                results: itemscli
            };
          },
      }
  });
}

function guardar_anexo15(){
  var form_register = $('#form_anexo15');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                referencia:{
                  required: true,
                  minlength: 1,
                  maxlength: 14
                },
                fecha_arrenda:{
                  required: true
                },
                fecha_fin_arrenda:{
                  required: true
                },
                tipo_inmueble:{
                  required: true
                },
                valor_inmueble:{
                  required: true
                },
                calle:{
                  required: true
                },
                no_ext:{
                  required: true
                },
                colonia:{
                  required: true
                },
                cp:{
                  required: true
                },
                folio:{
                  required: true
                },
                fecha_pago:{
                  required: true
                },
                forma_pago:{
                  required: true
                },
                instrum_notario:{
                  required: true
                },
                tipo_moneda:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
              success_register.fadeOut(500);
              error_register.fadeIn(500);
              scrollTo(form_register,-100);

            },
            highlight: function (element) { // hightlight error inputs
              $(element).addClass('vd_bd-red');
              $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
              $(element).closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = form_register.valid();
        var band_aviso = 0;
        if(valid){
          totot1 = replaceAll($("#monto_opera").val(), ",", "" );
          monto_opera = replaceAll(totot1, "$", "" );
          
          valorm = replaceAll($("#valor_inmueble").val(), ",", "" );
          valorm = replaceAll(valorm, "$", "" );
          //valorm = Math.trunc(valorm);
          name_table = "anexo_15";
          if($("#aviso").val()>0){
            name_table = "anexo_15_aviso";
            //band_aviso=1;
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
              
          }
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Transaccion/submit15",
              async: false,
              data: $('#form_anexo15').serialize()+"&tabla="+name_table+"&valor_inmueble="+valorm+"&monto_opera="+monto_opera,
              statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success: function (result) {
                  //console.log(result);
                  var array = $.parseJSON(result);
                  var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                  //////////////////////////////////////////////////
                  var DATAP= [];
                  var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
                  TABLAP.each(function(){         
                        item = {};
                        item ["id_anexo_15"]=id;
                        item ["aviso"]=$("#aviso").val();
                        item ["id"]=$(this).find("input[id*='id_a']").val();
                        item ["fecha_pago"]=$(this).find("input[id*='fecha_pago_a']").val();
                        item ["forma_pago"]=$(this).find("select[id*='forma_pago_a'] option:selected").val();
                        item ["instrum_notario"]=$(this).find("select[id*='instrum_notario_a'] option:selected").val();
                        item ["tipo_moneda"]=$(this).find("select[id*='tipo_moneda_a'] option:selected").val();
                        montop = replaceAll($(this).find("input[id*='monto_a']").val(), ",", "" );
                        montop = replaceAll(montop, "$", "" );
                        item ["monto"]=montop;
                        DATAP.push(item);
                  });
                  INFOP  = new FormData();
                  aInfop   = JSON.stringify(DATAP);
                  INFOP.append('data', aInfop);
                  $.ajax({
                      data: INFOP,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_pago_anexo15',
                      async: false,
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  ///////////////////////////////////////
                  $("input[name*='id_aviso']").val(id);
                  //console.log("monto operacion: "+monto);
                  ValidLimitByUmas(id);
                  /*window.location.href = base_url+"Transaccion/anexo15_xml/"+id+"/"+$('#id_perfilamiento').val();
                  swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                  setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);*/
              }
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }//valid
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function tipo_moneda(id){
  var total = parseFloat($("#total_liquida").val()).toFixed(2);
 //console.log("montog: "+montog);
  monto = $(''+id+'').val();
  var totalg = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total+monto);
  $("#total_liquida").val(totalg);
}

function tipo_modeda(){
  totot1 = replaceAll($('#monto_opera').val(), ",", "" );
  totot2 = replaceAll(totot1, "$", "" );
  var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totot2);
  $('#monto_opera').val(totalg);
  suma_tablaliquidacion();
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

function formatNumber (n) {
  n = String(n).replace(/\D/g, "");
  return n === '' ? n : Number(n).toLocaleString();
}
function agregar_liqui(){
  addliquidacion(0,'','','','','');   
}
var aux_liqui=0;
function addliquidacion(id,fecha_pago,forma_pago,instrum_notario,tipo_moneda,monto){
    var aviso_tipo=$("#aviso").val();
    var fecha_max=$('#fecha_max').val();
    var fp1=''; var fp2=''; var fp3=''; var fp4=''; var fp5='';
    if(forma_pago==1){ fp1='selected'; }
    else if(forma_pago==2){fp2='selected';}
    else if(forma_pago==3){fp3='selected';}
    else if(forma_pago==4){fp4='selected';}
    else if(forma_pago==5){fp5='selected';}
    ///////////////////////////////////////
    var in1=''; var in2=''; var in3=''; var in4=''; var in5=''; var in6=''; var in7=''; var in8=''; var in9=''; 
    var in10=''; var in11=''; var in12=''; var in13=''; var in14=''; var in15=''; var in16=''; var in17='';
    if(instrum_notario==1){ in1='selected'; }
    else if(instrum_notario==2){in2='selected';}
    else if(instrum_notario==3){in3='selected';}
    else if(instrum_notario==4){in4='selected';}
    else if(instrum_notario==5){in5='selected';}
    else if(instrum_notario==6){in6='selected';}
    else if(instrum_notario==7){in7='selected';}
    else if(instrum_notario==8){in8='selected';}
    else if(instrum_notario==9){in9='selected';}
    else if(instrum_notario==10){in10='selected';}
    else if(instrum_notario==11){in11='selected';}
    else if(instrum_notario==12){in12='selected';}
    else if(instrum_notario==13){in13='selected';}
    else if(instrum_notario==14){in14='selected';}
    else if(instrum_notario==15){in15='selected';}
    else if(instrum_notario==16){in16='selected';}
    else if(instrum_notario==17){in17='selected';}
    ///////////////////////////////////////
    var html='<div class="row row_div_'+aux_liqui+'">\
                  <input type="hidden" id="id_a" value="'+id+'">\
                  <div class="col-md-4 form-group">\
                    <label>Fecha de pago</label>\
                    <input onkeydown="return false" class="form-control fecha_pago_a_'+aux_liqui+'" max="'+fecha_max+'" type="date" id="fecha_pago_a" value="'+fecha_pago+'" onchange="validar_fecha_opera('+aux_liqui+')">\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Forma de pago</label>\
                    <select id="forma_pago_a" class="form-control">\
                      <option value="1" '+fp1+'>Contado</option>\
                      <option value="2" '+fp2+'>Diferido</option>\
                      <option value="3" '+fp3+'>Dación en pago</option>\
                      <option value="4" '+fp4+'>Préstamo o crédito</option>\
                      <option value="5" '+fp5+'>Permuta</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Instrumento monetario con el que se realizó la operación o acto</label>\
                    <select id="instrum_notario_a" class="form-control">\
                      <option value="1" '+in1+'>Efectivo</option>\
                      <option value="2" '+in2+'>Tarjeta de Crédito</option>\
                      <option value="3" '+in3+'>Tarjeta de Debito</option>\
                      <option value="4" '+in4+'>Tarjeta de Prepago</option>\
                      <option value="5" '+in5+'>Cheque Nominativo</option>\
                      <option value="6" '+in6+'>Cheque de Caja</option>\
                      <option value="7" '+in7+'>Cheques de Viajero</option>\
                      <option value="8" '+in8+'>Transferencia Interbancaria</option>\
                      <option value="9" '+in9+'>Transferencia Misma Institución</option>\
                      <option value="10" '+in10+'>Transferencia Internacional</option>\
                      <option value="11" '+in11+'>Orden de Pago</option>\
                      <option value="12" '+in12+'>Giro</option>\
                      <option value="13" '+in13+'>Oro o Platino Amonedados</option>\
                      <option value="14" '+in14+'>Plata Amonedada</option>\
                      <option value="15" '+in15+'>Metales Preciosos</option>\
                      <option value="16" '+in16+'>Activos Virtuales</option>\
                      <option value="17" '+in17+'>Otros</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Tipo de moneda o divisa de la operación</label>\
                    <div class="tipo_moneda_select_'+aux_liqui+'"></div>\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Monto de la operación o acto sin IVA ni accesorios</label>\
                    <input class="form-control monto_a_'+aux_liqui+'" type="text" id="monto_a" value="'+monto+'" onchange="cambio_moneda_liqui('+aux_liqui+')">\
                  </div>\
                  <div class="row">\
                    <div class="col-md-10 form-group">\
                      <div>\
                        <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_liqui==0){
                      html+=''; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="removerliquidacion('+aux_liqui+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar liquidación</button>';
                        }
                        
                html+='</div>\
                    </div>\
                  </div><div class="col-md-2"><br><h3 class="monto_divisa" style="color: red;"></h3></div>\
                  <div class="col-md-12">\
                    <hr class="subsubtitle">\
                  </div>\
              </div>';
    $('.liquidacion_pago_text').append(html);
    agregartipo_moneda(aux_liqui,tipo_moneda);
    //cambio_moneda_liqui(aux_liqui);
    if(id>0){
      cambio_moneda_liqui(aux_liqui);
    }
    aux_liqui++;
}
function agregartipo_moneda(id,tipo_moneda){
   $.ajax({
      type: "POST",
      url: base_url+"index.php/Transaccion/get_tipo_moneda",
      data:{t_pago:tipo_moneda},
      success: function (data) {
        $('.tipo_moneda_select_'+id).html(data);
      }
    });
}
function removerliquidacion(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el pago!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_pago_anexo15',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.row_div_'+aux).remove();
              confirmarPagosMonto();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.row_div_'+aux).remove();
  }
}
function tabla_liquidacion_anexo15(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_liquidacion_anexo15",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  addliquidacion(element.id,element.fecha_pago,element.forma_pago,element.instrum_notario,element.tipo_moneda,element.monto);
                });
            }else{
                agregar_liqui();
            }   
        }
    });
}



function confirmarPagosMonto(){ //para sumar el efectivo nadamas
  suma_tablaliquidacion();
  suma_no_efect=0;
  suma_efect=0;
  suma_no_efect2=0;
  suma_efect2=0;
  suma_no_efect3=0;
  suma_efect3=0;
  contta=0;
  valor_divi=0;
  montoe=0;
  montone=0;
  cont=0;
  var TABLA = $(".form_pagos_padre > .cont_hijo");   
  TABLA.each(function(){    
    console.log("recorre tabla de pagos cont_hijo");    
    // pesos de Liquidación 
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
      //console.log("pesos cont_hijo");
      if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){ //en efectivo
        var monto = $(this).find("input[name*='monto']").val();
        //console.log("monto: "+$(this).find("input[name*='monto']").val());
        if(monto.indexOf('$') != -1){
          montoe = replaceAll(monto, ",", "" );
          montoe = replaceAll(montoe, "$", "" );
        }else{
          montoe=monto;
        }
        suma_efect += parseFloat(montoe);

      }  
      else{
        var monto = $(this).find("input[name*='monto']").val();
        if(monto.indexOf('$') != -1){
          montone = replaceAll(monto, ",", "" );
          montone = replaceAll(montone, "$", "" );
        }else{
          montone=monto;
        }
        suma_no_efect += parseFloat(montone);
      }
      /*console.log("suma_nefect pesos: "+suma_efect);
      console.log("suma_no_efect pesos: "+suma_no_efect);*/
    }
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){ //!= de pesos
      console.log("!pesos cont_hijo");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      monto_opera = $(this).find("input[name*='monto']").val();
      console.log("monto (de divisa) capturada: "+monto_opera);
      var inst = $(this).find("select[id*='instrum_notario'] option:selected").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago']").val()},
        success: function(result_divi){
          valor_divi=parseFloat(result_divi);
          console.log(" valor_divi:" +valor_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera.indexOf('$') != -1){
              tot_moDE = replaceAll(monto_opera, ",", "" );
              tot_moDE = replaceAll(tot_moDE, "$", "" );
            }else{
              tot_moDE = monto_opera;
            }
            console.log("tot_moDE hijo:" +tot_moDE);
            var preefect = parseFloat(tot_moDE)*valor_divi;
            console.log("preefect hijo:" +preefect);
            suma_efect += parseFloat(preefect);
            console.log("suma_efect hijo:" +suma_efect);
          }else{
            if(monto_opera.indexOf('$') != -1){
              montone = replaceAll(monto_opera, ",", "" );
              montone = replaceAll(montone, "$", "" );
            }else{
              montone=monto_opera;
            }
            var montone = parseFloat(montone)*valor_divi;
            suma_no_efect += parseFloat(montone);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
    }
    contta++;
  });
  var TABLA2 = $(".cont_padre .form_pagos > .cont_hijoExist"); 
  TABLA2.each(function(){    
    //console.log("recorre tabla de pagos cont_hijoExist");    
    // pesos de Liquidación 
    //cont++;
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
      //console.log("pesos cont_hijoExist");
      if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){ //en efectivo
        var monto2 = $(this).find("input[name*='monto']").val();
        console.log("monto pesos exist: "+$(this).find("input[name*='monto']").val());
        if(monto2.indexOf('$') != -1){
          montoe2 = replaceAll(monto2, ",", "" );
          montoe2 = replaceAll(montoe2, "$", "" );
        }else{
          montoe2=monto2;
        }
        suma_efect2 += parseFloat(montoe2);
        console.log("suma_efect2 pesos:" +suma_efect2);
        //console.log("contta:" +contta);
      }  
      else{
        var monto2 = $(this).find("input[name*='monto']").val();
        if(monto2.indexOf('$') != -1){
          montone2 = replaceAll(monto2, ",", "" );
          montone2 = replaceAll(montone2, "$", "" );
        }else{
          montone2=monto2;
        }
        suma_no_efect2 += parseFloat(montone2);
      }
      /*console.log("suma_nefect pesos: "+suma_efect);
      console.log("suma_no_efect pesos: "+suma_no_efect);*/
    }
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){ //!= de pesos
      //console.log("!pesos cont_hijoExist");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      contta++;
      console.log("contta: "+contta);
      monto_opera2 = $(this).find("input[name*='monto']").val();
      console.log("monto divisas exist: "+monto_opera2);
      var inst = $(this).find("select[id*='instrum_notario'] option:selected").val(); //es para saber si son efectivo o no

      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async: false,
        data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago']").val()},
        success: function(result_divi){
          valor_divi=parseFloat(result_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera2.indexOf('$') != -1){
              tot_moDE2 = replaceAll(monto_opera2, ",", "" );
              tot_moDE2 = replaceAll(tot_moDE2, "$", "" );
            }else{
              tot_moDE2 = monto_opera2;
            }
            var preefect2 = parseFloat(tot_moDE2)*valor_divi;
            suma_efect2 += parseFloat(preefect2);
            console.log("suma_efect2 divisa:" +suma_efect2);
          }else{
            if(monto_opera2.indexOf('$') != -1){
              montone2 = replaceAll(monto_opera2, ",", "" );
              montone2 = replaceAll(montone2, "$", "" );
            }else{
              montone2=monto_opera2;
            }
            var montone2 = parseFloat(montone2)*valor_divi;
            suma_no_efect2 += parseFloat(montone2);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);

    }
    
  });
  var TABLA3 = $(".cont_padre > .cont_hijo_clone"); 
  TABLA3.each(function(){    
    //console.log("recorre tabla de pagos cont_hijo_clone");    
    // pesos de Liquidación 
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()=="0"){ //en pesos
      //console.log("pesos cont_hijo_clone");
      if($(this).find("select[id*='instrum_notario'] option:selected").val()=="1"){ //en efectivo
        var monto3 = $(this).find("input[name*='monto']").val();
        //console.log("monto: "+$(this).find("input[name*='monto']").val());
        if(monto3.indexOf('$') != -1){
          montoe3 = replaceAll(monto3, ",", "" );
          montoe3 = replaceAll(montoe3, "$", "" );
        }else{
          montoe3=monto3;
        }
        suma_efect3 += parseFloat(montoe3);

      }  
      else{
        var monto3 = $(this).find("input[name*='monto']").val();
        if(monto3.indexOf('$') != -1){
          montone3 = replaceAll(monto3, ",", "" );
          montone3 = replaceAll(montone3, "$", "" );
        }else{
          montone3=monto3;
        }
        suma_no_efect3 += parseFloat(montone3);
      }
      /*console.log("suma_nefect pesos: "+suma_efect);
      console.log("suma_no_efect pesos: "+suma_no_efect);*/
    }
    if($(this).find("select[id*='tipo_moneda'] option:selected").val()!="0"){ //!= de pesos
      //console.log("!pesos cont_hijo_clone");
      //para sacar cant en pesos por tipo de divisa en liquidacion 
      monto_opera3 = $(this).find("input[name*='monto']").val();
      //console.log("monto: "+monto_opera);
      var inst = $(this).find("select[id*='instrum_notario'] option:selected").val();
      $.ajax({
        type: 'POST',
        url : base_url+'Divisas/getDivisaTipo',
        async:false,
        data: { tipo: $(this).find("select[id*='tipo_moneda'] option:selected").val(),fecha:$(this).find("input[id*='fecha_pago']").val()},
        success: function(result_divi){
          valor_divi=parseFloat(result_divi);
          if(inst=="1"){ //efectivo 
            if(monto_opera3.indexOf('$') != -1){
              tot_moDE3 = replaceAll(monto_opera3, ",", "" );
              tot_moDE3 = replaceAll(tot_moDE3, "$", "" );
            }else{
              tot_moDE3 = monto_opera3;
            }
            var preefect3 = parseFloat(tot_moDE3)*valor_divi;
            suma_efect3 += parseFloat(preefect3);
          }else{
            if(monto_opera3.indexOf('$') != -1){
              montone3 = replaceAll(monto_opera3, ",", "" );
              montone3 = replaceAll(montone3, "$", "" );
            }else{
              montone3=monto_opera3;
            }
            var montone3 = parseFloat(montone3)*valor_divi;
            suma_no_efect3 += parseFloat(montone3);
          }  
        }
      });
      $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
    }
    contta++;
  });
  
  setTimeout(function () { 
    total = parseFloat(suma_no_efect+suma_efect+suma_no_efect2+suma_efect2+suma_no_efect3+suma_efect3).toFixed(2);
    var aux_mo=0;
    var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
    totalmontomo = replaceAll(totalmontomo, "$", "" );
    var vstotalmo = parseFloat(totalmontomo);
    aux_mo = parseFloat(vstotalmo);
    if(aux_mo==total){
      validar_xml=1;
      $('.validacion_cantidad').html('');
    }else{
      validar_xml=0;
      $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
    }
    console.log("suma_no_efect:" +suma_no_efect);
    console.log("suma_efect:" +suma_efect);
    console.log("suma_no_efect2:" +suma_no_efect2);
    console.log("suma_efect2:" +suma_efect2);
    console.log("suma_no_efect3:" +suma_no_efect3);
    console.log("suma_efect3:" +suma_efect3);
    console.log("total:" +total);
    //$("#total_liquida_efect").val(convertMoneda(suma_efect+suma_efect2+suma_efect3));
    //$("#total_liquida").val(convertMoneda(total));
    //verificarLimite();
    //ValidLimitByUmas();
  }, 1500);
}

function cambio_moneda_liqui(aux){
  var mon=$('.monto_a_'+aux).val();
  //confirmarPagosMonto();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_a_"+aux+"").val(monto);
  suma_tablaliquidacion();
}

function suma_tablaliquidacion(){
  //console.log("funcion suma_tablaliquidaciona ");
  var min = true;
  var addtp = 0;
  var mte = 0;
  var totalmonto=0;
  var totalE=0;
  var totalNE=0;
  var divival=1;
  var TABLAP = $(".liquidacion_pago .liquidacion_pago_text > div");                  
    TABLAP.each(function(){
      console.log("liquidacion_pago: ");
        totalmonto = replaceAll($(this).find("input[id*='monto_a']").val(), "$", "" );
        totalmonto = replaceAll(totalmonto, ",", "" );
        totalmonto=Number(totalmonto);
        console.log("totalmonto: "+totalmonto);
        //intrumento monetario
        insm=$(this).find("select[id*='instrum_notario_a'] option:selected").val();
        if(insm==1){
          divisa=$(this).find("select[id*='tipo_moneda_a'] option:selected").val();
          fecha=$(this).find("input[id*='fecha_pago_a']").val();
          console.log("monto: "+totalmonto)
          console.log("divisa: "+divisa);
          console.log(fecha);
          if (divisa!=0){
            //es diferente de pesos
            console.log('es diferente de pesos');
            divival=GetValDivisa(divisa,fecha);
            $(this).find(".monto_divisa").html('Valor de divisa: $'+divival);
          }else{
            divival=1;
          }
          console.log(divival);
          totalE+=totalE+(totalmonto*divival);
        }else{
          console.log('totalmonto');
          totalNE+=(totalmonto*divival);
        }
        console.log(totalNE);
    });
    totalNE=totalNE+totalE;

  //convert formato moneda
  var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalNE);
  $('#total_liquida').val(monto_mtl);
  var monto_ttle=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalE);
  $('#total_liquida_efect').val(monto_ttle);
  

  var aux_mo=0;
  var totalmontomo = replaceAll($('#monto_opera').val(), ",", "" );
  totalmontomo = replaceAll(totalmontomo, "$", "" );
  var vstotalmo = totalmontomo;
  aux_mo = Number(vstotalmo);


  var total_liquidado = replaceAll($('#total_liquida').val(), ",", "" );
  total_liquidado = replaceAll(total_liquidado, "$", "" );

  if(aux_mo==total_liquidado){
    validar_xml=1;
    $('.validacion_cantidad').html('');
  }else{
    validar_xml=0;
    $('.validacion_cantidad').html('Monto total de las liquidaciones no coincide con el monto de la factura.'); 
  }
}

function GetValDivisa(divisa,fecha){
  valorD=1;
  $.ajax({
    type: 'POST',
    url : base_url+'Divisas/getDivisaTipo',
    async: false,
    data: { 
      'tipo':divisa,
      'fecha':fecha
    },
    success: function(result_divi){
      valorD=result_divi;
    }
  });
  return valorD;
}

function ValidLimitByUmas(id){
  var id_recibe=id;
  var anio = $("#anio").val();
  var valor_uma; var band_umbral=false;
  var addtp=0; var addtpD=0;
  var act = new Date();
  var mes_act = act.getMonth();
  var anio_act = act.getFullYear();
  var umas_anexo=0;
  //var addtpHF=0; var umas_anexoH=0; 
  var umas_anexoTot_historico=0;
  $.ajax({
    type: 'POST',
    url : base_url+'Umbrales/getUmbral',
    async: false,
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      //var array = $.parseJSON(result);
      var array= JSON.parse(result);
      var aviso = parseFloat(array[0].aviso).toFixed(2);
      var siempre_avi=array[0].siempre_avi;
      var siempre_iden=array[0].siempre_iden;
      var identifica=array[0].identificacion;
      //console.log("siempre avi: "+siempre_avi);
      cont=0;
      var TABLA = $(".liquidacion_pago .liquidacion_pago_text > div");              
      TABLA.each(function(){        
        if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()=="0"){//en pesos
          console.log("pesos de Liquidación ");
          var monto_operacion = $(this).find("input[id*='monto_a']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
          if(monto_operacion.indexOf('$') != -1){
            tot_mo = replaceAll(monto_operacion, ",", "" );
            tot_mo = replaceAll(tot_mo, "$", "" );
          }else{
            tot_mo=monto_operacion;
          }
          //console.log("monto_operacion: "+monto_operacion);
          //console.log("fecha_pago: "+fecha_pago);
          tot_mo = parseFloat(tot_mo);
          //console.log("tot_mo: "+tot_mo);
          var vstotal1 = tot_mo;
          addtp += Number(vstotal1);
          //console.log("addtp: "+addtp);
          //addtp += Number(vstotal1);
          //if(addtp>0){
            $.ajax({
              type: 'POST',
              url : base_url+'Umas/getUmasAnio',
              async: false,
              data: { anio: fecha_pago},
              success: function(data2){
                //console.log("valor_uma: "+data2);
                valor_uma=data2;
                if(valor_uma>0){
                  umas_anexo += addtp/valor_uma;
                }else{
                  umas_anexo +=addtp/1;
                }
                //console.log("umas_anexo: "+umas_anexo);
              }
            });
          //}
        }
        if($(this).find("select[id*='tipo_moneda_a'] option:selected").val()!="0"){
          //para sacar cant en pesos por tipo de divisa en liquidacion numeraria  
          var monto_operacion = $(this).find("input[id*='monto_a']").val();
          var fecha_pago = $(this).find("input[id*='fecha_pago_a']").val();
          $.ajax({
            type: 'POST',
            url : base_url+'Divisas/getDivisaTipo',
            async: false,
            data: { 
              tipo: $(this).find("select[id*='tipo_moneda_a'] option:selected").val(), 
              fecha:$(this).find("input[id*='fecha_pago_a']").val()
          },
            success: function(result_divi){
              valor_divi=result_divi;
              $(this).find(".monto_divisa").html('Valor de divisa: $'+valor_divi);
              //console.log("monto operacion: "+monto_operacion);
              //console.log("valor_divi: "+valor_divi);
              if(monto_operacion.indexOf('$') != -1){
                tot_moD = replaceAll(monto_operacion, ",", "" );
                tot_moD = replaceAll(tot_moD, "$", "" );
              }else{
                tot_moD = monto_operacion;
              }
              tot_moD = parseFloat(tot_moD);
              //console.log("valor2 de tot_moD: "+tot_moD);
              var vstotal1D = tot_moD*valor_divi;
              //console.log("valor2 de vstotal1D: "+vstotal1D);
              let date = new Date(fecha_pago);
              var mes_pago = date.getMonth();
              mes_pago = parseInt(mes_pago);
              mes_pago = mes_pago +1;
              var anio_pago = date.getFullYear();
              //if(mes_act<=6 && mes_pago<=6 && anio_act==anio_pago || mes_act>6 && mes_pago>6 && anio_act==anio_pago){
                addtpD += Number(vstotal1D);
              //}
              //addtpD += Number(vstotal1D);
              //console.log("valor2 de addtpD: "+addtpD);
              //if(addtpD>0){
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umas/getUmasAnio',
                  async: false,
                  data: { anio: fecha_pago},
                  success: function(data2){
                    valor_uma=data2;
                    //console.log("valor uma en function umasAnio: "+valor_uma);
                    umas_anexo += addtpD/valor_uma;
                    //umas_anexo = parseFloat(umas_anexo).toFixed(2);
                    //console.log("valor2 de umas_anexo: "+umas_anexo);
                  }
                });
              //}
            }
          });
        }
        cont++;
      });

      var time = cont*500;
      setTimeout(function () { 
        if(lim_efec_band==true){
          swal("Alerta!", "El efectivo de la transaccion es mayor al limite permitido", "error");
          $('.limite_efect_msj').html('El efectivo(UMAS) de la transaccion es mayor al limite permitido.');
        }else{
          lim_efec_band=false;
          $('.limite_efect_msj').html('');
        }

        umas_anexo = parseFloat(umas_anexo).toFixed(2);
        console.log("valor de umas_anexo final: "+umas_anexo);
        console.log("valor de umas aviso: "+aviso);
        addtpF = addtp + addtpD;
        //console.log("valor de addtpF: "+addtpF);

        umas_anexo = umas_anexo*1;
        aviso = aviso*1;
        //console.log("aviso: "+aviso);
        if(siempre_avi=="1"){
          band_umbral=true;
          $.ajax({ //cambiar pago a acusado
            type: 'POST',
            url : base_url+'Operaciones/acusaPago2',
            async: false,
            data: { id:id_recibe, act:$("#id_actividad").val()},
            success: function(result_divi){

            }
          });
        }else{
          if(umas_anexo>=aviso){
            band_umbral=true;
            $.ajax({ //cambiar pago a acusado
            type: 'POST',
            url : base_url+'Operaciones/acusaPago2',
            async: false,
            data: { id:id_recibe, act:$("#id_actividad").val()},
            success: function(result_divi){

            }
          });
          }
        }
        id_acusado=0;
        if(umas_anexo<aviso && $("#anio").val()==$(".anio_actual").val()){
          $.ajax({
            type: 'POST',
            url : base_url+'Operaciones/getOpera',
            async: false,
            data: { aviso:$("#aviso").val(), id_opera: $("input[name*='idopera']").val(), id_act:$("#id_actividad").val(), id_union:$("#id_union").val(), id_anexo:id_recibe, id_perfilamiento: $("#id_perfilamiento").val() },
            success: function(result){
              var data= $.parseJSON(result);
              var datos = data.data;
              datos.forEach(function(element) {
                var addtpHF=0; var umas_anexoH=0;
                //console.log("tipo_moneda: "+element.tipo_moneda);
                if(element.tipo_moneda!=0){ //divisa
                  $.ajax({
                    type: 'POST',
                    url : base_url+'Divisas/getDivisaTipo',
                    async: false,
                    data: { tipo: element.tipo_moneda, fecha: element.fecha_pago },
                    success: function(result_divi){
                      valor_divi=parseFloat(result_divi);
                      tot_moDH = parseFloat(element.monto);
                      //console.log("valor2 de tot_moD: "+tot_moD);
                      var totalHD = tot_moDH*valor_divi;
                      totalHD = totalHD.toFixed(2);
                      addtpHF += Number(totalHD);

                      $.ajax({
                        type: 'POST',
                        url : base_url+'Umas/getUmasAnio',
                        async: false,
                        data: { anio: element.fecha_pago},
                        success: function(data2){
                          valor_uma=parseFloat(data2).toFixed(2);
                          //console.log("valor de uma en historico: "+valor_uma);
                          //umas_anexoH += parseFloat(addtpHF/valor_uma);
                          precalc= addtpHF/valor_uma;
                          if(precalc>=identifica){
                            umas_anexoH += parseFloat(addtpHF/valor_uma);
                          }
                        }
                      });

                    }
                  });
                }//if de divisa
                else{
                  tot_moH = parseFloat(element.monto);
                  totalHD = tot_moH.toFixed(2);
                  addtpHF += Number(totalHD); 

                  $.ajax({
                    type: 'POST',
                    url : base_url+'Umas/getUmasAnio',
                    async: false,
                    data: { anio: element.fecha_pago},
                    success: function(data2){
                      valor_uma=parseFloat(data2).toFixed(2);
                      //console.log("valor de uma en historico: "+valor_uma);
                      //umas_anexoH += parseFloat(addtpHF/valor_uma);
                      //console.log("addtpHF: "+addtpHF);
                      precalc= Number(addtpHF)/Number(valor_uma);
                      //console.log("precalc: "+precalc);
                      //console.log("identifica: "+identifica);
                      if(precalc>=identifica){
                        umas_anexoH += parseFloat(addtpHF/valor_uma);
                      }
                    }
                  });
                }
                addtpHF=parseFloat(addtpHF).toFixed(2);
                umas_anexoH=parseFloat(umas_anexoH).toFixed(2);
                umas_anexoH=umas_anexoH*1;
                //console.log("valor de umas_anexoH: "+umas_anexoH);
                if(identifica<=umas_anexoH || siempre_iden=="1" /*|| identifica>umas_anexo*/){
                //if(identifica<=umas_anexoH && identifica!="" && identifica>0){ 

                  if(umas_anexoH>=identifica || siempre_iden=="1")
                    umas_anexoTot_historico = umas_anexo + umas_anexoH;
                  else
                    umas_anexoTot_historico = umas_anexoH;

                  /*if(umas_anexoH>=identifica || siempre_iden=="1"){
                    umas_anexoTot_historico += umas_anexoH;
                  }else{
                    umas_anexoTot_historico = umas_anexoH;
                  }*/

                  //umas_anexoTot_historico = umas_anexo + umas_anexoH;
                  if(umas_anexoTot_historico>=aviso){
                    $.ajax({ //cambiar pago a acusado
                      type: 'POST',
                      url : base_url+'Operaciones/acusaPago',
                      async: false,
                      data: { id: element.id, act:$("#id_actividad").val(), id_anexo:id_recibe, aviso:$("#aviso").val(), pago_ayuda:1},
                      success: function(result2){
                        id_acusado=result2;
                        //console.log("acusado: "+id_acusado);
                      }
                    });
                  }
                }
                /*else{
                  umas_anexoTot_historico = umas_anexo;
                }*/
                //console.log("umas_anexoTot_historico: "+umas_anexoTot_historico);
                if(umas_anexoTot_historico>=aviso){
                  band_umbral=true;
                }
              });//foreach
            }//success de  get opera
          });
        }

        //console.log("band_umbral: "+band_umbral);
        var montof = replaceAll($('#monto_opera').val(), "$", "" );
        montof = replaceAll(montof, ",", "" );
        var montof = parseFloat(montof);
        if (addtpF==montof){ //hay diferentes divisas y no pueden ser los mismos montos siempre
          validar_xml=1;
        }else{
          validar_xml=0;
        }
        //console.log("validar_xml: "+validar_xml);
        setTimeout(function () {
          if(band_umbral==true && band_umbral!=undefined){
            if(validar_xml==1 && !isNaN(umas_anexo)){
              if($("#aviso").val()>0){
                window.location.href = base_url+"Transaccion/anexo15_xml/"+id_recibe+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
              }else{
                window.location.href = base_url+"Transaccion/anexo15_xml/"+id_recibe+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/"+id_acusado;
              }
              
              swal("Éxito!", "Se han realizado los cambios correctamente", "success");
              /*if(history.back()!=undefined){
                setTimeout(function () { history.back() }, 1500);
              }*/
              if($("#aviso").val()>0){
                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
              }else{
                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
              }
            }else{
              swal("Éxito!", "Se han realizado los cambios correctamente", "success");
              /*if(history.back()!=undefined){
                setTimeout(function () { history.back() }, 1500);
              }*/
              if($("#aviso").val()>0){
                setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
              }else{
                setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
              }
            }
          }
          else{
            swal("Éxito!", "Se han realizado los cambios correctamente", "success");
            /*if(history.back()!=undefined){
              setTimeout(function () { history.back() }, 1500);
            }*/
            if($("#aviso").val()>0){
              setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
            }else{
              setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
            }
          } 
        }, 1500); //esperar a la sumatoria

      }, time);

    }//success  
  });  //ajax
}

function validar_fecha_opera(aux){
  var fecha_pago_liqui=$('.fecha_pago_a_'+aux).val();
  var fecha_operacion=$('#fecha_operacion').val();
  if(fecha_pago_liqui==fecha_operacion){ 
  }else{
    swal("¡Atención!", "La última fecha de liquidación no coincide con la fecha de operación o acto.", "error");
  }
}
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
function btn_folio_ayuda(){
  $('#modal_folio_ayuda').modal();
}
// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}
// < / > // 
/*
$("#monto_a").on('change',function(){
    //console.log("cambia monto");
    var min = true;
    var addtp = 0; i=0;
    $(".monto_a").each(function() {
        totalmonto = replaceAll($(this).val(), ",", "" );
        totalmonto = replaceAll(totalmonto, "$", "" );
        //totalmonto = Math.trunc(totalmonto);
        var vstotal = totalmonto;
        
        vstotal
        addtp += Number(vstotal);
        //addtp_min = $(".porcentaje_b");
    });
    //var comparara_ddtp = addtp;
    //var comparara_monto_opera= $('#monto_opera').val();
    //alert(comparara_monto_opera);
    //var monto = parseFloat($("#monto").val()).toFixed(2);
    totalmontov = replaceAll($("#monto_a").val(), ",", "" );
    totalmontov = replaceAll(totalmontov, "$", "" );
    //totalmontov = Math.trunc(totalmontov);

    var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalmontov);
    //console.log("monto: "+monto);
    $("#monto_a").val(monto);
    //console.log("addtp: "+addtp);
    
    //var totalg=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(addtp);
    //$("#total_liquida").val(totalg);
    //tipoPagoNvo();
  });
*/  