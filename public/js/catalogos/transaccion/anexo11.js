var base_url = $('#base_url').val();
var id_aux = $('#id').val();
var tipo_actividad_aux=0;
var lim_efec_band=false;
var umas_anexo=0;
var suma_efect=0;
var siempre_avi;
var valor_uma=0;
$(document).ready(function(){
  var status = $('.status').text(); 
  setTimeout(function () { 
    if(status==1){
      $("select").prop('disabled', true);
      $("checkbox").prop('disabled', true);
      $("input").prop('disabled', true);
      $("textarea").prop('disabled', true);
      //$("button").remove();
      $("button").prop('disabled', true);
      $(".regresar").prop('disabled', false);
      $(".confirm").prop('disabled', false);
      $("#btn_submit").attr('disabled', true); 
    }
  }, 2500); 
	ComboAnio();
  tipo_modeda(1);
  if($('#mes_aux').val()==''){
    $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
  }else{
    $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
  }
  $('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");
  //======
  var aux_tipo_actividad = $('#aux_tipo_actividad').val();
  //tipo_actividad_select(aux_tipo_actividad); Porque existe ? 
  //======
  getpais(1,1);
  tipo_persona_opt(1);
  datos_instrumento_publico_opt1();
  tipo_persona_opt(2);
  getpais(2,2);
  tipo_activo_opt3();
  tipo_persona_moral_opt4();
  //agregar_accionistas_socios4();
  var aux_cc4=$('#aux_cc4').val();
  //alert(aux_cc4);
  if(aux_cc4==1){
    tabla_anexo11_constitucion_personas_morales_socios4(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios4();
  } 

  tipo_persona_opt(5);
  getpais(5,5);
  aportacion_monetaria_opt5();
  getpais(6,6);
  //agregar_accionistas_socios6();
  var aux_cc6=$('#aux_cc6').val();
  //alert(aux_cc4);
  if(aux_cc6==1){
    tabla_anexo11_constitucion_personas_morales_socios6(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios6();
  } 

  getpais(7,7);
  //agregar_accionistas_socios7();
  var aux_cc7=$('#aux_cc7').val();
  if(aux_cc7==1){
    tabla_anexo11_constitucion_personas_morales_socios7(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios7();
  } 
  //agregar_accionistas_socios27();
  var aux_cc72=$('#aux_cc72').val();
  if(aux_cc72==1){
    tabla_anexo11_constitucion_personas_morales_socios72(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios72();
  } 
  tipo_persona_opt(8);
  getpais(8,8);
  //agregar_accionistas_socios9();
  var aux_cc9=$('#aux_cc9').val();
  if(aux_cc9==1){
    tabla_anexo11_constitucion_personas_morales_socios9(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios9();
  } 
  aportacion_monetaria_opt9();
  //agregar_accionistas_socios29();
  var aux_cc92=$('#aux_cc92').val();
  if(aux_cc92==1){
    tabla_anexo11_constitucion_personas_morales_socios92(id_aux,$("#aviso").val());
  }else{
    agregar_accionistas_socios92();
  } 
  getpais(9,9);
  getpais(10,10);
  tipo_persona_opt(10);
  MostrarSeccion();
  setTimeout(function () {  
    valorUma();
  }, 1500);
  
  $("#fecha_operacion").on("change",function(){
    valorUma();
    if(status==0 && $("#aviso").val()>0)
      verificaFechas();
  });
  if(status==0 && $("#aviso").val()>0)
    verificaFechas();

  /* ************OUTSOURCING*********** */
  $("#tipo_area_servicio").on("change",function(){
    if(this.value=="99"){
      $("#descripcion4_div").show("slow");
    }else{
      $("#descripcion4_div").hide("slow"); 
    }
  });
  $("#tipo_activo_admin").on("change",function(){
    if(this.value=="99"){
      $("#descripcion4_2_div").show("slow");
    }else{
      $("#descripcion4_2_div").hide("slow"); 
    }
  });
  if($("#id").val()>0){
    divOutsourcingOtro();
  }
  $("#ocupacion").on("change",function(){
    showDescOcupa(this.value);
  });
  showDescOcupa($("#ocupacion option:selected").val());
  /* ********************************* */

});

function showDescOcupa(val){
  if(val=="99"){
    $("#cont_otra_ocupa").show("slow");
  }else{
    $("#desc_otra_ocupa").val("");
    $("#cont_otra_ocupa").hide("slow"); 
  }
}

function divOutsourcingOtro(){
  if($("#tipo_area_servicio").val()=="99"){
    $("#descripcion4_div").show("slow");
  }else{
    $("#descripcion4_div").hide("slow"); 
  }

  if($("#tipo_activo_admin").val()=="99"){
    $("#descripcion4_2_div").show("slow");
  }else{
    $("#descripcion4_2_div").hide("slow"); 
  }
}

function verificaFechas(){
  var fecha_act = moment($(".fecha_actual").text());
  var fecha_ope = moment($("#fecha_operacion").val());
  //console.log(fecha_act.diff(fecha_ope, 'days'), ' dias de diferencia');
  var dif_day = fecha_act.diff(fecha_ope, 'days');
  if(parseInt(dif_day)>30)
    swal("Alerta!", "La transacción que va a registrar tiene un plazo mayor a 30 días", "warning");
}

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function verificarConfigLimite(band_efe=0){
  $.ajax({
    type: 'POST',
    url : base_url+'Configuracion/getConfigAct',
    data: {anexo: $("#id_actividad").val()},
    success: function(result){
      var permite = result;
      console.log("permite: "+permite);
      if(permite=="1" && $('.status').text()==0){
        band_limite_config=true;
        $("#btn_submit").attr("disabled",false);
      }else if(permite==0 && band_efe==0 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",false);
        //swal("¡Alerta!", "No se permite continuar, sí se llegar a superar el limite de efectivo permitido", "error");
      }else if(permite==0 && band_efe==1 && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No se permite continuar, se supera limite de efectivo permitido", "error");
      }
      else if(permite=="n" && $('.status').text()==0){
        band_limite_config=false;
        $("#btn_submit").attr("disabled",true);
        swal("¡Alerta!", "No existe configuración para continuar en caso de superar limite de efectivo permitido", "error");
      }
    }
  });
}

function valorUma(){
  $.ajax({
    type: 'POST',
    url : base_url+'Umas/getUmasAnio',
    data: { anio: $("#fecha_operacion").val()},
    success: function(data2){
      valor_uma=data2;
      valor_uma=parseFloat(valor_uma); 
      //console.log("valor_uma ajax: "+valor_uma);
    }
  });
}

function ValidCurp(T){
  let ex=/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
  valor=T.value;
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "CURP No valido", "error");
    }
  }
}

function ValidRfc(T){
  let ex=/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;
  valor=T.value;
  if (valor!=""){
    valid=valor.match(ex);
    if (valid){
      console.log("valido");
    }else{
      swal("¡Error!", "RFC No valido", "error");
    }
  }
}

function ValidCapital(T){
  valor=T.value;
  if (valor!=""){
    if (valor.length>=4 && valor.length<=17){
      console.log("valido");
    }else{
      swal("¡Error!", "Informacion No valida", "error");
    }
  }
}

$("#DataOpcion").change(function(event){
  MostrarSeccion();
});

function MostrarSeccion(){
  op=$("#DataOpcion").val();
  //console.log("opcion: "+op);
  tipo_actividad_aux=op;
  switch (op) {
      case "1":
      $('.tipo_actividadtxt1').css('display','block');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "2":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','block');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "3":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','block');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "4":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','block');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "5":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','block');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "6":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','block');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "7":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','block');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "8":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','block');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "9":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','block');
      $('.tipo_actividadtxt10').css('display','none');
      break;
      case "10":
      $('.tipo_actividadtxt1').css('display','none');
      $('.tipo_actividadtxt2').css('display','none');
      $('.tipo_actividadtxt3').css('display','none');
      $('.tipo_actividadtxt4').css('display','none');
      $('.tipo_actividadtxt5').css('display','none');
      $('.tipo_actividadtxt6').css('display','none');
      $('.tipo_actividadtxt7').css('display','none');
      $('.tipo_actividadtxt8').css('display','none');
      $('.tipo_actividadtxt9').css('display','none');
      $('.tipo_actividadtxt10').css('display','block');
      break;
    default:
      break;
  }
}

function tipo_modeda(aux){
  var mon=$('.monto_'+aux).val();
  nvo = replaceAll(mon, ",", "" );
  nvo = replaceAll(nvo, "$", "" );
  //nvo = Math.trunc(nvo);
  var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo);
  $(".monto_"+aux+"").val(monto);
}

function tipo_persona_opt(id){
  if($('#tipo_persona_'+id+'_1').is(':checked')){
    $('.tipo_persona_txt_'+id+'_1').css('display','block');
  }else{
    $('.tipo_persona_txt_'+id+'_1').css('display','none');
  }
  if($('#tipo_persona_'+id+'_2').is(':checked')){
    $('.tipo_persona_txt_'+id+'_2').css('display','block');
  }else{
    $('.tipo_persona_txt_'+id+'_2').css('display','none');
  }
  if($('#tipo_persona_'+id+'_3').is(':checked')){
    $('.tipo_persona_txt_'+id+'_3').css('display','block');
  }else{
    $('.tipo_persona_txt_'+id+'_3').css('display','none');
  }
}
function datos_instrumento_publico_opt1(){
  if($('#datos_instrumento_publico1').is(':checked')){
    $('.datos_instrumento_publicotxt1').css('display','block');
  }else{
    $('.datos_instrumento_publicotxt1').css('display','none');
  }
  if($('#datos_instrumento_publico2').is(':checked')){
    $('.datos_instrumento_publicotxt2').css('display','block');
  }else{
    $('.datos_instrumento_publicotxt2').css('display','none');
  }
}
function tipo_activo_opt3(){
	if($('#tipo_activo3_1').is(':checked')){
	    $('.tipo_activo_txt3_1').css('display','block');
	}else{
	    $('.tipo_activo_txt3_1').css('display','none');
	}
	if($('#tipo_activo3_2').is(':checked')){
	    $('.tipo_activo_txt3_2').css('display','block');
	}else{
	    $('.tipo_activo_txt3_2').css('display','none');
	}
	if($('#tipo_activo3_3').is(':checked')){
	    $('.tipo_activo_txt3_3').css('display','block');
	}else{
	    $('.tipo_activo_txt3_3').css('display','none');
	}
  if($('#tipo_activo3_4').is(':checked')){
    $('.tipo_activo_txt3_4').css('display','block');
  }else{
    $('.tipo_activo_txt3_4').css('display','none');
  }
}
function getpais(txt,id){
  //console.log("get pais");
  $('.pais_'+txt+'_'+id).select2({
      width: '350px',
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Transaccion/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}

function addPagosData(id){
  var DATA = [];
  var TABLA = $("#table_pays tr");
  TABLA.each(function() {
    item = {};
    item["id_anexo11"] = id;
    item["aviso"] = $("#aviso").val();
    item["id"] = $(this).find("input[id*='id_pago']").val();
    item["fecha"] = $(this).find("input[id*='fecha']").val();
    item["instrumento_monetario3_4"] = $(this).find("select[id*='instrumento_monetario3_4'] option:selected").val();
    item["moneda3_4"] = $(this).find("select[id*='moneda3_4'] option:selected").val();
    item["monto_operacion3_4"] = $(this).find("input[id*='monto_operacion3_4']").val();
    if ($(this).find("input[id*='monto_operacion3_4']").val()!= "") {
      DATA.push(item);
    }
  });
  
  INFO = new FormData();
  aInfo = JSON.stringify(DATA);
  INFO.append('data', aInfo);
  //console.log("aInfo: "+aInfo);
  $.ajax({ 
    data: INFO,
    type: 'POST',
    url: base_url+'Transaccion/insertPagosA11',
    processData: false,
    contentType: false,
    async:false,
    success: function(data) {
      //console.log(data);
    }
  });
}

var cont_pay=0;
function addPago(){
  var html='<tr id="tr_pays_'+cont_pay+'">\
              <td width="20%">\
                <div class="col-md-12 form-group">\
                  <input type="hidden" id="id_pago" value="0">\
                  <label>Fecha</label>\
                  <input class="form-control" type="date" id="fecha" value="">\
                </div>\
              </td>\
              <td width="25%">\
                <div class="col-md-12 form-group">\
                  <label>Instrumento monetario con el que se realizó la aportación</label>\
                  <select class="form-control" id="instrumento_monetario3_4">\
                    <option value="1">Efectivo</option>\
                    <option value="2">Tarjeta de Crédito</option>\
                    <option value="3">Tarjeta de Debito</option>\
                    <option value="4">Tarjeta de Prepago</option>\
                    <option value="5">Cheque Nominativo</option>\
                    <option value="6">Cheque de Caja</option>\
                    <option value="7">Cheques de Viajero</option>\
                    <option value="8">Transferencia Interbancaria</option>\
                    <option value="9">Transferencia Misma Institución</option>\
                    <option value="10">Transferencia Internacional</option>\
                    <option value="11">Orden de Pago</option>\
                    <option value="12">Giro</option>\
                    <option value="13">Oro o Platino Amonedados</option>\
                    <option value="14">Plata Amonedada</option>\
                    <option value="15">Metales Preciosos</option>\
                    <option value="16">Activos Virtuales</option>\
                    <option value="17">Otros</option>\
                </select>\
                </div>\
              </td>\
              <td width="25%">\
                <div class="col-md-12 form-group">\
                  <label>Tipo de moneda o divisa de la aportación</label>\
                  <select class="form-control" id="moneda3_4"> \
                    <option value="0">Pesos Mexicanos</option>\
                    <option value="1">Dólar Americano</option>\
                    <option value="2">Euro</option>\
                    <option value="3">Libra Esterlina</option>\
                    <option value="4">Dólar canadiense</option>\
                    <option value="5">Yuan Chino</option>\
                    <option value="6">Centenario</option>\
                    <option value="7">Onza de Plata</option>\
                    <option value="8">Onza de Oro</option>\
                  </select>\
                </div>\
              </td>\
              <td width="25%">\
                <div class="col-md-12 form-group">\
                  <label>Monto de la aportación</label>\
                  <input class="form-control" type="number" id="monto_operacion3_4" value="">\
                </div> \
              </td>\
              <td width="5%">\
                <div class="col-md-1 form-group">\
                  <label style="color: transparent;">agregar</label>\
                  <button type="button" onclick="deletePago(0,'+cont_pay+')" class="btn gradient_nepal2"><i class="fa fa-trash-o"></i></button>\
                </div> \
              </td>\
            </tr>';
  $("#body_pays").append(html);
}

function deletePago(i,id){
  if(i>0){
    title = "¿Desea eliminar este registro?";
    swal({
        title: title,
        text: "Se eliminará el pago!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type:'POST',
          url: base_url+'Transaccion/eliminarPagoAnexo11',
          data:{ id: id, tipo:$("#aviso").val()},
          success:function(data){
            $("#tr_each_"+id).remove();
            swal("Éxito", "Pago eliminado correctamente", "success");
          }
        }); //cierra ajax
      }
    });
  }else{
    $("#tr_pays_"+id).remove();
  }
}

function tipo_persona_moral_opt4(){
	var opt = $('#tipo_persona_moral4 option:selected').val();
	if(opt==99){
	    $('.tipo_persona_moraltxt4').css('display','block');
	}else{
	    $('.tipo_persona_moraltxt4').css('display','none');
	}
}
//====== ACtividad 4======
function agregar_accionistas_socios4(){
  add_accionistas_socios4(0,'','','','','','','','','','','','','','','','','','','');
}
var aux_accs=0;
function add_accionistas_socios4(id,cargo_accionista,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //alert(pais);
  ///////////////////////////////////
  var cargo_accionista1=''; var cargo_accionista2=''; var cargo_accionista3=''; var cargo_accionista4=''; var cargo_accionista5='';
  if(cargo_accionista==1){
    cargo_accionista1='selected'; cargo_accionista2=''; cargo_accionista3=''; cargo_accionista4=''; cargo_accionista5='';
  }else if(cargo_accionista==2){
    cargo_accionista1=''; cargo_accionista2='selected'; cargo_accionista3=''; cargo_accionista4=''; cargo_accionista5='';
  }else if(cargo_accionista==3){
    cargo_accionista1=''; cargo_accionista2=''; cargo_accionista3='selected'; cargo_accionista4=''; cargo_accionista5='';
  }else if(cargo_accionista==4){
    cargo_accionista1=''; cargo_accionista2=''; cargo_accionista3=''; cargo_accionista4='selected'; cargo_accionista5='';
  }else if(cargo_accionista==5){
    cargo_accionista1=''; cargo_accionista2=''; cargo_accionista3=''; cargo_accionista4=''; cargo_accionista5='selected';
  }
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios4_'+aux_accs+'">\
              <input class="form-control" type="hidden" id="id4" value="'+id+'">';
     html+='<div class="row">\
              <div class="col-md-6 form-group">\
                <label>Cargo del accionista dentro de la persona moral</label>\
                <select class="form-control" id="cargo_accionista4">\
                  <option value="1" '+cargo_accionista1+'>Administrador único</option>\
                  <option value="2" '+cargo_accionista2+'>Presidente del consejo de administración u órgano equivalente</option>\
                  <option value="3" '+cargo_accionista3+'>Otro miembro del consejo de administración u órgano equivalente</option>\
                  <option value="4" '+cargo_accionista4+'>Comisario o Miembro del consejo de vigilancia u órgano equivalente</option>\
                  <option value="5" '+cargo_accionista5+'>No aplica</option>\
                </select>\
              </div>\
            </div>\
            <h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_personas4_'+aux_accs+'" id="tipo_persona1_4_'+aux_accs+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(4,'+aux_accs+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_personas4_'+aux_accs+'" id="tipo_persona2_4_'+aux_accs+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(4,'+aux_accs+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona4" name="tipo_personas4_'+aux_accs+'" id="tipo_persona3_4_'+aux_accs+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(4,'+aux_accs+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_4_'+aux_accs+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre4" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno4" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno4" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento4" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc4" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp4" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad4 pais_4_'+aux_accs+'" id="pais_nacionalidad4_'+aux_accs+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_4_'+aux_accs+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm4" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm4" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm4" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm4 pais_4_'+aux_accs+'" id="pais_nacionalidad_accms_'+aux_accs+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_4_'+aux_accs+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf4" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf4" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof4" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o certificados de aportación</label>\
                  <input class="form-control" type="number" id="numero_accionesf4" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accs==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios4()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn gradient_nepal2" onclick="remover_anexo11_constitucion_personas_morales_socios4('+aux_accs+','+id+','+aviso_tipo+')"><i class="fa fa-trash-o"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios').append(html);
  tipo_personav_select(4,aux_accs);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  getpais_socios(4,aux_accs); 
  aux_accs++;   
}
function tabla_anexo11_constitucion_personas_morales_socios4(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo11_act4_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios4(element.id,
                                        element.cargo_accionista,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios4();
            }   
        }
    });
}
function remover_anexo11_constitucion_personas_morales_socios4(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo11_act4_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios4_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios4_'+aux).remove();
  }
}
//====== ACtividad 5======
function aportacion_monetaria_opt5(){
  if($('#aportacion_monetaria5_1').is(':checked')){
      $('.aportacion_monetariatxt5_1').css('display','block');
  }else{
      $('.aportacion_monetariatxt5_1').css('display','none');
  }
  if($('#aportacion_monetaria5_2').is(':checked')){
      $('.aportacion_monetariatxt5_2').css('display','block');
  }else{
      $('.aportacion_monetariatxt5_2').css('display','none');
  }
  if($('#aportacion_monetaria5_3').is(':checked')){
      $('.aportacion_monetariatxt5_3').css('display','block');
  }else{
      $('.aportacion_monetariatxt5_3').css('display','none');
  }
}
//====== ACtividad 6======
function agregar_accionistas_socios6(){
  add_accionistas_socios6(0,'','','','','','','','','','','','','','','','','','');
}

var aux_accsa6=0;
function add_accionistas_socios6(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  //alert(aux_accsa6);
  var aviso_tipo=$("#aviso").val();
  //alert(pais);
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  //alert(aux_accs6);
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios6_'+aux_accsa6+'">\
              <input class="form-control" type="hidden" id="id6" value="'+id+'">';
     html+='<h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona6" name="tipo_personas6_'+aux_accsa6+'" id="tipo_persona1_6_'+aux_accsa6+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(6,'+aux_accsa6+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona6" name="tipo_personas6_'+aux_accsa6+'" id="tipo_persona2_6_'+aux_accsa6+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(6,'+aux_accsa6+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona6" name="tipo_personas6_'+aux_accsa6+'" id="tipo_persona3_6_'+aux_accsa6+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(6,'+aux_accsa6+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_6_'+aux_accsa6+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre6" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno6" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno6" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento6" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc6" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp6" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad6 pais_6_'+aux_accsa6+'" id="pais_nacionalidad6_'+aux_accsa6+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_6_'+aux_accsa6+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm6" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm6" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm6" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm6 pais_6_'+aux_accsa6+'" id="pais_nacionalidad_accms_'+aux_accsa6+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_6_'+aux_accsa6+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf6" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf6" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof6" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf6" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accsa6==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios6()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo11_constitucion_personas_morales_socios6('+aux_accsa6+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios6').append(html);
  getpais_socios(6,aux_accsa6); 
  tipo_personav_select(6,aux_accsa6);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_accsa6++;   
}
function tabla_anexo11_constitucion_personas_morales_socios6(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo11_act6_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios6(element.id,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios6();
            }   
        }
    });
}
function remover_anexo11_constitucion_personas_morales_socios6(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo11_act6_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios6_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios6_'+aux).remove();
  }
}
/*
function getpais_socios_acc(txt,id){
  //console.log("get pais");
  //alert('.pais_'+txt+'_'+id);
  $('.pais_'+txt+'_'+id).select2({
      width: '350px',
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Transaccion/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
*/
//====== ACtividad 7======
function agregar_accionistas_socios7(){
  add_accionistas_socios7(0,'','','','','','','','','','','','','','','','','','','');
}
var aux_accs7=0;
function add_accionistas_socios7(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios7_'+aux_accs7+'">\
              <input class="form-control" type="hidden" id="id7" value="'+id+'">';
     html+='<h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona7" name="tipo_personas7_'+aux_accs7+'" id="tipo_persona1_7_'+aux_accs7+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(7,'+aux_accs7+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona7" name="tipo_personas7_'+aux_accs7+'" id="tipo_persona2_7_'+aux_accs7+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(7,'+aux_accs7+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona7" name="tipo_personas7_'+aux_accs7+'" id="tipo_persona3_7_'+aux_accs7+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(7,'+aux_accs7+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_7_'+aux_accs7+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre7" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno7" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno7" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento7" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc7" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp7" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad7 pais_7_'+aux_accs7+'" id="pais_nacionalidad7_'+aux_accs7+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_7_'+aux_accs7+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm7" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm7" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm7" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm7 pais_7_'+aux_accs7+'" id="pais_nacionalidadm7_'+aux_accs7+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_7_'+aux_accs7+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf7" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf7" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof7" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf7" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accs7==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios7()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo11_constitucion_personas_morales_socios7('+aux_accs7+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios7').append(html);
  getpais_socios(7,aux_accs7); 
  tipo_personav_select(7,aux_accs7);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_accs7++;   
}
function tabla_anexo11_constitucion_personas_morales_socios7(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo11_act7_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios7(element.id,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios7();
            }   
        }
    });
}
function remover_anexo11_constitucion_personas_morales_socios7(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo11_act7_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios7_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios7_'+aux).remove();
  }
}
//===
function agregar_accionistas_socios72(){
  add_accionistas_socios72(0,'','','','','','','','','','','','','','','','','','');
}
var aux_accs72=0;
function add_accionistas_socios72(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //alert(pais);
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios72_'+aux_accs72+'">\
              <input class="form-control" type="hidden" id="id72" value="'+id+'">';
     html+='<h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona72" name="tipo_personas72_'+aux_accs72+'" id="tipo_persona1_72_'+aux_accs72+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(72,'+aux_accs72+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona72" name="tipo_personas72_'+aux_accs72+'" id="tipo_persona2_72_'+aux_accs72+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(72,'+aux_accs72+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona72" name="tipo_personas72_'+aux_accs72+'" id="tipo_persona3_72_'+aux_accs72+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(72,'+aux_accs72+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_72_'+aux_accs72+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre72" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno72" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno72" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento72" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc72" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp72" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad72 pais_72_'+aux_accs72+'" id="pais_nacionalidad72_'+aux_accs72+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_72_'+aux_accs72+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm72" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm72" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm72" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm72 pais_72_'+aux_accs72+'" id="pais_nacionalidadm72_'+aux_accs72+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_72_'+aux_accs72+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf72" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf72" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof72" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf72" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accs72==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios72()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo11_constitucion_personas_morales_socios72('+aux_accs72+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios72').append(html);
  getpais_socios(72,aux_accs72); 
  tipo_personav_select(72,aux_accs72);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_accs72++;   
}
function tabla_anexo11_constitucion_personas_morales_socios72(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo11_act72_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios72(element.id,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios72();
            }   
        }
    });
}
function remover_anexo11_constitucion_personas_morales_socios72(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo11_act72_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios72_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios72_'+aux).remove();
  }
}
//==
function getpais_socios(txt,id){
  //console.log("get pais");
  $('.pais_'+txt+'_'+id).select2({
      width: '350px',
      minimumInputLength: 3,
      minimumResultsForSearch: 10,
      placeholder: 'Buscar un país',
      ajax: {
          url: base_url + 'Transaccion/searchpais',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.clave,
                      text: element.pais,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}
//====== ACtividad 9======
function agregar_accionistas_socios9(){
  add_accionistas_socios9(0,'','','','','','','','','','','','','','','','','','');
}
var aux_accs9=0;
function add_accionistas_socios9(id,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios9_'+aux_accs9+'">\
              <input class="form-control" type="hidden" id="id9" value="'+id+'">';
     html+='<h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona9" name="tipo_personas9_'+aux_accs9+'" id="tipo_persona1_9_'+aux_accs9+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(9,'+aux_accs9+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona9" name="tipo_personas9_'+aux_accs9+'" id="tipo_persona2_9_'+aux_accs9+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(9,'+aux_accs9+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona9" name="tipo_personas9_'+aux_accs9+'" id="tipo_persona3_9_'+aux_accs9+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(9,'+aux_accs9+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_9_'+aux_accs9+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre9" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno9" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno9" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento9" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc9" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp9" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad9 pais_9_'+aux_accs9+'" id="pais_nacionalidad9_'+aux_accs9+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_9_'+aux_accs9+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm9" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm9" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm9" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm9 pais_9_'+aux_accs9+'" id="pais_nacionalidadm9_'+aux_accs9+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_9_'+aux_accs9+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf9" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf9" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof9" value="'+identificador_fideicomisof+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accs9==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios9()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo11_constitucion_personas_morales_socios9('+aux_accs9+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios9').append(html);
  getpais_socios(9,aux_accs9); 
  tipo_personav_select(9,aux_accs9);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_accs9++;   
}
function tabla_anexo11_constitucion_personas_morales_socios9(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo11_act9_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios9(element.id,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios9();
            }   
        }
    });
}
function remover_anexo11_constitucion_personas_morales_socios9(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo11_act9_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios9_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios9_'+aux).remove();
  }
}
//====== ACtividad 92======
function agregar_accionistas_socios92(){
  add_accionistas_socios92(0,'','','','','','','','','','','','','','','','','','','','');
}
var aux_accs92=0;
function add_accionistas_socios92(id,datos_fideicomisarios_determinados,tipo_persona,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,rfc,curp,pais_nacionalidad,pais,denominacion_razonm,fecha_constitucionm,rfcm,pais_nacionalidadm,paism,denominacion_razonf,rfcf,identificador_fideicomisof,numero_accionesf){
  /////////////////////////////////////////////////////////////////////////////////
  var aviso_tipo=$("#aviso").val();
  //
  var datos_fideicomisarios_determinadostxt1=''; var datos_fideicomisarios_determinadostxt2='';
  if(datos_fideicomisarios_determinados=='SI'){
    datos_fideicomisarios_determinadostxt1='checked'; datos_fideicomisarios_determinadostxt2='';
  }else if(datos_fideicomisarios_determinados=='NO'){
    datos_fideicomisarios_determinadostxt1=''; datos_fideicomisarios_determinadostxt2='checked';
  }
  //////////////////////////////////
  var tipo_persona1=''; var tipo_persona2=''; var tipo_persona3='';
  if(tipo_persona==1){
    tipo_persona1='checked'; tipo_persona2=''; tipo_persona3='';
  }else if(tipo_persona==2){
    tipo_persona1=''; tipo_persona2='checked'; tipo_persona3='';
  }else if(tipo_persona==3){
    tipo_persona1=''; tipo_persona2=''; tipo_persona3='checked';
  }
  /////////////////////////////////////////////////////////////////////////////////
  var html='<div class="constitucion_accionistas_socios92_'+aux_accs92+'">\
              <input class="form-control" type="hidden" id="id92" value="'+id+'">';
     html+='<div class="row">\
              <div class="col-md-12">\
                <label>Los datos del o los fideicomisarios se encuentran determinados</label>\
              </div>\
            </div>\
            <div class="row"> \
              <div class="col-md-1 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input datos_fideicomisarios_determinados92" name="datos_fideicomisarios_determinados92" value="SI" '+datos_fideicomisarios_determinadostxt1+'>\
                    SI\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-1 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input datos_fideicomisarios_determinados92" name="datos_fideicomisarios_determinados92" value="NO" '+datos_fideicomisarios_determinadostxt2+'>\
                   NO\
                  </label>\
                </div>\
              </div>\
            </div>\
            <h5>Tipo de Persona</h5>\
            <div class="row">\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona92" name="tipo_personas92_'+aux_accs92+'" id="tipo_persona1_92_'+aux_accs92+'" value="1" '+tipo_persona1+' onclick="tipo_personav_select(92,'+aux_accs92+')">\
                    Persona Fisica\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group">\
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona92" name="tipo_personas92_'+aux_accs92+'" id="tipo_persona2_92_'+aux_accs92+'" value="2" '+tipo_persona2+' onclick="tipo_personav_select(92,'+aux_accs92+')">\
                    Persona Moral\
                  </label>\
                </div>\
              </div>\
              <div class="col-md-2 form-group"> \
                <div class="form-check form-check-success">\
                  <label class="form-check-label">\
                    <input type="radio" class="form-check-input tipo_persona92" name="tipo_personas92_'+aux_accs92+'" id="tipo_persona3_92_'+aux_accs92+'" value="3" '+tipo_persona3+' onclick="tipo_personav_select(92,'+aux_accs92+')">\
                    Fideicomiso\
                  </label>\
                </div>\
              </div>\
            </div>\
            <div class="tipo_personatxt1_92_'+aux_accs92+'" style="display: none">\
              <hr>\
              <div class="row">\
                <div class="col-md-4 form-group">\
                  <label>Nombre(s)</label>\
                  <input class="form-control" type="text" id="nombre92" value="'+nombre+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Paterno</label>\
                  <input class="form-control" type="text" id="apellido_paterno92" value="'+apellido_paterno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Apellido Materno</label>\
                  <input class="form-control" type="text" id="apellido_materno92" value="'+apellido_materno+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label><i class="fa fa-calendar"></i> Fecha de Nacimiento</label>\
                  <input class="form-control" type="date" id="fecha_nacimiento92" value="'+fecha_nacimiento+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfc92" value="'+rfc+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave Única de Registro de Población (CURP)</label>\
                  <input class="form-control" type="text" id="curp92" value="'+curp+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidad92 pais_92_'+aux_accs92+'" id="pais_nacionalidad92_'+aux_accs92+'">';
                    if(pais_nacionalidad!=''){
                html+='<option value="'+pais_nacionalidad+'">'+pais+'</option>';
                    } 
           html+='</select>\
                </div>\
              </div>\
              <hr>  \
            </div>\
            <div class="tipo_personatxt2_92_'+aux_accs92+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-5 form-group">\
                  <label>Denominación o Razón Social</label>\
                  <input class="form-control" type="text" id="denominacion_razonm92" value="'+denominacion_razonm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Fecha de Constitución</label>\
                  <input class="form-control" type="date" id="fecha_constitucionm92" value="'+fecha_constitucionm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC)</label>\
                  <input class="form-control" type="text" id="rfcm92" value="'+rfcm+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Clave país nacionalidad</label>\
                  <select class="form-control pais_nacionalidadm92 pais_92_'+aux_accs92+'" id="pais_nacionalidadm92_'+aux_accs92+'">';
                    if(pais_nacionalidadm!=''){
                html+='<option value="'+pais_nacionalidadm+'">'+paism+'</option>';
                    }
           html+='</select>\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="tipo_personatxt3_92_'+aux_accs92+'" style="display: none;">\
              <hr>\
              <div class="row">\
                <div class="col-md-7 form-group">\
                  <label>Denominación o Razón Social del Fiduciario</label>\
                  <input class="form-control" type="text" id="denominacion_razonf92" value="'+denominacion_razonf+'">\
                </div>\
                <div class="col-md-5 form-group">\
                  <label>Registro Federal de Contribuyentes (RFC) del Fideicomiso</label>\
                  <input class="form-control" type="text" id="rfcf92" value="'+rfcf+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número, referencia o identificador del fideicomiso</label>\
                  <input class="form-control" type="text" id="identificador_fideicomisof92" value="'+identificador_fideicomisof+'">\
                </div>\
                <div class="col-md-4 form-group">\
                  <label>Número de acciones o partes sociales</label>\
                  <input class="form-control" type="number" id="numero_accionesf92" value="'+numero_accionesf+'">\
                </div>\
              </div> \
              <hr> \
            </div>\
            <div class="row">\
                <div class="col-md-12" align="right">\
                  <label style="color: transparent;">agregar liquidación</label>';
                        if(aux_accs92==0){
                      html+='<button type="button" class="btn gradient_nepal2" onclick="agregar_accionistas_socios92()"><i class="fa fa-plus"></i> Agregar otro socio</button>'; 
                        }else{
                      html+='<button type="button" class="btn btn_borrar" onclick="remover_anexo11_constitucion_personas_morales_socios92('+aux_accs92+','+id+','+aviso_tipo+')"><i class="fa fa-minus"></i> Quitar socio</button>';
                        }
          html+='</div>\
              </div><hr>';
   html+='</div>';
  $('.accionistas_socios92').append(html);
  getpais_socios(92,aux_accs92); 
  tipo_personav_select(92,aux_accs92);
  $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  aux_accs92++;   
}
function tabla_anexo11_constitucion_personas_morales_socios92(id,aviso){
    $.ajax({
        type:'POST',
        url: base_url+"Transaccion/get_anexo11_act92_constitucion_personas_morales_socios",
        data: {id:id,aviso:aviso},
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element){
                  add_accionistas_socios92(element.id,
                                        element.datos_fideicomisarios_determinados,
                                        element.tipo_persona,
                                        element.nombre,
                                        element.apellido_paterno,
                                        element.apellido_materno,
                                        element.fecha_nacimiento,
                                        element.rfc,
                                        element.curp,
                                        element.pais_nacionalidad,
                                        element.pais,
                                        element.denominacion_razonm,
                                        element.fecha_constitucionm,
                                        element.rfcm,
                                        element.pais_nacionalidadm,
                                        element.paism,
                                        element.denominacion_razonf,
                                        element.rfcf,
                                        element.identificador_fideicomisof,
                                        element.numero_accionesf);
                });
            }else{
                agregar_accionistas_socios92();
            }   
        }
    });
}
function remover_anexo11_constitucion_personas_morales_socios92(aux,id,aviso){
  if(id!=0){
      title = "¿Desea eliminar este registro?";
      swal({
          title: title,
          text: "Se eliminará el registro!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Transaccion/eliminar_anexo11_act92_constitucion_personas_morales_socios',
            data:{id:id,aviso:aviso},
            success:function(data){
              setTimeout(function () {  
                swal("Éxito", "Pago eliminado correctamente", "success");
              }, 1000);
              $('.constitucion_accionistas_socios92_'+aux).remove();
            }
          }); //cierra ajax
        }
      });
  }else{
    $('.constitucion_accionistas_socios92_'+aux).remove();
  }
}

//====== ACtividad 9======
function aportacion_monetaria_opt9(){
  if($('#aportacion_monetaria9_1').is(':checked')){
      $('.aportacion_monetariatxt9_1').css('display','block');
  }else{
      $('.aportacion_monetariatxt9_1').css('display','none');
  }
  if($('#aportacion_monetaria9_2').is(':checked')){
      $('.aportacion_monetariatxt9_2').css('display','block');
  }else{
      $('.aportacion_monetariatxt9_2').css('display','none');
  }
  if($('#aportacion_monetaria9_3').is(':checked')){
      $('.aportacion_monetariatxt9_3').css('display','block');
  }else{
      $('.aportacion_monetariatxt9_3').css('display','none');
  }
}
//============================
function tipo_personav_select(txt,id){
  if($('#tipo_persona1_'+txt+'_'+id).is(':checked')){
    $('.tipo_personatxt1_'+txt+'_'+id).css('display','block');
  }else{
    $('.tipo_personatxt1_'+txt+'_'+id).css('display','none');
  }
  if($('#tipo_persona2_'+txt+'_'+id).is(':checked')){
    $('.tipo_personatxt2_'+txt+'_'+id).css('display','block');
  }else{
    $('.tipo_personatxt2_'+txt+'_'+id).css('display','none');
  }
  if($('#tipo_persona3_'+txt+'_'+id).is(':checked')){
    $('.tipo_personatxt3_'+txt+'_'+id).css('display','block');
  }else{
    $('.tipo_personatxt3_'+txt+'_'+id).css('display','none');
  }
}
//=======================================================
function registrar(){
  var form_register = $('#form_anexo0,#frm1');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        if($("#ocupacion option:selected").val()=="99"){
          $('#form_anexo0').removeData('validator');
          var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
                referencia:{
                  required: true
                },
                ocupacion:{
                  required: true
                },
                desc_otra_ocupa:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        }else{
          $('#form_anexo0').removeData('validator');
          var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_operacion:{
                  required: true
                },
                monto_opera:{
                  required: true
                },
                referencia:{
                  required: true
                },
                ocupacion:{
                  required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        }
        
        //////////Registro///////////
        var valid = $("#form_anexo0").valid();
        var band_aviso = 0; var val_area_serv=0; var val_tipo_act=0; var msj="";
        if(tipo_actividad_aux==3){ //admin de activos
          if($("#tipo_activo3_4").is(":checked")==true){ //outsourcing
            if($("#tipo_area_servicio").val()=="99" && $("#descripcion4").val().trim()==""){
              val_area_serv++;
              msj = swal("Álerta!", "Ingrese descripción de otra área de servicio", "error");
            }
            if($("#tipo_activo_admin").val()=="99" && $("#desc_otro_activo_admin").val().trim()==""){
              val_tipo_act++;
              msj = swal("Álerta!", "Ingrese descripción de otro activo administrado", "error");
            }
          }
        }
        if(valid && val_area_serv==0 && val_tipo_act==0) {
          monto_oper = replaceAll($("#monto_opera").val(), ",", "" );
          monto_opera = replaceAll(monto_oper, "$", "" );
          if($("#aviso").val()>0){
            name_table = "anexo11_aviso";
          }else{
            name_table = "anexo11";
          }

          if($("#aviso").val()>0){
            if($("#referencia_modifica").val()!="" && $("#folio_modificatorio").val()!="" && $("#descrip_modifica").val()!=""){
              band_aviso=1;
            }
          }

          var datos_anexo = form_register.serialize()+"&tabla="+name_table+"&monto_opera="+monto_opera;
          if($("#aviso").val()>0 && band_aviso==1 || $("#aviso").val()==0){
            $.ajax({
              type:'POST',
              url: base_url+'Transaccion/registro_anexo11',
              data:datos_anexo,
              statusCode:{
                  404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                  },
                  500: function(){
                      swal("Error!", "500", "error");
                  }
              },
              beforeSend: function(){
                $("#btn_submit").attr("disabled",true);
              },
              success:function(data){
                var array = $.parseJSON(data);
                var id=array.id; var monto=parseFloat(array.monto); var id_clientec_transac=array.id_clientec_transac;
                //===================
                swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                //==
                if(tipo_actividad_aux==3 && $("#tipo_activo3_4").is(":checked")==true){
                  addPagosData(id);
                }
                if(tipo_actividad_aux==4){
                  var form_act=$('#form_actividad'+tipo_actividad_aux+',#form_actividad41').serialize()+"&idanexo11="+id+"&aviso="+$("#aviso").val(); 
                }else if(tipo_actividad_aux==7){
                  var form_act=$('#form_actividad'+tipo_actividad_aux+',#form_actividad71').serialize()+"&idanexo11="+id+"&aviso="+$("#aviso").val(); 
                }else if(tipo_actividad_aux==9){
                  var form_act=$('#form_actividad'+tipo_actividad_aux+',#form_actividad91,#form_actividad92').serialize()+"&idanexo11="+id+"&aviso="+$("#aviso").val(); 
                }else{
                  var form_act=$('#form_actividad'+tipo_actividad_aux).serialize()+"&idanexo11="+id+"&aviso="+$("#aviso").val(); 
                }

                $.ajax({
                  type:'POST',
                  url: base_url+'Transaccion/registro_anexo11_actividad'+tipo_actividad_aux,
                  data:form_act,
                  statusCode:{
                    404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                      swal("Error!", "500", "error");
                    }
                  },
                  success:function(data){
                  }
                });  
                if(tipo_actividad_aux==4){
                  var DATA4= [];
                  var TABLA4 = $(".row_accionistas_socios .accionistas_socios > div");                  
                  TABLA4.each(function(){         
                        item = {};
                        item ["idanexo11"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id4']").val();
                        item ["cargo_accionista"]=$(this).find("select[id*='cargo_accionista4']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona4']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre4']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno4']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno4']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento4']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc4']").val();
                        item ["curp"]=$(this).find("input[id*='curp4']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad4']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm4']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm4']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm4']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm4']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf4']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf4']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof4']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf4']").val();
                        DATA4.push(item);
                  });
                  INFO4  = new FormData();
                  aInfo4   = JSON.stringify(DATA4);
                  INFO4.append('data', aInfo4);
                  $.ajax({
                      data: INFO4,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo11_act4constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                }else if(tipo_actividad_aux==6){
                  var DATA6= [];
                  var TABLA6 = $(".row_accionistas_socios6 .accionistas_socios6 > div");                  
                  TABLA6.each(function(){         
                        item = {};
                        item ["idanexo11"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id6']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona6']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre6']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno6']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno6']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento6']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc6']").val();
                        item ["curp"]=$(this).find("input[id*='curp6']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad6']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm6']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm6']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm6']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm6']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf6']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf6']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof6']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf6']").val();
                        DATA6.push(item);
                  });
                  INFO6  = new FormData();
                  aInfo6   = JSON.stringify(DATA6);
                  INFO6.append('data', aInfo6);
                  $.ajax({
                      data: INFO6,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo11_act6constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                }else if(tipo_actividad_aux==7){
                  var DATA7= [];
                  var TABLA7 = $(".row_accionistas_socios7 .accionistas_socios7 > div");                  
                  TABLA7.each(function(){         
                        item = {};
                        item ["idanexo11"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id7']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona7']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre7']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno7']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno7']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento7']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc7']").val();
                        item ["curp"]=$(this).find("input[id*='curp7']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad7']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm7']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm7']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm7']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm7']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf7']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf7']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof7']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf7']").val();
                        DATA7.push(item);
                  });
                  INFO7  = new FormData();
                  aInfo7   = JSON.stringify(DATA7);
                  INFO7.append('data', aInfo7);
                  $.ajax({
                      data: INFO7,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo11_act7constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                  //
                  var DATA72= [];
                  var TABLA72 = $(".row_accionistas_socios72 .accionistas_socios72 > div");                  
                  TABLA72.each(function(){         
                        item = {};
                        item ["idanexo11"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id72']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona72']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre72']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno72']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno72']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento72']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc72']").val();
                        item ["curp"]=$(this).find("input[id*='curp72']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad72']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm72']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm72']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm72']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm72']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf72']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf72']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof72']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf72']").val();
                        DATA72.push(item);
                  });
                  INFO72  = new FormData();
                  aInfo72   = JSON.stringify(DATA72);
                  INFO72.append('data', aInfo72);
                  $.ajax({
                      data: INFO72,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo11_act72constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  }); 
                }else if(tipo_actividad_aux==9){
                  var DATA9= [];
                  var TABLA9 = $(".row_accionistas_socios9 .accionistas_socios9 > div");                  
                  TABLA9.each(function(){         
                        item = {};
                        item ["idanexo11"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id9']").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona9']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre9']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno9']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno9']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento9']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc9']").val();
                        item ["curp"]=$(this).find("input[id*='curp9']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad9']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm9']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm9']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm9']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm9']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf9']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf9']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof9']").val();
                        DATA9.push(item);
                  });
                  INFO9  = new FormData();
                  aInfo9   = JSON.stringify(DATA9);
                  INFO9.append('data', aInfo9);
                  $.ajax({
                      data: INFO9,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo11_act9constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  });
                  //
                  var DATA92= [];
                  var TABLA92 = $(".row_accionistas_socios92 .accionistas_socios92 > div");                  
                  TABLA92.each(function(){         
                        item = {};
                        item ["idanexo11"]=id;
                        item ["aviso"]=$("#aviso").val();

                        item ["id"]=$(this).find("input[id*='id92']").val();
                        item ["datos_fideicomisarios_determinados"]=$(this).find("input:radio[class*='datos_fideicomisarios_determinados92']:checked").val();
                        item ["tipo_persona"]=$(this).find("input:radio[class*='tipo_persona92']:checked").val();
                        item ["nombre"]=$(this).find("input[id*='nombre92']").val();
                        item ["apellido_paterno"]=$(this).find("input[id*='apellido_paterno92']").val();
                        item ["apellido_materno"]=$(this).find("input[id*='apellido_materno92']").val();
                        item ["fecha_nacimiento"]=$(this).find("input[id*='fecha_nacimiento92']").val();

                        item ["rfc"]=$(this).find("input[id*='rfc92']").val();
                        item ["curp"]=$(this).find("input[id*='curp92']").val();
                        item ["pais_nacionalidad"]=$(this).find("select[class*='pais_nacionalidad92']").val();

                        item ["denominacion_razonm"]=$(this).find("input[id*='denominacion_razonm92']").val();
                        item ["fecha_constitucionm"]=$(this).find("input[id*='fecha_constitucionm92']").val();
                        item ["rfcm"]=$(this).find("input[id*='rfcm92']").val();
                        item ["pais_nacionalidadm"]=$(this).find("select[class*='pais_nacionalidadm92']").val();

                        item ["denominacion_razonf"]=$(this).find("input[id*='denominacion_razonf92']").val();
                        item ["rfcf"]=$(this).find("input[id*='rfcf92']").val();
                        item ["identificador_fideicomisof"]=$(this).find("input[id*='identificador_fideicomisof92']").val();
                        item ["numero_accionesf"]=$(this).find("input[id*='numero_accionesf92']").val();
                        DATA92.push(item);
                  });
                  INFO92  = new FormData();
                  aInfo92   = JSON.stringify(DATA92);
                  INFO92.append('data', aInfo92);
                  $.ajax({
                      data: INFO92,
                      type: 'POST',
                      url : base_url+'index.php/Transaccion/inset_anexo11_act92constitucion_personas_morales_accionistas_socios',
                      processData: false, 
                      contentType: false,
                      async: false,
                      statusCode:{
                          404: function(data2){
                              //toastr.error('Error!', 'No Se encuentra el archivo');
                          },
                          500: function(){
                              //toastr.error('Error', '500');
                          }
                      },
                      success: function(data2){
                        
                      }
                  });
                }  

                var anio = $("#anio").val();
                var band_umbral=false;
                var addtp=0; var addtpD=0;
                var umas_anexo=0;
                $.ajax({
                  type: 'POST',
                  url : base_url+'Umbrales/getUmbral',
                  data: {anexo: $("#id_actividad").val()},
                  success: function(result){
                    //var array = $.parseJSON(result);
                    var array= JSON.parse(result);
                    var aviso = parseFloat(array[0].aviso).toFixed(2);
                    var siempre_avi=array[0].siempre_avi;
                    
                    /*console.log("valor_uma: "+valor_uma);
                    console.log("aviso: "+aviso);
                    console.log("siempre_avi: "+siempre_avi);*/
                    if(siempre_avi=="1"){
                      band_umbral=true;
                      $.ajax({ //cambiar pago a acusado
                        type: 'POST',
                        url : base_url+'Operaciones/acusaPago2',
                        async: false,
                        data: { id:id, act:$("#id_actividad").val()},
                        success: function(result_divi){

                        }
                      });
                    }else{
                      umas_anexo=monto/valor_uma;
                      umas_anexo = parseFloat(umas_anexo).toFixed(2);
                      //console.log("valor de umas_anexo final: "+umas_anexo);
                      //console.log("valor de umas aviso: "+aviso);
                      //console.log("siempre_avi: "+siempre_avi);
                      umas_anexo = umas_anexo*1;
                      aviso = aviso*1;
                      if(umas_anexo>=aviso){
                        band_umbral=true;
                        $.ajax({ //cambiar pago a acusado
                        type: 'POST',
                        url : base_url+'Operaciones/acusaPago2',
                        async: false,
                        data: { id:id, act:$("#id_actividad").val()},
                        success: function(result_divi){

                        }
                      });
                      }
                    }
                    //console.log("band_umbral: "+band_umbral);

                    /*if (suma==monto){
                      validar_xml=1;
                    }else{
                      validar_xml=0;
                    }*/
                    if(band_umbral==true && band_umbral!=undefined && !isNaN(umas_anexo)){

                      if($("#aviso").val()>0){
                        window.location.href = base_url+"Transaccion/anexo11_xml/"+id+"/"+$('#id_perfilamiento').val()+"/1/0/"+$("input[name*='idopera']").val()+"/0";
                      }else{
                        window.location.href = base_url+"Transaccion/anexo11_xml/"+id+"/"+$('#id_perfilamiento').val()+"/0/0/"+$("input[name*='idopera']").val()+"/0";
                      }
                      swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                      /*if(history.back()!=undefined){
                        setTimeout(function () { history.back() }, 1500);
                      }*/
                      if($("#aviso").val()>0){
                        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                      }else{
                        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                      }
                    }else{
                      swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                      /*if(history.back()!=undefined){
                        setTimeout(function () { history.back() }, 1500);
                      }*/
                      if($("#aviso").val()>0){
                        setTimeout(function () {  window.location.href = base_url+"Estadisticas/bitacora_transacciones" }, 1500);
                      }else{
                        setTimeout(function () {  window.location.href = base_url+"Operaciones" }, 1500);
                      }
                    }
                  }//success  
                });  //ajax
              }  
            });  
          }//band aviso
          else{
            swal("Error!", "Los campos de Aviso son obligatorios", "error");
          }
        }
        else{
          msj;
        }
}

//=======================================================
function regresar(){
  //history.back();
  if(history.back()!=undefined)
    setTimeout(function () { history.back() }, 1500);
  else
    window.location.href = base_url+"Operaciones/procesoInicial/"+$("input[name*='idopera']").val();
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
////////////////////// Operaciones
function btn_fecha_opera(){
  $('#modal_fecha_opera').modal();
}
function btn_referencia_ayuda(){
  $('#modal_referencia_ayuda').modal();
}
function btn_factura_ayuda(){
  $('#modal_no_factura_ayuda').modal();
}
function btn_folio_ayuda(){
  $('#modal_folio_ayuda').modal();
}
function btn_foliomer_ayuda(){
  $('#modal_foliomer_ayuda').modal();
}
function btn_NRIF_ayuda(){
  $('#modal_NRIF_ayuda').modal(); 
}
function btn_NIPFE_ayuda() {
  $('#modal_NIPFE_ayuda').modal();
}
function btn_nom_inst_ayuda(){
  $('#modal_nom_insti_ayuda').modal();
}
function btn_num_cta_ayuda(){
  $('#modal_numcta__ayuda').modal(); 
}
function btn_MCV_ayuda(){
  $('#modal_MCV_ayuda').modal();
}
function btn_CS_ayuda(){
  $('#modal_CS_ayuda').modal();
}
function btn_DRS_ayuda(){
  $('#modal_DRS_ayuda').modal();
}
function btn_DTA_ayuda(){
  $('#modal_DTA_ayuda').modal();
}
function btn_DDBA_ayuda(){
  $('#modal_DDBA_ayuda').modal();
}
function btn_DDTAYO_ayuda(){
  $('#modal_DDTAYO_ayuda').modal();
}
/// < / >

function cambiaCP(id){
  cp = $("#"+id+"").val();  
  if(cp!="" && id=="codigo_postal1"){
    col = "colonia1";
  }
  else if(cp!="" && id=="codigo_postal2"){
    col="colonia2";
  }
  else if(cp!="" && id=="codigo_postal3"){
    col="colonia3";
  }

  var itemc = [];
  $('#colonia_dir').select2({
    placeholder: {
      id: 0,
      text: 'Buscar colonia:'
    },
    width: '100%',
    allowClear: true,
    data: itemc
  });
  autoComplete(col);

  /*console.log("cp cambiaCP: "+cp);
  console.log("col cambiaCP: "+col);*/
}
function autoComplete(idcol){
  if(idcol=="colonia1"){
    cp = $("#codigo_postal1").val(); 
    col = $('#colonia').val();  
  }
  else if(idcol=="colonia2"){
    cp = $("#codigo_postal2").val(); 
    col = $('#colonia2').val();  
  }
  else if(idcol=="colonia3"){
    cp = $("#codigo_postal3").val(); 
    col = $('#colonia3').val();  
  }
  /*console.log("cp: "+cp);
  console.log("col: "+col);*/

  $('#'+idcol+'').select2({
      width: 'resolve',
      minimumInputLength: 0,
      minimumResultsForSearch: 10,
      placeholder: 'Seleccione una colonia:',
      ajax: {
          url: base_url+'Perfilamiento/getDatosCPSelect',
          dataType: "json",
          data: function(params) {
              var query = {
                  search: params.term,
                  cp: cp, 
                  col: col ,
                  type: 'public'
              }
              return query;
          },
          processResults: function(data) {
              var itemscli = [];
              data.forEach(function(element) {
                  itemscli.push({
                      id: element.colonia,
                      text: element.colonia,
                  });
              });
              return {
                  results: itemscli
              };
          },
      }
  });
}

// Aviso modificatorio //
function btn_referenciam_ayuda(){
  $('#modal_referenciam_ayuda').modal();
}
function btn_foliom_ayuda(){
  $('#modal_foliom_ayuda').modal();
}