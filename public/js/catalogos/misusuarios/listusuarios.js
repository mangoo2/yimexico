var base_url = $('#base_url').val();
$(document).ready(function($) {
    /// Tabla de clientes
    tabla_load_usuarios();
});
function tabla_load_usuarios(){
    tabla=$("#table_usuario").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
           "url": base_url+"MisUsuarios/getlistado_usuarios",
           type: "post",
            error: function(){
               $("#table_usuario").css("display","none");
            }
        },
        "columns": [
            {"data": "idcliente_usuario"},
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var html_usuario='';
                           html_usuario=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;      
                    return html_usuario;
                }
            },
            {"data": "funcion_puesto"},
            {"data": null,
                "render": function (data, type, row, meta ) {
                      var html='';          
                    //var html= '<a class="btn " title="Editar" style="color: #2b254e;"><i class="fa fa-eye"></i> Actividades</a>'
                    return html;
                } 
            },
            {"data": null,
                "render": function ( data, type, row, meta ) {
                    var html='';
                    //html+='<button type="button" class="btn" style="color: #212b4c;" title="Ver listado actividad: Último"><i class="fa fa-clock-o" style="font-size: 22px;"></i></button>\
                    html+='<a class="btn" style="color: #212b4c;" title="Editar" style="color: white;" href="'+base_url+'MisUsuarios/addusuarios_cliente/'+row.idcliente_usuario+'"><i class="fa fa-edit" style="font-size: 22px;"></i></a>\
                          <button type="button" class="btn" style="color: #212b4c;" title="Cancelar" onclick="modaleliminar('+row.idcliente_usuario+')"><i class="fa fa-trash" style="font-size: 22px;"></i></button>';        
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 25, 50], [5, 25, 50]],
    });
}
function inicio_cliente(){
	location.href= base_url+'Inicio_cliente';
}
function modaleliminar(id) {
    $('#modal_eliminar').modal();
    $('#idcliente_usuario_e').val(id);
}
function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'MisUsuarios/updateregistro',
        data:{id:$('#idcliente_usuario_e').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                tabla.ajax.reload();
            }, 1500);
        }
    });     
}
function regresar_view(){
    window.history.back();
}