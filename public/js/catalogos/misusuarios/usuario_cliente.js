var base_url = $('#base_url').val();
var verifi_usuario=0;
var verifi_pass=0;
var valid_save_menu=0; var valid_save_submenu=0;
var contmenus=0;
$(document).ready(function() {
    getpais('u',1);
    pais_verifica();
    /*
    var idcliente_usuario = $('#idcliente_usuario').val();
    if(idcliente_usuario>0){
      tabla_usuario_menu_detalles(idcliente_usuario);
    }
    */
    setTimeout(function(){
      if($("#idcliente_usuario").val()==0 || $('#UsuarioID').val()==0){
        $("#Usuario").val("");
        $("#contrasena").val("");
      }
    }, 1500);
    $(".campo").on("change",function(){
      preGuardar();
    });
    $(".campos").on("change",function(){
      preGuardar();
    });

    /*$(".sub_menu_checkx_2").on("click",function(){
      validaChecksDepen();
    });*/
    if($("#idcliente_usuario").val()>0 || $('#UsuarioID').val()>0){
      validaPermisoValida();
    }
});

function validaChecksDepen(id_menu,id_sub,act_band){
  //console.log("valida cheks");
  var dato_msm=$(".submen_"+id_menu+"_"+id_sub+"").data("msb");
  //console.log("dato_msm: "+dato_msm);
  if(dato_msm=="15_2" && $(".submen_"+id_menu+"_"+id_sub+"").is(":checked")==true && act_band==1 || dato_msm=="16_2" && $(".submen_"+id_menu+"_"+id_sub+"").is(":checked")==true && act_band==1){
    $(".check_menux_4").attr("checked",true);
    $(".submen_9_4").attr("checked",true);
    $(".submen_9_4").attr("disabled",true);
    vizualizar_submenu(4);
    $(".submen_10_4").attr("checked",false);
  }if($(".submen_15_2").is(":checked")==false && $(".submen_16_2").is(":checked")==false && act_band==1){
    $(".submen_9_4").attr("disabled",false);
    $(".submen_9_4").attr("checked",false);
    $(".submen_10_4").attr("checked",false);
    
    //$(".check_menux_4").prop("checked",false);
  }
  if(id_menu==16 && id_sub==2){
    if($(".submen_9_4").is(":checked")==false){
      swal("Alerta!", "Para validar expendientes debe activar Nueva Operación, dentro del menú Operaciones", "error");
      $(".submen_16_2").prop("checked",false);
    }
  }
}

function validaPermisoValida(){
  if($(".submen_9_4").is(":checked")==false){
    swal("Alerta!", 'Para validar expendientes debe activar "Nueva Operación", dentro del menú "Operaciones"', "error");
    $(".submen_16_2").prop("checked",false);
  }else{
    $(".check_menux_4").prop("checked",true);
    $(".submen_9_4").prop("checked",true);
  }
}

function preGuardar(){
  var datos_usuario = $('#form_usuario_cliente').serialize();
  $.ajax({
    type:'POST',
    url: base_url+'MisUsuarios/registro_usuario_cliente',
    data: datos_usuario,
    statusCode:{
      404: function(data){
          swal("Error!", "No Se encuentra el archivo", "error");
      },
      500: function(){
          swal("Error!", "500", "error");
      }
    },
    success:function(data){
      var array = $.parseJSON(data);
      if (array.status==1) {
        //swal("Éxito", "Guardado Correctamente", "success");
      }else if (array.status==2) {
        //swal("Éxito", "Actualizado Correctamente", "success");
      }
      var idcli=array.id;
      $("#idcliente_usuario").val(idcli);
      
      //===============
      /*if($("#Usuario").val()!="" && $("#contrasena").val()!=""){
        guardar_menu_usuario(idcli);
        $.ajax({
          type:'POST',
          url: base_url+'MisUsuarios/registro_usuario_cliente_usuario',
          data: {UsuarioID:$('#UsuarioID').val(),
                Usuario:$('#Usuario').val(),
                perfilId:$('#perfilId').val(),
                idsucursal:$('#idsucursal').val(),
                contrasena:$('#contrasena').val(),
                idcliente_usuario:idcli
          },
          statusCode:{
              404: function(data){
                //swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                //swal("Error!", "500", "error");
              }
          },
          success:function(data){
          }  
        });  
      }*/
      //===============
      /*setTimeout(function(){
          location.href= base_url+'MisUsuarios';
      }, 1500);*/
    }  

  });  
}
function guardar_usuario(){
    //////////VAlidacion///////////
    var form_register = $('#form_usuario_cliente');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            apellido_paterno:{
              required: true
            },
            apellido_materno:{
              required: true
            },
            funcion_puesto:{
              required: true
            },
            Usuario:{
              required: true
            },
            perfilId:{
              required: true
            },
            contrasena:{
              required: true
            },

        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var valid = $("#form_usuario_cliente").valid();
    if(valid) {
        if(verifi_usuario==1){
          swal("Error!", "El usuario ya existe.", "error");
        }else{
          if(verifi_pass==1 || $('#contrasena').val().length<6){
            swal("Error!", "Por favor, introduzca el mismo valor de nuevo y/o contraseña de 6 caracteres minimo.", "error");
          }else{
            var datos_usuario = $('#form_usuario_cliente').serialize();
            $.ajax({
              type:'POST',
              url: base_url+'MisUsuarios/registro_usuario_cliente',
              data: datos_usuario,
              statusCode:{
                404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                  swal("Error!", "500", "error");
                }
              },
              success:function(data){
                var array = $.parseJSON(data);
                if (array.status==1) {
                  swal("Éxito", "Guardado Correctamente", "success");
                }else if (array.status==2) {
                  swal("Éxito", "Actualizado Correctamente", "success");
                }
                var idcli=array.id;
                guardar_menu_usuario(idcli);
                //===============

                $.ajax({
                  type:'POST',
                  url: base_url+'MisUsuarios/registro_usuario_cliente_usuario',
                  data: {UsuarioID:$('#UsuarioID').val(),
                        Usuario:$('#Usuario').val(),
                        //perfilId:$('#perfilId').val(),
                        perfilId:7,
                        idsucursal:$('#idsucursal').val(),
                        contrasena:$('#contrasena').val(),
                        idcliente_usuario:idcli
                  },
                  statusCode:{
                    404: function(data){
                      swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                      swal("Error!", "500", "error");
                    }
                  },
                  success:function(data){
                  }  
                });  
                //===============
                setTimeout(function(){
                  if(valid_save_menu==1 && valid_save_submenu==1){
                    location.href= base_url+'MisUsuarios';
                  }
                }, 1500);
              }  

            });  
            ///
            
          } 
        }  
        /*
        
        */
    }
}
function getpais(text,id){
    var estado_text = $('#estado_text').val();
    $('.idpais_'+text+'_'+id).select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Mis_datos/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
      var pais = $('#pais option:selected').val();       
      if(pais=='MX'){
       //<!-------->
        $.ajax({
          type:'POST',
          url: base_url+'MisUsuarios/get_estados',
          statusCode:{
              404: function(data){
                  swal("Error!", "No Se encuentra el archivo", "error");
              },
              500: function(){
                  swal("Error!", "500", "error");
              }
          },
          success:function(data){
            $('.span_div').html(data);
          }  
        });
       //<!-------->   
    }else{
      $('.span_div').html('<input class="form-control" type="text" name="estado" value="'+estado_text+'">'); 
    }
    setTimeout(function(){
      $('#estado option[value="'+estado_text+'"]').attr("selected", "selected");
    }, 1500);
    });
}
/// Verificar que no exista el usuario
function verificauser(){
    $.ajax({
        type:'POST',
        url: base_url+'MisUsuarios/verificauser',
        data:{user:$('#Usuario').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                $('.vefi_iser').html('El usuario ya existe.');
                verifi_usuario=1;
            }else{
                $('.vefi_iser').html('');
                verifi_usuario=0;
            }
        }
    }); 
}
function verificarpass(){
    var pass1=$('#contrasena').val();
    var pass2=$('#contrasenav').val();
    if(pass1!=pass2){
      $('.text_validar_pass').html('Por favor, introduzca el mismo valor de nuevo.');
      verifi_pass=1;
    }else{
      $('.text_validar_pass').html('');
      verifi_pass=0;
    }
}
function pais_verifica(){
  var estado_text = $('#estado_text').val();
  var pais = $('#pais option:selected').val();       
  if(pais=='MX'){
  //<!-------->
    $.ajax({
      type:'POST',
      url: base_url+'MisUsuarios/get_estados',
      statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
      },
      success:function(data){
        $('.span_div').html(data);
      }  
    });
  //<!-------->   
  }else{
    $('.span_div').html('<input class="form-control" type="text" name="estado" value="'+estado_text+'">'); 
  }
  setTimeout(function(){
    $('#estado option[value="'+estado_text+'"]').attr("selected", "selected");
  }, 1500);
}

function vizualizar_submenu(id){
  if($('.check_menux_'+id).is(':checked')){
    $('.sub_menu_permiso_'+id).css('display','block');
    $('.sub_menu_checkx_'+id).prop('checked',true);
  }else{
    $('.sub_menu_permiso_'+id).css('display','none');
    $('.sub_menu_checkx_'+id).prop('checked',false);
  }
  if(id==2 && $('.check_menux_'+id).is(':checked')==true){
    $('.check_menux_4').prop('checked',true);
    $('.sub_menu_permiso_4').css('display','block');
    $('.sub_menu_checkx_4').prop('checked',true);
  }else if(id==2 && $('.check_menux_'+id).is(':checked')==false){
    $('.check_menux_4').prop('checked',false);
    $('.sub_menu_permiso_4').css('display','none');
    $('.sub_menu_checkx_4').prop('checked',false);
  }
}

/*
function agregar_menu(){
  var id_menu=$('#menu_permiso option:selected').val();
  var id_menutxt=$('#menu_permiso option:selected').text();
  add_menu(0,id_menu,id_menutxt);
}

var cont_menu=0;
function add_menu(id,idm,nombre){
  var id_menu=$('#menu_permiso option:selected').val();
  var id_menutxt=$('#menu_permiso option:selected').text();
  var html='';
  html+='<tr class="row_menu_'+cont_menu+'">\
            <td><input type="hidden" id="idpermisox" value="'+id+'">\
                <input type="hidden" id="id_menux" value="'+idm+'">\
                '+nombre+'</td>\
            <td><button type="button" class="btn btn_borrar btn-rounded btn-icon" onclick="remove_menu('+cont_menu+','+id+')">\
                  <i class="fa fa-trash-o"></i>\
                </button></td>\
          </tr>';
  $('#menu_tbody').append(html);     
  cont_menu++;
}
var id_permiso=0;
var cont_permiso=0;
function remove_menu(cont,id){
  if(id==0){
    $('.row_menu_'+cont).remove();
  }else{
    $('#modal_eliminar_menu').modal();
    id_permiso=id;
    cont_permiso=cont;
  }
}
function delete_registro(){
    var id=id_permiso;
    $.ajax({
        type:'POST',
        url: base_url+'MisUsuarios/delete_menu_registro',
        data: {id:id},
        async: false,
        statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
        },
        success:function(data){
          swal("Éxito", "Eliminado Correctamente", "success");
          $('#modal_eliminar_menu').modal('hide');
          $('.row_menu_'+cont_permiso).remove();
        }
    });
}
*/
function guardar_menu_usuario(id){
  contmenus=0;
  var DATAP= [];
  var TABLAP = $(".tabla_usuario_permiso .tabla_usuario_permiso2 > .menu_usuario");                  
  TABLAP.each(function(){
    var chek_m=0;
    if($(this).find("input[class*='check_menux']").is(':checked')){    
      chek_m=1;
      contmenus++;
    }else{
      chek_m=0;
    }  
    item = {};
    item ["idcliente_usuario"] = id;
    item ["idpermiso"] = $(this).find("input[id*='idpermisox']").val();
    item ["id_menu"] = $(this).find("input[id*='id_menux']").val();
    item ["check_menu"] = chek_m;
    DATAP.push(item);
  });
  INFOP  = new FormData();
  aInfop   = JSON.stringify(DATAP);
  INFOP.append('data', aInfop);
  $.ajax({
    data: INFOP,
    type: 'POST',
    url : base_url+'MisUsuarios/insert_menu',
    processData: false, 
    contentType: false,
    async: false,
    statusCode:{
      404: function(data2){
        //toastr.error('Error!', 'No Se encuentra el archivo');
      },
      500: function(){
        //toastr.error('Error', '500');
      }
    },
    success: function(data2){
      valid_save_menu=1;
      guardar_menu_usuario_sub(id);
    }
  });
}

function guardar_menu_usuario_sub(id){
  contmenus = contmenus-1;
  console.log("contmenus: "+contmenus);
  var DATASP= [];
  var cont_submenus=0;
  var TABLASP = $(".sub_menu_permiso .sub_menu_permiso2 > div");   
  TABLASP.each(function(){
    var chek_m=0;
    if($(this).find("input[class*='sub_menu_checkx']").is(':checked')){    
      chek_m=1;
      cont_submenus++;
    }else{
      chek_m=0;
    }  
    item = {};
    item ["idcliente_usuario"] = id;
    item ["idpermisosub"] = $(this).find("input[id*='idpermisosubx']").val();
    item ["id_menu"] = $(this).find("input[id*='id_menuxx']").val();
    item ["id_menusub"] = $(this).find("input[id*='id_menusubxx']").val();
    item ["check_menu"] = chek_m;
    DATASP.push(item);
    
  });
  INFOP  = new FormData();
  aInfop   = JSON.stringify(DATASP);
  INFOP.append('data', aInfop);
  /*console.log("INFOP: "+INFOP);
  console.log("aInfop: "+aInfop);
  console.log("DATASP: "+DATASP);*/
  console.log("cont_submenus: "+cont_submenus);
  if(cont_submenus>=contmenus){
    $.ajax({
      data: INFOP,
      type: 'POST',
      url : base_url+'MisUsuarios/insert_menu_sub',
      processData: false, 
      contentType: false,
      async: false,
      statusCode:{
        404: function(data2){
          //toastr.error('Error!', 'No Se encuentra el archivo');
        },
        500: function(){
          //toastr.error('Error', '500');
        }
      },
      success: function(data2){
        valid_save_submenu=1;
      }
    });
  }else{
    swal("Error!", "Debe elegir al menos una opción del submenú", "error");
  }
}

/*
function tabla_usuario_menu_detalles(id){
    $.ajax({
        type:'POST',
        url: base_url+"MisUsuarios/get_usuario_menu_detalles",
        data: {id:id},
        success:function(data){
            var array = $.parseJSON(data);
            if(array.length>0) {
                array.forEach(function(element){
                  add_menu(element.idpermiso,element.id_menu,element.nombre);
                });
            }   
        }
    });
}
*/

