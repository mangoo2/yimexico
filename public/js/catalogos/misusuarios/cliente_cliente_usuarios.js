var base_url = $('#base_url').val();
var verifi_pass;
$(document).ready(function($) {
	$('.guardar_registro').click(function(event) {
        //////////VAlidacion///////////
        var form_register = $('#form_usuario');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                contrasena:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var valid = $("#form_usuario").valid();
        if(valid) {
            if(verifi_pass==1){
              swal("Error!", "Por favor, introduzca el mismo valor de nuevo.", "error");
            }else{
                var datos_usuario = $('#form_usuario').serialize();
		        $.ajax({
		          type:'POST',
		          url: base_url+'Mis_datos_c_c_u/registro_clientes_c_usuario',
		          data: datos_usuario,
		          statusCode:{
		              404: function(data){
		                  swal("Error!", "No Se encuentra el archivo", "error");
		              },
		              500: function(){
		                  swal("Error!", "500", "error");
		              }
		          },
		          success:function(data){
                    if(data==1){
                        swal("¡Atención!", "La contraseña es igual a la anterior. Por favor, introduzca una diferente.", "error");
                    }else{
		              	swal("Éxito", "Actualizado Correctamente", "success");
		              	setTimeout(function(){
				         location.href= base_url+'Inicio_cliente';
				        }, 1500);
		            } 
                 }  
		        });  
		        ///
		        
      
            }
        }
    });
});
function verificarpass(){
    var pass1=$('#contrasena').val();
    var pass2=$('#contrasenav').val();
    if(pass1!=pass2){
      $('.text_validar_pass').html('Por favor, introduzca el mismo valor de nuevo.');
      verifi_pass=1;
    }else{
      $('.text_validar_pass').html('');
      verifi_pass=0;
    }
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}