var base_url = $('#base_url').val();
$(document).ready(function($) {
    
   ComboAno();
   $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
   setTimeout(function(){ load(); }, 3000);

    var idc= $('#idcliente').val();
    var idp= $('#idperfilamiento').val()
    $.ajax({
        type: "POST",
        url: base_url+"Operaciones/get_clientes_select",
        data:{idc:idc,idp:idp},
        success: function (data){
          $('.select_clientes').html(data);
        }
    });

});	  
function ComboAno(){
    var d = new Date();
    var n = d.getFullYear();
    var select = document.getElementById("ano");
    for(var i = n; i >= 1900; i--) {
        var opc = document.createElement("option");
        opc.text = i;
        opc.value = i;
        select.add(opc)
    }
}

function load(){  
    var anio = $('#ano option:selected').val();
    var mes = $('#mes option:selected').val(); 
    var cliente = $('#cliente option:selected').val();
    var actividad = $('#actividad option:selected').val();
    var id_pagoa=0;
    var tipo=3;
    $('.tabla_transaccion').html('<table class="table" id="table">\
            <thead>\
              <tr>\
                <th>No</th>\
                <th>Cliente</th>\
                <th>Fecha de Registro</th>\
                <!--<th>Resumen</th>\
                <th>Total de factura</th>-->\
                <th style="width: 205px;">Aviso 24 Hrs.</th>\
                <th>Acuse SAT PDF</th>\
                <th>Acuse SAT PDF</th>\
              </tr>\
            </thead>\
            <tbody>\
            </tbody>\
          </table>');
    table = $('#table').DataTable({
            "bProcessing": true,
            "serverSide": true,
            "ajax":{
                "url": base_url+"Estadisticas/getListado_transacion",
                type: "post",
                error: function(){
                    $("#table").css("display","none");
                }, 
                data:{anio:anio,mes:mes,cliente:cliente,actividad:actividad,tipo:tipo},
            },
            "columns": [
                    //{"data": "idctrans"},

                    //{"data": "idopera"},
                    {"data": "folio_cliente"},
                    {"data": "nombre",
                        "render": function ( data, type, row, meta ) {
                            //console.log("total: "+row.totalPagos);
                            var html_cliente='';
                                if(row.idtipo_cliente==1){
                                   html_cliente=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                                }else if(row.idtipo_cliente==2){
                                   html_cliente=row.nombre2+' '+row.apellido_paterno2+' '+row.apellido_materno2;
                                }else if(row.idtipo_cliente==3){
                                   html_cliente=row.razon_social;
                                }else if(row.idtipo_cliente==4){
                                   html_cliente=row.nombre_persona;
                                }else if(row.idtipo_cliente==5){
                                   html_cliente=row.denominacion2;
                                }else if(row.idtipo_cliente==6){
                                   html_cliente=row.denominacion;
                                }
                            //return html_cliente;
                            return row.cliente;

                        } 
                    },
                    {"data": "fecha_reg"}, 
                    //{"data": "transaccion"},
                    //{"data": "totalPagos"},
                    /*{"data": null,
                        "render": function ( url, type, full) {
                            if(actividad==15){
                                return rw="N/A";
                            }
                            else{
                                if(full['totalpagos']!=null){
                                    var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['totalpagos']);
                                }
                                else{
                                    rw="$"+0.00;
                                }
                            }   
                            return rw;
                        }
                    },*/
                    {"data": null,
                        "render": function ( data, type, row, meta ) {
                            var cm = "'";
                            var anexo = row.transaccion;
                            var ane = anexo.split('-');
                            var ruta = ane[0];
                            rr = ruta.replace(" ","");
                            rr = rr.toLowerCase();
                            rr = rr.replace(/ /g, "");  
                            var sta=0;
                            if(row.xml=="1"){ 
                                sta=1;
                            }
                            //console.log("row.xml: "+row.xml);
                            var html='';
                            
                            //GENERAR AVISOS 24 HORAS
                            if(actividad==14 /*|| actividad==16*/){  //
                                html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Descargar Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/0/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                            }else if(actividad!=14 /*|| actividad!=16*/){
                                html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Descargar Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/'+row.id_pago_ayuda+'/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                            }
                            id_pagoa = row.id_pago_ayuda;  
                            return html;
                         }//render
                    },//data
                    {"data": null,
                        "render": function ( data, type, row, meta ) {
                            //console.log("total: "+row.totalPagos);
                            var acuse='<div class="row">\
                                    <div class="col-md-10">\
                                      <label class="control-label">PDF DE ACUSE</label>\
                                      <div class="row row_file">\
                                          <div class="col-md-12">\
                                              <input width="50%" onchange="cargarDoc('+row.id_anexo_tabla+',this.value,this.id,this.name,this.files[0].size,'+row.id_acuse_24+','+row.id_actividad+',1)" class="doc '+row.id_anexo_tabla+'" id="xml_'+row.id_anexo_tabla+'" name="'+row.id_acuse_24+'" type="file">\
                                          </div>\
                                          <div class="col-md-12"></div>\
                                      </div>\
                                    </div>\
                                  </div>';
                                  acuseXML(row.id_acuse_24,row.acuse_24,row.id_anexo_tabla,1);
                            return acuse;
                        } 
                    },
                    {"data": null,
                    "render": function ( data, type, row, meta ) {
                        //console.log("total: "+row.totalPagos);
                        var acuse='<div class="row">\
                                <div class="col-md-10">\
                                  <label class="control-label">PDF DE ACUSE</label>\
                                  <div class="row row_file">\
                                      <div class="col-md-12">\
                                          <input width="50%" onchange="cargarDoc('+row.id_anexo_tabla+',this.value,this.id,this.name,this.files[0].size,'+row.id_acuse_24+','+row.id_actividad+',2)" class="doc '+row.id_anexo_tabla+'" id="pdf_'+row.id_anexo_tabla+'" name="'+row.id_acuse+'" type="file">\
                                      </div>\
                                      <div class="col-md-12"></div>\
                                  </div>\
                                </div>\
                              </div>';
                              acuseXML(row.id_acuse,row.acuse24_pdf,row.id_anexo_tabla,2);
                        return acuse;
                    } 
                }
                // window.location.href = base_url+"Transaccion/"+rr+"/"+data.id_clientec+"/"+data.id_actividad+"/"+data.id_perfilamiento+"/"+data.id_transaccion+"/"+data.idtransbene;    
            ],
            order: [[ 0, "desc" ]],
    });
    $(".table").addClass("table-striped table-bordered dt-responsive");
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}
function regresar_view(){
    window.history.back();
}

function acuseXML(id_acu,xml,id,tipo){
    //console.log("xml: "+xml);
    type="";
    if(xml!=""){
        if(tipo==1){ //xml
            prevx="true";
            inix= base_url+"uploads/acuses_generados/"+xml;
            defx= base_url+'uploads/acuses_generados/'+xml+'" ';
            //type="xml";
            type="pdf";
        }else{ //pdf
            prevx="true";
            inix= base_url+"uploads/acuses_generados/"+xml;
            defx= base_url+'uploads/acuses_generados/'+xml+'" ';
            type="pdf";
        }
    }else{
        prevx="";
        inix="";
        defx="";
    }
      //console.log("prevx: "+prevx);
    var name_input="";
    if(tipo==1){//xml{
        name_input="#xml_"+id+"";
    }else{
        name_input="#pdf_"+id+"";
    }
    $(name_input).fileinput({
        theme: 'explorer-fa5',
        overwriteInitial: true,
        maxFileSize: 5000,
        showClose: false,
        showCaption: false,
        showZomm: false,
        reversePreviewOrder: true,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: defx,
        initialPreviewDownloadUrl: inix,

        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: [""+type+""],
        initialPreview: [
          // IMAGE RAW MARKUP
          ""+inix+"",
        ],
        initialPreviewAsData: prevx,
        initialPreviewFileType: type, // image is the default and can be overridden in config below
        initialPreviewConfig: [
          {type: type, url: base_url+"Estadisticas/delete_acuse/"+id_acu+"/3/"+tipo+"", caption: xml, key: 1},
        ]
    });
    /*if(tipo==1)
        $(".kv-file-zoom").hide();*/
}

function cargarDoc(id_a8,val,id,name,size,id_acuse,id_actividad,tipo){
    filezise = size;                     
    if(filezise > 2048000) { // 512000 bytes = 500 Kb
      //console.log($('.img')[0].files[0].size);
      $(this).val('');
      swal("Error!", "El archivo supera el límite de peso permitido", "error");
    }else { //ok
        tama_perm = true; 
        var archivo = val;
        var id = id; //id con nombre de xml o pdf
        var name = name; //id solito - id de movimiento (saldo)

        extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
        /*if(tipo==1){
            extensiones_permitidas = new Array(".xml");
        }
        else
            extensiones_permitidas = new Array(".pdf");*/

        extensiones_permitidas = new Array(".pdf");

        permitida = false;
        if($('#'+id)[0].files.length > 0) {
            for (var i = 0; i < extensiones_permitidas.length; i++) {
              if (extensiones_permitidas[i] == extension) {
                permitida = true;
              break;
              }
            }  
            if(permitida==true && tama_perm==true){
              //console.log("tamaño permitido");
              var inputFileImage = document.getElementById(id);
              var file = inputFileImage.files[0];
              var data = new FormData();
              //id_docs = $("#id").val();
              data.append('foto',file);
              $.ajax({
                url:base_url+'Estadisticas/cargafiles/'+id_a8+'/'+id_actividad+'/'+id_acuse+"/3/"+tipo,
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                  var array = $.parseJSON(data);
                  //console.log(array.ok);
                  if (array.ok=true) {
                    id_docs = array.id;
                    load();
                    //console.log("id_docs: "+id_docs);
                    swal("Éxito", "Acuse cargado correctamente", "success");
                    $("#id_docs").val(id_docs);
                  }else{
                    swal("Error!", "No Se encuentra el archivo", "error");
                  }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                  var data = JSON.parse(jqXHR.responseText);
                }
              });
            }else if(permitida==false){
              swal("Error!", "Tipo de archivo no permitido", "error");
            } 
        }else{
            swal("Error!", "No se a selecionado un archivo", "error");
        } 
    }
}