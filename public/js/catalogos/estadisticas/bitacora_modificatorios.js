var base_url = $('#base_url').val();
$(document).ready(function($) {
    
   ComboAno();
   $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
   setTimeout(function(){ load(); }, 3000);

    var idc= $('#idcliente').val();
    var idp= $('#idperfilamiento').val()
    $.ajax({
        type: "POST",
        url: base_url+"Operaciones/get_clientes_select",
        data:{idc:idc,idp:idp},
        success: function (data){
          $('.select_clientes').html(data);
        }
    });

});	  
function ComboAno(){
    var d = new Date();
    var n = d.getFullYear();
    var select = document.getElementById("ano");
    for(var i = n; i >= 1900; i--) {
        var opc = document.createElement("option");
        opc.text = i;
        opc.value = i;
        select.add(opc)
    }
}

function load(){  
    var anio = $('#ano option:selected').val();
    var mes = $('#mes option:selected').val(); 
    var cliente = $('#cliente option:selected').val();
    var actividad = $('#actividad option:selected').val();
    var id_pagoa=0;
    var tipo=2;
    $('.tabla_transaccion').html('<table class="table" id="table">\
            <thead>\
              <tr>\
                <th>No</th>\
                <th>Cliente</th>\
                <th>Fecha de Operación</th>\
                <!--<th>Resumen</th>\
                <th>Total de factura</th>-->\
                <th style="width: 205px;">Aviso Modificatorio</th>  \
                <th>Acuse SAT PDF</th>\
                <th>Acuse SAT PDF</th>\
              </tr>\
            </thead>\
            <tbody>\
            </tbody>\
          </table>');
    table = $('#table').DataTable({
            "bProcessing": true,
            "serverSide": true,
            "ajax":{
                "url": base_url+"Estadisticas/getListado_transacion",
                type: "post",
                error: function(){
                    $("#table").css("display","none");
                }, 
                data:{anio:anio,mes:mes,cliente:cliente,actividad:actividad,tipo:tipo},
            },
            "columns": [
                    //{"data": "idctrans"},

                    //{"data": "idopera"},
                    {"data": "folio_cliente"},
                    {"data": "nombre",
                        "render": function ( data, type, row, meta ) {
                            //console.log("total: "+row.totalPagos);
                            var html_cliente='';
                                if(row.idtipo_cliente==1){
                                   html_cliente=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                                }else if(row.idtipo_cliente==2){
                                   html_cliente=row.nombre2+' '+row.apellido_paterno2+' '+row.apellido_materno2;
                                }else if(row.idtipo_cliente==3){
                                   html_cliente=row.razon_social;
                                }else if(row.idtipo_cliente==4){
                                   html_cliente=row.nombre_persona;
                                }else if(row.idtipo_cliente==5){
                                   html_cliente=row.denominacion2;
                                }else if(row.idtipo_cliente==6){
                                   html_cliente=row.denominacion;
                                }
                            //return html_cliente;
                            return row.cliente;

                        } 
                    },
                    {"data": "fecha_operacion"}, 
                    //{"data": "transaccion"},
                    //{"data": "totalPagos"},
                    /*{"data": null,
                        "render": function ( url, type, full) {
                            if(actividad==15){
                                return rw="N/A";
                            }
                            else{
                                if(full['totalpagos']!=null){
                                    var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['totalpagos']);
                                }
                                else{
                                    rw="$"+0.00;
                                }
                            }   
                            return rw;
                        }
                    },*/
                    {"data": null,
                        "render": function ( data, type, row, meta ) {
                            var cm = "'";
                            var anexo = row.transaccion;
                            var ane = anexo.split('-');
                            var ruta = ane[0];
                            rr = ruta.replace(" ","");
                            rr = rr.toLowerCase();
                            rr = rr.replace(/ /g, "");  
                            var sta=0;
                            if(row.xml=="1"){ 
                                sta=1;
                            }
                            //console.log("row.xml: "+row.xml);
                            var html='';
                            
                            // avisos //
                            if(actividad==11 && row.xml==1 && row.idaviso>0){ //xml aviso Vehículos 
                                html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo8_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            } 

                            if(actividad==1 && row.xml==1 && row.idaviso>0){  //xml normal 1 Juegos, apuestas y sorteos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo1_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==2 && row.xml==1 && row.idaviso>0){  //xml Anexo 2A - Emisión o comercialización de tarjetas prepagadas o el abono a instrumentos de almacenamiento de valor monetario.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo2a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==3 && row.xml==1 && row.idaviso>0){  //xml Anexo 2B - Emisión o comercialización de tarjetas prepagadas.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo2b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==4 && row.xml==1 && row.idaviso>0){  //xml Anexo 2C - Modederos y tarjetas de devoluciones y recompensas.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo2c_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==5 && row.xml==1 && row.idaviso>0){  //xml Anexo 3 - Cheques de viajero.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo3_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                                                       
                            if(actividad==6 && row.xml==1 && row.idaviso>0){  //xml aviso 6 Servicios de mutuo, préstamos o créditos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo4_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==7 && row.xml==1 && row.idaviso>0){  //xml normal 5a Servicios de construcción o desarrollo inmobiliario
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo5a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==8 && row.xml==1 && row.idaviso>0){   //xml de aviso 5b Desarrollos Inmobiliarios
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo5b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==9 && row.xml==1 && row.idaviso>0){  
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo6_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==10 && row.xml==1 && row.idaviso>0){  
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo7_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==12 && row.xml==1 && row.idaviso>0){  
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo9_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==13 && row.xml==1 && row.idaviso>0){  //Anexo 10 - Traslado y custodia de valores.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'TransaccionAn10/anexo10_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==14 && row.xml==1 && row.idaviso>0){  //xml Anexo 11 - Servicios Profesionales.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo11_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/0"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==15 && row.xml==1 && row.idaviso>0){  //xml Anexo 12 A - Fedatarios públicos
                                id_de_ayuda = row.id_pago_ayuda;
                                if(row.id_pago_ayuda==0){
                                    id_de_ayuda = row.id_pago_normal;
                                }
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo12a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+id_de_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==16 && row.xml==1 && row.idaviso>0){  //xml Anexo 12 B - Servidores públicos
                                id_de_ayuda = row.id_pago_ayuda;
                                if(row.id_pago_ayuda==0){
                                    id_de_ayuda = row.id_pago_normal;
                                }
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo12b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+id_de_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==17 && row.xml==1 && row.idaviso>0){  //xml aviso Donativos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo13_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==19 && row.xml==1 && row.idaviso>0){  //xml aviso Arrendamiento de inmuebles
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo15_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            } 
                            if(actividad==20 && row.xml==1 && row.idaviso>0){  //xml normal 1 Juegos, apuestas y sorteos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo16_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }

                            //GENERAR AVISOS 24 HORAS
                            /*if(actividad==14 && row.resultado!="" || actividad==16 && row.resultado!=""){  //
                                html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/0/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                            }else if(actividad!=14 && row.resultado!="" || actividad!=16 && row.resultado!=""){
                                html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/'+row.id_pago_ayuda+'/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                            }
                            */

                            id_pagoa = row.id_pago_ayuda;  
                            return html;
                         }//render
                    },
                    {"data": null,
                        "render": function ( data, type, row, meta ) {
                            //console.log("total: "+row.totalPagos);
                            var acuse='<div class="row">\
                                    <div class="col-md-10">\
                                      <label class="control-label">PDF DE ACUSE</label>\
                                      <div class="row row_file">\
                                          <div class="col-md-12">\
                                              <input width="50%" onchange="cargarDoc('+row.id_anexo_tabla_aviso+',this.value,this.id,this.name,this.files[0].size,'+row.id_acuse_avi+','+row.id_actividad+',1)" class="doc '+row.id_anexo_tabla_aviso+'" id="xml_'+row.id_anexo_tabla_aviso+'" name="'+row.id_acuse_avi+'" type="file">\
                                          </div>\
                                          <div class="col-md-12"></div>\
                                      </div>\
                                    </div>\
                                  </div>';
                                  acuseXML(row.id_acuse_avi,row.acuse_aviso,row.id_anexo_tabla_aviso,1);
                            return acuse;
                        } 
                    },
                    {"data": null,
                    "render": function ( data, type, row, meta ) {
                        //console.log("total: "+row.totalPagos);
                        var acuse='<div class="row">\
                                <div class="col-md-10">\
                                  <label class="control-label">PDF DE ACUSE</label>\
                                  <div class="row row_file">\
                                      <div class="col-md-12">\
                                          <input width="50%" onchange="cargarDoc('+row.id_anexo_tabla+',this.value,this.id,this.name,this.files[0].size,'+row.id_acuse+','+row.id_acuse_avi+',2)" class="doc '+row.id_anexo_tabla+'" id="pdf_'+row.id_anexo_tabla+'" name="'+row.id_acuse+'" type="file">\
                                      </div>\
                                      <div class="col-md-12"></div>\
                                  </div>\
                                </div>\
                              </div>';
                              acuseXML(row.id_acuse,row.acuse_avi_pdf,row.id_anexo_tabla,2);
                        return acuse;
                    } 
                }
                // window.location.href = base_url+"Transaccion/"+rr+"/"+data.id_clientec+"/"+data.id_actividad+"/"+data.id_perfilamiento+"/"+data.id_transaccion+"/"+data.idtransbene;    
            ],
            order: [[ 0, "desc" ]],
    });
    $(".table").addClass("table-striped table-bordered dt-responsive");
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}
function regresar_view(){
    window.history.back();
}

function acuseXML(id_acu,xml,id,tipo){
    //console.log("xml: "+xml);
    type="";
    if(xml!=""){
        if(tipo==1){ //xml
            prevx="true";
            inix= base_url+"uploads/acuses_generados/"+xml;
            defx= base_url+'uploads/acuses_generados/'+xml+'" ';
            //type="xml";
            type="pdf";
        }else{ //pdf
            prevx="true";
            inix= base_url+"uploads/acuses_generados/"+xml;
            defx= base_url+'uploads/acuses_generados/'+xml+'" ';
            type="pdf";
        }
    }else{
        prevx="";
        inix="";
        defx="";
    }
    //console.log("prevx: "+prevx);
    var name_input="";
    if(tipo==1){//xml{
        name_input="#xml_"+id+"";
    }else{
        name_input="#pdf_"+id+"";
    }
    $(name_input).fileinput({
        theme: 'explorer-fa5',
        overwriteInitial: true,
        maxFileSize: 5000,
        showClose: false,
        showCaption: false,
        showZomm: false,
        reversePreviewOrder: true,
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: defx,
        initialPreviewDownloadUrl: inix,

        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: [""+type+""],
        initialPreview: [
          // IMAGE RAW MARKUP
          ""+inix+"",
        ],
        initialPreviewAsData: prevx,
        initialPreviewFileType: type, // image is the default and can be overridden in config below
        initialPreviewConfig: [
          {type: type, url: base_url+"Estadisticas/delete_acuse/"+id_acu+"/2/"+tipo+"", caption: xml, key: 1},
        ]
    });
    /*if(tipo==1)
        $(".kv-file-zoom").hide();*/
}

function cargarDoc(id_a8,val,id,name,size,id_acuse,id_actividad,tipo){
    filezise = size;                     
    if(filezise > 2048000) { // 512000 bytes = 500 Kb
      //console.log($('.img')[0].files[0].size);
      $(this).val('');
      swal("Error!", "El archivo supera el límite de peso permitido", "error");
    }else { //ok
      tama_perm = true; 
      var archivo = val;
      var id = id; //id con nombre de xml o pdf
      var name = name; //id solito - id de movimiento (saldo)

      extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
        /*if(tipo==1)
            extensiones_permitidas = new Array(".xml");
        else
            extensiones_permitidas = new Array(".pdf");*/

        extensiones_permitidas = new Array(".pdf");

      permitida = false;
      if($('#'+id)[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
          if (extensiones_permitidas[i] == extension) {
            permitida = true;
          break;
          }
        }  
        if(permitida==true && tama_perm==true){
          //console.log("tamaño permitido");
          var inputFileImage = document.getElementById(id);
          var file = inputFileImage.files[0];
          var data = new FormData();
          //id_docs = $("#id").val();
          data.append('foto',file);
          $.ajax({
            url:base_url+'Estadisticas/cargafiles/'+id_a8+'/'+id_actividad+'/'+id_acuse+"/1/"+tipo,
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
              var array = $.parseJSON(data);
              //console.log(array.ok);
              if (array.ok=true) {
                id_docs = array.id;
                load();
                //console.log("id_docs: "+id_docs);
                swal("Éxito", "Acuse cargado correctamente", "success");
                $("#id_docs").val(id_docs);
              }else{
                swal("Error!", "No Se encuentra el archivo", "error");
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              var data = JSON.parse(jqXHR.responseText);
            }
          });
        }else if(permitida==false){
          swal("Error!", "Tipo de archivo no permitido", "error");
        } 
      }else{
        swal("Error!", "No se a selecionado un archivo", "error");
      } 
    }
}