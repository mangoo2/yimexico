var base_url = $('#base_url').val();

$(document).ready(function() {
	$("#cata_inmueble").on("click",function(){
		location.href= base_url+'Clientes/catalogoInmuebles';	
	});

	$("#expedientes").on("click",function(){
		location.href= base_url+'Clientes_cliente/exportarExpedientes';	
	});
});

function href_bita_cliente(){
    location.href= base_url+'Clientes_cliente';	
}
function bitacora_transacciones(){
    location.href= base_url+'Estadisticas/bitacora_transacciones';	
}
function bitacora_expedientes(){
    location.href= base_url+'Estadisticas/bitacora_expedientes';	
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}
function href_mis_usuarios(){
	location.href= base_url+'MisUsuarios';
}
function href_consultas(){
	//location.href= base_url+'Estadisticas/consulta';
	location.href= base_url+'Estadisticas/bitacora_transacciones';
}
function href_aviso_cero(){
	location.href= base_url+'Estadisticas/aviso_cero';
}
function href_divisas(){
    location.href= base_url+'Divisas';
}
function href_aviso_cero(){
    location.href= base_url+'Estadisticas/aviso_cero';
}

function href_avisos(){
    location.href= base_url+'Estadisticas/avisos';
}
