var base_url = $('#base_url').val();
var table;
$(document).ready(function() {
	ComboAnio();
  $('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");
  $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");
  load();
});

function regresar_view(){
  window.history.back();
}
function inicio_cliente(){
  location.href= base_url+'Inicio_cliente';
}
function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
    var opc = document.createElement("option");
    opc.text = i;
    opc.value = i;
    select.add(opc)
  }
}
function registrar_aviso(){
  var informe=0;
  /*if($("#informe").is(":checked")==true){
    informe=1;
  }*/
  informe = $("#exento").val();

  var datos = $('#form_aviso').serialize();
  $.ajax({
    type:'POST',
    url: base_url+'Operaciones/guadar_aviso_cero',
    data: datos+"&informe="+informe,
    statusCode:{
      404: function(data){
        swal("Error!", "No Se encuentra el archivo", "error");
      },
      500: function(){
        swal("Error!", "500", "error");
      }
    },
    success:function(data){
      //regresar_view();
      var id_avi = data;
      window.location.href = base_url+'Operaciones/aviso_cero_xml/'+$("#exento").val()+"/"+id_avi;
      swal("Éxito", "Se guardo Correctamente", "success");
      setTimeout(function(){
        //if($("#exento").val()==0)
          location.href= base_url+'Estadisticas/aviso_cero'; 
      }, 1500);
      
    } 
  });  
}
function reload_table(){
  table.destroy();
  load();
}
function load(){
  //table_tipo.destroy();
  table=$("#table_aviso").DataTable({
    "bProcessing": true,
    "serverSide": true,
    destroy: true,
    "ajax": {
       "url": base_url+"Estadisticas/getlistado_aviso_cero",
       type: "post",
       data:{anio:$('#anio option:selected').val(),actividad:$('#actividad option:selected').val(),tipo:$('#tipo_aviso option:selected').val()},
        error: function(){
          $("#table_aviso").css("display","none");
        }
    },
    "columns": [
        {"data": "id"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var html='';  
                if(row.mes_acuse=='01'){
                  html='Enero';  
                }else if(row.mes_acuse=='02'){
                  html='Febrero';  
                }else if(row.mes_acuse=='03'){
                  html='Marzo';  
                }else if(row.mes_acuse=='04'){
                  html='Abril';  
                }else if(row.mes_acuse=='05'){
                  html='Mayo';  
                }else if(row.mes_acuse=='06'){
                  html='Junio';  
                }else if(row.mes_acuse=='07'){
                  html='Julio';  
                }else if(row.mes_acuse=='08'){
                  html='Agosto';  
                }else if(row.mes_acuse=='09'){
                  html='Septiembre';  
                }else if(row.mes_acuse=='10'){
                  html='Octubre';  
                }else if(row.mes_acuse=='11'){
                  html='Noviembre';  
                }else if(row.mes_acuse=='12'){
                  html='Diciembre';  
                }                  
            return html;
            }
        }, 
        {"data": null,
          "render": function ( data, type, row, meta ) {
            var html='';  
            if(row.informe=='0'){
              html='Aviso exento';  
            }else if(row.informe=='1'){
              html='Aviso en 0';  
            }               
          return html;
          }
        },    
        {"data": null,
          "render": function ( data, type, row, meta ) {
          var html='<!--<a class="btn" style="color: #212b4c;" title="Ver Anexo" style="color: white;" href="'+base_url+'Operaciones/aviso_cero/'+row.id+'"><i class="fa fa-edit" style="font-size: 22px;"></i></a>-->\
                   <a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Operaciones/aviso_cero_xml/'+$("#exento").val()+"/"+row.id+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';        
          return html;
          }
        },
        {"data": null,
          "render": function ( data, type, row, meta ) {
            //console.log("total: "+row.totalPagos);
            var acuse='<div class="row">\
                    <div class="col-md-11">\
                      <label class="control-label">PDF DE ACUSE</label>\
                      <div class="row row_file">\
                          <div class="col-md-12">\
                              <input width="50%" onchange="cargarDoc('+row.id+',this.value,this.id,this.name,this.files[0].size,'+row.actividad+')" class="doc '+row.id+'" id="xml_'+row.id+'" name="'+row.id+'" type="file">\
                          </div>\
                          <div class="col-md-12"></div>\
                      </div>\
                    </div>\
                  </div>';
                  acuseXML(row.id,row.acuse_sat);
            return acuse;
          } 
        }
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}

function acuseXML(id_acu,xml){
  //console.log("xml: "+xml);
  type="";
  if(xml!=""){
      prevx="true";
      inix= base_url+"uploads/acuses_generados/"+xml;
      defx= base_url+'uploads/acuses_generados/'+xml+'" ';
  }else{
      prevx="";
      inix="";
      defx="";
  }
    //console.log("prevx: "+prevx);
  $("#xml_"+id_acu+"").fileinput({
      theme: 'explorer-fa5',
      overwriteInitial: true,
      maxFileSize: 5000,
      showClose: false,
      showCaption: false,
      showZomm: false,
      reversePreviewOrder: true,
      removeTitle: 'Cancel or reset changes',
      elErrorContainer: '#kv-avatar-errors-1',
      msgErrorClass: 'alert alert-block alert-danger',
      defaultPreviewContent: defx,
      initialPreviewDownloadUrl: inix,

      layoutTemplates: {main2: '{preview} {remove} {browse}'},
      allowedFileExtensions: ["pdf"],
      initialPreview: [
        // IMAGE RAW MARKUP
        ""+inix+"",
      ],
      initialPreviewAsData: prevx,
      initialPreviewFileType: 'pdf', // image is the default and can be overridden in config below
      initialPreviewConfig: [
        {type: "pdf", url: base_url+"Estadisticas/delete_acuse/"+id_acu+"/1/1", caption: xml, key: 1},
      ]
  });
  //$(".kv-file-zoom").hide();
}

function cargarDoc(id_acu,val,id,name,size,id_actividad){
  filezise = size;                     
  if(filezise > 2048000) { // 512000 bytes = 500 Kb
    //console.log($('.img')[0].files[0].size);
    $(this).val('');
    swal("Error!", "El archivo supera el límite de peso permitido", "error");
  }else { //ok
    tama_perm = true; 
    var archivo = val;
    var id = id; //id con nombre de xml o pdf
    var name = name; //id solito - id de movimiento (saldo)

    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".pdf");
    permitida = false;
    if($('#'+id)[0].files.length > 0) {
      for (var i = 0; i < extensiones_permitidas.length; i++) {
        if (extensiones_permitidas[i] == extension) {
          permitida = true;
        break;
        }
      }  
      if(permitida==true && tama_perm==true){
        //console.log("tamaño permitido");
        var inputFileImage = document.getElementById(id);
        var file = inputFileImage.files[0];
        var data = new FormData();
        //id_docs = $("#id").val();
        data.append('foto',file);
        $.ajax({
          url:base_url+'Estadisticas/cargafiles/'+id_acu+'/'+id_actividad+'/'+id_acu+"/2/1",
          type:'POST',
          contentType:false,
          data:data,
          processData:false,
          cache:false,
          success: function(data) {
            var array = $.parseJSON(data);
            //console.log(array.ok);
            if (array.ok=true) {
              id_docs = array.id;
              load();
              //console.log("id_docs: "+id_docs);
              swal("Éxito", "Acuse cargado correctamente", "success");
              $("#id_docs").val(id_docs);
            }else{
              swal("Error!", "No Se encuentra el archivo", "error");
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
          }
        });
      }else if(permitida==false){
        swal("Error!", "Tipo de archivo no permitido", "error");
      } 
    }else{
      swal("Error!", "No se a selecionado un archivo", "error");
    } 
  }
}