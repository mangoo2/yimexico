var base_url = $('#base_url').val();
$(document).ready(function($) {
    
   ComboAno();
   $('#mes option[value="'+$('.mes_actual').text()+'"]').attr("selected", "selected");
   setTimeout(function(){ load(); }, 3000);

    var idc= $('#idcliente').val();
    var idp= $('#idperfilamiento').val()
    $.ajax({
        type: "POST",
        url: base_url+"Operaciones/get_clientes_select",
        data:{idc:idc,idp:idp},
        success: function (data){
          $('.select_clientes').html(data);
        }
    });

});	  
function ComboAno(){
    var d = new Date();
    var n = d.getFullYear();
    var select = document.getElementById("ano");
    for(var i = n; i >= 1900; i--) {
        var opc = document.createElement("option");
        opc.text = i;
        opc.value = i;
        select.add(opc)
    }
}

function load(){  
    var anio = $('#ano option:selected').val();
    var mes = $('#mes option:selected').val(); 
    var cliente = $('#cliente option:selected').val();
    var actividad = $('#actividad option:selected').val();
    var id_pagoa=0; var id_pagoaa=0; var id_pagon=0;
    var tipo="";
    $('.tabla_transaccion').html('<table class="table" id="table">\
            <thead>\
              <tr>\
                <th>No</th>\
                <th>Cliente</th>\
                <th>Fecha de Operación</th>\
                <!--<th>Resumen</th>\
                <th>Total de factura</th>-->\
                <th style="width: 205px;">Acciones</th>  \
              </tr>\
            </thead>\
            <tbody>\
            </tbody>\
          </table>');
    table = $('#table').DataTable({
            "bProcessing": true,
            "serverSide": true,
            "ajax":{
                "url": base_url+"Estadisticas/getListado_transacion",
                type: "post",
                error: function(){
                    $("#table").css("display","none");
                }, 
                data:{anio:anio,mes:mes,cliente:cliente,actividad:actividad,tipo:tipo},
            },
            "columns": [
                    //{"data": "idctrans"},
                    
                    //{"data": "idopera"},
                    {"data": "folio_cliente"},
                    {"data": "nombre",
                        "render": function ( data, type, row, meta ) {
                            //console.log("total: "+row.totalPagos);
                            var html_cliente='';
                                if(row.idtipo_cliente==1){
                                   html_cliente=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                                }else if(row.idtipo_cliente==2){
                                   html_cliente=row.nombre2+' '+row.apellido_paterno2+' '+row.apellido_materno2;
                                }else if(row.idtipo_cliente==3){
                                   html_cliente=row.razon_social;
                                }else if(row.idtipo_cliente==4){
                                   html_cliente=row.nombre_persona;
                                }else if(row.idtipo_cliente==5){
                                   html_cliente=row.denominacion2;
                                }else if(row.idtipo_cliente==6){
                                   html_cliente=row.denominacion;
                                }
                            //return html_cliente;
                            return row.cliente;

                        } 
                    },
                    {"data": "fecha_operacion"}, 
                    //{"data": "transaccion"},
                    //{"data": "totalPagos"},
                    /*{"data": null,
                        "render": function ( url, type, full) {
                            if(actividad==15){
                                return rw="N/A";
                            }
                            else{
                                if(full['totalpagos']!=null){
                                    var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['totalpagos']);
                                }
                                else{
                                    rw="$"+0.00;
                                }
                            }   
                            return rw;
                        }
                    },*/
                    {"data": null,
                        "render": function ( data, type, row, meta ) {
                            var cm = "'";
                            var anexo = row.transaccion;
                            var ane = anexo.split('-');
                            var ruta = ane[0];
                            rr = ruta.replace(" ","");
                            rr = rr.toLowerCase();
                            rr = rr.replace(/ /g, "");  
                            var sta=0;
                            var pinta_aviso=0; var sta_avi=0;
                            if(row.xml=="1" || row.acusado==1){ 
                                sta=1;
                            }
                            //console.log("row.xml: "+row.xml);
                            var html='<a class="btn" target="_blank" style="color: #212b4c;" title="Ver Anexo" style="color: white;" href="'+base_url+'Transaccion/'+rr+'/'+sta+'/'+row.id_clientec+'/'+row.id_actividad+'/'+row.id_perfilamiento+'/'+row.id_transaccion+'/'+row.idtransbene+'/'+row.idopera+'"><i class="fa fa-edit" style="font-size: 22px;"></i></a>';
                            if(actividad==14 && row.xml=="1" || actividad==16 && row.xml=="1"){
                                pinta_aviso=1;
                                //sta_avi=1;
                            }
                            if(actividad!=14 && row.xml=="1" && id_pagoa != row.id_pago_normal /*&& row.id_pago_ayude!=id_pagoaa && row.id_pago_ayude>0*/ || actividad!=16 && row.xml=="1" && id_pagoa != row.id_pago_normal /*&& row.id_pago_ayude!=id_pagoaa && row.id_pago_ayude>0*/){
                                pinta_aviso=1;
                                //sta_avi=1;
                            }
                            /*if(actividad==15 && row.xml=="1" && id_pagoa != row.id_pago_normal && row.id_pago_ayude==0 || actividad==15 && row.xml=="1" && id_pagoa != row.id_pago_normal && row.id_pago_ayude==id_pagon){
                                pinta_aviso=1;
                                //sta_avi=1;
                            }*/
                            if(actividad==15 && row.xml=="1" || actividad==16 && row.xml=="1"){
                                pinta_aviso=1;
                                //sta_avi=1;
                            }

                            if(actividad==1 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml Anexo 1 - Juegos, apuestas y sorteos.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo1_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                                //pinta_aviso=1;
                            }
                            if(actividad==11 && row.xml=="1" && id_pagoa != row.id_pago_normal){  //xml normal 11 Vehículos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo8_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                                //console.log("idopera: "+row.idopera);
                                //console.log("id_pago_ayuda: "+row.id_pago_ayuda);
                            }
                            //console.log("id_pagoa: "+id_pagoa);
                            //console.log("id_pago_normal: "+row.id_pago_normal);
                            if(actividad==2 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml Anexo 2A - Emisión o comercialización de tarjetas prepagadas o el abono a instrumentos de almacenamiento de valor monetario.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo2a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==3 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml Anexo 2B - Emisión o comercialización de tarjetas prepagadas.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo2b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==4 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml Anexo 2C - Modederos y tarjetas de devoluciones y recompensas.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo2c_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==5 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml Anexo 3 - Cheques de viajero.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo3_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==6 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml Anexo 4 - normal 6 Servicios de mutuo, préstamos o créditos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo4_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==7 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml normal 5a Servicios de construcción o desarrollo inmobiliario
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo5a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==8 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml normal 5b Desarrollos Inmobiliarios
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo5b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==9 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml normal Anexo 6 Comercialización de metales preciosos, joyas y relojes
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo6_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==10 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml normal Anexo 7 obras de arte cabiar cantidad
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo7_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            } 
                            if(actividad==12 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml normal 9 Blindage
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo9_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==13 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //Anexo 10 - Traslado y custodia de valores.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'TransaccionAn10/anexo10_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==14 && row.xml=="1"){   //xml Anexo 11 - Servicios Profesionales.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo11_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            } 
                            /*console.log("id: "+row.idA12a);
                            console.log("idopera: "+row.idopera);
                            console.log("id_pago_ayude: "+row.id_pago_ayude);
                            console.log("id_pagoaa: "+id_pagoaa);
                            console.log("idcliente: "+row.idcliente);*/
                            if(actividad==15 && row.xml=="1" && id_pagoa != row.id_pago_normal && row.id_pago_ayude==0 /*&& row.id_pago_ayude!=id_pagoaa*/){   //xml Anexo 12 A - Fedatarios públicos
                                id_de_ayuda = row.id_pago_ayuda;
                                if(row.id_pago_ayuda==0){
                                    id_de_ayuda = row.id_pago_normal;
                                }
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo12a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+id_de_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            } 
                            if(actividad==16 && row.xml=="1" && id_pagoa != row.id_pago_normal && row.id_pago_ayude==0){   //xml Anexo 12 B - Servidores públicos
                                id_de_ayuda = row.id_pago_ayuda;
                                if(row.id_pago_ayuda==0){
                                    id_de_ayuda = row.id_pago_normal;
                                }
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo12b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+id_de_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==17 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml anexo 13 normal Donativos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo13_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==19 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //278885 xml normal 19  Arrendamiento de inmuebles
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo15_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==20 && row.xml=="1" && id_pagoa != row.id_pago_normal){   //xml Anexo 1 - Juegos, apuestas y sorteos.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo16_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/0/0/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(row.xml==1 && row.idaviso>0){
                                sta_avi=1;
                            }
                            // avisos //
                            if(pinta_aviso==1){   //acuse modifica  --ok ya funciona
                                html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Generar Aviso modificatorio" style="color: white;" href="'+base_url+'Transaccion/'+rr+'/'+sta_avi+'/'+row.id_clientec+'/'+row.id_actividad+'/'+row.id_perfilamiento+'/'+row.id_transaccion+'/'+row.idtransbene+'/'+row.idopera+'/1"><i class="fa fa-exclamation-triangle" style="font-size: 22px;"></i></a>';
                                //html+='<a class="btn" style="color: #212b4c;" title="Generar Acuse modificatorio" style="color: white;" href="'+base_url+'Transaccion/'+rr+'/0/'+row.id_clientec+'/'+row.id_actividad+'/'+row.id_perfilamiento+'/'+row.id_transaccion+'/'+row.idtransbene+'/'+row.idopera+'/1"><i class="fa fa-exclamation-triangle" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==11 && row.xml==1 && row.idaviso>0){ //xml aviso Vehículos 
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo8_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            } 

                            if(actividad==1 && row.xml==1 && row.idaviso>0){  //xml normal 1 Juegos, apuestas y sorteos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo1_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==2 && row.xml==1 && row.idaviso>0){  //xml Anexo 2A - Emisión o comercialización de tarjetas prepagadas o el abono a instrumentos de almacenamiento de valor monetario.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo2a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==3 && row.xml==1 && row.idaviso>0){  //xml Anexo 2B - Emisión o comercialización de tarjetas prepagadas.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo2b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==4 && row.xml==1 && row.idaviso>0){  //xml Anexo 2C - Modederos y tarjetas de devoluciones y recompensas.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo2c_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==5 && row.xml==1 && row.idaviso>0){  //xml Anexo 3 - Cheques de viajero.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo3_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                                                       
                            if(actividad==6 && row.xml==1 && row.idaviso>0){  //xml aviso 6 Servicios de mutuo, préstamos o créditos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo4_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==7 && row.xml==1 && row.idaviso>0){  //xml normal 5a Servicios de construcción o desarrollo inmobiliario
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo5a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==8 && row.xml==1 && row.idaviso>0){   //xml de aviso 5b Desarrollos Inmobiliarios
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML" style="color: white;" href="'+base_url+'Transaccion/anexo5b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==9 && row.xml==1 && row.idaviso>0){  
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo6_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==10 && row.xml==1 && row.idaviso>0){  
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo7_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==12 && row.xml==1 && row.idaviso>0){  
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo9_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==13 && row.xml==1 && row.idaviso>0){  //Anexo 10 - Traslado y custodia de valores.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'TransaccionAn10/anexo10_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==14 && row.xml==1 && row.idaviso>0){  //xml Anexo 11 - Servicios Profesionales.
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo11_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/0"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==15 && row.xml==1 && row.idaviso>0 && row.id_pago_ayude==0){  //xml Anexo 12 A - Fedatarios públicos
                                id_de_ayuda = row.id_pago_ayuda;
                                if(row.id_pago_ayuda==0){
                                    id_de_ayuda = row.id_pago_normal;
                                }
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo12a_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+id_de_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==16 && row.xml==1 && row.idaviso>0 && row.id_pago_ayude==0){  //xml Anexo 12 B - Servidores públicos
                                id_de_ayuda = row.id_pago_ayuda;
                                if(row.id_pago_ayuda==0){
                                    id_de_ayuda = row.id_pago_normal;
                                }
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo12b_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/0"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==17 && row.xml==1 && row.idaviso>0){  //xml aviso Donativos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo13_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            if(actividad==19 && row.xml==1 && row.idaviso>0){  //xml aviso Arrendamiento de inmuebles
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo15_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            } 
                            if(actividad==20 && row.xml==1 && row.idaviso>0){  //xml normal 1 Juegos, apuestas y sorteos
                                html+='<a class="btn" style="color: #212b4c;" title="Descargar XML Aviso" style="color: white;" href="'+base_url+'Transaccion/anexo16_xml/'+row.id_transaccion+'/'+row.id_perfilamiento+'/1/1/'+row.idopera+'/'+row.id_pago_ayuda+'"><i class="fa fa-file-excel-o" style="font-size: 22px;"></i></a>';
                            }
                            /* ********************************************** */
                            //GENERAR AVISOS 24 HORAS
                            //console.log(row.resultado.length);
                            if(row.id_union==0){
                                if(actividad==14 && row.resultado.length>78 /*&& row.resultado!=""*/ || actividad==16 && row.resultado.length>78 /*&& row.resultado!=""*/){  //
                                    html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Descargar Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/0/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                                }else if(actividad!=14 && row.resultado.length>78 /*&& row.resultado!=""*/ || actividad!=16 && row.resultado.length>78 /*&& row.resultado!=""*/){
                                    html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Descargar Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/'+row.id_pago_ayuda+'/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                                }
                            }else{
                                var veri = verificaResultado(row.idopera);
                                //console.log("veri: "+veri);
                                /*var array = $.parseJSON(verificaResultado(row.idopera));
                                if (array.length>0) {
                                    array.forEach(function(element) {
                                        console.log("band: "+element.band);
                                    });
                                }*/
                                if(actividad==14 && veri==1 || actividad==16 && veri==1){  //
                                    html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Descargar Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/0/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                                }else if(actividad!=14 && veri==1 || actividad!=16 && veri==1){
                                    html+='<a class="btn" target="_blank" style="color: #212b4c;" title="Descargar Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+row.idopera+'/'+row.idpacuse+'/'+row.id_transaccion+'/'+actividad+'/'+row.id_pago_ayuda+'/1"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
                                }  
                            }

                            id_pagoa = row.id_pago_ayuda;
                            id_pagon = row.id_pago_normal;  
                            id_pagoaa= row.id_pago_ayude;
                            return html;
                         }//render
                    }//data
                // window.location.href = base_url+"Transaccion/"+rr+"/"+data.id_clientec+"/"+data.id_actividad+"/"+data.id_perfilamiento+"/"+data.id_transaccion+"/"+data.idtransbene;    
            ],
            order: [[ 0, "desc" ]],
    });
    $(".table").addClass("table-striped table-bordered dt-responsive");
}

function verificaResultado(idopera){
    var data=0;
    $.ajax({
        type: 'POST',
        url : base_url+'Transaccion/verificaResultado',
        data: { idopera: idopera},
        async: false,
        success: function(data2){
            //console.log(data2);
            var array = $.parseJSON(data2);
            //console.log("band: "+array.band);
            data = array.band;
            /*console.log(array.length);
            if (array.length>0) {
                array.forEach(function(element) {
                    //console.log("band: "+element.band);
                    data = element.band;
                });
            }*/
        }
    });
    return data;
}
function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}
function regresar_view(){
    window.history.back();
}