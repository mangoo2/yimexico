
$(document).ready(function($) {
  $("#sin_beneficiario").on("click",function(){
    if($("#sin_beneficiario").is(":checked")==true){ 
      $("#bton_impri").show();
    }else{
      $("#bton_impri").hide();
    }
  });
  get_beneficiarios();

  var TABLA = $(".col-md-12 > #cont_benes_asigna"); 
  TABLA.each(function(){    
    ban_sinb=$(this).find("input[id*='band_sin_dueno']").val();
    //console.log("ban_sinb: "+ban_sinb);
    $("#sin_beneficiario").attr("disabled",true);
    $("#sin_beneficiario").attr("checked",true);
    $("#asigna_db").attr("href","javascript:void(0)");
    $('#asigna_db').removeAttr('data-toggle');
    $('#asigna_db').removeAttr('onclick');

    $("#agrega_db").attr("href","javascript:void(0)");
    $('#agrega_db').removeAttr('data-toggle');
    $('#agrega_db').removeAttr('onclick');
  });

  var TABLA = $(".col-md-12 > #table_benes"); 
  TABLA.each(function(){    
    ban_sinb=$(this).find("input[id*='band_sin_dueno']").val();
    //console.log("ban_sinb: "+ban_sinb);
    $("#sin_beneficiario").attr("disabled",true);
  });

  setTimeout(function(){ 
    if($("#sin_beneficiario").is(":checked")==true){ 
      $("#bton_impri").show();
    }else{
      $("#bton_impri").hide();
    }
  }, 1500);
  

});

function nuevo_bene(band){
  $("#modal_bene").modal();
  if(band==0){ //elegir uno
    $("#cont_select_bene").show();
    $("#cont_nvo_bene").hide();
    $("#otro_beneficiario").attr("checked",false);
  }else{
    $("#cont_nvo_bene").show();
    $("#cont_sin_bene").hide();
    $("#cont_select_bene").hide();
    $("#otro_beneficiario").attr("checked",true);
  }
  get_beneficiarios();
  if($("#sin_beneficiario").is(":checked")){
    $(".btn-impresion").show();
  }else{
    $(".btn-impresion").hide();
  }
}

function get_beneficiarios(){
  $.ajax({
    type: "POST",
    url: base_url+"Clientes_cliente/get_beneficiarios_select",
    data:{ idc:$("#idcc").val(), idp:$("#idp").val(), id_union:$("#id_union").val(), id_opera:$("#id_opera").val() },
    success: function (data){
      //console.log(data);
      $('.select_beneficiario').html(data);
      if($('#beneficiario_ option').length<=1){
        $("#a_benes_funt").hide();
      }
    }
  });
}

function imprimir_formato1(tipocon,idcc){ 
  /*var na= $("#name_cli").text();
  $("#nombre_cli").text(na);
  $.ajax({
    type:'POST',
    url: base_url+'Operaciones/getNames',
    async:false,
    data:{
      tipo:tipocon,idcc:idcc
    },
    success:function(data){
      //$("#nombre_cli").html(data);
      $("#nombre_cli_imp").html(data);
    }
  }); //cierra ajax

  $('.firma_tipo_cliente').css('display','block');
  setTimeout(function(){ 
    $('.firma_tipo_cliente').css('display','none');
  }, 500);
  window.print();*/

  //ido = $("#id_opera").val();
  //window.open(base_url+'Operaciones/generarPDF_SDB/'+idcc+"/"+tipocon+"/"+ido, '_blank');

  $('#modal_fecha').modal('show');
  $("#tipo_con").val(tipocon);
  $("#id_cc").val(idcc);
}

function imprimir_formato(){
  idcc = $("#id_cc").val();
  tipocon = $("#tipo_con").val();
  ido = $("#id_opera").val();
  fechaf = $('#fecha_f').val();
  window.open(base_url+'Operaciones/generarPDF_SDB/'+idcc+"/"+tipocon+"/"+ido+"/"+fechaf, '_blank');
}

function imprimir_formatoUnion(ido){ 
  var na= $("#name_cli").text();
  $("#nombre_cli").text(na);
  $.ajax({
    type:'POST',
    url: base_url+'Operaciones/getNameUnion',
    async:false,
    data:{
      ido:ido
    },
    success:function(data){
      //$("#nombre_cli").html(data);
      $("#nombre_cli_imp").html(data);
    }
  }); //cierra ajax

  $('.firma_tipo_cliente').css('display','block');
  setTimeout(function(){ 
    $('.firma_tipo_cliente').css('display','none');
  }, 500);
  window.print();
}

function aceptarAddNext(){

  if($("#sin_beneficiario").is(":checked")==false || $("#sin_beneficiario").is(":checked")==true && $("#sin_beneficiario").is(":disabled")==true){
    setTimeout(function(){ 
      window.location.href = base_url+"Operaciones/procesoInicial/"+$("#id_opera").val();
    }, 500);
  }else{
    id_bene=0;
    tipo_bene="0";
    
    id_cliente=$("#idcc").val();
    idpefilanvo=$("#idp").val();
    id_ob=0;
    $.ajax({
      type: "POST",
      url: base_url+"Operaciones/submit_operacionDB",
      data:{ id:$("#id_opera").val(), id_duenio_bene:id_bene, tipo_bene:tipo_bene, id_perfilamiento:idpefilanvo, id_cliente:id_cliente,id_ob:id_ob },
      beforeSend: function(){
        $("#btn_submit").attr("disabled",true);
      },
      success: function (data){
        console.log(data);
        var array = $.parseJSON(data);
        var id=array.id;

        swal("Éxito", "Guardado Correctamente", "success");
        //inforclienteb(cont_bene);
        setTimeout(function(){ 
          window.location.href = base_url+"Operaciones/procesoInicial/"+$("#id_opera").val();
        }, 1500);
        
      }
    });
  }
}

function aceptarAdd(){
  if($("#beneficiario_").val()!=""){
    var extraer = $("#beneficiario_").val().split('-');
    id_bene = extraer[0];
    tipo_bene = extraer[1];
    id_bene_mm = extraer[2];
    //console.log("id_bene guardado: "+id_bene);
  }else{
    id_bene=0;
    tipo_bene="0";
  }
  //console.log(" cont_bene: "+cont_bene);
  if(tipo_bene=="fisica"){
    tipo_bene="1";
  }
  if(tipo_bene=="moralmoral" || tipo_bene=="moralfisica"){
    tipo_bene="2";
  }
  if(tipo_bene=="fideicomiso"){
    tipo_bene="3";
  }
  id_cliente=$("#idcc").val();
  idpefilanvo=$("#idp").val();
  id_union=$("#id_union").val();
  id_ob=0;

  if($("#beneficiario_").val()!=""){
    $.ajax({
      type: "POST",
      url: base_url+"Operaciones/submit_operacionDB",
      data:{ id:$("#id_opera").val(), id_duenio_bene:id_bene, tipo_bene:tipo_bene, id_perfilamiento:idpefilanvo, id_cliente:id_cliente,id_ob:id_ob },
      beforeSend: function(){
        $("#btn_submit").attr("disabled",true);
     },
      success: function (data){
        console.log(data);
        var array = $.parseJSON(data);
        var id=array.id;
        $("#modal_bene").modal('hide');//ocultamos el modal
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        swal("Éxito", "Guardado Correctamente", "success");
        //inforclienteb(cont_bene);
        setTimeout(function(){ 
          window.location.href = base_url+"Operaciones/procesoInicial/"+$("#id_opera").val();
        }, 1500);
        
      }
    });
  }
}

function aceptarAddNvo(){
  if($("#beneficiario_").val()!=""){
    var extraer = $("#beneficiario_").val().split('-');
    id_bene = extraer[0];
    tipo_bene = extraer[1];
    id_bene_mm = extraer[2];
    //console.log("id_bene guardado: "+id_bene);
  }else{
    id_bene=0;
    tipo_bene="0";
  }
  //console.log(" cont_bene: "+cont_bene);
  if(tipo_bene=="fisica"){
    tipo_bene="1";
  }
  if(tipo_bene=="moralmoral" || tipo_bene=="moralfisica"){
    tipo_bene="2";
  }
  if(tipo_bene=="fideicomiso"){
    tipo_bene="3";
  }
  id_cliente=$("#idcc").val();
  idpefilanvo=$("#idp").val();
  id_union=$("#id_union").val();
  id_ob=0;

  if($("#id_union").val()!=0){
    if($("#fisica").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBene/0/0/0/0/"+$("#id_opera").val()+"/0/"+id_union;
    if($("#moral").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/0/0/0/0/"+$("#id_opera").val()+"/0/"+id_union;
    if($("#fideicomiso").is(':checked'))
        window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/0/0/0/0/"+$("#id_opera").val()+"/0/"+id_union;
  }else{
    if($("#fisica").is(':checked'))
      window.location.href = base_url+"Clientes_c_beneficiario/addBene/0/"+$("#id_cliente").val()+"/"+idpefilanvo+"/"+$("#tipo_cliente").val()+"/"+$("#id_opera").val()+'/'+id_cliente+'/'+id_union;
    if($("#moral").is(':checked'))
      window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/0/"+$("#id_cliente").val()+"/"+idpefilanvo+"/"+$("#tipo_cliente").val()+"/"+$("#id_opera").val()+'/'+id_cliente+'/'+id_union;
    if($("#fideicomiso").is(':checked'))
      window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/0/"+$("#id_cliente").val()+"/"+idpefilanvo+"/"+$("#tipo_cliente").val()+"/"+$("#id_opera").val()+'/'+id_cliente+'/'+id_union; 
  }
  
}

function eliminaBene(id){
  title = "¿Desea eliminar este registro?";
  swal({
      title: title,
      text: "Se eliminará el beneficiario de esta operación!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Aceptar",
      closeOnConfirm: true
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        type:'POST',
        url: base_url+'Operaciones/eliminaBO',
        data:{
          'id':id
        },
        success:function(data){
          location.reload();
        }
      }); //cierra ajax
    }
  });
  
}

function eliminaClie(id){
  title = "¿Desea eliminar este registro?";
  swal({
      title: title,
      text: "Se eliminará el cliente de esta operación!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Aceptar",
      closeOnConfirm: true
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        type:'POST',
        url: base_url+'Operaciones/eliminaCO',
        data:{
          'id':id
        },
        success:function(data){
          location.reload();
        }
      }); //cierra ajax
    }
  });
  
}

function verDBAsignado(tipo,idb,idp){
  if(tipo==1)
    window.location.href = base_url+"Clientes_c_beneficiario/addBene/"+idb+"/0/"+idp+"/0/"+$("#id_opera").val()+'/0/'+$("#id_union").val();
  if(tipo==2)
    window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/"+idb+"/o/"+idp+"/0/"+$("#id_opera").val()+'/0/'+$("#id_union").val();
  if(tipo==3)
    window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/"+idb+"/o/"+idp+"/0/"+$("#id_opera").val()+'/0/'+$("#id_union").val(); 
}


function inicio_cliente(){
  location.href= base_url+'Operaciones';
}
