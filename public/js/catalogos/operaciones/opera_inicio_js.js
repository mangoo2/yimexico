
$(document).ready(function($) {
  $("#ver_autoriza").on("click",function(){
    $.ajax({
      type: "POST",
      url: base_url+"Operaciones/validaAutoriza",
      data:{ ido:$("#id_opera").val() },
      async: false,
      success: function (data){
        var array = JSON.parse(data);
        var id=array.id; 
        var motivo=array.motivo; 
        if(id>0){
          $("#usuario").val("");
          $("#contrasena").val("");
          $("#usuario").attr("disabled",true);
          $("#contrasena").attr("disabled",true);
          $("#motivo").val(motivo);
          $("#motivo").attr("disabled",true);
          $("#id_autorizado").val(id);
        }
      }
    });
  });
});

function aceptarValida(){
  if($("#usuario").val()!="" && $("#contrasena").val()!="" && $("#motivo").val()!="" && $("#id_autorizado").val()==0){
    $.ajax({
      type: "POST",
      url: base_url+"Operaciones/validaUSerAutoriza",
      data:{ user:$("#usuario").val(), pass:$("#contrasena").val() },
      async: false,
      success: function (data){
        //console.log(data);
        var array = JSON.parse(data);
        var usuarioid=array.usuarioid; 
        var count=array.count; 
        if(count==2){
          swal("Error", "Usuario ingresado sin privilegios de autorización", "warning");
          $("#usuario").val("");
          $("#contrasena").val("");
          $("#motivo").val("");
          ocultaModal();
        }else if(count==1){
          cambiar_estatus(usuarioid);
          //swal("Éxito", "Autorización realizada correctamente", "success");
          //ocultaModal();
        }else if(count==0){
          swal("Error", "Usuario y/o contraseña incorrectos ", "warning");
        }

      }
    });
  }else if($("#usuario").val()=="" && $("#id_autorizado").val()==0 || $("#contrasena").val()=="" && $("#id_autorizado").val()==0 || $("#motivo").val()=="" && $("#id_autorizado").val()==0){
    swal("Error", "Ingrese los datos solicitados", "warning");
  }else if($("#id_autorizado").val()>0){
    ocultaModal();
  }
}

function ocultaModal(){
  $("#modal_autoriza").modal('hide');//ocultamos el modal
  $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
  $('.modal-backdrop').remove();//eliminamos el backdrop del modal
}

function cambiar_estatus(usuarioid){
  $.ajax({
    type: "POST",
    url: base_url+"Operaciones/cambiaEstatusAutoriza",
    data:{ motivo:$("#motivo").val(),ido:$("#id_opera").val(), idu:usuarioid },
    async: false,
    success: function (data){
      //console.log(data);
      swal("Éxito", "Autorización realizada correctamente", "success");
      ocultaModal();
      location.reload();
    }
  });
}