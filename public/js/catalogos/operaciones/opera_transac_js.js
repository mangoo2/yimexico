var base_url = $('#base_url').val();
var id_clitran="";
var id_act="0";
var id_trans="0";
var id_transbene="0";
$(document).ready(function($) { 
  verifica_proceso(); 
});

function verifica_proceso(){
  $.ajax({ // verifica proceso de operación 1.- paso 
     type: "POST",
     url: base_url+"Operaciones/verificaProceso",
     data: { "id": $("#id_opera").val() },
     async: false,
     success: function (result) {
        var data= $.parseJSON(result);
        var datos = data.data;
        datos.forEach(function(element) {
          /*console.log("element.validadoC: ".element.validadoC);
          console.log("element.validadoB: ".element.validadoB);*/
          //if(element.id_duenio_bene>0){
            //cuando si existe un dueño beneficiario
            if(element.id_duenio_bene!="" && element.grado!=0 && element.validadoC=="1" && element.validadoB=="1"){ //si existe db desbloqueo el siguiente paso
              //$("#tran").removeAttr("disabled").removeClass('btn-outline-dark').addClass('btn_operacion').attr("style","width: 180px; height: 125px; background-color:#009336");
              $("#btn_transacc").removeAttr("disabled");
              if(element.id_cliente_transac!=""){
                id_clitran = element.id_cliente_transac;
              }
            }else{
              $("#btn_transacc").attr("disabled",true); 
            }
          //}
          //if(element.id_duenio_bene==0){
            if(element.id_duenio_bene!="" && element.grado!=0 && element.validadoC=="1"){ //si existe db desbloqueo el siguiente paso
              //$("#tran").removeAttr("disabled").removeClass('btn-outline-dark').addClass('btn_operacion').attr("style","width: 180px; height: 125px; background-color:#009336");
              $("#btn_transacc").removeAttr("disabled");
              //console.log("activa boton");
              if(element.id_cliente_transac!=""){
                id_clitran = element.id_cliente_transac;
              }
            }else{
              $("#btn_transacc").attr("disabled",true); 
            }
          /*if(element.id_duenio_bene=="" && element.grado>0 && element.validadoC=="1"){ //si existe db desbloqueo el siguiente paso
            //$("#btn_transacc").removeAttr("disabled");
            $("#btn_transacc").attr("disabled",false); 
            console.log("activa boton");
            if(element.id_cliente_transac!=""){
              id_clitran = element.id_cliente_transac;
            }
          }*/
          //}

          var clit = data.ct;
          //console.log("clit: "+clit);
          if(clit != ""){
            //console.log("clit !=: ");
            clit.forEach(function(element2) {
              //if(id_clitran!="" || id_clitran>0){
                id_act = element2.id_actividad;
                id_trans = element2.id_transaccion;
                id_transbene = element2.idtransbene;
                //paso6();
              //}
            });
          }
          console.log("id_act: "+id_act);
        });//datos.forEach
        paso6();
     }//success
  });//ajax
}

function paso6(){
  //console.log("tras bene: "+id_transbene);
  //console.log("id_trans: "+id_trans);
  if(id_trans!="" && id_trans>0){
    if(id_act==1)anexo="anexo1";if(id_act==2)anexo="anexo2a";if(id_act==3)anexo="anexo2b";if(id_act==4)anexo="anexo2c";if(id_act==5)anexo="anexo3";
    if(id_act==6)anexo="anexo4";if(id_act==7)anexo="anexo5a";if(id_act==8)anexo="anexo5b";if(id_act==9)anexo="anexo6";if(id_act==10)anexo="anexo7";
    if(id_act==11)anexo="anexo8";if(id_act==12)anexo="anexo9";if(id_act==13)anexo="anexo10"; if(id_act==14)anexo="anexo11";if(id_act==15)anexo="anexo12a";
    if(id_act==16)anexo="anexo12b";if(id_act==17)anexo="anexo13";if(id_act==18)anexo="anexo14";if(id_act==19)anexo="anexo15";if(id_act==20)anexo="anexo16";
    //window.open(base_url+'Transaccion/'+anexo+'/0/'+$("#idclientec").val()+"/"+id_act+"/"+idperfilamiento+"/"+id_trans+"/"+id_transbene+"/"+$('#id').val());
    location.href=base_url+'Transaccion/'+anexo+'/0/0/'+id_act+"/0/"+id_trans+"/"+id_transbene+"/"+$('#id_opera').val()+"/0";
  }else{
    //$('#transacción_actividades').modal();
    actividades_clientes(); 
  }  
}

function actividades_clientes(){
  var idact_cal=0;
  id_opera=$('#id_opera').val()
  $.ajax({
    type: "POST",
    url: base_url+"Operaciones/operaGradoAct",
    data:{ id_opera:id_opera },
    success: function (data){
      //console.log("data:" +data);
      if(data!=0){
        //console.log("entro a data !=0:" +data);
        var array = $.parseJSON(data);
        idact_cal=array.id_actividad;
      }

      $.ajax({
        type: "POST",
        url: base_url+"index.php/Clientes_cliente/link_actividades_clientes",
        data:{id:$("#idcliente").val(), id_act_cal: idact_cal},
        success: function (data){
          $('.transaccion_acti').html(data);
          setTimeout(function(){ 
            aceptarAct(); 
          }, 500);
        }
      });

    }
  });

  /*$.ajax({
      type: "POST",
      url: base_url+"index.php/Clientes_cliente/link_actividades_clientes",
      data:{id:$("#idcliente").val()},
      success: function (data){
          $('.transaccion_acti').html(data);
      }
  });*/
}

function aceptarAct(){
  var idact=$('#selec_activ').val();
  $.ajax({
    type: "POST",
    url: base_url+"Clientes_cliente/guardar_beneficiario_transaccion_nva",
    data:{
      id_actividad: idact, id_opera:$("#id_opera").val(), 
    },
    success: function (data){
      //swal("Éxito", "Guardado Correctamente", "success");
      swal("Éxito", "Redirigiendo a la transacción", "success");
      var array = $.parseJSON(data);
      id_benetra = array.id;

      if(idact==1)anexo="anexo1";if(idact==2)anexo="anexo2a";if(idact==3)anexo="anexo2b";if(idact==4)anexo="anexo2c";if(idact==5)anexo="anexo3";
      if(idact==6)anexo="anexo4";if(idact==7)anexo="anexo5a";if(idact==8)anexo="anexo5b";if(idact==9)anexo="anexo6";if(idact==10)anexo="anexo7";
      if(idact==11)anexo="anexo8";if(idact==12)anexo="anexo9";if(idact==13)anexo="anexo10"; if(idact==14)anexo="anexo11";if(idact==15)anexo="anexo12a";
      if(idact==16)anexo="anexo12b";if(idact==17)anexo="anexo13";if(idact==18)anexo="anexo14";if(idact==19)anexo="anexo15";if(idact==20)anexo="anexo16";
      window.location.href = base_url+"Transaccion/"+anexo+"/0/0/"+idact+"/0/"+id_trans+"/"+id_benetra+"/"+$('#id_opera').val();
    }
  });

}

function inicio_cliente(){
    location.href= base_url+'Operaciones';
}
function regresar_view(){
    window.history.back();
}