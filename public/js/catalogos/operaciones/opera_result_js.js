$(document).ready(function () {
  load(); 
  loadBene(); 
});

var lang={"loadingRecords": "No se encontraron registros en la busqueda"};

function load(){
  if($("#tabla_resultc").length >= 1){ 
    table = $('#tabla_resultc').DataTable({
      destroy: true,
      "ajax": {
         "url": base_url+"Operaciones/resultado_clientes_data",
         type: "post",
         data: { ido:$("#id_opera").val() }
      },
      "columns": [
          {"data": "Denominacion"},
          {"data": "Identificacion"},
          {"data": "Id_Tributaria"},
          {"data": "Otra_Identificacion"},
          {"data": "Cargo"},
          {"data": "Lugar_Trabajo"},
          {"data": "Direccion"},
          //{"data": "Enlace"},
          {"data": null,
            "render": function ( url, type, full) {
                return '<a href='+full["Enlace"]+' target="_blank">'+full["Enlace"]+'</a>';
            }
          },
          {"data": "Tipo"},
          {"data": "Sub_Tipo"},
          {"data": "Estado"},
          {"data": "Lista"},
          {"data": "Pais_Lista"},
          {"data": "Cod_Individuo"},
          {"data": "Exactitud_Denominacion"},
          {"data": "Exactitud_Identificacion"},
          {"data": "id_perfilamiento",
            "render": function ( url, type, full) {
              var idp=full["id_perfilamiento"];
              var ido=$("#id_opera").val();
              html='<a class="btn" target="_blank" style="color: #212b4c;" title="Generar Aviso 24 hrs." style="color: white;" href="'+base_url+'Aviso24/generarAviso/'+ido+'/'+idp+'/0/0/0/0"><i class="fa fa-file-pdf-o" style="font-size: 22px;"></i></a>';
              return html;
            }
          },
      ],
      "order": [[ 0, "asc" ]],
      "language": lang
    });
    $("#cont_btn_pep").show();
  }else{
    $("#cont_btn_pep").hide();
    $('#tabla_resultc').DataTable({});
  }
}

function loadBene(){
  if($("#tabla_resultdb").length >= 1){ 
    table = $('#tabla_resultdb').DataTable({
        destroy: true,
        "ajax": {
           "url": base_url+"Operaciones/resultado_beneficiario_data",
           type: "post",
           data: { ido:$("#id_opera").val() }
        },
        "columns": [
            {"data": "Denominacion"},
            {"data": "Identificacion"},
            {"data": "Id_Tributaria"},
            {"data": "Otra_Identificacion"},
            {"data": "Cargo"},
            {"data": "Lugar_Trabajo"},
            {"data": "Direccion"},
            //{"data": "Enlace"},
            {"data": null,
              "render": function ( url, type, full) {
                  return '<a href='+full["Enlace"]+' target="_blank">'+full["Enlace"]+'</a>';
              }
            },
            {"data": "Tipo"},
            {"data": "Sub_Tipo"},
            {"data": "Estado"},
            {"data": "Lista"},
            {"data": "Pais_Lista"},
            {"data": "Cod_Individuo"},
            {"data": "Exactitud_Denominacion"},
            {"data": "Exactitud_Identificacion"},
        ],
        "order": [[ 0, "asc" ]],
        "language": lang
    });
  }else{
    $('#tabla_resultdb').DataTable({});
  }
}

function cuestionarioPEP(perfila,tipoc){
  if(tipoc==1 || tipoc==2){
    //window.open(base_url+"Perfilamiento/formulariopep/"+perfila+"/"+tipoc, "_blank");
    location.href=base_url+"Perfilamiento/formulariopep/"+perfila+"/"+tipoc;
  }
  else{
    //window.open(base_url+"Perfilamiento/formulariopep_moral/"+perfila+"/"+tipoc, "_blank");
    location.href=base_url+"Perfilamiento/formulariopep_moral/"+perfila+"/"+tipoc;
  }
}

function ayuda_pep(){
  $("#ayuda_pep").modal();
}