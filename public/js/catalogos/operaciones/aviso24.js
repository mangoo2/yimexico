var base_url = $('#base_url').val();
var table;
$(document).ready(function() {
	ComboAnio();
  $('#anio option[value="'+$('#anio_acuse_aux').val()+'"]').attr("selected", "selected");
  $('#mes option[value="'+$('#mes_aux').val()+'"]').attr("selected", "selected");

  $("#id_act").on("change",function(){
    get_alerta(this.value);
    $('#id_alerta').val('');
    verOtraDesc();
    //console.log("value: "+this.value);
  });
  get_alerta($("#id_act option:selected").val());

  $("#id_alerta").on("change",function(){
    verOtraDesc();
  });
});

function verOtraDesc(){
  console.log("clave: "+$('#id_alerta option:selected').data('value'));
  if($('#id_alerta option:selected').data('value')=="9999")
    $("#cont_otra").show();
  else
    $("#cont_otra").hide();
}

function regresar_view(){
    window.history.back();
}
function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}
function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var select = document.getElementById("anio");
  for(var i = n; i >= 2000; i--) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}

function registrar_aviso(){
  var idp=$("#id_perfilamiento").val();
  var ido=$("#id_opera").val();
  var anio=$("#anio").val();
  var mes=$("#mes").val();
  var ida=$("#id_act").val();
  var idale=$("#id_alerta").val();
  var clave=$('#id_alerta option:selected').data('value');
  var descrip="0";
  var idtran=$("#idtran").val();
  var idpagoa=$("#idpagoa").val();
  var desdebit=$("#desdebit").val();
  if(clave=="9999"){
    descrip=$("#descripcion").val();
  }
  if(desdebit==0){
    window.location.href= base_url+'Aviso24/aviso24_xml/'+idp+"/"+anio+"/"+mes+"/"+ida+"/"+ido+"/"+clave+"/"+descrip+"/"+idtran; 
  }
  if(ida==1 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo1_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  } else if(ida==2 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo2a_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==3 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo2b_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==4 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo2c_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==5 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo3_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==6 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo4_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==7 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo5a_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==8 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo5b_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==9 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo6_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==10 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo7_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==11 && desdebit==1){
    //window.location.href= base_url+'Aviso24/aviso24_xml/'+idp+"/"+anio+"/"+mes+"/"+ida+"/"+ido+"/"+clave+"/"+descrip+"/"+idtran;
    window.location.href=base_url+'Transaccion/anexo8_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==12 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo9_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==13 && desdebit==1){
    window.location.href=base_url+'TransaccionAn10/anexo10_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==14 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo11_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==15 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo12a_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==16 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo12b_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==17 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo13_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==19 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo15_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }else if(ida==20 && desdebit==1){
    window.location.href=base_url+'Transaccion/anexo16_xml/'+idtran+'/'+idp+'/0/0/'+ido+'/'+idpagoa+"/1/"+clave+"/"+descrip;
  }
  swal("Éxito!", "Aviso generado y descargado", "success");
}

function get_alerta(id){
  $.ajax({
    type:'POST',
    url: base_url+"Aviso24/get_alertas",
    data: { id: id},
    success: function (response){
      //console.log(response);
      var array = $.parseJSON(response);
      $('#id_alerta').html("");
      if (array.length>0) {
        array.forEach(function(element) {
          $('#id_alerta').append('<option data-value="'+element.clave+'" value="'+element.id+'">'+element.indicador+'</option>');
        });
      }   
    },
    error: function(response){
        swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
    }
  });
}