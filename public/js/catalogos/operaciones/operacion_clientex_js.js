var usuario="espinozabe";
var pass="6B89401F";
var id_union=$('#id_union').val();
$(document).ready(function($) {

  var idc= $('#idcliente').val();
  var idp= $('#idperfilamiento').val()
  $.ajax({
    type: "POST",
    url: base_url+"Operaciones/get_clientes_select",
    data:{idc:idc,idp:idp},
    success: function (data){
      $('.select_clientes').html(data);
      $('#cliente_').select2({ sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)) });

    }
  });
 
  $("#si_exist").on("click",function(){
    $("#cont_cliente").show();
  });
  $("#no_existe").on("click",function(){
    $("#cont_cliente").hide();
    opera_nvo();
  });

  $("#si_mas").on("click",function(){
    window.location.href = base_url+"Operaciones/existente/"+$("#id_opera").val()+"";
  });
  $("#no_mas").on("click",function(){
    //debe mandar al listad de clientes para sobre ese asignar un(os) beneficiarios
    window.location.href = base_url+"Operaciones/clientesOperacion/"+$("#id_opera").val()+""; 
    //window.location.href = base_url+"Operaciones/asignarBene/"+$("#id_opera").val()+""; 
  });

  /* ************************/ 
  $("#si_exist_bene").on("click",function(){
    $("#cont_benes").show();
    nuevo_bene(0);
  });
  $("#no_existe_bene").on("click",function(){
    $("#cont_benes").hide();
    nuevo_bene(1);
  });
  $("#sin_beneficiario").on("click",function(){
    if($("#sin_beneficiario").is(":checked")==true){ 
      $("#cont_select_bene").hide();
      $("#beneficiario_").val("");
    }else{
      $("#cont_select_bene").show();
    }
  });

  $("#nombre_ejec_imp").text($(".nav-profile-name").text());
  $("#sin_beneficiario").on("click",function(){
    if($("#sin_beneficiario").is(":checked")){ 
      $("#cont_tipo_bene").hide();
      $("#beneficiario_").val("");
      $("#cont_select_bene").hide();
      $("#otro_beneficiario").prop("checked",false);
      $("#fisica").prop("checked",false);
      $("#moral").prop("checked",false);
      $("#fideicomiso").prop("checked",false);
      $(".btn-impresion").show();
      id_bene = "";
      tipo_bene = "";
    }else{
      $("#cont_select_bene").show();
      $(".btn-impresion").hide();
    }
  });

  /*************************/
});

function imprimir_formato1(){
  $("#modal_bene").modal('hide');//ocultamos el modal
  $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
  $('.modal-backdrop').remove();//eliminamos el backdrop del modal
  $('.firma_tipo_cliente').css('display','block');
  setTimeout(function(){ 
    $('.firma_tipo_cliente').css('display','none');
  }, 500);
  window.print();
}

function siguiente_exist(){ //primer paso-> guarda al cliente existente
  var id_opera=$("#id_opera").val();
  if($("#cliente_ option:selected").val()!=""){
    var datos = $("#cliente_").val().split('-');
    var idcs = datos[0];
    var idps = datos[1];
    var tipo = datos[2];
    var idunion = datos[3];
    $.ajax({
      type: "POST",
      url: base_url+"Operaciones/validar_exite_operacion",
      data:{ idop:$("#id_opera").val(), id_clientec:idcs},
      async: false,
      statusCode: {
        404: function(data){
            swal("Error!", "No Se encuentra el archivo", "error");
        },
        500: function(){
            swal("Error!", "500", "error");
        },
        503: function(){
          window.location.href = base_url+"Operaciones";
        }
      },
      success: function (data){
        if(data==0){
          $.ajax({
            type: "POST",
            url: base_url+"Operaciones/submit_operacion",
            async: false,
            data:{ id:$("#id_opera").val(), id_clientec:idcs, id_perfilamiento:idps, id_hist: 0, idunion:idunion },
            success: function (data){
              var array = $.parseJSON(data);
              var id=array.id;
              var id_hist=array.hist_cpb;
              var resultpb = array.resultpb;
              //comentado para pruea, se actualiza deesde el controlados por clientes de operacion
              /*$.ajax({
                type: "POST",
                url: base_url+"Perfilamiento/updateConsultaPBH",
                data:{ id: id_hist, resultado:resultpb},
                async: false,
                success: function (data){
                  console.log("historico actualizado para cliente existente");
                }
              });*/


              /*$.ajax({
                type: "POST",
                url: base_url+"Perfilamiento/get_cliente_datos",
                data:{ id_clientec:idcs, id_perfilamiento:idps, tipo: tipo },
                success: function (resultbn){
                  var array = $.parseJSON(data);  
                  var nombre="";
                  var apellidos="";
                  var identifica="";
                  var result_be="";
                  console.log("tipo: "+tipo);
                  if(tipo==1 || tipo==2){
                    nombre = array[0].nombre;
                    apellidos = array[0].apellido_paterno+" "+array[0].apellido_materno;
                    identifica = array[0].numero_identificacion;
                    if(array[0].r_f_c_t!=""){
                      identifica = array[0].numero_identificacion+"|"+array[0].r_f_c_t; 
                    }
                    if(array[0].curp_t!=""){
                      identifica = array[0].numero_identificacion+"|"+array[0].r_f_c_t+"|"+array[0].curp_t; 
                    }
                  }
                  if(tipo==3){
                    nombre = array[0].razon_social;
                    apellidos = "";
                    identifica = array[0].clave_registro;
                  }
                  if(tipo==4){
                    identifica = array[0].clave_registro;
                    identifica = array[0].clave_registro;
                    nombre = "";
                    apellidos = array[0].nombre_persona;
                    identifica = "";
                  }
                  if(tipo==5){
                    nombre = ""
                    apellidos = array[0].denominacion;
                    identifica = array[0].clave_registro;
                  }
                  if(tipo==6){
                    nombre = "";
                    apellidos = array[0].denominacion;
                    identifica = array[0].clave_registro+"|"+array[0].numero_referencia;
                  }

                  var json = { 
                      "Usuario" : usuario,
                      "Password" : pass,
                      "Apellido" : nombre,
                      "Nombre" : apellidos,
                      "Identificacion" : identifica,
                      "PEPS_otros_paises" : "S",
                      "Incluye_SAT" : "S",
                      "SATxDenominacion" : "S",
                  };

                  $.ajax({
                    type: "POST",
                    url: "https://www.prevenciondelavado.com/listas/api/busqueda",
                    //data: JSON.stringify(json),
                    data: json,
                    //async: false,
                    //contentType : 'application/json; charset=utf-8',
                    //dataType: 'json',
                    success: function (resultpb){
                      //console.log("resultpb: "+resultpb);
                      if(response.resultpb){
                        result_be = resultpb; 
                        $.ajax({
                          type: "POST",
                          url: base_url+"Perfilamiento/updateConsultaPBH",
                          data:{ id: id_hist, resultado_bene:result_be},
                          success: function (data){
                            console.log("historico actualizado para cliente existente");
                          }
                        });
                      }
                    }
                  });
                  //console.log("data de consulta: "+resultbn);
                 
                }
              });*/
              swal("Éxito", "Guardado Correctamente", "success");
              setTimeout(function(){ 
                //window.location.href = base_url+"Operaciones/clientesOperacion/"+id+"";
                window.location.href = base_url+"Operaciones/procesoInicial/"+id+"";

                //window.location.href = base_url+"Operaciones/proceso_operacion/"+id+"/"+idps+"/"+idcs;
              }, 1500);
            },error: function(response){
              window.location.href = base_url+"Operaciones";
            }
          })/*.fail( function( jqXHR, textStatus, errorThrown ) {
            window.location.href = base_url+"Operaciones";
          })*/;
        }else{
          swal("Atención", "El cliente ya existe en esta operación", "warning");
        }
      },error: function(response){
        window.location.href = base_url+"Operaciones";
      }
    });
      

  }else{
    swal("Error", "Elige un cliente", "warning");
  }
}

function opera_nvo(){
  if($("#id_opera").val()==0){
    $.ajax({
      type: "POST",
      url: base_url+"Operaciones/creaOperacion",
      async: false,
      success: function (data){
        var ido = data;
        swal("Éxito", "Guardado Correctamente", "success");
        setTimeout(function(){ 
          window.location.href = base_url+'Perfilamiento/index/1/'+ido+'';  
        }, 1500);
      }
    });
  }else{
    window.location.href = base_url+'Perfilamiento/index/1/'+$("#id_opera").val()+''; 
  }
}

function nuevo_bene(band){
  $("#modal_bene").modal();
  if(band==0){ //elegir uno
    $("#cont_select_bene").show();
    $("#cont_nvo_bene").hide();
    $("#otro_beneficiario").attr("checked",false);
  }else{
    $("#cont_nvo_bene").show();
    $("#cont_sin_bene").hide();
    $("#cont_select_bene").hide();
    $("#otro_beneficiario").attr("checked",true);
  }
  get_beneficiarios();
  if($("#sin_beneficiario").is(":checked")){
    $(".btn-impresion").show();
  }else{
    $(".btn-impresion").hide();
  }
}

function get_beneficiarios(){
  $.ajax({
    type: "POST",
    url: base_url+"Clientes_cliente/get_beneficiarios_select",
    data:{ idc:$("#id_clientec").val(), idp:$("#id_perfilamiento").val(), id_union:id_union, id_opera:$("#id_opera").val() },
    success: function (data){
      //console.log(data);
      $('.select_beneficiario').html(data);
      
      if($("#beneficiario_").val()!=""){
        ben = $("#beneficiario_").val()
        datos = ben.split('-');
        tipo_uri = datos[1];
        if(tipo_uri=="fisica"){
          uri_bene="detBene";
          $('.select_beneficiario').append('<a class="btn btn-secondary" href="'+base_url+'Clientes_c_beneficiario/'+uri_bene+'/'+nvoid_bene+'/'+idcc+'/'+nvoperfila+'/'+nvotipo_bene+'/'+$("#id").val()+'"><i class="fa fa-eye">  Detalles</i></a> '); 
        }
        if(tipo_uri=="moralmoral"){
          uri_bene="detBeneMoral";
          $('.select_beneficiario').append('<a class="btn btn-secondary" href="'+base_url+'Clientes_c_beneficiario/'+uri_bene+'/'+nvoid_bene+'/'+idcc+'/'+nvoperfila+"/"+tipo_cliente+'/'+$("#id").val()+'"><i class="fa fa-eye">  Detalles</i></a> '); 
          
        }
        if(tipo_uri=="fideicomiso"){
          uri_bene="detBeneFide";
          $('.select_beneficiario').append('<a class="btn btn-secondary" href="'+base_url+'Clientes_c_beneficiario/'+uri_bene+'/'+nvoid_bene+'/'+idcc+'/'+nvoperfila+"/"+tipo_cliente+'/'+$("#id").val()+'"><i class="fa fa-eye">  Detalles</i></a> '); 
        }
      } 
    }
  });
}

function check_beneficiario(){
  if($("#otro_beneficiario").is(":checked")){ 
    $("#cont_tipo_bene").show();
    $("#beneficiario_").val("");
    id_bene = "";
    tipo_bene = "";
  }else{
    $("#cont_tipo_bene").hide();
  }
}

function aceptarAdd(){
  if($("#beneficiario_").val()!=""){
    var extraer = $("#beneficiario_").val().split('-');
    id_bene = extraer[0];
    tipo_bene = extraer[1];
    id_bene_mm = extraer[2];
    //console.log("id_bene guardado: "+id_bene);
  }else{
    id_bene=0;
    tipo_bene="0";
  }
  //console.log(" cont_bene: "+cont_bene);
  //console.log(" id_bene: "+id_bene);
  if(tipo_bene=="fisica"){
    tipo_bene="1";
  }
  if(tipo_bene=="moralmoral" || tipo_bene=="moralfisica"){
    tipo_bene="2";
  }
  if(tipo_bene=="fideicomiso"){
    tipo_bene="3";
  }
  id_cliente=$("#id_clientec").val();
  idpefilanvo=$("#id_perfilamiento").val();;
  id_ob=0;

  if($("#beneficiario_").val()!="" || $("#sin_beneficiario").is(":checked")==true){
    $.ajax({
      type: "POST",
      url: base_url+"Operaciones/submit_operacionDB",
      data:{ id:$("#id_opera").val(), id_duenio_bene:id_bene, tipo_bene:tipo_bene, id_perfilamiento:idpefilanvo, id_cliente:id_cliente,id_ob:id_ob },
      beforeSend: function(){
        $("#btn_submit").attr("disabled",true);
     },
      success: function (data){
        console.log(data);
        var array = $.parseJSON(data);
        var id=array.id;
        $("#modal_bene").modal('hide');//ocultamos el modal
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        swal("Éxito", "Guardado Correctamente", "success");
        //inforclienteb(cont_bene);
        setTimeout(function(){ 
          window.location.href = base_url+"Operaciones/procesoInicial/"+$("#id_opera").val();
        }, 1500);
        
      }
    });
  }

  if($("#fisica").is(':checked'))
    window.location.href = base_url+"Clientes_c_beneficiario/addBene/0/"+$("#id_cliente").val()+"/"+idpefilanvo+"/"+$("#tipo_cliente").val()+"/"+$("#id_opera").val()+'/'+id_cliente+'/'+id_union;
  if($("#moral").is(':checked'))
    window.location.href = base_url+"Clientes_c_beneficiario/addBeneMoral/0/"+$("#id_cliente").val()+"/"+idpefilanvo+"/"+$("#tipo_cliente").val()+"/"+$("#id_opera").val()+'/'+id_cliente+'/'+id_union;
  if($("#fideicomiso").is(':checked'))
    window.location.href = base_url+"Clientes_c_beneficiario/addBeneFide/0/"+$("#id_cliente").val()+"/"+idpefilanvo+"/"+$("#tipo_cliente").val()+"/"+$("#id_opera").val()+'/'+id_cliente+'/'+id_union;
}

function inicio_cliente(){
  location.href= base_url+'Operaciones';
}
