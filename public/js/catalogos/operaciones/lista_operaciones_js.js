var base_url = $('#base_url').val();
$(document).ready(function($) {
	tabla=$("#table_opera").DataTable({
    //"bProcessing": true,
    //"serverSide": true,
    "ajax": {
     "url": base_url+"Operaciones/records_operaciones",
     type: "post",
    },
    "columns": [
      //{"data": "idopera"},
      {"data": "folio_cliente"},
      {"data": "fecha_reg"},
      {"data": null,
        "render": function ( data, type, row, meta ) {
          var html_cliente='';
          /*if(row.idtipo_cliente==1){
            html_cliente=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
          }else if(row.idtipo_cliente==2){
            html_cliente=row.nombre2+' '+row.apellido_paterno2+' '+row.apellido_materno2;
          }else if(row.idtipo_cliente==3){
            html_cliente=row.razon_social;
          }else if(row.idtipo_cliente==4){
            html_cliente=row.nombre_persona;
          }else if(row.idtipo_cliente==5){
            html_cliente=row.denominacion2;
          }else if(row.idtipo_cliente==6){
            html_cliente=row.denominacion;
          }*/
          if(row.id_union!=0){
            return row.cliente;
          }else{
            let cli = row.cliente
            const name = cli.split("/");
            //return name[0];
            return row.cliente;
          }
        }
      },
      //{"data": "anexo"},
      /*{"data": null,
        "render": function ( data, type, row, meta ) {
          if(row.anexo!="" && row.anexo!=null)
            msj="TERMINADA: "+row.anexo;
          else
            msj="EN PROCESO ";
        return msj;
        }
      },*/
      {"data": null,
        "render": function ( data, type, row, meta ) {
          var idopera=row.idopera;
          var dis_db="";
          var dis_gra=""; var dis_op="";
          if(row.idob=="N"){
            dis_db="disabled";
          }
          if(row.id_transaccion=="0"){
            dis_op="disabled";
          }
          if(row.grado==0){
            dis_gra="disabled";
          }/*else if(row.grado>2.51){
            console.log("ando alto carnal");
          }*/
          var sub="";
          if(row.idtipo_cliente!="6" && row.idtipo_cliente!="7"){
            sub="<button title='Ver Clientes Asignados' type='button' class='btn' onclick='clientes("+idopera+")'><i class='fa fa-user'></i></button>\
            <button "+dis_db+" title='Ver Dueños Beneficiarios' type='button' class='btn' onclick='duenos("+idopera+")'><i class='fa fa-users'></i></button>\
            <button "+dis_db+" title='Ver Resultados de busqueda Persona Bloqueada' type='button' class='btn' onclick='busqueda("+idopera+")'><i class='fa fa-search'></i></button>\
            <button "+dis_db+" title='Ver Calculadora de Grado de Riesgo' type='button' class='btn' onclick='calculadora("+idopera+")'><i class='fa fa-calculator'></i></button>\
            <button "+dis_db+" title='Ver Expediente de Cliente(s)' type='button' class='btn' onclick='expCliente("+idopera+")'><i class='fa fa-files-o'></i></button>\
            <button "+dis_db+" title='Ver Expediente de Dueño(s) Beneficio(s)' type='button' class='btn' onclick='expBene("+idopera+")'><i class='fa fa-files-o'></i></button>";
            sub+='<button '+dis_op+' '+dis_gra+' title="Ver Transacción" type="button" class="btn" onclick="detalleTransac('+idopera+')"><i class="mdi mdi-eye"></i></button>';
          }else{
            sub="<button title='Ver Clientes Asignados' type='button' class='btn' onclick='clientes("+idopera+")'><i class='fa fa-user'></i></button>\
            <button title='Ver Resultados de busqueda Persona Bloqueada' type='button' class='btn' onclick='busqueda("+idopera+")'><i class='fa fa-search'></i></button>\
            <button "+dis_gra+" title='Ver Calculadora de Grado de Riesgo' type='button' class='btn' onclick='calculadora("+idopera+")'><i class='fa fa-calculator'></i></button>\
            <button title='Ver Expediente de Cliente(s)' type='button' class='btn' onclick='expCliente("+idopera+")'><i class='fa fa-files-o'></i></button>";
            sub+='<button '+dis_op+' '+dis_gra+' title="Ver Transacción" type="button" class="btn" onclick="detalleTransac('+idopera+')"><i class="mdi mdi-eye"></i></button>';
          }
          return sub;
        }
      },
      {"data": null,
        "render": function ( data, type, row, meta ) {
          var dis="";
          var id_ct_bt=row.id_ct_bt;
          if(parseInt(id_ct_bt)>0){
            dis="disabled";
          }

          var html='<div class="btn-group"><button type="button" class="btn opera_nva" style="color: #212b4c;" title="Ver Proceso"><i class="fa fa-rocket" style="font-size: 22px;"></i></button>\
                  <button type="button" class="btn alertas_ver" style="color: #212b4c;" title="Ver Alertas"><i class="fa fa-eye" style="font-size: 22px;"></i></button>\
                  <button type="button" '+dis+' class="btn eliminar" style="color: #212b4c;" title="Eliminar"><i class="fa fa-trash" style="font-size: 22px;"></i></button></div>';        
          return html;
        }
      },
    ],
    "order": [[ 0, "desc" ]],
    rowCallback: function (row, data) {
      $(row).addClass('printcolor');
      //console.log("grado: "+data.grado);
      if(data.grado>=2.51 && data.id_autorizado==0) { 
        $('td:gt(-7)', row).css("background-color","rgba(255,0,0,0.7)");
        var desc = $(row).find('td:gt(-7)');
        desc.prop("title", "Operación "+data.idopera+" pendiente por autorizar");
        desc.attr("data-toggle", "tooltip");
      }   
    }
    }); 

    $('#table_opera').on('click', 'button.opera_nva', function () {
      var tr = $(this).closest('tr');
      var data = tabla.row(tr).data();
      if(data.idcc1!=0){
          idcc=data.idcc1;
      }
      if(data.idcc2!=0){
          idcc=data.idcc2;
      }
      if(data.idcc3!=0){
          idcc=data.idcc3;
      }
      if(data.idcc4!=0){
          idcc=data.idcc4;
      }
      if(data.idcc5!=0){
          idcc=data.idcc5;
      }
      if(data.idcc6!=0){
          idcc=data.idcc6;
      }
      //window.location.href = base_url+"Operaciones/proceso_operacion/"+data.idopera+"/"+data.idperfilamientop+"/"+idcc;
      window.location.href = base_url+"Operaciones/procesoInicial/"+data.idopera;
        
    });

    $('#table_opera').on('click', 'button.eliminar', function () {
      var tr = $(this).closest('tr');
      var data = tabla.row(tr).data();
      //window.location.href = base_url+"Operaciones/eliminarOperacion/"+data.idopera;
      eliminar(data.idopera);
      //console.log("idopera: "+data.idopera);
    });

    $('#table_opera').on('click', 'button.alertas_ver', function () {
      var tr = $(this).closest('tr');
      var data = tabla.row(tr).data();
      //window.location.href = base_url+"Operaciones/eliminarOperacion/"+data.idopera;
      if(data.id_actividad>0)
        alertasGet(data.idopera, data.id_actividad, data.transaccionalidad_forma,data.id_transaccion);
      //console.log("idopera: "+data.idopera);
    });

}); 

function alertasGet(ido,ida,trans_forma,idt){ //por ahora solo está aplicando para anexo8 vehiculos --01-09-21
  $("#detalles_alert").modal();

  $.ajax({
    type: 'POST',
    url : base_url+'Alertas/getPagos',
    async: false,
    data: { ida: ida, idt: idt },
    success: function(result){
      $(".body_alert").html("");
      var data= $.parseJSON(result);
      var html="";
      data.forEach(function(element) {
        //console.log("transac forma: "+trans_forma);
        instrum_comp=element.instrum_notario;
        idtipo_cliente=element.idtipo_cliente;
        clasificacion=element.clasificacion;
        preg1=element.preg1;
        //trans_forma=element.transaccionalidad_forma;

        if(idtipo_cliente==1 && clasificacion==16 || idtipo_cliente!=5){ //clientes No PEP
          if(trans_forma==1){ //lo que declaró en la calculadora
            if(instrum_comp!="10" && instrum_comp!="11" && instrum_comp!="12"){
              html+= "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción";
            }
          }
          if(trans_forma==2){ //lo que declaró en la calculadora
            if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9"){
              //console.log("instrum_comp 2: "+instrum_comp);
              html+= "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción";
            }
          }
          if(trans_forma==3){ //lo que declaró en la calculadora
            if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9" && instrum_comp!="10" && instrum_comp!="11"){
              html+= "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción";
            }
          }
          if(trans_forma==4){ //lo que declaró en la calculadora
            if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9"){
              html+= "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción";
            }
          }
          if(trans_forma==5 && instrum_comp==1){ //lo que declaró en la calculadora
            html+= "Toda vez que el instrumento de pago es efectivo, se recomienda validar la congruencia de la información y documentación presentada y los montos en efectivo a recibir";
          }
          if(trans_forma==5 && instrum_comp!=1){ //lo que declaró en la calculadora
            html+= "No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es) de la transacción";
          }
        }
        /* ******************************CLIENTES PEP**************************************** */
        if(idtipo_cliente==1 && clasificacion!=16 || idtipo_cliente==5){ //clientes PEP
          if(preg1==1 && instrum_comp==1){ //lo que declaró en la calculadora - El vehículo se liquidará total o parcialmente en efectivo
            html+="Toda vez que el instrumento de pago es efectivo, se recomienda validar la congruencia de la información y documentación presentada y los montos en efectivo a recibir";
          }
          if(preg1==1 && instrum_comp!=1){ //lo que declaró en la calculadora - El vehículo se liquidará total o parcialmente en efectivo
            html+="No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es) de la transacción";
          }
          if(preg1==2){ //lo que declaró en la calculadora - El vehículo se liquidará con transferencia bancaria nacional
            if(instrum_comp!="2" && instrum_comp!="3" && instrum_comp!="5" && instrum_comp!="8" && instrum_comp!="9"){
              html+="No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción";
            }
          }
          if(preg1==3){ //lo que declaró en la calculadora - El vehículo se liquidará con transferencia bancaria internacional
            if(instrum_comp!="10" && instrum_comp!="11" && instrum_comp!="12"){
              html+="No coincide la declaración de la forma de pago revelada en la entrevista inicial, respecto de la(s) forma(s) de pago real(es)  de la transacción";
            }
          }
        }
        
      });     
      $(".body_alert").append(html+"<br>");
    }
  });   
}
function detalleTransac(idopera){
  window.open(base_url+"Operaciones/Transaccion/"+idopera);
}
function clientes(id_operacion){
  window.open(base_url+"Operaciones/clientesOperacion/"+id_operacion, '_blank');
}
function duenos(id_operacion){
  window.open(base_url+"Operaciones/clientesOperacionBenes/"+id_operacion, '_blank');
}
function busqueda(id_operacion){
  window.open(base_url+"Operaciones/resultadosBusqueda/"+id_operacion, '_blank');
}
function calculadora(id_operacion){
  window.open(base_url+"Operaciones/gradoRiesgo/"+id_operacion, '_blank');
}
function expCliente(id_operacion){
  window.open(base_url+"Operaciones/documentosCliente/"+id_operacion, '_blank');
}
function expBene(id_operacion){
  window.open(base_url+"Operaciones/documentosBenes/"+id_operacion, '_blank');
}

function opera_nvo(){
  location.href= base_url+'Perfilamiento/index/1';	
}
function opera_exist(){
  //location.href= base_url+'Operaciones/existente';	
  location.href= base_url+'Operaciones/procesoInicial';  
}

function eliminar(id){
    title = "¿Desea eliminar este registro?";
    swal({
          title: title,
          text: "Se eliminará la operación del listado!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Operaciones/eliminarOperacion',
            data:{ id: id},
            success:function(data){
                tabla.ajax.reload();
                swal("Éxito", "Operación eliminada correctamente", "success");
            }
          }); //cierra ajax
       }
    });
}

function inicio_cliente(){
    location.href= base_url+'Inicio_cliente';
}

function href_generacion_xml(){
    location.href= base_url+'Operaciones/generacionxml';
}
function href_aviso_cero(){
  location.href= base_url+'Operaciones/aviso_cero';
} 

function href_aviso_exento(){
  location.href= base_url+'Operaciones/aviso_exento';
} 