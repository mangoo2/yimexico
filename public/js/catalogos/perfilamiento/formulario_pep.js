$(document).ready(function() {
    btn_existe_otro_accionista();
    var tipo_formulario = $('#tipo_formulario').val();
    if(tipo_formulario==4){
        getpais(4);
    }else{
        getpais(1);
        getpais(2);
        getpais(3);
        getpais(4);
        cambia_cp();
        setTimeout(function () { 
            var colonia_aux = $('#colonia_aux').val();
            $("#colonia option[value='"+colonia_aux+"']").attr("selected",true);
        }, 2000);
    }
    presenta_esposa();
    $('#no_hijos').change(function(event){
        cuantos_hijos();
    });

    $("#regresar").on("click",function(){
        if(history.back()!=undefined){
            history.back();
        }else{
          location.href= base_url+'Operaciones';  
        }
    });

});
function presenta_esposa(){
    if($('#si_esposa').is(':checked')==true){
        $("#cont_conyugue").show();
    }else{
       $("#cont_conyugue").hide(); 
    }
}
function guardar_formulario_pep(){
    var form_register = $('#form_formulario_pep');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            rfc_homeclave:{
              rfc: true
            },
            rfc_homeclave2:{
              rfc: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_formulario_pep").valid();
    if(valid){
    	//modal_imprimir();
        guardar_formulario_pep2();
        //modal_pregunta();
    }
}

var cont_tabla=0;
function cuantos_hijos(){
    var cant_depen = $("#no_hijos").val();
    var cont_row=0;
    if(Math.sign(cant_depen)==-1){
        swal("Advertencia!", "Ingresar un número positivo", "error");
        $("#no_hijos").val('');
    }
    /*if(cant_depen==0){
        swal("Advertencia!", "Ingresar un numero mayor a 0", "error");
        $("#no_hijos").val('');
    }*/
    /*if(cant_depen==1){
        $("#otros_dependientes").html('');
    }*/

    if($("#id").val()>0){
        cont_tabla = $(".row_hijos .datos_hijos > div").length;
        cont_row = cont_tabla;
    }
    var cant_depen_ant=0; 
    
    if(cant_depen>=1){
        var html_dep='';
        //cant_depen=cant_depen-1;
        //cant_depen = cant_depen-cant_depen_ant;
        
        if(cont_tabla>1 && cont_tabla<=1){
            cant_depen = cant_depen-cont_tabla-1;
        }
        else{
            cant_depen = cant_depen-cont_tabla;
        }
        /*if(Math.sign(cant_depen)==-1){
            nega = (-1);
            cant_depen = cant_depen * nega;
        }
        if(cont_tabla>cant_depen){
            cant_depen = cont_tabla-cant_depen;
        }*/

        //console.log("cant_depen: "+cant_depen);
        //console.log("cont_tabla: "+cont_tabla);
        for(c=0; c<cant_depen; c++){
            cont_row++;
            html_dep+='<!--<form class="form" method="post" role="form" id="form_cuestionario_otros" required>-->\
                <div class="row" id="row_'+cont_row+'">\
                <input class="form-control" type="hidden" id="id" value="0">\
                  <div class="col-md-8 form-group">\
                    <label>Nombre del hijo:</label>\
                    <input class="form-control" type="text" id="nombre">\
                  </div>\
                  <div class="col-md-4 form-group">\
                    <label>Edad del hijo:</label>\
                    <input class="form-control" type="text" id="edad">\
                  </div>\
                  <div class="col-md-8 form-group">\
                    <label>Nacionalidad del hijo:</label>\
                    <input class="form-control" type="text" id="nacionalidad">\
                  </div>\
                <div class="col-md-3 form-group">\
                  <label style="color:transparent">eliminar</label>\
                  <button type="button" class="btn btn_borrar" onclick="eliminaHijo('+cont_row+')"><i class="fa fa-minus"></i> </button>\
                </div>\
              </div>\
              <!--</form>-->';
        }
        $(".datos_hijos").append(html_dep);
        //cant_depen_ant = cant_depen+1;
        cont_tabla = $(".row_hijos .datos_hijos > div").length;   
        //console.log("cant_depen_ant: "+cant_depen_ant);
    }
}

function eliminaHijo(num_row){
    $('#row_'+num_row).remove();
    $("#no_hijos").val($(".row_hijos .datos_hijos > div").length);
}

function eliminaHijoId(id){
    swal({
        title: "¿Desea eliminar este registro?",
        text: "Se eliminará del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            url: base_url+'Perfilamiento/eliminarHijo',
            type: 'POST',
            data: { id: id},
            success: function(data){ 
                $('#row_'+id).remove();
            }
        });
    });
    
}

function guardarHijos(id){ //acá me quedo
    var DATAP= [];
    if($(".row_hijos .datos_hijos > div").length>0){
        var TABLAP = $(".row_hijos .datos_hijos > div");                  
        TABLAP.each(function(){         
            item = {};
            item ["id"]=$(this).find("input[id*='id']").val();
            item ["id_cuestionariopep"]=id;
            item ["nombre"]=$(this).find("input[id*='nombre']").val();
            item ["edad"]=$(this).find("input[id*='edad']").val();
            item ["nacionalidad"]=$(this).find("input[id*='nacionalidad']").val();

            DATAP.push(item);
        });
        INFOP  = new FormData();
        aInfop   = JSON.stringify(DATAP);
        INFOP.append('data', aInfop);
        $.ajax({
            data: INFOP,
            type: 'POST',
            url : base_url+'Perfilamiento/insert_hijos_pep',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data2){
                    //toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    //toastr.error('Error', '500');
                }
            },
            success: function(data2){
              
            }
        }); 
    }
}

function modal_imprimir(){
    $('#modalimprimir').modal();
}

function modal_pregunta(){
	$('#modal_accionista').modal();
}

function guardar_formulario_pep2() {
	$('.btn_registro').attr('disabled',true);
    if($('#si_esposa').is(':checked')==true){
        var si_esposa=1;
    }else{
        var si_esposa=0;
    }
	var datos = $('#form_formulario_pep').serialize()+"&si_esposa="+si_esposa;
    $.ajax({
	    type:'POST',
	    url: base_url+'Perfilamiento/add_formulario_pep',
	    data: datos,
	    statusCode:{
	      404: function(data){
	          swal("Error!", "No Se encuentra el archivo", "error");
	      },
	      500: function(){
	          swal("Error!", "500", "error");
	      }
	    },
	    success:function(data){
	        swal("Éxito", "Actualizado Correctamente", "success");
	        var pre=$('input:radio[name=existe_otro_accionista]:checked').val();
	        var idperfilamiento = $('#idperfilamiento').val();
	        var idtipo_cliente = $('#idtipo_cliente').val();
            guardarHijos(data);
            window.open(base_url+'Perfilamiento/generarPDF_FPEPPF/'+idperfilamiento+"/"+idtipo_cliente);
	        setTimeout(function () { 
				/*if(pre==1){
				    location.href= base_url+'Perfilamiento/formulariopep_accionista/'+idperfilamiento+'/'+idtipo_cliente;
				}else{
				    //location.href= base_url+'Clientes_cliente';
                    history.back();
				}*/
                //history.back();
                location.href= base_url+'Operaciones';
	        }, 1500);
	        
	    }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        window.location.href = base_url+"Operaciones";
      }); 
}

function imprimir_formato1(){
    var pre=$('input:radio[name=imprimir]:checked').val();
    if(pre==1){
        $('#modalimprimir').modal('hide');
        $('.btn_guardar_text').css('display','none');
        $('.texto_firma').css('display','block');
        setTimeout(function () { 
            window.print();
        }, 500);
        setTimeout(function () { 
            $('.btn_guardar_text').css('display','block');
            $('.texto_firma').css('display','none');
        }, 1000);
    }else{
        $('#modalimprimir').modal('hide');
        if($("#idtipo_cliente").val()==1 || $("#idtipo_cliente").val()==2)
            guardar_formulario_pep2();
        else
            modal_pregunta();
    }
}

function btn_existe_otro_accionista(){
    var pre=$('input:radio[name=funcion_desempena]:checked').val();
    if(pre==9){
        $('.text_espicificar').css('display','block');
    }else{
        $('.text_espicificar').css('display','none');
    }
}

function cambia_cp(){
  //console.log("id: "+id);
    cp = $("#codigo_postal").val();  
    //console.log("cp: "+cp);
    $('#colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Perfilamiento/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                /*console.log("estado: "+array[0].estado);
                console.log("mnpio: "+array[0].mnpio);
                console.log("ciudad: "+array[0].ciudad);*/
                //$("#estado").val(array[0].id_estado);
                    $("#estado").attr("readonly",true);
                    $("#estado option[value="+array[0].idestado+"]").attr("selected",true);
                    $("#delegacion_municipio").val(array[0].mnpio.toUpperCase());
                    $("#ciudad_poblacion").val(array[0].estado);
                    
                    $.ajax({
                        url: base_url+'Perfilamiento/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 
                            data.forEach(function(element){
                                $('#colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                        }
                     });
                }else{
                    $("#estado").attr("readonly",false);
                }

            }
        });


    }else{
        $("#estado").attr("readonly",false);
    }
    //});  
}

function getpais(id){
  //console.log("get pais2");
    $('#pais_'+id).select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Perfilamiento/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}


function imprimir_for(){
    /*$('.btn_guardar_text').css('display','none');
    $('.texto_firma').css('display','block');
    setTimeout(function () { 
        window.print();
    }, 500);
    setTimeout(function () { 
        $('.btn_guardar_text').css('display','block');
        $('.texto_firma').css('display','none');
    }, 1000);*/
    idp = $("#idperfilamiento").val();
    tipo = $("#idtipo_cliente").val();

    window.open(base_url+'Perfilamiento/generarPDF_FPEPPF/'+idp+"/"+tipo, '_blank');
}
