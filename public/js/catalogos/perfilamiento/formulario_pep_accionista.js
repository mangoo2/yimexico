$(document).ready(function() {
    btn_existe_otro_accionista();
    getpais(1);
    getpais(2);
    getpais(3);
    getpais(4);
    cambia_cp();
    setTimeout(function () { 
        var colonia_aux = $('#colonia_aux').val();
        $("#colonia option[value='"+colonia_aux+"']").attr("selected",true);
    }, 2000);

    $("#regresar").on("click",function(){
        if(history.back()!=undefined){
            history.back();
        }else{
          location.href= base_url+'Operaciones';  
        }
    });
    
});
function guardar_formulario_pep(){
    var form_register = $('#form_formulario_pep');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            rfc_homeclave:{
              rfc: true
            },
            rfc_homeclave2:{
              rfc: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_formulario_pep").valid();
    if(valid){
    	//modal_imprimir();
        guardar_formulario_pep2();
    }
}

function modal_imprimir(){
    $('#modalimprimir').modal();
}

function guardar_formulario_pep2() {
    $('.btn_registro').attr('disabled',true);
    var datos = $('#form_formulario_pep').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Perfilamiento/add_formulario_pep',
        data: datos,
        statusCode:{
          404: function(data){
              swal("Error!", "No Se encuentra el archivo", "error");
          },
          500: function(){
              swal("Error!", "500", "error");
          }
        },
        success:function(data){
            swal("Éxito", "Actualizado Correctamente", "success");
            idp = $("#idperfilamiento").val();
            tipo = $("#idtipo_cliente").val();
            window.open(base_url+'Perfilamiento/generarPDF_PEPAccion/'+idp+"/"+tipo);
            setTimeout(function () { 
                //location.href= base_url+'Clientes_cliente';
                //history.back();
                /*if(history.back()!=undefined){
                    location.history.back();
                }else{
                    location.href= base_url+'Clientes_cliente';
                }*/
                location.href= base_url+'Operaciones';
            }, 1500);
            
        }
    })
    .fail( function( jqXHR, textStatus, errorThrown ) {
        window.location.href = base_url+"Operaciones";
    }); 
}


function btn_existe_otro_accionista(){
    var pre=$('input:radio[name=funcion_desempena]:checked').val();
    if(pre==9){
        $('.text_espicificar').css('display','block');
    }else{
        $('.text_espicificar').css('display','none');
    }
}

function cambia_cp(){
  //console.log("id: "+id);
    cp = $("#codigo_postal").val();  
    //console.log("cp: "+cp);
    $('#colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Perfilamiento/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                /*console.log("estado: "+array[0].estado);
                console.log("mnpio: "+array[0].mnpio);
                console.log("ciudad: "+array[0].ciudad);*/
                //$("#estado").val(array[0].id_estado);
                    $("#estado").attr("readonly",true);
                    $("#estado option[value="+array[0].idestado+"]").attr("selected",true);
                    $("#delegacion_municipio").val(array[0].mnpio.toUpperCase());
                    $("#ciudad_poblacion").val(array[0].estado);
                    
                    $.ajax({
                        url: base_url+'Perfilamiento/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 
                            data.forEach(function(element){
                                $('#colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                        }
                     });
                }else{
                    $("#estado").attr("readonly",false);
                }

            }
        });


    }else{
        $("#estado").attr("readonly",false);
    }
    //});  
}

function autoComplete(id){
    //console.log("cp: "+cp);
    //console.log("id: "+id);
    col = $('#'+id+'').val(); 
    /*$('#'+id+'').autocomplete({
        delay: 300,
        source: function (request, response) {
            $.ajax({
                url: base_url+'Perfilamiento/getDatosCP',
                type: 'POST',
                dataType: "json",
                data: { cp: cp, col: col },
                success: function(json) {
                  response($.map(json, function(item) {
                    return {
                      label: item.colonia,
                      value: item.colonia,
                    }
                  }));
                }
            });
        }, 
        select: function(event, ui) { 
          $('#'+id+'').val(ui.item.value);
        }, 
        focus: function(event, ui) {
          return false;
        }
    });*/
    if(id=="colonia_d1"){
      cp= $("#cp_d1").val();  
    }
    if(id=="colonia_t2"){
      cp= $("#cp_t2").val();  
    }
    if(id=="colonia_p3"){
      cp= $("#cp_p3").val();  
    }
    //console.log("cp: "+cp);
    //console.log("col: "+col);
    $('#'+id+'').select2({
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione una colonia:',
        ajax: {
            url: base_url+'Perfilamiento/getDatosCPSelect',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp: cp, 
                    col: col ,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    cadena = element.colonia;
                    cadena = cadena.toUpperCase();
                    itemscli.push({
                        id: element.colonia,
                        text: cadena,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}

function getpais(id){
  //console.log("get pais2");
    $('#pais_'+id).select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un país',
        ajax: {
            url: base_url + 'Perfilamiento/searchpais',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: element.pais,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}

function imprimir_formato1(){
    var pre=$('input:radio[name=imprimir]:checked').val();
    if(pre==1){
        $('#modalimprimir').modal('hide');
        $('.btn_guardar_text').css('display','none');
        $('.texto_firma').css('display','block');
        setTimeout(function () { 
            window.print();
        }, 500);
        setTimeout(function () { 
            $('.btn_guardar_text').css('display','block');
            $('.texto_firma').css('display','none');
        }, 1000);
    }else{
        $('#modalimprimir').modal('hide');
        guardar_formulario_pep2();
    }
}

function imprimir_for(){
    /*$('.btn_guardar_text').css('display','none');
    $('.texto_firma').css('display','block');
    setTimeout(function () { 
        window.print();
    }, 500);
    setTimeout(function () { 
        $('.btn_guardar_text').css('display','block');
        $('.texto_firma').css('display','none');
    }, 1000); */
    idp = $("#idperfilamiento").val();
    tipo = $("#idtipo_cliente").val();

    window.open(base_url+'Perfilamiento/generarPDF_PEPAccion/'+idp+"/"+tipo, '_blank');
}
