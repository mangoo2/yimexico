var base_url=$('#base_url').val();
var table;
$(document).ready(function () {
        $(".select2").select2();
        var form_register = $('#form_sucursal');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                sucursal:{
                  required: true
                },
                representante:{
                  required: true
                },  
                correo:{
                  required: true
                },   
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        $('.guardarregistro').click(function(event) {
            var $valid = $("#form_sucursal").valid();
            if($valid) {
                var datos = form_register.serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Sucursales/registro',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        if (data==1) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else if (data==2) {
                            swal("Éxito", "Actualizado Correctamente", "success");
                        }
                        setTimeout(function(){ location.href= base_url+'Sucursales';
                        }, 2000);
                    }
                });
            }
        });
//=============================Data table ===================================//
        load();
});
function load(){  
    table = $('#table').DataTable({
            "ajax":{
                url : base_url+"Sucursales/getListado",
                type: "post",  
                    error: function(){
                       $("#table").css("display","none");
                     }
            },
            "columns": [
                    {"data": "idsucursal"},
                    {"data": "sucursal"}, 
                    {"data": "correo"},
                    {"data": "direccion"},
                    {"data": "telefono"},
                    {"data": null,
                        "render": function ( data, type, row, meta ) {
                            var cm = "'";
                        var html='<div class="btn-group-vertical" role="group" aria-label="Basic example">\
                                      <div class="btn-group">\
                                        <button type="button" class="btn dropdown-toggle" style="border-color: red;color: red;" data-toggle="dropdown">Acciones</button>\
                                        <div class="dropdown-menu">\
                                          <a class="dropdown-item" href="'+base_url+'Sucursales/add/'+row.idsucursal+'">Editar</a>\
                                          <a class="dropdown-item" onclick="modal_eliminar('+row.idsucursal+','+cm+row.sucursal+cm+')">Eliminar</a>\
                                        </div>\
                                      </div>\
                                  </div>';        
                        return html;
                        }
                    },

            ],
            "order": [[ 0, "desc" ]],
            "lengthMenu": [[25, 50, 100], [25, 50, 100]],

    });
}
function modal_eliminar(id,texto) {
    $('#password').val('');
    $('#modaleliminar').modal();
    $('#titulo_texto').html(texto);
    $('#idsucursal').val(id);
}
function eliminar() {
    $.ajax({
        type:'POST',
        url: base_url+'Sucursales/verificapass',
        data:{pass:$('#password').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                $('#modaleliminar').modal('hide');
                swal("Éxito", "Eliminado Correctamente", "success");
                setTimeout(function() { 
                    table.ajax.reload(); 
                }, 1000);
                $.ajax({
                    type:'POST',
                    url: base_url+'Sucursales/updateregistro',
                    data:{id:$('#idsucursal').val()},
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                    }
                });     
            }else{
                swal("Error!", "Contraseña Incorrecta", "error");
            }
        }
    }); 
}