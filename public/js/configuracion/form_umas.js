$(function () {
  ComboAnio();
  $('#form_umas').validate({
      rules: {
        anio: "required",
        valor: "required"
      },
      messages: {
        anio: "Ingrese año de UMA (4 dígitos)",
        valor: "Ingrese un valor",
      },
      submitHandler: function (form) {
        $.ajax({
           type: "POST",
           url: base_url+"Umas/submit",
           data: $(form).serialize(),
           beforeSend: function(){
              $(".guardarregistro").attr("disabled",true);
           },
           success: function (result) {
              //console.log(result);
              var idc=result;
              $(".guardarregistro").attr("disabled",false);
              swal("Éxito", "Guardado Correctamente", "success");
              setTimeout(function () { window.location.href = base_url+"Umas" }, 1500);
           }
        });
       return false; // required to block normal submit for ajax
       },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
  });

});

function ComboAnio(){
  var d = new Date();
  var n = d.getFullYear();
  var nm = n+10;
  var select = document.getElementById("anio");
  for(var i = n; i <= nm; i++) {
      var opc = document.createElement("option");
      opc.text = i;
      opc.value = i;
      select.add(opc)
  }
}