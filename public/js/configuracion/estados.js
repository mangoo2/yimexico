var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
//=============

    load();

});
function load(){
  //table_tipo.destroy();
  table=$("#table_datos").DataTable({
    "bProcessing": true,
    "serverSide": true,
    "ajax": {
       "url": base_url+"Estados/getlistado",
       type: "post",
       "data": function(d){
        // d.bodegaId = $('#bodegaId option:selected').val()
        },
        error: function(){
           $("#table_datos").css("display","none");
        }
    },
    "columns": [
        {"data": "clave"},
        {"data": "estado"},
        {"data": "calificacion"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
            var riesgo_texto='';   
            if(row.riesgo==2){
               riesgo_texto='Alta'; 
            }else if(row.riesgo==1){
               riesgo_texto='Baja';
            }     
            return riesgo_texto;
            }
        },
        {"data": null,
            "render": function ( data, type, row, meta ) {
            var html='<a class="btn dato_text_'+row.clave+'" data-estado="'+row.estado+'" data-calificacion="'+row.calificacion+'" data-riesgo="'+row.riesgo+'" title="Editar" onclick="modal_edit('+"'"+row.clave+"'"+')"><i class="fa fa-edit" style="color: #212b4c; font-size: 22px;"></i></a>'; 

                //html='<button type="button" class="btn btn-sm dato_text_'+row.clave+'" data-estado="'+row.estado+'" data-calificacion="'+row.calificacion+'" data-riesgo="'+row.riesgo+'" style="background-color: #00af79; color:white" title="Editar" onclick="modal_edit('+"'"+row.clave+"'"+')"><i class="fa fa-edit"></i></button>';        
            
            return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}
function modal_edit(clave) {
	$('#modal_editar').modal();
	$('#riesgo').find('option').remove();
	$('#riesgo').prepend("<option value='2' >Alto</option>");
	$('#riesgo').prepend("<option value='1' >Bajo</option>");
	$('.estado_nombre').html($('.dato_text_'+clave).data('estado'));
	$('#calificacion').val($('.dato_text_'+clave).data('calificacion'));
	var riesgo = $('.dato_text_'+clave).data('riesgo');
	$('#riesgo option[value="'+riesgo+'"]').attr("selected","selected");
	$('#clave').val(clave);
}
function editar(){
	var form_register = $('#form_estado');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                calificacion:{
                  required: true
                } 
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#form_estado").valid();
        if($valid) {
            var datos = form_register.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'Estados/registro',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                	$('#modal_editar').modal('hide');
                    swal("Éxito", "Actualizado Correctamente", "success");
                    table.ajax.reload();
                }
            });
        }
      
}