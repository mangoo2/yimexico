var base_url = $('#base_url').val();
function tipo_riesgo(){
	var tipo = $('#riesgo option:selected').val();
	if(tipo==1){
        location.href= base_url+'Paises/lugares_frontera';
	}else if(tipo==2){
        location.href= base_url+'Paises/puertos_entrada_salida_acuerdo_sistema_portuario_nacional';
	}else if(tipo==3){
        location.href= base_url+'Paises/indice_basilea';
	}else if(tipo==4){
        location.href= base_url+'Paises/indice_secrecia_bancaria';
	}else if(tipo==5){
        location.href= base_url+'Paises/indice_corrupcion';
	}else if(tipo==6){
		location.href= base_url+'Paises/indice_gafi';
	}else if(tipo==7){
        location.href= base_url+'Paises/incidencia_delictiva';
	}else if(tipo==8){
        location.href= base_url+'Paises/percepcion_corrupcion_estatal_zonas_urbanas';
	}else if(tipo==9){
		location.href= base_url+'Paises/indice_paz';
	}else if(tipo==10){
        location.href= base_url+'Paises/paises_guia_terrorismo';
	}else if(tipo==11){
        location.href= base_url+'Paises/indice_global_terrorismo';
	}else if(tipo==12){
		location.href= base_url+'Paises/paises_indice_corrupcion_global';
	}else if(tipo==13){
        location.href= base_url+'Paises/paises_regimenes_fiscales_preferentes';
	}
}