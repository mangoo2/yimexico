$(document).ready(function () {
    load(); 
    $("#moneda").on("change",function(){
        load();
    });
});

function load(){
    table = $('#table_datos').DataTable({
        destroy: true,
        "ajax": {
           "url": base_url+"Divisas/datatable_records",
           type: "post",
           data: { moneda: $("#moneda option:selected").val()}
        },
        "columns": [
            {"data": "id"},
            {"data": "fecha_format"},
            {"data": null,
                "render": function ( url, type, full) {
                    msj ="";
                    if(full["moneda"]==1)
                        msj="Dólar Americano";
                    else if(full["moneda"]==2)
                        msj="Euro";
                    else if(full["moneda"]==3)
                        msj="Libra Esterlina";
                    else if(full["moneda"]==4)
                        msj="Dólar canadiense";
                    else if(full["moneda"]==5)
                        msj="Yuan Chino";
                    else if(full["moneda"]==6)
                        msj="Centenario";
                    else if(full["moneda"]==7)
                        msj="Onza de Plata";
                    else if(full["moneda"]==8)
                        msj="Onza de Oro";

                    return msj;

                }
            },
            {"data": null,
                "render": function ( url, type, full) {
                    var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['valor']);
                    return rw;
                }
            },
            {"data": null,
                "render": function ( url, type, full) {
                    var btn="";
                    if($("#tipo").val()==1){
                        btn="<button id='edit' title='Editar' class='btn edit'><i class='fa fa-edit' style='color: #212b4c; font-size: 22px;'></i></button>\
                        <button id='elimina' title='Eliminar' class='btn delete'><i class='fa fa-trash' style='color: #212b4c; font-size: 22px;'></i></button>";
                    }
                    return btn;
                }
            },
            /*{
                "data": null,
                "defaultContent": "<button id='edit' title='Editar' class='btn edit'><i class='fa fa-edit' style='color: #212b4c; font-size: 22px;'></i></button>\
                    <button id='elimina' title='Eliminar' class='btn delete'><i class='fa fa-trash' style='color: #212b4c; font-size: 22px;'></i></button>"
            }*/
        ],
        "order": [[ 0, "desc" ]],
        "pageLength": 25
    });
}

$('#table_datos').on('click', 'button.edit', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    var data = row.data();
    window.location = base_url+'Divisas/alta/'+data.id;
});

$('#table_datos').on('click', 'button.delete', function () {
    var tr = $(this).closest('tr');
    var data = table.row(tr).data();
    delete_record(data.id);
});

function delete_record(id) {
    title = "¿Desea eliminar este registro?";
    swal({
          title: title,
          text: "Se eliminará definitivamente del listado!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
        }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Divisas/eliminar',
            data:{ id: id},
            success:function(data){
              load();
              swal("Éxito", "Eliminado correctamente", "success");
            }
          }); //cierra ajax
        }
    });

} 
