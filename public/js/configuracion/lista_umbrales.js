$(document).ready(function () {
    load(); 
});

function load(){
    table = $('#table_datos').DataTable({
        destroy: true,
        "ajax": {
           "url": base_url+"Umbrales/datatable_records",
           type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "anexo"},
            /*{"data": "identificacion"},
            {"data": "aviso"},*/
            {"data": null,
                "render": function ( url, type, full) {
                    if(full["identificacion"]>0 && full["identificacion"]!=""){
                        rw=full['identificacion'];
                    }else if(full["siempre_iden"]==1){
                        rw="SIEMPRE";
                    }
                    return rw;
                }
            },
            {"data": null,
                "render": function ( url, type, full) {
                    if(full["aviso"]>0 && full["aviso"]!=""){
                        rw2=full['aviso'];
                    }else if(full["siempre_avi"]==1){
                        rw2="SIEMPRE";
                    }
                    return rw2;
                }
            },
            {"data": null,
                "render": function ( url, type, full) {
                    if(full["limite_efectivo"]>=0 && full["limite_efectivo"]!=""){
                        //rw3=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['limite_efectivo']);
                        rw3=full['limite_efectivo'];
                    }else if(full["na"]==1){
                        rw3="NA";
                    }
                    return rw3;
                }
            },
            {
                "data": null,
                "defaultContent": "<button id='edit' title='Editar' class='btn edit'><i class='fa fa-edit' style='color: #212b4c; font-size: 22px;'></i></button>"
            }
        ],
        "order": [[ 0, "asc" ]],
        "pageLength": 25
    });
}

$('#table_datos').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'Umbrales/alta/'+data.id;
});
