var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
//=============
    load();
});
function load(){
  //table_tipo.destroy();
  table=$("#table_datos").DataTable({
    "ajax": {
       "url": base_url+"Configuracion/datatable_records",
       type: "post",
    },
    "columns": [
        {"data": "nombre"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
            var html='<a class="btn dato_text_'+row.id_cliente_actividad+'" data-permite="'+row.permite_avance+'" data-anexo="'+row.idactividad+'" data-cliente="'+row.idcliente+'" data-actividad="'+row.actividad+'" title="Editar" onclick="modal_edit('+"'"+row.idconfig+"'"+','+"'"+row.permite_avance+"'"+','+"'"+row.id_cliente_actividad+"'"+')"><i class="fa fa-edit" style="color: #212b4c; font-size: 22px;"></i></a>';         
             
            return html;
            }
        },
    ],
    "order": [[ 1, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}
function modal_edit(id,permite,id_ca) {
	$('#modal_editar').modal();
    sel="";sel2="";sel3="";
    if(permite=="0"){
        sel="selected";
    }
    else if(permite=="1"){
        sel2="selected";
    }
    else if(permite=="n"){
        sel3="selected";
    }
    $('#permite_avance').find('option').remove();
    $('#permite_avance').prepend("<option "+sel3+" value=''></option>");
    $('#permite_avance').prepend("<option "+sel+" value='0' >No</option>");
    $('#permite_avance').prepend("<option "+sel2+" value='1' >Si</option>");
	$('.actividad_nombre').html($('.dato_text_'+id_ca).data('actividad'));
    $('#id_cliente').val($('.dato_text_'+id_ca).data('cliente'));
    $('#id_anexo').val($('.dato_text_'+id_ca).data('anexo'));
	$('#id').val(id);
}
function editar(){
	var form_register = $('#form_actividad');
    var datos = form_register.serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Configuracion/submit',
        data: datos,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
        	$('#modal_editar').modal('hide');
            swal("Éxito", "Actualizado Correctamente", "success");
            table.ajax.reload();
        }
    });     
}