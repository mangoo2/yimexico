$(function () {
  $('#form_divisas').validate({
      rules: {
        fecha: "required",
        moneda: "required",
        valor: "required"
      },
      messages: {
        fecha: "Ingrese fecha de connfiguración",
        moneda: "Ingrese tipo de moneda",
        valor: "Ingrese un valor"
      },
      submitHandler: function (form) {
        $.ajax({
           type: "POST",
           url: base_url+"Divisas/submit",
           data: $(form).serialize(),
           beforeSend: function(){
              $(".guardarregistro").attr("disabled",true);
           },
           success: function (result) {
              //console.log(result);
              var idc=result;
              $(".guardarregistro").attr("disabled",false);
              swal("Éxito", "Guardado Correctamente", "success");
              setTimeout(function () { window.location.href = base_url+"Divisas" }, 1500);
           }
        });
       return false; // required to block normal submit for ajax
       },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
  });

});

