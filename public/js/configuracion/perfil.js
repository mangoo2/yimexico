var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
//=============Form perfil
        var form_register = $('#form_perfil');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                perfil:{
                  required: true
                },   
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        $('.guardarregistro').click(function(event) {
            var $valid = $("#form_perfil").valid();
            if($valid) {
                var datos = form_register.serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Perfiles/registro',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        var array = $.parseJSON(data);
                        if (array.status==1) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else if (array.status==2) {
                            swal("Éxito", "Actualizado Correctamente", "success");
                        }
                        setTimeout(function(){ location.href= base_url+'Perfiles';
                        }, 2000);
                    // Guardar detalles de perfil
                        var idpro=array.id;
                        ///=====1
                        var DATA1  = [];
                        var TABLA1 = $(".submenu1 div > label");
                            TABLA1.each(function(){ 
                                if ($(this).find("input[id*='MenusubId']").is(":checked")) {
                                   item = {};
                                    item ["perfilId"] = idpro;
                                    item ["MenusubId"]   = $(this).find("input[id*='MenusubId']").val();
                                    DATA1.push(item); 
                                }        
                            });
                            aInfo1   = JSON.stringify(DATA1);
                        ///=====2
                        var DATA2  = [];
                        var TABLA2 = $(".submenu2 div > label");
                            TABLA2.each(function(){ 
                                if ($(this).find("input[id*='MenusubId']").is(":checked")) {
                                   item = {};
                                    item ["perfilId"] = idpro;
                                    item ["MenusubId"]   = $(this).find("input[id*='MenusubId']").val();
                                    DATA2.push(item); 
                                }        
                            });
                            aInfo2   = JSON.stringify(DATA2);
                        ///=====3
                        var DATA3  = [];
                        var TABLA3 = $(".submenu3 div > label");
                            TABLA3.each(function(){ 
                                if ($(this).find("input[id*='MenusubId']").is(":checked")) {
                                    item = {};
                                    item ["perfilId"] = idpro;
                                    item ["MenusubId"]   = $(this).find("input[id*='MenusubId']").val();
                                    DATA3.push(item); 
                                }        
                            });
                            aInfo3   = JSON.stringify(DATA3);
                        ///=====4
                        var DATA4  = [];
                        var TABLA4 = $(".submenu4 div > label");
                            TABLA4.each(function(){ 
                                if ($(this).find("input[id*='MenusubId']").is(":checked")) {
                                    item = {};
                                    item ["perfilId"] = idpro;
                                    item ["MenusubId"]   = $(this).find("input[id*='MenusubId']").val();
                                    DATA4.push(item); 
                                }        
                            });
                            aInfo4 = JSON.stringify(DATA4);
                            
                        ///================================    
                            var INFO = 'submenu1='+aInfo1+'&submenu2='+aInfo2+'&submenu3='+aInfo3+'&submenu4='+aInfo4;
                            $.ajax({
                                data: INFO, 
                                type: 'POST',
                                url : base_url + 'Perfiles/guardar_perfil_detalles',
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        swal("Error!", "No Se encuentra el archivo", "error");
                                    },
                                    500: function(){
                                        swal("Error!", "500", "error");
                                    }
                                },
                                success: function(data){
                                }
                            });
                    }
                });
            }
        });    
//=============Data table ===========//
    load();
});
function load(){
  //table_tipo.destroy();
  table=$("#table").DataTable({
    "bProcessing": true,
    "serverSide": true,
    "ajax": {
       "url": base_url+"Perfiles/getlistregistro",
       type: "post",
       "data": function(d){
        // d.bodegaId = $('#bodegaId option:selected').val()
        },
        error: function(){
           $("#table").css("display","none");
        }
    },
    "columns": [
        {"data": "perfilId"},
        {"data": "perfil"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var cm = "'";
                var btn_elimnar = '';
            if(row.perfilId==1){
                btn_elimnar=''; 
            }else if(row.perfilId==2){
                btn_elimnar='';
            }else{
                btn_elimnar='<a class="dropdown-item" onclick="modal_eliminar('+row.perfilId+','+cm+row.perfil+cm+')">Eliminar</a>';
            }    
            var html='<div class="btn-group-vertical" role="group" aria-label="Basic example">\
                          <div class="btn-group">\
                            <button type="button" class="btn dropdown-toggle" style="border-color: #293353;color: #293353;" data-toggle="dropdown">Acciones</button>\
                            <div class="dropdown-menu">\
                              <a class="dropdown-item" onclick="registro('+row.perfilId+','+cm+row.perfil+cm+')">Editar</a>\
                              '+btn_elimnar+'\
                              </div>\
                          </div>\
                      </div>';        
            return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}
function registro(id,perfil) {
    $('#perfilId').val(id);
    $('#perfil').val(perfil);
    $('.titulo_text').html('Edición de');
    $('.btn_text').html('Editar');
    $('.form-check-input').prop('checked',false);
    $.ajax({
        url: base_url+'Perfiles/visualizar_permisos',
        dataType: 'json',
        data: {id:id},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
           var datos=data;
                datos.forEach(function(r) {
                      $(".check_s_"+r.MenusubId).prop('checked', true);
                });
        }
    });
    $('.btn_cancelar').html('<a class="btn btn-secondary btn-rounded btn-fw" href="'+base_url+'Perfiles">Regresar</a>');

}
function modal_eliminar(id,texto) {
    $('#password').val('');
    $('#modaleliminar').modal();
    $('#titulo_texto').html(texto);
    $('#perfilId_e').val(id);
}
function eliminar() {
    $.ajax({
        type:'POST',
        url: base_url+'Perfiles/verificapass',
        data:{pass:$('#password').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                $('#modaleliminar').modal('hide');
                swal("Éxito", "Eliminado Correctamente", "success");
                setTimeout(function() { 
                    table.ajax.reload(); 
                }, 1000);
                $.ajax({
                    type:'POST',
                    url: base_url+'Perfiles/deleteregistro',
                    data:{id:$('#perfilId_e').val()},
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                    }
                });     
            }else{
                swal("Error!", "Contraseña Incorrecta", "error");
            }
        }
    }); 
}