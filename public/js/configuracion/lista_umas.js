$(document).ready(function () {
    load(); 
});

function load(){
    table = $('#table_datos').DataTable({
        destroy: true,
        "ajax": {
           "url": base_url+"Umas/datatable_records",
           type: "post",
            error: function(){
               //$("#tabla_perfs").css("display","none");
            }
        },
        "columns": [
            {"data": "anio"},
            {"data": null,
                "render": function ( url, type, full) {
                    var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['valor']);
                    return rw;
                }
            },
            {
                "data": null,
                "defaultContent": "<button id='edit' style='color: #212b4c;' title='Editar' class='btn edit'><i class='fa fa-edit' style='font-size: 22px;'></i></button>\
                    <button id='elimina' style='color: #212b4c;' title='Eliminar' class='btn delete'><i class='fa fa-trash' style='font-size: 22px;'></i></button>"
            }
        ],
        "pageLength": 25
    });
}

$('#table_datos').on('click', 'button.edit', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    var data = row.data();
    window.location = base_url+'Umas/alta/'+data.id;
});

$('#table_datos').on('click', 'button.delete', function () {
    var tr = $(this).closest('tr');
    var data = table.row(tr).data();
    delete_record(data.id);
});

function delete_record(id) {
    title = "¿Desea eliminar este registro?";
    swal({
          title: title,
          text: "Se eliminará definitivamente del listado!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Aceptar",
          closeOnConfirm: true
        }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type:'POST',
            url: base_url+'Umas/eliminar',
            data:{ id: id},
            success:function(data){
              load();
              swal("Éxito", "Eliminado correctamente", "success");
            }
          }); //cierra ajax
        }
    });

} 
