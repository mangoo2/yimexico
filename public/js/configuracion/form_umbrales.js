$(function () {
  /*$(".anexoconfig_12s_"+$("#sub_anexo option:selected").val()+"").show();
  $("#sub_anexo").on("change", function(){
    $(".anexoconfig_12s_"+this.value+"").show();
    if(this.value==1){
      $(".anexoconfig_12s_2").hide();
      $(".anexoconfig_12s_3").hide();
      $(".anexoconfig_12s_4").hide();
      $(".anexoconfig_12s_5").hide();
    }
    if(this.value==2){
      $(".anexoconfig_12s_1").hide();
      $(".anexoconfig_12s_3").hide();
      $(".anexoconfig_12s_4").hide();
      $(".anexoconfig_12s_5").hide();
    }
    if(this.value==3){
      $(".anexoconfig_12s_1").hide();
      $(".anexoconfig_12s_2").hide();
      $(".anexoconfig_12s_4").hide();
      $(".anexoconfig_12s_5").hide();
    }
    if(this.value==4){
      $(".anexoconfig_12s_1").hide();
      $(".anexoconfig_12s_2").hide();
      $(".anexoconfig_12s_3").hide();
      $(".anexoconfig_12s_5").hide();
    }
    if(this.value==5){
      $(".anexoconfig_12s_1").hide();
      $(".anexoconfig_12s_2").hide();
      $(".anexoconfig_12s_3").hide();
      $(".anexoconfig_12s_4").hide();
    }
  });*/
  $("#siempre_iden").on("click",function(){
    verificaChecks();
  });
  $("#siempre_avi").on("click",function(){
    verificaChecks();
  });
  $("#na").on("click",function(){
    verificaChecks();
  });
  /* ************************ */
  $("#siempre_iden_1").on("click",function(){
    verificaChecks();
  });
  $("#siempre_avi_1").on("click",function(){
    verificaChecks();
  });
  $("#na_1").on("click",function(){
    verificaChecks();
  });
  /* ************************ */
  $("#siempre_iden_2").on("click",function(){
    verificaChecks();
  });
  $("#siempre_avi_2").on("click",function(){
    verificaChecks();
  });
  $("#na_2").on("click",function(){
    verificaChecks();
  });
  /* ************************ */
  $("#siempre_iden_3").on("click",function(){
    verificaChecks();
  });
  $("#siempre_avi_3").on("click",function(){
    verificaChecks();
  });
  $("#na_3").on("click",function(){
    verificaChecks();
  });
  /* ************************ */
  $("#siempre_iden_4").on("click",function(){
    verificaChecks();
  });
  $("#siempre_avi_4").on("click",function(){
    verificaChecks();
  });
  $("#na_4").on("click",function(){
    verificaChecks();
  });
  /* ************************ */
  $("#siempre_iden_5").on("click",function(){
    verificaChecks();
  });
  $("#siempre_avi_5").on("click",function(){
    verificaChecks();
  });
  $("#na_5").on("click",function(){
    verificaChecks();
  });

  $('#form_umbrales').validate({
      rules: {
        id_anexo_gral: "required",
        //identificacion: "required",
        //aviso: "required"
      },
      messages: {
        id_anexo_gral: "Seleccione un anexo",
        //identificacion: "Ingrese un monto de identificación",
        //aviso: "Ingrese un monto de aviso"
      },
      submitHandler: function (form) {
        if($("#id_anexo_gral option:selected").val()!=15 && $("#id_anexo_gral option:selected").val()!=16){
          var siempre_iden = $('#siempre_iden').is(':checked') == true ? 1 : 0;
          var siempre_avi = $('#siempre_avi').is(':checked') == true ? 1 : 0;
          var na = $('#na').is(':checked') == true ? 1 : 0;
          $.ajax({
             type: "POST",
             url: base_url+"Umbrales/submit",
             data: $(form).serialize()+"&siempre_iden="+siempre_iden+"&siempre_avi="+siempre_avi+"&na="+na,
             beforeSend: function(){
                $(".guardarregistro").attr("disabled",true);
             },
             success: function (result) {
                //console.log(result);
                var idc=result;
                $(".guardarregistro").attr("disabled",false);
                swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function () { window.location.href = base_url+"Umbrales" }, 1500);
             }
          });
        }else{
          if($("#form_umbrales > #anexoconfig_12s").length>0) {
            //console.log("guarda pagos de liquidacion");
            var DATA  = [];
            var i=0;
            var TABLA   = $("#form_umbrales > #anexoconfig_12s");                  
            TABLA.each(function(){     
              i++;
              console.log("i: "+i);
              var siempre_iden = $('#siempre_iden_'+i+'').is(':checked') == true ? 1 : 0;
              var siempre_avi = $('#siempre_avi_'+i+'').is(':checked') == true ? 1 : 0;
              var na = $('#na_'+i+'').is(':checked') == true ? 1 : 0;    
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["id_anexo_gral"] = $("select[id*='id_anexo_gral'] option:selected").val();
                //item ["sub_anexo"] = $(this).find("select[id*='sub_anexo'] option:selected").val();
                item ["sub_anexo"] = $(this).find("textarea[id*='sub_anexo']").data("val");
                item ["identificacion"] = $(this).find("input[id*='identificacion']").val();
                item ["aviso"] = $(this).find("input[id*='aviso']").val();
                item ["limite_efectivo"] = $(this).find("input[id*='limite_efectivo']").val();
                item ["siempre_iden"] = siempre_iden;
                item ["siempre_avi"] = siempre_avi;
                item ["na"] = na;
                DATA.push(item);
            });

            INFO  = new FormData();
            aInfo   = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            //console.log("aInfo: "+aInfo);
            //console.log("INFO: "+INFO);
            //console.log("DATA: "+DATA);

            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Umbrales/submit12a',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                  404: function(data2){
                    //toastr.error('Error!', 'No hay datos cargados');
                  },
                  500: function(){
                    //toastr.error('Error', 'Error 500, falla de conexción con servidor');
                  }
                },
                success: function(data2){
                  $(".guardarregistro").attr("disabled",false);
                  swal("Éxito", "Guardado Correctamente", "success");
                  setTimeout(function () { window.location.href = base_url+"Umbrales" }, 1500);
                }
            }); 
          }
        }
       return false; // required to block normal submit for ajax
       },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
  });

  verificaChecks();

});

function verificaChecks(){
  if($("#siempre_iden").is(":checked")==true){
    $("input[name*='identificacion']").val("");
    $("input[name*='identificacion']").attr("readonly",true);
  }else{
    $("input[name*='identificacion']").attr("readonly",false); 
  } 

  if($("#siempre_avi").is(":checked")==true){
    $("input[name*='aviso']").val("");
    $("input[name*='aviso']").attr("readonly",true);
  }else{
    $("input[name*='aviso']").attr("readonly",false); 
  } 

  if($("#na").is(":checked")==true){
    $("input[name*='limite_efectivo']").val("");
    $("input[name*='limite_efectivo']").attr("readonly",true);
  }else{
    $("input[name*='limite_efectivo']").attr("readonly",false); 
  } 
 /* ***************************************** */
  if($("#siempre_iden_1").is(":checked")==true){
    $("#identificacion_1").val("");
    $("#identificacion_1").attr("readonly",true);
  }else{
    $("#identificacion_1").attr("readonly",false); 
  } 

  if($("#siempre_avi_1").is(":checked")==true){
    $("input[id*='aviso_1']").val("");
    $("input[id*='aviso_1']").attr("readonly",true);
  }else{
    $("input[id*='aviso_1']").attr("readonly",false); 
  } 

  if($("#na_1").is(":checked")==true){
    $("input[id*='limite_efectivo_1']").val("");
    $("input[id*='limite_efectivo_1']").attr("readonly",true);
  }else{
    $("input[id*='limite_efectivo_1']").attr("readonly",false); 
  } 

  /* ************************** */
  if($("#siempre_iden_2").is(":checked")==true){
    $("#identificacion_2").val("");
    $("#identificacion_2").attr("readonly",true);
  }else{
    $("#identificacion_2").attr("readonly",false); 
  } 

  if($("#siempre_avi_2").is(":checked")==true){
    $("input[id*='aviso_2']").val("");
    $("input[id*='aviso_2']").attr("readonly",true);
  }else{
    $("input[id*='aviso_2']").attr("readonly",false); 
  } 

  if($("#na_2").is(":checked")==true){
    $("input[id*='limite_efectivo_2']").val("");
    $("input[id*='limite_efectivo_2']").attr("readonly",true);
  }else{
    $("input[id*='limite_efectivo_2']").attr("readonly",false); 
  } 
  /* ************************** */
  if($("#siempre_iden_3").is(":checked")==true){
    $("#identificacion_3").val("");
    $("#identificacion_3").attr("readonly",true);
  }else{
    $("#identificacion_3").attr("readonly",false); 
  } 

  if($("#siempre_avi_3").is(":checked")==true){
    $("input[id*='aviso_3']").val("");
    $("input[id*='aviso_3']").attr("readonly",true);
  }else{
    $("input[id*='aviso_3']").attr("readonly",false); 
  } 

  if($("#na_3").is(":checked")==true){
    $("input[id*='limite_efectivo_3']").val("");
    $("input[id*='limite_efectivo_3']").attr("readonly",true);
  }else{
    $("input[id*='limite_efectivo_3']").attr("readonly",false); 
  }
  /* ************************** */
  if($("#siempre_iden_4").is(":checked")==true){
    $("#identificacion_4").val("");
    $("#identificacion_4").attr("readonly",true);
  }else{
    $("#identificacion_4").attr("readonly",false); 
  } 

  if($("#siempre_avi_4").is(":checked")==true){
    $("input[id*='aviso_4']").val("");
    $("input[id*='aviso_4']").attr("readonly",true);
  }else{
    $("input[id*='aviso_4']").attr("readonly",false); 
  } 

  if($("#na_4").is(":checked")==true){
    $("input[id*='limite_efectivo_4']").val("");
    $("input[id*='limite_efectivo_4']").attr("readonly",true);
  }else{
    $("input[id*='limite_efectivo_4']").attr("readonly",false); 
  }
  /* ************************** */
  if($("#siempre_iden_5").is(":checked")==true){
    $("#identificacion_5").val("");
    $("#identificacion_5").attr("readonly",true);
  }else{
    $("#identificacion_5").attr("readonly",false); 
  } 

  if($("#siempre_avi_5").is(":checked")==true){
    $("input[id*='aviso_5']").val("");
    $("input[id*='aviso_5']").attr("readonly",true);
  }else{
    $("input[id*='aviso_5']").attr("readonly",false); 
  } 

  if($("#na_5").is(":checked")==true){
    $("input[id*='limite_efectivo_5']").val("");
    $("input[id*='limite_efectivo_5']").attr("readonly",true);
  }else{
    $("input[id*='limite_efectivo_5']").attr("readonly",false); 
  }
}

