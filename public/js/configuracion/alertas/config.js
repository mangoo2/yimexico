$(function () {
    $('#form_alerta').validate({
        rules: {
            avance: "required",
        },
        messages: {
            avance: "Campo obligatorio",

        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: base_url+"ConfigAlerta/submitConfig",
                data: $(form).serialize(),
                beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                },
                success: function (result) {
                    //console.log(result);
                    $("#btn_submit").attr("disabled",false);
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    setTimeout(function () { window.location.href = base_url+"Administracion_sistema"; }, 1500);
                }
            });
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });
});
