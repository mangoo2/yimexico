var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
//=============
    load();
});
function load(){
  //table_tipo.destroy();
  table=$("#table_datos").DataTable({
    "bProcessing": true,
    "serverSide": true,
    "ajax": {
       "url": base_url+"Giros_fiscales/getlistado",
       type: "post",
       "data": function(d){
        // d.bodegaId = $('#bodegaId option:selected').val()
        },
        error: function(){
           $("#table_datos").css("display","none");
        }
    },
    "columns": [
        {"data": "clave"},
        {"data": "acitividad"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
            var riesgo_texto='';   
            if(row.riesgo==2){
               riesgo_texto='Alta'; 
            }else if(row.riesgo==1){
               riesgo_texto='Baja';
            }     
            return riesgo_texto;
            }
        },
        {"data": null,
            "render": function ( data, type, row, meta ) {
            var html='<a class="btn dato_text_'+row.clave+'" data-clave="'+row.clave+'" data-actividad="'+row.acitividad+'" data-riesgo="'+row.riesgo+'" title="Editar" onclick="modal_edit('+"'"+row.clave+"'"+')"><i class="fa fa-edit" style="color: #212b4c; font-size: 22px;"></i></a>';         
                //html='<button type="button" class="btn btn-sm dato_text_'+row.clave+'" data-clave="'+row.clave+'" data-actividad="'+row.acitividad+'" data-riesgo="'+row.riesgo+'" style="background-color: #00af79; color:white" title="Editar" onclick="modal_edit('+"'"+row.clave+"'"+')"><i class="fa fa-edit"></i></button>';        
            return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}
function modal_edit(clave) {
	$('#modal_editar').modal();
	$('#riesgo').find('option').remove();
	$('#riesgo').prepend("<option value='2' >Alto</option>");
	$('#riesgo').prepend("<option value='1' >Bajo</option>");
    $('.clave_nombre').html($('.dato_text_'+clave).data('clave'));
	$('.actividad_nombre').html($('.dato_text_'+clave).data('actividad'));
	var riesgo = $('.dato_text_'+clave).data('riesgo');
	$('#riesgo option[value="'+riesgo+'"]').attr("selected","selected");
	$('#clave').val(clave);
}
function editar(){
	var form_register = $('#form_actividad');
    var datos = form_register.serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Giros_fiscales/registro',
        data: datos,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
        	$('#modal_editar').modal('hide');
            swal("Éxito", "Actualizado Correctamente", "success");
            table.ajax.reload();
        }
    });     
}