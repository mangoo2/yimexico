var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
//=============

    load();

});
function load(){
  //table_tipo.destroy();
  table=$("#table_datos").DataTable({
    "bProcessing": true,
    "serverSide": true,
    "ajax": {
       "url": base_url+"Paises/getlistado_indice_global_terrorismo",
       type: "post",
       "data": function(d){
        // d.bodegaId = $('#bodegaId option:selected').val()
        },
        error: function(){
           $("#table_datos").css("display","none");
        }
    },
    "columns": [
        {"data": "pais"},
        {"data": "riesgo"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
            var html='<a class="btn dato_registro_'+row.id+'" data-pais="'+row.pais+'" data-estado="'+row.estado+'" data-riesgo="'+row.riesgo+'" title="Editar" onclick="modal_edit('+row.id+')"><i class="fa fa-edit" style="color: #212b4c; font-size: 22px;"></i></a>'; 
                html+='<button id="elimina" title="Eliminar" class="btn delete" onclick="modaleliminar('+row.id+')"><i class="fa fa-trash" style="color: #212b4c; font-size: 22px;"></i></button>';
            return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}
function modal_edit(id) {
	$('#modal_editar').modal();
    $('.modal-title').html('Editar País');
	$('#riesgo').find('option').remove();
	$('#riesgo').prepend("<option value='1' >1</option>");
    $('#riesgo').prepend("<option value='2' >2</option>");
    $('#riesgo').prepend("<option value='3' >3</option>");
	$('#pais').val($('.dato_registro_'+id).data('pais'));
	var riesgo = $('.dato_registro_'+id).data('riesgo');
    setTimeout(function(){
    $('#riesgo option[value="'+riesgo+'"]').attr("selected",true);
    }, 1000);
	$('#id_registro').val(id);
}
function editar(){
	var form_register = $('#form_registro');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            /*
            rules: {
                calificacion:{
                  required: true
                } 
            },
            */
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#form_registro").valid();
        if($valid) {
            var datos = form_register.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'Paises/registro_indice_global_terrorismo',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                	$('#modal_editar').modal('hide');
                    swal("Éxito", "Actualizado Correctamente", "success");
                    table.ajax.reload();
                }
            });
        }
      
}

function nuevo_registro(){
    $('#modal_editar').modal();
    $('.modal-title').html('Alta País');
    $('#riesgo').find('option').remove();
    $('#riesgo').prepend("<option value='1' >1</option>");
    $('#riesgo').prepend("<option value='2' >2</option>");
    $('#riesgo').prepend("<option value='3' >3</option>");
    $('#pais').val('');
    $('#id_registro').val(0);
}

var id_registro = 0;
function modaleliminar(id){
    id_registro=id;
    $('#modal_eliminar').modal();
}

function eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Paises/update_paises_indice_global_terrorismo',
        data:{id:id_registro},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito", "Eliminado Correctamente", "success");
            setTimeout(function(){ 
               $('#modal_eliminar').modal('hide');
                table.ajax.reload();
            }, 1500);
        }
    });     
}