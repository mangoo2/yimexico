var base_url=$('#base_url').val();
$(document).ready(function() {
    $.ajax({
        type: 'POST',
        url : base_url+'Bitacora/getClientes',
        success: function(data){
            //console.log(data);
            $("#id_cliente").html(data);
            $("#id_cliente").select2({ sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)) });
        }
    }); 
    $("#id_cliente").on("change",function(){
        if($("#id_cliente").val()!="0"){
            load();
        }
    });
    $("#exportar").on("click",function(){
        if($("#id_cliente option:selected").val()!=undefined && $("#id_cliente option:selected").val()!=0){
            exportar();
        }
    });
    //load();
});

function exportar(){
    id_cliente=$("#id_cliente option:selected").val(); id_tipo=$("#id_cliente option:selected").data("tipo");
    window.open(base_url+"Bitacora/Exportar/"+id_cliente+"/"+id_tipo, '_blank');
}

function load(){
  //table_tipo.destroy();
  table=$("#table_bita").DataTable({
    "bProcessing": true,
    "serverSide": true,
    destroy:true,
    "ajax": {
        "url": base_url+"Bitacora/getlistado",
        type: "post",
        data: { id_cliente:$("#id_cliente option:selected").val(), id_tipo:$("#id_cliente option:selected").data("tipo") }
    },
    "columns": [
        {"data": "id"},
        {"data": "folio_cliente"},
        /*{"data": null,
            "render": function ( data, type, row, meta ) {
                var html = "";
                if(row.idtipo_cliente==1)
                    html=row.nombre+" "+row.apellido_paterno+" "+row.apellido_materno;
                else if(row.idtipo_cliente==2)
                    html=row.razon_social;
                else if(row.idtipo_cliente==3)
                    html=row.denominacion_razon_social;

                return html;
            }
        },*/
        {"data": "fecha_consulta"},
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var tipo="";
                if(row.id_bene>0)
                    tipo="Búsqueda de DB";
                else
                    tipo="Búsqueda de Cliente";

                return tipo;
            }
        },
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var tipo="";
                if(row.id_bene>0)
                    tipo=row.beneficiario;
                else
                    tipo=row.cliente;

                return tipo;
            }
        },
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var resultado="";
                //console.log("resultado: "+row.resultado);
                //console.log("resultado_bene: "+row.resultado_bene);
                if(row.id_bene==0 && row.resultado!=""){
                    //var array = $.parseJSON(row.resultado);
                    //resultado=array.Message;
                    /*$.each(array, function(index, item) {
                        if(item.resultado!=""){
                            var resultado = $.parseJSON(item.resultado);
                            //console.log(resultado);
                        }
                    });*/
                    /*if(row.resultado.length>74){
                        var array = $.parseJSON(row.resultado);
                        $.each(array, function(index, item) {
                            if(item.Status=="OK"){

                            }
                        });
                    }*/
                    
                    resultado = row.resultado;
                }else if(row.id_bene>0 && row.resultado_bene!=""){
                    //var array = $.parseJSON(row.resultado_bene);
                    //resultado=array.Message;
                    /*$.each(array, function(index, item) {
                        if(item.resultado_bene!=""){
                            var resultado = $.parseJSON(item.resultado_bene);
                            //console.log(resultado);
                        }
                    });*/
                    resultado = row.resultado_bene;
                }
                return resultado;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  });
}
